Date: 12 March 2015 
PCM firmware V1.08 for the followings. 
1.	Removed web pages for total & run time data  display
2.	PCM & Scales RTC updated at 1:00 am instead of 12:00 am
3.	PCM will use only PCS IP address resolved from DNS and will not use hardcoded IP. 
4.	PCM decodes the response received against post measurement request for field "NeedsStatus: true" and sends the connectivity status 
accordingly.

Note: Please also note that PCM doesn�t close a socket opened for PCS server communication when it finds the scales are not connected.  
I think  in this case, PCS might be closing the socket when it doesn�t receive any post measurement request from PCM. Need to confirm this from James. 

