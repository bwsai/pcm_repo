/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    HTTP_CGI.C
 *      Purpose: HTTP Server CGI Module
 *      Rev.:    V4.70
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2013 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <Net_Config.h>
#include <stdio.h>
#include <string.h>
#include "Http_Client.h"
#include "Modbus_Tcp_Master.h"
#include "version.h"
#include "RTOS_main.h"
#include "Global_ex.h"
#include "I2C_driver.h"
#include "main.h"
/* ---------------------------------------------------------------------------
 * The HTTP server provides a small scripting language.
 *
 * The script language is simple and works as follows. Each script line starts
 * with a command character, either "i", "t", "c", "#" or ".".
 *   "i" - command tells the script interpreter to "include" a file from the
 *         virtual file system and output it to the web browser.
 *   "t" - command should be followed by a line of text that is to be output
 *         to the browser.
 *   "c" - command is used to call one of the C functions from the this file.
 *         It may be followed by the line of text. This text is passed to
 *         'cgi_func()' as a pointer to environment variable.
 *   "#' - command is a comment line and is ignored (the "#" denotes a comment)
 *   "." - denotes the last script line.
 *
 * --------------------------------------------------------------------------*/

extern U8     own_hw_adr[ETH_ADRLEN];
/* at_System.c */
extern  LOCALM localm[];

#define LocPPP localm[NETIF_PPP] //Added by DK on 2 Feb 2015 
/* Net_Config.c */
extern struct tcp_cfg   tcp_config;
extern struct http_cfg  http_config;
#define tcp_NumSocks    tcp_config.NumSocks
#define tcp_socket      tcp_config.Scb
#define http_EnAuth     http_config.EnAuth
#define http_auth_passw http_config.Passw

//extern BOOL LEDrun;
//extern void LED_out (U32 val);

/* Local variables. */
//static U8 P2;
static const char state[][9] = {
  "FREE",
  "CLOSED",
  "LISTEN",
  "SYN_REC",
  "SYN_SENT",
  "FINW1",
  "FINW2",
  "CLOSING",
  "LAST_ACK",
  "TWAIT",
  "CONNECT"};

/* My structure of CGI status U32 variable. This variable is private for */
/* each HTTP Session and is not altered by HTTP Server. It is only set to  */
/* zero when the cgi_func() is called for the first time.                  */
typedef struct {
  U16 xcnt;
  U16 unused;
} MY_BUF;
#define MYBUF(p)        ((MY_BUF *)p)

/*----------------------------------------------------------------------------
 * HTTP Server Common Gateway Interface Functions
 *---------------------------------------------------------------------------*/

/*--------------------------- cgi_process_var -------------------------------*/

void cgi_process_var (U8 *qs) {
  /* This function is called by HTTP server to process the Querry_String   */
  /* for the CGI Form GET method. It is called on SUBMIT from the browser. */
  /*.The Querry_String.is SPACE terminated.                                */
}


/*--------------------------- cgi_process_data ------------------------------*/

void cgi_process_data (U8 code, U8 *dat, U16 len) {
  /* This function is called by HTTP server to process the returned Data    */
  /* for the CGI Form POST method. It is called on SUBMIT from the browser. */
  /* Parameters:                                                            */
  /*   code  - callback context code                                        */
  /*           0 = www-url-encoded form data                                */
  /*           1 = filename for file upload (0-terminated string)           */
  /*           2 = file upload raw data                                     */
  /*           3 = end of file upload (file close requested)                */
  /*           4 = any xml encoded POST data (single or last stream)        */
  /*           5 = the same as 4, but with more xml data to follow          */
  /*               Use http_get_content_type() to check the content type    */  
  /*   dat   - pointer to POST received data                                */
  /*   len   - received data length                                         */
//  U8 passw[12],retyped[12];
  //U8 *var,stpassw,*pType;
//	U8 *var,stpassw;
		
  switch (code) {
    case 0:
      /* Url encoded form data received. */
			while(1);//NVIC_SystemReset();
      break;

    case 1:
      /* Filename for file upload received as encoded by the browser. */
      /* It might contain an absolute path to a file from the sending */
      /* host. Open a file for writing. */
      return;

    case 2:
      /* File content data received. Write data to a file. */
      /* This function will be called several times with   */
      /* code 2 when a big file is being uploaded.         */
      return;

    case 3:
      /* File upload finished. Close a file. */
      return;

    case 4:
      /* XML encoded content type, last packet. */
   //   pType = http_get_content_type ();
		   http_get_content_type ();
      /* check the content type for CGX file request. */
      /* pType is a pointer to a 0-terminated string  */
      /* For example: text/xml; charset=utf-8         */
      return;
     
    case 5:
      /* XML encoded as under 4, but with more to follow. */
      return;

    default:
      /* Ignore all other codes. */
      return;
  }

}


/*--------------------------- cgi_func --------------------------------------*/

U16 cgi_func (U8 *env, U8 *buf, U16 buflen, U32 *pcgi) {
  /* This function is called by HTTP server script interpreter to make a    */
  /* formated output for 'stdout'. It returns the number of bytes written   */
  /* to the output buffer. Hi-bit of return value (len is or-ed with 0x8000)*/
  /* is a repeat flag for the system script interpreter. If this bit is set */
  /* to 1, the system will call the 'cgi_func()' again for the same script  */
  /* line with parameter 'pcgi' pointing to a 4-byte buffer. This buffer    */
  /* can be used for storing different status variables for this function.  */
  /* It is set to 0 by HTTP Server on first call and is not altered by      */
  /* HTTP server for repeated calls. This function should NEVER write more  */
  /* than 'buflen' bytes to the buffer.                                     */
  /* Parameters:                                                            */
  /*   env    - environment variable string                                 */
  /*   buf    - HTTP transmit buffer                                        */
  /*   buflen - length of this buffer (500-1400 bytes - depends on MSS)     */
  /*   pcgi   - pointer to session local buffer used for repeated loops     */
  /*            This is a U32 variable - size is 4 bytes. Value is:         */
  /*            - on 1st call = 0                                           */
  /*            - 2nd call    = as set by this function on first call       */
  TCP_INFO *tsoc;
  U32 len = 0;
//  U8 id, *lang;
	U8 *lang;
  U8 max_integrators;
	FLOAT_UNION temp;
	INT32_DATA itemp1,itemp2;
	
  int num1,num2;
	char tChar1,tChar2;
	//os_mut_wait (&tcp_com_mutex, 0xffff);	

  switch (env[0]) {
    /* Analyze the environment string. It is the script 'c' line starting */
    /* at position 2. What you write to the script file is returned here. */
    case 'a' :
      /* PCM Configurations - file 'PCM_Settings.cgi' */
      switch (env[2]) {
				case 'a': //Unique PCM number
          len = sprintf((char *)buf,(const char *)&env[4],PCM_ID);
          break;
				case 'y': //PCM MAC Address
          len = sprintf((char *)buf,(const char *)&env[4],own_hw_adr[0],own_hw_adr[1],own_hw_adr[2],own_hw_adr[3],own_hw_adr[4],own_hw_adr[5]);
          break;
        case 'i':  //PCM IP address
          len = sprintf((char *)buf,(const char *)&env[4],LocM.IpAdr[0],
                        LocM.IpAdr[1],LocM.IpAdr[2],LocM.IpAdr[3]);
          break;
        case 'm':  //PCM IP Subnet mask 
          len = sprintf((char *)buf,(const char *)&env[4],LocM.NetMask[0],
                        LocM.NetMask[1],LocM.NetMask[2],LocM.NetMask[3]);
          break;
        case 'g':  //PCM IP Gateway address
          len = sprintf((char *)buf,(const char *)&env[4],LocM.DefGW[0],
                        LocM.DefGW[1],LocM.DefGW[2],LocM.DefGW[3]);
          break;
        case 'p':  //PCM primary DNS server address
          len = sprintf((char *)buf,(const char *)&env[4],LocM.PriDNS[0],
                        LocM.PriDNS[1],LocM.PriDNS[2],LocM.PriDNS[3]);
          break;
        case 's':  //PCM secondary DNS server address
          len = sprintf((char *)buf,(const char *)&env[4],LocM.SecDNS[0],
                        LocM.SecDNS[1],LocM.SecDNS[2],LocM.SecDNS[3]);
          break;
				case 't':  //PCS IP address
          len = sprintf((char *)buf,(const char *)&env[4],PCS_IpAdr[0],
                        PCS_IpAdr[1],PCS_IpAdr[2],PCS_IpAdr[3]);
          break;
				case 'v':  //PCM firmware Version 
          len = sprintf((char *)buf,(const char *)&env[4],FirmwareVer);
          break;
				//Added by DK on 2 Feb 2015 
				case 'b':  //PCM PPP IP address
          len = sprintf((char *)buf,(const char *)&env[4],LocPPP.IpAdr[0],
                        LocPPP.IpAdr[1],LocPPP.IpAdr[2],LocPPP.IpAdr[3]);
          break;
        case 'c':  //PCM PPP IP Gateway address
          len = sprintf((char *)buf,(const char *)&env[4],LocPPP.DefGW[0],
                        LocPPP.DefGW[1],LocPPP.DefGW[2],LocPPP.DefGW[3]);
          break;
        case 'd':  //PCM PPP primary DNS server address
          len = sprintf((char *)buf,(const char *)&env[4],LocPPP.PriDNS[0],
                        LocPPP.PriDNS[1],LocPPP.PriDNS[2],LocPPP.PriDNS[3]);
          break;
        case 'e':  //PCM PPP secondary DNS server address
          len = sprintf((char *)buf,(const char *)&env[4],LocPPP.SecDNS[0],
                        LocPPP.SecDNS[1],LocPPP.SecDNS[2],LocPPP.SecDNS[3]);
          break;
        case 'w':  //DST ON/OFF
					if(dst_on_off == 0)
						len = sprintf((char *)buf,(const char *)&env[4],"OFF");
					else
						len = sprintf((char *)buf,(const char *)&env[4],"ON");
          break;				
      }
      break;

		case 'b' :
      /* Scale Configurations - file 'Scale_Settings.cgi' */
		//	max_integrators = MAX_NO_OF_INTEGRATORS;  //Commented  by DK on 23 Feb 2015 
			max_integrators = Total_Scales;   //Added by DK on 23 Feb 2015 
		  tChar1 = env[4];
			tChar2 = env[5];
			num1=0;
		  num2=0;
			if((tChar1 >= '0') && (tChar1 < '4') && (tChar2 >= '0') && (tChar2 <= '9'))
			{
				num1= tChar1- '0';
				num1= num1 * 10;
				num2 = tChar2- '0';
				num1= num1 + num2;
			}
		//	if(num1 > 0 && num1 <=max_integrators)
			if(num1 > 0 && num1 < MAX_NO_OF_INTEGRATORS) //Changed by DK on 23 Feb 2015 
			{
				num1 = num1 -1;
			}
			else break; //Added by DK on 23 Feb 2015 
			switch (env[2]) {
				case 'a': //Total number of Scales 
          len = sprintf((char *)buf,(const char *)&env[4],Total_Scales);
          break;
        case 'b':  //Scale's MB Slave ID 
						if(max_integrators > 0 && num1 < max_integrators )
						{
								len = sprintf((char *)buf,(const char *)&env[6],Modbus_Tcp_Slaves[num1].Slave_ID);
						}
						break;
				case 'c':  //Scale's MB Slave IP address 
						if(max_integrators > 0 && num1 < max_integrators )
						{
							len = sprintf((char *)buf,(const char *)&env[6],Modbus_Tcp_Slaves[num1].IP_ADRESS[3],Modbus_Tcp_Slaves[num1].IP_ADRESS[2], Modbus_Tcp_Slaves[num1].IP_ADRESS[1], Modbus_Tcp_Slaves[num1].IP_ADRESS[0]);
						}
						break;		
				case 'd':  //Scale's connection status 
					  if(max_integrators > 0 && num1 < max_integrators )
						{
							if(Modbus_Tcp_Slaves[num1].Connect !=0)
							len = sprintf((char *)buf,(const char *)&env[6],"Connected");
							else
							len = sprintf((char *)buf,(const char *)&env[6],"Disconnected");	
						}
						break;		
				case 'e':  //Scale's MB Slave socket number  
					  if(max_integrators > 0 && num1 < max_integrators )
						{
							len = sprintf((char *)buf,(const char *)&env[6],Modbus_Tcp_Slaves[num1].socket_number);
						}
						break;					
	    }
      break;
			
case 'e':
     /* Integrator Reocrds - file 'Records.cgi' */
			MB_Polling=RECORDS_POLLING;    
      //max_integrators=MAX_NO_OF_INTEGRATORS;
			max_integrators = Total_Scales;
      tChar1 = env[4];
			tChar2 = env[5];
      num1=0;
		  num2=0;
			if((tChar1 >= '0') && (tChar1 < '4') && (tChar2 >= '0') && (tChar2 <= '9'))
			{
				num1= tChar1- '0';
				num1= num1 * 10;
				num2 = tChar2- '0';
				num1= num1 + num2;
			}
      //if(num1 > 0 && num1 <=max_integrators)
			if(num1 > 0 && num1 < MAX_NO_OF_INTEGRATORS) //Changed by DK on 23 Feb 2015 
			{
				num1 = num1 -1;
			}
			else break; //Added by DK on 23 Feb 2015 
			switch (env[2]) {
        case 'a':
         		if(max_integrators > 0 && num1 < max_integrators )
						{
							len = sprintf((char *)buf,(const char *)&env[6],Scale_Record_Mb_Reg[num1].Scale_Id.uint_val);
						}	
						break;
				case 'b':
						if(max_integrators > 0 && num1 < max_integrators )
						{
							len = sprintf((char *)buf,(const char *)&env[6],Scale_Record_Mb_Reg[num1].Function_Code.uint_val);
						}	
          break;		
				case 'c':
						if(max_integrators > 0 && num1 < max_integrators )
						{
							len = sprintf((char *)buf,(const char *)&env[6],Scale_Record_Mb_Reg[num1].Record_Hash.uint_val);
						}	
          break;				
				case 'd':
						if(max_integrators > 0 && num1 < max_integrators )
						{
							itemp1.int_array[0]=Scale_Record_Mb_Reg[num1].acc_wt.s_array[1];
							itemp1.int_array[1]=Scale_Record_Mb_Reg[num1].acc_wt.s_array[0];
							itemp2.int_array[0]=Scale_Record_Mb_Reg[num1].acc_wt.s_array[3];
							itemp2.int_array[1]=Scale_Record_Mb_Reg[num1].acc_wt.s_array[2];
							len = sprintf((char *)buf,(const char *)&env[6],itemp1.value,itemp2.value);
						}	
          break;				
				case 'e':
						if(max_integrators > 0 && num1 < max_integrators )
						{
							temp.f_array[0]=Scale_Record_Mb_Reg[num1].scale_rate.f_array[1];
							temp.f_array[1]=Scale_Record_Mb_Reg[num1].scale_rate.f_array[0];
							//len = sprintf((char *)buf,(const char *)&env[4],Scale_Record_Mb_Reg[0].scale_rate.float_val);
							len = sprintf((char *)buf,(const char *)&env[6],temp.float_val);
						}	
          break;						
				case 'f':
						if(max_integrators > 0 && num1 < max_integrators )
						{
							temp.f_array[0]=Scale_Record_Mb_Reg[num1].scale_load_perc.f_array[1];
							temp.f_array[1]=Scale_Record_Mb_Reg[num1].scale_load_perc.f_array[0];
							len = sprintf((char *)buf,(const char *)&env[6],temp.float_val);
						}	
          break;								
				case 'g':
						if(max_integrators > 0 && num1 < max_integrators )
						{
							temp.f_array[0]=Scale_Record_Mb_Reg[num1].scale_angle.f_array[1];
							temp.f_array[1]=Scale_Record_Mb_Reg[num1].scale_angle.f_array[0];
							len = sprintf((char *)buf,(const char *)&env[6],temp.float_val);
						}	
				  break;										
				case 'h':
						if(max_integrators > 0 && num1 < max_integrators )
						{
							itemp1.int_array[0]=Scale_Record_Mb_Reg[num1].daily_wt.s_array[1];
							itemp1.int_array[1]=Scale_Record_Mb_Reg[num1].daily_wt.s_array[0];
							itemp2.int_array[0]=Scale_Record_Mb_Reg[num1].daily_wt.s_array[3];
							itemp2.int_array[1]=Scale_Record_Mb_Reg[num1].daily_wt.s_array[2];
							len = sprintf((char *)buf,(const char *)&env[6],itemp1.value,itemp2.value);
						}			
					break;

				
      }
      break;
			
    case 'p':
      /* TCP status - file 'tcp.cgi' */
      while ((len + 150) < buflen) {
        tsoc = &tcp_socket[MYBUF(pcgi)->xcnt];
        MYBUF(pcgi)->xcnt++;
        /* 'sprintf' format string is defined here. */
        len += sprintf((char *)(buf+len),"<tr align=\"center\">");
        if (tsoc->State <= TCP_STATE_CLOSED) {
          len += sprintf ((char *)(buf+len),
                          "<td>%d</td><td>%s</td><td>-</td><td>-</td>"
                          "<td>-</td><td>-</td></tr>\r\n",
                          MYBUF(pcgi)->xcnt,state[tsoc->State]);
        }
        else if (tsoc->State == TCP_STATE_LISTEN) {
          len += sprintf ((char *)(buf+len),
                          "<td>%d</td><td>%s</td><td>-</td><td>-</td>"
                          "<td>%d</td><td>-</td></tr>\r\n",
                          MYBUF(pcgi)->xcnt,state[tsoc->State],tsoc->LocPort);
        }
        else {
          len += sprintf ((char *)(buf+len),
                          "<td>%d</td><td>%s</td><td>%d.%d.%d.%d</td>"
                          "<td>%d</td><td>%d</td><td>%d</td></tr>\r\n",
                          MYBUF(pcgi)->xcnt,state[tsoc->State],
                          tsoc->RemIpAdr[0],tsoc->RemIpAdr[1],
                          tsoc->RemIpAdr[2],tsoc->RemIpAdr[3],
                          tsoc->RemPort,tsoc->LocPort,tsoc->AliveTimer);
        }
        /* Repeat for all TCP Sockets. */
        if (MYBUF(pcgi)->xcnt == tcp_NumSocks) {
          break;
        }
      }
      if (MYBUF(pcgi)->xcnt < tcp_NumSocks) {
        /* Hi bit is a repeat flag. */
        len |= 0x8000;
      }
      break;

    case 't':
      /* Browser Language - file 'language.cgi' */
      lang = http_get_lang();
      if (strcmp ((const char *)lang, "en") == 0) {
        lang = "English";
      }
      else if (strcmp ((const char *)lang, "en-us") == 0) {
        lang = "English USA";
      }
      else if (strcmp ((const char *)lang, "en-gb") == 0) {
        lang = "English GB";
      }
      else if (strcmp ((const char *)lang, "de") == 0) {
        lang = "German";
      }
      else if (strcmp ((const char *)lang, "de-ch") == 0) {
        lang = "German CH";
      }
      else if (strcmp ((const char *)lang, "de-at") == 0) {
        lang = "German AT";
      }
      else if (strcmp ((const char *)lang, "fr") == 0) {
        lang = "French";
      }
      else if (strcmp ((const char *)lang, "sl") == 0) {
        lang = "Slovene";
      }
      else {
        lang = "Unknown";
      }
      len = sprintf((char *)buf,(const char *)&env[2],lang,http_get_lang());
      break;
  }
	//os_mut_release (&tcp_com_mutex);
	return ((U16)len);
}

/*--------------------------- cgx_content_type ------------------------------*/

U8 *cgx_content_type (void) {
  /* User configurable Content-type for CGX script files. If this function */
  /* is missing or returns a NULL pointer, the default 'text/xml' content  */
  /* type is used from the library for xml-script file types.              */

  /* Content type 0-terminated string must contain also termination cr-lf. */
  return ("Text/xml; charset=utf-8\r\n");
}


/*--------------------------- http_accept_host ------------------------------*/
#if 0
BOOL http_accept_host (U8 *rem_ip, U16 rem_port) {
  /* This function checks if a connection from remote host is accepted or  */
  /* not. If this function is missing, all remote hosts are accepted.      */

   if (rem_ip[0] == 192  &&
       rem_ip[1] == 168  &&
       rem_ip[2] == 1    &&
       rem_ip[3] == 1) {
      /* Accept a connection. */  
      return (__TRUE);
   }
   /* Deny a connection. */
   return (__FALSE);
}
#endif

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
