/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename        : USB_main.c
* @brief               : Controller Board
*
* @author               : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include <RTL.h>                      /* RTL kernel functions & defines       */
#include <stdio.h>                    /* standard I/O .h-file                 */
#include <ctype.h>                    /* character functions                  */
#include <string.h>                   /* string and memory functions          */
#include "File_Config.h"
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "USB_main.h"
#include "rtos_main.h"
//#include "File_update.h"
#include <float.h>
//#include "Ext_flash_high_level.h"
#include "../GUI/Application/Screen_global_ex.h"
#ifdef USB
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
int con1 =0;
int corrupt_usb_Flag=0;
unsigned int USB_err_no;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
 prop_file_ini prop_file;
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
//U8 file_write;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
extern U32  usbh_ohci_hw_reg_rd           (U32 reg_ofs);
extern void usbh_ohci_hw_reg_wr           (U32 reg_ofs, U32 val);

/*****************************************************************************
* @note       Function name: static int init_msd (char *letter)
* @returns    returns       : 1 if initialization is successful
                           : -1 if initialization is unsuccessful
* @param      arg1         : Name of the drive to be created
* @author                   : Anagha Basole
* @date       date created : 25th July 2012
* @brief      Description   : Initialize a Flash Mass Storage Device
* @note       Notes         : None
*****************************************************************************/
#ifdef USB
int init_msd (char *letter)
{
  U32 retv;

  retv = finit (letter);
	if (retv > 1)
  {
    //printf ("\nMass Storage Device %s is Unformatted", letter);
    retv = 0;
  }
  return (~(retv) & 1);
}
#endif /*#ifdef USB*/
/*****************************************************************************
* @note       Function name: __task void usb_task (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created :
* @brief      Description  : USB task for enumeration of the drives
* @note       Notes        : None
*****************************************************************************/
__task void usb_task (void)
{
FILE *fp;
	LPC_RTC->ALDOW = 0;
  while (1)
  {

    os_itv_set(USB_CON_TASK_INTERVAL);
    os_itv_wait();
		LPC_RTC->GPREG4 |= (0x01<<4);
    usbh_engine_all();
// 		if((con == 0)&&(con_ex ==0 ))
// 		{
// 			finit("U0:");
// 		}
// 		else
// 		{
    con1 = usbh_msc_status(0, 0);
//		}
    //Flags_struct.Connection_flags &= ~con;
	            if (((con1 ^ con_ex)&&(con1==1)))
            {
                if ((LPC_RTC->ALDOW &(0x01<<2))==(0x01<<2))
                {
                    con =0;
										corrupt_usb_Flag =1;

                }
                else
                {
                    LPC_RTC->ALDOW |=0x01<<2;
                    finit("U0");
                    LPC_RTC->ALDOW &=~(0x01<<2);
                    USB_err_no = usbh_msc_get_last_error(0,0);
										
                    if ((USB_err_no >0)&& (con1 ==1))
                    {
                        con =0;
												corrupt_usb_Flag =1;

                    }
                    if ((USB_err_no ==0)&& (con1 ==1))
                    {
                        if ((fp = fopen("temp","w"))==0)
                        {
                            con =0;
														corrupt_usb_Flag =1;

                        }
                        else
                        {
												 fclose (fp);
                            con =1;
														corrupt_usb_Flag =0;
                            Flags_struct.Connection_flags/*.Communication_flags*/|=  USB_CON_FLAG;
                        }
                    }
                    else
                    {
                        con =0;

                    }

                }
            }
						else
						{
							if ((con1 ^ con_ex)&&(con1 ==0))
							{
								corrupt_usb_Flag=0;
								con=0;
								Flags_struct.Connection_flags/*.Communication_flags*/&=  ~USB_CON_FLAG;
							}
// 							else if((con1 == 0)&&(con_ex == 0)&&(con==0))
// 							{
// 								Flags_struct.Connection_flags/*.Communication_flags*/&=  ~USB_CON_FLAG;
// 							}
						}	
				if ((corrupt_usb_Flag==1)&&(disp_power_on_screen_fg==1))
				{
														GUI_DrawBitmap(&bmUSB_red_small, WINDOW_FOOTER_ICON2_POS_X0, WINDOW_FOOTER_ICON2_POS_Y0);
                            GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);
                            GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
                            GUI_DispStringAt("CORRUPT USB!!", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
                            Flags_struct.Connection_flags/*.Communication_flags*/&=~USB_CON_FLAG;
				}	
					 con_ex  = con1;
	
		
		
// 		if (con == 1)
// 		{
// 		//	 Flags_struct.Connection_flags/*.Communication_flags*/ |= USB_CON_FLAG;
// 			USB_Connection|= USB_CON_FLAG;
// 			Flags_struct.Connection_flags |= USB_CON_FLAG;
// 		}
// 		else
// 		{
// 			//  Flags_struct.Connection_flags/*.Communication_flags*/ &= ~USB_CON_FLAG;
// 			USB_Connection&= ~USB_CON_FLAG;
// 			Flags_struct.Connection_flags &= ~USB_CON_FLAG;
// 		}
//     //if((Flags_struct.Connection_flags & USB_CON_FLAG)^(Flags_struct.Connection_flags & USB_CON_EX_FLAG))
//     if(con^con_ex)
//     {                       /* if connection changed */
//         if ((con ^ con_ex) &  con & 1)
// 				{
// 					 con |= init_msd ("U0:");
// 				}
//         fflush (stdout);
//         con_ex = con ;
//     }
   /* if((con == 1) && ((Flags_struct.Log_flags & LOGS_WR_FLAG) == LOGS_WR_FLAG))
    {
      flash_log_restore_to_usb();
      Flags_struct.Log_flags &= ~LOGS_WR_FLAG;
    }*/
		LPC_RTC->GPREG4 &= ~(0x01<<4);
//		WDTFeed();	
  }/*while 1*/
}
#endif /*ifdef USB*/ //removed 31st July

/*****************************************************************************
* End of file
*****************************************************************************/
