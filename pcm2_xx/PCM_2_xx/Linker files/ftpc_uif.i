#line 1 "FTP_Client\\FTPC_uif.c"









 

#line 1 "C:\\Keil_v4\\ARM\\RV31\\INC\\Net_Config.h"









 




#line 1 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"









 




 

 



#line 28 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"







 typedef unsigned int   size_t;


typedef signed char     S8;
typedef unsigned char   U8;
typedef short           S16;
typedef unsigned short  U16;
typedef int             S32;
typedef unsigned int    U32;
typedef long long       S64;
typedef unsigned long long U64;
typedef unsigned char   BIT;
typedef unsigned int    BOOL;

#line 55 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

#line 71 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"



 





 
typedef U32 OS_SEM[2];

 

typedef U32 OS_MBX[];

 
typedef U32 OS_MUT[3];

 
typedef U32 OS_TID;

 
typedef void *OS_ID;

 
typedef U32 OS_RESULT;

 












 




#line 202 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"



 



 
extern void      os_set_env    (void);
extern void      rt_sys_init   (void (*task)(void), U8 priority, void *stk);
extern void      rt_tsk_pass   (void);
extern OS_TID    rt_tsk_self   (void);
extern OS_RESULT rt_tsk_prio   (OS_TID task_id, U8 new_prio);
extern OS_TID    rt_tsk_create (void (*task)(void), U8 priority, void *stk, void *argv);
extern OS_RESULT rt_tsk_delete (OS_TID task_id);

#line 238 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

extern void      _os_sys_init(U32 p, void (*task)(void), U32 prio_stksz,
                                     void *stk)                        __svc_indirect(0);
extern OS_TID    _os_tsk_create (U32 p, void (*task)(void), U32 prio_stksz,
                                        void *stk, void *argv)         __svc_indirect(0);
extern OS_TID    _os_tsk_create_ex (U32 p, void (*task)(void *), U32 prio_stksz,
                                           void *stk, void *argv)      __svc_indirect(0);
extern OS_TID    _os_tsk_self (U32 p)                                  __svc_indirect(0);
extern void      _os_tsk_pass (U32 p)                                  __svc_indirect(0);
extern OS_RESULT _os_tsk_prio (U32 p, OS_TID task_id, U8 new_prio)     __svc_indirect(0);
extern OS_RESULT _os_tsk_delete (U32 p, OS_TID task_id)                __svc_indirect(0);

 
extern OS_RESULT rt_evt_wait (U16 wait_flags,  U16 timeout, BOOL and_wait);
extern void      rt_evt_set  (U16 event_flags, OS_TID task_id);
extern void      rt_evt_clr  (U16 clear_flags, OS_TID task_id);
extern U16       rt_evt_get  (void);







extern OS_RESULT _os_evt_wait(U32 p, U16 wait_flags, U16 timeout,
                                     BOOL and_wait)                    __svc_indirect(0);
extern void      _os_evt_set (U32 p, U16 event_flags, OS_TID task_id)  __svc_indirect(0);
extern void      _os_evt_clr (U32 p, U16 clear_flags, OS_TID task_id)  __svc_indirect(0);
extern U16       _os_evt_get (U32 p)                                   __svc_indirect(0);
extern void      isr_evt_set (U16 event_flags, OS_TID task_id);

 
extern void      rt_sem_init (OS_ID semaphore, U16 token_count);
extern OS_RESULT rt_sem_send (OS_ID semaphore);
extern OS_RESULT rt_sem_wait (OS_ID semaphore, U16 timeout);





extern void      _os_sem_init (U32 p, OS_ID semaphore, 
                                      U16 token_count)                 __svc_indirect(0);
extern OS_RESULT _os_sem_send (U32 p, OS_ID semaphore)                 __svc_indirect(0);
extern OS_RESULT _os_sem_wait (U32 p, OS_ID semaphore, U16 timeout)    __svc_indirect(0);
extern void      isr_sem_send (OS_ID semaphore);

 
extern void      rt_mbx_init  (OS_ID mailbox, U16 mbx_size);
extern OS_RESULT rt_mbx_send  (OS_ID mailbox, void *p_msg,    U16 timeout);
extern OS_RESULT rt_mbx_wait  (OS_ID mailbox, void **message, U16 timeout);
extern OS_RESULT rt_mbx_check (OS_ID mailbox);







extern void      _os_mbx_init (U32 p, OS_ID mailbox, U16 mbx_size)     __svc_indirect(0);
extern OS_RESULT _os_mbx_send (U32 p, OS_ID mailbox, void *message_ptr,
                                      U16 timeout)                     __svc_indirect(0);
extern OS_RESULT _os_mbx_wait (U32 p, OS_ID mailbox, void  **message,
                                      U16 timeout)                     __svc_indirect(0);
extern OS_RESULT _os_mbx_check (U32 p, OS_ID mailbox)                  __svc_indirect(0);
extern void      isr_mbx_send (OS_ID mailbox, void *message_ptr);
extern OS_RESULT isr_mbx_receive (OS_ID mailbox, void **message);

 
extern void      rt_mut_init    (OS_ID mutex);
extern OS_RESULT rt_mut_release (OS_ID mutex);
extern OS_RESULT rt_mut_wait    (OS_ID mutex, U16 timeout);





extern void      _os_mut_init (U32 p, OS_ID mutex)                     __svc_indirect(0);
extern OS_RESULT _os_mut_release (U32 p, OS_ID mutex)                  __svc_indirect(0);
extern OS_RESULT _os_mut_wait (U32 p, OS_ID mutex, U16 timeout)        __svc_indirect(0);

 
extern U32       rt_time_get (void);
extern void      rt_dly_wait (U16 delay_time);
extern void      rt_itv_set  (U16 interval_time);
extern void      rt_itv_wait (void);






extern U32       _os_time_get (U32 p)                                  __svc_indirect(0);
extern void      _os_dly_wait (U32 p, U16 delay_time)                  __svc_indirect(0);
extern void      _os_itv_set (U32 p, U16 interval_time)                __svc_indirect(0);
extern void      _os_itv_wait (U32 p)                                  __svc_indirect(0);

 
extern OS_ID     rt_tmr_create (U16 tcnt, U16 info);
extern OS_ID     rt_tmr_kill   (OS_ID timer);




extern OS_ID     _os_tmr_create (U32 p, U16 tcnt, U16 info)            __svc_indirect(0);
extern OS_ID     _os_tmr_kill (U32 p, OS_ID timer)                     __svc_indirect(0);

 
extern U32       rt_suspend    (void);
extern void      rt_resume     (U32 sleep_time);
extern void      rt_tsk_lock   (void);
extern void      rt_tsk_unlock (void);






extern U32       _os_suspend (U32 p)                                   __svc_indirect(0);
extern void      _os_resume (U32 p, U32 sleep_time)                    __svc_indirect(0);
extern void      _os_tsk_lock (U32 p)                                  __svc_indirect(0);
extern void      _os_tsk_unlock (U32 p)                                __svc_indirect(0);

 
extern int       _init_box (void *box_mem, U32 box_size, U32 blk_size);
extern void     *_alloc_box (void *box_mem);
extern void     *_calloc_box (void *box_mem);
extern int       _free_box (void *box_mem, void *box);








 




 

typedef struct {                         
  U8  hr;                                
  U8  min;                               
  U8  sec;                               
  U8  day;                               
  U8  mon;                               
  U16 year;                              
} RL_TIME;

typedef struct {                         
  S8  name[256];                         
  U32 size;                              
  U16 fileID;                            
  U8  attrib;                            
  RL_TIME time;                          
} FINFO;

extern int finit (const char *drive);
extern int funinit (const char *drive);
extern int fdelete (const char *filename);
extern int frename (const char *oldname, const char *newname);
extern int ffind (const char *pattern, FINFO *info);
extern U64 ffree (const char *drive);
extern int fformat (const char *drive);
extern int fanalyse (const char *drive);
extern int fcheck (const char *drive);
extern int fdefrag (const char *drive);
extern int fattrib (const char *par, const char *path);
extern int fvol    (const char *drive, char *buf);

 




 

 



 
#line 428 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 436 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 449 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 




 




 



 



 



 
#line 484 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 



 





 



 





 
#line 512 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 




 
#line 527 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 537 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 545 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 



 
typedef struct sockaddr {          
  U16  sa_family;                  
  char sa_data[14];                
} SOCKADDR;

#pragma push
#pragma anon_unions

typedef struct in_addr {           
  union {
    struct {
      U8 s_b1,s_b2,s_b3,s_b4;      
    };
    struct {
      U16 s_w1,s_w2;               
    };
    U32 s_addr;                    
  };
} IN_ADDR;
#pragma pop

typedef struct sockaddr_in {       
  S16 sin_family;                  
  U16 sin_port;                    
  IN_ADDR sin_addr;                
  S8  sin_zero[8];                 
} SOCKADDR_IN;

typedef struct hostent {           
  char *h_name;                    
  char **h_aliases;                
  S16  h_addrtype;                 
  S16  h_length;                   
  char **h_addr_list;              
} HOSTENT;

extern void init_TcpNet (void);
extern BOOL main_TcpNet (void);
extern void timer_tick (void);
extern U8   udp_get_socket (U8 tos, U8 opt, 
                            U16 (*listener)(U8 socket, U8 *remip, U16 port, U8 *buf, U16 len));
extern BOOL udp_release_socket (U8 socket);
extern BOOL udp_open (U8 socket, U16 locport);
extern BOOL udp_close (U8 socket);
extern BOOL udp_mcast_ttl (U8 socket, U8 ttl);
extern U8  *udp_get_buf (U16 size);
extern BOOL udp_send (U8 socket, U8 *remip, U16 remport, U8 *buf, U16 dlen);
extern U8   tcp_get_socket (U8 type, U8 tos, U16 tout,
                            U16 (*listener)(U8 socket, U8 event, U8 *buf, U16 len));
extern BOOL tcp_release_socket (U8 socket);
extern BOOL tcp_listen (U8 socket, U16 locport);
extern BOOL tcp_connect (U8 socket, U8 *remip, U16 remport, U16 locport);
extern U8  *tcp_get_buf (U16 size);
extern U16  tcp_max_dsize (U8 socket);
extern BOOL tcp_check_send (U8 socket);
extern U8   tcp_get_state (U8 socket);
extern BOOL tcp_send (U8 socket, U8 *buf, U16 dlen);
extern BOOL tcp_close (U8 socket);
extern BOOL tcp_abort (U8 socket);
extern void tcp_reset_window (U8 socket);
extern BOOL arp_cache_ip (U8 *ipadr, U8 type);
extern BOOL arp_cache_mac (U8 *hwadr);
extern void ppp_listen (const char *user, const char *passw);
extern void ppp_connect (const char *dialnum, const char *user, const char *passw);
extern void ppp_close (void);
extern BOOL ppp_is_up (void);
extern void slip_listen (void);
extern void slip_connect (const char *dialnum);
extern void slip_close (void);
extern BOOL slip_is_up (void);
extern U8   get_host_by_name (U8 *hostn, void (*cbfunc)(U8 event, U8 *host_ip));
extern BOOL smtp_connect (U8 *ipadr, U16 port, void (*cbfunc)(U8 event));
extern void dhcp_disable (void);
extern BOOL igmp_join (U8 *group_ip);
extern BOOL igmp_leave (U8 *group_ip);
extern BOOL snmp_trap (U8 *manager_ip, U8 gen_trap, U8 spec_trap, U16 *obj_list);
extern BOOL snmp_set_community (const char *community);
extern BOOL icmp_ping (U8 *remip, void (*cbfunc)(U8 event));
extern BOOL ftpc_connect (U8 *ipadr, U16 port, U8 command, void (*cbfunc)(U8 event));
extern BOOL tftpc_put (U8 *ipadr, U16 port,
                       const char *src, const char *dst, void (*cbfunc)(U8 event));
extern BOOL tftpc_get (U8 *ipadr, U16 port, 
                       const char *src, const char *dst, void (*cbfunc)(U8 event));

 
extern int  socket (int family, int type, int protocol);
extern int  bind (int sock, const SOCKADDR *addr, int addrlen);
extern int  listen (int sock, int backlog);
extern int  accept (int sock, SOCKADDR *addr, int *addrlen);
extern int  connect (int sock, SOCKADDR *addr, int addrlen);
extern int  send (int sock, const char *buf, int len, int flags);
extern int  sendto (int sock, const char *buf, int len, int flags, SOCKADDR *to, int tolen);
extern int  recv (int sock, char *buf, int len, int flags);
extern int  recvfrom (int sock, char *buf, int len, int flags, SOCKADDR *from, int *fromlen);
extern int  closesocket (int sock);
extern int  getpeername (int sock, SOCKADDR *name, int *namelen);
extern int  getsockname (int sock, SOCKADDR *name, int *namelen);
extern int  ioctlsocket (int sock, long cmd, unsigned long *argp);
extern HOSTENT *gethostbyname (const char *name, int *err);







 


 
#line 16 "C:\\Keil_v4\\ARM\\RV31\\INC\\Net_Config.h"

 



                                   






 






 




 
#line 53 "C:\\Keil_v4\\ARM\\RV31\\INC\\Net_Config.h"

 





 
#line 83 "C:\\Keil_v4\\ARM\\RV31\\INC\\Net_Config.h"

 





typedef struct os_frame {          
  U16 length;                      
  U16 index;                       
  U8  data[1];                     
} OS_FRAME;

typedef struct arp_info {          
  U8  State;                       
  U8  Type;                        
  U8  Retries;                     
  U8  Tout;                        
  U8  HwAdr[6];           
  U8  IpAdr[4];            
} ARP_INFO;

typedef struct igmp_info {         
  U8  State;                       
  U8  Tout;                        
  U8  Flags;                       
  U8  GrpIpAdr[4];         
} IGMP_INFO;

typedef struct udp_info {          
  U8  State;                       
  U8  Opt;                         
  U8  Flags;                       
  U8  Tos;                         
  U16 LocPort;                     
  U8  McastTtl;                    
                                   
  U16 (*cb_func)(U8 socket, U8 *rem_ip, U16 port, U8 *buf, U16 len);
} UDP_INFO;

typedef struct tcp_info {          
  U8  State;                       
  U8  Type;                        
  U8  Flags;                       
  U8  Tos;                         
  U8  RemIpAdr[4];         
  U16 RemPort;                     
  U16 LocPort;                     
  U16 MaxSegSize;                  
  U16 Tout;                        
  U16 AliveTimer;                  
  U16 RetryTimer;                  
  U8  AckTimer;                    
  U8  Id;                          
  U8  Retries;                     
  U8  DupAcks;                     
  U32 SendUna;                     
  U32 SendNext;                    
  U32 SendChk;                     
  U32 SendWl1;                     
  U32 SendWl2;                     
  U16 SendWin;                     
  S16 RttSa;                       
  S16 RttSv;                       
  U16 CWnd;                        
  U16 SsThresh;                    
  U16 RecWin;                      
  U32 RecNext;                     
  OS_FRAME *unack_list;            
                                   
  U16 (*cb_func)(U8 socket, U8 event, U8 *p1, U16 p2);
} TCP_INFO;

typedef struct bsd_info {          
  U8  State;                       
  U8  Socket;                      
  U8  Flags;                       
  U8  Type;                        
  U16 LocPort;                     
  U16 RemPort;                     
  U32 LocIP;                       
  U32 RemIP;                       
  U8  AcceptSock;                  
  U8  ParentSock;                  
  U8  Event;                       
  U8  Task;                        
  U16 Tout;                                               
  void *buf_list;                  
} BSD_INFO;

typedef struct http_info {         
  U8  State;                       
  U8  Socket;                      
  U16 Flags;                       
  U8  FType;                       
  U8  PostSt;                      
  U16 DelimSz;                     
  U8  UserId;                      
  U32 CGIvar;                      
  U32 DLen;                        
  U32 Count;                       
  U16 BCnt;                        
  U8  Lang[6];                     
  U32 LMDate;                      
  U8 *Script;                      
  U8 *pDelim;                      
  void *sFile;                     
  void *dFile;                     
} HTTP_INFO;

typedef struct http_file {         
  const U32 Id;                    
  const U8 *Start;                 
} HTTP_FILE;

typedef struct tnet_info {         
  U8  State;                       
  U8  Socket;                      
  U8  Flags;                       
  U8  BCnt;                        
  U16 Tout;                        
  U8  UserId;                      
  U8  Widx;                        
  U8  Ridx;                        
  U8  hNext;                       
  U8  hCurr;                       
  U32 SVar;                        
  U8  LBuf[96];           
  U8  Fifo[128];           
  U8  Hist[128];           
} TNET_INFO;

typedef struct tftp_info {         
  U8  State;                       
  U8  Socket;                      
  U8  Flags;                       
  U8  Retries;                     
  U8  RemIpAdr[4];         
  U16 RemPort;                     
  U16 BlockSz;                     
  U16 BlockNr;                     
  U8  Timer;                       
  U16 BufLen;                      
  U8  *Buf;                        
  void *File;                      
} TFTP_INFO;

typedef struct ftp_info {          
  U8  State;                       
  U8  Socket;                      
  U16 Flags;                       
  U8  RemIpAdr[4];         
  U16 DPort;                       
  U8  DSocket;                     
  U8  UserId;                      
  U8  Resp;                        
  U8  PathLen;                     
  U8 *Path;                        
  U8 *Name;                        
  void *File;                      
} FTP_INFO;

typedef struct dns_cache {         
  U32 HostId;                      
  U32 Ttl;                         
  U8  IpAdr[4];            
} DNS_CACHE;

typedef struct localm {            
  U8  IpAdr[4];            
  U8  DefGW[4];            
  U8  NetMask[4];          
  U8  PriDNS[4];           
  U8  SecDNS[4];           
} LOCALM;

typedef struct remotem {           
  U8  IpAdr[4];            
  U8  HwAdr[6];           
} REMOTEM;

typedef struct mib_entry {         
  U8  Type;                        
  U8  OidLen;                      
  U8  Oid[17];              
  U8  ValSz;                       
  void *Val;                       
  void (*cb_func)(int mode);       
} MIB_ENTRY;

typedef struct sys_cfg {           
  U32 *MemPool;                    
  U32 MemSize;                     
  U8  TickRate;                    
  U8  TickItv;                     
  U8  T200ms;                      
  U8  NetCfg;                      
  U8 *HostName;                    
} const SYS_CFG;

typedef struct arp_cfg {           
  ARP_INFO *Table;                 
  U8  TabSize;                     
  U8  TimeOut;                     
  U8  MaxRetry;                    
  U8  Resend;                      
  U8  Notify;                      
} const ARP_CFG;

typedef struct igmp_cfg {          
  IGMP_INFO *Table;                
  U16 TabSize;                     
} const IGMP_CFG;

typedef struct dhcp_cfg {          
  U8 *Vcid;                        
  U8  Opt;                         
} const DHCP_CFG;

typedef struct ppp_cfg {           
  U32 ACCmap;                      
  U16 SerTout;                     
  U16 RetryTout;                   
  U16 EchoTout;                    
  U8  MaxRetry;                    
  U8  EnAuth;                      
} const PPP_CFG;

typedef struct udp_cfg {           
  UDP_INFO *Scb;                   
  U8  NumSocks;                    
} const UDP_CFG;

typedef struct tcp_cfg {           
  TCP_INFO *Scb;                   
  U8  NumSocks;                    
  U8  MaxRetry;                    
  U16 RetryTout;                   
  U16 T2MSLTout;                   
  U16 SynRetryTout;                
  U16 InitRetryTout;               
  U16 DefTout;                     
  U16 MaxSegSize;                  
  U16 RecWinSize;                  
  U8  ConRetry;                    
} const TCP_CFG;

typedef struct http_cfg {          
  HTTP_INFO *Scb;                  
  U8  NumSess;                     
  U8  EnAuth;                      
  U16 PortNum;                     
  U8 const *SrvId;                 
  U8 const *Realm;                 
  U8 const *User;                  
  U8 *Passw;                       
} const HTTP_CFG;

typedef struct tnet_cfg {          
  TNET_INFO *Scb;                  
  U8  NumSess;                     
  U8  EnAuth;                      
  U8  NoEcho;                      
  U16 PortNum;                     
  U16 IdleTout;                    
  U8 const *User;                  
  U8 *Passw;                       
} const TNET_CFG;

typedef struct tftp_cfg {          
  TFTP_INFO *Scb;                  
  U8  NumSess;                     
  U8  MaxRetry;                    
  U16 PortNum;                     
  U8  DefTout;                     
  U8  EnFwall;                     
} const TFTP_CFG;

typedef struct tftpc_cfg {         
  U16 BlockSize;                   
  U16 RetryTout;                   
  U8  MaxRetry;                    
} const TFTPC_CFG;

typedef struct ftp_cfg {           
  FTP_INFO *Scb;                   
  U8  NumSess;                     
  U8  EnAuth;                      
  U16 PortNum;                     
  U16 IdleTout;                    
  U8  MsgLen;                      
  U8 const *Msg;                    
  U8 const *User;                  
  U8 *Passw;                       
} const FTP_CFG;

typedef struct ftpc_cfg {          
  U8  DefTout;                     
  U8  PasvMode;                    
} const FTPC_CFG;

typedef struct dns_cfg {           
  DNS_CACHE *Table;                
  U8  TabSize;                     
} const DNS_CFG;

typedef struct snmp_cfg {          
  U16 PortNum;                     
  U16 TrapPort;                    
  U8  TrapIp[4];           
  U8 const *Community;             
  U8  TickItv;                     
} const SNMP_CFG;

typedef struct bsd_cfg {           
  BSD_INFO *Scb;                   
  U8  NumSocks;                    
  U8  InRtx;                       
  U16 RcvTout;                     
} const BSD_CFG;

typedef enum {                     
  ERR_MEM_ALLOC,                   
  ERR_MEM_FREE,                    
  ERR_MEM_CORRUPT,                 
  ERR_MEM_LOCK,                    
  ERR_UDP_ALLOC,                   
  ERR_TCP_ALLOC,                   
  ERR_TCP_STATE                    
} ERROR_CODE;




 

 
extern void init_system (void);
extern void run_system (void);
extern void sys_error (ERROR_CODE code);

 
extern OS_FRAME *alloc_mem (U32 byte_size);
extern void free_mem (OS_FRAME *mem_ptr);

 
extern void eth_init_link (void);
extern void eth_run_link (void);
extern void put_in_queue (OS_FRAME *frame);
extern BOOL eth_send_frame (OS_FRAME *frame);
extern BOOL eth_chk_adr (OS_FRAME *frame);
extern U8  *eth_get_adr (U8 *ipadr);

 
extern void ppp_init_link (void);
extern void ppp_run_link (void);
extern BOOL ppp_send_frame (OS_FRAME *frame, U16 prot);

 
extern void pap_init (void);
extern void pap_process (OS_FRAME *frame);
extern void pap_run (void);

 
extern void chap_init (void);
extern void chap_process (OS_FRAME *frame);
extern void chap_run (void);

 
extern void slip_init_link (void);
extern void slip_run_link (void);
extern BOOL slip_send_frame (OS_FRAME *frame);

 
extern int  mem_copy (void *dp, void *sp, int len);
extern void mem_rcopy (void *dp, void *sp, int len);
extern BOOL mem_comp (void *sp1, void *sp2, int len);
extern void mem_set (void *dp, U8 val, int len);
extern BOOL mem_test (void *sp, U8 val, int len);
extern BOOL str_scomp (U8 *sp, U8 const *cp);
extern int  str_copy (U8 *dp, U8 *sp);
extern void str_up_case (U8 *dp, U8 *sp);

 
extern void arp_notify (void);
extern BOOL arp_get_info (REMOTEM *info);

 
extern void ip_init (void);
extern void ip_run_local (void);

 
extern void dhcp_cbfunc (U8 opt, U8 *val);

 
extern void icmp_init (void);
extern void icmp_run_engine (void);

 
extern void igmp_init (void);
extern void igmp_run_host (void);
extern void igmp_process (OS_FRAME *frame);

 
extern void udp_init (void);
extern void udp_process (OS_FRAME *frame);

 
extern void tcp_init (void);
extern void tcp_poll_sockets (void);
extern void tcp_process (OS_FRAME *frame);

 
extern void bsd_init (void);
extern void bsd_poll_sockets (void);
extern U8   bsd_wait (BSD_INFO *bsd_s, U8 evt);
extern void bsd_enable (BSD_INFO *bsd_s, U8 evt);

 
extern void bsd_init_host (void);

 
extern void http_init (void);
extern void http_run_server (void);
extern void *http_fopen (U8 *name);
extern void http_fclose (void *file);
extern U16  http_fread (void *file, U8 *buf, U16 len);
extern BOOL http_fgets (void *file, U8 *buf, U16 size);
extern U32  http_finfo (U8 *name);
extern void cgi_process_var (U8 *qstr);
extern void cgi_process_data (U8 code, U8 *dat, U16 len);
extern U16  cgi_func (U8 *env, U8 *buf, U16 buflen, U32 *pcgi);
extern U8  *cgx_content_type (void);
extern BOOL http_accept_host (U8 *rem_ip, U16 rem_port);
extern BOOL http_file_access (U8 *fname, U8 user_id);
extern U8   http_check_account (U8 *user, U8 *passw);
extern U8  *http_get_var (U8 *env, void *ansi, U16 maxlen);
extern U8  *http_get_lang (void);
extern void http_get_info (REMOTEM *info);
extern U8   http_get_session (void);
extern U8   http_get_user_id (void);
extern U8  *http_get_content_type (void);
extern U32  http_date (RL_TIME *time);

 
extern void tnet_init (void);
extern void tnet_run_server (void);
extern U16  tnet_cbfunc (U8 code, U8 *buf, U16 buflen);
extern U16  tnet_process_cmd (U8 *cmd, U8 *buf, U16 buflen, U32 *pvar);
extern BOOL tnet_ccmp (U8 *buf, U8 *cmd);
extern void tnet_set_delay (U16 cnt);
extern void tnet_get_info (REMOTEM *info);
extern U8   tnet_get_session (void);
extern U8   tnet_get_user_id (void);
extern BOOL tnet_msg_poll (U8 session);
extern BOOL tnet_accept_host (U8 *rem_ip, U16 rem_port);
extern U8   tnet_check_account (U8 code, U8 *id);

 
extern void tftp_init (void);
extern void tftp_run_server (void);
extern void *tftp_fopen (U8 *fname, U8 *mode);
extern void tftp_fclose (void *file);
extern U16  tftp_fread (void *file, U8 *buf, U16 len);
extern U16  tftp_fwrite (void *file, U8 *buf, U16 len);
extern BOOL tftp_accept_host (U8 *rem_ip, U16 rem_port);

 
extern void tftpc_init (void);
extern void tftpc_run_client (void);
extern void *tftpc_fopen (U8 *fname, U8 *mode);
extern void tftpc_fclose (void *file);
extern U16  tftpc_fread (void *file, U8 *buf, U16 len);
extern U16  tftpc_fwrite (void *file, U8 *buf, U16 len);

 
extern void ftp_init (void);
extern void ftp_run_server (void);
extern void *ftp_fopen (U8 *fname, U8 *mode);
extern void ftp_fclose (void *file);
extern U16  ftp_fread (void *file, U8 *buf, U16 len);
extern U16  ftp_fwrite (void *file, U8 *buf, U16 len);
extern BOOL ftp_fdelete (U8 *fname);
extern BOOL ftp_frename (U8 *fname, U8 *newn);
extern U16  ftp_ffind (U8 code, U8 *buf, U8 *mask, U16 len);
extern BOOL ftp_accept_host (U8 *rem_ip, U16 rem_port);
extern U8   ftp_check_account (U8 code, U8 *id);
extern U8   ftp_get_user_id (void);
extern BOOL ftp_file_access (U8 *fname, U8 mode, U8 user_id);

 
extern void ftpc_init (void);
extern void ftpc_run_client (void);
extern void *ftpc_fopen (U8 *mode);
extern void ftpc_fclose (void *file);
extern U16  ftpc_fread (void *file, U8 *buf, U16 len);
extern U16  ftpc_fwrite (void *file, U8 *buf, U16 len);
extern U16  ftpc_cbfunc (U8 code, U8 *buf, U16 buflen);

 
extern void dhcp_init (void);
extern void dhcp_run_client (void);


 
extern void nbns_init (void);

 
extern void dns_init (void);
extern void dns_run_client (void);
extern U8   get_host_by_name (U8 *hostn, void (*cbfunc)(U8, U8 *));

 
extern void smtp_init (void);
extern void smtp_run_client (void);
extern U16  smtp_cbfunc (U8 code, U8 *buf, U16 buflen, U32 *pvar);
extern BOOL smtp_accept_auth (U8 *srv_ip);

 
extern void snmp_init (void);
extern void snmp_run_agent (void);

 
extern void init_ethernet (void);
extern void send_frame (OS_FRAME *frame);
__weak void poll_ethernet (void);
extern void int_enable_eth (void);
extern void int_disable_eth (void);

 
extern void init_serial (void);
extern int  com_getchar (void);
extern BOOL com_putchar (U8 c);
extern BOOL com_tx_active (void);

 
extern void modem_init (void);
extern void modem_dial (U8 *dialnum);
extern void modem_hangup (void);
extern void modem_listen (void);
extern BOOL modem_online (void);
extern BOOL modem_process (U8 ch);
extern void modem_run (void);







 



#line 13 "FTP_Client\\FTPC_uif.c"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdio.h"
 
 
 





 






 













#line 38 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdio.h"


  
  typedef unsigned int size_t;    








 
 

 
  typedef struct __va_list __va_list;





   




 




typedef struct __fpos_t_struct {
    unsigned __int64 __pos;
    



 
    struct {
        unsigned int __state1, __state2;
    } __mbstate;
} fpos_t;
   


 


   

 

typedef struct __FILE FILE;
   






 

extern FILE __stdin, __stdout, __stderr;
extern FILE *__aeabi_stdin, *__aeabi_stdout, *__aeabi_stderr;

#line 129 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdio.h"
    

    

    





     



   


 


   


 

   



 

   


 




   


 





    


 






extern __declspec(__nothrow) int remove(const char *  ) __attribute__((__nonnull__(1)));
   





 
extern __declspec(__nothrow) int rename(const char *  , const char *  ) __attribute__((__nonnull__(1,2)));
   








 
extern __declspec(__nothrow) FILE *tmpfile(void);
   




 
extern __declspec(__nothrow) char *tmpnam(char *  );
   











 

extern __declspec(__nothrow) int fclose(FILE *  ) __attribute__((__nonnull__(1)));
   







 
extern __declspec(__nothrow) int fflush(FILE *  );
   







 
extern __declspec(__nothrow) FILE *fopen(const char * __restrict  ,
                           const char * __restrict  ) __attribute__((__nonnull__(1,2)));
   








































 
extern __declspec(__nothrow) FILE *freopen(const char * __restrict  ,
                    const char * __restrict  ,
                    FILE * __restrict  ) __attribute__((__nonnull__(2,3)));
   








 
extern __declspec(__nothrow) void setbuf(FILE * __restrict  ,
                    char * __restrict  ) __attribute__((__nonnull__(1)));
   




 
extern __declspec(__nothrow) int setvbuf(FILE * __restrict  ,
                   char * __restrict  ,
                   int  , size_t  ) __attribute__((__nonnull__(1)));
   















 
#pragma __printf_args
extern __declspec(__nothrow) int fprintf(FILE * __restrict  ,
                    const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   


















 
#pragma __printf_args
extern __declspec(__nothrow) int _fprintf(FILE * __restrict  ,
                     const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   



 
#pragma __printf_args
extern __declspec(__nothrow) int printf(const char * __restrict  , ...) __attribute__((__nonnull__(1)));
   




 
#pragma __printf_args
extern __declspec(__nothrow) int _printf(const char * __restrict  , ...) __attribute__((__nonnull__(1)));
   



 
#pragma __printf_args
extern __declspec(__nothrow) int sprintf(char * __restrict  , const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   






 
#pragma __printf_args
extern __declspec(__nothrow) int _sprintf(char * __restrict  , const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   



 

#pragma __printf_args
extern __declspec(__nothrow) int snprintf(char * __restrict  , size_t  ,
                     const char * __restrict  , ...) __attribute__((__nonnull__(3)));
   















 

#pragma __printf_args
extern __declspec(__nothrow) int _snprintf(char * __restrict  , size_t  ,
                      const char * __restrict  , ...) __attribute__((__nonnull__(3)));
   



 
#pragma __scanf_args
extern __declspec(__nothrow) int fscanf(FILE * __restrict  ,
                    const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   






























 
#pragma __scanf_args
extern __declspec(__nothrow) int _fscanf(FILE * __restrict  ,
                     const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   



 
#pragma __scanf_args
extern __declspec(__nothrow) int scanf(const char * __restrict  , ...) __attribute__((__nonnull__(1)));
   






 
#pragma __scanf_args
extern __declspec(__nothrow) int _scanf(const char * __restrict  , ...) __attribute__((__nonnull__(1)));
   



 
#pragma __scanf_args
extern __declspec(__nothrow) int sscanf(const char * __restrict  ,
                    const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   








 
#pragma __scanf_args
extern __declspec(__nothrow) int _sscanf(const char * __restrict  ,
                     const char * __restrict  , ...) __attribute__((__nonnull__(1,2)));
   



 

 
extern __declspec(__nothrow) int vfscanf(FILE * __restrict  , const char * __restrict  , __va_list) __attribute__((__nonnull__(1,2)));
extern __declspec(__nothrow) int vscanf(const char * __restrict  , __va_list) __attribute__((__nonnull__(1)));
extern __declspec(__nothrow) int vsscanf(const char * __restrict  , const char * __restrict  , __va_list) __attribute__((__nonnull__(1,2)));

extern __declspec(__nothrow) int _vfscanf(FILE * __restrict  , const char * __restrict  , __va_list) __attribute__((__nonnull__(1,2)));
extern __declspec(__nothrow) int _vscanf(const char * __restrict  , __va_list) __attribute__((__nonnull__(1)));
extern __declspec(__nothrow) int _vsscanf(const char * __restrict  , const char * __restrict  , __va_list) __attribute__((__nonnull__(1,2)));

extern __declspec(__nothrow) int vprintf(const char * __restrict  , __va_list  ) __attribute__((__nonnull__(1)));
   





 
extern __declspec(__nothrow) int _vprintf(const char * __restrict  , __va_list  ) __attribute__((__nonnull__(1)));
   



 
extern __declspec(__nothrow) int vfprintf(FILE * __restrict  ,
                    const char * __restrict  , __va_list  ) __attribute__((__nonnull__(1,2)));
   






 
extern __declspec(__nothrow) int vsprintf(char * __restrict  ,
                     const char * __restrict  , __va_list  ) __attribute__((__nonnull__(1,2)));
   






 

extern __declspec(__nothrow) int vsnprintf(char * __restrict  , size_t  ,
                     const char * __restrict  , __va_list  ) __attribute__((__nonnull__(3)));
   







 

extern __declspec(__nothrow) int _vsprintf(char * __restrict  ,
                      const char * __restrict  , __va_list  ) __attribute__((__nonnull__(1,2)));
   



 
extern __declspec(__nothrow) int _vfprintf(FILE * __restrict  ,
                     const char * __restrict  , __va_list  ) __attribute__((__nonnull__(1,2)));
   



 
extern __declspec(__nothrow) int _vsnprintf(char * __restrict  , size_t  ,
                      const char * __restrict  , __va_list  ) __attribute__((__nonnull__(3)));
   



 
extern __declspec(__nothrow) int fgetc(FILE *  ) __attribute__((__nonnull__(1)));
   







 
extern __declspec(__nothrow) char *fgets(char * __restrict  , int  ,
                    FILE * __restrict  ) __attribute__((__nonnull__(1,3)));
   










 
extern __declspec(__nothrow) int fputc(int  , FILE *  ) __attribute__((__nonnull__(2)));
   







 
extern __declspec(__nothrow) int fputs(const char * __restrict  , FILE * __restrict  ) __attribute__((__nonnull__(1,2)));
   




 
extern __declspec(__nothrow) int getc(FILE *  ) __attribute__((__nonnull__(1)));
   







 




    extern __declspec(__nothrow) int (getchar)(void);

   





 
extern __declspec(__nothrow) char *gets(char *  ) __attribute__((__nonnull__(1)));
   









 
extern __declspec(__nothrow) int putc(int  , FILE *  ) __attribute__((__nonnull__(2)));
   





 




    extern __declspec(__nothrow) int (putchar)(int  );

   



 
extern __declspec(__nothrow) int puts(const char *  ) __attribute__((__nonnull__(1)));
   





 
extern __declspec(__nothrow) int ungetc(int  , FILE *  ) __attribute__((__nonnull__(2)));
   






















 

extern __declspec(__nothrow) size_t fread(void * __restrict  ,
                    size_t  , size_t  , FILE * __restrict  ) __attribute__((__nonnull__(1,4)));
   











 

extern __declspec(__nothrow) size_t __fread_bytes_avail(void * __restrict  ,
                    size_t  , FILE * __restrict  ) __attribute__((__nonnull__(1,3)));
   











 

extern __declspec(__nothrow) size_t fwrite(const void * __restrict  ,
                    size_t  , size_t  , FILE * __restrict  ) __attribute__((__nonnull__(1,4)));
   







 

extern __declspec(__nothrow) int fgetpos(FILE * __restrict  , fpos_t * __restrict  ) __attribute__((__nonnull__(1,2)));
   








 
extern __declspec(__nothrow) int fseek(FILE *  , long int  , int  ) __attribute__((__nonnull__(1)));
   














 
extern __declspec(__nothrow) int fsetpos(FILE * __restrict  , const fpos_t * __restrict  ) __attribute__((__nonnull__(1,2)));
   










 
extern __declspec(__nothrow) long int ftell(FILE *  ) __attribute__((__nonnull__(1)));
   











 
extern __declspec(__nothrow) void rewind(FILE *  ) __attribute__((__nonnull__(1)));
   





 

extern __declspec(__nothrow) void clearerr(FILE *  ) __attribute__((__nonnull__(1)));
   




 

extern __declspec(__nothrow) int feof(FILE *  ) __attribute__((__nonnull__(1)));
   


 
extern __declspec(__nothrow) int ferror(FILE *  ) __attribute__((__nonnull__(1)));
   


 
extern __declspec(__nothrow) void perror(const char *  );
   









 

extern __declspec(__nothrow) int _fisatty(FILE *   ) __attribute__((__nonnull__(1)));
    
 

extern __declspec(__nothrow) void __use_no_semihosting_swi(void);
extern __declspec(__nothrow) void __use_no_semihosting(void);
    





 











#line 948 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdio.h"



 

#line 14 "FTP_Client\\FTPC_uif.c"

char ftp_local_file[80];  
const char ftp_user_name[]={"admin"};
const char ftp_password[]={"pcm"};



char ftp_path[20];	
char ftp_file[80];  
const char ftp_new_name[]={""};
const char ftp_dir_name[]={""};
const char ftp_list_name[]={""};


 

 

void *ftpc_fopen (U8 *mode) {
   
   
  return (fopen ((const char*)ftp_local_file, (char *)mode));
}


 

void ftpc_fclose (void *file) {
   
  fclose (file);
}


 

U16 ftpc_fread (void *file, U8 *buf, U16 len) {
   
   
   
  return (fread (buf, 1, len, file));
}


 
U16 tmp_len;
U16 ftpc_fwrite (void *file, U8 *buf, U16 len) {
   
	tmp_len = len;
  return (fwrite (buf, 1, len, file));

}


 

U16 ftpc_cbfunc (U8 code, U8 *buf, U16 buflen) {
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
  U32 i,len = 0;
  
  switch (code) {
    case 0:
       
     	len = str_copy (buf, (U8* )ftp_user_name);
			break;

    case 1:
       
		  len = str_copy (buf, (U8* )ftp_password);
      break;

    case 2:
       
		  len = str_copy (buf, (U8* )ftp_path);
      break;

    case 3:
       
		  len = str_copy (buf, (U8* )ftp_file);
      break;

    case 4:
       
			len = str_copy (buf, (U8* )ftp_new_name);
			break;

    case 5:
       
		  len = str_copy (buf, (U8* )ftp_dir_name);
      break;

    case 6:
       
		  len = str_copy (buf, (U8* )ftp_list_name);
      break;

    case 7:
       
       
      for (i = 0; i < buflen; i++) {
      
			
      }
		  
      break;
  }
  return ((U16)len);
}




 
