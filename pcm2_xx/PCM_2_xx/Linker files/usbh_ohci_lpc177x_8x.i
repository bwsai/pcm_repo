#line 1 "USB\\usbh_ohci_lpc177x_8x.c"










 

#line 1 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"









 




 

 



#line 28 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"







 typedef unsigned int   size_t;


typedef signed char     S8;
typedef unsigned char   U8;
typedef short           S16;
typedef unsigned short  U16;
typedef int             S32;
typedef unsigned int    U32;
typedef long long       S64;
typedef unsigned long long U64;
typedef unsigned char   BIT;
typedef unsigned int    BOOL;

#line 55 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

#line 71 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"



 





 
typedef U32 OS_SEM[2];

 

typedef U32 OS_MBX[];

 
typedef U32 OS_MUT[3];

 
typedef U32 OS_TID;

 
typedef void *OS_ID;

 
typedef U32 OS_RESULT;

 












 




#line 202 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"



 



 
extern void      os_set_env    (void);
extern void      rt_sys_init   (void (*task)(void), U8 priority, void *stk);
extern void      rt_tsk_pass   (void);
extern OS_TID    rt_tsk_self   (void);
extern OS_RESULT rt_tsk_prio   (OS_TID task_id, U8 new_prio);
extern OS_TID    rt_tsk_create (void (*task)(void), U8 priority, void *stk, void *argv);
extern OS_RESULT rt_tsk_delete (OS_TID task_id);

#line 238 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

extern void      _os_sys_init(U32 p, void (*task)(void), U32 prio_stksz,
                                     void *stk)                        __svc_indirect(0);
extern OS_TID    _os_tsk_create (U32 p, void (*task)(void), U32 prio_stksz,
                                        void *stk, void *argv)         __svc_indirect(0);
extern OS_TID    _os_tsk_create_ex (U32 p, void (*task)(void *), U32 prio_stksz,
                                           void *stk, void *argv)      __svc_indirect(0);
extern OS_TID    _os_tsk_self (U32 p)                                  __svc_indirect(0);
extern void      _os_tsk_pass (U32 p)                                  __svc_indirect(0);
extern OS_RESULT _os_tsk_prio (U32 p, OS_TID task_id, U8 new_prio)     __svc_indirect(0);
extern OS_RESULT _os_tsk_delete (U32 p, OS_TID task_id)                __svc_indirect(0);

 
extern OS_RESULT rt_evt_wait (U16 wait_flags,  U16 timeout, BOOL and_wait);
extern void      rt_evt_set  (U16 event_flags, OS_TID task_id);
extern void      rt_evt_clr  (U16 clear_flags, OS_TID task_id);
extern U16       rt_evt_get  (void);







extern OS_RESULT _os_evt_wait(U32 p, U16 wait_flags, U16 timeout,
                                     BOOL and_wait)                    __svc_indirect(0);
extern void      _os_evt_set (U32 p, U16 event_flags, OS_TID task_id)  __svc_indirect(0);
extern void      _os_evt_clr (U32 p, U16 clear_flags, OS_TID task_id)  __svc_indirect(0);
extern U16       _os_evt_get (U32 p)                                   __svc_indirect(0);
extern void      isr_evt_set (U16 event_flags, OS_TID task_id);

 
extern void      rt_sem_init (OS_ID semaphore, U16 token_count);
extern OS_RESULT rt_sem_send (OS_ID semaphore);
extern OS_RESULT rt_sem_wait (OS_ID semaphore, U16 timeout);





extern void      _os_sem_init (U32 p, OS_ID semaphore, 
                                      U16 token_count)                 __svc_indirect(0);
extern OS_RESULT _os_sem_send (U32 p, OS_ID semaphore)                 __svc_indirect(0);
extern OS_RESULT _os_sem_wait (U32 p, OS_ID semaphore, U16 timeout)    __svc_indirect(0);
extern void      isr_sem_send (OS_ID semaphore);

 
extern void      rt_mbx_init  (OS_ID mailbox, U16 mbx_size);
extern OS_RESULT rt_mbx_send  (OS_ID mailbox, void *p_msg,    U16 timeout);
extern OS_RESULT rt_mbx_wait  (OS_ID mailbox, void **message, U16 timeout);
extern OS_RESULT rt_mbx_check (OS_ID mailbox);







extern void      _os_mbx_init (U32 p, OS_ID mailbox, U16 mbx_size)     __svc_indirect(0);
extern OS_RESULT _os_mbx_send (U32 p, OS_ID mailbox, void *message_ptr,
                                      U16 timeout)                     __svc_indirect(0);
extern OS_RESULT _os_mbx_wait (U32 p, OS_ID mailbox, void  **message,
                                      U16 timeout)                     __svc_indirect(0);
extern OS_RESULT _os_mbx_check (U32 p, OS_ID mailbox)                  __svc_indirect(0);
extern void      isr_mbx_send (OS_ID mailbox, void *message_ptr);
extern OS_RESULT isr_mbx_receive (OS_ID mailbox, void **message);

 
extern void      rt_mut_init    (OS_ID mutex);
extern OS_RESULT rt_mut_release (OS_ID mutex);
extern OS_RESULT rt_mut_wait    (OS_ID mutex, U16 timeout);





extern void      _os_mut_init (U32 p, OS_ID mutex)                     __svc_indirect(0);
extern OS_RESULT _os_mut_release (U32 p, OS_ID mutex)                  __svc_indirect(0);
extern OS_RESULT _os_mut_wait (U32 p, OS_ID mutex, U16 timeout)        __svc_indirect(0);

 
extern U32       rt_time_get (void);
extern void      rt_dly_wait (U16 delay_time);
extern void      rt_itv_set  (U16 interval_time);
extern void      rt_itv_wait (void);






extern U32       _os_time_get (U32 p)                                  __svc_indirect(0);
extern void      _os_dly_wait (U32 p, U16 delay_time)                  __svc_indirect(0);
extern void      _os_itv_set (U32 p, U16 interval_time)                __svc_indirect(0);
extern void      _os_itv_wait (U32 p)                                  __svc_indirect(0);

 
extern OS_ID     rt_tmr_create (U16 tcnt, U16 info);
extern OS_ID     rt_tmr_kill   (OS_ID timer);




extern OS_ID     _os_tmr_create (U32 p, U16 tcnt, U16 info)            __svc_indirect(0);
extern OS_ID     _os_tmr_kill (U32 p, OS_ID timer)                     __svc_indirect(0);

 
extern U32       rt_suspend    (void);
extern void      rt_resume     (U32 sleep_time);
extern void      rt_tsk_lock   (void);
extern void      rt_tsk_unlock (void);






extern U32       _os_suspend (U32 p)                                   __svc_indirect(0);
extern void      _os_resume (U32 p, U32 sleep_time)                    __svc_indirect(0);
extern void      _os_tsk_lock (U32 p)                                  __svc_indirect(0);
extern void      _os_tsk_unlock (U32 p)                                __svc_indirect(0);

 
extern int       _init_box (void *box_mem, U32 box_size, U32 blk_size);
extern void     *_alloc_box (void *box_mem);
extern void     *_calloc_box (void *box_mem);
extern int       _free_box (void *box_mem, void *box);








 




 

typedef struct {                         
  U8  hr;                                
  U8  min;                               
  U8  sec;                               
  U8  day;                               
  U8  mon;                               
  U16 year;                              
} RL_TIME;

typedef struct {                         
  S8  name[256];                         
  U32 size;                              
  U16 fileID;                            
  U8  attrib;                            
  RL_TIME time;                          
} FINFO;

extern int finit (const char *drive);
extern int funinit (const char *drive);
extern int fdelete (const char *filename);
extern int frename (const char *oldname, const char *newname);
extern int ffind (const char *pattern, FINFO *info);
extern U64 ffree (const char *drive);
extern int fformat (const char *drive);
extern int fanalyse (const char *drive);
extern int fcheck (const char *drive);
extern int fdefrag (const char *drive);
extern int fattrib (const char *par, const char *path);
extern int fvol    (const char *drive, char *buf);

 




 

 



 
#line 428 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 436 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 449 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 




 




 



 



 



 
#line 484 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 



 





 



 





 
#line 512 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 




 
#line 527 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 537 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 
#line 545 "C:\\Keil_v4\\ARM\\RV31\\INC\\RTL.h"

 



 
typedef struct sockaddr {          
  U16  sa_family;                  
  char sa_data[14];                
} SOCKADDR;

#pragma push
#pragma anon_unions

typedef struct in_addr {           
  union {
    struct {
      U8 s_b1,s_b2,s_b3,s_b4;      
    };
    struct {
      U16 s_w1,s_w2;               
    };
    U32 s_addr;                    
  };
} IN_ADDR;
#pragma pop

typedef struct sockaddr_in {       
  S16 sin_family;                  
  U16 sin_port;                    
  IN_ADDR sin_addr;                
  S8  sin_zero[8];                 
} SOCKADDR_IN;

typedef struct hostent {           
  char *h_name;                    
  char **h_aliases;                
  S16  h_addrtype;                 
  S16  h_length;                   
  char **h_addr_list;              
} HOSTENT;

extern void init_TcpNet (void);
extern BOOL main_TcpNet (void);
extern void timer_tick (void);
extern U8   udp_get_socket (U8 tos, U8 opt, 
                            U16 (*listener)(U8 socket, U8 *remip, U16 port, U8 *buf, U16 len));
extern BOOL udp_release_socket (U8 socket);
extern BOOL udp_open (U8 socket, U16 locport);
extern BOOL udp_close (U8 socket);
extern BOOL udp_mcast_ttl (U8 socket, U8 ttl);
extern U8  *udp_get_buf (U16 size);
extern BOOL udp_send (U8 socket, U8 *remip, U16 remport, U8 *buf, U16 dlen);
extern U8   tcp_get_socket (U8 type, U8 tos, U16 tout,
                            U16 (*listener)(U8 socket, U8 event, U8 *buf, U16 len));
extern BOOL tcp_release_socket (U8 socket);
extern BOOL tcp_listen (U8 socket, U16 locport);
extern BOOL tcp_connect (U8 socket, U8 *remip, U16 remport, U16 locport);
extern U8  *tcp_get_buf (U16 size);
extern U16  tcp_max_dsize (U8 socket);
extern BOOL tcp_check_send (U8 socket);
extern U8   tcp_get_state (U8 socket);
extern BOOL tcp_send (U8 socket, U8 *buf, U16 dlen);
extern BOOL tcp_close (U8 socket);
extern BOOL tcp_abort (U8 socket);
extern void tcp_reset_window (U8 socket);
extern BOOL arp_cache_ip (U8 *ipadr, U8 type);
extern BOOL arp_cache_mac (U8 *hwadr);
extern void ppp_listen (const char *user, const char *passw);
extern void ppp_connect (const char *dialnum, const char *user, const char *passw);
extern void ppp_close (void);
extern BOOL ppp_is_up (void);
extern void slip_listen (void);
extern void slip_connect (const char *dialnum);
extern void slip_close (void);
extern BOOL slip_is_up (void);
extern U8   get_host_by_name (U8 *hostn, void (*cbfunc)(U8 event, U8 *host_ip));
extern BOOL smtp_connect (U8 *ipadr, U16 port, void (*cbfunc)(U8 event));
extern void dhcp_disable (void);
extern BOOL igmp_join (U8 *group_ip);
extern BOOL igmp_leave (U8 *group_ip);
extern BOOL snmp_trap (U8 *manager_ip, U8 gen_trap, U8 spec_trap, U16 *obj_list);
extern BOOL snmp_set_community (const char *community);
extern BOOL icmp_ping (U8 *remip, void (*cbfunc)(U8 event));
extern BOOL ftpc_connect (U8 *ipadr, U16 port, U8 command, void (*cbfunc)(U8 event));
extern BOOL tftpc_put (U8 *ipadr, U16 port,
                       const char *src, const char *dst, void (*cbfunc)(U8 event));
extern BOOL tftpc_get (U8 *ipadr, U16 port, 
                       const char *src, const char *dst, void (*cbfunc)(U8 event));

 
extern int  socket (int family, int type, int protocol);
extern int  bind (int sock, const SOCKADDR *addr, int addrlen);
extern int  listen (int sock, int backlog);
extern int  accept (int sock, SOCKADDR *addr, int *addrlen);
extern int  connect (int sock, SOCKADDR *addr, int addrlen);
extern int  send (int sock, const char *buf, int len, int flags);
extern int  sendto (int sock, const char *buf, int len, int flags, SOCKADDR *to, int tolen);
extern int  recv (int sock, char *buf, int len, int flags);
extern int  recvfrom (int sock, char *buf, int len, int flags, SOCKADDR *from, int *fromlen);
extern int  closesocket (int sock);
extern int  getpeername (int sock, SOCKADDR *name, int *namelen);
extern int  getsockname (int sock, SOCKADDR *name, int *namelen);
extern int  ioctlsocket (int sock, long cmd, unsigned long *argp);
extern HOSTENT *gethostbyname (const char *name, int *err);







 


 
#line 14 "USB\\usbh_ohci_lpc177x_8x.c"
#line 1 "C:\\Keil_v4\\ARM\\RV31\\INC\\rl_usb.h"









 








#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdint.h"
 
 





 










#line 26 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdint.h"







 

     

     
typedef   signed          char int8_t;
typedef   signed short     int int16_t;
typedef   signed           int int32_t;
typedef   signed       __int64 int64_t;

     
typedef unsigned          char uint8_t;
typedef unsigned short     int uint16_t;
typedef unsigned           int uint32_t;
typedef unsigned       __int64 uint64_t;

     

     
     
typedef   signed          char int_least8_t;
typedef   signed short     int int_least16_t;
typedef   signed           int int_least32_t;
typedef   signed       __int64 int_least64_t;

     
typedef unsigned          char uint_least8_t;
typedef unsigned short     int uint_least16_t;
typedef unsigned           int uint_least32_t;
typedef unsigned       __int64 uint_least64_t;

     

     
typedef   signed           int int_fast8_t;
typedef   signed           int int_fast16_t;
typedef   signed           int int_fast32_t;
typedef   signed       __int64 int_fast64_t;

     
typedef unsigned           int uint_fast8_t;
typedef unsigned           int uint_fast16_t;
typedef unsigned           int uint_fast32_t;
typedef unsigned       __int64 uint_fast64_t;

     
typedef   signed           int intptr_t;
typedef unsigned           int uintptr_t;

     
typedef   signed       __int64 intmax_t;
typedef unsigned       __int64 uintmax_t;




     

     





     





     





     

     





     





     





     

     





     





     





     

     


     


     


     

     


     


     


     

     



     



     


     
    
 



#line 197 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdint.h"

     







     










     











#line 261 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\stdint.h"



 



#line 20 "C:\\Keil_v4\\ARM\\RV31\\INC\\rl_usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"









 




 
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_def.h"









 




#pragma anon_unions


 



 





 





 
typedef __packed struct _REQUEST_TYPE {
  U8 Recipient : 5;                      
  U8 Type      : 2;                      
  U8 Dir       : 1;                      
} REQUEST_TYPE;

 
#line 53 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_def.h"

 




 



 
typedef __packed struct _USB_SETUP_PACKET {
  REQUEST_TYPE bmRequestType;            
  U8  bRequest;                          
  __packed union {
    U16        wValue;                   
    __packed struct {
      U8         wValueL;
      U8         wValueH;
    };
  };
  __packed union {
    U16        wIndex;                   
    __packed struct {
      U8         wIndexL;
      U8         wIndexH;
    };
  };
  U16          wLength;                  
} USB_SETUP_PACKET;


 
#line 97 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_def.h"

 
#line 111 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_def.h"

 





 


 




 
#line 142 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_def.h"

 
typedef __packed struct _USB_DEVICE_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U16 bcdUSB;
  U8  bDeviceClass;
  U8  bDeviceSubClass;
  U8  bDeviceProtocol;
  U8  bMaxPacketSize0;
  U16 idVendor;
  U16 idProduct;
  U16 bcdDevice;
  U8  iManufacturer;
  U8  iProduct;
  U8  iSerialNumber;
  U8  bNumConfigurations;
} USB_DEVICE_DESCRIPTOR;

 
typedef __packed struct _USB_DEVICE_QUALIFIER_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U16 bcdUSB;
  U8  bDeviceClass;
  U8  bDeviceSubClass;
  U8  bDeviceProtocol;
  U8  bMaxPacketSize0;
  U8  bNumConfigurations;
  U8  bReserved;
} USB_DEVICE_QUALIFIER_DESCRIPTOR;

 
typedef __packed struct _USB_CONFIGURATION_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U16 wTotalLength;
  U8  bNumInterfaces;
  U8  bConfigurationValue;
  U8  iConfiguration;
  U8  bmAttributes;
  U8  bMaxPower;
} USB_CONFIGURATION_DESCRIPTOR;

 
typedef __packed struct _USB_INTERFACE_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U8  bInterfaceNumber;
  U8  bAlternateSetting;
  U8  bNumEndpoints;
  U8  bInterfaceClass;
  U8  bInterfaceSubClass;
  U8  bInterfaceProtocol;
  U8  iInterface;
} USB_INTERFACE_DESCRIPTOR;

 
typedef __packed struct _USB_ENDPOINT_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U8  bEndpointAddress;
  U8  bmAttributes;
  U16 wMaxPacketSize;
  U8  bInterval;
} USB_ENDPOINT_DESCRIPTOR;

 
typedef __packed struct _USB_STRING_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U16 bString ;
} USB_STRING_DESCRIPTOR;

 
typedef __packed struct _USB_COMMON_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
} USB_COMMON_DESCRIPTOR;

 
typedef __packed struct _USB_INTERFACE_ASSOCIATION_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U8  bFirstInterface;
  U8  bInterfaceCount;
  U8  bFunctionClass;
  U8  bFunctionSubclass;
  U8  bFunctionProtocol;
  U8  iFunction;
} USB_INTERFACE_ASSOCIATION_DESCRIPTOR;


#line 17 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"









 





 





 



 
#line 33 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"


 
#line 45 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 





 




 
#line 69 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"


 
#line 79 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"


 
#line 94 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"


 


 


 
#line 113 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 



 
 
 

 
 
 

 
 


 
 




 
 




 
 






 
 

 




 

 
#line 168 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 







 






 







 

 
#line 201 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 




 
#line 216 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"


 
#line 232 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"


 
#line 245 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 



 
 
 

 
 
 

 
 


 
 





 
 




 
 






 
 

 





 

 




 
#line 307 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 
#line 317 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 
#line 325 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 





 
#line 341 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"

 
#line 363 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_adc.h"


#line 18 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_cdc.h"









 






 
 


 
 


 
 
#line 34 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_cdc.h"

 
 


 
 


 
 
#line 57 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_cdc.h"

 
 



 
 
#line 82 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_cdc.h"

 
 
 
#line 121 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_cdc.h"

 
 



 
 




 
 



 
 
 
#line 149 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_cdc.h"

 
 
#line 173 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_cdc.h"




 

 
 
 
typedef __packed struct _CDC_HEADER_DESCRIPTOR{
  U8  bFunctionLength;                       
  U8  bDescriptorType;                       
  U8  bDescriptorSubtype;                    
  U16 bcdCDC;                                
} CDC_HEADER_DESCRIPTOR;

 
 
 
typedef __packed struct _CDC_CALL_MANAGEMENT_DESCRIPTOR {
  U8  bFunctionLength;                       
  U8  bDescriptorType;                       
  U8  bDescriptorSubtype;                    
  U8  bmCapabilities;                        
  U8  bDataInterface;                        
} CDC_CALL_MANAGEMENT_DESCRIPTOR;

 
 
 
typedef __packed struct _CDC_ABSTRACT_CONTROL_MANAGEMENT_DESCRIPTOR {
  U8  bFunctionLength;                       
  U8  bDescriptorType;                       
  U8  bDescriptorSubtype;                    
  U8  bmCapabilities;                        
} CDC_ABSTRACT_CONTROL_MANAGEMENT_DESCRIPTOR;

 
 
 
typedef __packed struct _CDC_UNION_DESCRIPTOR {
  U8  bFunctionLength;                       
  U8  bDescriptorType;                       
  U8  bDescriptorSubtype;                    
  U8  bMasterInterface;                      
} CDC_UNION_DESCRIPTOR;

 
 
typedef __packed struct _CDC_UNION_1SLAVE_DESCRIPTOR {
  CDC_UNION_DESCRIPTOR sUnion;               
  U8                   bSlaveInterfaces[1];  
} CDC_UNION_1SLAVE_DESCRIPTOR;

 
 
 
typedef __packed struct _CDC_LINE_CODING {
  U32 dwDTERate;                             
  U8  bCharFormat;                           
  U8  bParityType;                           
  U8  bDataBits;                             
} CDC_LINE_CODING;

 
 
 
typedef USB_SETUP_PACKET CDC_NOTIFICATION_HEADER;



#line 19 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"









 





 



 







 





 
typedef __packed struct _HID_DESCRIPTOR {
  U8  bLength;
  U8  bDescriptorType;
  U16 bcdHID;
  U8  bCountryCode;
  U8  bNumDescriptors;
   
  __packed struct _HID_DESCRIPTOR_LIST {
    U8  bDescriptorType;
    U16 wDescriptorLength;
  } DescriptorList[1];
} HID_DESCRIPTOR;


 
#line 56 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"

 





 
#line 80 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"
 


 
#line 124 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"
 

 
 


 

 
 

 
 

 
 

 
 

 

 
extern const unsigned char HID_KEYBOARD_ID_TO_ASCII[256];
extern const unsigned char HID_KEYBOARD_ALT_ID_TO_ASCII[57];

 





 



 







 





 
#line 186 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"

 
#line 196 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"

 

 
#line 275 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"

 
 

 
 

 
#line 290 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"
 

 

 

 


 

 






 
#line 325 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"

 
#line 334 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"

 
#line 363 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_hid.h"

 





#line 20 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_msc.h"









 





 
#line 23 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_msc.h"

 





 




 
#line 43 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_msc.h"


 
typedef __packed struct _MSC_CBW {
  U32 dSignature;
  U32 dTag;
  U32 dDataLength;
  U8  bmFlags;
  U8  bLUN;
  U8  bCBLength;
  U8  CB[16];
} MSC_CBW;

 
typedef __packed struct _MSC_CSW {
  U32 dSignature;
  U32 dTag;
  U32 dDataResidue;
  U8  bStatus;
} MSC_CSW;





 





 
#line 100 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb_msc.h"


#line 21 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"

 
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_core.h"









 





 

 
typedef struct _USBD_EP_DATA {
  U8 *pData;
  U16 Count;
} USBD_EP_DATA;


 

 
extern U16         USBD_DeviceStatus;
extern U8          USBD_DeviceAddress;
extern U8          USBD_Configuration;
extern U32         USBD_EndPointMask;
extern U32         USBD_EndPointHalt;
extern U32         USBD_EndPointStall;
extern U8          USBD_NumInterfaces;
extern U8          USBD_HighSpeed;
extern U8          USBD_ZLP;

extern USBD_EP_DATA     USBD_EP0Data;
extern USB_SETUP_PACKET USBD_SetupPacket;

extern OS_TID      USBD_RTX_DevTask;
extern OS_TID      USBD_RTX_EPTask[];
extern OS_TID      USBD_RTX_CoreTask;


 

extern void        USBD_SetupStage     (void);
extern void        USBD_DataInStage    (void);
extern void        USBD_DataOutStage   (void);
extern void        USBD_StatusInStage  (void);
extern void        USBD_StatusOutStage (void);


 

extern        void usbd_class_init    (void);

extern        void USBD_EndPoint0     (U32 event);

extern __declspec(noreturn) void USBD_RTX_EndPoint0 (void);


#line 24 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_core_adc.h"









 





 

extern BOOL USBD_EndPoint0_Setup_ADC_ReqToIF (void);
extern BOOL USBD_EndPoint0_Setup_ADC_ReqToEP (void);
extern BOOL USBD_EndPoint0_Out_ADC_ReqToIF   (void);
extern BOOL USBD_EndPoint0_Out_ADC_ReqToEP   (void);


#line 25 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_core_cdc.h"










 





 

extern BOOL USBD_EndPoint0_Setup_CDC_ReqToIF (void);
extern BOOL USBD_EndPoint0_Out_CDC_ReqToIF   (void);


#line 26 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_core_hid.h"










 





 

extern BOOL USBD_ReqGetDescriptor_HID        (U8 **pD, U32 *len);
extern BOOL USBD_EndPoint0_Setup_HID_ReqToIF (void);
extern BOOL USBD_EndPoint0_Out_HID_ReqToIF   (void);


#line 27 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_core_msc.h"










 





 

extern void USBD_ReqClrFeature_MSC           (U32 EPNum);
extern BOOL USBD_EndPoint0_Setup_MSC_ReqToIF (void);
extern BOOL USBD_EndPoint0_Out_MSC_ReqToIF   (void);


#line 28 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_core_cls.h"









 





 

extern BOOL USBD_EndPoint0_Setup_CLS_ReqToDEV (void);
extern BOOL USBD_EndPoint0_Setup_CLS_ReqToIF  (void);
extern BOOL USBD_EndPoint0_Setup_CLS_ReqToEP  (void);
extern BOOL USBD_EndPoint0_Out_CLS_ReqToDEV   (void);
extern BOOL USBD_EndPoint0_Out_CLS_ReqToIF    (void);
extern BOOL USBD_EndPoint0_Out_CLS_ReqToEP    (void);


#line 29 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_desc.h"









 




#line 25 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_desc.h"

#line 30 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_event.h"









 





 
#line 25 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_event.h"

 
#line 40 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_event.h"

 





 
extern void (* const USBD_P_Power_Event    )(BOOL power);
extern void (* const USBD_P_Reset_Event    )(void);
extern void (* const USBD_P_Suspend_Event  )(void);
extern void (* const USBD_P_Resume_Event   )(void);
extern void (* const USBD_P_WakeUp_Event   )(void);
extern void (* const USBD_P_SOF_Event      )(void);
extern void (* const USBD_P_Error_Event    )(U32 error);

 
extern void (* const USBD_P_EP[16])         (U32 event);

 
extern void (* const USBD_P_Configure_Event)(void);
extern void (* const USBD_P_Interface_Event)(void);
extern void (* const USBD_P_Feature_Event  )(void);

 
extern void USBD_RTX_TaskInit               (void);

#line 31 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_adc.h"









 





 

 
extern        U16 USBD_ADC_VolCur;
extern const  U16 USBD_ADC_VolMin;
extern const  U16 USBD_ADC_VolMax;
extern const  U16 USBD_ADC_VolRes;
extern        U8  USBD_ADC_Mute;
extern        U32 USBD_ADC_Volume;
extern        U16 USBD_ADC_DataOut;
extern        U16 USBD_ADC_DataIn;
extern        U8  USBD_ADC_DataRun;

 
extern const  U32 usbd_adc_cfg_datafreq;
extern const  U32 usbd_adc_cfg_p_s;
extern const  U32 usbd_adc_cfg_p_c;
extern const  U32 usbd_adc_cfg_b_s;
extern        S16 USBD_ADC_DataBuf      [];


 

extern void USBD_ADC_SOF_Event     (void);


#line 32 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_cdc_acm.h"










 





 

extern           void USBD_CDC_ACM_Reset_Event          (void);

extern           void USBD_CDC_ACM_SOF_Event            (void);

extern           void USBD_CDC_ACM_EP_INTIN_Event       (U32 event);
extern           void USBD_CDC_ACM_EP_BULKIN_Event      (U32 event);
extern           void USBD_CDC_ACM_EP_BULKOUT_Event     (U32 event);
extern           void USBD_CDC_ACM_EP_BULK_Event        (U32 event);

extern    __declspec(noreturn) void USBD_RTX_CDC_ACM_EP_INTIN_Event   (void);
extern    __declspec(noreturn) void USBD_RTX_CDC_ACM_EP_BULKIN_Event  (void);
extern    __declspec(noreturn) void USBD_RTX_CDC_ACM_EP_BULKOUT_Event (void);
extern    __declspec(noreturn) void USBD_RTX_CDC_ACM_EP_BULK_Event    (void);


#line 33 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_hid.h"









 





 

 
enum {
  USBD_HID_REQ_EP_CTRL = 0,              
  USBD_HID_REQ_EP_INT,                   
  USBD_HID_REQ_PERIOD_UPDATE             
};


 

extern        void USBD_HID_Configure_Event    (void);
extern        void USBD_HID_SOF_Event          (void);

extern        void USBD_HID_EP_INTIN_Event     (U32 event);
extern        void USBD_HID_EP_INTOUT_Event    (U32 event);
extern        void USBD_HID_EP_INT_Event       (U32 event);

extern __declspec(noreturn) void USBD_RTX_HID_EP_INTIN_Event (void);
extern __declspec(noreturn) void USBD_RTX_HID_EP_INTOUT_Event(void); 
extern __declspec(noreturn) void USBD_RTX_HID_EP_INT_Event   (void);


#line 34 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_msc.h"









 





 

 
extern       BOOL USBD_MSC_MediaReady;
extern       BOOL USBD_MSC_ReadOnly;
extern       U32  USBD_MSC_MemorySize;
extern       U32  USBD_MSC_BlockSize;
extern       U32  USBD_MSC_BlockGroup;
extern       U32  USBD_MSC_BlockCount;
extern       U8  *USBD_MSC_BlockBuf;


 

extern        void USBD_MSC_EP_BULKIN_Event      (U32 event);
extern        void USBD_MSC_EP_BULKOUT_Event     (U32 event);
extern        void USBD_MSC_EP_BULK_Event        (U32 event);

extern __declspec(noreturn) void USBD_RTX_MSC_EP_BULKIN_Event  (void);
extern __declspec(noreturn) void USBD_RTX_MSC_EP_BULKOUT_Event (void);
extern __declspec(noreturn) void USBD_RTX_MSC_EP_BULK_Event    (void);


#line 35 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_cls.h"









 





 

extern void USBD_CLS_SOF_Event     (void);


#line 36 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbd_hw.h"









 





 
extern void USBD_Init        (void);
extern void USBD_Connect     (BOOL con);
extern void USBD_Reset       (void);
extern void USBD_Suspend     (void);
extern void USBD_Resume      (void);
extern void USBD_WakeUp      (void);
extern void USBD_WakeUpCfg   (BOOL cfg);
extern void USBD_SetAddress  (U32  adr, U32 setup);
extern void USBD_Configure   (BOOL cfg);
extern void USBD_ConfigEP    (USB_ENDPOINT_DESCRIPTOR *pEPD);
extern void USBD_DirCtrlEP   (U32  dir);
extern void USBD_EnableEP    (U32  EPNum);
extern void USBD_DisableEP   (U32  EPNum);
extern void USBD_ResetEP     (U32  EPNum);
extern void USBD_SetStallEP  (U32  EPNum);
extern void USBD_ClrStallEP  (U32  EPNum);
extern void USBD_ClearEPBuf  (U32  EPNum);
extern U32  USBD_ReadEP      (U32  EPNum, U8 *pData);
extern U32  USBD_WriteEP     (U32  EPNum, U8 *pData, U32 cnt);
extern U32  USBD_GetFrame    (void);
extern U32  USBD_GetError    (void);

#line 37 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"

 
#line 1 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"









 




#pragma anon_unions


 

 
typedef enum { 
  ERROR_SUCCESS = 0,                     
                                         
  ERROR_USBH_HCI,                        
  ERROR_USBH_HCD,                        
  ERROR_USBH_DCI,                        
  ERROR_USBH_DCD,                        

  ERROR_USBH_MEM_INIT,                   
  ERROR_USBH_MEM_ALLOC,                  
  ERROR_USBH_MEM_DEALLOC,                

  ERROR_USBH_PINS_CONFIG,                
  ERROR_USBH_INIT,                       
  ERROR_USBH_PORT_POWER,                 
  ERROR_USBH_PORT_RESET,                 

  ERROR_USBH_EP,                         
  ERROR_USBH_EP_ADD,                     
  ERROR_USBH_EP_CONFIG,                  
  ERROR_USBH_EP_REMOVE,                  

  ERROR_USBH_URB,                        
  ERROR_USBH_URB_SUBMIT,                 
  ERROR_USBH_URB_CANCEL,                 
  ERROR_USBH_URB_TRANSFER,               
  ERROR_USBH_URB_TOUT,                   

  ERROR_USBH_ENUMERATE,                  
  ERROR_USBH_UNINIT_DEVS,                

  ERROR_USBH_MSC,                        
  ERROR_USBH_MSC_SIG,                    
  ERROR_USBH_MSC_TAG,                    
  ERROR_USBH_MSC_RESID,                  

  ERROR_USBH_HID,                        

  ERROR_USBH_CLS,                        

  ERROR_USBH_CLS_UNKNOWN,                
} USBH_ERROR;

 
enum { 
  USBH_LS  = 0,                          
  USBH_FS,                               
  USBH_HS                                
};

 
enum {
  USBH_PACKET_OUT      =  1,             
  USBH_PACKET_IN       =  9,             
  USBH_PACKET_SOF      =  5,             
  USBH_PACKET_SETUP    = 13,             
  USBH_PACKET_DATA0    =  3,             
  USBH_PACKET_DATA1    = 11,             
  USBH_PACKET_DATA2    =  7,             
  USBH_PACKET_MDATA    = 15,             
  USBH_PACKET_ACK      =  2,             
  USBH_PACKET_NAK      = 10,             
  USBH_PACKET_STALL    = 14,             
  USBH_PACKET_NYET     =  6,             
  USBH_PACKET_PRE      = 12,             
  USBH_PACKET_ERR      = 12,             
  USBH_PACKET_SPLIT    =  8,             
  USBH_PACKET_PING     =  4,             
  USBH_PACKET_RESERVED =  0              
};

 
enum {
  USBH_TR_NO_ERROR     =  0,             
  USBH_TR_ERROR_BSTUFF =  1,             
  USBH_TR_ERROR_PID    =  2,             
  USBH_TR_ERROR_CRC    =  3,             
  USBH_TR_ERROR_BUSTT  =  4,             
  USBH_TR_ERROR_FEOP   =  4,             
  USBH_TR_ERROR_BABBLE =  4,             
  USBH_TR_ERROR_EOF    =  5,             
  USBH_TR_ERROR_DOVER  =  6,             
  USBH_TR_ERROR_DUNDER =  7,             
  USBH_TR_ERROR_OTHER  =  8,             
};

 

#line 116 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 125 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 133 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 141 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 148 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 156 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 164 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 171 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"







#line 184 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 191 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 198 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

#line 222 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usbh.h"

 

typedef volatile struct {                
  U8          *ptrDataBuffer;            
  U8          *ptrCurrentDataBuffer;     
  U32          DataLength;               
  U32          DataTransferred;          
  union {
    U32        Parameters;               
    struct {
      U32      PacketType        :  4;   
      U32      ToggleBit         :  1;   
      U32      ToggleForce       :  1;  
 
    };
  };
  union {
    U32        Status;                   
    struct {
      U32      Submitted         :  1;   
      U32      InProgress        :  1;   
      U32      Cancelled         :  1;   
      U32      Completed         :  1;   
      U32      Timeout           :  1;   
      U32      ResponsePacketType:  4;   
      U32      Reserved          :  7;   
      U32      Error             :  8;   
    };
  };
  U32          NAKRetries;               
  U32          TimeoutCount;             
  void       (*CompletedCallback)(void); 
} USBH_URB;

typedef __packed struct {                
  U32          Handle;                   
  __packed union {
    U32 Para;                            
    __packed struct {
      U8       Address           : 8;    
      U8       Speed             : 8;    
    };
  };
  USB_ENDPOINT_DESCRIPTOR Descriptor;    
} USBH_EP;

typedef struct {                         
  union {
    struct {
      U32      MultiPckt;                
    };
  };
  U32          MaxDataSz;                
  U32          CtrlNAKs;                 
  U32          BulkNAKs;                 
} USBH_HCI_CAP;

typedef struct {                         
  USBH_HCI_CAP Cap;                      
  U32          PortCon;                  
  U8           LastDevAdr;               
  USBH_EP      EP0;                      
  U32          LastError;                
} USBH_HCI;

typedef struct {                         
  U8           DoPing;                   
  USBH_EP      BulkInEP;                 
  USBH_EP      BulkOutEP;                
  U8           MaxLUN;                   
  U32          Tag;                      
} USBH_MSC;

typedef struct {                         
  USBH_URB     IntUrb;                   
  USBH_EP      IntInEP;                  
  USBH_EP      IntOutEP;                 
  U8           SubClass;                 
  U8           ReportDescTyp;            
  U16          ReportDescLen;            
  U8           ReportInDataBuf[8];       
  struct {
    U8         ReportInReceived  : 1;    
  };
} USBH_HID;

typedef struct {                         
  U8           Protocol;                 
  U8           Port;                     
  U8           Address;                  
  U8           Speed;                     
  struct {
    U8         Config            : 1;    
    U8         Init              : 1;    
  };
  U32          LastError;                
  union {
    USBH_MSC  *ptrMSC;                   
    USBH_HID  *ptrHID;                   
    void      *ptrCLS;                   
  };
} USBH_DCI;

typedef struct {                         
  void       (*get_capabilities) (USBH_HCI_CAP *cap);  
  void       (*delay_ms        ) (U32  ms);    
  BOOL       (*pins_config     ) (BOOL on);    
  BOOL       (*init            ) (BOOL on);    
  BOOL       (*port_power      ) (BOOL on);    
  BOOL       (*port_reset      ) (U8   port);  
  U32        (*get_connect     ) (void);       
  U32        (*get_speed       ) (void);       
  U32        (*ep_add          ) (            U8 dev_adr, U8 ep_spd, USB_ENDPOINT_DESCRIPTOR *ptr_epd);
  BOOL       (*ep_config       ) (U32  hndl,  U8 dev_adr, U8 ep_spd, USB_ENDPOINT_DESCRIPTOR *ptr_epd);
  BOOL       (*ep_remove       ) (U32  hndl);
  BOOL       (*urb_submit      ) (U32  hndl,  USBH_URB *ptr_urb);
  BOOL       (*urb_cancel      ) (U32  hndl,  USBH_URB *ptr_urb);
} USBH_HCD;

typedef struct {                         
  U32          Ports;                    
  U16          MaxED;                    
  U16          MaxTD;                    
  U16          MaxITD;                   
  U32         *PtrHCCA;                  
  U32         *PtrED;                    
  U32         *PtrTD;                    
  U32         *PtrITD;                   
  U32         *PtrTDURB;                 
  void       (*get_capabilities) (USBH_HCI_CAP *cap);  
  void       (*delay_ms        ) (U32  ms);    
  void       (*reg_wr          ) (U32  reg_ofs, U32 val);    
  U32        (*reg_rd          ) (U32  reg_ofs);             
  BOOL       (*pins_config     ) (BOOL on);    
  BOOL       (*init            ) (BOOL on);    
  BOOL       (*port_power      ) (U32  on);    
  BOOL       (*irq_en          ) (BOOL on);    
} USBH_HWD_OHCI;

typedef struct {                         
  U32          Ports;                    
  U16          Max_qH;                   
  U16          Max_qTD;                  
  U16          Max_iTD;                  
  U16          Max_siTD;                 
  U16          Max_FSTN;                 
  U32         *Ptr_PFL;                  
  U32         *Ptr_qH;                   
  U32         *Ptr_qTD;                  
  U32         *Ptr_iTD;                  
  U32         *Ptr_siTD;                 
  U32         *Ptr_FSTN;                 
  U32         *Ptr_qTDURB;               
  void       (*get_capabilities) (USBH_HCI_CAP *cap);  
  void       (*delay_ms        ) (U32  ms);    
  void       (*reg_wr          ) (U32  reg_ofs, U32 val);    
  U32        (*reg_rd          ) (U32  reg_ofs);             
  BOOL       (*pins_config     ) (BOOL on);    
  BOOL       (*init            ) (BOOL on);    
  BOOL       (*port_power      ) (U32  on);    
  BOOL       (*irq_en          ) (BOOL on);    
} USBH_HWD_EHCI;

typedef struct {                         
  U8           ClassID;
  U8         (*config          ) (U8 ctrl, U8 port, U8 spd, U8 adr, USB_CONFIGURATION_DESCRIPTOR *ptr_cfg_desc);
  BOOL       (*unconfig        ) (U8 ctrl, U8 dev_idx);
  BOOL       (*init            ) (U8 ctrl, U8 dev_idx);
  BOOL       (*uninit          ) (U8 ctrl, U8 dev_idx);
  U32        (*get_last_error  ) (U8 ctrl, U8 dev_idx);
} USBH_DCD;


 

extern BOOL USBH_Send_Setup      (U8 ctrl, U8 *ptr_data);
extern BOOL USBH_Send_Data       (U8 ctrl, U8 *ptr_data, U16 data_len);
extern BOOL USBH_Rece_Data       (U8 ctrl, U8 *ptr_data, U16 data_len);

extern BOOL USBH_GetStatus       (U8 ctrl, U8  rcpnt, U8 idx, U8 *stat_dat);
extern BOOL USBH_ClearFeature    (U8 ctrl, U8  rcpnt, U8 idx, U8  feat_sel);
extern BOOL USBH_SetFeature      (U8 ctrl, U8  rcpnt, U8 idx, U8  feat_sel);
extern BOOL USBH_SetAddress      (U8 ctrl, U8  dev_adr);
extern BOOL USBH_GetDescriptor   (U8 ctrl, U8  rcpnt, U8 desc_typ, U8 desc_idx, U8 lang_id, U8 *desc_dat, U16 desc_len);
extern BOOL USBH_SetDescriptor   (U8 ctrl, U8  rcpnt, U8 desc_typ, U8 desc_idx, U8 lang_id, U8 *desc_dat, U16 desc_len);
extern BOOL USBH_GetConfiguration(U8 ctrl, U8 *cfg_dat);
extern BOOL USBH_SetConfiguration(U8 ctrl, U8  cfg_val);
extern BOOL USBH_GetInterface    (U8 ctrl, U8  idx, U8 *alt_dat);
extern BOOL USBH_SetInterface    (U8 ctrl, U8  idx, U8  alt_set);
extern BOOL USBH_SyncFrame       (U8 ctrl, U8  idx, U8 *frm_num);


#pragma no_anon_unions

#line 40 "C:\\Keil_v4\\ARM\\ARMCC\\bin\\..\\include\\..\\..\\RL\\USB\\INC\\usb.h"

#line 21 "C:\\Keil_v4\\ARM\\RV31\\INC\\rl_usb.h"

 

 
extern BOOL  usbh_mem_init              (U8 ctrl, U32 *ptr_pool, U32 pool_sz);
extern BOOL  usbh_mem_alloc             (U8 ctrl, U8 **ptr, U32 sz);
extern BOOL  usbh_mem_free              (U8 ctrl, U8  *ptr);
extern BOOL  usbh_transfer              (U8 ctrl, USBH_EP *ptr_ep, USBH_URB *ptr_urb, U16 tout);
extern BOOL  usbh_init                  (U8 ctrl);
extern BOOL  usbh_init_all              (void);
extern BOOL  usbh_uninit                (U8 ctrl);
extern BOOL  usbh_uninit_all            (void);
extern BOOL  usbh_engine                (U8 ctrl);
extern BOOL  usbh_engine_all            (void);
extern U32   usbh_get_last_error        (U8 ctrl);
extern U8   *usbh_get_error_string      (U32 err);

 
extern BOOL  usbh_msc_status            (U8 ctrl, U8 dev_idx);
extern BOOL  usbh_msc_read              (U8 ctrl, U8 dev_idx, U32  blk_adr, U8 *ptr_data, U16 blk_num);
extern BOOL  usbh_msc_write             (U8 ctrl, U8 dev_idx, U32  blk_adr, U8 *ptr_data, U16 blk_num);
extern BOOL  usbh_msc_read_config       (U8 ctrl, U8 dev_idx, U32 *tot_blk_num, U32 *blk_sz);
extern U32   usbh_msc_get_last_error    (U8 ctrl, U8 dev_idx);

 
extern BOOL  usbh_hid_status            (U8 ctrl, U8 dev_idx);
extern int   usbh_hid_data_in           (U8 ctrl, U8 dev_idx, U8  *ptr_data);
extern int   usbh_hid_data_out          (U8 ctrl, U8 dev_idx, U8  *ptr_data, U16 data_len);
extern U32   usbh_hid_get_last_error    (U8 ctrl, U8 dev_idx);
 
extern void  usbh_hid_parse_report_desc (U8 ctrl, U8 dev_idx, U8  *ptrHIDReportDesc);
extern void  usbh_hid_data_in_callback  (U8 ctrl, U8 dev_idx, U8  *ptr_data, U16 data_len);
extern int   usbh_hid_kbd_getkey        (U8 ctrl, U8 dev_idx);  
extern BOOL  usbh_hid_mouse_getdata     (U8 ctrl, U8 dev_idx, U8  *btn, S8 *x, S8 *y, S8 *wheel);

 
extern void  usbd_init                  (void);
extern void  usbd_connect               (BOOL con);
extern void  usbd_reset_core            (void);
extern BOOL  usbd_configured            (void);

 
extern void  usbd_hid_init              (void);
extern BOOL  usbd_hid_get_report_trigger(U8 rid,   U8 *buf, int len);
extern int   usbd_hid_get_report        (U8 rtype, U8 rid, U8 *buf, U8  req);
extern void  usbd_hid_set_report        (U8 rtype, U8 rid, U8 *buf, int len, U8 req);
extern U8    usbd_hid_get_protocol      (void);
extern void  usbd_hid_set_protocol      (U8 protocol);

 
extern void  usbd_msc_init              (void);
extern void  usbd_msc_read_sect         (U32 block, U8 *buf, U32 num_of_blocks);
extern void  usbd_msc_write_sect        (U32 block, U8 *buf, U32 num_of_blocks);
extern void  usbd_msc_start_stop        (BOOL start);

 
extern void  usbd_adc_init              (void);

 
extern int32_t  USBD_CDC_ACM_Initialize                (void);
extern int32_t  USBD_CDC_ACM_Uninitialize              (void);
extern int32_t  USBD_CDC_ACM_Reset                     (void);
 
extern int32_t  USBD_CDC_ACM_PortInitialize            (void);
extern int32_t  USBD_CDC_ACM_PortUninitialize          (void);
extern int32_t  USBD_CDC_ACM_PortReset                 (void);
extern int32_t  USBD_CDC_ACM_PortSetLineCoding         (CDC_LINE_CODING *line_coding);
extern int32_t  USBD_CDC_ACM_PortGetLineCoding         (CDC_LINE_CODING *line_coding);
extern int32_t  USBD_CDC_ACM_PortSetControlLineState   (uint16_t ctrl_bmp);
extern int32_t  USBD_CDC_ACM_DataSend                  (const uint8_t *buf, int32_t len);
extern int32_t  USBD_CDC_ACM_PutChar                   (const uint8_t  ch);
extern int32_t  USBD_CDC_ACM_DataRead                  (      uint8_t *buf, int32_t len);
extern int32_t  USBD_CDC_ACM_GetChar                   (void);
extern int32_t  USBD_CDC_ACM_DataAvailable             (void);
extern int32_t  USBD_CDC_ACM_Notify                    (uint16_t stat);
 
extern int32_t  USBD_CDC_ACM_SendEncapsulatedCommand   (void);
extern int32_t  USBD_CDC_ACM_GetEncapsulatedResponse   (void);
extern int32_t  USBD_CDC_ACM_SetCommFeature            (uint16_t feat);
extern int32_t  USBD_CDC_ACM_GetCommFeature            (uint16_t feat);
extern int32_t  USBD_CDC_ACM_ClearCommFeature          (uint16_t feat);
extern int32_t  USBD_CDC_ACM_SetLineCoding             (void);
extern int32_t  USBD_CDC_ACM_GetLineCoding             (void);
extern int32_t  USBD_CDC_ACM_SetControlLineState       (uint16_t ctrl_bmp);
extern int32_t  USBD_CDC_ACM_SendBreak                 (uint16_t dur);

 
extern void  usbd_cls_init              (void);
extern void  usbd_cls_sof               (void);
extern BOOL  usbd_cls_dev_req           (BOOL setup);
extern BOOL  usbd_cls_if_req            (BOOL setup);
extern BOOL  usbd_cls_ep_req            (BOOL setup);





#line 15 "USB\\usbh_ohci_lpc177x_8x.c"
#line 1 ".\\Common\\LPC177x_8x.h"


 





















 








 

typedef enum IRQn
{
 
  NonMaskableInt_IRQn           = -14,       
  MemoryManagement_IRQn         = -12,       
  BusFault_IRQn                 = -11,       
  UsageFault_IRQn               = -10,       
  SVCall_IRQn                   = -5,        
  DebugMonitor_IRQn             = -4,        
  PendSV_IRQn                   = -2,        
  SysTick_IRQn                  = -1,        

 
  WDT_IRQn                      = 0,         
  TIMER0_IRQn                   = 1,         
  TIMER1_IRQn                   = 2,         
  TIMER2_IRQn                   = 3,         
  TIMER3_IRQn                   = 4,         
  UART0_IRQn                    = 5,         
  UART1_IRQn                    = 6,         
  UART2_IRQn                    = 7,         
  UART3_IRQn                    = 8,         
  PWM1_IRQn                     = 9,         
  I2C0_IRQn                     = 10,        
  I2C1_IRQn                     = 11,        
  I2C2_IRQn                     = 12,        
  Reserved0_IRQn                = 13,        
  SSP0_IRQn                     = 14,        
  SSP1_IRQn                     = 15,        
  PLL0_IRQn                     = 16,        
  RTC_IRQn                      = 17,        
  EINT0_IRQn                    = 18,        
  EINT1_IRQn                    = 19,        
  EINT2_IRQn                    = 20,        
  EINT3_IRQn                    = 21,        
  ADC_IRQn                      = 22,        
  BOD_IRQn                      = 23,        
  USB_IRQn                      = 24,        
  CAN_IRQn                      = 25,        
  DMA_IRQn                      = 26,        
  I2S_IRQn                      = 27,        
  ENET_IRQn                     = 28,        
  MCI_IRQn                      = 29,        
  MCPWM_IRQn                    = 30,        
  QEI_IRQn                      = 31,        
  PLL1_IRQn                     = 32,        
  USBActivity_IRQn              = 33,        
  CANActivity_IRQn              = 34,        
  UART4_IRQn                    = 35,        
  SSP2_IRQn                     = 36,        
  LCD_IRQn                      = 37,        
  GPIO_IRQn                     = 38,        
  PWM0_IRQn                     = 39,        
  EEPROM_IRQn                   = 40,        
} IRQn_Type;






 

 





#line 1 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"
 




















 






















 




 


 

 













#line 89 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"


 







#line 119 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"

#line 121 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"
#line 1 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cmInstr.h"
 




















 





 



 


 









 







 







 






 








 







 







 









 









 

__attribute__((section(".rev16_text"))) static __inline __asm uint32_t __REV16(uint32_t value)
{
  rev16 r0, r0
  bx lr
}








 

__attribute__((section(".revsh_text"))) static __inline __asm int32_t __REVSH(int32_t value)
{
  revsh r0, r0
  bx lr
}










 











 









 









 









 











 











 











 







 










 










 









 






#line 618 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cmInstr.h"

   

#line 122 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"
#line 1 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cmFunc.h"
 




















 





 



 


 





 
 






 
static __inline uint32_t __get_CONTROL(void)
{
  register uint32_t __regControl         __asm("control");
  return(__regControl);
}







 
static __inline void __set_CONTROL(uint32_t control)
{
  register uint32_t __regControl         __asm("control");
  __regControl = control;
}







 
static __inline uint32_t __get_IPSR(void)
{
  register uint32_t __regIPSR          __asm("ipsr");
  return(__regIPSR);
}







 
static __inline uint32_t __get_APSR(void)
{
  register uint32_t __regAPSR          __asm("apsr");
  return(__regAPSR);
}







 
static __inline uint32_t __get_xPSR(void)
{
  register uint32_t __regXPSR          __asm("xpsr");
  return(__regXPSR);
}







 
static __inline uint32_t __get_PSP(void)
{
  register uint32_t __regProcessStackPointer  __asm("psp");
  return(__regProcessStackPointer);
}







 
static __inline void __set_PSP(uint32_t topOfProcStack)
{
  register uint32_t __regProcessStackPointer  __asm("psp");
  __regProcessStackPointer = topOfProcStack;
}







 
static __inline uint32_t __get_MSP(void)
{
  register uint32_t __regMainStackPointer     __asm("msp");
  return(__regMainStackPointer);
}







 
static __inline void __set_MSP(uint32_t topOfMainStack)
{
  register uint32_t __regMainStackPointer     __asm("msp");
  __regMainStackPointer = topOfMainStack;
}







 
static __inline uint32_t __get_PRIMASK(void)
{
  register uint32_t __regPriMask         __asm("primask");
  return(__regPriMask);
}







 
static __inline void __set_PRIMASK(uint32_t priMask)
{
  register uint32_t __regPriMask         __asm("primask");
  __regPriMask = (priMask);
}








 







 








 
static __inline uint32_t  __get_BASEPRI(void)
{
  register uint32_t __regBasePri         __asm("basepri");
  return(__regBasePri);
}







 
static __inline void __set_BASEPRI(uint32_t basePri)
{
  register uint32_t __regBasePri         __asm("basepri");
  __regBasePri = (basePri & 0xff);
}







 
static __inline uint32_t __get_FAULTMASK(void)
{
  register uint32_t __regFaultMask       __asm("faultmask");
  return(__regFaultMask);
}







 
static __inline void __set_FAULTMASK(uint32_t faultMask)
{
  register uint32_t __regFaultMask       __asm("faultmask");
  __regFaultMask = (faultMask & (uint32_t)1);
}




#line 293 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cmFunc.h"


#line 612 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cmFunc.h"

 


#line 123 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"








 
#line 153 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"

 






 
#line 169 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"

 












 


 





 


 
typedef union
{
  struct
  {

    uint32_t _reserved0:27;               





    uint32_t Q:1;                         
    uint32_t V:1;                         
    uint32_t C:1;                         
    uint32_t Z:1;                         
    uint32_t N:1;                         
  } b;                                    
  uint32_t w;                             
} APSR_Type;



 
typedef union
{
  struct
  {
    uint32_t ISR:9;                       
    uint32_t _reserved0:23;               
  } b;                                    
  uint32_t w;                             
} IPSR_Type;



 
typedef union
{
  struct
  {
    uint32_t ISR:9;                       

    uint32_t _reserved0:15;               





    uint32_t T:1;                         
    uint32_t IT:2;                        
    uint32_t Q:1;                         
    uint32_t V:1;                         
    uint32_t C:1;                         
    uint32_t Z:1;                         
    uint32_t N:1;                         
  } b;                                    
  uint32_t w;                             
} xPSR_Type;



 
typedef union
{
  struct
  {
    uint32_t nPRIV:1;                     
    uint32_t SPSEL:1;                     
    uint32_t FPCA:1;                      
    uint32_t _reserved0:29;               
  } b;                                    
  uint32_t w;                             
} CONTROL_Type;

 






 


 
typedef struct
{
  volatile uint32_t ISER[8];                  
       uint32_t RESERVED0[24];
  volatile uint32_t ICER[8];                  
       uint32_t RSERVED1[24];
  volatile uint32_t ISPR[8];                  
       uint32_t RESERVED2[24];
  volatile uint32_t ICPR[8];                  
       uint32_t RESERVED3[24];
  volatile uint32_t IABR[8];                  
       uint32_t RESERVED4[56];
  volatile uint8_t  IP[240];                  
       uint32_t RESERVED5[644];
  volatile  uint32_t STIR;                     
}  NVIC_Type;

 



 






 


 
typedef struct
{
  volatile const  uint32_t CPUID;                    
  volatile uint32_t ICSR;                     
  volatile uint32_t VTOR;                     
  volatile uint32_t AIRCR;                    
  volatile uint32_t SCR;                      
  volatile uint32_t CCR;                      
  volatile uint8_t  SHP[12];                  
  volatile uint32_t SHCSR;                    
  volatile uint32_t CFSR;                     
  volatile uint32_t HFSR;                     
  volatile uint32_t DFSR;                     
  volatile uint32_t MMFAR;                    
  volatile uint32_t BFAR;                     
  volatile uint32_t AFSR;                     
  volatile const  uint32_t PFR[2];                   
  volatile const  uint32_t DFR;                      
  volatile const  uint32_t ADR;                      
  volatile const  uint32_t MMFR[4];                  
  volatile const  uint32_t ISAR[5];                  
       uint32_t RESERVED0[5];
  volatile uint32_t CPACR;                    
} SCB_Type;

 















 






























 




#line 396 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"

 





















 









 


















 










































 









 









 















 






 


 
typedef struct
{
       uint32_t RESERVED0[1];
  volatile const  uint32_t ICTR;                     



       uint32_t RESERVED1[1];

} SCnSCB_Type;

 



 










 






 


 
typedef struct
{
  volatile uint32_t CTRL;                     
  volatile uint32_t LOAD;                     
  volatile uint32_t VAL;                      
  volatile const  uint32_t CALIB;                    
} SysTick_Type;

 












 



 



 









 






 


 
typedef struct
{
  volatile  union
  {
    volatile  uint8_t    u8;                   
    volatile  uint16_t   u16;                  
    volatile  uint32_t   u32;                  
  }  PORT [32];                           
       uint32_t RESERVED0[864];
  volatile uint32_t TER;                      
       uint32_t RESERVED1[15];
  volatile uint32_t TPR;                      
       uint32_t RESERVED2[15];
  volatile uint32_t TCR;                      
       uint32_t RESERVED3[29];
  volatile  uint32_t IWR;                      
  volatile const  uint32_t IRR;                      
  volatile uint32_t IMCR;                     
       uint32_t RESERVED4[43];
  volatile  uint32_t LAR;                      
  volatile const  uint32_t LSR;                      
       uint32_t RESERVED5[6];
  volatile const  uint32_t PID4;                     
  volatile const  uint32_t PID5;                     
  volatile const  uint32_t PID6;                     
  volatile const  uint32_t PID7;                     
  volatile const  uint32_t PID0;                     
  volatile const  uint32_t PID1;                     
  volatile const  uint32_t PID2;                     
  volatile const  uint32_t PID3;                     
  volatile const  uint32_t CID0;                     
  volatile const  uint32_t CID1;                     
  volatile const  uint32_t CID2;                     
  volatile const  uint32_t CID3;                     
} ITM_Type;

 



 



























 



 



 



 









   






 


 
typedef struct
{
  volatile uint32_t CTRL;                     
  volatile uint32_t CYCCNT;                   
  volatile uint32_t CPICNT;                   
  volatile uint32_t EXCCNT;                   
  volatile uint32_t SLEEPCNT;                 
  volatile uint32_t LSUCNT;                   
  volatile uint32_t FOLDCNT;                  
  volatile const  uint32_t PCSR;                     
  volatile uint32_t COMP0;                    
  volatile uint32_t MASK0;                    
  volatile uint32_t FUNCTION0;                
       uint32_t RESERVED0[1];
  volatile uint32_t COMP1;                    
  volatile uint32_t MASK1;                    
  volatile uint32_t FUNCTION1;                
       uint32_t RESERVED1[1];
  volatile uint32_t COMP2;                    
  volatile uint32_t MASK2;                    
  volatile uint32_t FUNCTION2;                
       uint32_t RESERVED2[1];
  volatile uint32_t COMP3;                    
  volatile uint32_t MASK3;                    
  volatile uint32_t FUNCTION3;                
} DWT_Type;

 






















































 



 



 



 



 



 



 



























   






 


 
typedef struct
{
  volatile uint32_t SSPSR;                    
  volatile uint32_t CSPSR;                    
       uint32_t RESERVED0[2];
  volatile uint32_t ACPR;                     
       uint32_t RESERVED1[55];
  volatile uint32_t SPPR;                     
       uint32_t RESERVED2[131];
  volatile const  uint32_t FFSR;                     
  volatile uint32_t FFCR;                     
  volatile const  uint32_t FSCR;                     
       uint32_t RESERVED3[759];
  volatile const  uint32_t TRIGGER;                  
  volatile const  uint32_t FIFO0;                    
  volatile const  uint32_t ITATBCTR2;                
       uint32_t RESERVED4[1];
  volatile const  uint32_t ITATBCTR0;                
  volatile const  uint32_t FIFO1;                    
  volatile uint32_t ITCTRL;                   
       uint32_t RESERVED5[39];
  volatile uint32_t CLAIMSET;                 
  volatile uint32_t CLAIMCLR;                 
       uint32_t RESERVED7[8];
  volatile const  uint32_t DEVID;                    
  volatile const  uint32_t DEVTYPE;                  
} TPI_Type;

 



 



 












 






 



 





















 



 





















 



 



 


















 






   







 


 
typedef struct
{
  volatile const  uint32_t TYPE;                     
  volatile uint32_t CTRL;                     
  volatile uint32_t RNR;                      
  volatile uint32_t RBAR;                     
  volatile uint32_t RASR;                     
  volatile uint32_t RBAR_A1;                  
  volatile uint32_t RASR_A1;                  
  volatile uint32_t RBAR_A2;                  
  volatile uint32_t RASR_A2;                  
  volatile uint32_t RBAR_A3;                  
  volatile uint32_t RASR_A3;                  
} MPU_Type;

 









 









 



 









 






























 







 


 
typedef struct
{
  volatile uint32_t DHCSR;                    
  volatile  uint32_t DCRSR;                    
  volatile uint32_t DCRDR;                    
  volatile uint32_t DEMCR;                    
} CoreDebug_Type;

 




































 






 







































 






 

 
#line 1227 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"

#line 1236 "C:\\Keil_v4\\ARM\\CMSIS\\Include\\core_cm3.h"






 










 

 



 




 










 
static __inline void NVIC_SetPriorityGrouping(uint32_t PriorityGroup)
{
  uint32_t reg_value;
  uint32_t PriorityGroupTmp = (PriorityGroup & (uint32_t)0x07);                

  reg_value  =  ((SCB_Type *) ((0xE000E000UL) + 0x0D00UL) )->AIRCR;                                                    
  reg_value &= ~((0xFFFFUL << 16) | (7UL << 8));              
  reg_value  =  (reg_value                                 |
                ((uint32_t)0x5FA << 16) |
                (PriorityGroupTmp << 8));                                      
  ((SCB_Type *) ((0xE000E000UL) + 0x0D00UL) )->AIRCR =  reg_value;
}







 
static __inline uint32_t NVIC_GetPriorityGrouping(void)
{
  return ((((SCB_Type *) ((0xE000E000UL) + 0x0D00UL) )->AIRCR & (7UL << 8)) >> 8);    
}







 
static __inline void NVIC_EnableIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  
}







 
static __inline void NVIC_DisableIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->ICER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  
}











 
static __inline uint32_t NVIC_GetPendingIRQ(IRQn_Type IRQn)
{
  return((uint32_t) ((((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->ISPR[(uint32_t)(IRQn) >> 5] & (1 << ((uint32_t)(IRQn) & 0x1F)))?1:0));  
}







 
static __inline void NVIC_SetPendingIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->ISPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  
}







 
static __inline void NVIC_ClearPendingIRQ(IRQn_Type IRQn)
{
  ((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  
}










 
static __inline uint32_t NVIC_GetActive(IRQn_Type IRQn)
{
  return((uint32_t)((((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->IABR[(uint32_t)(IRQn) >> 5] & (1 << ((uint32_t)(IRQn) & 0x1F)))?1:0));  
}










 
static __inline void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
  if(IRQn < 0) {
    ((SCB_Type *) ((0xE000E000UL) + 0x0D00UL) )->SHP[((uint32_t)(IRQn) & 0xF)-4] = ((priority << (8 - 5)) & 0xff); }  
  else {
    ((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->IP[(uint32_t)(IRQn)] = ((priority << (8 - 5)) & 0xff);    }         
}












 
static __inline uint32_t NVIC_GetPriority(IRQn_Type IRQn)
{

  if(IRQn < 0) {
    return((uint32_t)(((SCB_Type *) ((0xE000E000UL) + 0x0D00UL) )->SHP[((uint32_t)(IRQn) & 0xF)-4] >> (8 - 5)));  }  
  else {
    return((uint32_t)(((NVIC_Type *) ((0xE000E000UL) + 0x0100UL) )->IP[(uint32_t)(IRQn)]           >> (8 - 5)));  }  
}













 
static __inline uint32_t NVIC_EncodePriority (uint32_t PriorityGroup, uint32_t PreemptPriority, uint32_t SubPriority)
{
  uint32_t PriorityGroupTmp = (PriorityGroup & 0x07);           
  uint32_t PreemptPriorityBits;
  uint32_t SubPriorityBits;

  PreemptPriorityBits = ((7 - PriorityGroupTmp) > 5) ? 5 : 7 - PriorityGroupTmp;
  SubPriorityBits     = ((PriorityGroupTmp + 5) < 7) ? 0 : PriorityGroupTmp - 7 + 5;

  return (
           ((PreemptPriority & ((1 << (PreemptPriorityBits)) - 1)) << SubPriorityBits) |
           ((SubPriority     & ((1 << (SubPriorityBits    )) - 1)))
         );
}













 
static __inline void NVIC_DecodePriority (uint32_t Priority, uint32_t PriorityGroup, uint32_t* pPreemptPriority, uint32_t* pSubPriority)
{
  uint32_t PriorityGroupTmp = (PriorityGroup & 0x07);           
  uint32_t PreemptPriorityBits;
  uint32_t SubPriorityBits;

  PreemptPriorityBits = ((7 - PriorityGroupTmp) > 5) ? 5 : 7 - PriorityGroupTmp;
  SubPriorityBits     = ((PriorityGroupTmp + 5) < 7) ? 0 : PriorityGroupTmp - 7 + 5;

  *pPreemptPriority = (Priority >> SubPriorityBits) & ((1 << (PreemptPriorityBits)) - 1);
  *pSubPriority     = (Priority                   ) & ((1 << (SubPriorityBits    )) - 1);
}





 
static __inline void NVIC_SystemReset(void)
{
  __dsb(0xF);                                                     
 
  ((SCB_Type *) ((0xE000E000UL) + 0x0D00UL) )->AIRCR  = ((0x5FA << 16)      |
                 (((SCB_Type *) ((0xE000E000UL) + 0x0D00UL) )->AIRCR & (7UL << 8)) |
                 (1UL << 2));                    
  __dsb(0xF);                                                      
  while(1);                                                     
}

 



 




 

















 
static __inline uint32_t SysTick_Config(uint32_t ticks)
{
  if ((ticks - 1) > (0xFFFFFFUL << 0))  return (1);       

  ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->LOAD  = ticks - 1;                                   
  NVIC_SetPriority (SysTick_IRQn, (1<<5) - 1);   
  ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->VAL   = 0;                                           
  ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->CTRL  = (1UL << 2) |
                   (1UL << 1)   |
                   (1UL << 0);                     
  return (0);                                                   
}



 



 




 

extern volatile int32_t ITM_RxBuffer;                     












 
static __inline uint32_t ITM_SendChar (uint32_t ch)
{
  if ((((ITM_Type *) (0xE0000000UL) )->TCR & (1UL << 0))                  &&       
      (((ITM_Type *) (0xE0000000UL) )->TER & (1UL << 0)        )                    )      
  {
    while (((ITM_Type *) (0xE0000000UL) )->PORT[0].u32 == 0);
    ((ITM_Type *) (0xE0000000UL) )->PORT[0].u8 = (uint8_t) ch;
  }
  return (ch);
}








 
static __inline int32_t ITM_ReceiveChar (void) {
  int32_t ch = -1;                            

  if (ITM_RxBuffer != 0x5AA55AA5) {
    ch = ITM_RxBuffer;
    ITM_RxBuffer = 0x5AA55AA5;        
  }

  return (ch);
}








 
static __inline int32_t ITM_CheckChar (void) {

  if (ITM_RxBuffer == 0x5AA55AA5) {
    return (0);                                  
  } else {
    return (1);                                  
  }
}

 





#line 106 ".\\Common\\LPC177x_8x.h"
#line 1 ".\\Common\\system_LPC177x_8x.h"


 





















 








#line 35 ".\\Common\\system_LPC177x_8x.h"

extern uint32_t SystemCoreClock;       
extern uint32_t PeripheralClock;	     
extern uint32_t EMCClock;			         
extern uint32_t USBClock;			         










 
extern void SystemInit (void);









 
extern void SystemCoreClockUpdate (void);



 










 





#line 107 ".\\Common\\LPC177x_8x.h"


 
 
 


#pragma anon_unions


 
typedef struct
{
  volatile uint32_t FLASHCFG;                    
       uint32_t RESERVED0[31];
  volatile uint32_t PLL0CON;                     
  volatile uint32_t PLL0CFG;                     
  volatile const  uint32_t PLL0STAT;                    
  volatile  uint32_t PLL0FEED;                    
       uint32_t RESERVED1[4];
  volatile uint32_t PLL1CON;                     
  volatile uint32_t PLL1CFG;                     
  volatile const  uint32_t PLL1STAT;                    
  volatile  uint32_t PLL1FEED;                    
       uint32_t RESERVED2[4];
  volatile uint32_t PCON;                        
  volatile uint32_t PCONP;                       
       uint32_t RESERVED3[14];
  volatile uint32_t EMCCLKSEL;                   
  volatile uint32_t CCLKSEL;                     
  volatile uint32_t USBCLKSEL;                   
  volatile uint32_t CLKSRCSEL;                   
  volatile uint32_t	CANSLEEPCLR;                 
  volatile uint32_t	CANWAKEFLAGS;                
       uint32_t RESERVED4[10];
  volatile uint32_t EXTINT;                      
       uint32_t RESERVED5[1];
  volatile uint32_t EXTMODE;                     
  volatile uint32_t EXTPOLAR;                    
       uint32_t RESERVED6[12];
  volatile uint32_t RSID;                        
       uint32_t RESERVED7[7];
  volatile uint32_t SCS;                         
  volatile uint32_t IRCTRIM;                     
  volatile uint32_t PCLKSEL;                     
       uint32_t RESERVED8;					
  volatile uint32_t PBOOST;						 	   
       uint32_t RESERVED9;					
  volatile uint32_t LCD_CFG;                     
       uint32_t RESERVED10[1];
  volatile uint32_t USBIntSt;                    
  volatile uint32_t DMAREQSEL;                   
  volatile uint32_t CLKOUTCFG;                   
  volatile uint32_t RSTCON0;                     
  volatile uint32_t RSTCON1;                     
       uint32_t RESERVED11[2];
  volatile uint32_t EMCDLYCTL;                   
  volatile uint32_t EMCCAL;                      
 } LPC_SC_TypeDef;

 
typedef struct
{
  volatile uint32_t P0_0;				 
  volatile uint32_t P0_1;
  volatile uint32_t P0_2;
  volatile uint32_t P0_3;
  volatile uint32_t P0_4;
  volatile uint32_t P0_5;
  volatile uint32_t P0_6;
  volatile uint32_t P0_7;

  volatile uint32_t P0_8;				 
  volatile uint32_t P0_9;
  volatile uint32_t P0_10;
  volatile uint32_t P0_11;
  volatile uint32_t P0_12;
  volatile uint32_t P0_13;
  volatile uint32_t P0_14;
  volatile uint32_t P0_15;

  volatile uint32_t P0_16;				 
  volatile uint32_t P0_17;
  volatile uint32_t P0_18;
  volatile uint32_t P0_19;
  volatile uint32_t P0_20;
  volatile uint32_t P0_21;
  volatile uint32_t P0_22;
  volatile uint32_t P0_23;

  volatile uint32_t P0_24;				 
  volatile uint32_t P0_25;
  volatile uint32_t P0_26;
  volatile uint32_t P0_27;
  volatile uint32_t P0_28;
  volatile uint32_t P0_29;
  volatile uint32_t P0_30;
  volatile uint32_t P0_31;

  volatile uint32_t P1_0;				 
  volatile uint32_t P1_1;
  volatile uint32_t P1_2;
  volatile uint32_t P1_3;
  volatile uint32_t P1_4;
  volatile uint32_t P1_5;
  volatile uint32_t P1_6;
  volatile uint32_t P1_7;

  volatile uint32_t P1_8;				 
  volatile uint32_t P1_9;
  volatile uint32_t P1_10;
  volatile uint32_t P1_11;
  volatile uint32_t P1_12;
  volatile uint32_t P1_13;
  volatile uint32_t P1_14;
  volatile uint32_t P1_15;

  volatile uint32_t P1_16;				 
  volatile uint32_t P1_17;
  volatile uint32_t P1_18;
  volatile uint32_t P1_19;
  volatile uint32_t P1_20;
  volatile uint32_t P1_21;
  volatile uint32_t P1_22;
  volatile uint32_t P1_23;

  volatile uint32_t P1_24;				 
  volatile uint32_t P1_25;
  volatile uint32_t P1_26;
  volatile uint32_t P1_27;
  volatile uint32_t P1_28;
  volatile uint32_t P1_29;
  volatile uint32_t P1_30;
  volatile uint32_t P1_31;

  volatile uint32_t P2_0;				 
  volatile uint32_t P2_1;
  volatile uint32_t P2_2;
  volatile uint32_t P2_3;
  volatile uint32_t P2_4;
  volatile uint32_t P2_5;
  volatile uint32_t P2_6;
  volatile uint32_t P2_7;

  volatile uint32_t P2_8;				 
  volatile uint32_t P2_9;
  volatile uint32_t P2_10;
  volatile uint32_t P2_11;
  volatile uint32_t P2_12;
  volatile uint32_t P2_13;
  volatile uint32_t P2_14;
  volatile uint32_t P2_15;

  volatile uint32_t P2_16;				 
  volatile uint32_t P2_17;
  volatile uint32_t P2_18;
  volatile uint32_t P2_19;
  volatile uint32_t P2_20;
  volatile uint32_t P2_21;
  volatile uint32_t P2_22;
  volatile uint32_t P2_23;

  volatile uint32_t P2_24;				 
  volatile uint32_t P2_25;
  volatile uint32_t P2_26;
  volatile uint32_t P2_27;
  volatile uint32_t P2_28;
  volatile uint32_t P2_29;
  volatile uint32_t P2_30;
  volatile uint32_t P2_31;

  volatile uint32_t P3_0;				 
  volatile uint32_t P3_1;
  volatile uint32_t P3_2;
  volatile uint32_t P3_3;
  volatile uint32_t P3_4;
  volatile uint32_t P3_5;
  volatile uint32_t P3_6;
  volatile uint32_t P3_7;

  volatile uint32_t P3_8;				 
  volatile uint32_t P3_9;
  volatile uint32_t P3_10;
  volatile uint32_t P3_11;
  volatile uint32_t P3_12;
  volatile uint32_t P3_13;
  volatile uint32_t P3_14;
  volatile uint32_t P3_15;

  volatile uint32_t P3_16;				 
  volatile uint32_t P3_17;
  volatile uint32_t P3_18;
  volatile uint32_t P3_19;
  volatile uint32_t P3_20;
  volatile uint32_t P3_21;
  volatile uint32_t P3_22;
  volatile uint32_t P3_23;

  volatile uint32_t P3_24;				 
  volatile uint32_t P3_25;
  volatile uint32_t P3_26;
  volatile uint32_t P3_27;
  volatile uint32_t P3_28;
  volatile uint32_t P3_29;
  volatile uint32_t P3_30;
  volatile uint32_t P3_31;

  volatile uint32_t P4_0;				 
  volatile uint32_t P4_1;
  volatile uint32_t P4_2;
  volatile uint32_t P4_3;
  volatile uint32_t P4_4;
  volatile uint32_t P4_5;
  volatile uint32_t P4_6;
  volatile uint32_t P4_7;

  volatile uint32_t P4_8;				 
  volatile uint32_t P4_9;
  volatile uint32_t P4_10;
  volatile uint32_t P4_11;
  volatile uint32_t P4_12;
  volatile uint32_t P4_13;
  volatile uint32_t P4_14;
  volatile uint32_t P4_15;

  volatile uint32_t P4_16;				 
  volatile uint32_t P4_17;
  volatile uint32_t P4_18;
  volatile uint32_t P4_19;
  volatile uint32_t P4_20;
  volatile uint32_t P4_21;
  volatile uint32_t P4_22;
  volatile uint32_t P4_23;

  volatile uint32_t P4_24;				 
  volatile uint32_t P4_25;
  volatile uint32_t P4_26;
  volatile uint32_t P4_27;
  volatile uint32_t P4_28;
  volatile uint32_t P4_29;
  volatile uint32_t P4_30;
  volatile uint32_t P4_31;

  volatile uint32_t P5_0;				 
  volatile uint32_t P5_1;
  volatile uint32_t P5_2;
  volatile uint32_t P5_3;
  volatile uint32_t P5_4;				 
} LPC_IOCON_TypeDef;

 
typedef struct
{
  volatile uint32_t DIR;
       uint32_t RESERVED0[3];
  volatile uint32_t MASK;
  volatile uint32_t PIN;
  volatile uint32_t SET;
  volatile  uint32_t CLR;
} LPC_GPIO_TypeDef;

typedef struct
{
  volatile const  uint32_t IntStatus;
  volatile const  uint32_t IO0IntStatR;
  volatile const  uint32_t IO0IntStatF;
  volatile  uint32_t IO0IntClr;
  volatile uint32_t IO0IntEnR;
  volatile uint32_t IO0IntEnF;
       uint32_t RESERVED0[3];
  volatile const  uint32_t IO2IntStatR;
  volatile const  uint32_t IO2IntStatF;
  volatile  uint32_t IO2IntClr;
  volatile uint32_t IO2IntEnR;
  volatile uint32_t IO2IntEnF;
} LPC_GPIOINT_TypeDef;

 
typedef struct
{
  volatile uint32_t IR;                      
  volatile uint32_t TCR;                     
  volatile uint32_t TC;                      
  volatile uint32_t PR;                      
  volatile uint32_t PC;                      
  volatile uint32_t MCR;                     
  volatile uint32_t MR0;                     
  volatile uint32_t MR1;                     
  volatile uint32_t MR2;                     
  volatile uint32_t MR3;                     
  volatile uint32_t CCR;                     
  volatile const  uint32_t CR0;                     
  volatile const  uint32_t CR1;					 
       uint32_t RESERVED0[2];
  volatile uint32_t EMR;                     
       uint32_t RESERVED1[12];
  volatile uint32_t CTCR;                    
} LPC_TIM_TypeDef;

 
typedef struct
{
  volatile uint32_t IR;                      
  volatile uint32_t TCR;                     
  volatile uint32_t TC;                      
  volatile uint32_t PR;                      
  volatile uint32_t PC;                      
  volatile uint32_t MCR;                     
  volatile uint32_t MR0;                     
  volatile uint32_t MR1;                     
  volatile uint32_t MR2;                     
  volatile uint32_t MR3;                     
  volatile uint32_t CCR;                     
  volatile const  uint32_t CR0;                     
  volatile const  uint32_t CR1;					 
  volatile const  uint32_t CR2;					 
  volatile const  uint32_t CR3;					 
       uint32_t RESERVED0;
  volatile uint32_t MR4;					 
  volatile uint32_t MR5;					 
  volatile uint32_t MR6;					 
  volatile uint32_t PCR;					 
  volatile uint32_t LER;					 
       uint32_t RESERVED1[7];
  volatile uint32_t CTCR;					 
} LPC_PWM_TypeDef;

 




 

#line 474 ".\\Common\\LPC177x_8x.h"
typedef struct
{
	union
	{
		volatile const  uint8_t  RBR;
		volatile  uint8_t  THR;
		volatile uint8_t  DLL;
		uint32_t RESERVED0;
	};
	union
	{
		volatile uint8_t  DLM;
		volatile uint32_t IER;
	};
	union
	{
		volatile const  uint32_t IIR;
		volatile  uint8_t  FCR;
	};
	volatile uint8_t  LCR;
	uint8_t  RESERVED1[7];
	volatile const  uint8_t  LSR;
	uint8_t  RESERVED2[7];
	volatile uint8_t  SCR;
	uint8_t  RESERVED3[3];
	volatile uint32_t ACR;
	volatile uint8_t  ICR;
	uint8_t  RESERVED4[3];
	volatile uint8_t  FDR;
	uint8_t  RESERVED5[7];
	volatile uint8_t  TER;
	uint8_t  RESERVED8[27];
	volatile uint8_t  RS485CTRL;
	uint8_t  RESERVED9[3];
	volatile uint8_t  ADRMATCH;
	uint8_t  RESERVED10[3];
	volatile uint8_t  RS485DLY;
	uint8_t  RESERVED11[3];
	volatile const  uint8_t  FIFOLVL;
}LPC_UART_TypeDef;



typedef struct
{
  union {
  volatile const  uint8_t  RBR;
  volatile  uint8_t  THR;
  volatile uint8_t  DLL;
       uint32_t RESERVED0;
  };
  union {
  volatile uint8_t  DLM;
  volatile uint32_t IER;
  };
  union {
  volatile const  uint32_t IIR;
  volatile  uint8_t  FCR;
  };
  volatile uint8_t  LCR;
       uint8_t  RESERVED1[3];
  volatile uint8_t  MCR;
       uint8_t  RESERVED2[3];
  volatile const  uint8_t  LSR;
       uint8_t  RESERVED3[3];
  volatile const  uint8_t  MSR;
       uint8_t  RESERVED4[3];
  volatile uint8_t  SCR;
       uint8_t  RESERVED5[3];
  volatile uint32_t ACR;
       uint32_t RESERVED6;
  volatile uint32_t FDR;
       uint32_t RESERVED7;
  volatile uint8_t  TER;
       uint8_t  RESERVED8[27];
  volatile uint8_t  RS485CTRL;
       uint8_t  RESERVED9[3];
  volatile uint8_t  ADRMATCH;
       uint8_t  RESERVED10[3];
  volatile uint8_t  RS485DLY;
       uint8_t  RESERVED11[3];
  volatile const  uint8_t  FIFOLVL;
} LPC_UART1_TypeDef;

typedef struct
{
  union {
  volatile const  uint32_t  RBR;                    
  volatile  uint32_t  THR;                    
  volatile uint32_t  DLL;                    
  };
  union {
  volatile uint32_t  DLM;                    
  volatile uint32_t  IER;                    
  };
  union {
  volatile const  uint32_t  IIR;                    
  volatile  uint32_t  FCR;                    
  };
  volatile uint32_t  LCR;                    
  volatile uint32_t  MCR;                    
  volatile const  uint32_t  LSR;                    
  volatile const  uint32_t  MSR;                    
  volatile uint32_t  SCR;                    
  volatile uint32_t  ACR;                    
  volatile uint32_t  ICR;                    
  volatile uint32_t  FDR;                    
  volatile uint32_t  OSR;                    
  volatile  uint32_t  POP;                    
  volatile uint32_t  MODE;                   
       uint32_t  RESERVED0[2];
  volatile uint32_t  HDEN;                   
       uint32_t  RESERVED1;
  volatile uint32_t  SCI_CTRL;				 
  volatile uint32_t  RS485CTRL;              
  volatile uint32_t  ADRMATCH;               
  volatile uint32_t  RS485DLY;               
  volatile uint32_t  SYNCCTRL;               
  volatile uint32_t  TER;                    
       uint32_t  RESERVED2[989];
  volatile const  uint32_t  CFG;                    
  volatile  uint32_t  INTCE;                  
  volatile  uint32_t  INTSE;                  
  volatile const  uint32_t  INTS;                   
  volatile const  uint32_t  INTE;                   
  volatile  uint32_t  INTCS;                  
  volatile  uint32_t  INTSS;                  
       uint32_t  RESERVED3[3];
  volatile const  uint32_t  MID;                    
} LPC_UART4_TypeDef;

 
typedef struct
{
  volatile uint32_t CR0;                     
  volatile uint32_t CR1;                     
  volatile uint32_t DR;                      
  volatile const  uint32_t SR;                      
  volatile uint32_t CPSR;                    
  volatile uint32_t IMSC;                    
  volatile uint32_t RIS;                     
  volatile uint32_t MIS;                     
  volatile uint32_t ICR;                     
  volatile uint32_t DMACR;
} LPC_SSP_TypeDef;

 
typedef struct
{
  volatile uint32_t CONSET;                  
  volatile const  uint32_t STAT;                    
  volatile uint32_t DAT;                     
  volatile uint32_t ADR0;                    
  volatile uint32_t SCLH;                    
  volatile uint32_t SCLL;                    
  volatile  uint32_t CONCLR;                  
  volatile uint32_t MMCTRL;                  
  volatile uint32_t ADR1;                    
  volatile uint32_t ADR2;                    
  volatile uint32_t ADR3;                    
  volatile const  uint32_t DATA_BUFFER;             
  volatile uint32_t MASK0;                   
  volatile uint32_t MASK1;                   
  volatile uint32_t MASK2;                   
  volatile uint32_t MASK3;                   
} LPC_I2C_TypeDef;

 
typedef struct
{
  volatile uint32_t DAO;
  volatile uint32_t DAI;
  volatile  uint32_t TXFIFO;
  volatile const  uint32_t RXFIFO;
  volatile const  uint32_t STATE;
  volatile uint32_t DMA1;
  volatile uint32_t DMA2;
  volatile uint32_t IRQ;
  volatile uint32_t TXRATE;
  volatile uint32_t RXRATE;
  volatile uint32_t TXBITRATE;
  volatile uint32_t RXBITRATE;
  volatile uint32_t TXMODE;
  volatile uint32_t RXMODE;
} LPC_I2S_TypeDef;

 
typedef struct
{
  volatile uint8_t  ILR;
       uint8_t  RESERVED0[7];
  volatile uint8_t  CCR;
       uint8_t  RESERVED1[3];
  volatile uint8_t  CIIR;
       uint8_t  RESERVED2[3];
  volatile uint8_t  AMR;
       uint8_t  RESERVED3[3];
  volatile const  uint32_t CTIME0;
  volatile const  uint32_t CTIME1;
  volatile const  uint32_t CTIME2;
  volatile uint8_t  SEC;
       uint8_t  RESERVED4[3];
  volatile uint8_t  MIN;
       uint8_t  RESERVED5[3];
  volatile uint8_t  HOUR;
       uint8_t  RESERVED6[3];
  volatile uint8_t  DOM;
       uint8_t  RESERVED7[3];
  volatile uint8_t  DOW;
       uint8_t  RESERVED8[3];
  volatile uint16_t DOY;
       uint16_t RESERVED9;
  volatile uint8_t  MONTH;
       uint8_t  RESERVED10[3];
  volatile uint16_t YEAR;
       uint16_t RESERVED11;
  volatile uint32_t CALIBRATION;
  volatile uint32_t GPREG0;
  volatile uint32_t GPREG1;
  volatile uint32_t GPREG2;
  volatile uint32_t GPREG3;
  volatile uint32_t GPREG4;
  volatile uint8_t  RTC_AUXEN;
       uint8_t  RESERVED12[3];
  volatile uint8_t  RTC_AUX;
       uint8_t  RESERVED13[3];
  volatile uint8_t  ALSEC;
       uint8_t  RESERVED14[3];
  volatile uint8_t  ALMIN;
       uint8_t  RESERVED15[3];
  volatile uint8_t  ALHOUR;
       uint8_t  RESERVED16[3];
  volatile uint8_t  ALDOM;
       uint8_t  RESERVED17[3];
  volatile uint8_t  ALDOW;
       uint8_t  RESERVED18[3];
  volatile uint16_t ALDOY;
       uint16_t RESERVED19;
  volatile uint8_t  ALMON;
       uint8_t  RESERVED20[3];
  volatile uint16_t ALYEAR;
       uint16_t RESERVED21;
  volatile uint32_t ERSTATUS;
  volatile uint32_t ERCONTROL;
  volatile uint32_t ERCOUNTERS;
       uint32_t RESERVED22;
  volatile uint32_t ERFIRSTSTAMP0;
  volatile uint32_t ERFIRSTSTAMP1;
  volatile uint32_t ERFIRSTSTAMP2;
       uint32_t RESERVED23;
  volatile uint32_t ERLASTSTAMP0;
  volatile uint32_t ERLASTSTAMP1;
  volatile uint32_t ERLASTSTAMP2;
} LPC_RTC_TypeDef;

 
typedef struct
{
  volatile uint8_t  MOD;
       uint8_t  RESERVED0[3];
  volatile uint32_t TC;
  volatile  uint8_t  FEED;
       uint8_t  RESERVED1[3];
  volatile const  uint32_t TV;
       uint32_t RESERVED2;
  volatile uint32_t WARNINT;
  volatile uint32_t WINDOW;
} LPC_WDT_TypeDef;

 
typedef struct
{
  volatile uint32_t CR;                      
  volatile uint32_t GDR;                     
       uint32_t RESERVED0;
  volatile uint32_t INTEN;                   
  volatile uint32_t DR[8];                   
  volatile const  uint32_t STAT;                    
  volatile uint32_t ADTRM;
} LPC_ADC_TypeDef;

 
typedef struct
{
  volatile uint32_t CR;
  volatile uint32_t CTRL;
  volatile uint32_t CNTVAL;
} LPC_DAC_TypeDef;

 
typedef struct
{
  volatile const  uint32_t CON;
  volatile  uint32_t CON_SET;
  volatile  uint32_t CON_CLR;
  volatile const  uint32_t CAPCON;
  volatile  uint32_t CAPCON_SET;
  volatile  uint32_t CAPCON_CLR;
  volatile uint32_t TC0;
  volatile uint32_t TC1;
  volatile uint32_t TC2;
  volatile uint32_t LIM0;
  volatile uint32_t LIM1;
  volatile uint32_t LIM2;
  volatile uint32_t MAT0;
  volatile uint32_t MAT1;
  volatile uint32_t MAT2;
  volatile uint32_t DT;
  volatile uint32_t CP;
  volatile uint32_t CAP0;
  volatile uint32_t CAP1;
  volatile uint32_t CAP2;
  volatile const  uint32_t INTEN;
  volatile  uint32_t INTEN_SET;
  volatile  uint32_t INTEN_CLR;
  volatile const  uint32_t CNTCON;
  volatile  uint32_t CNTCON_SET;
  volatile  uint32_t CNTCON_CLR;
  volatile const  uint32_t INTF;
  volatile  uint32_t INTF_SET;
  volatile  uint32_t INTF_CLR;
  volatile  uint32_t CAP_CLR;
} LPC_MCPWM_TypeDef;

 
typedef struct
{
  volatile  uint32_t CON;
  volatile const  uint32_t STAT;
  volatile uint32_t CONF;
  volatile const  uint32_t POS;
  volatile uint32_t MAXPOS;
  volatile uint32_t CMPOS0;
  volatile uint32_t CMPOS1;
  volatile uint32_t CMPOS2;
  volatile const  uint32_t INXCNT;
  volatile uint32_t INXCMP0;
  volatile uint32_t LOAD;
  volatile const  uint32_t TIME;
  volatile const  uint32_t VEL;
  volatile const  uint32_t CAP;
  volatile uint32_t VELCOMP;
  volatile uint32_t FILTERPHA;
  volatile uint32_t FILTERPHB;
  volatile uint32_t FILTERINX;
  volatile uint32_t WINDOW;
  volatile uint32_t INXCMP1;
  volatile uint32_t INXCMP2;
       uint32_t RESERVED0[993];
  volatile  uint32_t IEC;
  volatile  uint32_t IES;
  volatile const  uint32_t INTSTAT;
  volatile const  uint32_t IE;
  volatile  uint32_t CLR;
  volatile  uint32_t SET;
} LPC_QEI_TypeDef;

 
typedef struct
{
  volatile uint32_t POWER;
  volatile uint32_t CLOCK;
  volatile uint32_t ARGUMENT;
  volatile uint32_t COMMAND;
  volatile const  uint32_t RESP_CMD;
  volatile const  uint32_t RESP0;
  volatile const  uint32_t RESP1;
  volatile const  uint32_t RESP2;
  volatile const  uint32_t RESP3;
  volatile uint32_t DATATMR;
  volatile uint32_t DATALEN;
  volatile uint32_t DATACTRL;
  volatile const  uint32_t DATACNT;
  volatile const  uint32_t STATUS;
  volatile  uint32_t CLEAR;
  volatile uint32_t MASK0;
       uint32_t RESERVED0[2];
  volatile const  uint32_t FIFOCNT;
       uint32_t RESERVED1[13];
  volatile uint32_t FIFO;
} LPC_MCI_TypeDef;

 
typedef struct
{
  volatile uint32_t mask[512];               
} LPC_CANAF_RAM_TypeDef;

typedef struct                           
{
	
	volatile uint32_t AFMR;

	
	volatile uint32_t SFF_sa;

	
	volatile uint32_t SFF_GRP_sa;

	
	volatile uint32_t EFF_sa;

	
	volatile uint32_t EFF_GRP_sa;

	
	volatile uint32_t ENDofTable;

	
	volatile const  uint32_t LUTerrAd;

	
	volatile const  uint32_t LUTerr;

	
	volatile uint32_t FCANIE;

	
	volatile uint32_t FCANIC0;

	
	volatile uint32_t FCANIC1;
} LPC_CANAF_TypeDef;

typedef struct                           
{
  volatile const  uint32_t TxSR;
  volatile const  uint32_t RxSR;
  volatile const  uint32_t MSR;
} LPC_CANCR_TypeDef;

typedef struct                           
{
	
	volatile uint32_t MOD;

	
	volatile  uint32_t CMR;

	
	volatile uint32_t GSR;

	
	volatile const  uint32_t ICR;

	
	volatile uint32_t IER;

	
	volatile uint32_t BTR;

	
	volatile uint32_t EWL;

	
	volatile const  uint32_t SR;

	
	volatile uint32_t RFS;

	
	volatile uint32_t RID;

	
	volatile uint32_t RDA;

	
	volatile uint32_t RDB;

	
	volatile uint32_t TFI1;

	
	volatile uint32_t TID1;

	
	volatile uint32_t TDA1;

	
	volatile uint32_t TDB1;

	
	volatile uint32_t TFI2;

	
	volatile uint32_t TID2;

	
	volatile uint32_t TDA2;

	
	volatile uint32_t TDB2;

	
	volatile uint32_t TFI3;

	
	volatile uint32_t TID3;

	
	volatile uint32_t TDA3;

	
	volatile uint32_t TDB3;
} LPC_CAN_TypeDef;

 
typedef struct                           
{
  volatile const  uint32_t IntStat;
  volatile const  uint32_t IntTCStat;
  volatile  uint32_t IntTCClear;
  volatile const  uint32_t IntErrStat;
  volatile  uint32_t IntErrClr;
  volatile const  uint32_t RawIntTCStat;
  volatile const  uint32_t RawIntErrStat;
  volatile const  uint32_t EnbldChns;
  volatile uint32_t SoftBReq;
  volatile uint32_t SoftSReq;
  volatile uint32_t SoftLBReq;
  volatile uint32_t SoftLSReq;
  volatile uint32_t Config;
  volatile uint32_t Sync;
} LPC_GPDMA_TypeDef;

typedef struct                           
{
  volatile uint32_t CSrcAddr;
  volatile uint32_t CDestAddr;
  volatile uint32_t CLLI;
  volatile uint32_t CControl;
  volatile uint32_t CConfig;
} LPC_GPDMACH_TypeDef;

 
typedef struct
{
  volatile const  uint32_t Revision;              
  volatile uint32_t Control;
  volatile uint32_t CommandStatus;
  volatile uint32_t InterruptStatus;
  volatile uint32_t InterruptEnable;
  volatile uint32_t InterruptDisable;
  volatile uint32_t HCCA;
  volatile const  uint32_t PeriodCurrentED;
  volatile uint32_t ControlHeadED;
  volatile uint32_t ControlCurrentED;
  volatile uint32_t BulkHeadED;
  volatile uint32_t BulkCurrentED;
  volatile const  uint32_t DoneHead;
  volatile uint32_t FmInterval;
  volatile const  uint32_t FmRemaining;
  volatile const  uint32_t FmNumber;
  volatile uint32_t PeriodicStart;
  volatile uint32_t LSTreshold;
  volatile uint32_t RhDescriptorA;
  volatile uint32_t RhDescriptorB;
  volatile uint32_t RhStatus;
  volatile uint32_t RhPortStatus1;
  volatile uint32_t RhPortStatus2;
       uint32_t RESERVED0[40];
  volatile const  uint32_t Module_ID;

  volatile const  uint32_t IntSt;                
  volatile uint32_t IntEn;
  volatile  uint32_t IntSet;
  volatile  uint32_t IntClr;
  volatile uint32_t StCtrl;
  volatile uint32_t Tmr;
       uint32_t RESERVED1[58];

  volatile const  uint32_t DevIntSt;             
  volatile uint32_t DevIntEn;
  volatile  uint32_t DevIntClr;
  volatile  uint32_t DevIntSet;

  volatile  uint32_t CmdCode;              
  volatile const  uint32_t CmdData;

  volatile const  uint32_t RxData;               
  volatile  uint32_t TxData;
  volatile const  uint32_t RxPLen;
  volatile  uint32_t TxPLen;
  volatile uint32_t Ctrl;
  volatile  uint32_t DevIntPri;

  volatile const  uint32_t EpIntSt;              
  volatile uint32_t EpIntEn;
  volatile  uint32_t EpIntClr;
  volatile  uint32_t EpIntSet;
  volatile  uint32_t EpIntPri;

  volatile uint32_t ReEp;                 
  volatile  uint32_t EpInd;
  volatile uint32_t MaxPSize;

  volatile const  uint32_t DMARSt;               
  volatile  uint32_t DMARClr;
  volatile  uint32_t DMARSet;
       uint32_t RESERVED2[9];
  volatile uint32_t UDCAH;
  volatile const  uint32_t EpDMASt;
  volatile  uint32_t EpDMAEn;
  volatile  uint32_t EpDMADis;
  volatile const  uint32_t DMAIntSt;
  volatile uint32_t DMAIntEn;
       uint32_t RESERVED3[2];
  volatile const  uint32_t EoTIntSt;
  volatile  uint32_t EoTIntClr;
  volatile  uint32_t EoTIntSet;
  volatile const  uint32_t NDDRIntSt;
  volatile  uint32_t NDDRIntClr;
  volatile  uint32_t NDDRIntSet;
  volatile const  uint32_t SysErrIntSt;
  volatile  uint32_t SysErrIntClr;
  volatile  uint32_t SysErrIntSet;
       uint32_t RESERVED4[15];

  union {
  volatile const  uint32_t I2C_RX;                  
  volatile  uint32_t I2C_TX;
  };
  volatile  uint32_t I2C_STS;
  volatile uint32_t I2C_CTL;
  volatile uint32_t I2C_CLKHI;
  volatile  uint32_t I2C_CLKLO;
       uint32_t RESERVED5[824];

  union {
  volatile uint32_t USBClkCtrl;              
  volatile uint32_t OTGClkCtrl;
  };
  union {
  volatile const  uint32_t USBClkSt;
  volatile const  uint32_t OTGClkSt;
  };
} LPC_USB_TypeDef;

 
typedef struct
{
  volatile uint32_t MAC1;                    
  volatile uint32_t MAC2;
  volatile uint32_t IPGT;
  volatile uint32_t IPGR;
  volatile uint32_t CLRT;
  volatile uint32_t MAXF;
  volatile uint32_t SUPP;
  volatile uint32_t TEST;
  volatile uint32_t MCFG;
  volatile uint32_t MCMD;
  volatile uint32_t MADR;
  volatile  uint32_t MWTD;
  volatile const  uint32_t MRDD;
  volatile const  uint32_t MIND;
       uint32_t RESERVED0[2];
  volatile uint32_t SA0;
  volatile uint32_t SA1;
  volatile uint32_t SA2;
       uint32_t RESERVED1[45];
  volatile uint32_t Command;                 
  volatile const  uint32_t Status;
  volatile uint32_t RxDescriptor;
  volatile uint32_t RxStatus;
  volatile uint32_t RxDescriptorNumber;
  volatile const  uint32_t RxProduceIndex;
  volatile uint32_t RxConsumeIndex;
  volatile uint32_t TxDescriptor;
  volatile uint32_t TxStatus;
  volatile uint32_t TxDescriptorNumber;
  volatile uint32_t TxProduceIndex;
  volatile const  uint32_t TxConsumeIndex;
       uint32_t RESERVED2[10];
  volatile const  uint32_t TSV0;
  volatile const  uint32_t TSV1;
  volatile const  uint32_t RSV;
       uint32_t RESERVED3[3];
  volatile uint32_t FlowControlCounter;
  volatile const  uint32_t FlowControlStatus;
       uint32_t RESERVED4[34];
  volatile uint32_t RxFilterCtrl;            
  volatile const  uint32_t RxFilterWoLStatus;
  volatile  uint32_t RxFilterWoLClear;
       uint32_t RESERVED5;
  volatile uint32_t HashFilterL;
  volatile uint32_t HashFilterH;
       uint32_t RESERVED6[882];
  volatile const  uint32_t IntStatus;               
  volatile uint32_t IntEnable;
  volatile  uint32_t IntClear;
  volatile  uint32_t IntSet;
       uint32_t RESERVED7;
  volatile uint32_t PowerDown;
       uint32_t RESERVED8;
  volatile uint32_t Module_ID;
} LPC_EMAC_TypeDef;

 
typedef struct
{
  volatile uint32_t TIMH;                    
  volatile uint32_t TIMV;
  volatile uint32_t POL;
  volatile uint32_t LE;
  volatile uint32_t UPBASE;
  volatile uint32_t LPBASE;
  volatile uint32_t CTRL;
  volatile uint32_t INTMSK;
  volatile const  uint32_t INTRAW;
  volatile const  uint32_t INTSTAT;
  volatile  uint32_t INTCLR;
  volatile const  uint32_t UPCURR;
  volatile const  uint32_t LPCURR;
       uint32_t RESERVED0[115];
  volatile uint32_t PAL[128];
       uint32_t RESERVED1[256];
  volatile uint32_t CRSR_IMG[256];
  volatile uint32_t CRSR_CTRL;
  volatile uint32_t CRSR_CFG;
  volatile uint32_t CRSR_PAL0;
  volatile uint32_t CRSR_PAL1;
  volatile uint32_t CRSR_XY;
  volatile uint32_t CRSR_CLIP;
       uint32_t RESERVED2[2];
  volatile uint32_t CRSR_INTMSK;
  volatile  uint32_t CRSR_INTCLR;
  volatile const  uint32_t CRSR_INTRAW;
  volatile const  uint32_t CRSR_INTSTAT;
} LPC_LCD_TypeDef;

 
typedef struct
{
  volatile uint32_t Control;
  volatile const  uint32_t Status;
  volatile uint32_t Config;
       uint32_t RESERVED0[5];
  volatile uint32_t DynamicControl;
  volatile uint32_t DynamicRefresh;
  volatile uint32_t DynamicReadConfig;
       uint32_t RESERVED1[1];
  volatile uint32_t DynamicRP;
  volatile uint32_t DynamicRAS;
  volatile uint32_t DynamicSREX;
  volatile uint32_t DynamicAPR;
  volatile uint32_t DynamicDAL;
  volatile uint32_t DynamicWR;
  volatile uint32_t DynamicRC;
  volatile uint32_t DynamicRFC;
  volatile uint32_t DynamicXSR;
  volatile uint32_t DynamicRRD;
  volatile uint32_t DynamicMRD;
       uint32_t RESERVED2[9];
  volatile uint32_t StaticExtendedWait;
       uint32_t RESERVED3[31];
  volatile uint32_t DynamicConfig0;
  volatile uint32_t DynamicRasCas0;
       uint32_t RESERVED4[6];
  volatile uint32_t DynamicConfig1;
  volatile uint32_t DynamicRasCas1;
       uint32_t RESERVED5[6];
  volatile uint32_t DynamicConfig2;
  volatile uint32_t DynamicRasCas2;
       uint32_t RESERVED6[6];
  volatile uint32_t DynamicConfig3;
  volatile uint32_t DynamicRasCas3;
       uint32_t RESERVED7[38];
  volatile uint32_t StaticConfig0;
  volatile uint32_t StaticWaitWen0;
  volatile uint32_t StaticWaitOen0;
  volatile uint32_t StaticWaitRd0;
  volatile uint32_t StaticWaitPage0;
  volatile uint32_t StaticWaitWr0;
  volatile uint32_t StaticWaitTurn0;
       uint32_t RESERVED8[1];
  volatile uint32_t StaticConfig1;
  volatile uint32_t StaticWaitWen1;
  volatile uint32_t StaticWaitOen1;
  volatile uint32_t StaticWaitRd1;
  volatile uint32_t StaticWaitPage1;
  volatile uint32_t StaticWaitWr1;
  volatile uint32_t StaticWaitTurn1;
       uint32_t RESERVED9[1];
  volatile uint32_t StaticConfig2;
  volatile uint32_t StaticWaitWen2;
  volatile uint32_t StaticWaitOen2;
  volatile uint32_t StaticWaitRd2;
  volatile uint32_t StaticWaitPage2;
  volatile uint32_t StaticWaitWr2;
  volatile uint32_t StaticWaitTurn2;
       uint32_t RESERVED10[1];
  volatile uint32_t StaticConfig3;
  volatile uint32_t StaticWaitWen3;
  volatile uint32_t StaticWaitOen3;
  volatile uint32_t StaticWaitRd3;
  volatile uint32_t StaticWaitPage3;
  volatile uint32_t StaticWaitWr3;
  volatile uint32_t StaticWaitTurn3;
} LPC_EMC_TypeDef;

 
typedef struct
{
  volatile uint32_t MODE;
  volatile uint32_t SEED;
  union {
  volatile const  uint32_t SUM;
  struct {
  volatile  uint32_t DATA;
  } WR_DATA_DWORD;
  
  struct {
  volatile  uint16_t DATA;
       uint16_t RESERVED;
  }WR_DATA_WORD;
  
  struct {
  volatile  uint8_t  DATA;
       uint8_t  RESERVED[3];
  	}WR_DATA_BYTE;
  };
} LPC_CRC_TypeDef;

 
typedef struct
{
  volatile uint32_t CMD;			 
  volatile uint32_t ADDR;
  volatile uint32_t WDATA;
  volatile uint32_t RDATA;
  volatile uint32_t WSTATE;			 
  volatile uint32_t CLKDIV;
  volatile uint32_t PWRDWN;			 
       uint32_t RESERVED0[975];
  volatile uint32_t INT_CLR_ENABLE;	 
  volatile uint32_t INT_SET_ENABLE;
  volatile uint32_t INT_STATUS;		 
  volatile uint32_t INT_ENABLE;
  volatile uint32_t INT_CLR_STATUS;
  volatile uint32_t INT_SET_STATUS;
} LPC_EEPROM_TypeDef;


#pragma no_anon_unions


 
 
 
 
#line 1332 ".\\Common\\LPC177x_8x.h"

 
#line 1353 ".\\Common\\LPC177x_8x.h"

 
#line 1369 ".\\Common\\LPC177x_8x.h"

 
#line 1391 ".\\Common\\LPC177x_8x.h"




 
 
 
#line 1452 ".\\Common\\LPC177x_8x.h"

#line 16 "USB\\usbh_ohci_lpc177x_8x.c"


 











































#line 69 "USB\\usbh_ohci_lpc177x_8x.c"




 

#line 81 "USB\\usbh_ohci_lpc177x_8x.c"

#line 89 "USB\\usbh_ohci_lpc177x_8x.c"

U32     usbh_ohci_hcca  [64]                        __attribute__((at(0x20000000)));
U32     usbh_ohci_ed    [3<<2]       __attribute__((at((0x20000000+256))));
U32     usbh_ohci_td    [1<<2]       __attribute__((at(((0x20000000+256)+(3<<4)))));



U32     usbh_ohci_mpool [((256 + 3 * 16 + 1 * 16 + 4096 + 1 * 8 + 4 )-(((((0x20000000+256)+(3<<4))+(1<<4))+(0<<5))-0x20000000))>>2] __attribute__((at(((((0x20000000+256)+(3<<4))+(1<<4))+(0<<5)))));
U32     usbh_ohci_tdurb [(1+0)<<1];

static U32 calDelay = 0;


 

void usbh_ohci_hw_get_capabilities (USBH_HCI_CAP *cap);
void usbh_ohci_hw_delay_ms         (U32 ms);
void usbh_ohci_hw_reg_wr           (U32 reg_ofs, U32 val);
U32  usbh_ohci_hw_reg_rd           (U32 reg_ofs);
BOOL usbh_ohci_hw_pins_config      (BOOL on);
BOOL usbh_ohci_hw_init             (BOOL on);
BOOL usbh_ohci_hw_port_power       (BOOL on);
BOOL usbh_ohci_hw_irq_en           (BOOL on);


 

USBH_HWD_OHCI usbh0_hwd_ohci = {         
  0x00000002,                       
  3,                      
  1,                      
  0,                     
  (U32 *) &usbh_ohci_hcca,               
  (U32 *) &usbh_ohci_ed,                 
  (U32 *) &usbh_ohci_td,                 



  ((void *) 0),

  (U32 *) &usbh_ohci_tdurb,              
  usbh_ohci_hw_get_capabilities,         
  usbh_ohci_hw_delay_ms,                 
  usbh_ohci_hw_reg_wr,                   
  usbh_ohci_hw_reg_rd,                   
  usbh_ohci_hw_pins_config,              
  usbh_ohci_hw_init,                     
  usbh_ohci_hw_port_power,               
  usbh_ohci_hw_irq_en                    
};


 

extern void USBH_OHCI_IRQHandler (void);


 







 

BOOL usbh_i2c_init (BOOL on) {
  S32 tout;

  if (on) {
     
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_28 = 1;                
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_29 = 1;                

    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkCtrl |= (1 << 2);     

    tout = 10100;
    while (!(((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkSt & (1 << 2))) {          
      if (tout-- <= 100) {
        if (tout == 0)
          return (0);
        usbh_ohci_hw_delay_ms (10);                    
      }
    }

    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_CTL   = 1 << 8;         
    tout = 10100;
    while (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_CTL & (1 << 8)) {              
      if (tout-- <= 100) {
        if (tout == 0)
          return (0);
        usbh_ohci_hw_delay_ms (10);                    
      }
    }

                                         
    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_CLKHI = 240;            
    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_CLKLO = 240;            
  } else {
     
    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_CLKHI = 0xB9;
    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_CLKLO = 0xB9;

    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkCtrl &=~(1 << 2);     

    tout = 10100;
    while (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkSt & (1 << 2)) {             
      if (tout-- <= 100) {
        if (tout == 0)
          return (0);
        usbh_ohci_hw_delay_ms (10);                    
      }
    }

     
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_28 = 0x30;
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_29 = 0x30;
  }

  return (1);
}









 

int usbh_i2c_read (U8 i2c_adr, U8 reg_adr) {
  S32 tout;

  ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_TX    = (      1 << 8) |  
                       (i2c_adr << 1) |  
                       (      0 << 0) ;  
  for (tout=1010; tout>=0; tout--) {     
    if (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_STS & (1 << 11)) break;       
    if (tout ==  0) return (-1);
    if (tout <= 10) usbh_ohci_hw_delay_ms (10);
  }
  ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_TX    =  reg_adr;         
  for (tout=1010; tout>=0; tout--) {     
    if (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_STS & (1 << 11)) break;       
    if (tout ==  0) return (-1);
    if (tout <= 10) usbh_ohci_hw_delay_ms (10);
  }
  ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_TX    = (      1 << 8) |  
                       (i2c_adr << 1) |  
                       (      1 << 0) ;  
  ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_TX    = (      1 << 9) |  
                        0x55;            
  for (tout=1010; tout>=0; tout--) {     
    if (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_STS & (1 << 0)) break;        
    if (tout ==  0) return (-1);
    if (tout <= 10) usbh_ohci_hw_delay_ms (10);
  }
  for (tout=1010; tout>=0; tout--) {     
    if (!(((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_STS & (1 << 5))) break;     
    if (tout ==  0) return (-1);
    if (tout <= 10) usbh_ohci_hw_delay_ms (10);
  }
  return (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_RX & 0xFF);
}










 

int usbh_i2c_write (U8 i2c_adr, U8 reg_adr, U8 reg_val) {
  S32 tout;

  ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_TX    = (      1 << 8) |  
                       (i2c_adr << 1) |  
                       (      0 << 0) ;  
  for (tout=1010; tout>=0; tout--) {     
    if (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_STS & (1 << 11)) break;       
    if (tout ==  0) return (0);
    if (tout <= 10) usbh_ohci_hw_delay_ms (10);
  }
  ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_TX    =  reg_adr;         
  for (tout=1010; tout>=0; tout--) {     
    if (((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_STS & (1 << 11)) break;       
    if (tout ==  0) return (0);
    if (tout <= 10) usbh_ohci_hw_delay_ms (10);
  }
  ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_TX    = (      1 << 9) |  
                        reg_val;         
  for (tout=1010; tout>=0; tout--) {     
    if (!(((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->I2C_STS & (1 << 5))) break;     
    if (tout ==  0) return (0);
    if (tout <= 10) usbh_ohci_hw_delay_ms (10);
  }
  return (1);
}


 








 

void usbh_ohci_hw_get_capabilities (USBH_HCI_CAP *cap) {
  cap->MultiPckt = 1;
  cap->MaxDataSz = 4096;
  cap->CtrlNAKs  = 1000;
  cap->BulkNAKs  = 1000000;	
}









 

void usbh_ohci_hw_delay_ms (U32 ms) {
  U32 cnt = 0, vals = 0, vale = 0;

start:
  if (!calDelay) {                       
    cnt = 1000;
    if (!(((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->CTRL & (1UL << 0))) {
                                         
      vals = 0xFFFFFF;
      ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->LOAD  = 0xFFFFFF;
      ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->VAL   = 0xFFFFFF;
      ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->CTRL  = (1UL << 2) | 
                       (1UL << 0);
    } else {
      vals = ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->VAL;               
    }
  } else {
    cnt = ms * calDelay;
  }

  while (cnt--);

  if (!calDelay) {                       
    vale = ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->VAL;                 
    if (vale >= vals)                    
      vals += ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->LOAD;
    SystemCoreClockUpdate();             
    calDelay = SystemCoreClock / (vals - vale);    
    if (vals == 0xFFFFFF)                
      ((SysTick_Type *) ((0xE000E000UL) + 0x0010UL) )->CTRL  = 0;
    goto start;
  }
}









 

void usbh_ohci_hw_reg_wr (U32 reg_ofs, U32 val) {
  *((U32 *)(0x2008C000 + reg_ofs)) = val;
}








 

U32 usbh_ohci_hw_reg_rd (U32 reg_ofs) {
  return (*((U32 *)(0x2008C000 + reg_ofs)));
}








 

BOOL usbh_ohci_hw_pins_config (BOOL on) {

  if (on) {
     
    
    
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_31 = 1;                
                                         

    


    
    
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_12 = 1 | (1 << 7);     
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_13 = 1 | (1 << 7);     
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_14 = 1;                
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_30 = 1 | (1 << 7);     
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_31 = 1 | (1 << 7);     
  } else {
     
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_12 = 0xD0;
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_13 = 0xD0;
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_14 = 0x30;
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_30 = 0xD0;
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P1_31 = 0xD0;

    


    

    
    
    ((LPC_IOCON_TypeDef *) ((0x40000000UL) + 0x2C000) )->P0_31 = 0;
  }

  return (1);
}








 

BOOL usbh_ohci_hw_init (BOOL on) {
  S32 tout;

  if (on) {
    usbh_ohci_hw_delay_ms (10);          

     
    if (!usbh_mem_init(0, (U32 *)&usbh_ohci_mpool, sizeof(usbh_ohci_mpool)))
      return (0);

    ((LPC_SC_TypeDef *) ((0x40080000UL) + 0x7C000) )->PCONP       |=  (1UL << 31); 
    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkCtrl |=  0x19;        

    tout = 10100;
    while (!((((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkSt & 0x19) == 0x19)) {    
      if (tout-- <= 100) {
        if (tout == 0)
          return (0);
        usbh_ohci_hw_delay_ms (10);                    
      }
    }

		
    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->StCtrl   =  0x01;           

     
    usbh_i2c_init(1);
    switch (usbh_i2c_read (0x2D, 0x00) | (usbh_i2c_read (0x2D, 0x01) << 8)) {
      case 0x058D:                       
         
        usbh_i2c_write (0x2D, 0x06, 0x2C);
        break;
    }

    NVIC_SetPriority (USB_IRQn, 0);      
  } else {
    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->StCtrl     &= ~0x01;        

    ((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkCtrl &= ~0x1D;        

    for (tout = 100; ; tout--) {
      if ((((LPC_USB_TypeDef *) ((0x20080000UL) + 0x0C000) )->OTGClkSt & 0x1B) == 0) 
        break;
      if (!tout) 
        return (0);
    }

    ((LPC_SC_TypeDef *) ((0x40080000UL) + 0x7C000) )->PCONP       &= ~(1UL << 31); 
  }

  return (1);
}








 

BOOL usbh_ohci_hw_port_power (BOOL on) {

  return (1);
}








 

BOOL usbh_ohci_hw_irq_en (BOOL on) {

  if (on) {		
    NVIC_EnableIRQ  (USB_IRQn);          
  } else {
    NVIC_DisableIRQ (USB_IRQn);          
  }

  return (1);
}








 

void USB_IRQHandler (void) {
	
  USBH_OHCI_IRQHandler();
}
