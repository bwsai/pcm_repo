/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      	: Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     	: Beltway
*
* @file Filename 	   		: sntp.h
* @brief			   				: Controller Board
*
* @author			   				: Dnyaneshwar Kashid 
*
* @date Created		   		: November Tuesday, 2014  <Nov 25, 2014>
* @date Last Modified  	: November Tuesday, 2014  <Nov 25, 2014>
*
* @internal Change Log 	: <YYYY-MM-DD>
* @internal 		       	: 
* @internal 		       	:
*
*****************************************************************************/
#ifndef __SNTP_H
#define __SNTP_H

/** SNTP server port */
#ifndef SNTP_PORT
#define SNTP_PORT                   123
#endif
 
 
/** SNTP receive timeout - in milliseconds */
 
#ifndef SNTP_RECV_TIMEOUT
 
#define SNTP_RECV_TIMEOUT           3000
 
#endif
 

 
/** SNTP update delay - in milliseconds */
 
#ifndef SNTP_UPDATE_DELAY
 
#define SNTP_UPDATE_DELAY           60000
 
#endif
 

 
/** SNTP macro to change system time and/or the update the RTC clock */
 
#ifndef SNTP_SYSTEM_TIME
 
#define SNTP_SYSTEM_TIME(t)
 
#endif
 

 
/* SNTP protocol defines */
 
#define SNTP_MAX_DATA_LEN           48
 
#define SNTP_RCV_TIME_OFS           40 //32 by megha on 27/4/2017
 
#define SNTP_LI_NO_WARNING          0x00
 
#define SNTP_VERSION               (4/* NTP Version 4*/<<3)
 
#define SNTP_MODE_CLIENT            0x03
 
#define SNTP_MODE_SERVER            0x04
 
#define SNTP_MODE_BROADCAST         0x05
 
#define SNTP_MODE_MASK              0x07
 

 
/* number of seconds between 1900 and 1970 */
 
#define DIFF_SEC_1900_1970         (2208988800)

extern char utc_time[50],utc_time_disp[50]; 
extern U16 sntp_request(void);
extern U16 sntp_request1(void);


#endif /*SNTP*/
/*****************************************************************************
* End of file
*****************************************************************************/
