/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename       : sntp.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : November Monday, 2014  <Nov 25, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 26, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
/*============================================================================
* Include Header Files
*===========================================================================*/
#include <string.h>
#include <time.h>
#include <stdio.h>
#include "main.h"
#include "sntp.h"
#include "rtl.h"
//#include "mbport.h"
#include "RTOS_main.h"
#include "DebugLog.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define EVE_DNS  		0x1000 
#define EVE_CALLBACK 			0x2000 

/*============================================================================
* Private Data Types
*===========================================================================*/
/* Character variables section */
static U8   sntp_request_buf[SNTP_MAX_DATA_LEN];
static U8   sntp_response_buf[SNTP_MAX_DATA_LEN];
static U8   sntp_request_buf1[SNTP_MAX_DATA_LEN];
static U8   sntp_response_buf1[SNTP_MAX_DATA_LEN];
/* unsigned integer variables section */
static U16 usSntpClientRcvBufferLen,usSntpClientRcvBufferLen1;
unsigned char time_sync_fg;
unsigned int time_offset,delta,read_msec_cntr;
volatile unsigned int msec_cntr;
int calc_year;//,month,day,hour,min;
unsigned int calc_seconds;
/*============================================================================
*Public Variables
*===========================================================================*/

char utc_time[50],utc_time_disp[50]; 

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void init_timer2(void);
//void TIMER2_IRQHandler(void) ;
unsigned int  calc_curr_time_in_seconds(void);

/*============================================================================
* Function Implementation Section
*===========================================================================*/
static void dns_sntp_cbfunc (U8 event, U8 *ip) {
  switch (event) {
    case DNS_EVT_SUCCESS:
//      printf("IP Address: %d.%d.%d.%d\n",ip[0],ip[1],ip[2],ip[3]);
				SNTP_Server_IpAdr[0] = ip[0];
				SNTP_Server_IpAdr[1] = ip[1];
				SNTP_Server_IpAdr[2] = ip[2];
				SNTP_Server_IpAdr[3] = ip[3];
		os_evt_set(EVE_DNS,t_rtc_tick); 
		break;
    case DNS_EVT_NONAME:
  //    printf("Host name does not exist.\n");
      break;
    case DNS_EVT_TIMEOUT:
    //  printf("DNS Resolver Timeout expired, Host IP not resolved.\n");
      break;
    case DNS_EVT_ERROR:
   //   printf("DNS Resolver Error, check the host name, labels, etc.\n");
      break;
  }
}
static void dns_sntp_cbfunc1 (U8 event, U8 *ip) {
  switch (event) {
    case DNS_EVT_SUCCESS:
//      printf("IP Address: %d.%d.%d.%d\n",ip[0],ip[1],ip[2],ip[3]);
				SNTP_Server_IpAdr[0] = ip[0];
				SNTP_Server_IpAdr[1] = ip[1];
				SNTP_Server_IpAdr[2] = ip[2];
				SNTP_Server_IpAdr[3] = ip[3];
		os_evt_set(EVE_DNS,t_rtc_tick); 
		break;
    case DNS_EVT_NONAME:
  //    printf("Host name does not exist.\n");
      break;
    case DNS_EVT_TIMEOUT:
    //  printf("DNS Resolver Timeout expired, Host IP not resolved.\n");
      break;
    case DNS_EVT_ERROR:
   //   printf("DNS Resolver Error, check the host name, labels, etc.\n");
      break;
  }
}
/*****************************************************************************
* @note       Function name  : prvxUDPPortSNTPCallback
* @returns    returns        : 
* @param      arg1           : 
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 25 November 2014
* @brief      Description    : SNTP client socket call back function when a TCP event occurs
*                            : 
*                            : 
* @note       Notes          : None
*****************************************************************************/
static U16 prvxUDPPortSNTPCallback1(U8 socket, U8* remip, U16 port, U8 *ptr, U16 par)
{
	U16 retval = 0;
  //Check SNTP server response 
	if(( par <= SNTP_MAX_DATA_LEN) && (port == SNTP_PORT))
	{
		memcpy( sntp_response_buf1, ptr, par );
		usSntpClientRcvBufferLen1 = par;
		retval=1;
		os_evt_set(EVE_CALLBACK,t_rtc_tick); 
	}
  return retval;
}

static U16 prvxUDPPortSNTPCallback(U8 socket, U8* remip, U16 port, U8 *ptr, U16 par)
{
	U16 retval = 0;
  //Check SNTP server response 
	if(( par <= SNTP_MAX_DATA_LEN) && (port == SNTP_PORT))
	{
		memcpy( sntp_response_buf, ptr, par );
		usSntpClientRcvBufferLen = par;
		retval=1;
		os_evt_set(EVE_CALLBACK,t_rtc_tick); 
	}
  return retval;
}

/*****************************************************************************
* @note       Function name  : U16 sntp_request(void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 25 November 2014
* @brief      Description    : SNTP request
*                            : 
*                            : 
* @note       Notes          : None
*****************************************************************************/
U16 sntp_request(void)
{
	U8 sntp_sock;
  U8 res;
	U16 ret_val = 0;
	U32    sntp_server_address;
  U32    timestamp,ntoh_t1,ntoh_t2,hton_t1,ntoh_t1_sec,ntoh_t2_sec;
	time_t t,t1,t2;
	int time;
	U8 *send_buf;
	OS_RESULT  result;

	/* pool.ntp.org */
	SNTP_Server_IpAdr[0] = 213;
	SNTP_Server_IpAdr[1] = 161; 
	SNTP_Server_IpAdr[2] = 194;
	SNTP_Server_IpAdr[3] = 93;
	sntp_server_address = 0;
//	init_timer2();
	
	os_evt_clr(EVE_DNS,t_rtc_tick);
	
	if(DNS_RES_OK == (res = get_host_by_name ((U8*)&sntp_host_name,dns_sntp_cbfunc)))
	{
//		sntp_server_address =1; 
	}

	//os_dly_wait(2000);	
LPC_RTC->ALHOUR = 1;	
	result=os_evt_wait_or (EVE_DNS, 500);
LPC_RTC->ALHOUR = 2;	
	if(result == OS_R_EVT)
	{
		sntp_server_address = 1; 
	}	

  /* if we got a valid SNTP server address... */
	if(sntp_server_address!=0)
  {
		/* create new UDP socket for SNTP request */
		//sntp_sock = udp_get_socket( 0,(UDP_OPT_SEND_CS|UDP_OPT_CHK_CS ),prvxUDPPortSNTPCallback);
		sntp_sock = udp_get_socket( 0,0,prvxUDPPortSNTPCallback);
    if(sntp_sock != 0)
		{
			if(udp_open(sntp_sock,SNTP_PORT))
			{
					/* prepare SNTP request */
				memset(sntp_request_buf,0, sizeof(sntp_request_buf));
        sntp_request_buf[0] = SNTP_LI_NO_WARNING | SNTP_VERSION | SNTP_MODE_CLIENT;
				//length = strlen((char* )sntp_request_buf);
				/* send SNTP request to server */
				usSntpClientRcvBufferLen=0;
				os_evt_clr(EVE_CALLBACK,t_rtc_tick);
				send_buf = udp_get_buf (SNTP_MAX_DATA_LEN);
				memcpy (send_buf, sntp_request_buf,SNTP_MAX_DATA_LEN);
//----by megha for time sync in msec	
				
					msec_cntr=0;
					time_sync_fg = 0;					
					init_timer2();
//-------------			
	os_evt_clr(EVE_CALLBACK,t_rtc_tick);
				
				ret_val = udp_send (sntp_sock, SNTP_Server_IpAdr,SNTP_PORT,send_buf,SNTP_MAX_DATA_LEN);
			#ifdef USB_LOGGING
			
				Write_Debug_Logs_into_file(DEBUG_UDP_DATE_EVENT,send_buf,SNTP_MAX_DATA_LEN);
			#endif
				ret_val = 0; //Added by DK on 27 Feb 2015 ver1.07 
					/* receive SNTP server response */
				LPC_RTC->ALHOUR = 3;	
				result = os_evt_wait_or (EVE_CALLBACK, 500);
LPC_RTC->ALHOUR = 4;				
				if(result == OS_R_EVT)
				{	
			
					/* if this is a SNTP response... */
					if((usSntpClientRcvBufferLen > 0) && \
				   (((sntp_response_buf[0] & SNTP_MODE_MASK) == SNTP_MODE_SERVER) || \
					 ((sntp_response_buf[0] & SNTP_MODE_MASK) == SNTP_MODE_BROADCAST)))
					{
            /* extract GMT time from response */
            memcpy( &timestamp, (sntp_response_buf+SNTP_RCV_TIME_OFS), sizeof(timestamp));
						
            ntoh_t2_sec = t = (ntohl(timestamp) - DIFF_SEC_1900_1970);
//by megha for time sync	25/4/2017		

            memcpy( &timestamp, (sntp_response_buf+32), sizeof(timestamp));//64 bit of timestamp msb 32bits = sec, lsb 32bits = fraction of seconds in picoseconds
						
						ntoh_t1_sec	= ntohl(timestamp);				
            memcpy( &timestamp, (sntp_response_buf+36), sizeof(timestamp));
						ntoh_t1 = ntohl(timestamp) ;			
            memcpy( &timestamp, (sntp_response_buf+44), sizeof(timestamp));
						ntoh_t2 = ntohl(timestamp);
						if(ntoh_t1_sec == ntoh_t2_sec)
						{
							time_offset = (ntoh_t2 - ntoh_t1)/1000000000;
						}
						else
						{
							time_offset = ((1000000000000 - ntoh_t1)+ntoh_t2)/1000000000;
						}
						read_msec_cntr = msec_cntr;
						time_offset = (read_msec_cntr - time_offset)/2;
						msec_cntr = msec_cntr + time_offset;
						delta = msec_cntr - (ntoh_t2 - ntoh_t1);
//-----						
						memset(utc_time_disp, '\0',sizeof(utc_time_disp));
						sprintf(utc_time_disp,"%s",ctime(&t));
						strcat(utc_time_disp,"\r\n");
						
						//only for testing UTC to IST time 05:30 i.e. 5 * 3600 + 30 * 60 = 19800
						//t +=19800; //replace this by UTC conversion hr:min read from USB pen drive. 
						if(Lst_Sign == 0)  //plus 
						{
							t = t + (LstHour * 3600) + (LstMinutes * 60); 
						}
						else  //minus 
						{
							t = t - ((LstHour * 3600) + (LstMinutes * 60)); 
						}	
								
						if(dst_on_off == 1)  //DST on 
						{
							t += 3600; //advance clock by 1 hour/3600 seconds 
						}	
						// do time processing 
						memset(utc_time, '\0',sizeof(utc_time));
						sprintf(utc_time,"%s",ctime(&t));
						strcat(utc_time,"\r\n");
						update_rtc();
						ret_val = 1; //Added by DK on 27 Feb 2015 ver1.07 	
					}else ret_val=0;
				}
				udp_close(sntp_sock);  
				udp_release_socket(sntp_sock);
			}//end of if(udp_open(sntp_sock,10))
			else
			{
				udp_close(sntp_sock);  
				udp_release_socket(sntp_sock);
			} 
		} //end of if (sntp_sock != 0)
	} //end of if (sntp_server_address!=0)		
	
	return ret_val;
}

U16 sntp_request1(void)
{
	U8 sntp_sock;
  U8 res;
	U16 ret_val = 0;
	U32    sntp_server_address;
  U32    timestamp,ntoh_t1,ntoh_t2,hton_t1,ntoh_t1_sec,ntoh_t2_sec;
	time_t t,t1,t2;
	int time;
	U8 *send_buf;
	OS_RESULT  result;

	/* pool.ntp.org */
	SNTP_Server_IpAdr[0] = 213;
	SNTP_Server_IpAdr[1] = 161; 
	SNTP_Server_IpAdr[2] = 194;
	SNTP_Server_IpAdr[3] = 93;
	sntp_server_address = 0;
//	init_timer2();
	
	os_evt_clr(EVE_DNS,t_rtc_tick);
	
	if(DNS_RES_OK == (res = get_host_by_name ((U8*)&sntp_host_name,dns_sntp_cbfunc1)))
	{
//		sntp_server_address =1; 
	}

	//os_dly_wait(2000);	
LPC_RTC->ALHOUR = 5;	
	result=os_evt_wait_or (EVE_DNS, 500);
LPC_RTC->ALHOUR = 6;	
	if(result == OS_R_EVT)
	{
		sntp_server_address = 1; 
	}	

  /* if we got a valid SNTP server address... */
	if(sntp_server_address!=0)
  {
		/* create new UDP socket for SNTP request */
		//sntp_sock = udp_get_socket( 0,(UDP_OPT_SEND_CS|UDP_OPT_CHK_CS ),prvxUDPPortSNTPCallback);
		sntp_sock = udp_get_socket( 0,0,prvxUDPPortSNTPCallback1);
    if(sntp_sock != 0)
		{
			if(udp_open(sntp_sock,SNTP_PORT))
			{
					/* prepare SNTP request */
				memset(sntp_request_buf1,0, sizeof(sntp_request_buf1));
        sntp_request_buf1[0] = SNTP_LI_NO_WARNING | SNTP_VERSION | SNTP_MODE_CLIENT;
				//length = strlen((char* )sntp_request_buf);
				/* send SNTP request to server */
				usSntpClientRcvBufferLen1=0;
				os_evt_clr(EVE_CALLBACK,t_rtc_tick);
				send_buf = udp_get_buf (SNTP_MAX_DATA_LEN);
				memcpy (send_buf, sntp_request_buf1,SNTP_MAX_DATA_LEN);
				
	os_evt_clr(EVE_CALLBACK,t_rtc_tick);	
				ret_val = udp_send (sntp_sock, SNTP_Server_IpAdr,SNTP_PORT,send_buf,SNTP_MAX_DATA_LEN);
// 			#ifdef USB_LOGGING
// 			
// //				Write_Debug_Logs_into_file(DEBUG_UDP_DATE_EVENT,send_buf,SNTP_MAX_DATA_LEN);
// 			#endif
				ret_val = 0; //Added by DK on 27 Feb 2015 ver1.07 
					/* receive SNTP server response */
LPC_RTC->ALHOUR = 7;				
				result = os_evt_wait_or (EVE_CALLBACK, 500);
LPC_RTC->ALHOUR = 8;				
				if(result == OS_R_EVT)
				{	
			
					/* if this is a SNTP response... */
					if((usSntpClientRcvBufferLen1 > 0) && \
				   (((sntp_response_buf1[0] & SNTP_MODE_MASK) == SNTP_MODE_SERVER) || \
					 ((sntp_response_buf1[0] & SNTP_MODE_MASK) == SNTP_MODE_BROADCAST)))
					{
            /* extract GMT time from response */
            memcpy( &timestamp, (sntp_response_buf1+SNTP_RCV_TIME_OFS), sizeof(timestamp));
						
            t = (ntohl(timestamp) - DIFF_SEC_1900_1970);
					
						memset(utc_time_disp, '\0',sizeof(utc_time_disp));
						sprintf(utc_time_disp,"%s",ctime(&t));
						strcat(utc_time_disp,"\r\n");
						

						ret_val = 1; //Added by DK on 27 Feb 2015 ver1.07 	
					}else ret_val=0;
				}
				udp_close(sntp_sock);  
				udp_release_socket(sntp_sock);
			}//end of if(udp_open(sntp_sock,10))
			else
			{
				udp_close(sntp_sock);  
				udp_release_socket(sntp_sock);
			} 
		} //end of if (sntp_sock != 0)
	} //end of if (sntp_server_address!=0)		
	
	return ret_val;
}
// static U16 prvxUDPPortSNTPCallback1(U8 socket, U8* remip, U16 port, U8 *ptr, U16 par)
// {
// 	U16 retval = 0;
//   //Check SNTP server response 
// 	if(( par <= SNTP_MAX_DATA_LEN) && (port == SNTP_PORT))
// 	{
// 		memcpy( sntp_response_buf1, ptr, par );
// 		usSntpClientRcvBufferLen1 = par;
// 		retval=1;
// 		os_evt_set(0x0002,t_rtc_tick); 
// 	}
//   return retval;
// }





void init_timer2(void)
{
 LPC_SC->PCONP |= (1U << 22);	
  LPC_TIM2->TCR = 0x02;  // Reset timer
  LPC_TIM2->PR  = 0;  // ((SystemCoreClock/2)/1000000)Set prescaler to zero
  LPC_TIM2->MR0 = 1 * (__CPUCLK / (LPC_SC->PCLKSEL & 0x1F) / 1000 - 1);
  LPC_TIM2->MCR = 0x03;  // interrupt on match
  LPC_TIM2->TCR = 0x01;  // Start timer
	NVIC_EnableIRQ  (TIMER2_IRQn);	
}

void TIMER2_IRQHandler(void) 
{
    unsigned int isrMask;

    isrMask = LPC_TIM2->IR; 
		LPC_TIM2->IR = 1; /* Clear the Interrupt Bit */	
		if(msec_cntr < 1000)
		{
			msec_cntr++;
		}
		else
		{
			time_sync_fg = 1;
			LPC_TIM2->TCR = 0x00;
//			msec_cntr = 0;
			NVIC_DisableIRQ  (TIMER2_IRQn);
		}
}
unsigned int calc_curr_time_in_seconds(void)
{
	U32 hton_t2;
	time_t now;
  struct tm *tm1;

   now = LPC_RTC->HOUR;
	tm1 = gmtime (&now);
	
// 	calc_year = (Current_time.RTC_Year-1) - 1970;
 	calc_seconds = (((calc_year * 365)+(Current_time.RTC_Yday ))*24*60*60)+(Current_time.RTC_Hour*60*60)\
 																			+(Current_time.RTC_Min*60)+(Current_time.RTC_Sec);
// 	calc_seconds = calc_seconds;// + DIFF_SEC_1900_1970;
//	hton_t2 = htonl(calc_seconds);
 	return(calc_seconds);
}

 
