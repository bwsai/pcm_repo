/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : Emc_config.c
* @brief			         : Controller Board
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"                   /* LPC177x_8x definitions*/
#include "Emc_config.h"
#include "Ext_flash_low_level.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

//#define STATIC_RAM

/* Defines Section */
#define SDRAM_SIZE_MB    2
#define SDRAM_SIZE       (SDRAM_SIZE_MB * 1024 * 1024)
#define SDRAM_BASE_ADDR  0xA0000000
#define SRAM_BASE_ADDR   0x90000000

#define EMC_ADDR_HYST_EN 					0x21
#define EMC_DATA_HYST_EN 					0x21
#define EMC_WE_HYST_EN						0x21
/********************FLASH************************/
#define EMC_FLASH_CS0_HYST_EN  		0x21
#define EMC_FLSH_SRAM_OE_HYST_EN  0x21
#define EMC_FLSH_SRAM_WE_HYST_EN  0x21
#define BYTE_CTRL_FLSH_HYST_EN		0x20
#define RB_FLASH_HYST_EN					0x20
/**************************SRAM*******************/
#define EMC_SRAM_CS1_HYST_EN  		0x21
#define EMC_SRAM_BLS0_HYST_EN  		0x21
#define EMC_SRAM_BLS1_HYST_EN  		0x21
#define EMC_SRAM_OE_HYST_EN				0x21
/*********************SDRAM************************/
#define EMC_SDRAM_CS_HYST_EN			0x21
#define EMC_SDRAM_CLK_HYST_EN			0x21
#define EMC_SDRAM_CLE_HYST_EN			0x21
#define EMC_SDRAM_CAS_HYST_EN			0x21
#define EMC_SDRAM_RAS_HYST_EN			0x21
#define EMC_SDRAM_LDQM_HYST_EN		0x21
#define EMC_SDRAM_UDQM_HYST_EN		0x21
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

//static int TestSRAM(void);
static int TestSRAM_DRAM(void);
void DelayMs(U32 ms);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*********************************************************************
*
*       DelayMs()
*
* Function description
*   Starts a timer and waits for the given delay in ms.
*/
void DelayMs(U32 ms) {
  LPC_TIM1->TCR = 0x02;  // Reset timer
  LPC_TIM1->PR  = 0x00;  // Set prescaler to zero
  LPC_TIM1->MR0 = ms * (__CPUCLK / (LPC_SC->PCLKSEL & 0x1F) / 1000 - 1);
  LPC_TIM1->IR  = 0xFF;  // Reset all interrrupts
  LPC_TIM1->MCR = 0x04;  // Stop timer on match
  LPC_TIM1->TCR = 0x01;  // Start timer
  //
  // Wait until delay time has elapsed
  //
  while (LPC_TIM1->TCR & 1);
}

//*********************************************************************

/*********************************************************************
*
*       TestSRAM_DRAM()
*
* Function description
*   Simple SDRAM test for testing timing setup
*
* Return value
*   1: Error
*   0: O.K.
*/

static int TestSRAM_DRAM(void) 
{
//  U32 * pWriteLong;
  U16 * pWriteShort;
	
  U16            Data, Data1;
  U16            i;
  U16            j;
	
	//
  // Fill 16 bit wise
  //
	
	#ifdef STATIC_RAM	
		pWriteShort = (U16*)SRAM_BASE_ADDR;
		for (i = 0; i < 100; i++) 
		{
			for (j = 0; j < 8; j++) 
			{
				*pWriteShort =  j ;				
				pWriteShort++;
			}
		}
	#else
		 pWriteShort = (U16 *)SDRAM_BASE_ADDR;	
		 for (i = 0; i < 0x01; i++) 
		 {
				for (j = 0; j < 100; j++) 
				 {
					*pWriteShort++ = (i + j);
					*pWriteShort++ = (i + j) + 1;
				 }
		 }	
	#endif
		
	
  //
  // Verifying
  //
	
  #ifdef STATIC_RAM
		pWriteShort = (U16 *)SRAM_BASE_ADDR;
		for (i = 0; i < 100; i++)
		{
			for (j = 0; j < 8; j++) 
			{
				Data = *pWriteShort++;				
				Data1 = j;
				if (Data != Data1) 
				{
						return 1;
				}
			}
		}
	#else
		pWriteShort = (U16 *)SDRAM_BASE_ADDR;
		for (i = 0; i < 0x01 ; i++) 
		{
			for (j = 0; j < 100; j++)
			{
				Data = *pWriteShort++;
				Data1 = *pWriteShort++;
				if ((Data != (i+j)) |(Data1 != ((i + j) + 1)))
				{
						return 1;
				}
			}
		}
	#endif	
		
  return 0;  // O.K.
}


/*********************************************************************
*
*       _FindDelay()
*
* Function description:
*   Test for min./max. CMDDLY or FBCLKDLY values
*
* Parameter:
*   DelayType: 0 = CMDDLY, 1 = FBCLKDLY
*/
static void _FindDelay(int DelayType) 
{
  U32 Delay;
  U32 Min;
  U32 Max;
  U32 v;

  //
  // Init start values
  //
  Delay = 0x00;
  Min   = 0xFF;
  Max   = 0xFF;
  //
  // Test for DLY min./max. values
  //
  while (Delay < 32) {
    //
    // Setup new DLY value to test
    //
    if (DelayType == 0) {
      v                 = LPC_SC->EMCDLYCTL & ~0x001Ful;
      LPC_SC->EMCDLYCTL = v | Delay;
    } else {
      v                 = LPC_SC->EMCDLYCTL & ~0x1F00ul;
      LPC_SC->EMCDLYCTL = v | (Delay << 8);
    }
    //
    // Test configured DLY value and find out min./max. values that will work
    //
    if (TestSRAM_DRAM() == 0) {
      //
      // Test passed, remember min. DLY value if not done yet
      //
      if (Min == 0xFF) {
        Min = Delay;
      }
    } else {
      //
      // Test failed, if a min. value has been found before, remember the current value for max.
      //
      if (Min != 0xFF) {
        Max = Delay;
      }
    }
    Delay++;
  }
  //
  // Calc DLY value
  //
  if        (Max != 0xFF) {  // If we found a min. and max. value we use the average of the min. and max. values to get an optimal DQSIN delay
    Delay = (Min + Max) / 2;
  } else if (Min != 0xFF) {  // If we found only a min. value we use the average of the min. value and the longest DLY value to get an optimal DQSIN delay
    Delay = (Min + 0x1F) / 2;
  } else {                   // No working max. and/or min. values found
    while (1);  // Fatal error
  }
  //
  // Setup DLY value to work with
  //
  if (DelayType == 0) {
    v                 = LPC_SC->EMCDLYCTL & ~0x001Ful;
    LPC_SC->EMCDLYCTL = v | Delay;
  } else {
    v                 = LPC_SC->EMCDLYCTL & ~0x1F00ul;
    LPC_SC->EMCDLYCTL = v | (Delay << 8);
  }
}

/*********************************************************************
*
*       _CalibrateOsc()
*
* Function description:
*   Calibrate ring oscillator.
*
* Return value:
*   Current ring oscillator count
*/
static U32 _CalibrateOsc(void) 
{
  U32 Cnt;
  U32 v;
  U32 i;

  //
  // Init start values
  //
  Cnt = 0;
  //
  // Calibrate osc.
  //
  for (i = 0; i < 10; i++) {
    LPC_SC->EMCCAL = (1 << 14);     // Start calibration
    v = LPC_SC->EMCCAL;
    while ((v & (1 << 15)) == 0) {  // Wait for calibration done
      v = LPC_SC->EMCCAL;
    }
    Cnt += (v & 0xFF);
  }
  return (Cnt / 10);
}

/*********************************************************************
*
*       _AdjustEMCTiming()
*
* Function description:
*   Adjust timings for EMC.
*/

static void _AdjustEMCTiming(U32 Delay) 
{
  U32 FBClkDly;
  U32 FBDelay;
  U32 CmdDly;
  U32 v;

  FBDelay           = _CalibrateOsc();
  v                 = LPC_SC->EMCDLYCTL;
  CmdDly            = ((v &  0x001Ful) * Delay / FBDelay) & 0x1F;
  FBClkDly          = ((v &  0x1F00ul) * Delay / FBDelay) & 0x1F00;
  LPC_SC->EMCDLYCTL =  (v & ~0x1F1Ful) | FBClkDly | CmdDly;
}

/*********************************************************************
*
*       _EMC_Init()
*
*  Purpose:
*    Initializes external memory controller for SDRAM
*/

void _EMC_Init(void) 
{
	  U32 CmdDly;
		U32 Dummy;
		//U8 i;
		
		LPC_SC->PCONP     |= (1 << 11);   // Turn on EMC peripheral clock
		LPC_SC->EMCDLYCTL  = 0x00001010;
		LPC_EMC->Control   = 0x0001;      // EMC enable, Normal memory map
		LPC_EMC->Config    = 0;						// Little-endian mode, CCLK: CLKOUT ratio: = 1:1(default)
		//
		// Port init
		//
		LPC_IOCON->P3_0  |= EMC_DATA_HYST_EN;  // D0
		LPC_IOCON->P3_1  |= EMC_DATA_HYST_EN;  // D1
		LPC_IOCON->P3_2  |= EMC_DATA_HYST_EN;  // D2
		LPC_IOCON->P3_3  |= EMC_DATA_HYST_EN;  // D3
		LPC_IOCON->P3_4  |= EMC_DATA_HYST_EN;  // D4
		LPC_IOCON->P3_5  |= EMC_DATA_HYST_EN;  // D5
		LPC_IOCON->P3_6  |= EMC_DATA_HYST_EN;  // D6
		LPC_IOCON->P3_7  |= EMC_DATA_HYST_EN;  // D7
		LPC_IOCON->P3_8  |= EMC_DATA_HYST_EN;  // D8
		LPC_IOCON->P3_9  |= EMC_DATA_HYST_EN;  // D9
		LPC_IOCON->P3_10 |= EMC_DATA_HYST_EN;  // D10
		LPC_IOCON->P3_11 |= EMC_DATA_HYST_EN;  // D11
		LPC_IOCON->P3_12 |= EMC_DATA_HYST_EN;  // D12
		LPC_IOCON->P3_13 |= EMC_DATA_HYST_EN;  // D13
		LPC_IOCON->P3_14 |= EMC_DATA_HYST_EN;  // D14
		LPC_IOCON->P3_15 |= EMC_DATA_HYST_EN;  // D15
		
		LPC_IOCON->P4_0  |= EMC_ADDR_HYST_EN;  // A0
		LPC_IOCON->P4_1  |= EMC_ADDR_HYST_EN;  // A1
		LPC_IOCON->P4_2  |= EMC_ADDR_HYST_EN;  // A2
		LPC_IOCON->P4_3  |= EMC_ADDR_HYST_EN;  // A3
		LPC_IOCON->P4_4  |= EMC_ADDR_HYST_EN;  // A4
		LPC_IOCON->P4_5  |= EMC_ADDR_HYST_EN;  // A5
		LPC_IOCON->P4_6  |= EMC_ADDR_HYST_EN;  // A6
		LPC_IOCON->P4_7  |= EMC_ADDR_HYST_EN;  // A7
		LPC_IOCON->P4_8  |= EMC_ADDR_HYST_EN;  // A8
		LPC_IOCON->P4_9  |= EMC_ADDR_HYST_EN;  // A9
		LPC_IOCON->P4_10 |= EMC_ADDR_HYST_EN;  // A10
		LPC_IOCON->P4_11 |= EMC_ADDR_HYST_EN;  // A11
		LPC_IOCON->P4_12 |= EMC_ADDR_HYST_EN;  // A12
		LPC_IOCON->P4_13 |= EMC_ADDR_HYST_EN;  // A13
		LPC_IOCON->P4_14 |= EMC_ADDR_HYST_EN;  // A14
		LPC_IOCON->P4_15 |= EMC_ADDR_HYST_EN;  // A15
		LPC_IOCON->P4_16 |= EMC_ADDR_HYST_EN;  // A16
		LPC_IOCON->P4_17 |= EMC_ADDR_HYST_EN;  // A17
		LPC_IOCON->P4_18 |= EMC_ADDR_HYST_EN;  // A18
		LPC_IOCON->P4_19 |= EMC_ADDR_HYST_EN;  // A19
		LPC_IOCON->P4_20 |= EMC_ADDR_HYST_EN;  // A20
		LPC_IOCON->P4_21 |= EMC_ADDR_HYST_EN;  // A21
		LPC_IOCON->P4_22 |= EMC_ADDR_HYST_EN;  // A22				//by megha on 7/12/2016 to run source code with integrator bootloader ver11.
		LPC_IOCON->P4_24 |= EMC_FLSH_SRAM_OE_HYST_EN; // OE
		LPC_IOCON->P4_25 |= EMC_FLSH_SRAM_WE_HYST_EN;  // WE
  
		#ifndef STATIC_RAM
		//External SDRAM Config
		LPC_IOCON->P2_20 |= 1;  // EMC_DYCS0 - SDRAM_CS
		LPC_IOCON->P2_16 |= 1;  // CAS			- SDRAM_CAS
		LPC_IOCON->P2_17 |= 1;  // RAS			- SDRAM_RAS
		LPC_IOCON->P2_18 |= 1;  // CLKOUT0 - SDRAM_CLK 
		LPC_IOCON->P2_24 |= 1;  // CKEOUT0 - SDRAM_CLE  
		LPC_IOCON->P2_28 |= 1;  // DQMOUT0 - SDRAM_LDQM
		LPC_IOCON->P2_29 |= 1;  // DQMOUT1 - SDRAM_UDQM
		
		//********************************************************************************************
		// Setup EMC config for SDRAM, timings for 60MHz bus
		//
		LPC_EMC->DynamicConfig0    = 0x00000080;	//0x00000080;  // 16MB, rOW-Bank-column, 2 banks, 11 rows, 8 columns, buffers disabled
		LPC_EMC->DynamicRasCas0    = 0x00000202;  // 2 RAS, 2 CAS latency */
		LPC_EMC->DynamicReadConfig = 0x00000001;  // Command delayed strategy, using EMCCLKDELAY
		LPC_EMC->DynamicRP         = 0x00000002;  // n + 1 clock cycles - tRP Command Period (PRE to ACT) 3 clock cycles
		LPC_EMC->DynamicRAS        = 0x00000005;  // n + 1 clock cycles - tRAS Command Period (ACT to PRE) 6 clock cycles
		LPC_EMC->DynamicSREX       = 0x00000000;  // n + 1 clock cycles - Self-Refresh-Cycle dig(pg 40) exit-sel-refresh-mode = 1clk cycle
		LPC_EMC->DynamicAPR        = 0x00000000;  // n + 1 clock cycles - Command Period (PRE to ACT) 16 ns(minimum) 1 clk cycle
		LPC_EMC->DynamicDAL        = 0x00000005;  // n     clock cycles - Input Data To Active / Refresh CAS Latency(3) 2CLK+tRP ns = 5clk cycle
		LPC_EMC->DynamicWR         = 0x00000001;  // n + 1 clock cycles - tDPL Input Data To Precharge Command Delay Time 2 clk cycle
		LPC_EMC->DynamicRC         = 0x00000008;  // n + 1 clock cycles - tRC Command Period (REF to REF / ACT to ACT) 9 clk cycles
		LPC_EMC->DynamicRFC        = 0x00000008;  // n + 1 clock cycles	- tRC Command Period (REF to REF / ACT to ACT) 9 clk cycles
		LPC_EMC->DynamicXSR        = 0x00000000;  // n + 1 clock cycles - Self-Refresh-Cycle dig(pg 40) exit-sel-refresh-mode = 1clk cycle
		LPC_EMC->DynamicRRD        = 0x00000001;  // n + 1 clock cycles - tRRD Command Period (ACT[0] to ACT [1]) 2 clk cycles
		LPC_EMC->DynamicMRD        = 0x00000001;  // n + 1 clock cycles - tMCD Mode Register Set To Command Delay Time 2 clk cycles
		
		DelayMs(10);
		
		//for (i = 0; i < 0x80; i++);               // Wait 128 AHB clock cycles
		LPC_EMC->DynamicControl    = 0x00000183;  // Issue NOP command -NO CHANGE
		
		DelayMs(10);
		//for (i = 0; i < 0x80; i++);               // Wait 128 AHB clock cycles
		LPC_EMC->DynamicControl    = 0x00000103;  // Issue Precharge -ALL command-NO CHANGE
		LPC_EMC->DynamicRefresh    = 0x00000002;/*0x000003A9;*/  // n * 16 clock cycles
		
		DelayMs(10);
		//for (i = 0; i < 0x80; i++);               // Wait 128 AHB clock cycles
		LPC_EMC->DynamicRefresh    = 0x00000038;  // n * 16 clock cycles tREF Refresh Cycle Time (2048) � 32ms ->
		
		//
		// Init SDRAM
		//
		
		LPC_EMC->DynamicControl = 0x00000083;      // Issue MODE command-NO CHANGE
	
		// 8 burst, 2 CAS latency, sequential, left shift by (column bits + 1 bank )-for RBC
		Dummy = *((volatile U32*)(SDRAM_BASE_ADDR | (0x23 << (10))));
		Dummy = Dummy;
		
		DelayMs(10);
		//for (i = 0; i < 0x80; i++);               // Wait 128 AHB clock cycles
		LPC_EMC->DynamicControl = 0x00000000;     // Issue NORMAL command-NO CHANGE
		
		DelayMs(10);
		//for (i = 0; i < 0x80; i++);               // Wait 128 AHB clock cycles
		LPC_EMC->DynamicConfig0 = 0x00080080;     // 16MB,Bank-row-column , 2 banks, 11 rows, 8 columns, buffers (re-)enabled
		
		//
		// Auto calibrate timings
		//
		CmdDly = _CalibrateOsc();
		//
		// Find best delay values
		//
		_FindDelay(0);  // EMCDLY
		_FindDelay(1);  // FBCLKDLY
		_AdjustEMCTiming(CmdDly);
		
		//Test SDRAM for writing 
		//TestSRAM_DRAM();
		
	#else //External static RAM configuration
		LPC_IOCON->P4_24 |= EMC_SRAM_OE_HYST_EN;  // OE
		LPC_IOCON->P4_31 |= EMC_SRAM_CS1_HYST_EN;  // CS1
		LPC_IOCON->P4_26 |= EMC_SRAM_BLS0_HYST_EN;  // BLS0
		LPC_IOCON->P4_27 |= EMC_SRAM_BLS1_HYST_EN;  // BLS1

		//***************************************************************************************
		// Setup EMC config for SRAM, timings for 60MHz bus
		//
		LPC_EMC->StaticConfig1    = 0x00000081;  		// MW = 16bit, /cs = Active low, Buffer disable, Write protect = disable
		//_wait();
		LPC_EMC->StaticExtendedWait = 0x00000004;
		LPC_EMC->StaticWaitWen1		= 	0x00000001;   //one clk delay between /cs and write enable
		//_wait();
		LPC_EMC->StaticWaitOen1		=  0x00000000;		//no delay between c/s and output enable
		//_wait();
		LPC_EMC->StaticWaitRd1		= 	0x00000004;

		LPC_EMC->StaticWaitPage1	=  0x00000000;
		LPC_EMC->StaticWaitWr1		= 	0x00000004;

		LPC_EMC->StaticConfig1    = 0x00000081;  		// MW = 16bit, /cs = Active low, Buffer disable, Write protect = disable
		
		//Test SRAM for writing
		//TestSRAM_DRAM();
  #endif	
		
		//**********Configuration settings for external Flash ********************************
		LPC_IOCON->P4_30 |= EMC_FLASH_CS0_HYST_EN;  // CS0	
		LPC_IOCON->P3_31 |= BYTE_CTRL_FLSH_HYST_EN;  // /BYTE
		LPC_IOCON->P3_29 |= RB_FLASH_HYST_EN;  // /R/B
		
		LPC_GPIO3->DIR |=  (1UL << 31) ;
		LPC_GPIO3->SET = 1UL<<31;
		
		LPC_GPIO3->DIR &=  ~(1UL << RB_FLASH_PIN) ;
		
		LPC_EMC->StaticConfig0   = 0x00000081;
		LPC_EMC->StaticWaitWen0  = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
		LPC_EMC->StaticWaitOen0  = 0x00000003; /* ( n ) -> 0 clock cycles */
		LPC_EMC->StaticWaitRd0   = 0x00000006; /* ( n + 1 ) -> 7 clock cycles */
		LPC_EMC->StaticWaitPage0 = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
		LPC_EMC->StaticWaitWr0   = 0x00000005; /* ( n + 2 ) -> 7 clock cycles */
		LPC_EMC->StaticWaitTurn0 = 0x00000003; /* ( n + 1 ) -> 4 clock cycles */
		
		#ifdef EXT_FLASH
			if (__TRUE == ExtFlash_Init()) /*Flash is present and valid*/
			{
				chip_info.flash_valid = __TRUE;
			}
			else
			{
				chip_info.flash_valid = __FALSE;
			}
	  #endif
}
/*****************************************************************************
* End of file
*****************************************************************************/
