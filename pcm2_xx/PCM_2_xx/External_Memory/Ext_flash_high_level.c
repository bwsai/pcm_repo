/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Run_mode_screen.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012  
* @date Last Modified  : November, 2012  
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            : 
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stddef.h>
#include "Global_ex.h"
#include "Ext_Data_flag.h"
#include "LPC177x_8x.h"
#include "lpc177x_8x_iap.h"
#include "../gui/Application/Screen_global_ex.h"
#include "Ext_flash_high_level.h"
#include "Ext_flash_low_level.h"
#include "File_update.h"

#include "../gui/LCDConf.h"

#include "../gui/Application/Screen_data_enum.h"
#include "EEPROM_high_level.h"
#include "RTOS_main.h"
#include "../mvt/MVT.h"

#include "Main.h"
#include "I2C_driver.h"


#ifdef EXT_FLASH
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
BOOL Flash_Write_In_progress = 0; //Added on 27 April 2016  
U8 g_restore_prog = 0;
U8 g_Q_addition_prog ;
U16 g_last_periodic_id;
float g_old_fw_ver;
float g_current_fw_ver;
/* unsigned integer variables section */
//PVK - 11 Jan 2015
U16	gTotals_Calculated_CRC = 0;
U16 Record_no;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
	FLASH_LOG_STRUCTURE Flash_log_data;
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */
const char GUI_PARAM_fixDATAstring[] __attribute__((at(FLASH_BASE_ADDRESS))) =
	{
		0xFF, 0xFF,0x00, 0x00
	};
// Below constant array is stored in last sector of External memory to display firmware version Number in HEX File 	

static const unsigned char crc_table[512] =
{
	0x00, 0x00, 0xc0, 0xc1, 0xc1, 0x81, 0x01, 0x40, 0xc3, 0x01, 0x03, 0xc0, 0x02, 0x80, 0xc2, 0x41,
	0xc6, 0x01, 0x06, 0xc0, 0x07, 0x80, 0xc7, 0x41, 0x05, 0x00, 0xc5, 0xc1, 0xc4, 0x81, 0x04, 0x40,
	0xcc, 0x01, 0x0c, 0xc0, 0x0d, 0x80, 0xcd, 0x41, 0x0f, 0x00, 0xcf, 0xc1, 0xce, 0x81, 0x0e, 0x40,
	0x0a, 0x00, 0xca, 0xc1, 0xcb, 0x81, 0x0b, 0x40, 0xc9, 0x01, 0x09, 0xc0, 0x08, 0x80, 0xc8, 0x41,
	0xd8, 0x01, 0x18, 0xc0, 0x19, 0x80, 0xd9, 0x41, 0x1b, 0x00, 0xdb, 0xc1, 0xda, 0x81, 0x1a, 0x40,
	0x1e, 0x00, 0xde, 0xc1, 0xdf, 0x81, 0x1f, 0x40, 0xdd, 0x01, 0x1d, 0xc0, 0x1c, 0x80, 0xdc, 0x41,
	0x14, 0x00, 0xd4, 0xc1, 0xd5, 0x81, 0x15, 0x40, 0xd7, 0x01, 0x17, 0xc0, 0x16, 0x80, 0xd6, 0x41,
	0xd2, 0x01, 0x12, 0xc0, 0x13, 0x80, 0xd3, 0x41, 0x11, 0x00, 0xd1, 0xc1, 0xd0, 0x81, 0x10, 0x40,
	0xf0, 0x01, 0x30, 0xc0, 0x31, 0x80, 0xf1, 0x41, 0x33, 0x00, 0xf3, 0xc1, 0xf2, 0x81, 0x32, 0x40,
	0x36, 0x00, 0xf6, 0xc1, 0xf7, 0x81, 0x37, 0x40, 0xf5, 0x01, 0x35, 0xc0, 0x34, 0x80, 0xf4, 0x41,
	0x3c, 0x00, 0xfc, 0xc1, 0xfd, 0x81, 0x3d, 0x40, 0xff, 0x01, 0x3f, 0xc0, 0x3e, 0x80, 0xfe, 0x41,
	0xfa, 0x01, 0x3a, 0xc0, 0x3b, 0x80, 0xfb, 0x41, 0x39, 0x00, 0xf9, 0xc1, 0xf8, 0x81, 0x38, 0x40,
	0x28, 0x00, 0xe8, 0xc1, 0xe9, 0x81, 0x29, 0x40, 0xeb, 0x01, 0x2b, 0xc0, 0x2a, 0x80, 0xea, 0x41,
	0xee, 0x01, 0x2e, 0xc0, 0x2f, 0x80, 0xef, 0x41, 0x2d, 0x00, 0xed, 0xc1, 0xec, 0x81, 0x2c, 0x40,
	0xe4, 0x01, 0x24, 0xc0, 0x25, 0x80, 0xe5, 0x41, 0x27, 0x00, 0xe7, 0xc1, 0xe6, 0x81, 0x26, 0x40,
	0x22, 0x00, 0xe2, 0xc1, 0xe3, 0x81, 0x23, 0x40, 0xe1, 0x01, 0x21, 0xc0, 0x20, 0x80, 0xe0, 0x41,
	0xa0, 0x01, 0x60, 0xc0, 0x61, 0x80, 0xa1, 0x41, 0x63, 0x00, 0xa3, 0xc1, 0xa2, 0x81, 0x62, 0x40,
	0x66, 0x00, 0xa6, 0xc1, 0xa7, 0x81, 0x67, 0x40, 0xa5, 0x01, 0x65, 0xc0, 0x64, 0x80, 0xa4, 0x41,
	0x6c, 0x00, 0xac, 0xc1, 0xad, 0x81, 0x6d, 0x40, 0xaf, 0x01, 0x6f, 0xc0, 0x6e, 0x80, 0xae, 0x41,
	0xaa, 0x01, 0x6a, 0xc0, 0x6b, 0x80, 0xab, 0x41, 0x69, 0x00, 0xa9, 0xc1, 0xa8, 0x81, 0x68, 0x40,
	0x78, 0x00, 0xb8, 0xc1, 0xb9, 0x81, 0x79, 0x40, 0xbb, 0x01, 0x7b, 0xc0, 0x7a, 0x80, 0xba, 0x41,
	0xbe, 0x01, 0x7e, 0xc0, 0x7f, 0x80, 0xbf, 0x41, 0x7d, 0x00, 0xbd, 0xc1, 0xbc, 0x81, 0x7c, 0x40,
	0xb4, 0x01, 0x74, 0xc0, 0x75, 0x80, 0xb5, 0x41, 0x77, 0x00, 0xb7, 0xc1, 0xb6, 0x81, 0x76, 0x40,
	0x72, 0x00, 0xb2, 0xc1, 0xb3, 0x81, 0x73, 0x40, 0xb1, 0x01, 0x71, 0xc0, 0x70, 0x80, 0xb0, 0x41,
	0x50, 0x00, 0x90, 0xc1, 0x91, 0x81, 0x51, 0x40, 0x93, 0x01, 0x53, 0xc0, 0x52, 0x80, 0x92, 0x41,
	0x96, 0x01, 0x56, 0xc0, 0x57, 0x80, 0x97, 0x41, 0x55, 0x00, 0x95, 0xc1, 0x94, 0x81, 0x54, 0x40,
	0x9c, 0x01, 0x5c, 0xc0, 0x5d, 0x80, 0x9d, 0x41, 0x5f, 0x00, 0x9f, 0xc1, 0x9e, 0x81, 0x5e, 0x40,
	0x5a, 0x00, 0x9a, 0xc1, 0x9b, 0x81, 0x5b, 0x40, 0x99, 0x01, 0x59, 0xc0, 0x58, 0x80, 0x98, 0x41,
	0x88, 0x01, 0x48, 0xc0, 0x49, 0x80, 0x89, 0x41, 0x4b, 0x00, 0x8b, 0xc1, 0x8a, 0x81, 0x4a, 0x40,
	0x4e, 0x00, 0x8e, 0xc1, 0x8f, 0x81, 0x4f, 0x40, 0x8d, 0x01, 0x4d, 0xc0, 0x4c, 0x80, 0x8c, 0x41,
	0x44, 0x00, 0x84, 0xc1, 0x85, 0x81, 0x45, 0x40, 0x87, 0x01, 0x47, 0xc0, 0x46, 0x80, 0x86, 0x41,
	0x82, 0x01, 0x42, 0xc0, 0x43, 0x80, 0x83, 0x41, 0x41, 0x00, 0x81, 0xc1, 0x80, 0x81, 0x40, 0x40
};

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
	U16 Log_restore_buffer[70];
  U16 WRstatus;
	U32 flash_para_addr;
	U16 flash_para_record_no;
	U16 record_no = 0;
	static U16 g_Clr_wt_CRC = 0xFFFF;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void EEP_FL_create_backup (void);
void CalcCRC(unsigned char* pTargetData, unsigned char* pTargetCrc, unsigned int msg_len);
U8 EEP_FL_chk_FL_data_validity (void);
char Reload_totals_from_flash_with_CRC_Check(U32 addr);

void load_good_Clr_wt_data(void);
unsigned int Flash_ClrWt_Data_Validity_Org(void);
unsigned int Flash_ClrWt_Data_Validity_Bkp(void);
void Flash_ClrWt_Write_RAM_to_Org(void);
void Flash_ClrWt_Write_RAM_to_Bkp(void);
void Flash_ClrWt_Org_to_RAM(void);
void Flash_ClrWt_Bkp_to_RAM(void);
void Clear_weight_load_Default(void);
void Calculate_Clr_Wt_Screen_Avg_Rate(void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
//#if 0
/*****************************************************************************
* @note       Function name: void flash_struct_backup(U8 first_time_write)
* @returns    returns		   : None
* @param      arg1			   : Flag to indicate if the flash is being written for
*														 the first time
* @author			             : Anagha Basole
* @date       date created : July 12, 2012
* @brief      Description	 : Writes the backup of the configuration to the flash
														 to the 1st 8K sector. Flag is checked, to verify if
														 the flash is being written for the first time and changed
														 to 0x5A5A
* @note       Notes		     : None
*****************************************************************************/
void flash_struct_backup(U8 first_time_write)
{
	U16 * u16Var;
	U8 success;
	U16 * read_flash;
	U16 i;
	U16 u16_CRC = 0xFFFF;

	if (ExtFlash_eraseBlock(GUI_PARAM_SECTOR_126_START_ADDR))
	{
		
		/*REPORTS structure*/
		
		//PVK - 10- Feb -2016 
	  //Verify All Historical Totals
//		Verify_Historical_Totals();

	}
	
		
	return;
}


/*****************************************************************************
* @note       Function name: U8 sector_erase(U16 * read_flash)
* @returns    returns		   : status of the erase
* @param      arg1			   : The end address of the data to be written to the flash
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Checks if the data to be written exceeds the current sector.
*														 If the current sector does exceed then the next sector is
*														 erased. When the data exceeds the sector 130 then 
*														 the first sector for the data log(sector 128)of the flash
*														 needs to be erased to store the next logs. This overwrites
*														 the oldest records.
* @note       Notes		     : None
*****************************************************************************/
U8 sector_erase(U16 * read_flash)
{
	U8 error = 1;
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_127_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_127_END_ADDR) && 
			(read_flash >= (U16 *)SECTOR_127_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
	}
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_128_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_128_END_ADDR) && 
			(read_flash >= (U16 *)SECTOR_128_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
	}
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_129_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_129_END_ADDR) && 
			(read_flash >= (U16 *)SECTOR_129_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
	}
	
	//erase if equal for the first time
	if(Flash_log_data.Current_write_addr_ptr == (U16 *)SECTOR_130_START_ADDR)
	{
		error = ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
	}
	//also erase if the some bytes are being written in one sector and the next bytes 
	//are being written in the next sector
	if((Flash_log_data.Current_write_addr_ptr <= (U16 *)SECTOR_130_END_ADDR) &&
			(read_flash >= (U16 *)SECTOR_130_END_ADDR))
	{
		error = ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
	}
	
	return (error);
}

/*****************************************************************************
* @note       Function name: U8 flash_log_store(U8 type_of_log)
* @returns    returns		   : FALSE if there is any error while wrting to the flash
*														 TRUE if there is no error writing to the flash
* @param      arg1			   : Flag to indicate type of log being stored in the
*														 flash
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Stores the data log structures to the flash.
*  													 Checks if the sector has exceeded the current sector
*														 has exceeded, then it will erase the next sector to store
*														 the data.
* @note       Notes		     : None
*****************************************************************************/
U8 flash_log_store(U8 type_of_log)
{
	U16 * u16Var;
	U8 success = __TRUE;	
	U16 i;
	static int first_time = 0 ;
	U16 log_type = (U16)type_of_log;
	__align(4) PERIODIC_LOG_STRUCT 		clr_periodic_data;
	
	if (first_time == 0)
	{
		if((Flags_struct.Log_flags & LOGS_WR_FLAG) == LOGS_WR_FLAG)
		{
			Flash_log_data.Current_write_addr_ptr = (U16*)LPC_RTC->GPREG1;
		}
		else
		{
			Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
		}
		Flash_log_data.Nos_of_records = 0;
		first_time = 1;
	}
	Flash_log_data.Nos_of_records++;
		
	switch (type_of_log)
	{
		case periodic_log:
		case periodic_log_start:
		case periodic_log_stop:
		case periodic_log_stop_at_start:
	/*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/	
			u16Var = (U16*)&periodic_data;
			if (sector_erase(Flash_log_data.Current_write_addr_ptr + FLASH_SIZEOF(periodic_data) + (2*FLASH_SIZEOF(U16))))
			{
				for(i=0; i<(FLASH_SIZEOF(periodic_data)+(2*FLASH_SIZEOF(U16))); i++)
				{
					if(i==0)
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					else if((i>0) && (i<(FLASH_SIZEOF(periodic_data)+FLASH_SIZEOF(U16))))
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, *u16Var++);
					}
					else
					{
						success = ExtFlash_writeWord((uint32_t)Flash_log_data.Current_write_addr_ptr, log_type);
					}
					Flash_log_data.Current_write_addr_ptr++;
					if(Flash_log_data.Current_write_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
					{
						Flash_log_data.Current_write_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
					}
					if(success == __FALSE)
					{
						break;
					}
				}
			}			
		break;
		

	  default:
			break;
	}
	LPC_RTC->GPREG1 = (U32)Flash_log_data.Current_write_addr_ptr;
	LPC_RTC->GPREG2 = Flash_log_data.Nos_of_records;
	
	return(success);
}
/*****************************************************************************
* @note       Function name: void read_flash_log_data(U16 size_of_struct)
* @returns    returns		   : None
* @param      arg1			   : Size of data to be read from the external flash
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Writes the backup of the configuration to the flash
														 to the 1st 8K sector. Flag is checked, to verify if
														 the flash is being written for the first time and changed
														 to 0x5A5A
* @note       Notes		     : None
*****************************************************************************/
void read_flash_log_data(U16 size_of_struct)
{
	U16 j;
	
	for(j=0; j<size_of_struct; j++)
	{
		Log_restore_buffer[j] = *Flash_log_data.Thousand_records_addr_ptr;
		Flash_log_data.Thousand_records_addr_ptr++;
		if (Flash_log_data.Thousand_records_addr_ptr >= (U16 *)SECTOR_130_END_ADDR)
		{
			Flash_log_data.Thousand_records_addr_ptr = (U16 *)SECTOR_127_START_ADDR;
		}
	}	
	return;
}

/*****************************************************************************
* @note       Function name: void flash_log_restore_to_usb(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : Feb 20, 2013
* @brief      Description	 : Writes the backup of the configuration to the flash
														 to the 1st 8K sector. Flag is checked, to verify if
														 the flash is being written for the first time and changed
														 to 0x5A5A
* @note       Notes		     : None
*****************************************************************************/
void flash_log_restore_to_usb(void)
{
	volatile U16 log_type;
	U16 i;
	U16 size=0;
	U16 *this_record = NULL;
	U8 first_time = 0;
	U16 *periodic_id_ptr = NULL;
	char disp[100];	
	g_restore_prog = 1;
	Flash_log_data.Thousand_records_addr_ptr = Flash_log_data.Current_write_addr_ptr;
	
	for(i=0, Flash_log_data.Nos_of_records=0; i<1000; i++)
	{
			Flash_log_data.Thousand_records_addr_ptr--;
			log_type = *Flash_log_data.Thousand_records_addr_ptr;
			Flash_log_data.Nos_of_records++;
			switch(log_type & 0x00FF)
			{
				case periodic_log:
				case periodic_log_start:
				case periodic_log_stop:
				case periodic_log_stop_at_start:
		/*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/
					Flash_log_data.Thousand_records_addr_ptr = Flash_log_data.Thousand_records_addr_ptr - FLASH_SIZEOF(periodic_data) - FLASH_SIZEOF(U16);
					if(first_time == 0)
					{
						first_time = 1;
						periodic_id_ptr = (Flash_log_data.Thousand_records_addr_ptr + (4*FLASH_SIZEOF(U16)));
						g_last_periodic_id = *periodic_id_ptr;
					}
					break;
				
				case set_zero_log:
					Flash_log_data.Thousand_records_addr_ptr = Flash_log_data.Thousand_records_addr_ptr - FLASH_SIZEOF(set_zero_data_log) - FLASH_SIZEOF(U16);
					break;
				
				case clear_log:
					Flash_log_data.Thousand_records_addr_ptr = Flash_log_data.Thousand_records_addr_ptr - FLASH_SIZEOF(periodic_data) - FLASH_SIZEOF(U16);
					break;
				
				case calib_log:
				case digital_log:
				case mat_test_log:
				case tst_wt_log:	
					Flash_log_data.Thousand_records_addr_ptr = Flash_log_data.Thousand_records_addr_ptr - FLASH_SIZEOF(calib_data_log) - FLASH_SIZEOF(U16);
					break;
				
 				case error_log:
// 					Flash_log_data.Thousand_records_addr_ptr = Flash_log_data.Thousand_records_addr_ptr - FLASH_SIZEOF(error_data_log) - FLASH_SIZEOF(U16);
 					break;
				
				case length_log:
					Flash_log_data.Thousand_records_addr_ptr = Flash_log_data.Thousand_records_addr_ptr - FLASH_SIZEOF(length_cal_data_log) - FLASH_SIZEOF(U16);
					break;
				
				//in case of less than thousand records
				default:
					Flash_log_data.Nos_of_records = Flash_log_data.Nos_of_records-1;
					Flash_log_data.Thousand_records_addr_ptr++;
					i = 1000; //to break out of the for loop since no more records are available
					break;
			}
			if(Flash_log_data.Thousand_records_addr_ptr <(U16 *) SECTOR_127_START_ADDR)
			{
				//Flash_log_data.Thousand_records_addr_ptr += (FLASH_SIZEOF(periodic_data) - 1);
				size = ((U16 *)SECTOR_127_START_ADDR - Flash_log_data.Thousand_records_addr_ptr);
				Flash_log_data.Thousand_records_addr_ptr = ((U16 *)(SECTOR_130_END_ADDR + 1) - size);
			}
	}//for loop
	
	//store the data in the USB drive
	for(i=0; i<Flash_log_data.Nos_of_records; i++)
	{
			this_record = Flash_log_data.Thousand_records_addr_ptr;	//store address where we started for record retrieval
			//get the type of log that is being read
			if(i) //if it is not first record
			{
				Flash_log_data.Thousand_records_addr_ptr++;	//increment pointer as need to skip log type present at the end
			}
// 			if((flash_restore_to_usb_fg == 1))
// 			{
// 				if((i == (Flash_log_data.Nos_of_records -1)) )
// 				{
// 					flash_restore_to_usb_fg = 0;
// 				}
// 				else 
// 				{

// 					sprintf(&disp[0],"          Synchronizing Record  %d  of %d  ",(i+1),Flash_log_data.Nos_of_records);
// 					GUI_DispStringAt(disp, WINDOW_RECT_POS_X0, 230);
// 				}
// 			}
			
			log_type = (U16)*Flash_log_data.Thousand_records_addr_ptr;
			Flash_log_data.Thousand_records_addr_ptr++;
			switch(log_type & 0x00FF)
			{
					case periodic_log:	//if the log type is a periodic data log
					case periodic_log_start:
					case periodic_log_stop:
					case periodic_log_stop_at_start:
	/*Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix*/			
							read_flash_log_data(FLASH_SIZEOF(periodic_data));
							//memcpy((U16 *)&periodic_data, Log_restore_buffer, FLASH_SIZEOF(periodic_data));
							if(g_Q_addition_prog)	//dont update periodic_data if Q addition is in progress
							{
								//skip for now and go back to start of record
								Flash_log_data.Thousand_records_addr_ptr = this_record;
								i--;
							}
							else
							{
								memcpy((U16 *)&periodic_data, Log_restore_buffer, sizeof(periodic_data));
								clear_data_log.Clr_data_hdr.Id = periodic_data.Periodic_hdr.Id;		// To match both hdr ID
								log_file_write(periodic_log);
							}							
						break;
				
					case set_zero_log:	//if the log type is a set zero data log					
							read_flash_log_data(FLASH_SIZEOF(set_zero_data_log));
							memcpy((U16 *)&set_zero_data_log, Log_restore_buffer, sizeof(set_zero_data_log));
							log_file_write(set_zero_log);
						break;
			
					case clear_log:	//if the log type is a clear data log
							read_flash_log_data(FLASH_SIZEOF(periodic_data));
							//Updated this logic on 23-Nov-2015 to restore clear log along with periodic log data
							memcpy((U16 *)&periodic_data, Log_restore_buffer, sizeof(periodic_data));
							memcpy((U16 *)&clear_data_log.Clr_data_hdr.Data_type[0], &periodic_data.Periodic_hdr.Data_type[0], sizeof(clear_data_log.Clr_data_hdr.Data_type));
							clear_data_log.Clr_data_hdr.Id = periodic_data.Periodic_hdr.Id;
							log_file_write(clear_log);
						break;
			
					case calib_log:	//if the log type is a calibration data log
					case digital_log: //"CAL"
					case mat_test_log: //"CAL"
					case tst_wt_log: //"CAL"
							read_flash_log_data(FLASH_SIZEOF(calib_data_log));
							memcpy((U16 *)&calib_data_log, Log_restore_buffer, sizeof(calib_data_log));
							if ((log_type & 0x00FF) == calib_log)
							{
								log_file_write(calib_log);
							}
							else if ((log_type & 0x00FF) == digital_log)
							{
								log_file_write(digital_log);
							}
							else if ((log_type & 0x00FF) == mat_test_log)
							{
								log_file_write(mat_test_log);
							}
							else if ((log_type & 0x00FF) == tst_wt_log)
							{
								log_file_write(tst_wt_log);
							}
						break;

// 					case error_log:	//if the log type is a error data log "ERR"
// 							read_flash_log_data(FLASH_SIZEOF(error_data_log));
// 							memcpy((U16 *)&error_data_log, Log_restore_buffer, sizeof(error_data_log));
// 							log_file_write(error_log);
// 						break;
					
					case length_log:	//if the log type is a length data log "LEN"
							read_flash_log_data(FLASH_SIZEOF(length_cal_data_log));
							memcpy((U16 *)&length_cal_data_log, Log_restore_buffer, sizeof(length_cal_data_log));
							log_file_write(length_log);
						break;
					
					default:
						break;
			}
	}//for loop
// 	if((Flash_log_data.Nos_of_records == 0)&&(flash_restore_to_usb_fg == 1))
// 	{
// 		sprintf(&disp[0],"      NO RECORD FOUND IN FLASH     ");
// 		GUI_DispString(disp);	
// 					flash_restore_to_usb_fg = 0;
// 			
// 	}
	
	ExtFlash_eraseBlock(SECTOR_127_START_ADDR);
	ExtFlash_eraseBlock(SECTOR_128_START_ADDR);
	ExtFlash_eraseBlock(SECTOR_129_START_ADDR);
	ExtFlash_eraseBlock(SECTOR_130_START_ADDR);
	g_restore_prog = 0;
	return;
}

/*****************************************************************************
* @note       Function name: void flash_para_backup(void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Megha Gaikwad
* @date       date created : Aug 10, 2015
* @brief      Description	 : Writes the backup of the parameters to the flash. 
														 Parametres are All totals, Daily report, weekly report and 
														 monthly report parameters, all header IDs
														 These parameters will be restored after soft reset condition.
* @note       Notes		     : None
*****************************************************************************/
void flash_para_backup(void)
{
					
}


/*****************************************************************************
* @note       Function name: void flash_para_reload(U32 Totals_Current_addr,char Status_condition)
* @returns    returns		   : None
* @param      arg1			   : addr - Flash address to read data
* @author			             : Oaces Team
* @date       date created : dec 29, 2015
* @brief      Description	 : Read the all totals from the flash and check for CRC. 
                             If Matches then reload it else set to zero
* @note       Notes		     : None
*****************************************************************************/
void flash_para_reload(U32 Totals_Current_addr, char Status_condition)
{
 
	
}//Function end	


/*****************************************************************************
* @note       Function name: U32 flash_para_record_search(U32 addr)
* @returns    returns		   : addr - Record address
* @param      arg1			   : addr - Flash start address
* @author			             : Megha Gaikwad
* @date       date created : Aug 11, 2015
* @brief      Description	 : Read the record number from the flash and point to the latest  
														 Parametres records. Parameters will be All totals, Daily report, 
														 weekly report and monthly report parameters, all header IDs
* @note       Notes		     : None
*****************************************************************************/
U32 flash_para_record_search(U32 addr)
{
	U16 *read_flash;
	U16 Data_valid;
	U16 *Temp_read_flash;
	U16 Temp_Data_valid;
	U16 *read_flash_addr, *Temp_FlashStartAdd;
	int i;
	U32 addr1;
	U16 Block_Length;
	
#if 0
	FILE *fp;
#endif
	
#if 0
	fp = fopen ("Para.txt", "a");
	if(fp != NULL)
	{
		fprintf(fp, "%s\r\n", "Record search");
		fclose(fp);
	}
#endif
	
  //PVK
	//Check for data validity 
 	Temp_read_flash = (U16 *)EXT_FLASH_PARA_BACKUP_COPY_START_ADDR;
	Temp_Data_valid = *Temp_read_flash;	
	read_flash = (U16 *)addr;
	Data_valid = *read_flash;
	Block_Length = (EXT_FLASH_PAGE_SIZE_BYTES*4);
	
	if((g_old_fw_ver >= (float)4.19) || (!((g_old_fw_ver >= (float)0.90) && (g_old_fw_ver <= (float)0.99))))
	{
		//if working copy is not erased then take data from working copy else check for backup copy
		if(Data_valid != 0xFFFF)
		{
			read_flash = (U16 *)addr;
			Data_valid = *read_flash;
		}
		else
		{	
			if(Temp_Data_valid != 0xFFFF)
			{	
				//PVK - Copy data from backup to working copy
				Flash_Write_In_progress = 1; //Added on 27 April 2016  
				ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_START_ADDR);
				Flash_Write_In_progress = 0; //Added on 27 April 2016  
				Temp_FlashStartAdd = (U16 *)EXT_FLASH_PARA_BACKUP_START_ADDR;
				addr1 = EXT_FLASH_PARA_BACKUP_COPY_START_ADDR;
				read_flash_addr = (U16 *)addr1;
			
				//Take backup copy of data
				for (i=0; i<(Block_Length/2); i++)
				{
					//Read data from current flash and wirte it into backup copy
					Flash_Write_In_progress = 1; //Added on 27 April 2016  
					ExtFlash_writeWord((uint32_t)Temp_FlashStartAdd, *read_flash_addr);
					Flash_Write_In_progress = 0; //Added on 27 April 2016  
					read_flash_addr++;
					Temp_FlashStartAdd++;
				}	
				//Advance the address by block size
				addr = (EXT_FLASH_PARA_BACKUP_START_ADDR + (EXT_FLASH_PAGE_SIZE_BYTES*4));
				//addr = EXT_FLASH_PARA_BACKUP_START_ADDR;
				//record_no = 1;
				read_flash = (U16 *)addr;
				Data_valid = *read_flash;
				//Advance the address by block size
				//addr = (EXT_FLASH_PARA_BACKUP_START_ADDR + (EXT_FLASH_PAGE_SIZE_BYTES*4));
			}
			else
			{
				//If both sector are erased then take start address of working copy
				read_flash = (U16 *)EXT_FLASH_PARA_BACKUP_START_ADDR;
				Data_valid = *read_flash;
			}			
		}	
	}
	
	while(Data_valid != 0xFFFF)
	{	
#if 0
		fp = fopen ("Para.txt", "a");
		if(fp != NULL)
		{
			fprintf(fp,"%d,%d\r\n",addr,Data_valid);
			fclose(fp);
		}	
#endif
						
		addr = (addr + (EXT_FLASH_PAGE_SIZE_BYTES*4));
		
		//Added PVK Mar 2016 
		if(addr > EXT_FLASH_PARA_BACKUP_END_ADDR)
		{
      break;
    } 
		read_flash = (U16 *)addr;
		Data_valid = *read_flash;
	}
	
	//Check address range
	if(addr > EXT_FLASH_PARA_BACKUP_END_ADDR)
	{
		addr = EXT_FLASH_PARA_BACKUP_START_ADDR;
		flash_para_addr = 0;
		flash_para_record_no = 0;
	}
	else if(addr == EXT_FLASH_PARA_BACKUP_START_ADDR)
	{
		addr = EXT_FLASH_PARA_BACKUP_START_ADDR;
		flash_para_addr = 0;
		flash_para_record_no = 0;
	}
	else
	{
		addr = (addr - (EXT_FLASH_PAGE_SIZE_BYTES*4));
		read_flash = (U16 *)addr;
		Data_valid = *read_flash;	
		flash_para_addr = addr;
		flash_para_record_no = Data_valid;
	}

#if 0
	fp = fopen ("Para.txt", "a");
	if(fp != NULL)
	{
		fprintf(fp,"%d,%d\r\n",flash_para_addr,flash_para_record_no);
		fclose(fp);
	}	
#endif
						
	return addr;

}

/*****************************************************************************
* @note       Function name: U16 flash_para_record_flag(U8 type)
* @returns    returns		   : 0 - for set operation
														 data - for get operation
* @param      arg1			   : None
* @author			             : Megha Gaikwad
* @date       date created : Aug 27, 2015
* @brief      Description	 : Write/read flag to/from flash address.
* @note       Notes		     : This flag will be written to flash after writing periodic 
														 data into file. After soft reset this flag will be read.
*****************************************************************************/
U16 flash_para_record_flag(U8 type)
{

#if 0	
	U16 * read_flash_addr;
	U16 Data_valid;
	uint32_t addr;

#if 0
	FILE *fp;
#endif
	
	addr = flash_para_addr;

	addr = (addr + (EXT_FLASH_PAGE_SIZE_BYTES*4) - 2);			// Last address of record region
	read_flash_addr = (U16 *)addr;	
	
	if(type == SET_FLAG)
	{
		ExtFlash_writeWord((uint32_t)read_flash_addr, SET);
	
#if 0
		fp = fopen ("Para.txt", "a");
		if(fp != NULL)
		{
			fprintf(fp, "%s\r\n", "File write");
			fprintf(fp,"%d\r\n",addr);
			fclose(fp);
		}
#endif		
		
	}
	else
	{
		Data_valid = *read_flash_addr;		
		return Data_valid;
		//return 0;
	}
	#endif 
	
	return 0;
}



/*****************************************************************************
* @note       Function name: void EEP_FL_create_backup (void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Creates a backup copy of the EEPROM config data
														 into flash memory
* @note       Notes		     : 
*****************************************************************************/
void EEP_FL_create_backup (void)
{
	U16 * u16Var;
	U16 i;
	U16 *Current_write_addr_ptr;
	
	#ifdef DEBUG_PORT
	Debug_Buf[12] = 'T';
	Debug_Buf[13] = '4';
	Debug_Buf[14] = ' ';
	#endif
	
	Flash_Write_In_progress = 1; //Added on 27 April 2016  
	
	ExtFlash_eraseBlock(SECTOR_123_START_ADDR);
	
	#ifdef DEBUG_PORT
	Debug_Buf[15] = 'T';
	Debug_Buf[16] = '5';
	Debug_Buf[17] = ' ';
	#endif
	
	//Update the CRC first in RAM copy
	CalcCRC((unsigned char*)&Pcm_var, (unsigned char*)&Pcm_var.CRC_PCM_VAR, (unsigned int)offsetof(PCM_STRUCT, CRC_PCM_VAR));
	Current_write_addr_ptr = (U16 *)EEP_FL_PCM_VAR_START;
	u16Var = (U16*)&Pcm_var;
	for(i=0; i<(FLASH_SIZEOF(Pcm_var)); i++)
	{
		ExtFlash_writeWord((uint32_t)Current_write_addr_ptr, *u16Var++);
		Current_write_addr_ptr++;
	}
  

	Flash_Write_In_progress = 0; //Added on 27 April 2016  
}

/*****************************************************************************
* @note       Function name: U8 EEP_FL_chk_FL_data_validity (void)
* @returns    returns		   : status (valid-1/invalid-0)
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Checks the flash based backup copy's data validity
* @note       Notes		     : 
*****************************************************************************/
U8 EEP_FL_chk_FL_data_validity (void)
{
	U16 calculated_CRC = 0;
	U16 stored_CRC = 0;
	U8 data_buf[1000];
	U8 status = 0;
	
 	memcpy((U16 *)&data_buf, (U16*)EEP_FL_PCM_VAR_START, offsetof(PCM_STRUCT, CRC_PCM_VAR));
 	CalcCRC((unsigned char*)data_buf, (unsigned char*)&calculated_CRC, (unsigned int)offsetof(PCM_STRUCT, CRC_PCM_VAR));
 	stored_CRC = (U16)*(U16*)(EEP_FL_PCM_VAR_START + offsetof(PCM_STRUCT, CRC_PCM_VAR));
	
	if (calculated_CRC != stored_CRC)
	{
		status = 0;
	}
	else
	{
		if((calculated_CRC != 0)&&(stored_CRC != 0))
		{	
			status = 1;
					
		}
		else
		{
			status = 0;
		}	
	}
	return status;	
}

/*****************************************************************************
* @note       Function name: void CalcCRC(unsigned char* pTargetData, 
														 unsigned char* pTargetCrc, unsigned int msg_len)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Calculates the 16-bit CRC using the table
* @note       Notes		     : 
*****************************************************************************/
void CalcCRC(unsigned char* pTargetData, unsigned char* pTargetCrc, unsigned int msg_len)
{
  unsigned char CrcHigh, CrcLow, Temp;
  unsigned int Length;
  
  Length = msg_len; 
  CrcLow = 0xFF;
  CrcHigh = 0xFF;

  while(Length)
  {
    Temp = CrcHigh;
    CrcHigh = (unsigned char)(crc_table[2 * ((*pTargetData) ^ CrcLow)]);
    CrcLow = (unsigned char)(Temp ^ crc_table[(2 * ((*pTargetData) ^ CrcLow)) + 1]);
    pTargetData++;
    Length--;
  };

  *pTargetCrc = CrcLow;
  *(pTargetCrc + 1) = CrcHigh;
}



#endif /*#ifdef EXT_FLASH*/
/*****************************************************************************
* End of file
*****************************************************************************/
