										 /*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : Ext_flash_low_level.h
* @brief			         : External flash  interfacing functions
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     :
* @internal 			     :
*
*
*****************************************************************************/
#ifndef __EXT_FLASH_LOW_LEVEL_H
#define __EXT_FLASH_LOW_LEVEL_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

#ifdef EXT_FLASH
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define M29W640FB
#define FLASH_BASE_ADDRESS			0x80000000
#define M29W640FB_ID						0x002022FD            //Device ID for
#define FLASH_LAST_SEC_ADDRESS			0x7C0000         // Flash Last sector address
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
{
  uint32_t device_size;  			/* Device size in bytes */
  uint32_t num_mem_region1;  /* Number of sectors */
  uint32_t mem_region1_size;  /* Sector size in bytes */
  uint32_t num_mem_region2;   /* Number of blocks */
  uint32_t mem_region2_size;   /* Block size in bytes */
	uint16_t flash_valid;				/*set if the flash id is valid*/
} geometry_t;

extern geometry_t chip_info;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
uint8_t ExtFlash_Init(void);
uint32_t Read_DeviceId_Flash(void);
uint8_t ExtFlash_eraseBlock(uint32_t addr);
uint8_t ExtFlash_eraseEntireChip(void);
uint8_t ExtFlash_writeWord(uint32_t addr, uint16_t data);
uint8_t ExtFlash_writeWord_before_task(uint32_t addr, uint16_t data);
uint8_t ExtFlash_writeBuff(uint32_t addr, uint16_t* data, uint16_t len);
uint8_t ExtFlash_secid_getLockStatus(uint32_t blockAddr);

#endif /*#ifdef EXT_FLASH*/

#endif /*__EXT_FLASH_LOW_LEVEL_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
