/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Ext_flash_high_level.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 12, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __EXT_FLASH_HIGH_LEVEL_H
#define __EXT_FLASH_HIGH_LEVEL_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

#ifdef EXT_FLASH
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define BACKUP_PRESENT_FLAG             0x00005A5A
#define FLASH_BACKUP_FLAG								0x0000A5A5
#define DEF_PARAM_WRITE_FLAG_SECTOR_126 0x80770000

#define GUI_PARAM_SECTOR_126_START_ADDR 0x80770000
#define GUI_PARAM_SECTOR_126_END_ADDR   0x8077FFFF


#define FLASH_SIZEOF(X) 		(sizeof(X)/2)

#define TOTAL_STRUCT_SIZE 	(FLASH_SIZEOF(Scale_setup_var) + FLASH_SIZEOF(Calibration_var) + \
														FLASH_SIZEOF(Setup_device_var) + FLASH_SIZEOF(Rprts_diag_var) + \
														FLASH_SIZEOF(Admin_var)+FLASH_SIZEOF(Misc_var))
													
//#define SCALE_SETUP_ADDR    ((U16 *)GUI_PARAM_SECTOR_126_START_ADDR + 2)
//#define SCALE_SETUP_ADDR    ((U16 *)GUI_PARAM_SECTOR_126_START_ADDR)
//#define CALIBRATION_ADDR	  (SCALE_SETUP_ADDR + FLASH_SIZEOF(Scale_setup_var))
//#define SETUP_DEVICE_ADDR	  (CALIBRATION_ADDR + FLASH_SIZEOF(Calibration_var))
//#define RPRTS_DIAG_ADDR		  (SETUP_DEVICE_ADDR + FLASH_SIZEOF(Setup_device_var))
//#define ADMIN_ADDR	 			  (RPRTS_DIAG_ADDR + FLASH_SIZEOF(Rprts_diag_var))
//#define RPRTS_DIAG_CLEAR_WEIGHT_ADDR		 						 (GUI_PARAM_SECTOR_126_START_ADDR + offsetof(RPRTS_DIAG_STRUCT, LastClearedTotal))

#define RPRTS_DIAG_ADDR 															0x807702E0	//Sect_126 to maintain compatibility from old fw
#define RPRTS_DIAG_CLEAR_WEIGHT_ADDR		 						 (RPRTS_DIAG_ADDR + offsetof(RPRTS_DIAG_STRUCT, LastClearedTotal))	//0x807703A0												//Prasad


#define SIZEOF_RPRTS_DIAG_CLEAR_WEIGHT	 			 			 ((U16)(offsetof(RPRTS_DIAG_STRUCT, Trim_live_weight_disp) - offsetof(RPRTS_DIAG_STRUCT, LastClearedTotal)))	//Prasad 
#define RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_ADDR		 			 0x80600000												//Prasad 
#define RPRTS_DIAG_CLEAR_WEIGHT_CRC_ADDR			 		   0x8077FFFC												//Prasad 
#define RPRTS_DIAG_CLEAR_WEIGHT_BACKUP_CRC_ADDR			 0x8060FFFC												//Prasad 

#define EXT_FLASH_BLOCK_SIZE 		0xFFFF

#define SECTOR_123_START_ADDR		0x80740000
#define SECTOR_123_END_ADDR			0x8074FFFF
#define SECTOR_127_START_ADDR		0x80780000
#define SECTOR_127_END_ADDR			0x8078FFFF
#define SECTOR_128_START_ADDR		0x80790000
#define SECTOR_128_END_ADDR			0x8079FFFF
#define SECTOR_129_START_ADDR		0x807A0000
#define SECTOR_129_END_ADDR			0x807AFFFF
#define SECTOR_130_START_ADDR		0x807B0000
#define SECTOR_130_END_ADDR			0x807BFFFF


#define EXT_FLASH_PARA_BACKUP_COPY_START_ADDR     0x80500000
#define EXT_FLASH_PARA_BACKUP_START_ADDR     0x807D0000
//#define EXT_FLASH_PARA_BACKUP_START_ADDR     0x80700000
#define EXT_FLASH_PARA_BACKUP_END_ADDR       0x807DFFFF
//#define EXT_FLASH_PARA_BACKUP_END_ADDR       0x8070FFFF
#define EXT_FLASH_LAST_SECTOR_START_ADDR     0x807F0000
#define EXT_FLASH_LAST_SECTOR_END_ADDR       0x807FFFFF

#define EEP_EXT_FLASH_BACKUP		SECTOR_123_START_ADDR
#define EEP_FL_PCM_VAR_START		EEP_EXT_FLASH_BACKUP
#define EEP_FL_CALIBRATION_START		(EEP_FL_PCM_VAR_START+(sizeof(Scale_setup_var)))
#define EEP_FL_SETUP_DEVICE_START		(EEP_FL_CALIBRATION_START+(sizeof(Calibration_var)))
#define EEP_FL_ADMIN_START					(EEP_FL_SETUP_DEVICE_START+(sizeof(Setup_device_var)))
#define EEP_FL_MISC_START					(EEP_FL_ADMIN_START+(sizeof(Admin_var)))


#define EXT_FLASH_PAGE_SIZE_BYTES						 64
#define EXT_FLASH_TOTALS_SECTOR_BLOCK_LENGTH (EXT_FLASH_PAGE_SIZE_BYTES*4)

//offsetof macro:
//Usage: s - Structure name, m - structure member
//#define offsetof(s,m) (U16)&(((s *)0)->m)		//Gets the offset of the structure member
#ifdef REV3_BOARD
#define FIRMWARE_VER 				0x58
#define FIRMWARE_VER_MAJOR  0x03
#define FIRMWARE_VER_MAJOR1  0x00
#else 
#define FIRMWARE_VER 				0x58
#define FIRMWARE_VER_MAJOR  0x04
#define FIRMWARE_VER_MAJOR1  0x00
#endif 

/*============================================================================
* Public Data Types
*===========================================================================*/
typedef struct{
								U16 Nos_of_records;
								U16 * Current_write_addr_ptr;								
								U16 * Thousand_records_addr_ptr;
							}FLASH_LOG_STRUCTURE;

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern U8 g_restore_prog;
/* unsigned integer variables section */
extern U16 flash_para_record_no;
//PVK - 11 Jan 2015
extern U16	gTotals_Calculated_CRC;
/* Signed integer variables section */

/* unsigned long integer variables section */
extern U32 flash_para_addr;
/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */
extern float g_old_fw_ver;
extern float g_current_fw_ver;
/* Double variables section */

/* Structure or Union variables section */
	extern FLASH_LOG_STRUCTURE Flash_log_data; 
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void flash_struct_backup(U8 first_time_write);
extern void load_good_Clr_wt_data(void);
extern U8 flash_log_store(U8 type_of_log);
extern U8 sector_erase(U16 * read_flash);
extern void flash_log_restore_to_usb(void);
extern void read_flash_log_data(U16 size_of_struct);
extern void test_function_log (void);
extern void flash_para_backup(void);
extern void flash_para_reload(U32 Totals_Current_addr,char Status_condition);
extern U32 flash_para_record_search(U32 addr);
extern U16 flash_para_record_flag(U8 type);
extern void flash_clear_weight_backup(void);
extern void EEP_FL_create_backup (void);
extern void CalcCRC(unsigned char* pTargetData, unsigned char* pTargetCrc, unsigned int msg_len);
extern U8 EEP_FL_chk_FL_data_validity (void);
extern void Calculate_Clr_Wt_Screen_Avg_Rate(void);
extern char Err_data_hdrId_reload(U32 Totals_Current_addr); //Added by DK on 16Feb 2016 
#endif /*#ifdef EXT_FLASH*/

#endif /*__EXT_FLASH_HIGH_LEVEL_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
