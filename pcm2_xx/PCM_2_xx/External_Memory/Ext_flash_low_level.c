					/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : Ext_flash_low_level.c
* @brief			         : 
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     :
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "Emc_config.h"
#include "Ext_flash_low_level.h"
#include "lpc_types.h"

#ifdef EXT_FLASH
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define GET_ADDR(addr)  (volatile uint16_t *)(FLASH_BASE_ADDRESS | ((addr << 1) ))

#define CMD_SWID      					0x90
#define CMD_CFI_QRY   					0x98
#define CMD_ID_EXIT   					0xF0
#define CMD_ERASE_BLOCK  				0x30
#define CMD_ERASE_CHIP   				0x10
#define CMD_PROGRAM_WORD				0xA0

/*============================================================================
* Private Data Types
*===========================================================================*/


/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
geometry_t chip_info;

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*-------------------------PRIVATE FUNCTIONS------------------------------*/
/*****************************************************************************
* @note       Function name	: uint8_t Read_DeviceId_Flash(void)
* @returns    returns				: 1 -> if device id matches
*															0 -> device id not matches
* @param      arg1					: None
* @param      arg2					: None
* @author			        			: Shweta Pimple
* @date       date  Created	: 31/01/2013 <MM-DD-YYYY>
* @brief      Description		: Read the device ID of an external flash M29W640FB
* @note       Notes					: None
*****************************************************************************/

uint32_t Read_DeviceId_Flash(void)
{
	uint16_t manuid = 0;
  uint16_t devid = 0;
  uint32_t result = 0;

	__disable_irq();
	
  *(GET_ADDR(0x555)) = 0xAA;
  *(GET_ADDR(0x2aa)) = 0x55;
	*(GET_ADDR(0x555)) = 0x90;

	manuid = *(GET_ADDR(0x00));
  devid  = *(GET_ADDR(0x01));

  result = ((manuid << 16) | devid);

  *(GET_ADDR(0x0555)) = 0x00AA;
  *(GET_ADDR(0x02aa)) = 0x0055;
  *(GET_ADDR(0x0555)) = CMD_ID_EXIT;
	
	__enable_irq();

	return result;
}

static void getGeoInfo(uint16_t info[14])
{
  int i = 0;

	__disable_irq();
	
  *(GET_ADDR(0x555)) = 0x00AA;
  *(GET_ADDR(0x2aa)) = 0x0055;
  *(GET_ADDR(0x555)) = CMD_CFI_QRY;

  for (i = 0; i < 14; i++) {
    info[i] = *(GET_ADDR(0x27 + i));
  }

  *(GET_ADDR(0x555)) = 0x00AA;
  *(GET_ADDR(0x2aa)) = 0x0055;
  *(GET_ADDR(0x555)) = CMD_ID_EXIT;
	
	__enable_irq();
}
#if 0
/******************************************************************************
 *
 * Description:
 *    When the SST39VF160x/320x are in the internal Program operation, any
 *    attempt to read DQ7 will produce the complement of the true data. Once
 *    the Program operation is completed, DQ7 will produce true data. Note
 *    that even though DQ7 may have valid data immediately following the
 *    completion of an internal Write operation, the remaining data outputs
 *    may still be invalid: valid data on the entire data bus will appear in
 *    subsequent successive Read cycles after an interval of 1 �s. During
 *    internal Erase operation, any attempt to read DQ7 will produce a '0'.
 *    Once the internal Erase operation is completed, DQ7 will produce a '1'.
 *
 * Parameters:
 *    addr      The device address
 *    data      The original (true) data
 *    timeout   Maximum number of loops to delay
 *
 * Returns:
 *    TRUE if success
 *
 *****************************************************************************/
static uint16_t check_data_polling(uint32_t addr, uint16_t data, uint32_t timeout)
{
  volatile uint16_t *p = (uint16_t*) addr;
  uint16_t true_data = data & 0x80,currdata_DQ5;
  uint32_t i;

  for (i = 0; i < timeout; i++)
  {
		currdata_DQ5 = *p & 0x20;	
		if ( true_data == (*p &0x80) )
    {
      return (_TRUE);
    }
		else
		{
			if(currdata_DQ5 == 1)
			{

				if ( true_data == (*p &0x80) )
				{	 
						return (_TRUE);
				}
				else
				{
							return (_FALSE);
				}	
			}	
					
		}	
  }
  return (_FALSE);
}


/*static uint16_t check_ready_busy(uint32_t timeout)
{
   int i;
	 uint32_t Rdpinstatus;
	
	for (i = 0; i < timeout; i++)
	{
		Rdpinstatus = LPC_GPIO3->PIN;
		
		if((Rdpinstatus & (1UL << RB_FLASH_PIN)) != 0)
		{
			//DelayMs(20);
			return (_TRUE);
		}
  }
  return (_FALSE);
}*/
#endif

static uint16_t check_ready_busy(uint32_t addr, uint32_t timeout)
{
  volatile uint16_t *p = (uint16_t*) addr;
  uint16_t predata, currdata_DQ6, currdata_DQ5;
  int i;
	uint32_t Rdpinstatus;
	
  predata = *p & 0x40;
	
	for (i = 0; i < timeout; i++)
	{
    currdata_DQ6 = *p & 0x40;
		currdata_DQ5 = *p & 0x20;
		
		Rdpinstatus = LPC_GPIO3->PIN;
		
		if((Rdpinstatus & (1UL << RB_FLASH_PIN)) != 0)
		{	
    if (predata == currdata_DQ6)
    {
			//DelayMs(20);
			return (_TRUE);
		} 
		else
		{
				if(currdata_DQ5 == 1)
				{	 
						currdata_DQ6 = *p & 0x40;
						predata = currdata_DQ6;
					 __nop();
					 __nop();
					 currdata_DQ6 = *p & 0x40;
				 					
					 if (predata == currdata_DQ6)
					 {	 
						 //DelayMs(20);
						 return (_TRUE);
					 }
					 else
					 {
							return (_FALSE);
					}	 
			 }
		}	
		predata = currdata_DQ6;
  }
 } 
  return (_FALSE);
}

#if 0
/******************************************************************************
 *
 * Description:
 *    During the internal Program or Erase operation, any consecutive attempts
 *    to read DQ6 will produce alternating �1�s and �0�s, i.e., toggling
 *    between 1 and 0. When the internal Program or Erase operation is
 *    completed, the DQ6 bit will stop toggling. The device is then ready
 *    for the next operation.
 *
 * Parameters:
 *    addr      The device address
 *    timeout   Maximum number of loops to delay
 *
 * Returns:
 *    TRUE if success
 *
 *****************************************************************************/
static uint16_t check_toggle_ready(uint32_t addr, uint32_t timeout)
{
  volatile uint16_t *p = (uint16_t*) addr;
  uint16_t predata, currdata_DQ6, currdata_DQ5;
  int i;

  predata = *p & 0x40;
  for (i = 0; i < timeout; i++)
	{
    currdata_DQ6 = *p & 0x40;
		currdata_DQ5 = *p & 0x20;	
		
    if (predata == currdata_DQ6)
    {
				  return (_TRUE);
		} 
		else
		{
				if(currdata_DQ5 == 1)
				{	 
						currdata_DQ6 = *p & 0x40;
						predata = currdata_DQ6;
					 __nop();
					 __nop();
					 currdata_DQ6 = *p & 0x40;
				 					
					 if (predata == currdata_DQ6)
					 {	 
							return (_TRUE);
					 }
					 else
					 {
							return (_FALSE);
					}	 
			 }
		}	
    predata = currdata_DQ6;
  }
  return (_FALSE);
}
#endif
/******************************************************************************
 * Public Functions
 *****************************************************************************/


/*****************************************************************************
* @note       Function name	: uint8_t ExtFlash_Init(void)
* @returns    returns				: 1 -> if device id matches
*															0 -> device id not matches
* @param      arg1					: None
* @param      arg2					: None
* @author			        			: Shweta Pimple
* @date       date  Created	: 31/01/2013 <MM-DD-YYYY>
* @brief      Description		: Read the device ID of an external flash M29W640FB
* @note       Notes					: None
*****************************************************************************/
uint8_t ExtFlash_Init(void)
{
	uint32_t deviceId;
	uint16_t info[14];

//	deviceId = Read_DeviceId_Flash();
	Read_DeviceId_Flash();
		//read the device id
// 		if(deviceId!= M29W640FB_ID)
// 		{
// 				return _FALSE;
// 		}

		getGeoInfo(info);
		chip_info.device_size = 1 << info[0];
    chip_info.num_mem_region1 = ((info[7] << 8) | info[6]) + 1;
    chip_info.mem_region1_size = ((info[9] << 8) | info[8]) * 256;
    chip_info.num_mem_region2 =  ((info[11] << 8) | info[10]) + 1;
    chip_info.mem_region2_size = ((info[13] << 8) | info[12]) * 256;

		return _TRUE;
}


/*****************************************************************************
* @note       Function name	: uint8_t ExtFlash_eraseBlock(uint32_t addr)
* @returns    returns				: 1 -> if device erased successfully
*															0 -> device is not erased
* @param      arg1					: None
* @param      arg2					: None
* @author			        			: Shweta Pimple
* @date       date  Created	: 31/01/2013 <MM-DD-YYYY>
* @brief      Description		: Erase the block
* @note       Notes					: None
*****************************************************************************/
uint8_t ExtFlash_eraseBlock(uint32_t addr)
{
  volatile uint16_t* p;
	char Status;

	//tsk_lock();
	__disable_irq();

	*(GET_ADDR(0x555)) 	= 0x00AA;
  *(GET_ADDR(0x2AA)) 	= 0x0055;
  *(GET_ADDR(0x555)) 	= 0x0080;
  *(GET_ADDR(0x555)) 	= 0x00AA;
  *(GET_ADDR(0x2AA)) 	= 0x0055;

  p  = (uint16_t*) addr;
  *p = CMD_ERASE_BLOCK;

//	DelayMs(10);
	
	Status = check_ready_busy(addr, 5000000);
  //Status = check_toggle_ready(addr, 5000000);
	__enable_irq();
	//tsk_unlock();
	
	return(Status);
	//return(check_data_polling(addr, 0xFFFF, 5000000));
}

/*****************************************************************************
* @note       Function name	: uint8_t ExtFlash_eraseEntireChip()
* @returns    returns				: 1 -> if device erased successfully
*															0 -> device is not erased
* @brief      Description		: Erase the full chip
*****************************************************************************/
uint8_t ExtFlash_eraseEntireChip(void)
{
  char Status;
	
	//tsk_lock();
	__disable_irq(); 
	
	*(GET_ADDR(0x555)) 	= 0x00AA;
  *(GET_ADDR(0x2AA)) 	= 0x0055;
  *(GET_ADDR(0x555)) 	= 0x0080;
  *(GET_ADDR(0x555)) 	= 0x00AA;
  *(GET_ADDR(0x2AA)) 	= 0x0055;
  *(GET_ADDR(0x555)) 	= CMD_ERASE_CHIP;

   Status = check_ready_busy(FLASH_BASE_ADDRESS, 5000000);
  //Status = check_toggle_ready(FLASH_BASE_ADDRESS, 0xFFFFFFFF);
	
	__enable_irq();
	//tsk_unlock();
	
	return(Status);
	//poll the r/b pin

}

/*****************************************************************************
* @note       Function name	: uint8_t ExtFlash_writeWord(uint32_t addr, uint16_t data)
* @returns    returns				: 1 -> if device progrmmed successfully
*															0 -> device is not programmed
* @brief      Description		: to write the word into memory
*****************************************************************************/
uint8_t ExtFlash_writeWord(uint32_t addr, uint16_t data)
{
	volatile uint16_t *p;
	char Status;
	
	//tsk_lock();
	__disable_irq();
	
	*(GET_ADDR(0x555)) = 0x00AA;
  *(GET_ADDR(0x2AA)) = 0x0055;
  *(GET_ADDR(0x555)) = 0x00A0;

  p  = (uint16_t*) addr;
  *p = data;
	
  Status = check_ready_busy(addr, 5000000);
  //Status = check_toggle_ready(addr, 500000);
	
	__enable_irq();
	//tsk_unlock();
	
	return  (Status);
}

/*****************************************************************************
* @note       Function name	: uint8_t ExtFlash_writeWord_before_task(uint32_t addr, uint16_t data)
* @returns    returns				: 1 -> if device progrmmed successfully
*															0 -> device is not programmed
* @brief      Description		: to write the word into memory
*****************************************************************************/
uint8_t ExtFlash_writeWord_before_task(uint32_t addr, uint16_t data)
{
	volatile uint16_t *p;
	char Status;
	
	__disable_irq();
	
	*(GET_ADDR(0x555)) = 0x00AA;
  *(GET_ADDR(0x2AA)) = 0x0055;
  *(GET_ADDR(0x555)) = 0x00A0;

  p  = (uint16_t*) addr;
  *p = data;
	
  Status = check_ready_busy(addr, 5000000);
  //Status = check_toggle_ready(addr, 500000);
	
	__enable_irq();
	
	return  (Status);
}

/*****************************************************************************
* @note       Function name	: uint8_t ExtFlash_writeBuff(uint32_t addr, uint16_t* data, uint16_t len)
* @returns    returns				: 1 -> if device programmed successfully
*															0 -> device is not erased
* @brief      Description		: Write buffer
*****************************************************************************/
uint8_t ExtFlash_writeBuff(uint32_t addr, uint16_t* data, uint16_t len)
{
uint16_t i;
  for (i = 0; i < len; i++)
  {
    if (!ExtFlash_writeWord(addr, data[i]))
    {
      return (_FALSE);
    }
  }
  return (_TRUE);
}


/******************************************************************************
 *
 * Description:
 *    Checks if the user defined security segment has been locked or not.
 *    See the user manual for more information.
 *
 * Returns:
 *    TRUE if the segment is unlocked
 *
 *****************************************************************************/
uint8_t ExtFlash_secid_getLockStatus(uint32_t blockAddr)
{
  uint16_t status;

	//tsk_lock();
	__disable_irq();
	
  *(GET_ADDR(0x555)) = 0x00AA;
  *(GET_ADDR(0x2AA)) = 0x0055;
  *(GET_ADDR(0x555)) = 0x0090;

  // read status
  status = *(GET_ADDR((blockAddr << 12) | 0x01));

  *(GET_ADDR(0x555)) = 0x00AA;
  *(GET_ADDR(0x2AA)) = 0x0055;
  *(GET_ADDR(0x555)) = CMD_ID_EXIT;

		__enable_irq();
	//tsk_unlock();
	
  if (!status)
    return _TRUE; // Unprotected
  return _FALSE;  // protected
}


#endif //EXT_FLASH
/*****************************************************************************
* End of file
*****************************************************************************/
