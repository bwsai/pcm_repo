/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    SMTP_DEMO.C
 *      Purpose: SMTP Client demo example
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2011 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <stdio.h>
#include <RTL.h>
#include "LPC177x_8x.h"                 /* LPC177x_8x definitions*/
#include <string.h>
#include "SMTP_demo.h"
#include "RTOS_Main.h"

/* Enter your valid SMTP Server IP address here. */
U8 srv_ip[4] = { 192,168,1,200 };

BOOL tick2;

#ifdef EMAIL
/*---------------------------------------------------------------------------*/
static void smtp_cback (U8 code) 
{
  /* This function is called by TcpNet to inform about SMTP status. */
  switch (code) 
	{
    case SMTP_EVT_SUCCESS:
      printf ("Email successfully sent\n");
      #if 0
		  sent = __TRUE;
		  #endif
      break;
		
    case SMTP_EVT_TIMEOUT:
      /* Timeout, try again. */
      printf ("Mail Server timeout.\n");
      #if 0
      delay = 0;
		#endif
      break;
		
    case SMTP_EVT_ERROR:
      /* Error, try again. */
      printf ("Error sending email.\n");
		#if 0
      delay = 0;
		#endif
      break;
  }
}

/*---------------------------------------------------------------------------*/
__task void email_task (void)
{
  /* Main Thread of the TcpNet */
  while (1) 
	{
		//os_evt_wait_and (0x0001, 0xffff);   /* wait for an event flag 0x0001     */
		os_dly_wait (EMAIL_TASK_INTERVAL);                    /* delay 20 clock ticks               */
		os_itv_wait();                   /* delay 8 clock ticks               */
		smtp_connect ((U8 *)&srv_ip,25,smtp_cback);
  }
}

#endif //#ifdef EMAIL
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
