t <html><head><title>PCM Configuration</title>
t <h2 align=center><br>PCM Configuration</h2>
t <script language=JavaScript>
t function changeConfirm(f){
t  if(!confirm('Are you sure you want to Reboot?')) return;
t  f.submit();
t }
t </script></head>
#t <p><font size="2">Here you can change the PCM <b>Network Settings</b>.
#t  After you have changed the IP address, you need to change also the host IP address in 
#t  you Internet browser to re-connect to target. Make changes with <b>care</b> or you may
#t  permanently lose a connection until next hardware reset.<br><br>
#t  This Form uses a <b>GET</b> method to send data to a Web server.</font></p>
t <form action=PCM_Settings.cgi method=post name=cgi>
t <input type=hidden value="net" name=pg>
t <table border=0 width=99%><font size="3">
t <tr bgcolor=#aaccff>
t  <th width=40%>Item</th>
t  <th width=60%>Setting</th></tr>
# Here begin data setting which is formatted in HTTP_CGI.C module
t <tr><td><img src=pabb.gif>PCM Number</td>
c a a <th width=60%><input type=text name=PCM value="%d" size=18 maxlength=18></th></tr>
t <tr><td><img src=pabb.gif>PCM MAC Address</td>
c a y <th width=60%><input type=text name=MAC value="%x.%x.%x.%x.%x.%x" size=18 maxlength=18></th></tr>
t <tr><td><img src=pabb.gif>PCM IP Address</td>
c a i <th width=60%><input type=text name=ip value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>PCM Subnet Mask</td>
c a m <th width=60%><input type=text name=msk value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>PCM Default Gateway</td>
c a g <th width=60%><input type=text name=gw value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>PCM Primary DNS Server</td>
c a p <th width=60%><input type=text name=pdns value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>PCM Secondary DNS Server</td>
c a s <th width=60%><input type=text name=sdns value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><img src=pabb.gif>PCS IP Address</td>
c a t <th width=60%><input type=text name=ip value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><img src=pabb.gif>PCM Firmware Version</td>
c a v <th width=60%><input type=text name=version value="%s" size=18 maxlength=18></th></tr>
t <tr><td><img src=pabb.gif>PCM PPP IP Address</td>
c a b <th width=60%><input type=text name=ip value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>PCM PPP Default Gateway</td>
c a c <th width=60%><input type=text name=gw value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>PCM PPP Primary DNS Server</td>
c a d <th width=60%><input type=text name=pdns value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>PCM PPP Secondary DNS Server</td>
c a e <th width=60%><input type=text name=sdns value="%d.%d.%d.%d" size=18 maxlength=18></th></tr>
t <tr><td><IMG SRC=pabb.gif>DST</td>
c a w <th width=60%><input type=text name=dst value="%s" size=18 maxlength=18></th></tr>
t </font></table>
# Here begin button definitions
t <p align=center>
t <input type=button name=set value="Reboot" onclick="changeConfirm(this.form)">
#t <input type=reset value="Undo">
t </p></form>
#i pg_footer.inc
. End of script must be closed with period.

