/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    FTPC_UIF.C
 *      Purpose: FTP Client User Interface Module
 *      Rev.:    V4.60
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2012 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <Net_Config.h>
#include <stdio.h>

char ftp_local_file[80];  
const char ftp_user_name[]={"admin"};
const char ftp_password[]={"pcm"};
//const char ftp_path[]={"/"};

//const char ftp_path[]={"/2015/09/"};			//Suvrat testing
char ftp_path[20];	
char ftp_file[80];  
const char ftp_new_name[]={""};
const char ftp_dir_name[]={""};
const char ftp_list_name[]={""};
/*----------------------------------------------------------------------------
 *      FTP Client File Access and Data CallBack Functions
 *---------------------------------------------------------------------------*/

/*--------------------------- ftpc_fopen ------------------------------------*/

void *ftpc_fopen (U8 *mode) {
  /* Open local file for reading or writing. If the return value is NULL, */
  /* processing of FTP Client commands PUT, APPEND or GET is cancelled.   */
  return (fopen ((const char*)ftp_local_file, (char *)mode));
}


/*--------------------------- ftpc_fclose -----------------------------------*/

void ftpc_fclose (void *file) {
  /* Close a local file. */
  fclose (file);
}


/*--------------------------- ftpc_fread ------------------------------------*/

U16 ftpc_fread (void *file, U8 *buf, U16 len) {
  /* Read 'len' bytes from file to buffer 'buf'. Return number of bytes   */
  /* copied. The file will be closed, when the return value is 0.         */
  /* For optimal performance the return value should be 'len'             */
  return (fread (buf, 1, len, file));
}


/*--------------------------- ftpc_fwrite -----------------------------------*/
U16 tmp_len;
U16 ftpc_fwrite (void *file, U8 *buf, U16 len) {
  /* Write 'len' bytes from buffer 'buf' to a file. */
	tmp_len = len;
  return (fwrite (buf, 1, len, file));

}

//U8 ftp_buf[1460];
/*--------------------------- ftpc_cbfunc -----------------------------------*/

U16 ftpc_cbfunc (U8 code, U8 *buf, U16 buflen) {
  /* This function is called by the FTP client to get parameters. It returns*/
  /* the number of bytes written to buffer 'buf'. This function should NEVER*/
  /* write more than 'buflen' bytes to this buffer.                         */
  /* Parameters:                                                            */
  /*   code   - function code with following values:                        */
  /*             0 = Username for FTP authentication                        */
  /*             1 = Password for FTP authentication                        */
  /*             2 = Working directory path for all commands                */
  /*             3 = Filename for PUT, GET, APPEND, DELETE, RENAME command  */
  /*             4 - New filename for RENAME command                        */
  /*             5 - Directory name for MKDIR, RMDIR command                */
  /*             6 - File filter/mask for LIST command (wildcards allowed)  */
  /*             7 = Received directory listing on LIST command             */
  /*   buf    - transmit/receive buffer                                     */
  /*   buflen - length of this buffer                                       */
  /*             on transmit, it specifies size of 'buf'                    */
  /*             on receive, specifies the number of bytes received         */
  U32 i,len = 0;
  
  switch (code) {
    case 0:
      /* Enter Username for login. */
     	len = str_copy (buf, (U8* )ftp_user_name);
			break;

    case 1:
      /* Enter Password for login. */
		  len = str_copy (buf, (U8* )ftp_password);
      break;

    case 2:
      /* Enter a working directory path. */
		  len = str_copy (buf, (U8* )ftp_path);
      break;

    case 3:
      /* Enter a filename for file operations. */
		  len = str_copy (buf, (U8* )ftp_file);
      break;

    case 4:
      /* Enter a new name for rename command. */
			len = str_copy (buf, (U8* )ftp_new_name);
			break;

    case 5:
      /* Enter a directory name to create or remove. */
		  len = str_copy (buf, (U8* )ftp_dir_name);
      break;

    case 6:
      /* Enter a file filter for list command. */
		  len = str_copy (buf, (U8* )ftp_list_name);
      break;

    case 7:
      /* Process received directory listing in raw format. */
      /* Function return value is don't care.              */
      for (i = 0; i < buflen; i++) {
      //  putchar (buf[i]);
			//		ftp_buf[i]=buf[i];
      }
		  
      break;
  }
  return ((U16)len);
}


/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
