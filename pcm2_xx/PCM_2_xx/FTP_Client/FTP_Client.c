/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     : Beltway
*
* @file Filename       : FTP_Client.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid 
*
* @date Created        : December Thursday, 2014  <Dec 11, 2014>
* @date Last Modified  : December Thursday, 2014  <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "RTOS_main.h"
#include "USB_main.h"
#include "rtc.h"
#include "Modbus_Tcp_Master.h"
#include "Ext_flash_high_level.h"
#include "EEPROM_high_level.h"
#include "Main.h"
#include "EMAC_LPC177x_8x.h"
#include "Ext_flash_low_level.h"
#include "HTTP_Client.h"
#include "sntp.h"
#include "mbport.h"
#include "mbm.h"
#include "I2C_driver.h"
#include "../GUI/Application/Screen_global_ex.h"
#include "File_update.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
/*#ifdef DEBUG_PORT
char Fbuff[80];
#endif */

U8 Current_ftp_slave,ftp_send_ctr;
extern char ftp_local_file[80],ftp_file[80];
extern char ftp_path[20]; 
FILE *fp;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
//extern char utc_time[50]; 
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
unsigned char file_request_in_process=0;
void ftpc_notify (U8 event) {
  // Result notification function. 
//	os_evt_set(EVE_FTP_RESP, t_ftp_client);
//	LPC_RTC->ALDOY = 17;
  switch (event) {
    case FTPC_EVT_SUCCESS:
  		#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			//sprintf(Debug_Buf, "FTPC_EVT_SUCCESS Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
			sprintf(Debug_Buf, "FTPC_EVT_SUCCESS Slave %d!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
		  Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_recd = 1;
			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=0;	//by megha on 12/12/2017 for to get file upload ok...
      break;

    case FTPC_EVT_TIMEOUT:
  		#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			//sprintf(Debug_Buf, "FTPC_EVT_TIMEOUT Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
		  sprintf(Debug_Buf, "FTPC_EVT_TIMEOUT Slave %d!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
		LPC_RTC->ALDOY = 11;
					os_mut_wait (&usb_log_mutex, 0xffff);							
		LPC_RTC->ALDOY = 12;			
					sprintf(&system_log.log_buf[0],"FTP server timeout error Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[Current_ftp_slave].Scale_ID,\
		Modbus_Tcp_Slaves[Current_ftp_slave].file_year,Modbus_Tcp_Slaves[Current_ftp_slave].file_month,Modbus_Tcp_Slaves[Current_ftp_slave].file_day);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);		
			//Added by DK on 5 Feb2015 
//			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=1;
			file_request_in_process = 0;
      break;

    case FTPC_EVT_LOGINFAIL:
  		#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			//sprintf(Debug_Buf, "FTPC_EVT_LOGINFAIL Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
		 sprintf(Debug_Buf, "FTPC_EVT_LOGINFAIL Slave %d!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
		  //Added by DK on 5 Feb2015 
		LPC_RTC->ALDOY = 21;
					os_mut_wait (&usb_log_mutex, 0xffff);							
		LPC_RTC->ALDOY = 22;			
					sprintf(&system_log.log_buf[0],"File not available for Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[Current_ftp_slave].Scale_ID,Modbus_Tcp_Slaves[Current_ftp_slave].file_year,Modbus_Tcp_Slaves[Current_ftp_slave].file_month,Modbus_Tcp_Slaves[Current_ftp_slave].file_day);
		
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);			
			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=1;
      break;

    case FTPC_EVT_NOACCESS:
  		#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			//sprintf(Debug_Buf, "FTPC_EVT_NOACCESS Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
		  sprintf(Debug_Buf, "FTPC_EVT_NOACCESS Slave %d!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);	
			#endif
		  //Added by DK on 5 Feb2015 
		LPC_RTC->ALDOY = 23;
					os_mut_wait (&usb_log_mutex, 0xffff);							
		LPC_RTC->ALDOY = 24;			
					sprintf(&system_log.log_buf[0],"File not available for Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[Current_ftp_slave].Scale_ID,Modbus_Tcp_Slaves[Current_ftp_slave].file_year,Modbus_Tcp_Slaves[Current_ftp_slave].file_month,Modbus_Tcp_Slaves[Current_ftp_slave].file_day);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);			
			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=1;
      break;

    case FTPC_EVT_NOTFOUND:
  		/*#ifdef DEBUG_PORT
		  os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "FTPC_EVT_NOTFOUND Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
		  memset(Debug_Buf,'\0',sizeof(Debug_Buf));
		  strcpy(Debug_Buf,ftp_local_file);
		  Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
		  memset(Debug_Buf,'\0',sizeof(Debug_Buf));
		  strcpy(Debug_Buf,ftp_file);
		  Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif*/
		  //Added by DK on 5 Feb2015 
		LPC_RTC->ALDOY = 19;
					os_mut_wait (&usb_log_mutex, 0xffff);							
		LPC_RTC->ALDOY = 20;			
					sprintf(&system_log.log_buf[0],"File not found for Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[Current_ftp_slave].Scale_ID,Modbus_Tcp_Slaves[Current_ftp_slave].file_year,Modbus_Tcp_Slaves[Current_ftp_slave].file_month,Modbus_Tcp_Slaves[Current_ftp_slave].file_day);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);			
			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=1;
      break;

    case FTPC_EVT_NOPATH:
  		#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			//sprintf(Debug_Buf, "FTPC_EVT_NOPATH Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
		  sprintf(Debug_Buf, "FTPC_EVT_NOPATH Slave %d!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
		  //Added by DK on 5 Feb2015 
		LPC_RTC->ALDOY = 25;
					os_mut_wait (&usb_log_mutex, 0xffff);							
		LPC_RTC->ALDOY = 26;			
					sprintf(&system_log.log_buf[0],"File not available for Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[Current_ftp_slave].Scale_ID,Modbus_Tcp_Slaves[Current_ftp_slave].file_year,Modbus_Tcp_Slaves[Current_ftp_slave].file_month,Modbus_Tcp_Slaves[Current_ftp_slave].file_day);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);			
			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=1;
      break;

    case FTPC_EVT_ERRLOCAL:
  		#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			//sprintf(Debug_Buf, "FTPC_EVT_ERRLOCAL Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
			sprintf(Debug_Buf, "FTPC_EVT_ERRLOCAL Slave %d!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
		  //Added by DK on 5 Feb2015 
//			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=1;
		LPC_RTC->ALDOY = 13;
			os_mut_wait (&usb_log_mutex, 0xffff);							
			LPC_RTC->ALDOY = 14;
			sprintf(&system_log.log_buf[0],"FTP local r/w error scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[Current_ftp_slave].Scale_ID,Modbus_Tcp_Slaves[Current_ftp_slave].file_year,Modbus_Tcp_Slaves[Current_ftp_slave].file_month,Modbus_Tcp_Slaves[Current_ftp_slave].file_day);
			log_file_write(system_log1);	
			os_mut_release (&usb_log_mutex);		
      break;

    case FTPC_EVT_ERROR:
  		#ifdef DEBUG_PORT
		  os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			//sprintf(Debug_Buf, "FTPC_EVT_ERROR Slave %d!\r\n",Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID);
		  sprintf(Debug_Buf, "FTPC_EVT_ERROR Slave %d!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[Current_ftp_slave].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
		  //Added by DK on 5 Feb2015 
//			Modbus_Tcp_Slaves[Current_ftp_slave].ftp_file_not_available=1;
		LPC_RTC->ALDOY = 15;
			os_mut_wait (&usb_log_mutex, 0xffff);							
		LPC_RTC->ALDOY = 16;	
			sprintf(&system_log.log_buf[0],"FTP file operation error scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[Current_ftp_slave].Scale_ID,Modbus_Tcp_Slaves[Current_ftp_slave].file_year,Modbus_Tcp_Slaves[Current_ftp_slave].file_month,Modbus_Tcp_Slaves[Current_ftp_slave].file_day);
			log_file_write(system_log1);	
			os_mut_release (&usb_log_mutex);			
      break;
		
		default:
			break;
  }
Modbus_Tcp_Slaves[Current_ftp_slave].file_request_in_process=0;		//by megha for fast file requests from server
os_evt_set(EVE_FTP_RESP, t_ftp_client);	 
}

/*****************************************************************************
* @note       Function name: __task void ftp_client (void)
* @returns    returns      : None
* @param      arg1         : None
* @param      arg2         : None
* @author                  : Dnyaneshwar Kashid 
* @date       date created :
* @brief      Description  : 
* @note       Notes        : None
*****************************************************************************/
__task void ftp_client (void)
{
	int index;
OS_RESULT result;	
	U8 ftp_server_ip[4];
// 	void *file;
// 	file = ftp_local_file;
	while(1)
  {


		os_itv_set(FTP_TASK_INTERVAL);
		os_itv_wait();
		LPC_RTC->GPREG4 |= (0x01<<11);
		//for(index=0; index < MAX_NO_OF_INTEGRATORS; index++)
	//	os_mut_wait (&tcp_mutex, 0xffff);	 //commented by DK on 23 Dec 2014 
//	index = 1;
		for(index=0; index < Total_Scales; index++) //Changed by DK on 23 Feb 2015 	
		{
  		//if((Modbus_Tcp_Slaves[index].Connect == 1) && (Modbus_Tcp_Slaves[index].get_file == 1) )  //Commented by DK on 18May2016 to fix issue of PCM sometimes does not POST the upload to the PCS
      if((Modbus_Tcp_Slaves[index].Connect == 1) && (Modbus_Tcp_Slaves[index].get_file == 1) && \
          (Modbus_Tcp_Slaves[index].ftp_file_recd == 0)&&(file_request_in_process==1)/*&&(Modbus_Tcp_Slaves[index].file_request_in_process==0)*/)  //Changed by DK on 18May2016 to fix issue of PCM sometimes does not POST the upload to the PCS
			
		//	if(Modbus_Tcp_Slaves[index].Connect == 1) //only for testing on 24 Feb 2015 
			{	
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				//sprintf(Debug_Buf, "Just before Mutex in FTP Client\r\n"); //commented by DK on 29 Sep2015 
				sprintf(Debug_Buf, "\r\n Just before Mutex in FTP Client Month:%02d Date:%02d Year:%04d \
				                                                    Hour:%02d Minute:%02d Second:%02d\r\n",\
								Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif		
LPC_RTC->ALDOY = 1;				
////				os_mut_wait (&tcp_mutex, 0xffff);	 //commented by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015 PCM firmware V1.33
LPC_RTC->ALDOY = 2;				
				memset(ftp_local_file,'\0',sizeof(ftp_local_file));
				memset(ftp_file,'\0',sizeof(ftp_file));
				sprintf(ftp_local_file, "Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[index].Scale_ID,Modbus_Tcp_Slaves[index].file_year,Modbus_Tcp_Slaves[index].file_month,Modbus_Tcp_Slaves[index].file_day);
//				 ftpc_fclose (ftp_local_file);
								
			//  sprintf(ftp_file, "%04d-%02d-%02d_periodic_log.txt",2015,2,23); //only for testing on 24 Feb 2015 
				sprintf(ftp_file, "%04d-%02d-%02d_periodic_log.txt",\
					Modbus_Tcp_Slaves[index].file_year,Modbus_Tcp_Slaves[index].file_month,Modbus_Tcp_Slaves[index].file_day);		//Suvrat Folder structure

				Current_ftp_slave = index;
				ftp_server_ip[0]= Modbus_Tcp_Slaves[index].IP_ADRESS[3];
				ftp_server_ip[1]= Modbus_Tcp_Slaves[index].IP_ADRESS[2];
				ftp_server_ip[2]= Modbus_Tcp_Slaves[index].IP_ADRESS[1];
				ftp_server_ip[3]= Modbus_Tcp_Slaves[index].IP_ADRESS[0];
				//By DK on 10 Sep2015
				sprintf(ftp_path, "/%04d/%02d/",Modbus_Tcp_Slaves[index].file_year,Modbus_Tcp_Slaves[index].file_month);
				
				os_evt_clr(EVE_FTP_RESP,t_ftp_client);	
				
				if(ftpc_connect (ftp_server_ip,21,FTPC_CMD_GET, ftpc_notify) == 0)
				{
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "ftp con failed  Slave %d!!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
LPC_RTC->ALDOY = 3;					
					os_mut_wait (&usb_log_mutex, 0xffff);
LPC_RTC->ALDOY = 4;					
					sprintf(&system_log.log_buf[0],"FTP connection failed to scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[index].Scale_ID,
					Modbus_Tcp_Slaves[index].file_year,Modbus_Tcp_Slaves[index].file_month,Modbus_Tcp_Slaves[index].file_day);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);
					
//					os_dly_wait(800);	 
					os_dly_wait(100);
					Modbus_Tcp_Slaves[index].ftp_file_not_available=1;//if ftp connection is failed,send response to server as file not available

				}	
				else 
				{
//					os_dly_wait(100);					
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "ftp client started! Slave %d!!Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
										Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
LPC_RTC->ALDOY = 5;					
					os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALDOY = 6;					
					sprintf(&system_log.log_buf[0],"FTP connection ok to scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[index].Scale_ID,
					Modbus_Tcp_Slaves[index].file_year,Modbus_Tcp_Slaves[index].file_month,Modbus_Tcp_Slaves[index].file_day);
					log_file_write(system_log1);	

					os_mut_release (&usb_log_mutex);
LPC_RTC->ALDOY = 7;
//os_evt_set(EVE_FTP_RESP, t_ftp_client);						
					os_evt_wait_or(EVE_FTP_RESP,0xFFFF );//0xFFFF by megha on 30/9/2017 for delay in records update 
//			os_dly_wait(500);					
LPC_RTC->ALDOY = 8;
							if(result == OS_R_TMO)
							{	
									os_mut_wait (&usb_log_mutex, 0xffff);										
					sprintf(&system_log.log_buf[0],"FTP event not generated for scale %d",Modbus_Tcp_Slaves[index].Scale_ID);
					log_file_write(system_log1);	
									os_mut_release (&usb_log_mutex);								
							}

					
				} //end of else of if(ftpc_connect (ftp_server_ip,0,FTPC_CMD_GET, arFTP_TCP_Callback[index]) == 0)
				//os_evt_wait_or(EVE_FTP_RESP, 0xFFFF); 
				LPC_RTC->ALDOY = 17;
//				os_mut_release (&tcp_mutex);  //Added by DK on 23 Dec 2014  //was Commented by DK on 29 sep 2015 PCM firmware V1.33
				LPC_RTC->ALDOY = 18;
				if(Modbus_Tcp_Slaves[index].ftp_file_recd == 1)
				{
					Modbus_Tcp_Slaves[index].get_file = 0;
				//	Modbus_Tcp_Slaves[index].ftp_file_recd =0; //only for testing on 24 Feb 2015 
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					//sprintf(Debug_Buf, "ftp file %s received from slave %d\r\n", ftp_file,Modbus_Tcp_Slaves[index].Slave_ID); //commented by DK on 29 Sep2015 
					sprintf(Debug_Buf, "ftp file %s received from slave %d Month:%02d Date:%02d Year:%04d \
					                                                       Hour:%02d Minute:%02d Second:%02d\r\n",\
					ftp_file,Modbus_Tcp_Slaves[index].Slave_ID,\
					Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
					Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
LPC_RTC->ALDOY = 9;					
					os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALDOY = 10;					
					sprintf(&system_log.log_buf[0],"File Received from Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[index].Scale_ID,Modbus_Tcp_Slaves[index].file_year,Modbus_Tcp_Slaves[index].file_month,Modbus_Tcp_Slaves[index].file_day);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);
					
				}
				else if(Modbus_Tcp_Slaves[index].ftp_file_not_available==1)
				{
					os_mut_wait (&usb_log_mutex, 0xffff);							
					sprintf(&system_log.log_buf[0],"File Not Received from Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[index].Scale_ID,Modbus_Tcp_Slaves[index].file_year,Modbus_Tcp_Slaves[index].file_month,Modbus_Tcp_Slaves[index].file_day);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);

				}	
				fflush(stdout);  //Added by DK on 24 Feb 2015  
////				os_mut_release (&tcp_mutex);

				//os_dly_wait(2000);	 //Added by DK on 23 Dec 2014 
			} //end of if((Modbus_Tcp_Slaves[index].Connect == 1) && (Modbus_Tcp_Slaves[index].get_file == 1))  
		}//end of for(index=0; index < MAX_NO_OF_INTEGRATORS; index++)
		//os_mut_release (&tcp_mutex);  //Added by DK on 23 Dec 2014  
		//os_dly_wait(5000);	 //Added by DK on 24 Feb 2015 only for testing 
	LPC_RTC->GPREG4 &= ~(0x01<<11);
//		WDTFeed();
} //end of while(1)
}

/*****************************************************************************
* End of file
*****************************************************************************/
