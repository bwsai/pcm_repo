
#include "Global_ex.h"
#include "I2C_driver.h"

//#ifdef DEBUG_PORT

//#define BUFSIZE			(20)

/* Definition of internal register of SC16IS740 */
#define IODIR			0x0A
#define IOSTATE		0x0B
#define IOCON			0x0E

/***********************************************************************
 * @brief 		Get the register address in the forlat specified in datasheet of SC16IS740
 * @param[in]	Register address
 * @return 		The byte to send to slave
 **********************************************************************/
uint8_t GetAddressByte(uint8_t RegAddr)
{
	uint8_t Addr_byte = 0, Channel_Byte = 0;

	Addr_byte = (RegAddr << 3) | Channel_Byte;

	return (Addr_byte);
}

/***********************************************************************
 * @brief 		Initialize SC16IS740
 * @param[in]	i2cClockFreq	I2C clock frequency that SC16IS740 operate
 * @return 		None
 **********************************************************************/
void I2C_high_level_init(uint32_t i2cClockFreq)
{
	I2C_M_SETUP_Type transferCfg;
	uint8_t buffer[2]; 
	uint8_t Rx_Buf = 0;
	Status status;

	// Config Pin for I2C_SDA and I2C_SCL of I2C0
	// It's because the SC16IS740 IC is linked to LPC177x_8x by I2C0 clearly
	PINSEL_ConfigPin (0, 27, 1);
	PINSEL_ConfigPin (0, 28, 1);

	I2C_Init(I2C_0, i2cClockFreq);

	/* Enable I2C0 operation */
	I2C_Cmd(I2C_0, ENABLE);

	//0x80 to program baud rate
	buffer[0] = GetAddressByte(I2C_LCR_REG_ADDR);
	buffer[1] = LCR_LATCH_ENABLE;
	transferCfg.sl_addr7bit = SLAVE_I2CADDR;
	transferCfg.tx_data = buffer;
	transferCfg.tx_length = 2;
	transferCfg.rx_data = NULL;
	transferCfg.rx_length = 0;
	transferCfg.retransmissions_max = 3;
	status = I2C_MasterTransferData((uint8_t)I2C_0, &transferCfg, I2C_TRANSFER_POLLING);
	status = status;
	//DLL = 0 to program baud rate
	buffer[0] = GetAddressByte(I2C_DLL_REG_ADDR);
	buffer[1] = DLL_REG_VALUE_115200;
	transferCfg.sl_addr7bit = SLAVE_I2CADDR;
	transferCfg.tx_data = buffer;
	transferCfg.tx_length = 2;
	transferCfg.rx_data = NULL;
	transferCfg.rx_length = 0;
	transferCfg.retransmissions_max = 3;
	status = I2C_MasterTransferData((uint8_t)I2C_0, &transferCfg, I2C_TRANSFER_POLLING);


	//testing read the same data
	buffer[0] = GetAddressByte(I2C_DLL_REG_ADDR);
	//buffer[1] = LCR_COM_SETTINGS;
	transferCfg.sl_addr7bit = SLAVE_I2CADDR;
	transferCfg.tx_data = NULL;//buffer;
	transferCfg.tx_length = 0;
	transferCfg.rx_data = &Rx_Buf;
	transferCfg.rx_length = 1;
	transferCfg.retransmissions_max = 3;
	status = I2C_MasterTransferData((uint8_t)I2C_0, &transferCfg, I2C_TRANSFER_POLLING);


	//DLM = 0 to program baud rate
	buffer[0] = GetAddressByte(I2C_DLM_REG_ADDR);
	buffer[1] = DLM_REG_VALUE;
	transferCfg.sl_addr7bit = SLAVE_I2CADDR;
	transferCfg.tx_data = buffer;
	transferCfg.tx_length = 2;
	transferCfg.rx_data = NULL;
	transferCfg.rx_length = 0;
	transferCfg.retransmissions_max = 3;
	status = I2C_MasterTransferData((uint8_t)I2C_0, &transferCfg, I2C_TRANSFER_POLLING);

	//Set the communication settings
	buffer[0] = GetAddressByte(I2C_LCR_REG_ADDR);
	buffer[1] = LCR_COM_SETTINGS;
	transferCfg.sl_addr7bit = SLAVE_I2CADDR;
	transferCfg.tx_data = buffer;
	transferCfg.tx_length = 2;
	transferCfg.rx_data = NULL;
	transferCfg.rx_length = 0;
	transferCfg.retransmissions_max = 3;
	status = I2C_MasterTransferData((uint8_t)I2C_0, &transferCfg, I2C_TRANSFER_POLLING);

	//Enable the FIFO
	buffer[0] = GetAddressByte(I2C_FCR_REG_ADDR);
	buffer[1] = FCR_FIFO_ENABLE;
	transferCfg.sl_addr7bit = SLAVE_I2CADDR;
	transferCfg.tx_data = buffer;
	transferCfg.tx_length = 2;
	transferCfg.rx_data = NULL;
	transferCfg.rx_length = 0;
	transferCfg.retransmissions_max = 3;
	status = I2C_MasterTransferData((uint8_t)I2C_0, &transferCfg, I2C_TRANSFER_POLLING);

	//MCR register
	buffer[0] = GetAddressByte(I2C_MCR_REG_ADDR);
	buffer[1] = MCR_REG_VALUE;
	transferCfg.sl_addr7bit = SLAVE_I2CADDR;
	transferCfg.tx_data = buffer;
	transferCfg.tx_length = 2;
	transferCfg.rx_data = NULL;
	transferCfg.rx_length = 0;
	transferCfg.retransmissions_max = 3;
	status = I2C_MasterTransferData((uint8_t)I2C_0, &transferCfg, I2C_TRANSFER_POLLING);

	return;
}

/***********************************************************************
 * @brief 		Close Pca9532
 * @param[in]	None
 * @return 		None
 **********************************************************************/
void I2C_high_level_deinit(void)
{
	/* Enable I2C0 operation */
	I2C_Cmd(I2C_0, DISABLE);

	return;
}

/***********************************************************************
 * @brief 		Control led output
 * @param[in]	settings	Pointer that point to PCA configuration struct
 * @return 		Status, could be:
 * 				- PCA9532_RETFUNC_OK: success
 * 				- PCA9532_RETFUNC_FAILED_OP: fail
 **********************************************************************/
int Uart_i2c_data_send(uint8_t * i2cBuf, uint8_t size)
{
	I2C_M_SETUP_Type i2cData;
	static uint8_t i2c_temp_buf[2];

	//while(*i2cBuf != 0)
	while(size != 0)
	{
		i2c_temp_buf[0] = 0;   //THR register
		i2c_temp_buf[1] = *i2cBuf;   //data
		i2cData.sl_addr7bit = SLAVE_I2CADDR;
		i2cData.tx_data = i2c_temp_buf;
		i2cData.tx_length = 2;//size  + 1;
		i2cData.rx_data = NULL;
		i2cData.rx_length = 0;
		i2cData.retransmissions_max = 3;
		
		I2C_MasterTransferData((uint8_t)I2C_0, &i2cData, I2C_TRANSFER_POLLING);
		i2cBuf++;
		size--;
	}
	
/*
	if(size <= sizeof(i2c_temp_buf))
	{
		memcpy(&i2c_temp_buf[1], i2cBuf, size);
	}
	else
		return 0;

	

	if ()
	{
		return _TRUE;
	}
	else
	{
		return _FALSE;
	}
	*/
	return _TRUE;
}
//#endif //#ifdef DEBUG_PORT
