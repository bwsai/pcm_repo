/*****************************************************************************
* @copyright Copyright (c) 2016-2017 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : DebugLog.h
* @brief			         : Controller Board
*
* @author			         : Aniket
*
* @date Created		     : July Thursday, 2016  <July 08, 2016>
* @date Last Modified	 : July Thursday, 2016  <July 08, 2016>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __DEBUGLOG_H
#define __DEBUGLOG_H



/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdint.h>
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
//#define USB_LOGGING		1


#define DEBUG_TCP_EVENT   5
#define DEBUG_UDP_DATE_EVENT	6
#define DEBUG_PPP_EVENT 7

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/

void Write_Debug_Logs_into_file(int MsgNo,unsigned char *write_ptr,unsigned char write_size);

#endif /* __DEBUGLOG_H */
/*****************************************************************************
* End of file
*****************************************************************************/
