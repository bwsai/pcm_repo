/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : I2C_driver.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 16, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __I2C_DRIVER_H
#define __I2C_DRIVER_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "lpc_types.h"
#include "Global_ex.h"

//#ifdef DEBUG_PORT
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define SLAVE_I2CADDR		(0x90>>1)

#define I2C_THR_REG_ADDR  (0x00)
#define I2C_LCR_REG_ADDR  (0x03)
#define I2C_FCR_REG_ADDR  (0x02)
#define I2C_MCR_REG_ADDR  (0x04)
#define I2C_DLL_REG_ADDR  (0x00)
#define I2C_DLM_REG_ADDR  (0x01)

#define LCR_LATCH_ENABLE  (0x80)
//Value to achieve 9600 baud rate with 1.843Mhz crystal
#define DLL_REG_VALUE_9600   (0x0C)
#define DLL_REG_VALUE_19200  (0x06)
#define DLL_REG_VALUE_38400  (0x03)
#define DLL_REG_VALUE_57600  (0x02)
#define DLL_REG_VALUE_115200 (0x01)

#define DLM_REG_VALUE			(0x00)
//Value for 8 data bits,no parity and 1 stop bit
#define LCR_COM_SETTINGS	(0x03)
//Enable FIFO enable mode
#define FCR_FIFO_ENABLE		(0x01)
//enable divide by 1 clk input and normal uart mode
#define MCR_REG_VALUE			(0x00)
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
//#ifdef DEBUG_PORT
extern __align(4) char Debug_Buf[200] __attribute__ ((section ("PCM_CFG_BUFF")));  //Added by DK on 15 Jan 2015  
//#endif

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern int Uart_i2c_data_send(uint8_t * i2cBuf, uint8_t size);
extern void I2C_high_level_deinit(void);
extern void I2C_high_level_init(uint32_t i2cClockFreq);
//#endif /*DEBUG_PORT*/

#endif /* __I2C_DRIVER_H */
/*****************************************************************************
* End of file
*****************************************************************************/
