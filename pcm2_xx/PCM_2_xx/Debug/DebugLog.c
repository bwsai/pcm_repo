/*****************************************************************************
* @copyright Copyright (c) 2016-2017 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : DebugLog.c
* @brief			         : Controller Board
*
* @author			         : Aniket
*
* @date Created		     : July Thursday, 2016  <July 08, 2016>
* @date Last Modified	 : July Thursday, 2016  <July 08, 2016>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "DebugLog.h"

#include "Global_ex.h"
#include "Main.h"

#include "EEPROM_high_level.h"
#include "lpc177x_8x_iap.h"
#include "File_update.h"


#include "Ext_flash_low_level.h"
#include "Ext_flash_high_level.h"

#include "RTOS_main.h"

#include "math.h"
#include "Global_ex.h"
#include "Modbus_Tcp_Master.h"
#include "mbport.h"
//#include "mbs.h"
#include "mbm.h"
#include "common/mbtypes.h"
//#include "common/mbutils.h"
#include "common/mbportlayer.h"
#include "Http_Client.h"
#include "Main.h"
#include "sntp.h"
#include "rtc.h"
#include "Main.h"
#include "I2C_driver.h"
#include "Modbus_Tcp_Master.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/
extern 	prop_file_ini prop_file;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/


/*****************************************************************************
* @note       Function name : void Write_Debug_Logs_into_file(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : Aniket 
* @date       date created  : 08/07/2016
* @brief      Description   : log FW msg to file 
* @note       Notes         : None
*****************************************************************************/
void Write_Debug_Logs_into_file(int MsgNo,unsigned char *write_ptr,unsigned char write_size)
{
	FILE *fptr;
	char file_name[FILE_NAME_SIZE];
	int size=0;
	unsigned int loop_write_size = 0;
	U8 u8File_exists = 0;
		unsigned int index=0;
	FLOAT_UNION temp;
	
	float Temp_g_current_fw_ver;

	
	if (con == 1)
	{	 
		//error_data_log.Err_data_hdr.Id++;
		
		  strcpy(file_name,"System_Log.txt");
			fptr = fopen (file_name, "r");
			//sks->
		if (fptr==NULL){
				fptr = fopen (file_name, "w");
		}
			fseek (fptr, -1L, SEEK_END);                    // Read the last character from file
			size = ftell(fptr);   														// take a position of file pointer un size variable 
			if (prop_file.Max_size_int ==0)prop_file.Max_size_int = 6000000;
			if (size >prop_file.Max_size_int)	{//62939136
				fclose (fptr);
				fptr = fopen ("System_Log.bak", "r");
				if (fptr != NULL) {
					fclose(fptr);
					fdelete ("System_Log.bak");
				}
				else {
					if (frename ("System_Log.txt", "System_Log.bak") == 0){
						strcpy(file_name,"System_Log.txt");
						fptr = fopen (file_name, "a");
						fclose (fptr);
						fptr = fopen (file_name, "r");
					}
				}
			}
		}
		
		//Check for file present
		if (fptr != NULL)
		{
			u8File_exists = 1;
			fclose(fptr);
		}
		else
		{	
			u8File_exists = 0;
			//fclose(fp);
		}
		fflush(fptr);
		
		//Open file in read or write mode
		if(u8File_exists == 0)
		{
			fptr = fopen (file_name, "w");
		}
		else
		{
			fptr = fopen (file_name, "r");
		}

		if(fptr == NULL)
		{
			return ;
		}
		else
		{
			if(u8File_exists == 0)
			{
				//Added by DK on 2 Feb 2016 
// 				error_data_log.Err_data_hdr.Id = 1;
// 				fprintf(fptr,"Record #\t Date\t Time\t  \tType\t \tMessage\t\r\n");
// 				//fprintf(fp, " %c\t", '0');
// 				fprintf(fptr, " %d\t", error_data_log.Err_data_hdr.Id);
// 				fprintf(fptr, " %04d/%02d/%02d\t", error_data_log.Err_data_hdr.Date.Year,
// 																					error_data_log.Err_data_hdr.Date.Mon,
// 																					error_data_log.Err_data_hdr.Date.Day);
// 				fprintf(fptr, " %02d:%02d:%02d\t", error_data_log.Err_data_hdr.Time.Hr,
// 																					error_data_log.Err_data_hdr.Time.Min,
// 																					error_data_log.Err_data_hdr.Time.Sec);
// 				fprintf(fptr, " %s\t","Scale Info");
// 				fprintf(fptr, "Scale name =%c%s%c, Plant name =%c%s%c, Product name =%c%s%c, Firmware Version %s\t\r\n",\
// 																 '"',Admin_var.Scale_Name,'"',\
// 																 '"',Admin_var.Plant_Name,'"',\
// 																 '"',Admin_var.Product_Name,'"',\
// 																 Admin_var.Int_firmware_version);
			}
			//Else part is Added by PVK 05 Apr 2016
			else
			{
						  
			 //Close the file and open it in append mode
			 if(0 != fclose(fptr))
			 {
			  //		printf ("\nFile could not be closed!\n");
			 }
			 fflush(fptr);
			 
			 
			 //Open file in append mode to write events
			 fptr = fopen (file_name, "a");
			 if(fptr == NULL)
			 {
			 	 return;
			 }
      }//else end				

			
			fprintf(fptr, "PCM firmware Version %s Month:%02d Date:%02d Year:%04d \
                       Hour:%02d  Minute:%02d Second:%02d\r\n",FirmwareVer,Current_time.RTC_Mon,\
                       Current_time.RTC_Mday,Current_time.RTC_Year,Current_time.RTC_Hour,\
                       Current_time.RTC_Min,Current_time.RTC_Sec);
				fprintf(fptr, "PCM ID %03d\r\n",PCM_ID);
					fprintf(fptr, "MAC ID %02X:%02X:%02X:%02X:%02X:%02X\r\n",own_hw_adr[0],own_hw_adr[1],own_hw_adr[2],own_hw_adr[3],own_hw_adr[4],own_hw_adr[5]);		
					
			for(index=0; index < MAX_NO_OF_INTEGRATORS; index++)
			{
					if(Modbus_Tcp_Slaves[index].Connect== FALSE)
					{		
						;//fprintf(fptr, "Modbus_Tcp_Slaves[%d].Connect = %s\r\n",index,"FALSE");
					}	
					else if(Modbus_Tcp_Slaves[index].Connect== TRUE)
					{
							
							fprintf(fptr, "Modbus_Tcp_Slaves[%d].Connect = %s\r\n",index,"TRUE");
							fprintf(fptr, "NO Of Retries[%d] = %d \r\n",index,Modbus_Tcp_Slaves[index].NoOfRetry);
							//Modbus_Tcp_Slaves[index].Wait_Count=0; //Commented by DK on 6 Feb 2015 for PCM to Scale connectivity status update
							fprintf(fptr, "Socket Number[%d]  = %d \r\n",index,		Modbus_Tcp_Slaves[index].socket_number);	
							fprintf(fptr, "Scale connectivity[%d]  = %d \r\n",index,		Modbus_Tcp_Slaves[index].socket_number);	
						if(Modbus_Tcp_Slaves[index].scale_connectivity_status == NOT_CONNECTED)
								fprintf(fptr, "Modbus_Tcp_Slaves[%d].Connect = %s\r\n",index,"NOT CONNECTED");
							else if(Modbus_Tcp_Slaves[index].Connect== TRUE)
								fprintf(fptr, "Modbus_Tcp_Slaves[%d].Connect = %s\r\n",index,"CONNECTED");
							
							fprintf(fptr, "Modbus_Tcp_Slaves[%d].IP ADDR = %d:%d:%d:%d\r\n",index,Modbus_Tcp_Slaves[index].IP_ADRESS[3],Modbus_Tcp_Slaves[index].IP_ADRESS[2],Modbus_Tcp_Slaves[index].IP_ADRESS[1],Modbus_Tcp_Slaves[index].IP_ADRESS[0]);
							fprintf(fptr, "Modbus_Tcp_Slaves[%d].SlaveID = %03d\r\n",index,Modbus_Tcp_Slaves[index].Slave_ID);
						fprintf(fptr, "Modbus_Tcp_Slaves[%d].ScaleID = %03d\r\n",index,Modbus_Tcp_Slaves[index].Scale_ID);
				
							fprintf(fptr, "Scale Date and Time[%d] = %02d:%02d:%02d  %02d:%02d \r\n",index,((Scale_Record_Mb_Reg[index].Scale_Date.int_array[0]&0xFF00) >> 8),(Scale_Record_Mb_Reg[index].Scale_Date.int_array[0]&0x00FF ),(Scale_Record_Mb_Reg[index].Scale_Date.int_array[1] ),(Scale_Record_Mb_Reg[index].Scale_Time.int_array[1] ),(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0] ));

							fprintf(fptr, "Record Value[%d] = %d \r\n",index,(Scale_Record_Mb_Reg[index].Record_Hash.uint_array[0]));				

							fprintf(fptr, "Accumulated Weight[%d] = %d \r\n",index,((Scale_Record_Mb_Reg[index].acc_wt.s_array[0]<<16) | (Scale_Record_Mb_Reg[index].acc_wt.s_array[1])));				
						temp.f_array[0]=Scale_Record_Mb_Reg[index].scale_rate.f_array[1];
							temp.f_array[1]=Scale_Record_Mb_Reg[index].scale_rate.f_array[0];
					fprintf(fptr, "Rate[%d] = %f \r\n",index,temp.float_val);		

						temp.f_array[0]=Scale_Record_Mb_Reg[index].scale_angle.f_array[1];
						temp.f_array[1]=Scale_Record_Mb_Reg[index].scale_angle.f_array[0];
					fprintf(fptr, "Angle[%d] = %f \r\n",index,temp.float_val);		

						temp.f_array[0]=Scale_Record_Mb_Reg[index].belt_speed.f_array[1];
						temp.f_array[1]=Scale_Record_Mb_Reg[index].belt_speed.f_array[0];
					fprintf(fptr, "BeltSpeed[%d] = %f \r\n",index,temp.float_val);		
							
							
							
							
							
// 							
// 							case 'a': //Unique PCM number
//           len = sprintf((char *)buf,(const char *)&env[4],PCM_ID);
// 							
//           break;
// 				case 'y': //PCM MAC Address
//           len = sprintf((char *)buf,(const char *)&env[4],own_hw_adr[0],own_hw_adr[1],own_hw_adr[2],own_hw_adr[3],own_hw_adr[4],own_hw_adr[5]);
//           break;
//         case 'i':  //PCM IP address
//           len = sprintf((char *)buf,(const char *)&env[4],LocM.IpAdr[0],
//                         LocM.IpAdr[1],LocM.IpAdr[2],LocM.IpAdr[3]);
//           break;
//         case 'm':  //PCM IP Subnet mask 
//           len = sprintf((char *)buf,(const char *)&env[4],LocM.NetMask[0],
//                         LocM.NetMask[1],LocM.NetMask[2],LocM.NetMask[3]);
//           break;
//         case 'g':  //PCM IP Gateway address
//           len = sprintf((char *)buf,(const char *)&env[4],LocM.DefGW[0],
//                         LocM.DefGW[1],LocM.DefGW[2],LocM.DefGW[3]);
//           break;
//         case 'p':  //PCM primary DNS server address
//           len = sprintf((char *)buf,(const char *)&env[4],LocM.PriDNS[0],
//                         LocM.PriDNS[1],LocM.PriDNS[2],LocM.PriDNS[3]);
//           break;
//         case 's':  //PCM secondary DNS server address
//           len = sprintf((char *)buf,(const char *)&env[4],LocM.SecDNS[0],
//                         LocM.SecDNS[1],LocM.SecDNS[2],LocM.SecDNS[3]);
//           break;
// 				case 't':  //PCS IP address
//           len = sprintf((char *)buf,(const char *)&env[4],PCS_IpAdr[0],
//                         PCS_IpAdr[1],PCS_IpAdr[2],PCS_IpAdr[3]);
//           break;
// 				case 'v':  //PCM firmware Version 
//           len = sprintf((char *)buf,(const char *)&env[4],FirmwareVer);
//           break;
// 				//Added by DK on 2 Feb 2015 
// 				case 'b':  //PCM PPP IP address
//           len = sprintf((char *)buf,(const char *)&env[4],LocPPP.IpAdr[0],
//                         LocPPP.IpAdr[1],LocPPP.IpAdr[2],LocPPP.IpAdr[3]);
//           break;
//         case 'c':  //PCM PPP IP Gateway address
//           len = sprintf((char *)buf,(const char *)&env[4],LocPPP.DefGW[0],
//                         LocPPP.DefGW[1],LocPPP.DefGW[2],LocPPP.DefGW[3]);
//           break;
//         case 'd':  //PCM PPP primary DNS server address
//           len = sprintf((char *)buf,(const char *)&env[4],LocPPP.PriDNS[0],
//                         LocPPP.PriDNS[1],LocPPP.PriDNS[2],LocPPP.PriDNS[3]);
//           break;
//         case 'e':  //PCM PPP secondary DNS server address
//           len = sprintf((char *)buf,(const char *)&env[4],LocPPP.SecDNS[0],
//                         LocPPP.SecDNS[1],LocPPP.SecDNS[2],LocPPP.SecDNS[3]);
//           break;
// 							
							
							
							
							
							
							
					}
			}		
// 			error_data_log.Err_data_hdr.Id++;
// 			fprintf(fptr, " %d\t", error_data_log.Err_data_hdr.Id);
// 			fprintf(fptr, " %04d/%02d/%02d\t", error_data_log.Err_data_hdr.Date.Year,
// 																error_data_log.Err_data_hdr.Date.Mon,
// 																error_data_log.Err_data_hdr.Date.Day);
// 			fprintf(fptr, " %02d:%02d:%02d\t", error_data_log.Err_data_hdr.Time.Hr,
// 																error_data_log.Err_data_hdr.Time.Min,
// 																error_data_log.Err_data_hdr.Time.Sec);
// 			fprintf(fptr, " %s\t","Event");
			
		//	memset(file_name,0,sizeof(file_name));

//       switch(MsgNo) 
// 	    {
//          case 1:
// 					 fprintf(fptr, "PCM firmware Version %s Month:%02d Date:%02d Year:%04d \
//                       Hour:%02d  Minute:%02d Second:%02d\r\n",FirmwareVer,Current_time.RTC_Mon,\
//                       Current_time.RTC_Mday,Current_time.RTC_Year,Current_time.RTC_Hour,\
//                       Current_time.RTC_Min,Current_time.RTC_Sec);
// 					 //   fprintf(fptr, " %s\r\n","FW Ugrade Function - Before Opening Tot_bkup.txt");
// 							break;
// 				case 2:
// 					    fprintf(fptr, " %s\r\n","FW Ugrade Function - Before Opening ClrWeight_bkup.tx");
// 							break;	
// 				case 3:
// 					    fprintf(fptr, " %s\r\n","FW Ugrade Function - Before Erasing FLASH_CONFIG_SECTOR");
// 							break;	
// 				case 4:
// 					    fprintf(fptr, " %s\r\n","FW Ugrade Function - After Erasing FLASH_CONFIG_SECTOR");
// 							break;
// 				case DEBUG_TCP_EVENT:
// 					    fprintf(fptr, " %02d Date:%02d Year:%04d \
//                       Hour:%02d  Minute:%02d Second:%02d %s\r\n",Current_time.RTC_Mon,\
//                       Current_time.RTC_Mday,Current_time.RTC_Year,Current_time.RTC_Hour,\
//                       Current_time.RTC_Min,Current_time.RTC_Sec,"TCP EVENT");
// 										for(loop_write_size=0;loop_write_size<write_size;loop_write_size++)
// 										{
// 											fprintf(fptr, " %c ",*write_ptr++);
// 										}
// 										fprintf(fptr, "\r\n");
// 							break;
// 				case DEBUG_UDP_DATE_EVENT:
// 					     fprintf(fptr, " %02d Date:%02d Year:%04d \
//                       Hour:%02d  Minute:%02d Second:%02d %s\r\n",Current_time.RTC_Mon,\
//                       Current_time.RTC_Mday,Current_time.RTC_Year,Current_time.RTC_Hour,\
//                       Current_time.RTC_Min,Current_time.RTC_Sec,"UDP EVENT");
// 										for(loop_write_size=0;loop_write_size<write_size;loop_write_size++)
// 										{
// 											fprintf(fptr, " %X ",*write_ptr++);
// 										}
// 										fprintf(fptr, "\r\n");
// 							break;
// 				case DEBUG_PPP_EVENT:
// 					   fprintf(fptr, " %02d Date:%02d Year:%04d \
//                       Hour:%02d  Minute:%02d Second:%02d %s",Current_time.RTC_Mon,\
//                       Current_time.RTC_Mday,Current_time.RTC_Year,Current_time.RTC_Hour,\
//                       Current_time.RTC_Min,Current_time.RTC_Sec,"PPP EVENT\r\n");
// 										for(loop_write_size=0;loop_write_size<write_size;loop_write_size++)
// 										{
// 											fprintf(fptr, " %c",*write_ptr++);
// 										}
// 										fprintf(fptr, "\r\n");
// 							break;
// 				case 8:
// 							fprintf(fptr, " %s\r\n","Modbus TCP Instance1 - Connected");
// 							break;
// 				case 9:
// 							fprintf(fptr, " %s\r\n","Modbus TCP Instance1 - Disconnected");
// 							break;
// 				case 10:
// 							fprintf(fptr, " %s\r\n","Modbus TCP Instance2 - Connected");
// 							break;
// 				case 11:
// 							fprintf(fptr, " %s\r\n","Modbus TCP Instance2 - Disconnected");
// 							break;
// 				case 12:
// 					//->sks : BS205 :: prepare a syslog write when USB has been initialised
// 								fprintf(fptr, " %s\r\n","USB  Mass storage Device - Connected");
// 				//<- 26-05-2016
// 							break;
// // 				case POWER_OFF_SYSLOG_EVENT:
// // 					//->sks
// // 								fprintf(fptr, " %s\r\n","P OFF");
// // 				//<- 13-06-2016
// // 							break;
// 				
// 				default:
// 					break;
//       }				
			
			
			if(ferror(fptr))
			{
				//printf("\n Error log file write error");
			}
			
			if(0 != fclose(fptr))
			{
			//		printf ("\nFile could not be closed!\n");
			}
			
			fflush(fptr);
			memset(file_name,0,sizeof(file_name));
		}	
	}



/*****************************************************************************
* End of file
*****************************************************************************/
