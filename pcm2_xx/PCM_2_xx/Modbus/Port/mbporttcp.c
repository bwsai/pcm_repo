/*
 * File updated by DK on 15 Sep 2014 for PCM functionality - MODBUS TCP master (aka TCP client)
 * MODBUS Library: Bare port
 * Copyright (c) 2011 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbporttcp.c,v 1.1 2011/06/13 19:17:54 embedded-solutions.cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <RTL.h>
#include "RTOS_main.h"
#include "Global_ex.h"
#include <stdio.h>
/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"
#include "Main.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbframe.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"
#include "mbm.h"

#include "Modbus_Tcp_Master.h"
#include "I2C_driver.h"
#include "Http_Client.h"

/* ----------------------- Type definitions ---------------------------------*/
#define IDX_INVALID                         ( 255 )

#define MAX_MASTER_HDLS                     ( MAX_NO_OF_INTEGRATORS ) //Changed By DK on 8 Dec 2014 from 32 to 12 

#ifndef MBP_TCP_DEBUG
#define MBP_TCP_DEBUG                       ( 0 )
#endif

typedef enum
{
    TCP_MODBUS_UNKNOWN,
    TCP_MODBUS_MASTER,
    TCP_MODBUS_SLAVE
} xMBTCPIntHandleType;

#define MBP_TCP_HDL_COMMON \
    UBYTE           ubIdx; \
    xMBTCPIntHandleType eType; \
    BOOL            bIsRunning; \
    xMBHandle       xMBHdl
		
typedef struct
{
    int             iSocket;
    BOOL            bDelete;
    BYTE slave_id;
} xMBPTCPIntClientHandle;

typedef struct
{
    MBP_TCP_HDL_COMMON;
} xMBPTCPIntCommonHandle;

typedef struct
{
  // changed by DK on 8 Dec 2014 as at a time TCP MB master(i.e.PCM) communicates only with one scale (i.e. MB TCP slave)
//		  BYTE            arbClientRcvBuffer[260];
			USHORT          usClientRcvBufferPos;
      USHORT          usClientRcvBufferLen;
    peMBPTCPClientNewDataCB eMBPTCPClientNewDataFN;
    peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFN;
     MBP_TCP_HDL_COMMON;
	  xMBPTCPIntClientHandle xClientCon;
} xMBPTCPIntMasterHandle;


/* ----------------------- Static variables ---------------------------------*/
STATIC xMBPTCPIntMasterHandle xMBTCPMasterHdls[MAX_MASTER_HDLS];
STATIC BOOL     bIsInitalized = FALSE;

//static U64 mb_tcp_Thread_stack[800/8];

// Addd by DK on 8 Dec 2014 as at a time TCP MB master(i.e.PCM) communicates only with one scale (i.e. MB TCP slave)
STATIC BYTE            arbClientRcvBuffer[260];
//STATIC OS_TID arMB_TCP_TID[MAX_MASTER_HDLS];
//STATIC OS_TID MB_TCP_THREAD;
/* ----------------------- Static functions ---------------------------------*/
STATIC void     vMBPTCPInit( void );
/* ----------------------- Public functions ---------------------------------*/
int inet_aton(const char *cp, U32 *addr);
/* ----------------------- Start implementation -----------------------------*/
STATIC void
vMBTCPClientHandleReset( xMBPTCPIntClientHandle * pxClientHdl, BOOL bClose )
{
	if( bClose && ( -1 != pxClientHdl->iSocket ) )
	{
		tcp_abort(pxClientHdl->iSocket); //added by DK on 19 Dec 2014 
		tcp_close( pxClientHdl->iSocket );
		tcp_release_socket(pxClientHdl->iSocket);  
	}
	pxClientHdl->iSocket = -1;
	pxClientHdl->bDelete = FALSE;
}

STATIC void
prvxMBTCPMasterHandleReset( xMBPTCPIntMasterHandle * pxTCPMasterHdl, BOOL bClose,BOOL bFullReset)
{
	//MBP_ASSERT( NULL != pxTCPMasterHdl ); //Commented on 10 Dec 2014 
	//Added on 10 Dec 2014 
	if(pxTCPMasterHdl != NULL)
	{	
		vMBTCPClientHandleReset( &( pxTCPMasterHdl->xClientCon ), bClose );
		if( bFullReset )
		{
			pxTCPMasterHdl->ubIdx = IDX_INVALID;
			pxTCPMasterHdl->bIsRunning =FALSE; 
		//  pxTCPMasterHdl->eType =TCP_MODBUS_UNKNOWN;  //Commented by DK on 8 Dec 2014 
			pxTCPMasterHdl->xMBHdl = MB_HDL_INVALID;
			pxTCPMasterHdl->eMBPTCPClientNewDataFN = NULL;
			pxTCPMasterHdl->eMBPTCPClientDisconnectedFN = NULL;
			//pxTCPMasterHdl->eMBPTCPClientConnectedFN = NULL;
			pxTCPMasterHdl->eType = TCP_MODBUS_MASTER;
		}
		//Added by DK on 8 Dec 2014 
		pxTCPMasterHdl->usClientRcvBufferLen = 0;
		pxTCPMasterHdl->usClientRcvBufferPos = 0;
		memset(arbClientRcvBuffer,0,sizeof(arbClientRcvBuffer));  
	}
}

static U16 MBTCPPortMasterCallback(xMBPTCPIntMasterHandle *pxTCPIntMasterHdl,U8 soc, U8 event, U8 *ptr, U16 par)
{
    eMBErrorCode eStatus;
    U16 retval = 0;
    ( void )eStatus;
    switch (event)
    {
    case TCP_EVT_CONREQ:

        break;
    case TCP_EVT_ABORT:
//				Flags_struct.Connection_flags &= ~ETH_CON_FLAG;			
      eStatus = pxTCPIntMasterHdl->eMBPTCPClientDisconnectedFN( pxTCPIntMasterHdl->xMBHdl, &( pxTCPIntMasterHdl->xClientCon.iSocket ) );
      //abort socket to be changed on 8 Dec 2014 
			//tcp_abort(soc);
		
		  break;
    case TCP_EVT_CONNECT:
//					Flags_struct.Connection_flags |= ETH_CON_FLAG;			
      retval = 1;
      break;
    case TCP_EVT_CLOSE:
//				Flags_struct.Connection_flags &= ~ETH_CON_FLAG;			
      eStatus = pxTCPIntMasterHdl->eMBPTCPClientDisconnectedFN( pxTCPIntMasterHdl->xMBHdl, &( pxTCPIntMasterHdl->xClientCon.iSocket ) );
		  //close socket to be changed on 8 Dec 2014 
		  //tcp_abort(soc);						

		  break;
    case TCP_EVT_ACK:
			retval = 1;
      break;
    case TCP_EVT_DATA:
		  //if( par < MB_UTILS_NARRSIZE( pxTCPIntMasterHdl->arbClientRcvBuffer ))
			if( par < MB_UTILS_NARRSIZE(arbClientRcvBuffer))
      {
			//  memcpy( pxTCPIntMasterHdl->arbClientRcvBuffer, ptr, par );
				memcpy( arbClientRcvBuffer, ptr, par );
			  pxTCPIntMasterHdl->usClientRcvBufferLen = par;
				pxTCPIntMasterHdl->usClientRcvBufferPos = 0;
      }
			break;
// 		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
//       break;

  }
  return retval;
}

/************************************************************************/
STATIC U16 prvxMBTCPPortTCPMasterCallback1 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[0];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
  MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback2 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[1];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par); 
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback3 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[2];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback4 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[3];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback5 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[4];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback6 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[5];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback7 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[6];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback8 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[7];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback9 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[8];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback10 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[9];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback11 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[10];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback12 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[11];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback13 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[12];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback14 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[13];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback15 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[14];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback16 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[15];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

STATIC U16 prvxMBTCPPortTCPMasterCallback17 (U8 soc, U8 event, U8 *ptr, U16 par)
{
	xMBPTCPIntMasterHandle *pxTCPIntMasterHdl = &xMBTCPMasterHdls[16];
	U16 retval = 0;
	MBP_ENTER_CRITICAL_SECTION(  );
	retval=MBTCPPortMasterCallback(pxTCPIntMasterHdl,soc, event, ptr, par);  
	MBP_EXIT_CRITICAL_SECTION(  );	
	return retval;
}

typedef U16 (*ptMBTCP_Callback) (U8 soc, U8 event, U8 *ptr, U16 par);
ptMBTCP_Callback arTCPMasterCallback[MAX_NO_OF_INTEGRATORS]={&prvxMBTCPPortTCPMasterCallback1,\
&prvxMBTCPPortTCPMasterCallback2,&prvxMBTCPPortTCPMasterCallback3,&prvxMBTCPPortTCPMasterCallback4,
&prvxMBTCPPortTCPMasterCallback5,&prvxMBTCPPortTCPMasterCallback6,&prvxMBTCPPortTCPMasterCallback7,
&prvxMBTCPPortTCPMasterCallback8,&prvxMBTCPPortTCPMasterCallback9,&prvxMBTCPPortTCPMasterCallback10,
&prvxMBTCPPortTCPMasterCallback11,&prvxMBTCPPortTCPMasterCallback12,&prvxMBTCPPortTCPMasterCallback13,
&prvxMBTCPPortTCPMasterCallback14,&prvxMBTCPPortTCPMasterCallback15,&prvxMBTCPPortTCPMasterCallback16,	
&prvxMBTCPPortTCPMasterCallback17};

__task void mb_tcp_master_thread (void *pvArg )
{
	xMBPTCPIntMasterHandle *pxTCPMasterIntHdl = pvArg;
  int             iClientSocket;
  U8 sock_state;
	BOOL            bIsRunning = FALSE;
	
	do 
  {
		LPC_RTC->GPREG4 |= (0x01<<6);
		MBP_ENTER_CRITICAL_SECTION( );  
    if( TRUE == (bIsRunning = pxTCPMasterIntHdl->bIsRunning )) 
    {
      if( -1 != pxTCPMasterIntHdl->xClientCon.iSocket )
      {
				iClientSocket = pxTCPMasterIntHdl->xClientCon.iSocket;
        MBP_EXIT_CRITICAL_SECTION(  );   
        sock_state=tcp_get_state (iClientSocket);  
				if(sock_state == TCP_STATE_CONNECT)  
				{			
					MBP_ENTER_CRITICAL_SECTION(  );  
					if( TRUE == ( bIsRunning = pxTCPMasterIntHdl->bIsRunning ) )
          {
						if(( !pxTCPMasterIntHdl->xClientCon.bDelete) && (pxTCPMasterIntHdl->usClientRcvBufferLen > 0) && (NULL != iClientSocket) )
						{
							//MBP_ASSERT( NULL != pxTCPMasterIntHdl->eMBPTCPClientNewDataFN ); //Commented on 10 Dec 2014 
							//Added on 10 Dec 2014 
							if(pxTCPMasterIntHdl->eMBPTCPClientNewDataFN != NULL)
							( void )pxTCPMasterIntHdl->eMBPTCPClientNewDataFN( pxTCPMasterIntHdl->xMBHdl, &iClientSocket);
						}
					}	          
					MBP_EXIT_CRITICAL_SECTION(  );				 
				} 
			}  //end of if( -1 != pxTCPMasterIntHdl->xClientCon.iSocket )
			else
      {
         MBP_EXIT_CRITICAL_SECTION(  ); 
      }
		} //end of if( TRUE == (bIsRunning = pxTCPMasterIntHdl->bIsRunning ))      
		else 
		{
			MBP_EXIT_CRITICAL_SECTION(  );   
			os_tsk_pass();
//			os_dly_wait(5);
		}	
		os_dly_wait(5);
			LPC_RTC->GPREG4 &= ~(0x01<<6);
//		WDTFeed();
	}while (bIsRunning);

	MBP_ENTER_CRITICAL_SECTION( ); 
	prvxMBTCPMasterHandleReset( pxTCPMasterIntHdl, TRUE, TRUE );
	MBP_EXIT_CRITICAL_SECTION ( ); 
	
	os_tsk_delete_self();
}


eMBErrorCode 
eMBPTCPClientInit( xMBPTCPHandle * pxTCPHdl, 
                   xMBHandle xMBHdlArg,
                   peMBPTCPClientNewDataCB eMBPTCPClientNewDATAFNArg, 
									 peMBPTCPClientDisconnectedCB eMBPTCPClientDisconnectedFNArg)
{
	eMBErrorCode    eStatus = MB_EINVAL;
	xMBPTCPIntMasterHandle *pxTCPMasterIntHdl = NULL;
	UBYTE           ubIdx;

	vMBPTCPInit(  );
	if( NULL != pxTCPHdl )
  {
		MBP_ENTER_CRITICAL_SECTION(  ); 
		for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( xMBTCPMasterHdls ); ubIdx++ )
    {
			//if( IDX_INVALID == xMBTCPMasterHdls[ubIdx].ubIdx )   //Uncommented this by DK on 20 Oct 2014 
			{
				//pxTCPMasterIntHdl = &xMBTCPMasterHdls[ubIdx];
				pxTCPMasterIntHdl = &xMBTCPMasterHdls[Current_TCP_Slave];
				prvxMBTCPMasterHandleReset( pxTCPMasterIntHdl, FALSE, TRUE );
				//pxTCPMasterIntHdl->ubIdx = ubIdx;
				pxTCPMasterIntHdl->ubIdx = Current_TCP_Slave;
				break;
			}
    }
		MBP_EXIT_CRITICAL_SECTION(  );   
			
		if( NULL != pxTCPMasterIntHdl )
		{
			pxTCPMasterIntHdl->eMBPTCPClientNewDataFN = eMBPTCPClientNewDATAFNArg;
			pxTCPMasterIntHdl->eMBPTCPClientDisconnectedFN = eMBPTCPClientDisconnectedFNArg;
			pxTCPMasterIntHdl->xMBHdl = xMBHdlArg;
			pxTCPMasterIntHdl->bIsRunning = TRUE;

			pxTCPMasterIntHdl->xClientCon.slave_id = Modbus_Tcp_Slaves[Current_TCP_Slave].Slave_ID;   //added by DK on 22Dec 2014 
				
			//added on 10 Dec 2014
			*pxTCPHdl = pxTCPMasterIntHdl;
			eStatus = MB_ENOERR;
			
			if( MB_ENOERR != eStatus )
			{
				prvxMBTCPMasterHandleReset( pxTCPMasterIntHdl, TRUE, TRUE );
			}
					
		}	
		else
		{
			eStatus = MB_ENORES;
		}
	}
		return eStatus;
}

eMBErrorCode
eMBPTCPClientOpen( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle * pxTCPClientHdl, const CHAR * pcConnectAddress, USHORT usTCPPort )
{
	eMBErrorCode    eStatus = MB_EINVAL;
	xMBPTCPIntMasterHandle *pxTCPMasterIntHdl = xTCPHdl;
	int             iSockAddr=0;
	U32 address;
	U8 rem_ip[4],sock_state;

	inet_aton(pcConnectAddress, &address);
	rem_ip[0]= ((address >> 24) & 0x000000FF); 
	rem_ip[1]= ((address >> 16) & 0x000000FF);  
	rem_ip[2]= ((address >> 8) & 0x000000FF);  
	rem_ip[3]= (address  & 0x000000FF);  
	MBP_ENTER_CRITICAL_SECTION(  );  
	if( MB_IS_VALID_HDL( pxTCPMasterIntHdl, xMBTCPMasterHdls ) && ( pxTCPMasterIntHdl->eType == TCP_MODBUS_MASTER ) )
	{
		MBP_EXIT_CRITICAL_SECTION(  );   
		//if(Current_TCP_Slave < MAX_NO_OF_INTEGRATORS) //commented by DK on 25 Feb 2015 
		if((Current_TCP_Slave < Total_Scales ) && Current_TCP_Slave < MAX_NO_OF_INTEGRATORS ) //Added by DK on 25 Feb 2015 	
		{
			iSockAddr=tcp_get_socket( (TCP_TYPE_CLIENT |TCP_TYPE_KEEP_ALIVE) ,0,30,arTCPMasterCallback[Current_TCP_Slave]);
//			if(!tcp_connect( iSockAddr,rem_ip,usTCPPort,usTCPPort))		//commented  and changed source port on 30th dec 2016 my MSA
			if(!tcp_connect( iSockAddr,rem_ip,usTCPPort,MODBUS_CLIENT_LOCAL_PORT))			
			{
				eStatus = MB_EPORTERR; //uncommented by DK on 22 Oct2014 
				tcp_abort(iSockAddr); //added by DK on 19 Dec 2014 
				tcp_close( iSockAddr);
				tcp_release_socket(iSockAddr); 
			}
			else //connected to MB TCP slave 
			{
				if( -1 != pxTCPMasterIntHdl->xClientCon.iSocket )  //already connected to slave 
				{
					tcp_abort(iSockAddr); //added by DK on 19 Dec 2014 
					tcp_close(iSockAddr);  
					tcp_release_socket(iSockAddr);  
					eStatus = MB_ENORES;
				}
				else
				{
					os_dly_wait(200);  //Changed from 1000 to 200  on 18 Nov2014 
					sock_state=tcp_get_state (iSockAddr);  //Added this by DK on 22 Oct 2014 
					if(sock_state == TCP_STATE_CONNECT)  //Added this by DK on 22 Oct 2014 
					{	
						pxTCPMasterIntHdl->xClientCon.iSocket = iSockAddr;
						*pxTCPClientHdl = &( pxTCPMasterIntHdl->xClientCon );
						eStatus = MB_ENOERR;
						Modbus_Tcp_Slaves[Current_TCP_Slave].socket_number=iSockAddr;
					  pxTCPMasterIntHdl->xClientCon.slave_id = Modbus_Tcp_Slaves[Current_TCP_Slave].Slave_ID;   //added on 10Dec 2014 
						if(Current_TCP_Slave < MAX_MASTER_HDLS)
						{
							os_tsk_create_ex(mb_tcp_master_thread,MB_TCP_M_THREAD_PRIORITY,pxTCPMasterIntHdl);
						}	
					}
					else
					{
						tcp_abort(iSockAddr);  //Added by DK on 19 Dec 2014 
						tcp_close(iSockAddr);  
						tcp_release_socket(iSockAddr);   
						eStatus = MB_ENORES;
					}	
				}
		 } //end of else //connected to MB TCP slave 
		} //end of if(Current_TCP_Slave < MAX_NO_OF_INTEGRATORS)
		else 
		{
			eStatus = MB_ENORES; //uncommented by DK on 22 Oct2014
		}
	}  //end of if( MB_IS_VALID_HDL( pxTCPMasterIntHdl, xMBTCPMasterHdls ) && ( pxTCPMasterIntHdl->eType == TCP_MODBUS_MASTER ) )
	else
	{
		eStatus = MB_EPORTERR;
		MBP_EXIT_CRITICAL_SECTION(  );   
	}
	return eStatus;
}

eMBErrorCode
eMBPTCPClientClose( xMBPTCPHandle xTCPHdl )
{
	eMBErrorCode    eStatus = MB_EINVAL;
	xMBPTCPIntMasterHandle *pxTCPMasterIntHdl = xTCPHdl;

	MBP_ENTER_CRITICAL_SECTION(  ); 
	if( MB_IS_VALID_HDL( pxTCPMasterIntHdl, xMBTCPMasterHdls ) && ( pxTCPMasterIntHdl->eType == TCP_MODBUS_MASTER )) 
	{
		pxTCPMasterIntHdl->xClientCon.bDelete = TRUE;  //Added by DK on 10 Dec 2014 
		pxTCPMasterIntHdl->bIsRunning = FALSE;
		eStatus = MB_ENOERR;
	}
	MBP_EXIT_CRITICAL_SECTION(  ); 

	return eStatus;
}


/************************************************************************/
/*! \brief Close a TCP client connection.
 *
 * Called by the stack when a TCP client connection should be closed.
 *
 * \param xTCPHdl A handle to a TCP server instance.
 * \param xTCPClientHdl A handle for a TCP client which should be closed.
 * \return The function should return eMBErrorCode::MB_ENOERR if the client
 *   connection has been closed. If any of the handles are invalid it should
 *   return eMBErrorCode::MB_EINVAL. All other errors should be mapped to
 *   eMBErrorCode::MB_EPORTERR.
 */
eMBErrorCode
eMBPTCPConClose( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl )
{
	eMBErrorCode    eStatus = MB_EINVAL;
	xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
	xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;
	
	( void )pxTCPIntCommonHdl;

	if( NULL != xTCPClientHdl )
	{
		MBP_ENTER_CRITICAL_SECTION(  ); 
		pxTCPIntClientHdl->bDelete = TRUE;  //commented By DK on 17 Oct2014  uncommented this by DK on 20 Oct 2014 
		pxTCPIntCommonHdl->bIsRunning = FALSE; //Added by DK on 8 Dec 2014 --- to be tested 
		//tcp_close(pxTCPIntClientHdl->iSocket); //Added by DK on 10 Dec 2014 --- to be tested 
		//tcp_release_socket(pxTCPIntClientHdl->iSocket); //Added by DK on 10 Dec 2014 --- to be tested 
		MBP_EXIT_CRITICAL_SECTION(  ); 
		eStatus = MB_ENOERR;
	}
	
	return eStatus;
}


/************************************************************************/
/*! \brief This function is called by the MODBUS stack when new data should
 *    be read from a client.
 *
 * This function must not block and should read up to \c usBufferMax bytes and
 * store them into the buffer \c pubBuffer. The value of \c pusBufferLen should
 * be set to the number of bytes read where 0 is used when there is no data
 * available.
 *
 * \param xTCPHdl A handle to a TCP server instance.
 * \param xTCPClientHdl A handle for a TCP client.
 * \param pubBuffer A buffer of at least \c usBufferMax bytes where the read
 *   bytes from the client should be stored.
 * \param pusBufferLen On return the value should hold the number of bytes
 *   written into the buffer.
 * \param usBufferMax Maximum number of bytes which can be stored in the
 *   buffer.
 * \return The function should return eMBErrorCode::MB_ENOERR if zero or more
 *   bytes have been stored in the buffer and no error occurred. In case of a
 *   client error it should return eMBErrorCode::MB_EIO. In case of an invalid
 *   handle is should return eMBErrorCode::MB_EINVAL. Other errors should be
 *   mapped to eMBErrorCode::MB_EPORTERR signaling the stack that the porting
 *   layer is no longer functional.
 */
eMBErrorCode
eMBPTCPConRead( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, UBYTE * pubBuffer,
                USHORT * pusBufferLen, USHORT usBufferMax )
{
	eMBErrorCode    eStatus = MB_EINVAL;
	USHORT usToCopy;
	xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
	xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;
	xMBPTCPIntMasterHandle *pxTCPMasterIntHdl = xTCPHdl;
	U8 sock_state;

	( void )pxTCPIntCommonHdl;
	MBP_ENTER_CRITICAL_SECTION(  );  //Added by DK on 18 Nov 2014 
	if( NULL != xTCPClientHdl )
	{
		*pusBufferLen = 0;
		sock_state=tcp_get_state(pxTCPIntClientHdl->iSocket);  //Added this by DK on 20 Oct 2014 
		if(sock_state == TCP_STATE_CONNECT)  //Added this by DK on 20 Oct 2014 
		{	
		if( usBufferMax > pxTCPMasterIntHdl->usClientRcvBufferLen )	
		{
			//usToCopy = xMBTCPMasterHdls[0].usClientRcvBufferLen;
			usToCopy = pxTCPMasterIntHdl->usClientRcvBufferLen;
		}
		else
		{
			usToCopy = usBufferMax;
		}
	//	memcpy( pubBuffer, &( pxTCPMasterIntHdl->arbClientRcvBuffer[pxTCPMasterIntHdl->usClientRcvBufferPos] ), usToCopy );
		memcpy( pubBuffer, &(arbClientRcvBuffer[pxTCPMasterIntHdl->usClientRcvBufferPos] ), usToCopy );
		*pusBufferLen = usToCopy;
		//xMBTCPMasterHdls[0].usClientRcvBufferPos += usToCopy;
		pxTCPMasterIntHdl->usClientRcvBufferPos += usToCopy;
		//xMBTCPMasterHdls[0].usClientRcvBufferLen -= usToCopy;
		pxTCPMasterIntHdl->usClientRcvBufferLen -= usToCopy;
		eStatus = MB_ENOERR;
		}
		else eStatus = MB_EIO;  //Added this by DK on 22 Dec 2014 
	}	 
	MBP_EXIT_CRITICAL_SECTION(  );  //Added by DK on 18 Nov 2014 
	return eStatus;
}

/************************************************************************/
/*! \brief This function is called by the MODBUS stack when new data should
 *    be sent over a client connection.
 *
 * This function should not block and should transmit \c usBufferLen bytes over
 * the client connection.
 *
 * \param xTCPHdl A handle to a TCP server instance.
 * \param xTCPClientHdl A handle for a TCP client.
 * \param pubBuffer A buffer of \c usBufferLen bytes which should be transmitted.
 * \param usBufferLen Number of bytes to transmit.
 *
 * \return The function should return eMBErrorCode::MB_ENOERR if all bytes have
 *   been written. In case of an I/O error it should return eMBErrorCode::MB_EIO.
 *   In case of an invalid handle or invalid arguments it should return
 *   eMBErrorCode::MB_EINVAL. All other errors should be mapped to eMBErrorCode::MB_EPORTERR.
 */
eMBErrorCode
eMBPTCPConWrite( xMBPTCPHandle xTCPHdl, xMBPTCPClientHandle xTCPClientHdl, const UBYTE * pubBuffer, USHORT usBufferLen )
{
	eMBErrorCode    eStatus = MB_EINVAL;
	xMBPTCPIntCommonHandle *pxTCPIntCommonHdl = xTCPHdl;
	xMBPTCPIntClientHandle *pxTCPIntClientHdl = xTCPClientHdl;
	U8 *sendbuf;
	U16 maxlen;

 (void)pxTCPIntCommonHdl;
	MBP_ENTER_CRITICAL_SECTION(  );  //Added by DK on 18 Nov 2014 
	if( NULL != xTCPClientHdl )
	{
		if (!tcp_check_send (pxTCPIntClientHdl->iSocket))
		{
			eStatus = MB_EIO;  //Added this by DK on 20 Oct 2014 
		}
		else
		{
					/* The socket is ready to send the data. */
					//maxlen = tcp_max_dsize (xMBTCPSlaveHdls[0].xClientSoc);
			maxlen = tcp_max_dsize (pxTCPIntClientHdl->iSocket);
			if( maxlen < usBufferLen )
			{
				eStatus = MB_EIO;  //Added this by DK on 20 Oct 2014 
			}
			else
			{
				sendbuf = tcp_get_buf (usBufferLen);
				memcpy (sendbuf, pubBuffer, usBufferLen);
				tcp_send (pxTCPIntClientHdl->iSocket, sendbuf, usBufferLen);
				main_TcpNet();
				eStatus = MB_ENOERR;
			}
		 }
	}
	MBP_EXIT_CRITICAL_SECTION(  );  //Added by DK on 18 Nov 2014     
	return eStatus;
}

//Common firmware 

STATIC void
vMBPTCPInit(  )
{
	UBYTE           ubIdx;

	MBP_ENTER_CRITICAL_SECTION(  ); 
	if( !bIsInitalized )
	{
			for( ubIdx = 0; ubIdx < ( UBYTE ) MB_UTILS_NARRSIZE( xMBTCPMasterHdls ); ubIdx++ )
			{
				prvxMBTCPMasterHandleReset( &xMBTCPMasterHdls[ubIdx], FALSE, TRUE );
			}	
			bIsInitalized = TRUE;
	}
	MBP_EXIT_CRITICAL_SECTION(  );  
}


/**
 * Convert an u32_t from host- to network byte order.
 *
 * @param n u32_t in host byte order
 * @return n in network byte order
 */
unsigned int h_to_nl(unsigned int n)
{
  return ((n & 0xff) << 24) |
    ((n & 0xff00) << 8) |
    ((n & 0xff0000UL) >> 8) |
    ((n & 0xff000000UL) >> 24);
}

/**
 * Check whether "cp" is a valid ascii representation
 * of an Internet address and convert to a binary address.
 * Returns 1 if the address is valid, 0 if not.
 * This replaces inet_addr, the return value from which
 * cannot distinguish between failure and a local broadcast address.
 *
 * @param cp IP address in ascii represenation (e.g. "127.0.0.1")
 * @param addr pointer to which to save the ip address in network order
 * @return 1 if cp could be converted to addr, 0 on failure
 */
int inet_aton(const char *cp, U32 *addr)
{
  U32 val;
  U8 base;
  char c;
  U32 parts[4];
  U32 *pp = parts;

  c = *cp;
  for (;;) {
    /*
     * Collect number up to ``.''.
     * Values are specified as for C:
     * 0x=hex, 0=octal, 1-9=decimal.
     */
    if (!(c >= '0' && c <= '9'))
      return (0);
    val = 0;
    base = 10;
    if (c == '0') {
      c = *++cp;
      if (c == 'x' || c == 'X') {
        base = 16;
        c = *++cp;
      } else
        base = 8;
    }
    for (;;) {
      if (c >= '0' && c <= '9') {
        val = (val * base) + (int)(c - '0');
        c = *++cp;
      } else if (base == 16 && ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'))) {
        val = (val << 4) | (int)(c + 10 - ((c >= 'a' && c <= 'z') ? 'a' : 'A'));
        c = *++cp;
      } else
        break;
    }
    if (c == '.') {
      /*
       * Internet format:
       *  a.b.c.d
       *  a.b.c   (with c treated as 16 bits)
       *  a.b (with b treated as 24 bits)
       */
      if (pp >= parts + 3)
        return (0);
      *pp++ = val;
      c = *++cp;
    } else
      break;
  }
  /*
   * Check for trailing characters.
   */
  if (c != '\0' && !(c == ' ' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v'))
    return (0);
  /*
   * Concoct the address according to
   * the number of parts specified.
   */
  switch (pp - parts + 1) {

  case 0:
    return (0);       /* initial nondigit */

  case 1:             /* a -- 32 bits */
    break;

  case 2:             /* a.b -- 8.24 bits */
    if (val > 0xffffffUL)
      return (0);
    val |= parts[0] << 24;
    break;

  case 3:             /* a.b.c -- 8.8.16 bits */
    if (val > 0xffff)
      return (0);
    val |= (parts[0] << 24) | (parts[1] << 16);
    break;

  case 4:             /* a.b.c.d -- 8.8.8.8 bits */
    if (val > 0xff)
      return (0);
    val |= (parts[0] << 24) | (parts[1] << 16) | (parts[2] << 8);
    break;
  }
 // if (*addr)//Commented by DK on 12 Nov2014 
    *addr = h_to_nl(val);
  return (1);
}

/**
 * Convert numeric IP address into decimal dotted ASCII representation.
 * returns ptr to static buffer; not reentrant!
 *
 * @param addr ip address in network order to convert
 * @return pointer to a global static (!) buffer that holds the ASCII
 *         represenation of addr
 */
char *inet_ntoa(U32 addr)
{
  static char str[16];
  U32 s_addr = addr;
  char inv[3];
  char *rp;
  U8 *ap;
  U8 rem;
  U8 n;
  U8 i;

  rp = str;
  ap = (U8 *)&s_addr;
  for(n = 0; n < 4; n++) {
    i = 0;
    do {
      rem = *ap % (U8)10;
      *ap /= (U8)10;
      inv[i++] = '0' + rem;
    } while(*ap);
    while(i--)
      *rp++ = inv[i];
    *rp++ = '.';
    ap++;
  }
  *--rp = 0;
  return str;
}
