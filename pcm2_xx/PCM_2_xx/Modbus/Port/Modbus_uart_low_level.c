/**********************************************************************
* $Id$    Uart_Interrupt.c      2011-06-02
*//**
* @file    Uart_Interrupt.c
* @brief  This example describes how to using UART in interrupt mode
* @version  1.0
* @date    02. June. 2011
* @author  NXP MCU SW Application Team
*
* Copyright(C) 2011, NXP Semiconductor
* All rights reserved.
*
***********************************************************************
* Software that is described herein is for illustrative purposes only
* which provides customers with programming information regarding the
* products. This software is supplied "AS IS" without any warranties.
* NXP Semiconductors assumes no responsibility or liability for the
* use of the software, conveys no license or title under any patent,
* copyright, or mask work right to the product. NXP Semiconductors
* reserves the right to make changes in the software without
* notification. NXP Semiconductors also make no representation or
* warranty that such application will be suitable for the specified
* use without further testing or modification.
**********************************************************************/

#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_pinsel.h"
#include "Modbus_uart_low_level.h"

MODBUS_RTU_VAR_STRUCT Modbus_rtu_var;
/*********************************************************************//**
 * @brief    c_entry: Main UART program body
 * @param[in]  None
 * @return     int
 **********************************************************************/
int modbus_uart_init(int uart2_4)
{
    // UART Configuration structure variable
    UART_CFG_Type UARTConfigStruct;
    // UART FIFO configuration Struct variable
    UART_FIFO_CFG_Type UARTFIFOConfigStruct;

    __IO FlagStatus exitflag;

    if (uart2_4 == SENSOR_BOARD) //configure the port for the sensor board(UART 2)
    {
        /*
         * Initialize UART2 pin connect
         * P0.10: U2_TXD
         * P0.11: U2_RXD
         * P1.19: nRE - RS485 chip(I/O)[Enable Receive]
         * P0.19: DE  - RS485 chip(I/O)[Enable Transmit]
         */
        PINSEL_ConfigPin(0,10,1);
        PINSEL_ConfigPin(0,11,1);
        PINSEL_ConfigPin(1,19,0);
        PINSEL_ConfigPin(0,19,0);

        LPC_GPIO1->DIR |= (1UL<<19); //Set nRE to output
        LPC_GPIO1->CLR |= (1UL<<19); //Clear nRE to enable receive

        LPC_GPIO0->DIR |= (1UL<<19); //Set DE to output
        LPC_GPIO0->SET |= (1UL<<19); //Set DE to 1 for transmitting

        /* Initialize UART Configuration parameter structure to default state:
         * Baudrate = 921600bps
         * 8 data bit
         * 2 Stop bits
         * None parity
         */
        UARTConfigStruct.Baud_rate = 921600;
        UARTConfigStruct.Databits  = UART_DATABIT_8;
        UARTConfigStruct.Parity    = UART_PARITY_NONE;
        UARTConfigStruct.Stopbits  = UART_STOPBIT_2;

        // Initialize UART0 peripheral with given to corresponding parameter
        UART_Init((LPC_UART_TypeDef *)_SENSOR_UART, &UARTConfigStruct);

        /* Initialize FIFOConfigStruct to default state:
         *         - FIFO_DMAMode = DISABLE
         *         - FIFO_Level = UART_FIFO_TRGLEV0
         *         - FIFO_ResetRxBuf = ENABLE
         *         - FIFO_ResetTxBuf = ENABLE
         *         - FIFO_State = ENABLE
         */
        UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

        // Initialize FIFO for UART2 peripheral
        UART_FIFOConfig((LPC_UART_TypeDef *)_SENSOR_UART, &UARTFIFOConfigStruct);


        // Enable UART Transmit
        UART_TxCmd((LPC_UART_TypeDef *)_SENSOR_UART, ENABLE);

          /* Enable UART Rx interrupt */
        UART_IntConfig((LPC_UART_TypeDef *)_SENSOR_UART, UART_INTCFG_RBR, ENABLE);
        /* Enable UART line status interrupt */
        UART_IntConfig((LPC_UART_TypeDef *)_SENSOR_UART, UART_INTCFG_RLS, DISABLE);

        UART_IntConfig((LPC_UART_TypeDef *)_SENSOR_UART, UART_INTCFG_THRE, ENABLE);

        /* preemption = 1, sub-priority = 1 */
        NVIC_SetPriority(_SENSOR_UART_IRQ, 0x22);

        /* Enable Interrupt for UART0 channel */
        NVIC_EnableIRQ(_SENSOR_UART_IRQ);
    }
    else if (uart2_4 == INTEGRATOR_BOARD) //configure the port for the integrator board (UART 4)
    {
        /*
         * Initialize UART4 pin connect
         * P0.22: U4_TXD
         * P5.3 : U4_RXD
         * P0.21: nRE - RS485 chip(I/O)[Enable Receive]
         * P0.20: DE - RS485 chip(I/O)[Enable Transmit]
         */
        PINSEL_ConfigPin(0,22,3);
        PINSEL_ConfigPin(5,3,4);
        PINSEL_ConfigPin(0,21,0);
        PINSEL_ConfigPin(0,20,0);

        LPC_GPIO0->DIR |= ((1UL<<21) | (1UL<<20)); //Set nRE, DE to output

        LPC_GPIO0->CLR |= (1UL<<21); //Clear nRE to enable receive
        LPC_GPIO0->SET |= (1UL<<20); //Set DE to 1 for transmitting

        /* Initialize UART Configuration parameter structure to default state:
         * Baudrate = 921600bps
         * 8 data bit
         * 2 Stop bits
         * None parity
         */
        UARTConfigStruct.Baud_rate = 921600;
        UARTConfigStruct.Databits  = UART_DATABIT_8;
        UARTConfigStruct.Parity    = UART_PARITY_NONE;
        UARTConfigStruct.Stopbits  = UART_STOPBIT_2;

        // Initialize UART0 peripheral with given to corresponding parameter
        UART_Init((LPC_UART_TypeDef *)LPC_UART4, &UARTConfigStruct);

        /* Initialize FIFOConfigStruct to default state:
         *         - FIFO_DMAMode = DISABLE
         *         - FIFO_Level = UART_FIFO_TRGLEV0
         *         - FIFO_ResetRxBuf = ENABLE
         *         - FIFO_ResetTxBuf = ENABLE
         *         - FIFO_State = ENABLE
         */
        UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

        // Initialize FIFO for UART0 peripheral
        UART_FIFOConfig((LPC_UART_TypeDef *)LPC_UART4, &UARTFIFOConfigStruct);
        LPC_UART4->FCR = 0x01;

        // Enable UART Transmit
        UART_TxCmd((LPC_UART_TypeDef *)LPC_UART4, ENABLE);

          /* Enable UART Rx interrupt */
        UART_IntConfig((LPC_UART_TypeDef *)LPC_UART4, UART_INTCFG_RBR, ENABLE);
        /* Disable UART line status interrupt */
        UART_IntConfig((LPC_UART_TypeDef *)LPC_UART4, UART_INTCFG_RLS, DISABLE);

        UART_IntConfig((LPC_UART_TypeDef *)LPC_UART4, UART_INTCFG_THRE, ENABLE);

        /* preemption = 1, sub-priority = 1 */
        NVIC_SetPriority(_INTEGRATOR_UART_IRQ, 0x30);

        /* Enable Interrupt for UART0 channel */
        NVIC_EnableIRQ(_INTEGRATOR_UART_IRQ);
    }

    return 1;
}

