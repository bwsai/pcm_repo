#include "lpc177x_8x_timer.h"
#include "lpc177x_8x_pinsel.h"


 //Timer preset
#define BRD_TIMER_USED				(LPC_TIM0)
#define BRD_TIM_INTR_USED			(TIMER0_IRQn)
#define BRD_TIM_CAP_LINKED_PORT		(1)
#define BRD_TIM_CAP_LINKED_PIN		(26) 

#define BRD_TIMER_USED1				(LPC_TIM1)
#define BRD_TIM_INTR_USED1			(TIMER1_IRQn)


extern int c_entry(void);
extern int c_entry1(void);
