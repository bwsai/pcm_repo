/**********************************************************************
* $Id$    Timer_MatchInterrupt.c      2011-06-02
*//**
* @file    Timer_MatchInterrupt.c
* @brief  This example describes how to use TIMER in interrupt mode
* @version  1.0
* @date    02. June. 2011
* @author  NXP MCU SW Application Team
*
* Copyright(C) 2011, NXP Semiconductor
* All rights reserved.
*
***********************************************************************
* Software that is described herein is for illustrative purposes only
* which provides customers with programming information regarding the
* products. This software is supplied "AS IS" without any warranties.
* NXP Semiconductors assumes no responsibility or liability for the
* use of the software, conveys no license or title under any patent,
* copyright, or mask work right to the product. NXP Semiconductors
* reserves the right to make changes in the software without
* notification. NXP Semiconductors also make no representation or
* warranty that such application will be suitable for the specified
* use without further testing or modification.
**********************************************************************/

#include "lpc177x_8x_timer.h"
#include "lpc177x_8x_pinsel.h"
#include "Timer_MatchInterrupt.h"


/** @defgroup TIMER_MatchInterrupt    Timer Match Interrupt
 * @ingroup TIMER_Examples
 * @{
 */

/************************** PRIVATE VARIABLES *************************/

//timer init
TIM_TIMERCFG_Type TIM_ConfigStruct;
TIM_MATCHCFG_Type TIM_MatchConfigStruct ;
uint8_t volatile timer0_flag = _FALSE, timer1_flag = _FALSE;
FunctionalState LEDStatus = ENABLE;

/*-------------------------MAIN FUNCTION------------------------------*/
/*********************************************************************//**
 * @brief    c_entry: Main TIMER program body
 * @param[in]  None
 * @return     int
 **********************************************************************/
int c_entry(void)
{
  //LED_Init();
  // Conifg P1.28 as MAT0.0
  PINSEL_ConfigPin(BRD_TIM_CAP_LINKED_PORT, BRD_TIM_CAP_LINKED_PIN, 3);

  // Initialize timer 0, prescale count time of 100uS
  TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
  TIM_ConfigStruct.PrescaleValue  = 100;

  // use channel 0, MR0
  TIM_MatchConfigStruct.MatchChannel = 0;
  // Enable interrupt when MR0 matches the value in TC register
  TIM_MatchConfigStruct.IntOnMatch   = _TRUE;
  //Enable reset on MR0: TIMER will reset if MR0 matches it
  TIM_MatchConfigStruct.ResetOnMatch = _TRUE;
  //Stop on MR0 if MR0 matches it
  TIM_MatchConfigStruct.StopOnMatch  = _FALSE;
  //Toggle MR0.0 pin if MR0 matches it
  TIM_MatchConfigStruct.ExtMatchOutputType =TIM_EXTMATCH_TOGGLE;
  // Set Match value, count value of 10000 (10000 * 100uS = 1000000us = 1s --> 1 Hz)
  TIM_MatchConfigStruct.MatchValue   = 10;

  // Set configuration for Tim_config and Tim_MatchConfig
  TIM_Init(BRD_TIMER_USED, TIM_TIMER_MODE, &TIM_ConfigStruct);
  TIM_ConfigMatch(BRD_TIMER_USED, &TIM_MatchConfigStruct);

  /* preemption = 1, sub-priority = 1 */
  NVIC_SetPriority(BRD_TIM_INTR_USED, 0x21);

  /* Enable interrupt for timer 0 */
  NVIC_EnableIRQ(BRD_TIM_INTR_USED);

  // To start timer
  TIM_Cmd(BRD_TIMER_USED, ENABLE);

  //while (1);
  return 1;

}
