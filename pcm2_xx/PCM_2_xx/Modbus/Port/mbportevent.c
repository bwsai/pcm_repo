/* 
 * MODBUS Library: LPC2388/lwIP  port
 * Copyright (c) 2011 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbportevent.c,v 1.1 2011/06/13 19:17:54 embedded-solutions.cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
/* ----------------------- Platform includes --------------------------------*/
#include "mbport.h"
#include "Rtos_main.h"


/* ----------------------- Modbus includes ----------------------------------*/
#include "common/mbtypes.h"
#include "common/mbportlayer.h"
#include "common/mbframe.h"
#include "common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define MAX_EVENT_HDLS          ( 32 )  //Changed by DK from 5 to 32 on 12 Jan 2015 
#define IDX_INVALID             ( 255 )
#define EV_NONE                 ( 0 )
//#define MB_QUEUE_EVENT         	( 0x0100 )
#define MB_QUEUE_EVENT        	( 0x0001 )  //added by DK on 3 Dec 2014 
#define EV_WAIT_TICKS           ( 200 )  //Changed from 100 to 200 ticks by DK on 15 Jan2015

#define HDL_RESET( x ) do { \
    ( x )->ubIdx = IDX_INVALID; \
		( x )->xType = EV_NONE; \
} while( 0 )

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
		xMBPEventType   xType;
} xEventInternalHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC BOOL     bIsInitialized = FALSE;
STATIC xEventInternalHandle arxEventHdls[MAX_EVENT_HDLS];

/* ----------------------- Static functions ---------------------------------*/

/*------------------------------------------------------------------------*/
/* ----------------------- Start implementation -----------------------------*/

eMBErrorCode
eMBPEventCreate( xMBPEventHandle * pxEventHdl )
{
    /* Thread: 
     *  - Multiple MODBUS core threads from different handles
     * Protection: Full 
     */
    eMBErrorCode    eStatus = MB_EINVAL;
    UBYTE           i;
	
    if( NULL != pxEventHdl )
    {	
        MBP_ENTER_CRITICAL_SECTION(  );  
        if( !bIsInitialized )
        {
            for( i = 0; i < MAX_EVENT_HDLS; i++ )
            {
                HDL_RESET( &arxEventHdls[i] );								
            }
            bIsInitialized = TRUE;
        }
        for( i = 0; i < MAX_EVENT_HDLS; i++ )
        {
            if( IDX_INVALID == arxEventHdls[i].ubIdx )
            {                
								arxEventHdls[i].ubIdx = i;      
								arxEventHdls[i].xType = EV_NONE;
								*pxEventHdl = &arxEventHdls[i];
                eStatus = MB_ENOERR;
                break;
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  ); 
    }
    return eStatus;
}


eMBErrorCode
eMBPEventPost( const xMBPEventHandle xEventHdl, xMBPEventType xEvent )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xEventInternalHandle *pxEventHdl = xEventHdl;

    MBP_ENTER_CRITICAL_SECTION(  ); 
    if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {
			//  os_evt_set((U16)( MB_QUEUE_EVENT << pxEventHdl->ubIdx ),t_modbus_tcp_master);	//Added by DK to signal event on 3 Oct2014
			  pxEventHdl->xType = xEvent;
				eStatus = MB_ENOERR;
				os_evt_set((U16)( MB_QUEUE_EVENT << pxEventHdl->ubIdx ),t_modbus_tcp_master);	//Added by DK to signal event on 3 Dec2014
		}
    MBP_EXIT_CRITICAL_SECTION(  );  
    return eStatus;
}

BOOL
bMBPEventGet( const xMBPEventHandle xEventHdl, xMBPEventType * pxEvent )
{
    BOOL            bEventInQueue = FALSE;
    xEventInternalHandle *pxEventHdl = xEventHdl;
	
	//Added by DK on 3 Oct2014
		UBYTE           ubIdx;
		OS_RESULT result;
//	  U16 events;

    MBP_ENTER_CRITICAL_SECTION(  );  
    
		if( MB_IS_VALID_HDL( pxEventHdl, arxEventHdls ) )
    {	
			  ubIdx = pxEventHdl->ubIdx;
				MBP_EXIT_CRITICAL_SECTION(  ); 
		//		events=(U16)(MB_QUEUE_EVENT << ubIdx);
		//	  result =   os_evt_wait_or(events,EV_WAIT_TICKS);			
				result =   os_evt_wait_or((MB_QUEUE_EVENT << ubIdx),EV_WAIT_TICKS);			
				if(result == OS_R_EVT)
				{	
			    MBP_ENTER_CRITICAL_SECTION(  );  
					if( EV_NONE != pxEventHdl->xType ) 
					{
						bEventInQueue = TRUE;
						*pxEvent = pxEventHdl->xType;
						pxEventHdl->xType = EV_NONE;
					}
					MBP_EXIT_CRITICAL_SECTION(  ); 
			 }
    }
		else 
		{	
			MBP_EXIT_CRITICAL_SECTION(  ); 
		}
		
    return bEventInQueue;
}

void
vMBPEventDelete( xMBPEventHandle xEventHdl )
{
    xEventInternalHandle *pxEventIntHdl = xEventHdl;

    MBP_ENTER_CRITICAL_SECTION(  ); 
    if( MB_IS_VALID_HDL( pxEventIntHdl, arxEventHdls ) )
    {
      os_evt_clr( (U16)( MB_QUEUE_EVENT << pxEventIntHdl->ubIdx ),t_modbus_tcp_master);  
			HDL_RESET( pxEventIntHdl );
    }
    MBP_EXIT_CRITICAL_SECTION(  ); 
}

