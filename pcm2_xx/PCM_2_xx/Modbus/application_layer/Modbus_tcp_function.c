#if 0
else if( MB_ENOERR != ( eStatus = eMBSRegisterInputCB( xMBSHdl, eMyRegInputCB ) ) )
        {
            ( void )eMBSClose( xMBSHdl );
        }*/
        else if( MB_ENOERR != ( eStatus = eMBSRegisterHoldingCB( xMBSHdl, eMyRegHoldingCB ) ) )
        {
            ( void )eMBSClose( xMBSHdl );
        }
        /*else if( MB_ENOERR != ( eStatus = eMBSRegisterDiscreteCB( xMBSHdl, eMyDiscInputCB ) ) )
        {
            ( void )eMBSClose( xMBSHdl );
        }*/
				
	/*! \brief Register a function callback for coils.
	 *
	 * \param xHdl A handle to a MODBUS slave instance.
	 * \param peMBSCoilsCB A pointer to a function. This function is called
	 *   whenever a coil is read or written. Use \c NULL to remove the 
	 *   previous callback.
	 *
	 * \return eMBErrorCode::MB_ENOERR if the input callback has been set or removed.
	 *   eMBErrorCode::MB_EINVAL if the handle is not valid.
	 */
	eMBErrorCode    eMBSRegisterCoilCB( xMBSHandle xHdl, peMBSCoilCB peMBSCoilsCB );
			
	/*! \brief Callback function if a <em>coil register</em> is read or
	 *    written by the protocol stack.
	 *
	 * The callback may read or write up to 2000 coils where the first coil
	 * is address by the parameter \c usAddress. If the coils are read by
	 * the protocol stack the first coil should be written to the buffer 
	 * \c pubRegBuffer where the first 8 coils should be written to 
	 * pubRegBuffer[0], the second 8 coils to pubRegBuffer[1], ... If the
	 * total amount is not a multiple of 8 the missing coils should be
	 * set to zero.<br>
	 * If the coils are written by the protocol stack, which is indicated
	 * by the argument \c eRegMode set to eMBSRegisterMode::MBS_REGISTER_WRITE
	 * , then the callback should update its coils with the values supplied
	 * in \c pubRegBuffer. The enconding is the same as above. 
	 *
	 * \param pubRegBuffer If the values are read by the stack the callback
	 *   should update the buffer. Otherwise the callback can read the new
	 *   status of the coils from this buffer.
	 * \param usAddress Address of first coil.
	 * \param usNRegs Number of coils.
	 * \param eRegMode If set to eMBSRegisterMode::MBS_REGISTER_READ the 
	 *   coils are read by the protocol stack. In case of 
	 *   eMBSRegisterMode::MBS_REGISTER_WRITE the protocol stack needs to
	 *   know the current value of the coil register.
	 * \return If the callback returns eMBException::MB_PDU_EX_NONE a response is
	 *   sent back to the master. Otherwise an appropriate exception frame is
	 *   generated.
	 */
	#if MBS_CALLBACK_ENABLE_CONTEXT == 1
	typedef         eMBException( *peMBSCoilCB ) ( void *pvCtx, UBYTE * pubRegBuffer, USHORT usAddress,
																								 USHORT usNRegs, eMBSRegisterMode eRegMode )MB_CDECL_SUFFIX;
	#else
	typedef         eMBException( *peMBSCoilCB ) ( UBYTE * pubRegBuffer, USHORT usAddress,
																								 USHORT usNRegs, eMBSRegisterMode eRegMode ) MB_CDECL_SUFFIX;
	#endif
				
#endif

STATIC eMBException eMyRegInputCB( UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs );
STATIC eMBException eMyRegHoldingCB( UBYTE * pubRegBuffer, USHORT usAddress,
                                     USHORT usNRegs, eMBSRegisterMode eRegMode );
STATIC eMBException eMyCoilCB( UBYTE * pubRegBuffer, USHORT usAddress,
                                     USHORT usNRegs, eMBSRegisterMode eRegMode );
				
				
//-----------------------------------HOLDING REGISTER------------------------------------------------
eMBException
eMyRegHoldingCB( UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs, eMBSRegisterMode eRegMode )
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    STATIC const ULONG usRegsMappedAt = 0x0200;
    ULONG           usRegStart = usAddress;
    ULONG           usRegEnd = usAddress + usNRegs - 1;
    USHORT          usIndex;
    USHORT          usIndexEnd;

    if( ( usNRegs > 0 ) &&
        ( usRegStart >= usRegsMappedAt )
        && ( usRegEnd <= ( usRegsMappedAt + MB_UTILS_NARRSIZE( usRegHoldingValue ) ) ) )
    {
        usIndex = ( USHORT ) ( usRegStart - usRegsMappedAt );
        usIndexEnd = ( USHORT ) ( usRegEnd - usRegsMappedAt );
        switch ( eRegMode )
        {
        case MBS_REGISTER_WRITE:
            for( ; usIndex <= usIndexEnd; usIndex++ )
            {
                usRegHoldingValue[usIndex] = ( USHORT ) * pubRegBuffer++ << 8;
                usRegHoldingValue[usIndex] |= ( USHORT ) * pubRegBuffer++;
            }
            break;

        default:
        case MBS_REGISTER_READ:

            for( ; usIndex <= usIndexEnd; usIndex++ )
            {
                *pubRegBuffer++ = ( UBYTE ) ( usRegHoldingValue[usIndex] >> 8 );
                *pubRegBuffer++ = ( UBYTE ) ( usRegHoldingValue[usIndex] & 0xFF );
            }
            break;
        }
        eException = MB_PDU_EX_NONE;
    }
    return eException;
}

//--------------------------------------INPUT REGISTER-------------------------------------------
eMBException
eMyRegInputCB( UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs )
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    STATIC const ULONG usRegsMappedAt = 0x0100;
    ULONG           usRegStart = usAddress;
    ULONG           usRegEnd = usAddress + usNRegs - 1;
    USHORT          usIndex;
    USHORT          usIndexEnd;

    if( ( usNRegs > 0 ) &&
        ( usRegStart >= usRegsMappedAt ) && ( usRegEnd <= ( usRegsMappedAt + MB_UTILS_NARRSIZE( usRegInputValue ) ) ) )
    {
        usIndex = ( USHORT ) ( usRegStart - usRegsMappedAt );
        usIndexEnd = ( USHORT ) ( usRegEnd - usRegsMappedAt );
        for( ; usIndex <= usIndexEnd; usIndex++ )
        {
            *pubRegBuffer++ = ( UBYTE ) ( usRegInputValue[usIndex] >> 8 );
            *pubRegBuffer++ = ( UBYTE ) ( usRegInputValue[usIndex] & 0xFF );
        }
        eException = MB_PDU_EX_NONE;
    }
    return eException;
}

//-------------------------------------WRITE COIL---------------------------------------------------------