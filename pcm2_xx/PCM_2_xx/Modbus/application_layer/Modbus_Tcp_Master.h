/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename        : Modbus_Tcp_Master.h
* @brief               : Controller Board
*
* @author               : Dnyaneshwar Kashid 
*
* @date Created         : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified   : October Monday, 2014    <Oct 6, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __MODBUS_TCP_MASTER_H
#define __MODBUS_TCP_MASTER_H
/*============================================================================*/
#include "../GUI/APPLICATION/Screen_data_enum.h"
//#ifdef MODBUS_TCP
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
//#define HOST_ADDR								(Pcm_var.PCM_web_select==Screen212_str3?(&Http_Host_Name[7]):(&Http_Host_Name[8]))

/* Defines Section */
#define EVE_FTP_RESP    	0x0008  
//#define EVE_GET_HOST_IP  	0x0100  //commented by DK on 17 March 2015 
#define EVE_DNS_IP_RESOLVED	0x0100
#define EVE_DNS_IP_NONAME	  0x0200
#define EVE_DNS_IP_TIMEOUT  0x0400
#define EVE_DNS_IP_ERROR    0x0800 

#define EVE_HTTP_RES  		0x0001  
#define EVE_HTTP_ACK  		0x0010  
#define EVE_PPP_ACK  			0x0020   //Added by DK on 2 Jan2015 
#define EVE_PPP_RES  			0x0002  
#define EVE_GET_RES   		0x0010  
#define EVE_LTE_RES  		0x0004  
#define EVE_LTE_ACK  		0x0040 


//By DK on 15 Sep2014 for PCM
#define MBM_TCP_PORT   								 502
#define MAX_NO_OF_INTEGRATORS          17  //Changed from 10 to 17 on 23 Feb 2015 
#define MODBUS_CLIENT_LOCAL_PORT				1000
#define MB_SCALE_RUNTIME_NUM_RECORDS   5
#define MB_SCALE_TOTALS_NUM_RECORDS    6
#define MB_SCALE_RECORD_NUM_RECORDS    15		//14 by megha on 1/8/2017 for reading master total from scale

#define RUNTIME_POLLING 								1
#define TOTALS_POLLING 									2
#define RECORDS_POLLING 								3

//On Demand Scale Run time & Scale Totals variables MB Reg readings for 40001-15	 &  40050-74	
#define READ_REG                                 1
#define WRITE_REG                                2
#define MODBUS_REGISTER_BASE_ADDR                40001
#define SCALE_RUNTIME_TCP_REG_START_ADDR         40001
#define SCALE_TOT_WEIGHT_REG_ADDR         			 40001
#define SCALE_DISP_RATE_REG_ADDR         			   40005
#define SCALE_BELT_SPEED_REG_ADDR        			   40007
#define SCALE_LOAD_PERC_REG_ADDR        			   40009
#define SCALE_ANGLE_REG_ADDR    		    			   40011
#define SCALE_RUNTIME_TCP_REG_END_ADDR 	         40013

#define SCALE_TOTALS_TCP_REG_START_ADDR          40050
#define SCALE_DAILY_TOTAL_WT_REG_ADDR            40050
#define SCALE_WEEKLYY_TOTAL_WT_REG_ADDR          40054
#define SCALE_MONTHLY_TOTAL_WT_REG_ADDR          40058
#define SCALE_YEARLY_TOTAL_WT_REG_ADDR           40062
#define SCALE_JOB_TOTAL_REG_ADDR           			 40066
#define SCALE_MASTER_TOTAL_REG_ADDR           	 40070

#define SCALE_FW_VER_REG_ADDR           	 			 45202

//Continously polled registers 
#define SCALE_ID_REG_ADDR           						 46001
#define SCALE_RECORD_REG_START_ADDR   					 46002
#define SCALE_RECORD_FUNC_CODE_REG_ADDR 	  		 46002

#define SCALE_RECORD_HASH_REG_ADDR 	  		 			 46003
#define SCALE_RECORD_DATE_REG_ADDR 	  		 			 46004
#define SCALE_RECORD_TIME_REG_ADDR 	  		 			 46006
#define SCALE_RECORD_ACC_WT_REG_ADDR 	  	 			 46008
#define SCALE_RECORD_ACC_WT_UNIT_REG_ADDR 			 46012
#define SCALE_RECORD_RATE_REG_ADDR 			 				 46013
#define SCALE_RECORD_RATE_TM_UNIT_REG_ADDR 			 46015
#define SCALE_RECORD_LOAD_PERC_REG_ADDR 	 			 46016
#define SCALE_RECORD_ANGLE_REG_ADDR 	 			 		 46018
#define SCALE_RECORD_BELT_SPEED_REG_ADDR 		 		 46020
#define SCALE_RECORD_SPEED_UNIT_REG_ADDR 		 		 46022
#define SCALE_RECORD_DAILY_WT_REG_ADDR 		 		 	 46023
#define SCALE_RECORD_DAILY_WT__UNIT_REG_ADDR 		 46027
#define SCALE_MASTER_TOTAL_READ_REG_ADDR       	 46028
#define SCALE_RECORD_DAILY_TOTAL_POUNDS_REG_ADDR	46032
#define SCALE_RECORD_ZERO_NUMBER_POUNDS_REG_ADDR	46036
#define SCALE_RECORD_SPEED_FEET_HOUR_REG_ADDR			46038
#define SCALE_RECORD_INST_RATE_POUNDS_HOUR_REG_ADDR	46040
#define SCALE_USB_HEALTH_REG_ADDR									46044
#define SCALE_JOB_TOTALS_POUNDS										46045
#define	SCALE_ACCUMULATED_WEIGHT_POUNDS						46049
#define	SCALE_RECORD_ID_REGISTER_ADDRESS					46053

//Date & time entry 
#define SCALE_DATE_REG_ADDR   									44001 // 2 registers
#define SCALE_DOW_REG_ADDR 		   								44004
#define SCALE_TIME_REG_ADDR   									44005  // 2 registers

//by DK on 7 Oct2014 for PCM 
/*============================================================================
* Public Data Types
*===========================================================================*/
typedef enum
{
    NOT_CONNECTED=0,
    CONNECTED_IN_THREE_RETRIES,
	  CONNECTED_IN_3TO5_RETRIES,
	  CONNECTED_IN_5TO10_RETRIES,
}CONNECTIVITY_STATUS;  //Added by DK on 6 Feb 2015 

 typedef union {
	 long long int  ulong_val;			
 	 U16  ulong_array[4];
 }LONG_LONG_UNION;
 
 typedef union {
	 U16  uint_val;			//SJ-080415 For -ve record no. issue
 	 U16  uint_array[1];
 }SHORT_UNION;

typedef union 
{
	  long value;
    U16 int_array[2];
}INT32_DATA;

typedef union {
   double         double_val;//[2];
//	 U64	long_val;
   U16 s_array[4];
 }DOUBLE_UNION;

typedef struct {
   double         double_val[2];
   U16 s_array[4];
 }DOUBLE_STRUCT;
 
typedef union {
   float         float_val;
   U16 f_array[2];
 }FLOAT_UNION;

typedef union {
   int         int_val;
   U16 int_array[2];
 }SINT32_UNION;

typedef enum{
                bit16_int = 1,
                bit32_int,
                bit32_flt,
                bit64_dbl,
                discrete,
} VARIABLE_TYPE;

						
typedef struct{
                U16 modbus_reg_start_addr;         //virtual address of the modbus start register
                U16 number_of_elements;    //number of data elements for the data type 
							  void * var_data;               				//pointer to variable
                unsigned char data_type;              //type of variable  2 bytes or 4 bytes or 8 bytes 
} MODBUS_TCP_VAR_STRUCT;
						 
typedef struct
{
	DOUBLE_UNION RunTime_Tot_Wt;
	FLOAT_UNION disp_rate,belt_speed_rate,load_perc,angle;
}SCALE_RUNTIME_REGISTERS;						 

typedef struct
{
	DOUBLE_UNION Total_Regs[MB_SCALE_TOTALS_NUM_RECORDS];
}SCALE_TOTALS_REGISTERS;						 

typedef struct
{
	SHORT_UNION Scale_Id,Function_Code,Record_Hash;//,,daily_wt_unit;
	INT32_DATA Scale_Date,Scale_Time;
	
	DOUBLE_UNION acc_wt;
	SHORT_UNION accu_wt_unit;
	
	FLOAT_UNION scale_rate;
	SHORT_UNION rate_time_unit;	
	FLOAT_UNION scale_load_perc,scale_angle,belt_speed;
	SHORT_UNION belt_speed_unit;
	
	DOUBLE_UNION daily_wt;
	SHORT_UNION daily_wt_unit;
	
	LONG_LONG_UNION master_total;
	LONG_LONG_UNION daily_total;//in pounds
	SINT32_UNION zero_number;
	SINT32_UNION speed_feet_min;	
	LONG_LONG_UNION inst_rate_pounds_hour;	
	SHORT_UNION scale_usb_health;
	LONG_LONG_UNION job_total_pounds;
	LONG_LONG_UNION accumulated_weight_pounds;
	INT32_DATA	Record_id;
	
	
}SCALE_RECORD_REGISTERS;						 

typedef struct
{
	unsigned int Slave_ID; //changed by DK on 20 Jan 2015 from char to int
	//unsigned char Scale_ID;
	unsigned int Scale_ID; //Changed on 7 Jan2016 by DK 
	unsigned char IP_ADRESS[4];
	void   *xMBMHandle;
  BOOL 	Connect, rtc_update,get_file,ftp_file_recd,ftp_file_not_available,file_poll_complete,file_request_in_process; 
	unsigned int socket_number;  //changed by DK on 20 Jan 2015 from char to int
  unsigned int NoOfRetry; //,Wait_Count; //Commented by DK on 6 Feb 2015 for PCM to Scale connectivity status update
	unsigned int prev_record,cur_record;
	unsigned int file_day,file_month,file_year,file_scale_id; //Added by DK on 20 Jan 2015 
	CONNECTIVITY_STATUS scale_connectivity_status; //Added by DK on 6 Feb 2015 
	char fw_ver[2];
}MB_TCP_SLAVE;

typedef struct
{
unsigned int Scale_ID;
unsigned int file_day,file_month,file_year;
}FILE_REQUEST;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
extern BOOL report_connectivity_status; //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
/* Character variables section */
extern U8 Current_TCP_Slave;

/* unsigned integer variables section */
extern unsigned int MB_Polling;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern SCALE_RUNTIME_REGISTERS Scale_Runtime_Mb_Reg[MAX_NO_OF_INTEGRATORS];
extern SCALE_TOTALS_REGISTERS Scale_Totals_Mb_Reg[MAX_NO_OF_INTEGRATORS];
extern SCALE_RECORD_REGISTERS Scale_Record_Mb_Reg[MAX_NO_OF_INTEGRATORS];
extern MB_TCP_SLAVE Modbus_Tcp_Slaves[MAX_NO_OF_INTEGRATORS + 1];
extern FILE_REQUEST File_request_data ;

extern MODBUS_TCP_VAR_STRUCT Scale_runtime_tcp_var[MAX_NO_OF_INTEGRATORS][MB_SCALE_RUNTIME_NUM_RECORDS]; 
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern __task void modbus_tcp_master (void);  //By DK on 15Sep 2014 for PCM 
extern char *inet_ntoa(U32 addr);
extern int inet_aton(const char *cp, U32 *addr);
//#endif //#ifdef MODBUS_TCP

#endif /*__MODBUS_TCP_MASTER_H*/
/*****************************************************************************
* End of file
*****************************************************************************/

