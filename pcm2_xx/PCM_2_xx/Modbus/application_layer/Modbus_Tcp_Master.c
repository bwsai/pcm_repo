/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     : Beltway
*
* @file Filename       : Modbus_tcp_Master.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid 
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : December Thursday, 2014 <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
/* ----------------------- Platform includes --------------------------------*/
#include "math.h"
#include "Global_ex.h"
#include "Modbus_Tcp_Master.h"
#include "RTOS_main.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mbport.h"
#include "mbs.h"
#include "mbm.h"
#include "common/mbtypes.h"
#include "common/mbutils.h"
#include "common/mbportlayer.h"
#include "Http_Client.h"
#include "Main.h"
#include "sntp.h"
#include "rtc.h"
#include "Main.h"
#include "I2C_driver.h"
#include "../GUI/APPLICATION/Screen_global_ex.h"

#include "File_update.h"
#ifdef MODBUS_TCP
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL report_connectivity_status; //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update

/* Character variables section */
U8 Current_TCP_Slave;
//unsigned char accu_wt_unit[15];

/*#ifdef DEBUG_PORT
__align(4) char mb_temp_buf[100] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));
#endif  */
/* unsigned integer variables section */
unsigned int MB_Polling,disp_rx_scale_id;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */
U64	Long_val;
/* signed long long integer variables section */

/* Float variables section */
//double f_accum_wt_tmp;
/* Double variables section */
//double d_accu_wt_tmp;
/* Structure or Union variables section */
__align(4) unsigned short mb_temp_buf[50] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
__align(4) SCALE_RUNTIME_REGISTERS Scale_Runtime_Mb_Reg[MAX_NO_OF_INTEGRATORS] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));
__align(4) SCALE_TOTALS_REGISTERS Scale_Totals_Mb_Reg[MAX_NO_OF_INTEGRATORS] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));
__align(4) SCALE_RECORD_REGISTERS Scale_Record_Mb_Reg[MAX_NO_OF_INTEGRATORS] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));
__align(4) MB_TCP_SLAVE Modbus_Tcp_Slaves[MAX_NO_OF_INTEGRATORS + 1] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));

__align(4) MODBUS_TCP_VAR_STRUCT Scale_runtime_tcp_var[MAX_NO_OF_INTEGRATORS][MB_SCALE_RUNTIME_NUM_RECORDS] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));

__align(4) MODBUS_TCP_VAR_STRUCT Scale_Total_tcp_var[MAX_NO_OF_INTEGRATORS][MB_SCALE_TOTALS_NUM_RECORDS] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));

__align(4) MODBUS_TCP_VAR_STRUCT Scale_Record_Data_tcp_var[MAX_NO_OF_INTEGRATORS][MB_SCALE_RECORD_NUM_RECORDS] __attribute__ ((section ("MODBUS_REG_DATA_RAM")));
__align(4) FILE_REQUEST File_request_data __attribute__ ((section ("MODBUS_REG_DATA_RAM")));

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/


/*============================================================================
* Function Implementation Section
*===========================================================================*/
//By DK on 14 Oct 2014 for multiple TCP MB slaves (max 32)
static void Load_Runtime_Var(void)
{
	unsigned int index;
	
	for (index=0; index < MAX_NO_OF_INTEGRATORS; index++)
	{
			
			Scale_runtime_tcp_var [index][0].modbus_reg_start_addr = SCALE_TOT_WEIGHT_REG_ADDR ;
			Scale_runtime_tcp_var [index][0].data_type = bit64_dbl; //Total weight - 64 bit flt
			Scale_runtime_tcp_var [index][0].number_of_elements = 4;
			Scale_runtime_tcp_var [index][0].var_data = &Scale_Runtime_Mb_Reg[index].RunTime_Tot_Wt.s_array;
				
			Scale_runtime_tcp_var [index][1].modbus_reg_start_addr = SCALE_DISP_RATE_REG_ADDR ;
			Scale_runtime_tcp_var [index][1].data_type = bit32_flt; //Rate - 32 bit flt
			Scale_runtime_tcp_var [index][1].number_of_elements = 2;
			Scale_runtime_tcp_var [index][1].var_data = &Scale_Runtime_Mb_Reg[index].disp_rate.f_array;
			
			Scale_runtime_tcp_var [index][2].modbus_reg_start_addr = SCALE_BELT_SPEED_REG_ADDR ;
			Scale_runtime_tcp_var [index][2].data_type = bit32_flt; //Belt_speed - 32 bit flt
			Scale_runtime_tcp_var [index][2].number_of_elements = 2;
			Scale_runtime_tcp_var [index][2].var_data = &Scale_Runtime_Mb_Reg[index].belt_speed_rate.f_array;
			
			Scale_runtime_tcp_var [index][3].modbus_reg_start_addr = SCALE_LOAD_PERC_REG_ADDR ;
			Scale_runtime_tcp_var [index][3].data_type = bit32_flt; //Load_percentage - 32 bit flt
			Scale_runtime_tcp_var [index][3].number_of_elements = 2;
			Scale_runtime_tcp_var [index][3].var_data = &Scale_Runtime_Mb_Reg[index].load_perc.f_array;
			
			Scale_runtime_tcp_var [index][4].modbus_reg_start_addr = SCALE_ANGLE_REG_ADDR ;
			Scale_runtime_tcp_var [index][4].data_type = bit32_flt; //Angle - 32 bit flt
			Scale_runtime_tcp_var [index][4].number_of_elements = 2;
			Scale_runtime_tcp_var [index][4].var_data = &Scale_Runtime_Mb_Reg[index].angle.f_array;
	 }
}

static void Load_Totals_Var(void)
{
	unsigned int index,NoOfRecords;
	
	for (index=0; index < MAX_NO_OF_INTEGRATORS; index++)
	{
		for(NoOfRecords=0; NoOfRecords < MB_SCALE_TOTALS_NUM_RECORDS; NoOfRecords++)
		{	
			Scale_Total_tcp_var [index][NoOfRecords].modbus_reg_start_addr = (SCALE_DAILY_TOTAL_WT_REG_ADDR + (NoOfRecords * 4));
			Scale_Total_tcp_var [index][NoOfRecords].data_type = bit64_dbl; 
			Scale_Total_tcp_var [index][NoOfRecords].number_of_elements = 4;
			Scale_Total_tcp_var [index][NoOfRecords].var_data = &Scale_Totals_Mb_Reg[index].Total_Regs[NoOfRecords].s_array;
		}		
			
	 }
}

static void Load_Record_Data(void)
{
	unsigned int index;
	
	for (index=0; index < MAX_NO_OF_INTEGRATORS; index++)
	{
			Scale_Record_Data_tcp_var [index][0].modbus_reg_start_addr = SCALE_ID_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][0].data_type = bit16_int; //Scale ID - 16 bit int
			Scale_Record_Data_tcp_var [index][0].number_of_elements = 1;
			Scale_Record_Data_tcp_var [index][0].var_data = Scale_Record_Mb_Reg[index].Scale_Id.uint_array;
				
			Scale_Record_Data_tcp_var [index][1].modbus_reg_start_addr = SCALE_RECORD_FUNC_CODE_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][1].data_type = bit16_int; //Function Code- 16 bit int
			Scale_Record_Data_tcp_var [index][1].number_of_elements = 1;
			Scale_Record_Data_tcp_var [index][1].var_data = &Scale_Record_Mb_Reg[index].Function_Code.uint_array;
			
			Scale_Record_Data_tcp_var [index][2].modbus_reg_start_addr = SCALE_RECORD_HASH_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][2].data_type = bit16_int; //Record # - 16 bit int
			Scale_Record_Data_tcp_var [index][2].number_of_elements = 1;
			Scale_Record_Data_tcp_var [index][2].var_data = &Scale_Record_Mb_Reg[index].Record_Hash.uint_array;
			
			Scale_Record_Data_tcp_var [index][3].modbus_reg_start_addr = SCALE_RECORD_DATE_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][3].data_type = bit32_int; //Date - 32 bit int
			Scale_Record_Data_tcp_var [index][3].number_of_elements = 2;
			Scale_Record_Data_tcp_var [index][3].var_data = &Scale_Record_Mb_Reg[index].Scale_Date.int_array;
			
			Scale_Record_Data_tcp_var [index][4].modbus_reg_start_addr = SCALE_RECORD_TIME_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][4].data_type = bit32_int; //Time - 32 bit int
			Scale_Record_Data_tcp_var [index][4].number_of_elements = 2;
			Scale_Record_Data_tcp_var [index][4].var_data = &Scale_Record_Mb_Reg[index].Scale_Time.int_array;
			
			Scale_Record_Data_tcp_var [index][5].modbus_reg_start_addr = SCALE_RECORD_ACC_WT_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][5].data_type = bit64_dbl; //Accumulated Weight - 64 bit float
			Scale_Record_Data_tcp_var [index][5].number_of_elements = 4;
			Scale_Record_Data_tcp_var [index][5].var_data = &Scale_Record_Mb_Reg[index].acc_wt.s_array;
			
			Scale_Record_Data_tcp_var [index][6].modbus_reg_start_addr = SCALE_RECORD_ACC_WT_UNIT_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][6].data_type = bit16_int; //Accumulated Weight unit  - 16 bit int
			Scale_Record_Data_tcp_var [index][6].number_of_elements = 1;
			Scale_Record_Data_tcp_var [index][6].var_data = &Scale_Record_Mb_Reg[index].accu_wt_unit.uint_array;
			
			Scale_Record_Data_tcp_var [index][7].modbus_reg_start_addr = SCALE_RECORD_RATE_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][7].data_type = bit32_flt; //Record rate time- 32 bit float
			Scale_Record_Data_tcp_var [index][7].number_of_elements =2; 
			Scale_Record_Data_tcp_var [index][7].var_data = &Scale_Record_Mb_Reg[index].scale_rate.f_array; 
			
			Scale_Record_Data_tcp_var [index][8].modbus_reg_start_addr = SCALE_RECORD_RATE_TM_UNIT_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][8].data_type =bit16_int;  //Record rate time unit - 16 bit int
			Scale_Record_Data_tcp_var [index][8].number_of_elements =1; 
			Scale_Record_Data_tcp_var [index][8].var_data = &Scale_Record_Mb_Reg[index].rate_time_unit.uint_array; 
			
			Scale_Record_Data_tcp_var [index][9].modbus_reg_start_addr = SCALE_RECORD_LOAD_PERC_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][9].data_type =bit32_flt; //Load %- 32 bit float
			Scale_Record_Data_tcp_var [index][9].number_of_elements =2; 
			Scale_Record_Data_tcp_var [index][9].var_data = &Scale_Record_Mb_Reg[index].scale_load_perc.f_array;
			
			Scale_Record_Data_tcp_var [index][10].modbus_reg_start_addr = SCALE_RECORD_ANGLE_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][10].data_type = bit32_flt; //angle- 32 bit float
			Scale_Record_Data_tcp_var [index][10].number_of_elements =2; 
			Scale_Record_Data_tcp_var [index][10].var_data = &Scale_Record_Mb_Reg[index].scale_angle.f_array; 
			
			Scale_Record_Data_tcp_var [index][11].modbus_reg_start_addr = SCALE_RECORD_BELT_SPEED_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][11].data_type = bit32_flt;  //Belt speed- 32 bit float
			Scale_Record_Data_tcp_var [index][11].number_of_elements =2; 
			Scale_Record_Data_tcp_var [index][11].var_data = &Scale_Record_Mb_Reg[index].belt_speed.f_array; 
			
			Scale_Record_Data_tcp_var [index][12].modbus_reg_start_addr = SCALE_RECORD_SPEED_UNIT_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][12].data_type = bit16_int; //Belt speed unit- 16 bit int
			Scale_Record_Data_tcp_var [index][12].number_of_elements =1; 
			Scale_Record_Data_tcp_var [index][12].var_data = &Scale_Record_Mb_Reg[index].belt_speed_unit.uint_array; 
			
			Scale_Record_Data_tcp_var [index][13].modbus_reg_start_addr = SCALE_RECORD_DAILY_WT_REG_ADDR ;
			Scale_Record_Data_tcp_var [index][13].data_type = bit64_dbl;  //Daily weight- 64 bit double
			Scale_Record_Data_tcp_var [index][13].number_of_elements =4; 
			Scale_Record_Data_tcp_var [index][13].var_data = &Scale_Record_Mb_Reg[index].daily_wt.s_array; 
			
// 			Scale_Record_Data_tcp_var [index][14].modbus_reg_start_addr = SCALE_RECORD_MASTER_TOTAL_POUNDS_REG_ADDR ;
// 			Scale_Record_Data_tcp_var [index][14].data_type = bit64_dbl;  //Master total- 64 bit double
// 			Scale_Record_Data_tcp_var [index][14].number_of_elements =4; 
// 			Scale_Record_Data_tcp_var [index][14].var_data = &Scale_Record_Mb_Reg[index].master_total.s_array; 

// 			Scale_Record_Data_tcp_var [index][15].modbus_reg_start_addr = SCALE_RECORD_DAILY_TOTAL_POUNDS_REG_ADDR ;
// 			Scale_Record_Data_tcp_var [index][15].data_type = bit64_dbl;  //DAILY TOTAL
// 			Scale_Record_Data_tcp_var [index][15].number_of_elements =4; 
// 			Scale_Record_Data_tcp_var [index][15].var_data = &Scale_Record_Mb_Reg[index].master_total.s_array; 

// 			Scale_Record_Data_tcp_var [index][16].modbus_reg_start_addr = SCALE_RECORD_ZERO_NUMBER_POUNDS_REG_ADDR ;
// 			Scale_Record_Data_tcp_var [index][16].data_type = bit64_dbl;  //ZERO NUMBER
// 			Scale_Record_Data_tcp_var [index][16].number_of_elements =4; 
// 			Scale_Record_Data_tcp_var [index][16].var_data = &Scale_Record_Mb_Reg[index].master_total.s_array; 

// 			Scale_Record_Data_tcp_var [index][17].modbus_reg_start_addr = SCALE_RECORD_SPEED_FEET_MIN_REG_ADDR ;
// 			Scale_Record_Data_tcp_var [index][17].data_type = bit64_dbl;  //SPEED FEET/MIN
// 			Scale_Record_Data_tcp_var [index][17].number_of_elements =1; 
// 			Scale_Record_Data_tcp_var [index][17].var_data = &Scale_Record_Mb_Reg[index].master_total.s_array; 

// 			Scale_Record_Data_tcp_var [index][18].modbus_reg_start_addr = SCALE_RECORD_INST_RATE_POUNDS_HOUR_REG_ADDR ;
// 			Scale_Record_Data_tcp_var [index][18].data_type = bit64_dbl;  //RATE POUNDS/HR
// 			Scale_Record_Data_tcp_var [index][18].number_of_elements =1; 
// 			Scale_Record_Data_tcp_var [index][18].var_data = &Scale_Record_Mb_Reg[index].master_total.s_array; 
	 }
}


/*****************************************************************************
* @note       Function name  : __task void modbus_tcp_master (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 15 September 2014
* @brief      Description    : Modbus TCP master task, to initialize the modbus tcp master stack and
*                            : send modbus requests to integrator boards(1 to 32) every 50 ms.
*                            : 0x03 - read holding registers
* @note       Notes          : None
*****************************************************************************/
__task void modbus_tcp_master (void)
{
	eMBErrorCode    eStatus;
	unsigned int index=0,i=0, address,r_index;
	unsigned int usRegStart,usNoOfRecords,usNoOfRegs;
	U16 Date_Time[2];
  BOOL power_on; 
	
	Load_Runtime_Var();
	Load_Totals_Var();
	Load_Record_Data();
	MB_Polling=RECORDS_POLLING; 
	
	for(index=0; index < MAX_NO_OF_INTEGRATORS; index++)
	{
		Modbus_Tcp_Slaves[index].Connect= FALSE;
//		Modbus_Tcp_Slaves[index].rtc_update=0;
		Modbus_Tcp_Slaves[index].NoOfRetry =0;
		//Modbus_Tcp_Slaves[index].Wait_Count=0; //Commented by DK on 6 Feb 2015 for PCM to Scale connectivity status update
		Modbus_Tcp_Slaves[index].socket_number=0;
		Modbus_Tcp_Slaves[index].get_file	= 0;  //By DK on 20 Jan2015 
		//Added by DK on 5 Feb2015 
		Modbus_Tcp_Slaves[index].ftp_file_not_available=0;
		Modbus_Tcp_Slaves[index].ftp_file_recd = 0;
		//only for testing 
	/*	Modbus_Tcp_Slaves[index].Slave_ID = index +1;
		Modbus_Tcp_Slaves[index].IP_ADRESS[3] = 192;
		Modbus_Tcp_Slaves[index].IP_ADRESS[2] = 168;
		Modbus_Tcp_Slaves[index].IP_ADRESS[1] = 1;
		Modbus_Tcp_Slaves[index].IP_ADRESS[0] = 100 + index;*/
		Modbus_Tcp_Slaves[index].scale_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
		Modbus_Tcp_Slaves[index].fw_ver[0] = 0; //Added by DK on 6 Feb 2015 
		Modbus_Tcp_Slaves[index].fw_ver[1] = 0; //Added by DK on 6 Feb 2015 
	}	
	//Modbus_Tcp_Slaves[3].get_file	= 1;		//Suvrat testing
	power_on = 1; 
	report_connectivity_status = 0; //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
//	Total_Scales = 10; //only for testing 
	//wait for Get request response from PCS then only start.
	os_evt_wait_or(EVE_GET_RES,0xFFFF);  
  os_evt_clr(EVE_GET_RES, t_modbus_tcp_master); 
	
	while(1)
	{

		os_itv_set(MODBUS_TCP_MASTER_INTERVAL);
		os_itv_wait();
		LPC_RTC->GPREG4 |= (0x01<<7);		
		//os_mut_wait(&tcp_mutex,0xffff);  //Added by DK on 23 Dec 2014 
	
		for(index=0; index < Total_Scales; index++)
		{ 
			//if(index >= MAX_NO_OF_INTEGRATORS)
			if(index > (MAX_NO_OF_INTEGRATORS -1))//Changed by DK on 23 Feb 2015 	
			{
				break;
			}	

//			LPC_RTC->ALMIN = 1;
//			os_mut_wait(&tcp_mutex,0xffff);  //Added by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015 PCM firmware V1.33  
//			LPC_RTC->ALMIN = 2;
			eStatus = MB_ENOERR;
		/*-----Check for scale connections & initialize scales-------------------------------------------*/			
			//if ((Modbus_Tcp_Slaves[index].NoOfRetry == 0) && (Modbus_Tcp_Slaves[index].Connect == FALSE)) //Commented by DK on 6 Feb2015
			if(Modbus_Tcp_Slaves[index].Connect == FALSE) //Added by DK on 6 Feb2015
			{
				LPC_RTC->ALMIN = 1;				
				Current_TCP_Slave = index;
				
				//Initialize Modubus TCP master stack 
				/*#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(mb_temp_buf,'\0',sizeof(mb_temp_buf));
				sprintf(mb_temp_buf, "Initializing MB TCP Slave %d\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
				Uart_i2c_data_send((uint8_t *)mb_temp_buf, strlen(mb_temp_buf));
				os_mut_release (&debug_uart_mutex);
				#endif*/
				
				if(power_on_fg == 1)
				{
					strcpy(Debug_Buf, "INITIALIZING MODBUS TCP.......");
					GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(6*16)));	
//LPC_RTC->ALMIN = 3;					
							os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALMIN = 4;		
					if((1<<12)!= (msg_log_reg & (1<<12)))
					{		
						msg_log_reg |= 1<<12;	
						
						sprintf(&system_log.log_buf[0],"Initializing ModbusTCP......");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);					
				}	
LPC_RTC->ALMIN = 2;				
				if( MB_ENOERR != ( eStatus = eMBMTCPInit( &(Modbus_Tcp_Slaves[index].xMBMHandle)) ) )
				{
LPC_RTC->ALMIN = 3;					
					if(power_on_fg == 1)
					{
						strcpy(Debug_Buf, "FAILED TO INITIALIZE MODBUS TCP.......");
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(7*16)));	
						
							os_mut_wait (&usb_log_mutex, 0xffff);							
	
					if((1<<13)!= (msg_log_reg & (1<<13)))
					{		
						msg_log_reg |= 1<<13;							
						sprintf(&system_log.log_buf[0],"Failed to initialize ModbusTCP......");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);						
					}							
					//initialization is not successful 
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "Initilization of MB TCP for Slave%d failed!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif  
				}
				else  //initialization is successful 
				{
LPC_RTC->ALMIN = 4;						
					if(power_on_fg == 1)
					{
						strcpy(Debug_Buf, "MODBUS TCP INITIALIZATION OK.....");
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(7*16)));	
					
							os_mut_wait (&usb_log_mutex, 0xffff);							

					if((1<<14)!= (msg_log_reg & (1<<14)))
					{		
						msg_log_reg |= 1<<14;							
						sprintf(&system_log.log_buf[0],"ModbusTCP initialization OK......");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);						
					}						
					address= Modbus_Tcp_Slaves[index].IP_ADRESS[3] << 24;
					address |= Modbus_Tcp_Slaves[index].IP_ADRESS[2] << 16;
					address |= Modbus_Tcp_Slaves[index].IP_ADRESS[1] << 8;
					address |= Modbus_Tcp_Slaves[index].IP_ADRESS[0];
					//Connecting to MODBUS TCP slave 
					if(power_on_fg == 1)
					{
						
						strcpy(Debug_Buf, "CONNECTING TO MODBUS TCP SLAVE.....");
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(8*16)));	
						
							os_mut_wait (&usb_log_mutex, 0xffff);							
			
					if((1<<15)!= (msg_log_reg & (1<<15)))
					{		
						msg_log_reg |= 1<<15;								
						sprintf(&system_log.log_buf[0],"Connecting to ModbusTCP slave......");
						log_file_write(system_log1);
					}
						os_mut_release (&usb_log_mutex);						
					}	
LPC_RTC->ALMIN = 5;						
					if( MB_ENOERR == ( eStatus = eMBMTCPConnect( Modbus_Tcp_Slaves[index].xMBMHandle, inet_ntoa(address), MBM_TCP_PORT )))
					{
LPC_RTC->ALMIN = 6;							
//						scale_config_req_fg = 0;
						if(power_on_fg == 1)
						{
							
							strcpy(Debug_Buf, "CONNECTED TO MODBUS TCP SLAVE.....             ");
							GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(9*16)));
							
							os_mut_wait (&usb_log_mutex, 0xffff);							

		
	//						msg_log_reg =0;		//by megha on 7/7/2017 for log power on msg only once.
						if((1<<16)!= (msg_log_reg & (1<<16)))
						{		
							msg_log_reg |= 1<<16;							
							sprintf(&system_log.log_buf[0],"Connected to ModbusTCP slave successfully...... ");
							log_file_write(system_log1);	
						}
							os_mut_release (&usb_log_mutex);							
						}

						
						if(disp_power_on_screen_timeout_fg == 1)
						{
							disp_power_on_screen_timeout_fg = 0;
//							scale_config_req_fg = 0;
							get_sntp_time=1;		//by megha on 19/6/2017 for sntp on ethernet connection at power on

						}
						Modbus_Tcp_Slaves[index].Connect= TRUE;
						
						//Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
						if(Modbus_Tcp_Slaves[index].NoOfRetry < 4)
						{
							Modbus_Tcp_Slaves[index].scale_connectivity_status = CONNECTED_IN_THREE_RETRIES;
						}	
						else if(Modbus_Tcp_Slaves[index].NoOfRetry < 6)
						{
							Modbus_Tcp_Slaves[index].scale_connectivity_status = CONNECTED_IN_3TO5_RETRIES;
						}
						else 
						{
							Modbus_Tcp_Slaves[index].scale_connectivity_status = CONNECTED_IN_5TO10_RETRIES;
						}
						
						Modbus_Tcp_Slaves[index].NoOfRetry =0;
						
					/*	#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(mb_temp_buf,'\0',sizeof(mb_temp_buf));
						sprintf(mb_temp_buf, "Connecting to Slave %d\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
						Uart_i2c_data_send((uint8_t *)mb_temp_buf, strlen(mb_temp_buf));
						os_mut_release (&debug_uart_mutex);
						#endif*/
						
						//Added by DK on 30 Sep 2015 
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "Connected to Slave %d over MODBUS TCP!\
																	Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																	Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																 Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																 Current_time.RTC_Sec);	
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
LPC_RTC->ALMIN = 7;							
						usRegStart = SCALE_FW_VER_REG_ADDR - MODBUS_REGISTER_BASE_ADDR;
						//Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update reading firmware version of scale
						if( MB_ENOERR !=( eStatus = eMBMReadHoldingRegisters(Modbus_Tcp_Slaves[index].xMBMHandle, \
							  Modbus_Tcp_Slaves[index].Slave_ID, usRegStart,1,&Date_Time[0])) )
						{
LPC_RTC->ALMIN = 8;								
							//Reading of Holding resgisters failed. 
							Modbus_Tcp_Slaves[index].NoOfRetry++;
							
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(mb_temp_buf, "MB TCP Registers reading failed for Slave%d!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
							sprintf(Debug_Buf, "MB TCP Firmware Version Register reading failed for Slave%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
						}
						else 
						{
LPC_RTC->ALMIN = 9;								
//							Modbus_Tcp_Slaves[index].Connect= TRUE;							
						  Modbus_Tcp_Slaves[index].fw_ver[0] = (Date_Time[0] >> 8); 
							Modbus_Tcp_Slaves[index].fw_ver[1] = (Date_Time[0] & 0x00FF); 
						}
					}
					else //Not able to connect MODBUS TCP slave
					{
LPC_RTC->ALMIN = 10;								
						
						if(disp_power_on_screen_timeout_fg==1)
						{
//							scale_config_req_fg = 1;							

						}
						if(power_on_fg == 1)
						{
							strcpy(Debug_Buf, "FAILED TO CONNECT TO MODBUS TCP SLAVE.....");
							GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(9*16)));	
							
							os_mut_wait (&usb_log_mutex, 0xffff);							
		
					if((1<<17)!= (msg_log_reg & (1<<17)))
					{		
						msg_log_reg |= 1<<17;									
						sprintf(&system_log.log_buf[0],"Failed to connect to ModbusTCP slave......");
						log_file_write(system_log1);	
					}
							Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_CONFIGURATION_RECEIVED_OK_FLAG;
							GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME] = 0;						
							Flags_struct.Plant_connect_record_flag = MODBUS_TCP_CONNECTION_FAIL_FG;
							GUI_data_nav.Error_wrn_msg_flag[MODBUS_TCP_FAIL_MSG_TIME] = 0;					
						os_mut_release (&usb_log_mutex);							
						}
//							scale_config_req_fg = 1;
					/*	#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(mb_temp_buf,'\0',sizeof(mb_temp_buf));
						sprintf(mb_temp_buf, "Disconnecting & closing MB TCP Scale %d!\r\n",Modbus_Tcp_Slaves[index].Slave_ID);
						Uart_i2c_data_send((uint8_t *)mb_temp_buf, strlen(mb_temp_buf));
						os_mut_release (&debug_uart_mutex);
						#endif*/
LPC_RTC->ALMIN = 11;							
						if( MB_ENOERR != ( eStatus = eMBMTCPDisconnect( Modbus_Tcp_Slaves[index].xMBMHandle) ) )
						{	
LPC_RTC->ALMIN = 12;								
							//Can not disconnect from Modbus TCP slave
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(Debug_Buf, "Can not disconnect from Scale %d!\r\n",Modbus_Tcp_Slaves[index].Slave_ID);
							sprintf(Debug_Buf, "Can not disconnect from Slave %d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
						}
LPC_RTC->ALMIN = 13;							
						if( MB_ENOERR != ( eStatus = eMBMClose( Modbus_Tcp_Slaves[index].xMBMHandle ) ) )
						{
LPC_RTC->ALMIN = 14;								
								//Can not close MODBUS instance
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(Debug_Buf, "Can not Close MB connection for Scale %d!\r\n",Modbus_Tcp_Slaves[index].Slave_ID);
							sprintf(Debug_Buf, "Can not Close MB connection for Slave %d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
						}
LPC_RTC->ALMIN = 15;								

							Modbus_Tcp_Slaves[index].Connect= FALSE;
							Modbus_Tcp_Slaves[index].NoOfRetry++;
					}	
				} //end of else  //initialization is successful 
			
				//Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
				if((index == (Total_Scales -1)) && (power_on == 1)) 
				{
					report_connectivity_status = 1;
					power_on = 0;
				}	
				
			} //end of if ((Modbus_Tcp_Slaves[index].NoOfRetry == 0) && (Modbus_Tcp_Slaves[index].Connect == FALSE))
			//Commented by DK on 6 Feb 2015 for PCM to Scale connectivity status update
			/*else if (Modbus_Tcp_Slaves[index].Connect == FALSE)
			{
				Modbus_Tcp_Slaves[index].Wait_Count++;  
				if(Modbus_Tcp_Slaves[index].Wait_Count > 5) //changed by DK on 23 Dec 2014 from 10 to 5
				{
					Modbus_Tcp_Slaves[index].NoOfRetry =0;  
					Modbus_Tcp_Slaves[index].Wait_Count=0;
				}	
			}*/	
	/*------------------------------------------------------------------------------------------------------*/					
			if(Modbus_Tcp_Slaves[index].Connect == TRUE)
			{	
LPC_RTC->ALMIN = 16;					
				//RTC update 
				if((Modbus_Tcp_Slaves[index].rtc_update == 1) && (Modbus_Tcp_Slaves[index].NoOfRetry == 0)) //Added by DK on 21 Jan2015 Modbus_Tcp_Slaves[index].NoOfRetry == 0
				{
					usRegStart =SCALE_DATE_REG_ADDR - MODBUS_REGISTER_BASE_ADDR;
					//Current_time = RTCGetTime(); //Commented by DK on 15 Jan2015 
// 					Date_Time[0] = Current_time.RTC_Mday;
// 					Date_Time[0] = Date_Time[0] << 8; 
// 					Date_Time[0] |= Current_time.RTC_Mon;
// 					Date_Time[1] = Current_time.RTC_Year;
					Current_time = RTCGetTime();			//by megha on 11/9/2017 for correct updation of time on scale
					mb_temp_buf[0]=Current_time.RTC_Mday;		
					mb_temp_buf[0] = mb_temp_buf[0] << 8; 
					mb_temp_buf[0] |= Current_time.RTC_Mon;
					mb_temp_buf[1] = Current_time.RTC_Year;	
					if(Pcm_var.Current_Date_Format==MMDDYYYY)			//for compatibility with scale's date format on 28/8/2017
					{
					mb_temp_buf[2] = 1;			

					}
					else
					{
					mb_temp_buf[2] = 2;			

					}
					mb_temp_buf[3] = (Current_time.RTC_Wday + 1);  

					mb_temp_buf[4] = Current_time.RTC_Sec;  
					mb_temp_buf[4] = mb_temp_buf[4] << 8; 
					mb_temp_buf[4] |= Current_time.RTC_Min;
					mb_temp_buf[5] = Current_time.RTC_Hour;					
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					//sprintf(mb_temp_buf, "Writing RTC for Slave%d!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
					sprintf(Debug_Buf, "Writing RTC for Slave%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
						os_mut_wait (&usb_log_mutex, 0xffff);
					sprintf(&system_log.log_buf[0],"Synch. date of scale %d in process",Modbus_Tcp_Slaves[index].Scale_ID);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);					
LPC_RTC->ALMIN = 17;								
					
					if( MB_ENOERR !=( eStatus = eMBMWriteMultipleRegisters(Modbus_Tcp_Slaves[index].xMBMHandle,Modbus_Tcp_Slaves[index].Slave_ID,usRegStart,6,&mb_temp_buf[0])))
					{
LPC_RTC->ALMIN = 18;						
						//Writing of Holding resgisters failed. 
							Modbus_Tcp_Slaves[index].NoOfRetry++;
						
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(mb_temp_buf, "MB TCP Registers writing failed for Slave%d!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
							sprintf(Debug_Buf, "MB TCP Multiple Registers(RTC Date) writing failed for Slave%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
					os_mut_wait (&usb_log_mutex, 0xffff);						
					sprintf(&system_log.log_buf[0],"Synch. date of scale %d is failed",Modbus_Tcp_Slaves[index].Scale_ID);
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);
					}
					else //Writing of Date (Month,Day & Year) successful 
					{
LPC_RTC->ALMIN = 19;						
					os_mut_wait (&usb_log_mutex, 0xffff);						
					sprintf(&system_log.log_buf[0],"Synch. date of scale %d is successful",Modbus_Tcp_Slaves[index].Scale_ID);
					log_file_write(system_log1);		
					os_mut_release (&usb_log_mutex);
						
							if(Modbus_Tcp_Slaves[index].NoOfRetry>0)
							Modbus_Tcp_Slaves[index].NoOfRetry--;
							Modbus_Tcp_Slaves[index].rtc_update = FALSE;
/*
							usRegStart =SCALE_TIME_REG_ADDR - MODBUS_REGISTER_BASE_ADDR;
						//	Current_time = RTCGetTime();  //Commented by DK on 15 Jan2015 
							Date_Time[0] = Current_time.RTC_Sec;  
							Date_Time[0] = Date_Time[0] << 8; 
							Date_Time[0] |= Current_time.RTC_Min;
							Date_Time[1] = Current_time.RTC_Hour;
LPC_RTC->ALMIN = 21;							
					os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALMIN = 22;							
					sprintf(&system_log.log_buf[0],"Synch. time of scale %d in process",Modbus_Tcp_Slaves[index].Scale_ID);
					log_file_write(system_log1);		
					os_mut_release (&usb_log_mutex);
							
							if( MB_ENOERR !=( eStatus = eMBMWriteMultipleRegisters(Modbus_Tcp_Slaves[index].xMBMHandle,Modbus_Tcp_Slaves[index].Slave_ID,usRegStart,2,&Date_Time[0])))
							{
							//Writing of Holding resgisters failed. 
								Modbus_Tcp_Slaves[index].NoOfRetry++;
								
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								//sprintf(mb_temp_buf, "Writing RTC for Slave%d failed!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
								sprintf(Debug_Buf, "MB TCP Multiple Registers(RTC Time) writing failed for Slave%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
LPC_RTC->ALMIN = 23;								
							os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALMIN = 24;								
							sprintf(&system_log.log_buf[0],"Synch. time of scale %d is failed",Modbus_Tcp_Slaves[index].Scale_ID);
							log_file_write(system_log1);	
							os_mut_release (&usb_log_mutex);
								
							}
							else //Writing of Time (Minutes, Time & Hour) successful 
							{
LPC_RTC->ALMIN = 25;								
							os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALMIN = 26;								
								
								sprintf(&system_log.log_buf[0],"Synch. time of scale %d is successful",Modbus_Tcp_Slaves[index].Scale_ID);
								log_file_write(system_log1);		
							os_mut_release (&usb_log_mutex);
								
							if(Modbus_Tcp_Slaves[index].NoOfRetry>0)
							Modbus_Tcp_Slaves[index].NoOfRetry--;								
								usRegStart =SCALE_DOW_REG_ADDR - MODBUS_REGISTER_BASE_ADDR;
								Date_Time[0] = (Current_time.RTC_Wday + 1);
LPC_RTC->ALMIN = 27;							
							os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALMIN = 28;							
							sprintf(&system_log.log_buf[0],"Synch. week day of scale %d in process",Modbus_Tcp_Slaves[index].Scale_ID);
							log_file_write(system_log1);		
							os_mut_release (&usb_log_mutex);
							
								if( MB_ENOERR !=( eStatus = eMBMWriteMultipleRegisters(Modbus_Tcp_Slaves[index].xMBMHandle,Modbus_Tcp_Slaves[index].Slave_ID,usRegStart,1,&Date_Time[0])))
								{
									//Writing of Holding resgisters failed. 
									Modbus_Tcp_Slaves[index].NoOfRetry++;
LPC_RTC->ALMIN = 29;									
							os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALMIN = 30;									
							sprintf(&system_log.log_buf[0],"Synch. week day of scale %d is failed",Modbus_Tcp_Slaves[index].Scale_ID);
							log_file_write(system_log1);	
							os_mut_release (&usb_log_mutex);
									
								}
								else //Writing of Day of week register successful 
								{
LPC_RTC->ALMIN = 31;									
								os_mut_wait (&usb_log_mutex, 0xffff);							
LPC_RTC->ALMIN = 32;								
								sprintf(&system_log.log_buf[0],"Synch. week day of scale %d is successful",Modbus_Tcp_Slaves[index].Scale_ID);
								log_file_write(system_log1);			
							os_mut_release (&usb_log_mutex);
									
									if(Modbus_Tcp_Slaves[index].NoOfRetry>0)
									Modbus_Tcp_Slaves[index].NoOfRetry--;									
									Modbus_Tcp_Slaves[index].rtc_update = FALSE;
									
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									//sprintf(mb_temp_buf, "RTC Written to Scale %d!\r\n",Modbus_Tcp_Slaves[index].Slave_ID);
									sprintf(Debug_Buf, "RTC Written to Slave %d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif
									
								}//end of else //Writing of Day of week register successful 									
							}*/			//end of else //Writing of Time (Minutes, Time & Hour) successful 	
					} //end of else //Writing of Date (Month,Day & Year) successful 
				} //end of if(Modbus_Tcp_Slaves[index].rtc_update == TRUE)					
				
//				scale_config_req_fg = 0;				
				//On demand from PCS read these variables 
				if((MB_Polling == RUNTIME_POLLING) && (Modbus_Tcp_Slaves[index].NoOfRetry == 0))
				{	
					//Scale runtime variables
					usNoOfRecords=MB_SCALE_RUNTIME_NUM_RECORDS;				
					//read all the required variables from the start address to the end address
					for(r_index = 0; r_index < usNoOfRecords; r_index++)	
					{	
						usNoOfRegs	=Scale_runtime_tcp_var [index][r_index].number_of_elements;
						usRegStart 	=Scale_runtime_tcp_var [index][r_index].modbus_reg_start_addr - MODBUS_REGISTER_BASE_ADDR;
						if( MB_ENOERR !=( eStatus = eMBMReadHoldingRegisters(Modbus_Tcp_Slaves[index].xMBMHandle, Modbus_Tcp_Slaves[index].Slave_ID, usRegStart,usNoOfRegs,Scale_runtime_tcp_var [index][r_index].var_data) ) )
						{
							//Reading of Holding resgisters failed. 
							Modbus_Tcp_Slaves[index].NoOfRetry++;
							
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(Debug_Buf, "MB TCP Registers reading failed for Slave%d!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
							sprintf(Debug_Buf, "MB TCP Registers reading failed for Slave%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
							
							break; //added by DK on 22 Dec 2014 
						}
						else 
						{
							if(Modbus_Tcp_Slaves[index].NoOfRetry>0)
							Modbus_Tcp_Slaves[index].NoOfRetry--;
						}
					}
				}	//end of if(MB_Polling == RUNTIME_POLLING)			
				
				if((MB_Polling == TOTALS_POLLING) && (Modbus_Tcp_Slaves[index].NoOfRetry == 0))
				{	
					//Scale Total variables
					usNoOfRecords=MB_SCALE_TOTALS_NUM_RECORDS;				
					//read all the required variables from the start address to the end address
					for(r_index = 0; r_index < usNoOfRecords; r_index++)	
					{	
						usNoOfRegs	= Scale_Total_tcp_var [index][r_index].number_of_elements;
						usRegStart 	= Scale_Total_tcp_var [index][r_index].modbus_reg_start_addr - MODBUS_REGISTER_BASE_ADDR;
						if( MB_ENOERR !=( eStatus = eMBMReadHoldingRegisters(Modbus_Tcp_Slaves[index].xMBMHandle, Modbus_Tcp_Slaves[index].Slave_ID, usRegStart,usNoOfRegs,Scale_Total_tcp_var [index][r_index].var_data) ) )
						{
							//Reading of Holding resgisters failed. 
							Modbus_Tcp_Slaves[index].NoOfRetry++;
							
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(mb_temp_buf, "MB TCP Registers reading failed for Slave%d!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
							sprintf(Debug_Buf, "MB TCP Registers reading failed for Slave%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
							
							break; //added by DK on 22 Dec 2014 
						}
						else 
						{
							if(Modbus_Tcp_Slaves[index].NoOfRetry>0)
							Modbus_Tcp_Slaves[index].NoOfRetry--;
						}
					 }
				} //end of if(MB_Polling == TOTALS_POLLING)
				 
				//Scale Record first set 
				Current_time = RTCGetTime();
				usNoOfRecords=MB_SCALE_RECORD_NUM_RECORDS;
				if(((Modbus_Tcp_Slaves[index].cur_record) == (Modbus_Tcp_Slaves[index].prev_record)) && (Modbus_Tcp_Slaves[index].NoOfRetry == 0)
					&&((Scale_Record_Mb_Reg[index].Scale_Time.int_array[0] & 0x003F) != Current_time.RTC_Min))
				{	
LPC_RTC->ALMIN = 20;					
					//read all the required variables from the start address to the end address
					/*#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(mb_temp_buf,'\0',sizeof(mb_temp_buf));
					sprintf(mb_temp_buf, "MB TCP Registers reading for Slave%d!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
					Uart_i2c_data_send((uint8_t *)mb_temp_buf, strlen(mb_temp_buf));
					os_mut_release (&debug_uart_mutex);
					#endif  */
//BY MEGHA FOR SINGLE QUERY FOR ALL REGISTERS					
//					for(r_index = 0; r_index < usNoOfRecords; r_index++)	
					{	
						usNoOfRegs=54;//MB_SCALE_RECORD_NUM_RECORDS;//MB_SCALE_RECORD_NUM_RECORDS;//sizeof(Scale_Record_Mb_Reg[index])/2;//Scale_Record_Data_tcp_var [index][r_index].number_of_elements;
						usRegStart =SCALE_ID_REG_ADDR- MODBUS_REGISTER_BASE_ADDR;//Scale_Record_Data_tcp_var [index][r_index].modbus_reg_start_addr - MODBUS_REGISTER_BASE_ADDR;
						if(power_on_fg == 1)
						{
							strcpy(Debug_Buf, "REQUESTING RUN TIME DATA FROM SCALE...");
							GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(10*16)));	
							os_mut_wait (&usb_log_mutex, 0xffff);							
						sprintf(&system_log.log_buf[0],"Requesting run time data from scale......");
						log_file_write(system_log1);	
						os_mut_release (&usb_log_mutex);							
						}	
LPC_RTC->ALMIN = 21;						
						if( MB_ENOERR !=( eStatus = eMBMReadHoldingRegisters(Modbus_Tcp_Slaves[index].xMBMHandle, Modbus_Tcp_Slaves[index].Slave_ID, usRegStart,usNoOfRegs,mb_temp_buf)))//Scale_Record_Data_tcp_var [index][r_index].var_data) ) )
						{
									Flags_struct.Plant_connect_record_flag &= ~SCALE_DATA_RECEIVE_OK_FLAG;
									GUI_data_nav.Error_wrn_msg_flag[SCALE_DATA_RECEIVE_OK_MSG_TIME] = 0;
LPC_RTC->ALMIN = 22;							
									if(power_on_fg == 1)
									{
										strcpy(Debug_Buf, "FAILED TO READ DATA FROM SCALE...");
										GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(11*16)));	
										
							os_mut_wait (&usb_log_mutex, 0xffff);							
									
						sprintf(&system_log.log_buf[0],"Failed to read data from scale......");
						log_file_write(system_log1);	
						os_mut_release (&usb_log_mutex);										
									}							
						//Reading of Holding resgisters failed. 
							Modbus_Tcp_Slaves[index].NoOfRetry++;
							
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(Debug_Buf, "MB TCP Registers reading failed for Slave%d!\r\n", Modbus_Tcp_Slaves[index].Slave_ID);	
							sprintf(Debug_Buf, "MB TCP Registers reading failed for Slave%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
							
//							break; //added by DK on 22 Dec 2014 
						}
						else 
						{
LPC_RTC->ALMIN = 23;							
									r_index = 0;
//									memcpy(Scale_Record_Mb_Reg[index].Scale_Id,mb_temp_buf[r_index],sizeof(Scale_Record_Mb_Reg[index].Scale_Id));
									Scale_Record_Mb_Reg[index].Scale_Id.uint_val = mb_temp_buf[r_index];
							
									r_index++;
									Scale_Record_Mb_Reg[index].Function_Code.uint_val = mb_temp_buf[r_index];
							
									r_index++;
									Scale_Record_Mb_Reg[index].Record_Hash.uint_val = mb_temp_buf[r_index];
							
									r_index++;
									Scale_Record_Mb_Reg[index].Scale_Date.int_array[0] = (mb_temp_buf[r_index]);//(Date_Time[0] >> 8);
									r_index++;
									Scale_Record_Mb_Reg[index].Scale_Date.int_array[1] = (mb_temp_buf[r_index]);
							
									r_index++;
									Scale_Record_Mb_Reg[index].Scale_Time.int_array[0] = (mb_temp_buf[r_index]);
									r_index++;
									Scale_Record_Mb_Reg[index].Scale_Time.int_array[1] = (mb_temp_buf[r_index]);
							
									r_index++;
									Scale_Record_Mb_Reg[index].acc_wt.s_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].acc_wt.s_array[1] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].acc_wt.s_array[2] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].acc_wt.s_array[3] = mb_temp_buf[r_index];



									r_index++;
									Scale_Record_Mb_Reg[index].accu_wt_unit.uint_val = mb_temp_buf[r_index];
									
									r_index++;
									Scale_Record_Mb_Reg[index].scale_rate.f_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].scale_rate.f_array[1] = mb_temp_buf[r_index];
									
									r_index++;
									Scale_Record_Mb_Reg[index].rate_time_unit.uint_val = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].scale_load_perc.f_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].scale_load_perc.f_array[1] = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].scale_angle.f_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].scale_angle.f_array[1] = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].belt_speed.f_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].belt_speed.f_array[1] = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].belt_speed_unit.uint_val = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].daily_wt.s_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].daily_wt.s_array[1] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].daily_wt.s_array[2] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].daily_wt.s_array[3] = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].daily_wt_unit.uint_val = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].master_total.ulong_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].master_total.ulong_array[1] = mb_temp_buf[r_index];
// 									Long_val = (Long_val <<16);
// 									Long_val |= mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].master_total.ulong_array[2] = mb_temp_buf[r_index];
// 									Long_val = (Long_val <<16);
// 									Long_val |= mb_temp_buf[r_index];								
									r_index++;
									Scale_Record_Mb_Reg[index].master_total.ulong_array[3] = mb_temp_buf[r_index];
// 									Long_val = (Long_val <<16);
// 									Long_val |= mb_temp_buf[r_index];									

									
// 									f_accum_wt_tmp = (U64)((U64)(Scale_Record_Mb_Reg[index].master_total.s_array[0]) |
// 									(U64)((U64)(Scale_Record_Mb_Reg[index].master_total.s_array[1] << 16)) |
// 									(U64)((U64)(Scale_Record_Mb_Reg[index].master_total.s_array[2] << 32)) |
// 									(U64)((U64)(Scale_Record_Mb_Reg[index].master_total.s_array[3] << 48))) ;
									
//									f_accum_wt_tmp = Scale_Record_Mb_Reg[index].master_total.double_val;//[0];

									r_index++;
									Scale_Record_Mb_Reg[index].daily_total.ulong_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].daily_total.ulong_array[1] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].daily_total.ulong_array[2] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].daily_total.ulong_array[3] = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].zero_number.int_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].zero_number.int_array[1] = mb_temp_buf[r_index];
// 									r_index++;
// 									Scale_Record_Mb_Reg[index].zero_number.ulong_array[2] = mb_temp_buf[r_index];
// 									r_index++;
// 									Scale_Record_Mb_Reg[index].zero_number.ulong_array[3] = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].speed_feet_min.int_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].speed_feet_min.int_array[1] = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].inst_rate_pounds_hour.ulong_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].inst_rate_pounds_hour.ulong_array[1] = mb_temp_buf[r_index];										
									r_index++;
									Scale_Record_Mb_Reg[index].inst_rate_pounds_hour.ulong_array[2] = mb_temp_buf[r_index];										
									r_index++;
									Scale_Record_Mb_Reg[index].inst_rate_pounds_hour.ulong_array[3] = mb_temp_buf[r_index];										
									
									r_index++;
									Scale_Record_Mb_Reg[index].scale_usb_health.uint_val = mb_temp_buf[r_index];

									r_index++;
									Scale_Record_Mb_Reg[index].job_total_pounds.ulong_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].job_total_pounds.ulong_array[1] = mb_temp_buf[r_index];										
									r_index++;
									Scale_Record_Mb_Reg[index].job_total_pounds.ulong_array[2] = mb_temp_buf[r_index];										
									r_index++;
									Scale_Record_Mb_Reg[index].job_total_pounds.ulong_array[3] = mb_temp_buf[r_index];	

									r_index++;
									Scale_Record_Mb_Reg[index].accumulated_weight_pounds.ulong_array[0] = mb_temp_buf[r_index];
									r_index++;
									Scale_Record_Mb_Reg[index].accumulated_weight_pounds.ulong_array[1] = mb_temp_buf[r_index];										
									r_index++;
									Scale_Record_Mb_Reg[index].accumulated_weight_pounds.ulong_array[2] = mb_temp_buf[r_index];										
									r_index++;
									Scale_Record_Mb_Reg[index].accumulated_weight_pounds.ulong_array[3] = mb_temp_buf[r_index];	
									
									r_index++;
									Scale_Record_Mb_Reg[index].Record_id.int_array[1] = (mb_temp_buf[r_index]);
									r_index++;
									Scale_Record_Mb_Reg[index].Record_id.int_array[0] = (mb_temp_buf[r_index]);
									
							Flags_struct.Plant_connect_record_flag &=	~ MODBUS_TCP_CONNECTION_FAIL_FG;
							GUI_data_nav.Error_wrn_msg_flag[MODBUS_TCP_FAIL_MSG_TIME] = 0;	
							
									Flags_struct.Plant_connect_record_flag = SCALE_DATA_RECEIVE_OK_FLAG;
									GUI_data_nav.Error_wrn_msg_flag[SCALE_DATA_RECEIVE_OK_MSG_TIME] = 0;
									disp_rx_scale_id = index +1;
LPC_RTC->ALMIN = 37;							
									if(power_on_fg == 1)
									{
										strcpy(Debug_Buf, "SCALE DATA RECEIVED ON PCM");
										GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0+(11*16)));	

							os_mut_wait (&usb_log_mutex, 0xffff);							
										
						sprintf(&system_log.log_buf[0],"Scale data received on PCM......");
						log_file_write(system_log1);	
						os_mut_release (&usb_log_mutex);										
										power_on_fg = 0;	
									}	

							if(Modbus_Tcp_Slaves[index].NoOfRetry>0)
							Modbus_Tcp_Slaves[index].NoOfRetry--;
							
							//Added on 25 Nov 2014 to check for new record  //Commented by DK on 18 September 2015 
						/*	if((usRegStart == (SCALE_RECORD_HASH_REG_ADDR - MODBUS_REGISTER_BASE_ADDR) ) && \
								 (Modbus_Tcp_Slaves[index].cur_record == Scale_Record_Mb_Reg[index].Record_Hash.uint_val))
							{
								break; //no need to read rest of data as record not updated
							}*/	
							
						}
					}
					if(eStatus == MB_ENOERR)
					{
						Modbus_Tcp_Slaves[index].cur_record=Scale_Record_Mb_Reg[index].Record_id.value;//cHANGED BY MEGHA FOR 32BIT RECORD ID//Scale_Record_Mb_Reg[index].Record_Hash.uint_val; 
					}	
				} //end of if((Modbus_Tcp_Slaves[index].cur_record) == (Modbus_Tcp_Slaves[index].prev_record))

			
				if ((Modbus_Tcp_Slaves[index].NoOfRetry > 0)/* ||(pcs_connectivity_status == NOT_CONNECTED)*/) //changed by DK on 22 Dec 2014 from 3 to 0 
				{
					
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					//sprintf(mb_temp_buf, "Disconnecting & closing MB TCP Scale %d!\r\n",Modbus_Tcp_Slaves[index].Slave_ID);
					sprintf(Debug_Buf, "Disconnecting & closing MB TCP Slave %d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					Modbus_Tcp_Slaves[index].rtc_update = 0;
					Modbus_Tcp_Slaves[index].Connect = FALSE;
					Modbus_Tcp_Slaves[index].scale_connectivity_status = NOT_CONNECTED;
					Modbus_Tcp_Slaves[index].socket_number = 0;

					
					
LPC_RTC->ALMIN = 24;					
					if( MB_ENOERR != ( eStatus = eMBMTCPDisconnect( Modbus_Tcp_Slaves[index].xMBMHandle) ) )
					{		
LPC_RTC->ALMIN = 25;						
							//Can not disconnect from Modbus TCP slave
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(mb_temp_buf, "Can not disconnect from Scale %d!\r\n",Modbus_Tcp_Slaves[index].Slave_ID);
							sprintf(Debug_Buf, "Can not disconnect from Slave %d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
					}
LPC_RTC->ALMIN = 26;					
					if( MB_ENOERR != ( eStatus = eMBMClose( Modbus_Tcp_Slaves[index].xMBMHandle ) ) )
					{
LPC_RTC->ALMIN = 27;						
						//Can not close MODBUS instance
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(mb_temp_buf, "Can not Close MB connection for Scale %d!\r\n",Modbus_Tcp_Slaves[index].Slave_ID);
							sprintf(Debug_Buf, "Can not Close MB connection for Slave %d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Modbus_Tcp_Slaves[index].Slave_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
					}
LPC_RTC->ALMIN = 28;					
					Modbus_Tcp_Slaves[index].Connect= FALSE;
					Modbus_Tcp_Slaves[index].NoOfRetry =0;  //Added by DK on 18 Nov2014 
					Modbus_Tcp_Slaves[index].scale_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
					//Modbus_Tcp_Slaves[index].Wait_Count=0;	//Commented by DK on 6 Feb 2015 for PCM to Scale connectivity status update
				}	
			}  //end of if(Modbus_Tcp_Slaves[index].Connect == TRUE)
//			os_mut_release (&tcp_mutex);  //Added by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015 PCM firmware V1.33
			os_dly_wait(10);	
		}	//end of for(index=0; index < MAX_NO_OF_INTEGRATORS; index++)
		//os_mut_release (&tcp_mutex);  //Added by DK on 23 Dec 2014 
	LPC_RTC->GPREG4 &= ~(0x01<<7);
//		WDTFeed();	
} //end of while (1)			
}	
 

#endif //#ifdef MODBUS_TCP
/*****************************************************************************
* End of file
*****************************************************************************/
