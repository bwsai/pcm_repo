/* 
 * MODBUS Slave Library: A portable MODBUS slave for MODBUS ASCII/RTU/TCP/UDP.
 * Copyright (c) 2007-2011 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbsudp.h,v 1.1 2011/12/05 23:46:23 embedded-solutions.cwalter Exp $
 */

#ifndef _MBSUDP_H
#define _MBSUDP_H

#ifdef __cplusplus
PR_BEGIN_EXTERN_C
#endif

#if MBS_UDP_ENABLED == 1

/*! 
 * \if INTERNAL_DOCS
 * \addtogroup mbs_udp_int
 * @{
 * \endif
 */

/* ----------------------- Defines ------------------------------------------*/

/* ----------------------- Type definitions ---------------------------------*/

/* ----------------------- Function prototypes ------------------------------*/
eMBErrorCode eMBSUDPIntInit( xMBSInternalHandle *pxIntHdl, CHAR *pcBindAddress, USHORT usUDPPort );

/*!
 * \if INTERNAL_DOCS
 * @}
 * \endif
 */

#endif

#ifdef __cplusplus
PR_END_EXTERN_C
#endif

#endif
