/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : EEPROM_high_level.h
* @brief			         : Controller Board
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __EEPROM_HIGH_LEVEL_H
#define __EEPROM_HIGH_LEVEL_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "lpc177x_8x_eeprom.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

#ifdef EEPROM
#define PCM_VAR_NO_OF_PAGES      2


#define EEPROM_BACKUP_FLAG           0xAE
#define PAGE_NO_LOCATION             22
#define OFFSET_FOR_FLAG              0
#define OFFSET_FOR_PAGE_NO           1  //corresponds to 1st location
#define OFFSET_FOR_COUNTER           2  //corresponds to 8 bits variable since pg number is 8 bit var
#define ENDURANCE_CYCLES             99990
#define OFFSET_FOR_ENDURANCE_CTR     8  //corresponds to 64 bits/8 byte var since wt is a double
#define EEPROM_PAGE_NO_WT_BACKUP     40

#define EEPROM_WRITE_ERR_FLAG        0x01
#define EEPROM_WRITE_ERR_PAGE_NO     62

#define PCM_VAR_START_PAGE_NO    0
#define MAC_PAGE_NO							 25// original 21 on 10/1/2017 to keep compatible with previous modem mac id 	(PCM_VAR_START_PAGE_NO + PCM_VAR_NO_OF_PAGES)
//#define RPRTS_DIAG_START_PAGE_NO		 (ADMIN_START_PAGE_NO + ADMIN_NO_OF_PAGES)//20
#define OUI1         0x00
#define OUI2         0x11
#define OUI3         0x27

/*============================================================================
* Public Data Types
*===========================================================================*/
typedef struct
              {
								 uint8_t Page_number;
								 uint8_t Offset;
								 double Endurance_counter;
							}EEPROM_WT_BACKUP_STRUCT;
							
typedef union {
   unsigned int  uint_val;
   unsigned char uint_array[4];
}UINT_TYPE;
typedef struct {
	               UINT_TYPE oui; // Organization Unique ID
	               UINT_TYPE next_MAC; // Next MAC address
	               UINT_TYPE available_MACs; // Available MAC ID's
	               UINT_TYPE checksum;  // Check Sum for the file
}MAC_FILE_DATA;
// typedef union
// 							{
// 								 U8 oui[3];
// 								 U8 mac[6];
// 							}MAC_ID_STRUCT;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
extern unsigned char Struct_backup_fg;
/* Character variables section */
	
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern EEPROM_WT_BACKUP_STRUCT EEPROM_wt_backup;
extern U8 own_hw_adr[];
extern char MAC_String[MAC_OCTETS*3];
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void eeprom_first_time_write(void);
extern void eeprom_weight_backup(void);
extern void eeprom_weight_reload(void);
extern void eeprom_struct_backup(void);
extern void eeprom_struct_reload(void);
extern U8 eeprom_program_MAC_id(void);
void EEP_load_good_data (void);
#endif /*#ifdef EEPROM*/

#endif /*__EEPROM_HIGH_LEVEL_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
