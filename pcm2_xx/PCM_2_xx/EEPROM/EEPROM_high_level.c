/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename       : EEPROM_high_level.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "EEPROM_high_level.h"
#include "Ext_Data_flag.h"
#include <ctype.h>
#include "../mvt/mvt.h"
#include "../GUI/Application/Screen_global_ex.h"
#include "Ext_flash_high_level.h"
#include "../gui/Display_task.h"
#include "I2C_driver.h"

#ifdef EEPROM
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define SECRET_KEY 		289317
#define MAC_OCTETS		6
#define M1 						23
#define M2 						45
#define LAST_MAC_ID   800000
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
unsigned char Struct_backup_fg;
/* Character variables section */
char MAC_String[MAC_OCTETS*3];
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
EEPROM_WT_BACKUP_STRUCT EEPROM_wt_backup;
/*uint32_t Correctly_read = 0, Incorrectly_read = 0;
double old_weight = 0;*/
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */
// float g_old_fw_ver = 0.00;
// float g_current_fw_ver = 0.0;
/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
U8 EEP_FL_chk_EEP_data_validity (void);
void EEP_FL_restore_default_to_EEP (void);
void EEP_FL_copy_EEP_to_FL (void);
void EEP_FL_restore_flash_to_EEP (void);
void EEP_load_good_data (void);
static char parse_macfile(FILE *fp, MAC_FILE_DATA *mac_data);
static void update_field( FILE *MAC_file1, MAC_FILE_DATA *mac_data);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
void eeprom_first_time_write(void)
{
	  int i;
	  uint8_t backup_flag = EEPROM_BACKUP_FLAG;

	  //read page location from Pg no 22 at offset 0
    EEPROM_Read (OFFSET_FOR_FLAG, PAGE_NO_LOCATION, &backup_flag, MODE_8_BIT, sizeof(backup_flag));

	  if (backup_flag != EEPROM_BACKUP_FLAG)
		{
			  backup_flag = EEPROM_BACKUP_FLAG;
				for (i=PAGE_NO_LOCATION; i<EEPROM_PAGE_NO_WT_BACKUP; i++)
				{
					 EEPROM_Erase(i);
				}
				EEPROM_wt_backup.Endurance_counter = 0;
				EEPROM_wt_backup.Offset = 3; //offset for backup flag + page number + offset -->req for first pg only
				EEPROM_wt_backup.Page_number = 0;

				//write page location from Pg no 22 at offset 0
				EEPROM_Write (OFFSET_FOR_FLAG, PAGE_NO_LOCATION, &backup_flag, MODE_8_BIT, sizeof(backup_flag));

				//write page location from Pg no 22 at offset 1
				EEPROM_Write (OFFSET_FOR_PAGE_NO, PAGE_NO_LOCATION, &EEPROM_wt_backup.Page_number,
											 MODE_8_BIT, sizeof(EEPROM_wt_backup.Page_number));

				//write page location from Pg no 22 at offset 2
				EEPROM_Write (OFFSET_FOR_COUNTER, PAGE_NO_LOCATION, &EEPROM_wt_backup.Offset,
											 MODE_8_BIT, sizeof(EEPROM_wt_backup.Offset));

				//write the endurance counter value
				EEPROM_Write ((EEPROM_wt_backup.Offset + OFFSET_FOR_ENDURANCE_CTR),
											(EEPROM_wt_backup.Page_number + PAGE_NO_LOCATION),
											&EEPROM_wt_backup.Endurance_counter, MODE_8_BIT, sizeof(EEPROM_wt_backup.Endurance_counter));
				eeprom_struct_backup();  //Commented by DK on 12 sep 2014 for PCM 
 				#ifdef EXT_INT
				//erase the flash
				ExtFlash_eraseBlock(EXT_FLASH_LAST_SECTOR_START_ADDR);
				ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_START_ADDR);
				//PVK
				ExtFlash_eraseBlock(EXT_FLASH_PARA_BACKUP_COPY_START_ADDR);
				#endif				
		}
		else
		{
			EEP_load_good_data();		//Load only the known good config data
		}
		//eeprom_weight_reload();  //Commented by DK on 12 sep 2014 for PCM 

		return;
}

/*****************************************************************************
* @note       Function name: void EEP_load_good_data (void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 12, 2015
* @brief      Description	 : Checks the validity of EEPROM and flash copy data
														 and takes the decision of loading good known data
* @note       Notes		     : 
*****************************************************************************/
void EEP_load_good_data (void)
{
	U8 EEP_data_chk = 0;
	U8 FL_data_chk = 0;
	
	EEP_data_chk = EEP_FL_chk_EEP_data_validity();	//Check EEP data validity
	FL_data_chk = EEP_FL_chk_FL_data_validity();		//Check flash backup copy data validity
	
	if(EEP_data_chk)		//If EEP data is ok
	{
		eeprom_struct_reload();		//Use EEP data
		if(!FL_data_chk)
		{
			EEP_FL_copy_EEP_to_FL();	//Copy data from EEP to flash
		}
	}
	else		//EEP data is not ok
	{
		if(FL_data_chk)		//If flash data is ok
		{
			EEP_FL_restore_flash_to_EEP();		//Load data from flash to EEP
			eeprom_struct_reload();						//Use EEP data
		}
		else		//Flash data is also not ok
		{

				Struct_backup_fg = 1;
				EEP_FL_restore_default_to_EEP();	//Restore default data to EEP and to flash backup as well
				Struct_backup_fg = 0;
			
		}
	}
}
/*****************************************************************************
* @note       Function name: void EEP_FL_restore_default_to_EEP (void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Restores the default config data to EEPROM
* @note       Notes		     : 
*****************************************************************************/
void EEP_FL_restore_default_to_EEP (void)
{
	init_EEP_conf_vars();
	eeprom_struct_backup();
}
/*****************************************************************************
* @note       Function name: void EEP_FL_restore_flash_to_EEP (void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Restores good copy of config data from flash
														 to EEPROM
* @note       Notes		     : 
*****************************************************************************/
void EEP_FL_restore_flash_to_EEP (void)
{
	int i, nos_of_pages = 0;
	U8 data_buf[1000];
	
	/*Erase the required pages before updating*/
	nos_of_pages = PCM_VAR_NO_OF_PAGES;
	for(i=PCM_VAR_START_PAGE_NO; i<nos_of_pages; i++)
	{
		 EEPROM_Erase(i);
	}
	
	/*Scale setup structure*/
	memcpy((U16 *)&data_buf, (U16*)EEP_FL_PCM_VAR_START, sizeof(Pcm_var));
	EEPROM_Write (0, PCM_VAR_START_PAGE_NO, &data_buf,
								MODE_8_BIT, sizeof(Pcm_var));

	
}

/*****************************************************************************
* @note       Function name: void EEP_FL_copy_EEP_to_FL (void)
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Copy EEPROM config data to flash based backup copy
* @note       Notes		     : 
*****************************************************************************/
void EEP_FL_copy_EEP_to_FL (void)
{
	U16 * u16Var;
	U16 i;
	U16 *Current_write_addr_ptr;
	U8 data_buf[1000];
	
	EEPROM_Read (0, PCM_VAR_START_PAGE_NO, &data_buf, MODE_8_BIT, sizeof(Pcm_var));
	Current_write_addr_ptr = (U16 *)EEP_FL_PCM_VAR_START;
	
	//Added 23 Mar 2016
	ExtFlash_eraseBlock(SECTOR_123_START_ADDR);
	
	u16Var = (U16*)data_buf;
	for(i=0; i<(FLASH_SIZEOF(Pcm_var)); i++)
	{
		ExtFlash_writeWord((uint32_t)Current_write_addr_ptr, *u16Var++);
		Current_write_addr_ptr++;
	}
	

}
/*****************************************************************************
* @note       Function name: U8 EEP_FL_chk_EEP_data_validity (void)
* @returns    returns		   : status(valid-1/invalid-0)
* @param      arg1			   : None
* @author			             : Suvrat Joshi
* @date       date created : Dec 11, 2015
* @brief      Description	 : Checks the validity of EEPROM data
* @note       Notes		     : 
*****************************************************************************/
U8 EEP_FL_chk_EEP_data_validity (void)
{
	U16 calculated_CRC = 0, i=0;
	U16 stored_CRC = 0;
	U8 data_buf[1000];
	U8 status = 0;
	U16 fw_ver_idx = 0;
	char old_fw_ver[6] = {0};
	


	EEPROM_Read (0, PCM_VAR_START_PAGE_NO, &data_buf, MODE_8_BIT, sizeof(Pcm_var));
	fw_ver_idx = (U16)offsetof(PCM_STRUCT, Int_firmware_version);	//Get the old firmware version number
	for(i=0; i<5; i++)
	{
		old_fw_ver[i] = data_buf[fw_ver_idx+i];
	}
	g_old_fw_ver = atof(old_fw_ver);		//Store the old fw ver for further checking
	CalcCRC((unsigned char*)data_buf, (unsigned char*)&calculated_CRC, (unsigned int)offsetof(PCM_STRUCT, CRC_PCM_VAR));
	stored_CRC = (U16)*(U16*)(data_buf + offsetof(PCM_STRUCT, CRC_PCM_VAR));
	if ((calculated_CRC != stored_CRC))
	{
		status = 0;
	}
	else
	{
		if((calculated_CRC != 0)&&(stored_CRC != 0))
		{	
			status = 1;
					
		}
		else
		{
			status = 0;
		}	
	}	
	return status;
}

/*****************************************************************************
* @note       Function name: void Read_FW_Ver_From_EEPOM(void)
* @returns    returns      : None
* @param      arg1,arg2    : None
* @author                  : Oaces Team
* @date       date created : Dec 30, 2015
* @brief      Description  : Read FW version from ADMIN struct
* @note       Notes        : None
*****************************************************************************/
void Read_FW_Ver_From_EEPOM(void)
{
	char old_fw_ver[6] = {0};
  U16 fw_ver_idx = 0;
  U8  data_buf[1000];
	U16 i=0;

	EEPROM_Read (0, PCM_VAR_START_PAGE_NO, &data_buf, MODE_8_BIT, sizeof(Pcm_var));
	fw_ver_idx = (U16)offsetof(PCM_STRUCT, Int_firmware_version);   //Get the old firmware version number
	for(i=0; i<5; i++)
	{
		old_fw_ver[i] = data_buf[fw_ver_idx+i];
	}
	g_old_fw_ver = atof(old_fw_ver);                          //Store the old fw ver for further checking
	
	
	//Get current firmware version in float format
	strcpy(old_fw_ver,FirmwareVer);

	g_current_fw_ver = atof(old_fw_ver);	
}


/*****************************************************************************
* @note       Function name: void eeprom_weight_backup(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : July 9, 2013
* @brief      Description  : Writes the weight to the EEPROM
* @note       Notes        : None
*****************************************************************************/
//Commented by DK on 12 sep 2014 for PCM 
/*
void eeprom_weight_backup(void)
{
	  int offset;
	  uint8_t eeprom_error_flag;

	  //old_weight = Calculation_struct.Total_weight_accum;
    //read page location from Pg no 63 at offset 0
		EEPROM_Read (0, EEPROM_WRITE_ERR_PAGE_NO, &eeprom_error_flag, MODE_8_BIT, sizeof(eeprom_error_flag));
	  if (eeprom_error_flag != EEPROM_WRITE_ERR_FLAG)
		{
				//read page location from Pg no 22 at offset 1
				EEPROM_Read (OFFSET_FOR_PAGE_NO, PAGE_NO_LOCATION, &EEPROM_wt_backup.Page_number,
										 MODE_8_BIT, sizeof(EEPROM_wt_backup.Page_number));

				//read page location from Pg no 22 at offset 2
				EEPROM_Read (OFFSET_FOR_COUNTER, PAGE_NO_LOCATION, &EEPROM_wt_backup.Offset,
										 MODE_8_BIT, sizeof(EEPROM_wt_backup.Offset));

				//read the endurance counter from the specified page number and specified offset + 8 bytes offset
				EEPROM_Read ((EEPROM_wt_backup.Offset + OFFSET_FOR_ENDURANCE_CTR),
										 (EEPROM_wt_backup.Page_number + PAGE_NO_LOCATION),
										 &EEPROM_wt_backup.Endurance_counter, MODE_8_BIT, sizeof(EEPROM_wt_backup.Endurance_counter));

				//if endurance counter < endurance cycles then write the total weight at the same location
				//and increment the endurance counter
				if (EEPROM_wt_backup.Endurance_counter < ENDURANCE_CYCLES)
				{
						EEPROM_Write (EEPROM_wt_backup.Offset, (EEPROM_wt_backup.Page_number + PAGE_NO_LOCATION),
													&Calculation_struct.Total_weight_accum, MODE_8_BIT,
													sizeof(Calculation_struct.Total_weight_accum));

						EEPROM_wt_backup.Endurance_counter += 1;
						EEPROM_Write ((EEPROM_wt_backup.Offset + OFFSET_FOR_ENDURANCE_CTR),
													(EEPROM_wt_backup.Page_number + PAGE_NO_LOCATION),
													&EEPROM_wt_backup.Endurance_counter, MODE_8_BIT, sizeof(EEPROM_wt_backup.Endurance_counter));
				}
				else
				{
						offset = (EEPROM_wt_backup.Offset + OFFSET_FOR_ENDURANCE_CTR + OFFSET_FOR_ENDURANCE_CTR);
						//check if writing the weight and endurance counter to the next location
						if (offset >= EEPROM_PAGE_SIZE)
						{
							 EEPROM_wt_backup.Offset = 0;
							 EEPROM_wt_backup.Page_number++;
							 if (EEPROM_wt_backup.Page_number > EEPROM_PAGE_NO_WT_BACKUP)
							 {
									Flags_struct.Error_flags &= ~EEPROM_WRITE_ERR;
									eeprom_error_flag = EEPROM_WRITE_ERR_FLAG;
									//write page location from Pg no 63 at offset 0
									EEPROM_Write (0, EEPROM_WRITE_ERR_PAGE_NO, &eeprom_error_flag, MODE_8_BIT, sizeof(eeprom_error_flag));
							 }
						}
						else
						{
							 EEPROM_wt_backup.Offset = EEPROM_wt_backup.Offset + (OFFSET_FOR_ENDURANCE_CTR * 2);
						}

						//write page location from Pg no 22 at offset 1
						EEPROM_Write (OFFSET_FOR_PAGE_NO, PAGE_NO_LOCATION, &EEPROM_wt_backup.Page_number,
													 MODE_8_BIT, sizeof(EEPROM_wt_backup.Page_number));

						//write page location from Pg no 22 at offset 2
						EEPROM_Write (OFFSET_FOR_COUNTER, PAGE_NO_LOCATION, &EEPROM_wt_backup.Offset,
													 MODE_8_BIT, sizeof(EEPROM_wt_backup.Offset));

						EEPROM_Write (EEPROM_wt_backup.Offset, (EEPROM_wt_backup.Page_number + PAGE_NO_LOCATION),
													&Calculation_struct.Total_weight_accum, MODE_8_BIT,
													sizeof(Calculation_struct.Total_weight_accum));

						EEPROM_wt_backup.Endurance_counter = 0;
						EEPROM_Write ((EEPROM_wt_backup.Offset + OFFSET_FOR_ENDURANCE_CTR),
													(EEPROM_wt_backup.Page_number + PAGE_NO_LOCATION),
													&EEPROM_wt_backup.Endurance_counter, MODE_8_BIT, sizeof(EEPROM_wt_backup.Endurance_counter));
				}
		}
		else
		{
			Flags_struct.Error_flags &= ~EEPROM_WRITE_ERR;
		}
    return;
}
*/

/*****************************************************************************
* @note       Function name: void eeprom_weight_reload(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : July 9, 2013
* @brief      Description  : Reloads the stored weight from the EEPROM
* @note       Notes        : None
*****************************************************************************/
//Commented by DK on 12 sep 2014 for PCM 
/*
void eeprom_weight_reload(void)
{
//CALIBRATION_START_PAGE_NO

	 
	//read page location from Pg no 22 at offset 0
    EEPROM_Read (OFFSET_FOR_PAGE_NO, PAGE_NO_LOCATION, &EEPROM_wt_backup.Page_number,
                MODE_8_BIT, sizeof(EEPROM_wt_backup.Page_number));

    //read page location from Pg no 22 at offset 1
    EEPROM_Read (OFFSET_FOR_COUNTER, PAGE_NO_LOCATION, &EEPROM_wt_backup.Offset,
                MODE_8_BIT, sizeof(EEPROM_wt_backup.Offset));

	  EEPROM_Read (EEPROM_wt_backup.Offset, (EEPROM_wt_backup.Page_number + PAGE_NO_LOCATION),
                  &Calculation_struct.Total_weight_accum, MODE_8_BIT,
                  sizeof(Calculation_struct.Total_weight_accum));

	
	
		if (Calculation_struct.Total_weight_accum == old_weight)
		{
			Correctly_read++;
		}
		else
		{
			Incorrectly_read++; 
		}
	  return;
}
*/
/*****************************************************************************
* @note       Function name: void eeprom_struct_backup(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : July 12, 2012
* @brief      Description  : Writes the backup of the configuration to the eeprom
                             to the 1st 20 pages.
* @note       Notes        : None
*****************************************************************************/
//Commented by DK on 12 sep 2014 for PCM 
void eeprom_struct_backup(void)
{
     int i, nos_of_pages = 0;

		#ifdef DEBUG_PORT
	  for(i=0; i < 50; i++)
    {
      Debug_Buf[i] = '\0';
    }			
  	Debug_Buf[0] = '\r';
	  Debug_Buf[1] = '\n';
	  Debug_Buf[2] = 'T';
	  Debug_Buf[3] = '1';
	  Debug_Buf[4] = ' ';
		#endif
	
		//MSA - 13- dec -2016 
	  //Check all parameters for range - min and max if outside load default
	  Verify_PCM_Variables();
	
	/*
	1.	if GUI_data_nav.GUI_structure_backup is 0 gets into loop
	2.  if GUI_data_nav.GUI_structure_backup is 1 and neither of the flag is set goto  loop
	3.	if GUI_data_nav.GUI_structure_backup is 1 and any of the flag is set goto else loop
	*/
	/*	if(!((GUI_data_nav.GUI_structure_backup == 1) && 
			((Scale_setup_param_set_to_default_flag == 1) \
				|| (Calib_param_set_to_default_flag == 1)||(Admin_param_set_to_default_flag == 1) \
				|| (Setup_device_param_set_to_default_flag == 1))))*/
		if((GUI_data_nav.GUI_structure_backup == 1)||(Struct_backup_fg == 1)) {
			EEP_FL_create_backup();			//Always take a backup before writing to EEP	
			/*Erase the required pages before updating*/
			nos_of_pages = PCM_VAR_NO_OF_PAGES;
			for(i=PCM_VAR_START_PAGE_NO; i<nos_of_pages; i++)
			{
				 EEPROM_Erase(i);
			}
			#ifdef DEBUG_PORT
			Debug_Buf[5] = 'T';
			Debug_Buf[6] = '2';
			Debug_Buf[7] = ' ';
			#endif
			
			/*Pcm variables structure*/
			EEPROM_Write (0/*SCALE_SETUP_START_PAGE_NO*/, PCM_VAR_START_PAGE_NO, &Pcm_var,
										MODE_8_BIT, sizeof(Pcm_var));
		
		}	
	
		#ifdef DEBUG_PORT
	  Debug_Buf[8] = 'T';
	  Debug_Buf[9] = '3';
		Debug_Buf[10] = ' ';
		Debug_Buf[11] = ' ';
		#endif

    return;
}




/*****************************************************************************
* @note       Function name: void eeprom_struct_reload(void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : July 12, 2012
* @brief      Description  : Reloads the data from the eeprom to the configuration structures
* @note       Notes        : None
*****************************************************************************/
//Commented by DK on 12 sep 2014 for PCM 
void eeprom_struct_reload(void)
{
    EEPROM_Read (0/*SCALE_SETUP_START_PAGE_NO*/, PCM_VAR_START_PAGE_NO, &Pcm_var,
                  MODE_8_BIT, sizeof(PCM_STRUCT));
		strcpy(&Pcm_var.Int_firmware_version[0],FirmwareVer);
    strcpy(&Pcm_var.Int_update_version[0],FirmwareVer);
	Verify_PCM_Variables();
	
	  //if any one of the parameter is outside the range then write to EEPROM and Flash With CRC 
	  if((1 == Pcm_parameters_set_to_default_fg) )
		{
	    eeprom_struct_backup();
		}		
    return;
}

/*****************************************************************************
* @note       Function name: eeprom_program_MAC_id(void)
* @returns    returns      : On Success returns Zero,
														 On Failure return below CODES for respective conditions
                                  0x01 ----  Already programmed MAC ID
                                  0x02 ----  MAC ERROR:MACfile.txt file not found
                                  0x03 ----  MAC ERROR:USB Key not found
																	0x04 ----  MAC ERROR:Data in file is invalid
                                  0x05 ----  MAC ERROR:Verification Failed  
* @param      arg1         : None
* @author                  : Venkata Krishna Rao
* @date       date created : Feb 24, 2014
* @brief      Description  : Programs the unique MAC ID from USB MAC file to SCALE permenantly for the first time.
* @note       Notes        : None
*****************************************************************************/

U8 eeprom_program_MAC_id(void)
{
	U8 mac_id[6];  // Final Mac ID storing purpose
	MAC_FILE_DATA mac_d; // structure of parameters read from MACfile.txt 
	FILE *mac_file = NULL; // File pointer for the MACfile
	U32 Verify_chk_sum=0;
	memset( &mac_d,0,sizeof(MAC_FILE_DATA) );
	//Check whether valid OUI is present in EEPROM�s MAC ADDR location
	EEPROM_Read( 0,MAC_PAGE_NO,mac_id,MODE_8_BIT,6 );
	//IF valid OUI is not found in EEPROM then look for MAC list file in USB
 	if( !((mac_id[0] == OUI1) && (mac_id[1] == OUI2)) )
 	{
		// IF USB is connected
		//if( Flags_struct.Connection_flags & USB_CON_FLAG )
 		if(USB_Connection & USB_CON_FLAG)
 		{
			//IF MAC list file is found 
			if( (mac_file = fopen ("MACfile.txt", "r+") ) != NULL )
			{
				//Read the MAC ID from file
			   if( 1 == parse_macfile(mac_file, &mac_d) )
				 {
					 Verify_chk_sum =   SECRET_KEY + (mac_d.next_MAC.uint_val) + (M1*mac_d.oui.uint_val) +
		                          (M2*mac_d.available_MACs.uint_val);
					 if(Verify_chk_sum == mac_d.checksum.uint_val)
           { 
							 memset(mac_id,0,sizeof(mac_id));
							 mac_id[0] = OUI1;
							 mac_id[1] = OUI2;
							 mac_id[2] = mac_d.oui.uint_array[0];
							 mac_id[3] = mac_d.next_MAC.uint_array[2];
							 mac_id[4] = mac_d.next_MAC.uint_array[1];
							 mac_id[5] = mac_d.next_MAC.uint_array[0];
							 ////Program MAC ADDR in MAC ADDR location of EEPROM
							 EEPROM_Write (0, MAC_PAGE_NO, mac_id, MODE_8_BIT, sizeof(mac_id));
							 memset(mac_id,0,sizeof(mac_id));
							 // Programmed MAC ID Verfication
							 EEPROM_Read(0,MAC_PAGE_NO,mac_id,MODE_8_BIT,sizeof(mac_id));
							 if((mac_id[0] == OUI1) && (mac_id[1] == OUI2) && (mac_id[2] == mac_d.oui.uint_array[0]) &&
									(mac_id[3] == mac_d.next_MAC.uint_array[2]) && (mac_id[4] == mac_d.next_MAC.uint_array[1]) && (mac_id[5] == mac_d.next_MAC.uint_array[0]))
							 {
								 // Verification is Successfull
								mac_d.available_MACs.uint_val--;
								mac_d.next_MAC.uint_val--;
								if((mac_d.available_MACs.uint_val <= 0) || mac_d.next_MAC.uint_val == LAST_MAC_ID)
								{
									fclose(mac_file);
									fdelete("MACfile.txt");
								}
								else
								{
									// Update the Macfile Next Address, Available Mac's, checksum fileds in MACfile.txt
									update_field( mac_file,&mac_d);
									fclose(mac_file);
								}
								//Copy MAC ID from EEPROM to Ethernet MAC Address Register.
								memcpy(own_hw_adr,mac_id,sizeof(mac_id));
							 }
							 else
							 {
								 // Verification failed
								 memset(mac_id,0,sizeof(mac_id));
								 EEPROM_Write (0, MAC_PAGE_NO, mac_id, MODE_8_BIT, sizeof(mac_id));
								 fclose(mac_file);
								 return 0x05; //MAC ERROR:Verification Failed
							 }
					 }
					 else
					 {
						   fclose( mac_file );
					     return 0x04; //MAC ERROR:Data in file is Invalid
					 }
				 }
				 else
				 {
					  fclose( mac_file );
					  return 0x04; //MAC ERROR:Data in file is Invalid
				 }
			}
			else 
			{
				return 0x02; // MAC ERROR:MACfile.txt file not found
			}
		}
		else
		{
			return 0x03; //MAC ERROR:USB Key not found
		}
 	}
	else
	{
		// Copy MAC ID from EEPROM to Ethernet MAC Address Register.
		memcpy(own_hw_adr,mac_id,sizeof(mac_id));
		return 0x01; //Already programmed MAC ID
	}
	fflush (stdout);
	return 0; // Success
}

/*****************************************************************************
* @note       Function name: parse_macfile(FILE *fp, MAC_FILE_DATA *mac_data)
* @returns    returns      : On Success returns 1,
														 On Failure 0
* @param      arg1         : file to parse, structure of mac file data.
* @author                  : Venkata Krishna Rao
* @date       date created : Feb 24, 2014
* @brief      Description  : Programs the unique MAC ID from USB MAC file to SCALE permenantly for the first time.
* @note       Notes        : None
*****************************************************************************/
char parse_macfile(FILE *fp, MAC_FILE_DATA *mac_data)
{
	int temp=0;
	U8 i=0,j=0;
	char buff[10];
	memset(buff,0,sizeof(buff));
	for(i=0;i<4;i++)
	{
		temp = fgetc( fp );
		while(( feof ( fp ) == 0 ) && (temp != '<'))
		{
			buff[0] = (char) temp;
			temp = fgetc( fp );
		}
		if( temp == '<')
		{
	     j = 0;
		 temp = fgetc( fp );
		 while(( feof ( fp ) == 0 ) && (temp != '>') && (isdigit(temp)) && j < 10 )
		 {
			  buff[j] = (char) temp;
			  temp = fgetc( fp );
			  j++;
		 }
		 if(temp == '>')
		 {
			 switch(i)
			 {
			   case 0: mac_data->oui.uint_val = atoi(buff);
				         break;
			   case 1: mac_data->next_MAC.uint_val = atoi(buff);
				         break;
			   case 2: mac_data->available_MACs.uint_val = atoi(buff);
				         break;
			   case 3: mac_data->checksum.uint_val = atoi(buff);
				         break;
			   default: break;
			 }
			 if((mac_data->available_MACs.uint_val > 16777215) || (mac_data->oui.uint_val > 16777215) || 
			    (mac_data->next_MAC.uint_val > 16777215))
			 {
					return 0;
			 }
			 memset(buff,0,sizeof(buff));
		 }
		 else
		 {
			 return 0;
		 }
		}
		else
		{
			return 0;
		}
	}
	return 1;
}
/*****************************************************************************
* @note       Function name: update_field( FILE *MAC_file1, MAC_FILE_DATA *mac_data)
* @returns    returns      : On Success returns 1,
														 On Failure 0
* @param      arg1,arg2    : file to update, structure of mac file data.
* @author                  : Venkata Krishna Rao
* @date       date created : Feb 27, 2014
* @brief      Description  : Updates USB MAC file after successfull programming of MAC ID.
* @note       Notes        : None
*****************************************************************************/
void update_field( FILE *MAC_file1, MAC_FILE_DATA *mac_data)
{
	char ch, buff[10],stream[100];
	int temp=0;
	unsigned char i=0,j=0,k=0;
	// Check sum Calculation
	mac_data->checksum.uint_val = SECRET_KEY + (mac_data->next_MAC.uint_val) + (M1*mac_data->oui.uint_val) +
		                          (M2*mac_data->available_MACs.uint_val);
	rewind(MAC_file1);
	temp = fgetc( MAC_file1 );
	while( feof ( MAC_file1 ) == 0 )
	{
			if(temp == '<')
				i++;
			if(((i == 2) || (i == 3) || (i == 4)) && (temp == '<'))
			{
				ch = (char) temp; 
				stream[k] = ch;
				k++;
				sprintf(buff,"%d",*((int *)mac_data + (i-1)));				
				for(j = 0;buff[j] != '\0'; j++)
				{
					stream[k] = buff[j];
					k++;
				}
				memset(buff,0,sizeof(buff));
				stream[k] = '>';
				k++;
				if(i == 4)
				{
				  stream[k] = '\0';
				  k++;
				  fclose(MAC_file1);
				  MAC_file1 = fopen("MACfile.txt", "w");
				  fputs(stream, MAC_file1);
				  break;
				}
				else
				{
					while(ch != '>')
				  {
					  temp = fgetc( MAC_file1 );
					  ch = (char) temp;
				  }
				}
			}
			else
			{
				ch = (char) temp;
				stream[k] = ch;
				k++;
			}
			temp = fgetc( MAC_file1 );
	}
}
#endif /*#ifdef EEPROM*/
/*****************************************************************************
* End of file
*****************************************************************************/
