/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    SERIAL.C
 *      Purpose: Serial Port Driver for Philips LPC178X
 *      Rev.:    V4.70
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2013 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <Net_Config.h>
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "lpc177x_8x_uart.h"
#include "lpc177x_8x_pinsel.h"
#include "Serial_LPC178x.h"

#define UART1_BUF_SIZE 2048
/* Local variables */
struct buf_st  {
/*  U8 in;
  U8 out;
  U8 buf [256];
	*/
  U32 in;
  U32 out;
	U8 buf [UART1_BUF_SIZE]; //Changed by DK on 27 Jan2015 
};

static struct buf_st rbuf __attribute__ ((section ("PPP_BUF_DATA")));
static struct buf_st tbuf __attribute__ ((section ("PPP_BUF_DATA")));
static BIT    tx_active;

static UART_CFG_Type Modem_UART;
unsigned int Baud_rate;
//void handler_UART1 (void) __irq;
void UART1_IRQHandler (void) __irq;
//void def_interrupt (void) __irq;

/*----------------------------------------------------------------------------
 *      Serial Driver Functions
 *----------------------------------------------------------------------------
 *  Required functions for Serial driver module:
 *   - void init_serial ()
 *   - int  com_get_char ()
 *   - BOOL com_putchar (U8 c)
 *   - BOOL com_tx_active ()
 *   - interrupt function(s)
 *---------------------------------------------------------------------------*/


/*--------------------------- init_serial -----------------------------------*/

void init_serial (void) {
  // UART FIFO configuration Struct variable
  UART_FIFO_CFG_Type UARTFIFOConfigStruct;
	
	/* Initialize the serial interface */
  rbuf.in   = 0;
  rbuf.out  = 0;
  tbuf.in   = 0;
  tbuf.out  = 0;
  tx_active = __FALSE;

  /* Enable RxD1 and TxD1 pins. */
	/*
   * Initialize UART1 pin connect
   * P0.15: U1_TXD
   * P0.16: U1_RXD
   * P0.17: U1_CTS
   * P3.20: U1_DSR
   * P3.19: U1_DCD
   */
  PINSEL_ConfigPin(0,15,1);
  PINSEL_ConfigPin(0,16,1);
  PINSEL_ConfigPin(0,17,1);
  PINSEL_ConfigPin(3,20,3);
  PINSEL_ConfigPin(3,19,3);

	/* 8-bits, no parity, 1 stop bit */
	Baud_rate = Modem_UART.Baud_rate=115200;
	Modem_UART.Databits=UART_DATABIT_8;
	Modem_UART.Parity=UART_PARITY_NONE;
	Modem_UART.Stopbits=UART_STOPBIT_1;
	UART_Init((LPC_UART_TypeDef *)LPC_UART1, &Modem_UART);

  /* Enable FIFO with 8-byte trigger level. */
	/* Initialize FIFOConfigStruct to default state:
   *         - FIFO_DMAMode = DISABLE
   *         - FIFO_Level = UART_FIFO_TRGLEV0
   *         - FIFO_ResetRxBuf = ENABLE
   *         - FIFO_ResetTxBuf = ENABLE
   *         - FIFO_State = ENABLE
   */
  UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);
  // Initialize FIFO for UART1 peripheral
  UART_FIFOConfig((LPC_UART_TypeDef *) LPC_UART1, &UARTFIFOConfigStruct);
  
	/* Enable RDA and THRE interrupts. */
  UART_IntConfig((LPC_UART_TypeDef *) LPC_UART1, UART_INTCFG_RBR, ENABLE);
	UART_IntConfig((LPC_UART_TypeDef *) LPC_UART1, UART_INTCFG_THRE, ENABLE);
	// Enable UART Transmit
  UART_TxCmd((LPC_UART_TypeDef *) LPC_UART1, ENABLE);

/* Enable UART1 interrupts. */
/* preemption = 1, sub-priority = 1 */
  NVIC_SetPriority(UART1_IRQn, 0x21);

  /* Enable Interrupt for UART1 channel */
  NVIC_EnableIRQ(UART1_IRQn);
	
  //VICDefVectAddr = (U32)def_interrupt;
	/* Enable UART1 interrupts. */
  //VICVectAddr14  = (U32)handler_UART1;
  //VICVectCntl14  = 0x27;
  //VICIntEnable   = (1 << 7);
}


/*--------------------------- com_putchar -----------------------------------*/

BOOL com_putchar (U8 c) {
  /* Write a byte to serial interface. */
  struct buf_st *p = &tbuf;

  /* Write a byte to serial interface */
  //if ((U8)(p->in + 1) == p->out) 
	if ((p->in + 1) == p->out) 
	{
    /* Serial transmit buffer is full. */
    return (__FALSE);
  }
  
  /*Disable USART transmit interrupt.*/
 // UART_IntConfig((LPC_UART_TypeDef *)LPC_UART1, UART_INTCFG_THRE, DISABLE);
	
	if (tx_active == __FALSE) 
	{
    /* Send directly to UART. */
		UART_SendByte((LPC_UART_TypeDef *)LPC_UART1,(U8)c);
    tx_active = __TRUE;
  }
  else 
	{
    /* Add data to transmit buffer. */
    p->buf [p->in++] = c;
		if(p->in >= UART1_BUF_SIZE)
		{
			p->in =0;
		}	
  }

	/*Enable USART transmit interrupt.*/
	//UART_IntConfig((LPC_UART_TypeDef *) LPC_UART1, UART_INTCFG_THRE, ENABLE);  

	return (__TRUE);
}


/*--------------------------- com_getchar -----------------------------------*/

int com_getchar (void) {
  /* Read a byte from serial interface */
  struct buf_st *p = &rbuf;
	int ret_val;

  if (p->in == p->out) 
	{
    /* Serial receive buffer is empty. */
    return (-1);
  }
  
	//return (p->buf[p->out++]);
	
	ret_val=p->buf[p->out++];
	
	if(p->out >= UART1_BUF_SIZE)
	{
		p->out =0;
	}
  return ret_val;	
}


/*--------------------------- com_tx_active ---------------------------------*/

BOOL com_tx_active (void) {
  /* Return status Transmitter active/not active.         */
  /* When transmit buffer is empty, 'tx_active' is FALSE. */
	if(UART_GetLineStatus((LPC_UART_TypeDef *) LPC_UART1) & UART_LSR_THRE)
	tx_active=_FALSE;
	else 
	tx_active=_TRUE;

  return (tx_active);
}


/*--------------------------- handler_UART1 ---------------------------------*/

//void handler_UART1 (void) __irq {
void UART1_IRQHandler (void) __irq {
  /* Serial Rx and Tx interrupt handler. */
  struct buf_st *p;
  volatile U8 dummy;
  U32 iir,rbr;

  /* Repeat while there is at least one interrupt source. */
 // while (((iir = U1IIR) & 0x01) == 0) {
	 iir = UART_GetIntId((LPC_UART_TypeDef *) LPC_UART1) & UART_IIR_INTID_MASK ;
	/* while (iir) {
		iir = ((LPC_UART_TypeDef *) LPC_UART1->IIR & 0x03CF) & UART_IIR_INTID_MASK ; 
   */ 
		 /*******************If USART has a received char ready:*****************/
  if ((iir == UART_IIR_INTID_RDA) || (iir == UART_IIR_INTID_CTI))
	{
		p = &rbuf;
    /* Read a character to clear RDA Interrupt. */
    rbr = UART_ReceiveByte((LPC_UART_TypeDef *) LPC_UART1);
    
		//if ((U8)(p->in + 1) != p->out) 
		if ((p->in + 1) != p->out) 
		{
      p->buf [p->in++] = (U8)rbr;
    }
		
		if(p->in >= UART1_BUF_SIZE)
		{
			p->in =0;
		}
	}	
	/*****************If USART transmitter is empty:********************/
  else if (iir == UART_IIR_INTID_THRE)
  {
		p = &tbuf;
    if (p->in != p->out) 
		{
			UART_SendByte((LPC_UART_TypeDef *) LPC_UART1, (p->buf [p->out++]));
    
			if(p->out >= UART1_BUF_SIZE)
			{
				p->out =0;
			}
		}
    else 
		{
    tx_active = __FALSE;
    }
	}	
		
//  }
  /* Acknowledge Interrupt. */
//  VICVectAddr = 0;
}


/*--------------------------- def_interrupt ---------------------------------*/
/*
void def_interrupt (void) __irq {
  // Default Interrupt Function: may be called when timer ISR is disabled 
  VICVectAddr = 0;
}
*/

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

