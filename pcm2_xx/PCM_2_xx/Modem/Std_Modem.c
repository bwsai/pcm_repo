/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    STD_MODEM.C
 *      Purpose: Standard Modem Driver
 *      Rev.:    V4.70
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2013 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "Net_Config.h"
#include "Global_ex.h"
#include "I2C_driver.h"
#include "rtc.h"
#include "RTOS_main.h"
#include "Main.h"

/* Net_Config.c */
extern SYS_CFG  sys_config;
#define TSEC    sys_config.TickRate

#define MODEM_IDLE      0
#define MODEM_ERROR     1
#define MODEM_READY     2
#define MODEM_LISTEN    3
#define MODEM_ONLINE    4
#define MODEM_DIAL      5
#define MODEM_HANGUP    6

/* Local variables */
static U8  mbuf[32];                /* Modem Command buffer                  */
static U8  mlen;                    /* Length of data in 'mbuf'              */
static U8  modem_st;                /* Modem state                           */
static U8  *dial_num;               /* Dial Target Number                    */
static U8  *reply;                  /* Wait for 'reply'                      */
static U16 delay;
static U8  step;
static U8  retries;
static BIT wait_for;
static BIT wait_conn;
static BIT wait_csq; //Added by DK on 6 Feb 2015 for connectivity status update
static BIT listen_mode;

BIT csq_req;
/*----------------------------------------------------------------------------
 *      Modem Driver Functions
 *----------------------------------------------------------------------------
 *  Required functions for Modem driver module:
 *   - void modem_init ()
 *   - void modem_dial (U8 *dialnum)
 *   - void modem_listen ()
 *   - void modem_hangup ()
 *   - BOOL modem_online ()
 *   - BOOL modem_process (U8 ch)
 *   - void modem_run ()
 *---------------------------------------------------------------------------*/

/* Local Function Prototypes */
static void send_cmd (U8 *str);
static void flush_buf (void);
static void proc_listen (void);
static void proc_dial (void);
static void proc_hangup (void);
static void set_mode (void);

extern char *HTTPStrCaseStr(const char *pSrc,U32 nSrcLength,const char *pFind);

/*--------------------------- modem_signal_strength ----------------------------------*/
void put_cell_modem_in_cmd_state(void)
{
	step = 0; 
	delay =0;
  csq_req =_TRUE; 
}

static void proc_signal_strength (void) {
  switch (step) {
    case 0:
			wait_conn = __FALSE;
      if (modem_st <= MODEM_READY) {
        /* Modem not connected, we are done here. */
        break;
      }
      delay = TSEC * 2;
      wait_for = __FALSE;
      step++;
			wait_csq =__FALSE;
      break;

    case 1:
      /* Send ESC sequence after delay */
		  send_cmd ((U8*)"+++");
      delay = TSEC * 2;
      step++;
		  wait_csq =__FALSE;
      break;
		
		case 2:
      reply = (U8*)"+CSQ:";
      retries = 3;
      step++;
		  wait_csq =_TRUE;
atcsq1:  
		  send_cmd ((U8*)"AT+CSQ\r");
      delay = TSEC * 3;
      wait_for = __TRUE;
      break;

    case 3:
      /* Wait for '+CSQ' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atcsq1;
        }
        csq_req =_FALSE;
				break;
      }
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Cell Modem is now in Command Mode\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			
      reply = (U8*)"CONNECT";
      retries = 3;
      step++;
			wait_csq =__FALSE;
 ato0: 
			send_cmd ((U8* )"ATO0\r\n");
      delay =  TSEC * 3;
      wait_for = __TRUE;
			break;

    case 4:
			/* Wait for 'CONNECT' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto ato0;
        }
				csq_req =_FALSE;
        break;
      }
			
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Cell Modem is now in Data Mode\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			csq_req =_FALSE;
      break;
  }
}

/*--------------------------- modem_init ------------------------------------*/

void modem_init (void) {
  /* Initializes the modem variables and control signals DTR & RTS. */
  mlen = 0;
  mem_set (mbuf, 0, sizeof(mbuf));
  wait_for  = 0;
  wait_conn = 0;
	wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
  modem_st = MODEM_IDLE;
}


/*--------------------------- modem_dial ------------------------------------*/

void modem_dial (U8 *dialnum) {
  /* Modem dial target number 'dialnum' */

  dial_num = dialnum;
  listen_mode = 0;
  step = 0;
  proc_dial ();
}


/*--------------------------- modem_listen ----------------------------------*/

void modem_listen (void) {
  /* This function puts Modem into Answering Mode. */
  step = 0;
  listen_mode = 1;
  proc_listen ();
}


/*--------------------------- modem_hangup ----------------------------------*/

void modem_hangup (void) {
  /* This function clears DTR to force the modem to hang up if   */
  /* it was on line and/or make the modem to go to command mode. */
  step = 0;
  proc_hangup ();
}


/*--------------------------- modem_online ----------------------------------*/

BOOL modem_online (void) {
  /* Checks if the modem is online. Return false when not. */
  if (modem_st == MODEM_ONLINE) {
    return (__TRUE);
  }
  return (__FALSE);
}


/*--------------------------- flush_buf -------------------------------------*/

static void flush_buf (void) {
  /* Flushes the receiving buffer. */
  mlen = 0;
  mem_set (mbuf, 0, sizeof(mbuf));
}


/*--------------------------- send_cmd --------------------------------------*/

static void send_cmd (U8 *str) {
  /* Send Command Mode strings to Modem */
  while (*str) {
    com_putchar (*str++);
  }
}


/*--------------------------- proc_dial -------------------------------------*/

static void proc_dial (void) {
  /* Modem Dial target number and connect to remote modem */
  U32 state;
//  U8 cmd_buf[80];
	
  switch (step) {
    case 0:
      /* Send Reset Command 'ATZ' */
      state = modem_st;
      modem_st = MODEM_DIAL;
      wait_conn = __FALSE;
		  wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      if(state == MODEM_READY) {
       //Added by DK on 13August 2015       
        step =4; 
        break; 
				
				//step = 5;  //Commented by DK on 13August 2015
        //goto atdt;  //Commented by DK on 13August 2015
      }
      reply = "OK\r";
      retries = 3;
      step++;
			
atz:  
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending ATZ command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			
			send_cmd ("ATZ\r");
      delay = TSEC * 5;
      wait_for = __TRUE;
      break;

    case 1:
      /* Wait for 'OK' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atz;
        }
				else
				{
					goto escape_seq;			//SJ-080415 for retries of ATZ command issue
					//NVIC_SystemReset();
				}
				modem_st = MODEM_ERROR;  
        break;                   
      }
      /* Send 'ATE0V1' command to modem */
      retries = 3;
      step++;
ate0: 
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending ATE0V1 command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			
			send_cmd ("ATE0V1\r");
      delay =  TSEC * 3; 
      wait_for = __TRUE;
      break;

		case 2:
      // Wait for 'OK' reply 
      if (wait_for == __TRUE) {
        if (--retries) {
          goto ate0;
        }
        modem_st = MODEM_ERROR;
        break;
      }
      // Send 'AT+IFC=1,1' command to modem 
			retries = 3;
      step++;
atifc:
			
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending AT+IFC=0,0 command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			
			send_cmd ("AT+IFC=0,0\r");
      delay =  TSEC * 3; 
      wait_for = __TRUE;
      break;		
	
			
	case 3:
      // Wait for 'OK' reply 
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atifc;
        }
        modem_st = MODEM_ERROR;
        break;
      }
      // Send 'AT+CREG=2' command to modem 
			retries = 3;
      step++;
atcreg:
			
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending AT+CREG=2 command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			
			send_cmd ("AT+CREG=2\r");
      delay =  TSEC * 3; 
      wait_for = __TRUE;
      break;	
			
		case 4:
      // Wait for 'OK' reply 
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atcreg;
        }
        modem_st = MODEM_ERROR;
        break;
      }
      // Send 'AT+CSQ' command to modem 
			wait_csq =_TRUE; //Added by DK on 6 Feb 2015 for connectivity status update
			reply = "+CSQ:";
			retries = 3;
      step++;
atcsq:
			
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending AT+CSQ command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			
			send_cmd ("AT+CSQ\r");
      delay =  TSEC * 3; 
      wait_for = __TRUE;
      break;				
			
    case 5:
      /* Wait for 'OK' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atcsq;
        }
        modem_st = MODEM_ERROR;
        break;
      }
     
		
			
			/* Dial Target number. */
atdt: 
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending ATDT%s command to modem\r\n",dial_num);
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			reply = "CONNECT";
      delay = TSEC * 32; //To be Changed delay from 30 to 32 by DK on 05 Oct 2015 PCM ver1.34 
      wait_for = __TRUE;
      wait_conn = __TRUE;
			wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      step++; 
      send_cmd ("ATDT");
      send_cmd (dial_num);
      send_cmd ("\r");
			break;

    case 6:
	
     
			/* Timeout on Waiting for 'CONNECT', hangup */
      wait_conn = __FALSE;
		  wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      retries = 3;
      reply = "OK\r";
      step++;
ath:  
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending ATH command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
		
			send_cmd ("ATH\r");
      delay = TSEC * 2; 
      wait_for = __TRUE;
      break;

    case 7:
      /* Cancel the call */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto ath;
        }
        modem_st = MODEM_ERROR;
        break;
      }
      /* Timeout, hangup modem. */
      modem_st = MODEM_READY;
      break;
			
			
escape_seq:
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Entering in Escape Sequence\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
      step =8; 
      wait_conn = __FALSE;
     /* if (modem_st <= MODEM_READY) {
        // Modem not connected, we are done here. 
        break;
      }*/
      //modem_st = MODEM_HANGUP;
      delay = TSEC * 3; 
      wait_for = __FALSE;
			wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      //step++;
      break;

    case 8:
      /* Send ESC sequence after delay */
    #ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending +++ command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif  
		  send_cmd ("+++");
      delay = TSEC * 2; 
		  wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      step++;
      break;

    case 9:
      /* Now hangup the modem, send 'ATH' */
      reply = "OK\r";
      retries = 3;
		  wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      step++;
ath1:
    #ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Sending ATH command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif  		
		send_cmd ("ATH\r");
      delay = TSEC * 3; 
      wait_for = __TRUE;
      break;

    case 10:
      /* Wait for 'OK' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto ath1;
        }
        /* No reply, reset the modem. */
        retries = 3;
        step++;
atz1:    
				#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "Resending ATZ command to modem\r\n");
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif  
				send_cmd ("ATZ\r");
        delay = TSEC * 5; 
        wait_for = __TRUE;
        break;
      }
      /* OK, disconnected, we are done. */
      set_mode (); 
      break;

    case 11:
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atz1;
        }
        /* Modem Reset failed */
        modem_st = MODEM_ERROR;
        break;
      }
      set_mode ();
      break;

  }
}


/*--------------------------- proc_listen -----------------------------------*/

static void proc_listen (void) {
  /* Handle Modem listening mode (waiting for incomming connection) */

  switch (step) {
    case 0:
      /* Send Reset Command 'ATZ' */
      modem_st = MODEM_LISTEN;
      wait_conn = __FALSE;
      reply = "OK\r";
      retries = 3;
      step++;
atz:  send_cmd ("ATZ\r");
      delay = TSEC * 5;
      wait_for = __TRUE;
      break;

    case 1:
      /* Wait for 'OK' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atz;
        }
        modem_st = MODEM_ERROR;
        break;
      }
      /* Send 'ATE0V1' command to modem */
      retries = 3;
      step++;
ate0: send_cmd ("ATE0V1\r");
      delay =  TSEC * 3;
      wait_for = __TRUE;
      break;

    case 2:
      /* Wait for 'OK' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto ate0;
        }
        modem_st = MODEM_ERROR;
        break;
      }
      /* Wait for incomming call. */
ring: reply = "RING\r";
      delay = 0;
      wait_for = __TRUE;
      step++;
      break;

    case 3:
      /* Waiting for the call */
      if (wait_for == __TRUE) {
        break;
      }
      send_cmd ("ATA\r");
      reply = "CONNECT";
      delay = TSEC * 30;
      wait_for = __TRUE;
      wait_conn = __TRUE;
      step++;
      break;

    case 4:
      /* Timeout on Waiting for 'CONNECT', hangup */
      wait_conn = __FALSE;
      retries = 3;
      reply = "OK\r";
      step++;
ath:  send_cmd ("ATH\r");
      delay = TSEC * 2;
      wait_for = __TRUE;
      break;

    case 5:
      if (wait_for == __TRUE) {
        if (--retries) {
          goto ath;
        }
        /* Failed, send reset to modem. */
        step = 0;
        goto atz;
      }
      step = 2;
      goto ring;
  }
}


/*--------------------------- set_mode --------------------------------------*/

static void set_mode (void) {
  /* Set Modem mode when 'NO CARRIER' received */
  if (listen_mode == __TRUE) {
    modem_st = MODEM_LISTEN;
    wait_for = __FALSE;
    step = 2;
  }
  else {
    modem_st = MODEM_READY;
  }
}


/*--------------------------- proc_hangup -----------------------------------*/

static void proc_hangup (void) {
  /* Hangup Modem if connected. */

  switch (step) {
    case 0:
      wait_conn = __FALSE;
      if (modem_st <= MODEM_READY) {
        /* Modem not connected, we are done here. */
        break;
      }
      modem_st = MODEM_HANGUP;
      delay = TSEC * 3; 
      wait_for = __FALSE;
			wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      step++;
      break;

    case 1:
      /* Send ESC sequence after delay */
      send_cmd ("+++");
      delay = TSEC * 2; 
		  wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      step++;
      break;

    case 2:
      /* Now hangup the modem, send 'ATH' */
      reply = "OK\r";
      retries = 3;
		  wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
      step++;
ath:  send_cmd ("ATH\r");
      delay = TSEC * 3; 
      wait_for = __TRUE;
      break;

    case 3:
      /* Wait for 'OK' reply */
      if (wait_for == __TRUE) {
        if (--retries) {
          goto ath;
        }
        /* No reply, reset the modem. */
        retries = 3;
        step++;
atz:    send_cmd ("ATZ\r");
        delay = TSEC * 5; 
        wait_for = __TRUE;
        break;
      }
      /* OK, disconnected, we are done. */
      set_mode ();
      break;

    case 4:
      if (wait_for == __TRUE) {
        if (--retries) {
          goto atz;
        }
        /* Modem Reset failed */
        modem_st = MODEM_ERROR;
        break;
      }
      set_mode ();
      break;
  }
}


/*--------------------------- modem_process ---------------------------------*/

BOOL modem_process (U8 ch) {
  /* Modem character process event handler. This function is called when */
  /* a new character has been received from the modem in command mode    */
	int i; //Added by DK on 6 Feb 2015 for connectivity status update
	
  if (modem_st == MODEM_IDLE) {
    mlen = 0;
    return (__FALSE);
  }
  if (mlen < sizeof(mbuf)) {
    mbuf[mlen++] = ch;
		}
  /* Modem driver is processing a command */
  if (wait_for) {
    /* 'modem_run()' is waiting for modem reply */
     
		if(wait_csq ==_TRUE) //Added by DK on 6 Feb 2015 for connectivity status update
		{
				if(ch != ',')
				{

				}	
				else if(0!=(HTTPStrCaseStr((const char *)mbuf,5,(const char *)"+CSQ:"))) //Found string "+CSQ:"
				{	
					i=0;
					while(1)
					{
						if((mbuf[6+i] == ',') || (mbuf[6+i] == '\r') || (mbuf[6+i] == '\n'))
						{
							mbuf[6+i] = '\0';
							break;
						}	
						i++;
						if(i > 2)break;
					}
					if((i > 0) && (i < 3))
					{
						i = atoi((const char*)&mbuf[6]);
						switch(i)
						{
							case 0 : //Found string "0" RSSI -115dBm or less
											cell_modem_strength = 1;
							break;
						
							case 1 : //Found string "10" RSSI -111dBm 
											cell_modem_strength = 2;
							break;
							
							case (31) : //Found string "31" RSSI -52dBm or Greater
											cell_modem_strength = 4;
							break;
							
							case (99) : //Found string "99" RSSI not known 
											cell_modem_strength = 5;
							break;
							
              default:  //Found string "2...30" RSSI -110dBm ...-54dBm
										if((i > 1) && (i < 31))		
									cell_modem_strength = 3;
							break;
						}	
					}	
					wait_for = 0;
					delay    = 0;
					wait_csq =0; //Added by DK on 6 Feb 2015 for connectivity status update
								 //Added by DK on 13August 2015 for sending signal strength response to debug port  
					//if(str_scomp(reply,"+CSQ:") == __TRUE)
					
					{
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "%s\r\n",mbuf);
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
					}	
				}	
		 }
		else if (str_scomp (mbuf,reply) == __TRUE) {
			
			 	wait_for = 0;
				delay    = 0;
			 if (wait_conn) {
        /* OK, we are online here. */
        wait_conn = 0;
        modem_st  = MODEM_ONLINE;
        /* Inform the parent process we are online now. */
        return (__TRUE);
      }
    }
		
  }//end of if (wait_for) {
  /* Watch the modem disconnect because we do not use CD line */
  if (mem_comp (mbuf,"NO CARRIER",10) == __TRUE) {
    set_mode ();
  }
  if (ch == '\r' || ch == '\n') {
    flush_buf ();
  }
  /* Modem not connected, return FALSE */
  return (__FALSE);
}


/*--------------------------- modem_run -------------------------------------*/

void modem_run (void) {
  /* This is a main thread for MODEM Control module. It is called on every */
  /* system timer timer tick to implement delays easy. By default this is  */
  /* every 100ms. The 'sytem tick' timeout is set in 'Net_Config.c'        */

  if (delay) {
    if (--delay) {
      return;
    }
  }

  switch (modem_st) {
    case MODEM_IDLE:
    case MODEM_ERROR:
      /* Modem idle or in error */
      break;

    case MODEM_ONLINE:
      /* Modem is online - connected */
		
			if(csq_req ==_TRUE) 
			{
				proc_signal_strength();			
			}	 
		  break;

    case MODEM_DIAL:
      /* Dial target number */
			proc_dial ();
      break;

    case MODEM_LISTEN:
      /* Activate answering mode */
      proc_listen ();
      break;

    case MODEM_HANGUP:
      /* Hangup and reset the modem */
		  /*#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "Hang up Cell Modem\r\n");
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif*/
      proc_hangup ();
      break;
  }
}


/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
