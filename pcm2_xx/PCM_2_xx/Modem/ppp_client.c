/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     : Beltway
*
* @file Filename       : ppp_client.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid 
*
* @date Created        : November Friday,21 2014  <Nov 21, 2014>
* @date Last Modified  : November Friday,21 2014  <Nov 21, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
/* ----------------------- Platform includes --------------------------------*/
#include <ctype.h>
#include <stdlib.h>
#include "math.h"
#include "Global_ex.h"
#include "RTOS_main.h"
#include "Modbus_Tcp_Master.h"
#include "mbport.h"
#include "Serial_LPC178x.h"
#include "Http_Client.h"
#include "Main.h"
#include "Global_ex.h"
#include "I2C_driver.h"
#include "DebugLog.h"
#include "../GUI/APPLICATION/Screen_global_ex.h"
#include "../gui/GUI.h"
#include "File_update.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/
/* Character variables section */
//Added by DK on 2 Jan 2015 
__align(4) char PPP_TX_Buffer[PPP_TX_BUFFER_LENGTH] __attribute__ ((section ("PPP_BUF_DATA")));
__align(4) char PPP_RX_Buffer[PPP_RX_BUFFER_LENGTH] __attribute__ ((section ("PPP_BUF_DATA")));

/*#ifdef DEBUG_PORT
__align(4) char ppp_temp_buff[100] __attribute__ ((section ("PPP_BUF_DATA")));
#endif
*/

/* unsigned integer variables section */
static U16 usPppClientRcvBufferLen;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
extern BIT csq_req;
/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
extern void put_cell_modem_in_cmd_state(void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
static U16 prvxTCPPortPPPCallback(U8 soc, U8 event, U8 *ptr, U16 par)
{
    U16 retval = 0;

    switch (event)
    {
    case TCP_EVT_CONREQ:
				/* Return 1 to accept connection, or 0 to reject connection */
			//	retval = 1;  //commented by DK on 17 March 2015 
        break;
    case TCP_EVT_ABORT:

      break;
    case TCP_EVT_CONNECT:
			retval = 1;
      break;
    case TCP_EVT_CLOSE:

      break;
    case TCP_EVT_ACK:
			os_evt_set(EVE_PPP_ACK,t_ppp_client);
		  retval = 1;
      break;
    case TCP_EVT_DATA:
		  //Check PPP response from PCS
			if( par < sizeof(PPP_RX_Buffer))
      {
			  memset(PPP_RX_Buffer,'\0',sizeof(PPP_RX_Buffer)); //added by DK on 2 Jan 2015 
				memcpy( PPP_RX_Buffer, ptr, par );
			  usPppClientRcvBufferLen = par;
				os_evt_set(EVE_PPP_RES,t_ppp_client);
			}
			break;
		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
       break;

  }
  return retval;

}

static void ppp_dns_cbfunc (U8 event, U8 *ip) {
  switch (event) {
    case DNS_EVT_SUCCESS:
				PCS_IpAdr[0] = ip[0];
				PCS_IpAdr[1] = ip[1];
				PCS_IpAdr[2] = ip[2];
				PCS_IpAdr[3] = ip[3];
		os_evt_set(EVE_DNS_IP_RESOLVED,t_ppp_client); 
		break;
    case DNS_EVT_NONAME:
  //    printf("Host name does not exist.\n");
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "Host name does not exist\r\n");
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
		os_evt_set(EVE_DNS_IP_NONAME,t_ppp_client); 
      break;
    case DNS_EVT_TIMEOUT:
    //  printf("DNS Resolver Timeout expired, Host IP not resolved.\n");
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "DNS Resolver Timeout expired, Host IP not resolved.\n");
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
		  os_evt_set(EVE_DNS_IP_TIMEOUT,t_ppp_client); 
      break;
    case DNS_EVT_ERROR:
   //   printf("DNS Resolver Error, check the host name, labels, etc.\n");
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "DNS Resolver Error, check the host name, labels, etc.\n");
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
			os_evt_set(EVE_DNS_IP_ERROR,t_ppp_client); 
      break;
  }
}


/*****************************************************************************
* @note       Function name  : __task void ppp_client (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 21 November 2014
* @brief      Description    : The HTTP Measurement server(PCS) is a web application built to record measurement data 
														 : from PCM. PCM implements ppp client & connects to PCS to push the data records.
*                            : 
*                            : 
* @note       Notes          : None
*****************************************************************************/
__task void ppp_client (void)
{
	unsigned char pcs_connect=0,pcs_sock_state=0,power_on,dns_ip_resolved=0;
	int             pcs_sock_num=0,i,j,pcs_no_of_retry=0;
	OS_RESULT result;
	char *ptr,*http_ptr;
	char temp_buf[30];
	unsigned char debug_bug[500];
	U16 evt_flags;
  	
	for(i=0; i< MAX_NO_OF_INTEGRATORS; i++)
	{	
		Modbus_Tcp_Slaves[i].prev_record = 0;
		Modbus_Tcp_Slaves[i].cur_record = 0;
	}
	power_on = 1; //first get request
	usPppClientRcvBufferLen=0;
  dns_ip_resolved =0;  //Added by DK on 17 March 2015 
	pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 17 March 2015 
	
	//Added by DK on 17 March 2015 
	if(dhcp_enable == 1)
	{
		do 
		{	
			os_itv_set(PPP_CLIENT_INTERVAL);
			os_itv_wait();
		}while(dhcp_tout);
	}	
	
	while(1)
	{
		
	  os_itv_set(PPP_CLIENT_INTERVAL);
		os_itv_wait();	
		LPC_RTC->GPREG4 |= (0x01<<5); 	
		
		
		if(pcs_connect == 0) 
		{
			if(ppp_is_up())
			{
				
				//Added by DK onlly for debugging on 19 Jan 2015 
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "\r\nPPP link is up\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);	
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
				
			#ifdef USB_LOGGING
			
				Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif
				
				if(dns_ip_resolved == 0)
				{	
					os_evt_clr(EVE_DNS_IP_RESOLVED | EVE_DNS_IP_NONAME | EVE_DNS_IP_TIMEOUT | EVE_DNS_IP_ERROR,t_ppp_client); 
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "\r\n Starts the DNS client over PPP to resolve Host Name : %s\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					&Http_Host_Name[7],Current_time.RTC_Mon,Current_time.RTC_Mday,\
					                     Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
					                     Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif
				Flags_struct.Plant_connect_record_flag = PCS_SERVER_IP_RESOLVED_OK_FLAG;
				GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] = 0;					
				strcpy(Debug_Buf, "SERVER IP RESOLUTION IN PROCESS......");			//by megha on 31/1/2017
				GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (0*16)));						
					get_host_by_name ((U8*)&Http_Host_Name[7],ppp_dns_cbfunc);
					result = os_evt_wait_or(EVE_DNS_IP_RESOLVED | EVE_DNS_IP_NONAME | EVE_DNS_IP_TIMEOUT | EVE_DNS_IP_ERROR,7000); //By DK on 11 March 2015 changed timing from 7000 to 5000
					if(result != OS_R_EVT)  
					{	
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						//sprintf(Debug_Buf, "didn't resolve PCS IP over ppp!\r\n"); //commented by DK on 29 Sep2015 
						sprintf(Debug_Buf, "didn't resolve PCS IP over ppp!	Month:%02d Date:%02d Year:%04d \
						                                                    Hour:%02d Minute:%02d Second:%02d\r\n",\
                      				 Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						                   Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
					Flags_struct.Plant_connect_record_flag &= ~PCS_SERVER_IP_RESOLVED_OK_FLAG;
					GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] = 0;
					
					Flags_struct.Plant_connect_record_flag = SERVER_IP_RESOLUTION_FAIL_FG;					
					GUI_data_nav.Error_wrn_msg_flag[SERVER_IP_RESOLVE_FAIL_MSG_TIME] = 0;

					strcpy(Debug_Buf, "SERVER IP RESOLUTION FAILED");
					GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0,(DISP_LINE5_POS_Y0 + (1*16)));								
						
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif
						//Added by DK on 05 Oct 2015 
						if(pcs_no_of_retry++ > 3)
						{
							ppp_close();
							pcs_no_of_retry =0; 
						}
					}
					else 
					{
						evt_flags = os_evt_get();
						switch (evt_flags)
						{
							case EVE_DNS_IP_RESOLVED:
								os_evt_clr(EVE_DNS_IP_RESOLVED,t_ppp_client); //Added by DK on 17 March 2015 				
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								//sprintf(Debug_Buf, "Resolved PCS_IP is %d:%d:%d:%d\r\n",PCS_IpAdr[0],PCS_IpAdr[1],PCS_IpAdr[2],PCS_IpAdr[3]); //commented by DK on 29 Sep2015 
							  sprintf(Debug_Buf, "Resolved PCS_IP is %d:%d:%d:%d   Month:%02d Date:%02d Year:%04d \
						                        Hour:%02d Minute:%02d Second:%02d\r\n",\
							                      PCS_IpAdr[0],PCS_IpAdr[1],PCS_IpAdr[2],PCS_IpAdr[3],\
							                      Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
							
							
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif
								dns_ip_resolved = 1;  //Added by DK on 11 March 2015 
							  pcs_no_of_retry =0; //Added by DK on 05 Oct 2015 
								
							Flags_struct.Plant_connect_record_flag &= ~SERVER_IP_RESOLUTION_FAIL_FG;
							GUI_data_nav.Error_wrn_msg_flag[SERVER_IP_RESOLVE_FAIL_MSG_TIME] = 0;
						
							Flags_struct.Plant_connect_record_flag = PCS_SERVER_IP_RESOLVED_OK_FLAG;
							GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] = 0;

							strcpy(Debug_Buf, "SERVER IP RESOLVED OK");
							GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0,(DISP_LINE5_POS_Y0 + (1*16)));									
							 break;
						 
						 case EVE_DNS_IP_NONAME:
						    os_evt_clr(EVE_DNS_IP_NONAME,t_ppp_client); //Added by DK on 17 March 2015 				
						    break;
						
						case EVE_DNS_IP_TIMEOUT:
								os_mut_wait (&usb_log_mutex, 0xffff);							
								sprintf(&system_log.log_buf[0],"Server IP resolution time out...                 ");
								log_file_write(system_log1);	
								os_mut_release (&usb_log_mutex);								
								os_evt_clr(EVE_DNS_IP_TIMEOUT,t_ppp_client); //Added by DK on 17 March 2015
                //Added by DK on 05 Oct 2015 
								if(pcs_no_of_retry++ > 3)
								{
									ppp_close();
									pcs_no_of_retry =0; 
								}						
								break;
						
						case EVE_DNS_IP_ERROR:
								os_evt_clr(EVE_DNS_IP_ERROR,t_ppp_client); //Added by DK on 17 March 2015 				
								break;
						
						default :
										 break;
						}
					}
				}//end of if(dns_ip_resolved == 0)	
				
				if(dns_ip_resolved == 1) //Added by DK on 11 March 2015 	
				{	
					Flags_struct.Plant_connect_record_flag = SOCKET_CONNECTION_PROCESS_FLAG;		//by megha on 20/1/2017
					GUI_data_nav.Error_wrn_msg_flag[SOCKET_CONNECTION_PROCESS_MSG_TIME] = 0;		
					strcpy(Debug_Buf, "SOCKET CONNECTION IN PROCESS......");
					GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (2*16)));	
					
					pcs_sock_num = tcp_get_socket((TCP_TYPE_CLIENT | TCP_TYPE_KEEP_ALIVE),0,30,prvxTCPPortPPPCallback);
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					//sprintf(Debug_Buf, "Connecting to PCS server over PPP Socket number%d!\r\n",pcs_sock_num); //commented by DK on 29 Sep2015 
					sprintf(Debug_Buf, "\r\n Connecting to PCS server over PPP Socket number%d! \
					                         Month:%02d Date:%02d Year:%04d,Hour:%02d Minute:%02d Second:%02d\r\n",\
					                         pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
					                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			
					if(!tcp_connect( pcs_sock_num,PCS_IpAdr,http_port_number,PPP_CLIENT_LOCAL_PORT))//replace client port from http_port_number					
					{
					Flags_struct.Plant_connect_record_flag = PCS_SOCKET_CONNECTION_FAIL_FLAG;	//BY MEGHA ON 11/1/2017
					GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_FAIL_MSG_TIME] = 0;
						strcpy(Debug_Buf, "TCP SOCKET STATUS : NOT CONNECTED");
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (3*16)));
						
						tcp_abort(pcs_sock_num); 
						tcp_close( pcs_sock_num);
						tcp_release_socket(pcs_sock_num);
						ppp_close();
						//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						//sprintf(Debug_Buf, "Could not connect to PCS server!\r\n"); //commented by DK on 29 Sep2015 
						sprintf(Debug_Buf, "Could not connect to PCS server! Month:%02d Date:%02d Year:%04d,\
						                    Hour:%02d Minute:%02d Second:%02d \r\n" , \
						                    Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
					                      Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

					}
					else
					{
						//Commented by DK on 13August 2015 
// 						os_dly_wait(5000); 
// 						pcs_sock_state = tcp_get_state (pcs_sock_num); 
						//Added by DK on 13August 2015 
						for(i=0; i < 10; i++)
						{
							os_dly_wait(500); 
							pcs_sock_state = tcp_get_state (pcs_sock_num); 
             	if(pcs_sock_state == TCP_STATE_CONNECT)
							{
								Flags_struct.Plant_connect_record_flag = PCS_SOCKET_CONNECTION_OK_FLAG;		//BY MEGHA ON 11/1/2017
								GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_MSG_TIME] = 0;	
								strcpy(Debug_Buf, "TCP SOCKET STATUS : CONNECTED        ");				
								GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (3*16)));										
								os_dly_wait(500); //Added by DK on 06 Oct2015 PCM ver1.34  
								break;
							}	
							//Added by DK on 05 Oct2015 
							if(!ppp_is_up())
							{
								pcs_sock_state = TCP_STATE_CLOSED; //Added by DK on 06 Oct2015 PCM ver1.34  
								break;
							}	
						}
						
						if(pcs_sock_state == TCP_STATE_CONNECT)  //PCM is connected to PCS 
						{	
								pcs_connect = 1;
							  //Added by DK on 17 March 2015 for PCS connectivity status update
								if(pcs_no_of_retry < 4)
								{
									pcs_connectivity_status = CONNECTED_IN_THREE_RETRIES;
								}	
								else if(pcs_no_of_retry < 6)
								{
									pcs_connectivity_status = CONNECTED_IN_3TO5_RETRIES;
								}
								else 
								{
									pcs_connectivity_status = CONNECTED_IN_5TO10_RETRIES;
								}
						    pcs_no_of_retry =0; 	
								
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								
								//Modified by DK on 13August 2015 
								sprintf(Debug_Buf, "ppp client(pcm) connected to pcs server(pcs)-socket%d!\
								Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
								#ifdef USB_LOGGING
								
										Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
								#endif			

				   	}
						else
						{
						Flags_struct.Plant_connect_record_flag = PCS_SOCKET_STATUS_NC_FLAG;
						GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_STATUS_NC_MSG_TIME] = 0;		
						strcpy(Debug_Buf, "TCP SOCKET STATUS : NOT CONNECTED");
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (3*16)));	
							
							tcp_abort(pcs_sock_num); 
							tcp_close(pcs_sock_num);  
							tcp_release_socket(pcs_sock_num);
							ppp_close();
							//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
							pcs_no_of_retry++;
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							
							//Modified by DK on 13August 2015 
							sprintf(Debug_Buf, "Disconnected from PCS server!\
							Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
							Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
							
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
							
							
							
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

						}  //end of else of if(pcs_sock_state == TCP_STATE_CONNECT)  //PCM is connected to PCS 
					}	//end of else of if(!tcp_connect( pcs_sock_num,ppp_server_ip,http_port_number,http_port_number))					
				}	//end of if(dns_ip_resolved == 1) //Added by DK on 11 March 2015 	
			} //end of if(ppp_is_up())
			else
			{
					//connect to PCS server by dialing remote PCS(PPP Server). 
					if(!modem_online())
					{
								strcpy(Debug_Buf, "DIALING TO CONNECT TO PPP SERVER!");				
								GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (4*16)));							
						//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "\r\nDialing to connect to PPP server!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);	
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
						
						
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

						ppp_connect(ppp_dial_number,"admin","admin"); //Added by DK on 29 January 2015 
						os_dly_wait(500);  //changed from 6000 to 1000  
		
					}	
					else //Added by DK on 19 May2016 to resolve issue of PCM lockup if cell mdoem is connected
					{
								strcpy(Debug_Buf, "PPP LINK IS DOWN..");				
								GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (4*16)));						
            #ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "\r\nPPP link is down, but MODEM is online!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
                      					Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);	
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
						
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

						ppp_close();
						modem_init();
//						NVIC_SystemReset();
						while(1);
          }	
			} //end of else of if(ppp_is_up())
		} //end of if(pcs_connect == 0) 
				
		
		//If pcs is connected to PCM & PCM is powered on, then load the scales configurations using GET request
    if ((pcs_connect == 1) && (power_on == 1))
		{
			pcs_sock_state = tcp_get_state (pcs_sock_num);  
			if(pcs_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS 
			{
						os_mut_wait (&usb_log_mutex, 0xffff);							
				
						sprintf(&system_log.log_buf[0],"Modem is disconnected from server");
						log_file_write(system_log1);	
						os_mut_release (&usb_log_mutex);				
				pcs_connect=0;
				tcp_abort(pcs_sock_num); //Added by DK on 2 Jan 2015 
				tcp_close(pcs_sock_num);  
				tcp_release_socket(pcs_sock_num);
				ppp_close();
				//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
				pcs_no_of_retry=0;
				pcs_connectivity_status = NOT_CONNECTED; 
// 				for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 				{	
// 					Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 					Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 				}					
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				//Modified by DK on 13August 2015 
				sprintf(Debug_Buf, "\r\n Get Request-Disconnected from PCS over PPP.Closing socket%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
				
				
				#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

			}

			else  //Prepare packet as per HTTP message(Java Script Object Notation)
			{
				memset(PPP_TX_Buffer,'\0',sizeof(PPP_TX_Buffer)); 
				os_evt_clr(EVE_PPP_RES,t_ppp_client);				 //Added by DK on 17 March 2015 	
				strcpy(Debug_Buf, "REQUESTING SCALE CONFIGURATION FROM SERVER....");				
				GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (5*16)));					
				if(Http_Get_Request(pcs_sock_num, &PPP_TX_Buffer[0]))
				{
					Flags_struct.Plant_connect_record_flag = PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG;
					GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;					
				//wait for response from PCS on scales configurations
					result = os_evt_wait_or(EVE_PPP_RES,10000);
					os_evt_clr(EVE_PPP_RES,t_ppp_client);				
					if(result == OS_R_EVT)
					{	
						if(usPppClientRcvBufferLen > 0)
						{
							if(Http_Get_Read_Response(PPP_RX_Buffer, usPppClientRcvBufferLen))
							{
							Flags_struct.Plant_connect_record_flag = PCM_SCALE_CONFIGURATION_RECEIVED_OK_FLAG;
							GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME] = 0;							
							strcpy(Debug_Buf, "SCALE CONFIGURATION RECEIVED OK");
							GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (6*16)));								
								power_on =0;	
								usPppClientRcvBufferLen=0;
								os_evt_set(EVE_GET_RES,t_modbus_tcp_master);
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								//sprintf(Debug_Buf, "Recd Get Response from PCS over PPP!\r\n"); //commented by DK on 29 Sep2015 
								sprintf(Debug_Buf, "Recd Get Response from PCS over PPP! Month:%02d Date:%02d Year:%04d \
								                    Hour:%02d Minute:%02d Second:%02d\r\n",\
																    Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
								                    Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
								
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

							}
							else
							{
								Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG;
								GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;									
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								sprintf(Debug_Buf, "HTTP Get Response from PCS!\r\n");
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								strncpy(Debug_Buf,PPP_RX_Buffer,sizeof(Debug_Buf)); 
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								if(usPppClientRcvBufferLen > sizeof(Debug_Buf))
								{
									usPppClientRcvBufferLen -= sizeof(Debug_Buf);
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									strncpy(Debug_Buf,&PPP_RX_Buffer[sizeof(Debug_Buf)],sizeof(Debug_Buf)-4);
									strcat(Debug_Buf,"\r\n");
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								}	
								os_mut_release (&debug_uart_mutex);
								#endif
								
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

							}	
						} //end of if(usPppClientRcvBufferLen > 0)
					}//end of if(result == OS_R_EVT)
					else 
					{
					Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG;
					GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;	
						os_mut_wait (&usb_log_mutex, 0xffff);							
						sprintf(&system_log.log_buf[0],"Modem is disconnected from server due to no response of 'GET configuration' post" );
						log_file_write(system_log1);					
						os_mut_release (&usb_log_mutex);						
						pcs_connect=0;
						tcp_abort(pcs_sock_num); //Added by DK on 2 Jan 2015 
						tcp_close(pcs_sock_num);  
						tcp_release_socket(pcs_sock_num);
						ppp_close();
						//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
						pcs_no_of_retry=0;
						pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 
// 				for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 				{	
// 					Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 					Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 				}								
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						//sprintf(Debug_Buf, "Get Rsp Timeout from PCS over PPP.Closing socket%d!\r\n",pcs_sock_num); //commented by DK on 29 Sep2015 
						sprintf(Debug_Buf, "Get Rsp Timeout from PCS over PPP.Closing socket%d! \
						                    Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
						        						pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
								                Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 Sep2015 
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
						
						
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

					}	
				}		// end of else if(Http_Get_Request(pcs_sock_num))	
				else
				{
					Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG;
					GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;
				}				
			} //end of else of if(pcs_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS 
		}//end of  if ((pcs_connect == 1) && (power_on == 1))		
		
		if ((pcs_connect == 1) && (power_on == 0)) //PCM is connected to PCS. Send records to PCS 
		{
//			os_mut_wait (&tcp_mutex, 0xffff);	 //Added by DK on 5 Feb 2015  //was Commented by DK on 29 sep 2015 PCM firmware V1.33
			
			for(i=0; i < Total_Scales; i++)
			{	
				if(i > (MAX_NO_OF_INTEGRATORS - 1))	//Changed by DK on 23 Feb 2015 	
				{
					break;
        }					
				
				pcs_sock_state = tcp_get_state (pcs_sock_num);  
				//if(pcs_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS Commented by DK on 18 Sep2015 
				if(pcs_sock_state == TCP_STATE_CLOSED)  //PCM is disconnected from PCS  Added by DK on 18 Sep2015 
				{
					//pcs_no_of_retry++; //Commented by DK on 18 Sep2015  
					//if(pcs_no_of_retry > 3) //Commented by DK on 18 Sep2015 
					{
						pcs_connect=0;
						tcp_abort(pcs_sock_num); //Added by DK on 2 Jan 2015 
						tcp_close(pcs_sock_num);  
						tcp_release_socket(pcs_sock_num);
						ppp_close();
						//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
						pcs_no_of_retry=0;
						pcs_connectivity_status = NOT_CONNECTED; 
// 				for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 				{	
// 					Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 					Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 				}								
						//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						//Modified by DK on 13August 2015 
						sprintf(Debug_Buf, "\r\n Post Record Req-Disconnected from PCS.Closing socket%d!\
						Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
						
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

						//os_dly_wait(1000);  //Added by DK on 05 Oct 2015 
						break;
					}	
				}//end of if(pcs_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS 
				else if(pcs_sock_state == TCP_STATE_CONNECT)  //PCM is connected to PCS  Added by DK on 18 Sep2015 
				{	
					if((Modbus_Tcp_Slaves[i].Connect == 1) && (Modbus_Tcp_Slaves[i].cur_record != Modbus_Tcp_Slaves[i].prev_record))//if records updated & connection with Scale is ok.
					{	
							os_mut_wait (&usb_log_mutex, 0xffff);							
						
						sprintf(&system_log.log_buf[0],"Posting records of scale %d ",Modbus_Tcp_Slaves[i].Scale_ID);
						log_file_write(system_log1);	
							os_mut_release (&usb_log_mutex);
						
						os_evt_clr(EVE_PPP_RES,t_ppp_client);			
						//Preapre packet as per HTTP message(Java Script Object Notation)
						//if(Http_Post_Request(pcs_sock_num, i))  //Commented by DK on 2 Jan 2015 
						memset(PPP_TX_Buffer,'\0',sizeof(PPP_TX_Buffer)); //added by DK on 16 Dec 2014 
						if(Http_Post_Request(pcs_sock_num, i,&PPP_TX_Buffer[0]))  //Added by DK on 2 Jan 2015 
						{
							//Check repsonse from PCS If it is OK then only go for next record.
							result = os_evt_wait_or(EVE_PPP_RES,8000);
							if(result == OS_R_EVT)
							{	
								os_evt_clr(EVE_PPP_RES,t_ppp_client);	//Added by DK on 17 March 2015 			
								if(usPppClientRcvBufferLen > 0)
								{
									os_mut_wait (&usb_log_mutex, 0xffff);							
									
								sprintf(&system_log.log_buf[0],"Posting records of scale %d is succesful",Modbus_Tcp_Slaves[i].Scale_ID);
								log_file_write(system_log1);	
								os_mut_release (&usb_log_mutex);
									
									Flags_struct.Plant_connect_record_flag = PCM_SCALE_DATA_UPDATE_OK_FLAG;
									GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME] = 0;	
									disp_rx_scale_id = i+1;
									strcpy(Debug_Buf, "SCALE DATA UPDATED ON SERVER ");				
									GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (12*16)));
									
									ptr = &PPP_RX_Buffer[0];  
									if(0!= (http_ptr = HTTPStrCaseStr(ptr,11,"FileRequest")))
									{
										if(0!= (ptr = HTTPStrCaseStr(http_ptr,4,"Date")))
										{
											/*#ifdef DEBUG_PORT
											os_mut_wait (&debug_uart_mutex, 0xffff);		
											memset(Debug_Buf,'\0',sizeof(Debug_Buf));
											sprintf(Debug_Buf, "File Post request for scale id = %d received from PCS over ppp\r\n", Modbus_Tcp_Slaves[i].Scale_ID);	
											Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
											os_mut_release (&debug_uart_mutex);
											#endif
											*/
											
											Modbus_Tcp_Slaves[i].file_scale_id=0;
											ptr+=7;
											memset(temp_buf,'\0',sizeof(temp_buf));
											j=0;
											while(1)
											{
												if((*ptr == '\r') || (*ptr == '\n') || (*ptr == '"') || (*ptr == ','))
												{
													break;
												}	
												if(*ptr !='/')
												temp_buf[j]= *ptr;
												else 
												{	
													Modbus_Tcp_Slaves[i].file_month = atoi(temp_buf);		
													j++;
													ptr++;
													break;
												}
												j++;
												ptr++;
												if(j > 2)break;
											}
											j=0;
											memset(temp_buf,'\0',sizeof(temp_buf));
											while(1)
											{
												if((*ptr == '\r') || (*ptr == '\n') || (*ptr == '"') || (*ptr == ','))
												{
													break;
												}	
												if(*ptr !='/')
												temp_buf[j]= *ptr;
												else 
												{	
													Modbus_Tcp_Slaves[i].file_day = atoi(temp_buf);		
													j++;
													ptr++;
													break;
												}
												j++;
												ptr++;
												if(j > 2)break;
											}
											
											j=0;
											memset(temp_buf,'\0',sizeof(temp_buf));
											while(1)
											{
												if((*ptr == '\r') || (*ptr == '\n') || (*ptr == '"') || (*ptr == ','))
												{
													break;
												}	
												temp_buf[j]= *ptr;
												j++;
												ptr++;
												if(j > 4)break;
											}
											if(j == 4 )
											Modbus_Tcp_Slaves[i].file_year = atoi(temp_buf);			
										}	//end of if(0!= (http_ptr = HTTPStrCaseStr(http_ptr,7,"Date")))
										
										if(0!= (http_ptr = HTTPStrCaseStr(ptr,7,"ScaleId")))
										{
											http_ptr+=9;
											j=0;
											memset(temp_buf,'\0',sizeof(temp_buf));
											while(1)
											{
												if((*http_ptr == '\r') || (*http_ptr == '\n') || (*http_ptr == '"') || (*http_ptr == ',') || (*http_ptr == '}'))
												{
													break;
												}	
												temp_buf[j]= *http_ptr;
												j++;
												http_ptr++;
												if(j > 5)break;
											}
											
											if(j > 0 )
											Modbus_Tcp_Slaves[i].file_scale_id = atoi(temp_buf);	
											
											if(Modbus_Tcp_Slaves[i].file_scale_id == Modbus_Tcp_Slaves[i].Scale_ID)
											{
												//Added by DK on 5 Feb2015 
												Modbus_Tcp_Slaves[i].ftp_file_not_available=0;
												Modbus_Tcp_Slaves[i].get_file=1;
												#ifdef DEBUG_PORT
												os_mut_wait (&debug_uart_mutex, 0xffff);		
												memset(Debug_Buf,'\0',sizeof(Debug_Buf));
												sprintf(Debug_Buf, "\r\n File Post request received for scale Id %d,year%04d,month%02d,date%02d over ppp\r\n",\
												Modbus_Tcp_Slaves[i].file_scale_id,Modbus_Tcp_Slaves[i].file_year,\
												Modbus_Tcp_Slaves[i].file_month,Modbus_Tcp_Slaves[i].file_day);		
												Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
												os_mut_release (&debug_uart_mutex);
												#endif
														os_mut_wait (&usb_log_mutex, 0xffff);												
														sprintf(&system_log.log_buf[0],"FTP request to SCALE %d of %2d-%2d-%4d from server",Modbus_Tcp_Slaves[j].Scale_ID,
														Modbus_Tcp_Slaves[i].file_month,Modbus_Tcp_Slaves[i].file_day,Modbus_Tcp_Slaves[i].file_year);
														log_file_write(system_log1);		
														os_mut_release (&usb_log_mutex);										
												
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

											}	
										} //end of if(0!= (http_ptr = HTTPStrCaseStr(ptr,7,"ScaleId")))
									}	//end of if(0!= (http_ptr = HTTPStrCaseStr(ptr,11,"FileRequest")))

									//Added by DK on 12 March 2015 for Connectivity Status Request from PCS 
									ptr = &PPP_RX_Buffer[0];  
									memset(temp_buf,'\0',sizeof(temp_buf));
									temp_buf[0]='"';
									strcpy(&temp_buf[1],"NeedsStatus");
									temp_buf[12]='"';
									strcpy(&temp_buf[13],":true");
									if(0!= (http_ptr = HTTPStrCaseStr(ptr,strlen(temp_buf),temp_buf)))
									{
										//to be Added by DK on 19 March for quering cell modem signal strength by switching from data mode to command mode
                    put_cell_modem_in_cmd_state();
										j=0;
										do
										{
											if(csq_req == _FALSE)
											break;	
                      os_dly_wait(100);
			                if(j++ > 20)
											break;		
										}while(1);
										
										report_connectivity_status = 1; 
									}	//end of if(0!= (http_ptr = HTTPStrCaseStr(ptr,strlen(temp_buf),temp_buf)))
									
									usPppClientRcvBufferLen=0;	
									Modbus_Tcp_Slaves[i].prev_record = Modbus_Tcp_Slaves[i].cur_record;
									pcs_no_of_retry=0;
									
									//Added by DK on 30 sep 2015 
									/*
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									sprintf(Debug_Buf, "\r\n Successfully Sent Record %d of Scale ID %d to PCS over PPP!\
									Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
									Scale_Record_Mb_Reg[i].Record_Hash.uint_val, Modbus_Tcp_Slaves[i].Scale_ID,\
									Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
									Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif*/
									
								}
							}
							else
							{
								pcs_no_of_retry++;  
								if(pcs_no_of_retry > 3) 
								{
									Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_DATA_UPDATE_OK_FLAG;
									GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME] = 0;										
									pcs_connect=0;
									tcp_abort(pcs_sock_num);   //Added by DK on 2 Jan 2015 
									tcp_close(pcs_sock_num);  
									tcp_release_socket(pcs_sock_num);
									ppp_close();
									//modem_init(); //was Added on 25 sep 2015 PCM firmware Ver1.33 
									pcs_no_of_retry=0;
									pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 17 March 2015 
// 				for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 				{	
// 					Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 					Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 				}											
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									//Modified by DK on 13August 2015 
									sprintf(Debug_Buf, "\r\n Post Rsp Timeout from PCS over PPP.Closing socket%d!\
									Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",pcs_sock_num,\
									Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
				                        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif
								os_mut_wait (&usb_log_mutex, 0xffff);							
								sprintf(&system_log.log_buf[0],"Posting records of scale %d is failed,closing socket.",Modbus_Tcp_Slaves[i].Scale_ID);
								log_file_write(system_log1);				
								os_mut_release (&usb_log_mutex);									
									
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

									break; //Added by DK on 2 Jan 2015 
								}	
							}	
						}	//end of if(Http_Post_Request(pcs_sock_num, i))
						else
						{
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								/*sprintf(Debug_Buf, "Not able to send Post Request of scale ID %d to PCS over PPP!\r\n",\
							          Modbus_Tcp_Slaves[i].Scale_ID);*/ //Commented by DK on 29 sep 2015 
						  	sprintf(Debug_Buf, "\r\n Not able to send Post Request of scale ID %d to PCS over PPP! \
							                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
							          Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
							          Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015 
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
							
							
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

						}
					}//end of checking records update & connection with Scale
					
					if((Modbus_Tcp_Slaves[i].Connect == 1) && (Modbus_Tcp_Slaves[i].ftp_file_recd == 1))
					{
						//Preapre packet as per HTTP message(Java Script Object Notation)
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						//sprintf(Debug_Buf, "Posting a File of Scale Id %d to PCS over PPP!\r\n",Modbus_Tcp_Slaves[i].Scale_ID); //Commented by DK on 29 sep 2015
						sprintf(Debug_Buf, "\r\n Posting a File of Scale Id %d to PCS over PPP! \
						                    Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
						        Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						        Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
						os_mut_wait (&usb_log_mutex, 0xffff);	
						sprintf(&system_log.log_buf[0],"Posting a file of scale %d to server",Modbus_Tcp_Slaves[i].Scale_ID);
						log_file_write(system_log1);		
						os_mut_release (&usb_log_mutex);						
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

						os_evt_clr(EVE_PPP_RES,t_ppp_client);				
						if(Http_Post_File(pcs_sock_num, i,1))
						{
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(Debug_Buf, "Posted a File of Scale Id %d to PCS over ppp!\r\n",Modbus_Tcp_Slaves[i].Scale_ID); //Commented by DK on 29 sep 2015
							sprintf(Debug_Buf, "Posted a File of Scale Id %d to PCS over ppp!\
							Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
							Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
							Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
							os_mut_wait (&usb_log_mutex, 0xffff);	
							sprintf(&system_log.log_buf[0],"Posted a file of scale to server%d",Modbus_Tcp_Slaves[i].Scale_ID);
							log_file_write(system_log1);	
							os_mut_release (&usb_log_mutex);							
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

							//Check repsonse from PCS If it is OK then only go for next record.
							result = os_evt_wait_or(EVE_PPP_RES,15000); 
							if(result == OS_R_EVT)
							{
								if((usPppClientRcvBufferLen > 0) && (usPppClientRcvBufferLen < PPP_RX_BUFFER_LENGTH))
								{
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									//sprintf(Debug_Buf, "Recd Resp for file of Scale ID %d from PCS!\r\n",Modbus_Tcp_Slaves[i].Scale_ID); //Commented by DK on 29 sep 2015
									sprintf(Debug_Buf, "Recd Resp for file of Scale ID %d from PCS! \
									Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
									Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
							    Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif 
									os_mut_wait (&usb_log_mutex, 0xffff);							
										
									sprintf(&system_log.log_buf[0],"Received resp. for file of scale %d",Modbus_Tcp_Slaves[i].Scale_ID);
									log_file_write(system_log1);			
									os_mut_release (&usb_log_mutex);									
									
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

								}	
								Modbus_Tcp_Slaves[i].ftp_file_recd=0;
								//Added by DK on 18May2016 to fix issue of PCM sometimes does not POST the upload to the PCS
								Modbus_Tcp_Slaves[i].get_file = 0; 
							}
							else
							{
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							/*	sprintf(Debug_Buf, "Post file Request time out for scale ID %d to PCS over PPP!\r\n",\ Modbus_Tcp_Slaves[i].Scale_ID);*/ //Commented by DK on 29 sep 2015
								sprintf(Debug_Buf, "Post file Request time out for scale ID %d to PCS over PPP! \
								                    Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n", \
    						        Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
							    Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015	
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
								
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

							//	Modbus_Tcp_Slaves[i].ftp_file_recd=0;
							}		
						}
						else
						{
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								/*sprintf(Debug_Buf, "Not able to Post file of scale ID %d to PCS over PPP!\r\n",\
							          Modbus_Tcp_Slaves[i].Scale_ID);*/ //Commented by DK on 29 sep 2015
							  sprintf(Debug_Buf, "Not able to Post file of scale ID %d to PCS over PPP! \
							                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
							          Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
							    Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
									os_mut_wait (&usb_log_mutex, 0xffff);								
									sprintf(&system_log.log_buf[0],"Not able to post a file of scale %d",Modbus_Tcp_Slaves[i].Scale_ID);
									log_file_write(system_log1);	
									os_mut_release (&usb_log_mutex);							
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

							  Modbus_Tcp_Slaves[i].ftp_file_recd=0;
							 //Added by DK on 18May2016 to fix issue of PCM sometimes does not POST the upload to the PCS
								Modbus_Tcp_Slaves[i].get_file = 0; 
						}
					}	//end of if((Modbus_Tcp_Slaves[i].Connect == 1) && (i==0) && (Modbus_Tcp_Slaves[i].ftp_file_recd == 1))	
					
					//Added by DK on 5 Feb 2015 
					if((Modbus_Tcp_Slaves[i].Connect == 1) && (Modbus_Tcp_Slaves[i].ftp_file_not_available == 1))
					{
						//os_mut_wait (&tcp_mutex, 0xffff);	 //Added by DK on 3 Feb 2015 
												
						//Preapre packet as per HTTP message(Java Script Object Notation)
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						//sprintf(Debug_Buf, "Posting a File Not found response of Scale Id %d to PCS over PPP!\r\n",Modbus_Tcp_Slaves[i].Scale_ID); //Commented by DK on 29 sep 2015
						sprintf(Debug_Buf, "\r\n Posting a File Not found response of Scale Id %d to PCS over PPP! \
						                     Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d	\r\n",\
						Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
						
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

						os_evt_clr(EVE_PPP_RES,t_ppp_client);				
						if(Http_Post_File(pcs_sock_num, i,0))
						{
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							//sprintf(Debug_Buf, "Posted a File Not found response of Scale Id %d to PCS over ppp!\r\n",Modbus_Tcp_Slaves[i].Scale_ID); //Commented by DK on 29 sep 2015
							sprintf(Debug_Buf, "Posted a File Not found response of Scale Id %d to PCS over ppp! \
							                    Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
							Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						  Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

							//Check repsonse from PCS If it is OK then only go for next record.
							result = os_evt_wait_or(EVE_PPP_RES,5000); 
							if(result == OS_R_EVT)
							{
								if((usPppClientRcvBufferLen > 0) && (usPppClientRcvBufferLen < PPP_RX_BUFFER_LENGTH))
								{
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									//sprintf(Debug_Buf, "Recd Resp for file Not found response of Scale ID %d from PCS!\r\n",Modbus_Tcp_Slaves[i].Scale_ID); //Commented by DK on 29 sep 2015
									sprintf(Debug_Buf, "Recd Resp for file Not found response of Scale ID %d from PCS! \
									                    Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
									Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						      Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);  //Added by DK on 29 sep 2015
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif 
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

								}	
								Modbus_Tcp_Slaves[i].ftp_file_not_available=0;
								Modbus_Tcp_Slaves[i].get_file=0;
							}
							else
							{
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								/*sprintf(Debug_Buf, "Post file Not found response time out for scale ID %d to PCS over PPP!\r\n",\
							          Modbus_Tcp_Slaves[i].Scale_ID);*/ //Commented by DK on 29 sep 2015
								sprintf(Debug_Buf, "Post file Not found response time out for scale ID %d to PCS over PPP! \
								                    Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
								        Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						      Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
								
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

								Modbus_Tcp_Slaves[i].ftp_file_not_available=0;
								Modbus_Tcp_Slaves[i].get_file=0;
							}		
						} //end of if(Http_Post_File(pcs_sock_num, i,0))
						else
						{
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								/*sprintf(Debug_Buf, "Not able to Post file not found response  of scale ID %d to PCS over PPP!\r\n",\
							          Modbus_Tcp_Slaves[i].Scale_ID);*/ //Commented by DK on 29 sep 2015
							  sprintf(Debug_Buf, "Not able to Post file not found response  of scale ID %d to PCS over PPP! \
								                    Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d \r\n",\
								        Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
						            Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); //Added by DK on 29 sep 2015
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

							 // Modbus_Tcp_Slaves[i].ftp_file_not_available=0;
							//	Modbus_Tcp_Slaves[i].get_file=0;					
						}
						//os_mut_release (&tcp_mutex);  //Commented by DK on 3 Feb 2015 
					}	//end of if((Modbus_Tcp_Slaves[i].Connect == 1) && (i==0) && (Modbus_Tcp_Slaves[i].ftp_file_not_available == 1))	
				}//end of else of if(pcs_sock_state != TCP_STATE_CONNECT)  //i.e. PCM is connected to PCS 
			}//end of for (i=0;i<MAX_NO_OF_INTEGRATORS;i++) 
			
			//Added by DK on 12 March 2015 
			pcs_sock_state = tcp_get_state (pcs_sock_num);  
			if((report_connectivity_status == 1) && (pcs_sock_state == TCP_STATE_CONNECT))  //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
			{
				os_evt_clr(EVE_PPP_RES,t_ppp_client);				
				memset(PPP_TX_Buffer,'\0',sizeof(PPP_TX_Buffer)); //Added by DK on 11 March 2015 
				if(Http_Post_Connectivity_Status(pcs_sock_num,&PPP_TX_Buffer[0]))	
				{
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "\r\n Posted connectivity status to PCS over PPP!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

					//Check repsonse from PCS If it is OK 
					result = os_evt_wait_or(EVE_PPP_RES,5000); 	
					if(result == OS_R_EVT)
					{
						os_evt_clr(EVE_PPP_RES,t_ppp_client);				
						if((usPppClientRcvBufferLen > 0) && (usPppClientRcvBufferLen < PPP_RX_BUFFER_LENGTH))
						{
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Recd Resp for connectivity status from PCS over PPP!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif 
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			
						   
							report_connectivity_status=0;
						}	
					}	
				}
			}	//end of if(report_connectivity_status == 1) //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
//			os_mut_release (&tcp_mutex);  //Added by DK on 3 Feb 2015 	//was Commented by DK on 29 sep 2015 PCM firmware V1.33
		}	//end of if (pcs_connect == 1) && (power_on ==0)
		
		//Added by DK on 05 Oct 2015 
		if((!ppp_is_up()) && (pcs_connect == 1))
		{
			pcs_connect=0;
			tcp_abort(pcs_sock_num); //Added by DK on 2 Jan 2015 
			tcp_close(pcs_sock_num);  
			tcp_release_socket(pcs_sock_num);
			ppp_close();
			pcs_no_of_retry=0;
			pcs_connectivity_status = NOT_CONNECTED; 
// 				for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 				{	
// 					Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 					Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 				}					
			#ifdef DEBUG_PORT
			os_mut_wait (&debug_uart_mutex, 0xffff);		
			memset(Debug_Buf,'\0',sizeof(Debug_Buf));
			sprintf(Debug_Buf, "\r\n PPP link is down.Closing socket%d!\
			Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,Current_time.RTC_Year,\
													Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
			Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			os_mut_release (&debug_uart_mutex);
			#endif
			#ifdef USB_LOGGING
			
					Write_Debug_Logs_into_file(DEBUG_PPP_EVENT,Debug_Buf,strlen(Debug_Buf));
			#endif			

		}	
		
		  pcs_sock_state = tcp_get_state (pcs_sock_num); 
			if((send_heartbeat_msg_fg == 1) && (pcs_sock_state == TCP_STATE_CONNECT))  //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
			{
//				send_heartbeat_msg_fg = 0;;
//				Task_section = 4;
//				os_mut_wait (&tcp_mutex, 0xffff);
				os_evt_clr(EVE_PPP_RES,t_http_client);		
				memset(PPP_RX_Buffer,'\0',sizeof(PPP_RX_Buffer)); //Added by DK on 11 March 2015 						
//				if(Http_Post_Connectivity_Status(pcs_sock_num,&HTTP_TX_Buffer[0]))	Http_Post_Heartbeat_message
				os_mut_wait (&usb_log_mutex, 0xffff);							
				
				sprintf(&system_log.log_buf[0],"Posting heartbeat message ");
				log_file_write(system_log1);			
				os_mut_release (&usb_log_mutex);
				
				if(Http_Post_Heartbeat_message(pcs_sock_num,&PPP_TX_Buffer[0]))	
				
				{
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "\r\n Posted heartbeat message to PCS!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					//Check repsonse from PCS If it is OK 
					result = os_evt_wait_or(EVE_PPP_RES,3000); 	
					if(result == OS_R_EVT)
					{
							os_mut_wait (&usb_log_mutex, 0xffff);							
						
				sprintf(&system_log.log_buf[0],"Posted heartbeat message successfully ");
				log_file_write(system_log1);			
				os_mut_release (&usb_log_mutex);
						
						os_evt_clr(EVE_PPP_RES,t_ppp_client);				
						if((usPppClientRcvBufferLen > 0) && (usPppClientRcvBufferLen < PPP_RX_BUFFER_LENGTH))
						{
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Recd Resp for connectivity status from PCS!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif 
							send_heartbeat_msg_fg=0;
						}	
					}	
					
				} //end of if(Http_Post_Heartbeat_message(pcs_sock_num,&HTTP_TX_Buffer[0]))	
				else
				{
					os_mut_wait (&usb_log_mutex, 0xffff);							
					sprintf(&system_log.log_buf[0],"Failed to Post heartbeat message ");
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);
					
				}
//				os_mut_release (&tcp_mutex);
			}//end of if((send_heartbeat_msg_fg == 1) 		
		//os_dly_wait(100);
	LPC_RTC->GPREG4 &= ~(0x01<<5);
//		WDTFeed();	
}	//end of while(1);
}
/*****************************************************************************
* End of file
*****************************************************************************/
