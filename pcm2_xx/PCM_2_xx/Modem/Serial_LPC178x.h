/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 	     : Serial_LPC178x.h
* @brief			         : Controller Board
*
* @author			         : Dnyaneshwar Kashid 
*
* @date Created		     : November 21 Friday, 2014  <Nov 21, 2014>
* @date Last Modified  : November 21 Friday, 2014  <Nov 21, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 		       : 
* @internal 		       :
*
*****************************************************************************/
#ifndef __SERIAL_LPC178X_H
#define __SERIAL_LPC178X_H


/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */


/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void init_serial (void);
extern BOOL com_putchar (U8 c);
extern int com_getchar (void);
extern unsigned int Baud_rate;
#endif /*SERIAL_LPC178X*/
/*****************************************************************************
* End of file
*****************************************************************************/
