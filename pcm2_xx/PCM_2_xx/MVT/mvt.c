/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : MVT.c
* @brief               : Controller Board
*
* @author              : Megha S A
*
* @date Created        : December Thursday, 2016  <Dec 8, 2016>
* @date Last Modified  : 
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            : 
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stddef.h>
#include <float.h>
#include <stdio.h>
#include <string.h>
#include <RTL.h>
#include "Global_ex.h"
#include "../gui/application/Screen_data_enum.h"
#include "../gui/application/Screen_global_ex.h"
#include "math.h"
#include "mvt.h"
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/

#ifdef MVT_TABLE
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables*/
unsigned int Current_val;
/*===========================================================================*/
/* Constants section */
 const unsigned char Pcm_id_Min = 1; //16
 const unsigned char Pcm_id_Max = 255;
 const unsigned char Pcm_id_default = 36;

const unsigned int DHCP_Status_Min = DHCP_DISABLE;
const unsigned int DHCP_Status_Max = DHCP_ENABLE;
const unsigned int DHCP_Status_Def = DHCP_ENABLE;

const unsigned int DHCP_Status_enum_list[] =
{
	Screen22_str15,
	Screen22_str14,
};
//IP_STRUCT IP_Addr;
const IP_STRUCT FOUR_G_IP_Addr_Min = {1, 1, 1, 0};
const IP_STRUCT FOUR_G_IP_Addr_Max = {255, 255, 255, 250};
const IP_STRUCT FOUR_G_IP_Addr_Def = {1, 1, 1, 1}; //{192, 168, 1, 100};

//IP_STRUCT IP_Addr;
const IP_STRUCT IP_Addr_Min = {1, 1, 1, 0};
const IP_STRUCT IP_Addr_Max = {255, 255, 255, 250};
const IP_STRUCT IP_Addr_Def = {1, 1, 1, 1}; //{192, 168, 1, 100};

//unsigned int Aggre_IP_Addr_Min;
//unsigned int Aggre_IP_Addr_Max;

const IP_STRUCT Subnet_Mask_Min = {255, 0, 0, 0};
const IP_STRUCT Subnet_Mask_Max = {255, 255, 255, 254};
const IP_STRUCT Subnet_Mask_Def = {255, 255, 255, 0};

//unsigned int Aggre_Subnet_Mask_Min;
//unsigned int Aggre_Subnet_Mask_Max;

const IP_STRUCT Gateway_Min = {1, 1, 1, 0};
const IP_STRUCT Gateway_Max = {255, 255, 255, 250};
const IP_STRUCT Gateway_Def = {1, 1, 1, 1}; //{192, 168, 1, 1};
 
const IP_STRUCT PriDNS_Min = {1, 1, 1, 0};
const IP_STRUCT PriDNS_Max = {255, 255, 255, 255};
const IP_STRUCT PriDNS_Def = {8, 8, 8, 4};

const IP_STRUCT SecDNS_Min = {1, 1, 1, 0};
const IP_STRUCT SecDNS_Max = {255, 255, 255, 255};
const IP_STRUCT SecDNS_Def = {8, 8, 4, 4};

/* Constants section */
 const unsigned int Http_Port_Number_Min = 80;//1; //16
 const unsigned int Http_Port_Number_Max = 80;//65535;
 const unsigned int Http_Port_Number_default = 80;


const unsigned int WEB_Address_Min = PRODUCTION_SITE;
const unsigned int WEB_Address_Max = TEST_SITE;
const unsigned int WEB_Address_Def = TEST_SITE;
	const unsigned int Web_Address_enum_list[] =
{
	Screen212_str1,
	Screen212_str3,
};

const unsigned int PCM_connect_Min = LAN;
const unsigned int PCM_connect_Max = VERIZON_4G;
const unsigned int PCM_connect_Def = LAN;
	const unsigned int PCM_connect_enum_list[] =
{
	Screen213_str1,
	Screen213_str3,
	Screen213_str5,
};

const unsigned int PPP_Status_Min = PPP_DISABLE;
const unsigned int PPP_Status_Max = PPP_DISABLE;			//by megha on 27/7/2017 to disable ppp status editing
const unsigned int PPP_Status_Def = PPP_DISABLE;

const unsigned int PPP_Status_enum_list[] =
{
	Screen24_str10,
	Screen24_str9,
};

const unsigned int DST_Status_Min = DST_DISABLE;
const unsigned int DST_Status_Max = DST_ENABLE;
const unsigned int DST_Status_Def = DST_DISABLE;

const unsigned int DST_Status_enum_list[] =
{
	Screen25_str8,
	Screen25_str7,
};

const float GMT_value_Min = -12.00;
const float GMT_value_Max = 12.00;
const float GMT_value_Def = -5;


const U16 Config_Date_Year_Min = DATE_YR_MIN;       // Year     [1980..2107]
const U16 Config_Date_Year_Max = DATE_YR_MAX;
const U16 Config_Date_Year_Def = DATE_YR_DEF;

const U8 Config_Date_Mon_Min = DATE_MON_MIN;        // Month    [1..12]                  
const U8 Config_Date_Mon_Max = DATE_MON_MAX;
const U8 Config_Date_Mon_Def = DATE_MON_DEF;

const U8 Config_Date_Day_Min = DATE_DATE_MIN;
const U8 Config_Date_Day_Max = DATE_DATE_MAX;
const U8 Config_Date_Day_Def = DATE_DATE_DEF;

const U8 Config_Time_Hr_Min = TIME_HR_MIN;         // Hours    [0..23]
const U8 Config_Time_Hr_Max = TIME_HR_MAX;
const U8 Config_Time_Hr_Def = TIME_HR_DEF;

const U8 Config_Time_Min_Min = TIME_MINT_MIN;      // Minutes  [0..59] 
const U8 Config_Time_Min_Max = TIME_MINT_MAX;
const U8 Config_Time_Min_Def = TIME_MINT_DEF;

const U8 Config_Time_Sec_Min = TIME_SEC_MIN; 
const U8 Config_Time_Sec_Max = TIME_SEC_MAX;
const U8 Config_Time_Sec_Def = TIME_SEC_DEF;

const unsigned int Current_Time_Format_Min = H12_HR;
const unsigned int Current_Time_Format_Max = H24_HR;
const unsigned int Current_Time_Format_Def = H24_HR;

const unsigned int Current_Date_Format_Min = FMT_MMDDYYYY;
const unsigned int Current_Date_Format_Max = FMT_DDMMYYYY;
const unsigned int Current_Date_Format_Def = FMT_MMDDYYYY;

const unsigned int Time_Format_enum_list[] =
{
	TWELVE_HR,
	TWENTYFOUR_HR,
};

const unsigned int Date_Format_enum_list[] =
{
	MMDDYYYY,
	DDMMYYYY,
};


 
 DATA_VARIABLE_STRUCT Pcm_var_Data[] __attribute__ ((section ("PCM_GUI_DATA_RAM")))=
{
	&Pcm_var.PCM_id,
	&Pcm_var.PCM_web_select,
	&Pcm_var.PCM_connection_select,
	&Pcm_var.IP_Addr.Addr1,
	&Pcm_var.Subnet_Mask.Addr1,
	&Pcm_var.Gateway.Addr1,
	&Pcm_var.PriDNS.Addr1,
	&Pcm_var.SecDNS.Addr1,
	&Pcm_var.Http_Port_Number,
//	&Pcm_var.Http_host_addr,
	&Pcm_var.PPP_Status,
	&Pcm_var.PPP_dial_no,
	&Pcm_var.DST_Status,
	&Pcm_var.GMT_value,
	&Pcm_var.Sntp_host_addr,
	&Pcm_var.Config_Date.Year,
	&Pcm_var.Config_Date.Mon,
	&Pcm_var.Config_Date.Day,
	&Pcm_var.Config_Time.Hr,
	&Pcm_var.Config_Time.Min,
	&Pcm_var.Config_Time.Sec,	
	&Pcm_var.Current_Date_Format,	
	&Pcm_var.Current_Time_Format,	
	&Pcm_var.FourG_IP_Addr,	
	&Pcm_var.DHCP_Status,
};
 
 
/*****************************************************************************/
/* Boolean variables section */
/* Character variables section */
unsigned char  Pcm_parameters_set_to_default_fg;

/*****************************************************************************/
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */


//Populated the MVT table for PCM parameters
const ELEMENT_REC Elements_Record_PCM_setup[] =
{
	//Element_Section ,Element_ID, Element_String_Enum,	Element_Data_Type,	Element_Length,	Element_Min_Value	,Element_Max_Value,	Element_Default_Value,Element_Has_Options,Element_enum_list
	{Pcm_setup_section,PCM_id_Id,Screen21_str1,unsigned_char_type,sizeof(Pcm_var.PCM_id),&Pcm_id_Min,&Pcm_id_Max,&Pcm_id_default,0, NULL},
	{Pcm_setup_section,PCM_web_select_id,Screen21_str3,unsigned_int_type,sizeof(Pcm_var.PCM_web_select),&WEB_Address_Min,&WEB_Address_Max,&WEB_Address_Def,NO_OF_WEB_ADDRESS, (unsigned int *)&Web_Address_enum_list[0]},
	{Pcm_setup_section,PCM_connection_select_id,Screen21_str5,unsigned_int_type,sizeof(Pcm_var.PCM_connection_select),&PCM_connect_Min,&PCM_connect_Max,&PCM_connect_Def,NO_OF_PCM_CONNECTIONS, (unsigned int *)&PCM_connect_enum_list[0]},

	{Pcm_setup_section, IP_Addr_Addr1_Id, Screen22_str3, ip_struct, sizeof(Pcm_var.IP_Addr.Addr1), &IP_Addr_Min, &IP_Addr_Max, &IP_Addr_Def, 0, NULL},	//Aggre_
	{Pcm_setup_section, Subnet_Mask_Addr1_Id, Screen22_str5, ip_struct, sizeof(Pcm_var.Subnet_Mask.Addr1), &Subnet_Mask_Min, &Subnet_Mask_Max, &Subnet_Mask_Def, 0, NULL},
	{Pcm_setup_section, Gateway_Addr1_Id, Screen22_str7, ip_struct, sizeof(Pcm_var.Gateway.Addr1), &Gateway_Min, &Gateway_Max, &Gateway_Def, 0, NULL},
	{Pcm_setup_section, PriDNS_Id, Screen22_str9, ip_struct, sizeof(Pcm_var.PriDNS.Addr1), &PriDNS_Min, &PriDNS_Max, &PriDNS_Def, 0, NULL},
	{Pcm_setup_section, SecDNS_Id, Screen22_str11, ip_struct, sizeof(Pcm_var.SecDNS.Addr1), &SecDNS_Min, &SecDNS_Max, &SecDNS_Def, 0, NULL},
	{Pcm_setup_section,Http_port_Id,Screen23_str1,unsigned_int_type,sizeof(Pcm_var.Http_Port_Number),&Http_Port_Number_Min,&Http_Port_Number_Max,&Http_Port_Number_default,0, NULL},
//	{Pcm_setup_section, Http_host_Id, Screen23_str3, signed_char_type, sizeof(Pcm_var.Http_host_addr), NULL, NULL, NULL, 0, NULL},
	{Pcm_setup_section, PPP_Status_Id, Screen24_str1, unsigned_int_type, sizeof(Pcm_var.PPP_Status ), &PPP_Status_Min, &PPP_Status_Max, &PPP_Status_Def, NO_OF_PPP_STATUS, (unsigned int *)&PPP_Status_enum_list[0]},
	{Pcm_setup_section, PPP_dial_no_Id, Screen24_str3, signed_char_type, sizeof(Pcm_var.PPP_dial_no), NULL, NULL, NULL, 0, NULL},
	{Pcm_setup_section, DST_Status_Id, Screen25_str1, unsigned_int_type, sizeof(Pcm_var.DST_Status), &DST_Status_Min, &DST_Status_Max, &DST_Status_Def, NO_OF_DST_STATUS, (unsigned int *)&DST_Status_enum_list[0]},
	{Pcm_setup_section, GMT_value_Id, Screen25_str3, float_type, sizeof(Pcm_var.GMT_value), &GMT_value_Min, &GMT_value_Max, &GMT_value_Def, 0, NULL},
	{Pcm_setup_section, Sntp_host_Id, Screen25_str5, signed_char_type, sizeof(Pcm_var.Sntp_host_addr), NULL, NULL, NULL, 0, NULL},
	{Pcm_setup_section, Config_Date_Year_Id, MVT_str12, unsigned_short_type, sizeof(Pcm_var.Config_Date.Year), &Config_Date_Year_Min, &Config_Date_Year_Max, &Config_Date_Year_Def, 0, NULL},
	{Pcm_setup_section, Config_Date_Mon_Id, MVT_str13, unsigned_char_type, sizeof(Pcm_var.Config_Date.Mon), &Config_Date_Mon_Min, &Config_Date_Mon_Max, &Config_Date_Mon_Def, 0, NULL},
	{Pcm_setup_section, Config_Date_Day_Id, MVT_str14, unsigned_char_type, sizeof(Pcm_var.Config_Date.Day), &Config_Date_Day_Min, &Config_Date_Day_Max, &Config_Date_Day_Def, 0, NULL},
	{Pcm_setup_section, Config_Time_Hr_Id, MVT_str15, unsigned_char_type, sizeof(Pcm_var.Config_Time.Hr), &Config_Time_Hr_Min, &Config_Time_Hr_Max, &Config_Time_Hr_Def, 0, NULL},
	{Pcm_setup_section, Config_Time_Min_Id, MVT_str17, unsigned_char_type, sizeof(Pcm_var.Config_Time.Min), &Config_Time_Min_Min, &Config_Time_Min_Max, &Config_Time_Min_Def, 0, NULL},
	{Pcm_setup_section, Config_Time_Sec_Id, MVT_str18, unsigned_char_type, sizeof(Pcm_var.Config_Time.Sec), &Config_Time_Sec_Min, &Config_Time_Sec_Max, &Config_Time_Sec_Def, 0, NULL},
	{Pcm_setup_section, Current_Date_Format_Id, Screen31_str1, unsigned_int_type, sizeof(Pcm_var.Current_Date_Format), &Current_Date_Format_Min, &Current_Date_Format_Max, &Current_Date_Format_Def, NO_OF_DATE_FORMAT, (unsigned int *)&Date_Format_enum_list[0]},
	{Pcm_setup_section, Current_Time_Format_Id, Screen32_str1, unsigned_int_type, sizeof(Pcm_var.Current_Time_Format), &Current_Time_Format_Min, &Current_Time_Format_Max, &Current_Time_Format_Def, NO_OF_TIME_FORMAT, (unsigned int *)&Time_Format_enum_list[0]},
	{Pcm_setup_section, Four_g_IP_Addr_Addr1_Id, Screen24_str12, ip_struct, sizeof(Pcm_var.FourG_IP_Addr.Addr1), &FOUR_G_IP_Addr_Min, &FOUR_G_IP_Addr_Max, &FOUR_G_IP_Addr_Def, 0, NULL},	//Aggre_

	
	{Pcm_setup_section, DHCP_Status_Id, Screen22_str1, unsigned_int_type, sizeof(Pcm_var.DHCP_Status), &DHCP_Status_Min, &DHCP_Status_Max, &DHCP_Status_Def, NO_OF_DHCP_STATUS, (unsigned int *)&DHCP_Status_enum_list[0]},

};
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void Verify_PCM_Variables(void);
void Get_Default_Value(U16 Variables_ID, void* pdefault_val, void* pmin_val, void* pmax_val,void* pcurr_val);
unsigned int ConvIPAddr(IP_STRUCT *IP_addr);

/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name : void Verify_PCM_Variables(void)
* @returns    returns       : None
*                           :
* @param      arg1          : None
* @author                   : MSA
* @date       date created  : 8/12/2016
* @brief      Description   : Verify All PCM parameters for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_PCM_Variables(void)
{
  U16 ID_of_Variable, Temp_Id;
	
  Pcm_parameters_set_to_default_fg = 0;	
	
	for(ID_of_Variable = START_OF_PCM_VAR_ID; ID_of_Variable <= END_OF_PCM_VAR_ID; ID_of_Variable++)
  {
		Temp_Id = (ID_of_Variable - START_OF_PCM_VAR_ID);
		Verify_Variable((const ELEMENT_REC*)&Elements_Record_PCM_setup[Temp_Id]);		
	}
}





/*****************************************************************************
* @note       Function name : void Verify_Variable(const ELEMENT_REC *MVT_Table)
* @returns    returns       : None
*                           :
* @param      arg1          : STRUCTURE POINTER TO ELEMENTS OF ELEMENT_REC TABLE
* @author                   : MSA
* @date       date created  : 8/12/2016
* @brief      Description   : Verify All PCM parameters for min, max and if outside set to default 
* @note       Notes         : None
*****************************************************************************/
void Verify_Variable(const ELEMENT_REC *MVT_Table)
{
	float fElemt_max,fElemt_max_t,fElemt_min,fElemt_def,fCurrent_val; 
	double dElemt_max,dElemt_min,dElemt_def,dCurrent_val;
	unsigned int uiElemt_max,uiElemt_min,uiElemt_def,uiCurrent_val;
	signed int iElemt_max,iElemt_min,iElemt_def,iCurrent_val;
	unsigned short usElemt_max,usElemt_min,usElemt_def,usCurrent_val;
	unsigned char uchElemt_max,uchElemt_min,uchElemt_def,uchCurrent_val;
	IP_STRUCT *ipElemt_max,*ipElemt_min,ipElemt_def, *ptr_ipCurrentVal;
	
	U16 Variables_Data_Type;
	void *ptr_CurrentVal;
	
	unsigned int temp_unit = 0xFFFF, i;
	unsigned int Elemt_idx_max,Elemt_idx_min,Elemt_idx_def,*Elemt_List_Address;
	unsigned int Elemt_str_min, Elemt_str_max, Elemt_str_def;//,Current_val;
	
	U16 NO_OF_OPTIONS, Variables_Section;
	U16 Variable_ID;
	
	//Get Data type of the variable
	Variables_Data_Type = MVT_Table->Element_Data_Type;
	//Get Variable ID
	Variable_ID = MVT_Table->Element_ID;
	//Get Variable ID
	Variables_Section = MVT_Table->Element_Section;
	//Get no of options
	NO_OF_OPTIONS = MVT_Table->Element_Has_Options ;
	if(NO_OF_OPTIONS != 0)
	{
		//Get Max, Min and Def value from table
		Elemt_idx_max = *(unsigned int*)MVT_Table->Element_Max;
		Elemt_idx_min = *(unsigned int*)MVT_Table->Element_Min;	
		Elemt_idx_def = *(unsigned int*)MVT_Table->Element_Def;	
		ptr_CurrentVal = Pcm_var_Data[Variable_ID].DataVar;
		Current_val =    (*(unsigned int*)ptr_CurrentVal);
		
		//Get start address of option list 
		Elemt_List_Address = (unsigned int *)(MVT_Table->Element_enum_list);
		Elemt_str_min = (*(&Elemt_List_Address[Elemt_idx_min]));
		Elemt_str_max = (*(&Elemt_List_Address[Elemt_idx_max]));
		Elemt_str_def = (*(&Elemt_List_Address[Elemt_idx_def]));
	
		//Check for vaild values are entered in MVT table for Max, Min and Def 
		if((Elemt_idx_max <= (unsigned int)Elemt_idx_max) && (Elemt_idx_min <= Elemt_idx_max) && (Elemt_idx_def <= Elemt_idx_max))
		{	
			//check for all options  
			for(i = Elemt_idx_min; i < (NO_OF_OPTIONS & (~OPTION_MASK)); i++)
			{
				if(*(&Elemt_List_Address[i]) == Current_val)
				{
					temp_unit = i; 
				}	 
			}   
		}
		Get_Default_Value(Variable_ID, (unsigned int*)&Elemt_str_def, (unsigned int*)&Elemt_str_min, (unsigned int*)&Elemt_str_max,0);
		
		if((temp_unit < Elemt_idx_min) || (temp_unit > Elemt_idx_max)) 			
		{
			
			*(unsigned int*)Pcm_var_Data[Variable_ID].DataVar = Elemt_str_def;
			Pcm_parameters_set_to_default_fg = 1;		
		}
	}
	else
  {		
		switch(Variables_Data_Type)
		{
			case unsigned_char_type:
				uchElemt_max = *(unsigned char *)MVT_Table->Element_Max;
				uchElemt_min = *(unsigned char *)MVT_Table->Element_Min;
			  uchElemt_def = *(unsigned char *)MVT_Table->Element_Def;

				Get_Default_Value(Variable_ID, (unsigned char*)&uchElemt_def, (unsigned char*)&uchElemt_min, (unsigned char*)&uchElemt_max,0);
				//Check for range
				if((uchElemt_min == NULL) && (uchElemt_max == NULL) && (uchElemt_def == NULL))
				{
					;	//condition for character strings
				}
				else
				{				
					ptr_CurrentVal = Pcm_var_Data[Variable_ID].DataVar;
					uchCurrent_val = (*(unsigned char *)ptr_CurrentVal);
					if((uchCurrent_val > uchElemt_max) || (uchCurrent_val < uchElemt_min) ||(isnan(uchCurrent_val)))
					{
							*(unsigned char *)Pcm_var_Data[Variable_ID].DataVar = uchElemt_def;
							Pcm_parameters_set_to_default_fg = 1;
					}	//
				}
			break;		

			case unsigned_int_type:
				uiElemt_max = *(unsigned int *)MVT_Table->Element_Max;
				uiElemt_min = *(unsigned int *)MVT_Table->Element_Min;
				uiElemt_def = *(unsigned int *)MVT_Table->Element_Def;
				ptr_CurrentVal = Pcm_var_Data[Variable_ID].DataVar;
				uiCurrent_val = (*(unsigned int *)ptr_CurrentVal);
        Get_Default_Value(Variable_ID, (unsigned int*)&uiElemt_def, (unsigned int*)&uiElemt_min, (unsigned int*)&uiElemt_max,0);
				//Get_Default_Value(Variable_ID, (unsigned int*)&uiElemt_def);
			
			  //Check for range     
				if((uiCurrent_val > uiElemt_max) || (uiCurrent_val < uiElemt_min) ||(isnan(uiCurrent_val)))
				{
						//load default value
						*(unsigned int *)Pcm_var_Data[Variable_ID].DataVar = uiElemt_def;
						Pcm_parameters_set_to_default_fg = 1;
				}
			break;	

				
			case ip_struct:
				ipElemt_max = (IP_STRUCT *)MVT_Table->Element_Max;
				ipElemt_min = (IP_STRUCT *)MVT_Table->Element_Min;
				uiElemt_max = ConvIPAddr((IP_STRUCT*)MVT_Table->Element_Max);
				uiElemt_min = ConvIPAddr((IP_STRUCT*)MVT_Table->Element_Min);
				memcpy((IP_STRUCT *)&ipElemt_def, (IP_STRUCT *)MVT_Table->Element_Def, sizeof(IP_STRUCT));
			
				//ipElemt_def.Addr1 = *(IP_STRUCT *)MVT_Table->Element_Def.Addr1;
				ptr_CurrentVal = Pcm_var_Data[Variable_ID].DataVar;
			  ptr_ipCurrentVal = (IP_STRUCT*)ptr_CurrentVal;
			  uiCurrent_val = ConvIPAddr((IP_STRUCT*) ptr_CurrentVal);
				//uiCurrent_val = (*(unsigned int *)ptr_CurrentVal);
        
			  //Check for range     
				if((uiCurrent_val > uiElemt_max) || (uiCurrent_val < uiElemt_min))
				{
						//load default value
					memcpy((IP_STRUCT *)Pcm_var_Data[Variable_ID].DataVar, (IP_STRUCT *)&ipElemt_def, sizeof(IP_STRUCT));
					Pcm_parameters_set_to_default_fg = 1;
				}
			break;

			case unsigned_short_type:
				usElemt_max = *(unsigned short *)MVT_Table->Element_Max;
				usElemt_min = *(unsigned short *)MVT_Table->Element_Min;
				usElemt_def = *(unsigned short *)MVT_Table->Element_Def;
				ptr_CurrentVal = Pcm_var_Data[Variable_ID].DataVar;
				usCurrent_val = (*(unsigned short *)ptr_CurrentVal);
				Get_Default_Value(Variable_ID, (unsigned short*)&usElemt_def, (unsigned short*)&usElemt_min, (unsigned short*)&usElemt_max,0);
				//Get_Default_Value(Variable_ID, (unsigned short*)&usElemt_def);
			
				//Check for range
			  if((usCurrent_val > usElemt_max) || (usCurrent_val < usElemt_min) ||(isnan(usCurrent_val)))
				{
					
				//	if(GUI_data_nav.GUI_structure_backup != 1)
					{
						//load default value
						//Get_Default_Value(Variable_ID, (unsigned short*)&usElemt_def);
						*(unsigned short *)Pcm_var_Data[Variable_ID].DataVar = usElemt_def;
					}	
					Pcm_parameters_set_to_default_fg = 1;
				}
      break;	

			case signed_char_type:
           switch(Variable_ID) 				
		       { 
// 						case Http_host_Id:
// 							Pcm_var.Http_host_addr[HTTP_HOST_NOC-1] = '\0';
// 							break;
						case PPP_dial_no_Id:
							Pcm_var.PPP_dial_no[PPP_DIAL_LEN-1] = '\0';
							break;	
						case Sntp_host_Id:
							Pcm_var.Sntp_host_addr[SNTP_HOST_NOC-1] = '\0';
							break;							
						default:
							break;
					}
					break;
					
			case float_type:
			case float_type1:
			case float_type2:
				fElemt_max = *((float *)(MVT_Table->Element_Max));
				fElemt_max_t = *((float *)(MVT_Table->Element_Max));
				fElemt_min = *((float *)MVT_Table->Element_Min);
				fElemt_def = *(float *)MVT_Table->Element_Def;
				ptr_CurrentVal = Pcm_var_Data[Variable_ID].DataVar;
				fCurrent_val = (*(float *)ptr_CurrentVal);

				
//		fCurrent_val = (float)ConvWeightUnit((unsigned int)Test_zero_weight_Def_unit, (unsigned int)Calibration_var.Test_zero_weight_unit, (double)fCurrent_val);
 //(*(float *)ptr_CurrentVal)=fCurrent_val;
			  Get_Default_Value(Variable_ID, (float*)&fElemt_def, (float*)&fElemt_min, (float*)&fElemt_max,(float*)&fCurrent_val);
	//		*(Scale_var_Data[Variable_ID].DataVar) = fCurrent_val;
			//sks check for english to metric
				//Get_Default_Value(Variable_ID, (float*)&fElemt_def);
        //Check for range  
				if((fCurrent_val > fElemt_max) || (fCurrent_val < fElemt_min) ||(isnan(fCurrent_val)))
				{
					//load default value
					*(float *)Pcm_var_Data[Variable_ID].DataVar = fElemt_def;
					Pcm_parameters_set_to_default_fg = 1;
				}
			break;					
					
			default:
			break;									
				
		}
	}
	
}


void Get_Default_Value(U16 Variables_ID, void* pdefault_val, void* pmin_val, void* pmax_val,void* pcurr_val)
{
	float *f_def_val = ((float*)pdefault_val);
	float *f_min_val = ((float*)pmin_val);
	float *f_max_val = ((float*)pmax_val);
//	float *f_curr_val = ((float*)pcurr_val);
	//signed int *S32_ret_val = ((signed int*)pdefault_val);
	unsigned int *U32_ret_val = ((unsigned int*)pdefault_val);
	
	double *d_def_val = ((double*)pdefault_val);
	double *d_min_val = ((double*)pmin_val);
	double *d_max_val = ((double*)pmax_val);
//	double *d_curr_val = ((double*)pcurr_val);
	
	unsigned int *ui_def_val = ((unsigned int*)pdefault_val);
	unsigned int *ui_min_val = ((unsigned int*)pmin_val);
	unsigned int *ui_max_val = ((unsigned int*)pmax_val);
//	unsigned int *ui_curr_val = ((unsigned int*)pcurr_val);
	
}





/*****************************************************************************
* @note       Function name: U32 ConvIPAddr(IP_STRUCT *IP_addr)
* @returns    returns      : none
* @param      arg1         : Pointer to the IP structure
* @author                  : Suvrat Joshi
* @date       date created : 2nd Mar 2016
* @brief      Description  : Converts the IP address to aggregate address
* @note       Notes        : None
*****************************************************************************/
//void Convert_rate_user_values(unsigned int prev_rate_unit)
unsigned int ConvIPAddr(IP_STRUCT *IP_addr)
{
		unsigned int aggregate_ip = 0;
		aggregate_ip = ((IP_addr->Addr1)*1000) + ((IP_addr->Addr2)*100) + ((IP_addr->Addr3)*10) + ((IP_addr->Addr4)*1);

		return (aggregate_ip);
}

#endif /*#ifdef MVT_TABLE*/
/*****************************************************************************
* End of file
*****************************************************************************/
