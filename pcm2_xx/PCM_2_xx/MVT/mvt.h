/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Plant Connect Modem - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : MVT.h
* @brief			         : Controller Board
*
* @author			         : Megha 
*
* @date Created		     : December Thursday, 2016  <Dec 8, 2016>
* @date Last Modified	 : 
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __MVT_H
#define __MVT_H


/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

#ifdef MVT_TABLE
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
#define OPTION_MASK									(U16)0x8000 
#define START_OF_PCM_VAR_ID 					PCM_id_Id
#define END_OF_PCM_VAR_ID 						DHCP_Status_Id
#define DATE_YR_MIN			(U16)1980
#define DATE_YR_MAX			(U16)2107
#define DATE_YR_DEF			(U16)2015

#define DATE_MON_MIN		(U8)1
#define DATE_MON_MAX		(U8)12
#define DATE_MON_DEF		(U8)1

#define DATE_DATE_MIN		(U8)1
#define DATE_DATE_MAX		(U8)31
#define DATE_DATE_DEF		(U8)1

#define TIME_HR_MIN			(U16)0
#define TIME_HR_MAX			(U16)23
#define TIME_HR_DEF			(U16)0

#define TIME_AM_PM_MIN		AM_TIME
#define TIME_AM_PM_MAX		PM_TIME
#define TIME_AM_PM_DEF		AM_TIME

#define TIME_MINT_MIN		(U8)0
#define TIME_MINT_MAX		(U8)59
#define TIME_MINT_DEF		(U8)0

#define TIME_SEC_MIN		(U8)0
#define TIME_SEC_MAX		(U8)59
#define TIME_SEC_DEF		(U8)0

#define NO_OF_DHCP_STATUS						2
#define NO_OF_PPP_STATUS						2
#define NO_OF_DST_STATUS						2
#define NO_OF_TIME_FORMAT						2
#define NO_OF_DATE_FORMAT						2
#define NO_OF_WEB_ADDRESS						2
#define NO_OF_PCM_CONNECTIONS				3
/*============================================================================
* Public Data Types*/
typedef enum
{
	H12_HR = 0,
  H24_HR,
} TIME_FORMAT;

//Date Format
typedef enum
{
	FMT_MMDDYYYY = 0,
  FMT_DDMMYYYY,
} DATE_FORMAT;

typedef enum
{
    None = 0,
		Pcm_setup_section,
}ELEMENT_SECTION;

//DHCP Status
typedef enum
{
	DHCP_DISABLE = 0,
	DHCP_ENABLE,
} DHCP_STATUS;

typedef enum
{
	PPP_DISABLE = 0,
	PPP_ENABLE,
} PPP_STATUS;

typedef enum
{
	PRODUCTION_SITE = 0,
	TEST_SITE,
} WEB_ADDRESS;

typedef enum
{
	LAN = 0,
	MULTITECH_HSPA,
	VERIZON_4G,
} PCM_CONNECTION;


typedef enum
{
	DST_DISABLE = 0,
	DST_ENABLE,
} DST_STATUS;

typedef struct 
{
		U16 Element_Section;
    U16 Element_ID;	    
    U16 Element_String_Enum; 
    U16 Element_Data_Type;
    U16 Element_Length;
    const void *Element_Min;
    const void *Element_Max;
    const void *Element_Def; 
	  U16 Element_Has_Options;
	  void *Element_enum_list;
}ELEMENT_REC;

/*===========================================================================*/
typedef enum
{
		PCM_id_Id = 0,
		PCM_web_select_id,
		PCM_connection_select_id,
		IP_Addr_Addr1_Id,
		Subnet_Mask_Addr1_Id,
		Gateway_Addr1_Id,
		PriDNS_Id,
		SecDNS_Id,
		Http_port_Id,
//		Http_host_Id,	
		PPP_Status_Id,
		PPP_dial_no_Id,
		DST_Status_Id,
		GMT_value_Id,
		Sntp_host_Id,
		Config_Date_Year_Id,
		Config_Date_Mon_Id,
		Config_Date_Day_Id,
		Config_Time_Hr_Id,
		Config_Time_Min_Id,
		Config_Time_Sec_Id,
		Current_Date_Format_Id,
		Current_Time_Format_Id,
		Four_g_IP_Addr_Addr1_Id,
	
		DHCP_Status_Id,	
}ELEMENT_ID;

typedef struct
{
	void *DataVar;
}	
DATA_VARIABLE_STRUCT;
/*============================================================================*/
/*Public Variables*/

extern DATA_VARIABLE_STRUCT Pcm_var_Data[];
/*===========================================================================*/
/* Boolean variables section */

/* Character variables section */
extern unsigned char  Pcm_parameters_set_to_default_fg;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void Verify_PCM_Variables(void);
extern void Verify_Variable(const ELEMENT_REC *MVT_Table);
extern void Get_Default_Value(U16 Variables_ID, void* pdefault_val, void* pmin_val, void* pmax_val,void* pcurr_val);




#endif /*#ifdef MVT_TABLE*/

#endif /*__MVT_H*/
/*****************************************************************************
* End of file
*****************************************************************************/

