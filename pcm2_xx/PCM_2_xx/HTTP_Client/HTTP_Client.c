/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     : Beltway
*
* @file Filename       : HTTP_Client.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid 
*
* @date Created        : November Wednesday,5 2014  <Nov 5, 2014>
* @date Last Modified  : December Thursday, 2014 <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
/* ----------------------- Platform includes --------------------------------*/
#include <ctype.h>
#include <stdlib.h>
#include "math.h"
#include "Global_ex.h"
#include "Modbus_Tcp_Master.h"
#include "Http_Client.h"
#include "RTOS_main.h"
#include "mbport.h"
#include "Main.h"
#include "sntp.h"
#include "rtc.h"
#include "I2C_driver.h"
#include "DebugLog.h"
#include "../GUI/APPLICATION/Screen_global_ex.h"
#include "../gui/GUI.h"
#include "File_update.h"
#include <time.h>
#include "../GUI/APPLICATION/Screen_data_enum.h"



/*============================================================================
* Private Macro Definitions
*===========================================================================*/
double f_accum_wt_tmp;
extern unsigned char file_request_in_process;


/*============================================================================
* Private Data Types
*===========================================================================*/
#define RUNSCREEN_TEXT_EDIT_POS_Y     0

static char arcHttpClientRcvBuffer[HTTP_CLIENT_BUFFER_LENGTH];

static const char Get_Req_Header_2[]="/Configuration HTTP/1.1\r\n";
static const char Req_Header_3[] = "ApiKey: LStgRGV2yyCBib8E3t8z\r\n";
static const char Req_Header_4[] = "Content-Length: 213\r\n";
static const char Req_Header_5[] = "Cache-Control: no-cache\r\n";
static const char Req_Header_6[] = "Postman-Token: 438b3670-c345-2f52-e284-7d35633da806\r\n";
static const char Req_Header_7[] = "Accept: */*\r\n";
static const char Req_Header_8[] = "Content-Type: application/x-www-form-urlencoded\r\n";
static const char Req_Header_9[] = "\r\n";
static const char Req_Header_8_1[] = "Content-Type: application/json\r\n";//charset=utf-8\r\n";
static const char Req_Header_10[] ="Postman-Token: 6c839ad5-5697-a211-eb03-eb3248ec425c";
//char token_fourG[] ;
static const char Res_Header_1[] = "HTTP/1.1 200 OK";
static const char Res_Header_2[] = "ModemId";
static const char Res_Header_3[] = "[{";
static const char Res_Header_4[] = "ScaleId";
static const char Res_Header_5[] = "IpAddress";
static const char Res_Header_6[] = "ModbusSlaveAddress";
static const char Res_Header_7[] = "\"Status\":\"Ok\"";

static const char File_Req_Header_1[] ="Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW\r\n";
static const char File_Req_Header_2[] ="------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n";
static const char File_Req_Header_3[] ="Accept: */*\r\n""\r\n";
static const char File_Req_Header_4[] = "Content-Length:       \r\n";
static const char File_Req_Header_5[] ="Content-Disposition: form-data; name=";
static const char File_Req_Header_6[] ="FileDate";
static const char File_Req_Header_7[] ="ScaleId";
//static const char File_Req_Header_8[] ="TheFile"; //Commented by DK on 5 Feb 2015 
static const char File_Req_Header_8[] ="FileStatus"; //Added by DK on 5 Feb 2015 
static const char File_Req_Header_9[] ="filename=";
static const char File_Req_Header_10[] ="Content-Type:  text/plain\r\n\r\n";
//static const char File_Req_Header_11[] ="------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n"; //Commented by DK on 11 March 2015 
static const char File_Req_Header_11[] ="\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n"; //Added by DK on 11 March 2015 
static const char File_Req_Header_12[] ="FileIncluded\r\n"; //Added by DK on 5 Feb 2015 
static const char File_Req_Header_13[] ="FileNotPresent\r\n"; //Added by DK on 5 Feb 2015 
static const char File_Req_Header_14[] ="ScaleNotAvailable\r\n"; //Added by RG on 2 Jun 2015 
static const char File_Req_Header_15[] ="InvalidScaleID\r\n"; //Added by megha on 30/11/2017
static const char Connectivity_Req_Header_4[] = "Content-Length:    \r\n";

static const char Res_Header_3_1[] = "\"token\"";


extern unsigned char LTE_IpAdr[4];
extern unsigned char LTE_host_name[30];
extern char token_fourG[35] ;
extern BOOL Http_Get_LTE_login(U8 LTE_sock_num, char* ptr);
/*#ifdef DEBUG_PORT
__align(4) char h_temp_buff[200] __attribute__ ((section ("HTTP_BUF_DATA")));;
#endif
*/

static U16 usHttpClientRcvBufferLen;
//unsigned int token_arr_byte,token_arr_byte1; 
//Expected Response if successfull i.e. HTTP staus 200 OK 
//{"Payload":{"MeasurementStatus":"Saved","Message":null},"Status":"Ok"}  
//In case of any error for e.g. function field invalid 
//{"Payload":["The field Function is invalid."],"Status":"Error"}
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL url_redirected=0;
/* Character variables section */
unsigned char PCM_ID;
unsigned char PCS_IpAdr[4];
unsigned char Total_Scales;
unsigned char pcs_connect_disp_fg,scale_config_req_fg,Scale_configuration_received_ok_fg=0;

__align(4) char HTTP_TX_Buffer[HTTP_TX_BUFFER_LENGTH] __attribute__ ((section ("HTTP_BUF_DATA")));
__align(4) char HTTP_RX_Buffer[HTTP_RX_BUFFER_LENGTH] __attribute__ ((section ("HTTP_BUF_DATA")));
static __align(4) char redir_url[200] __attribute__ ((section ("HTTP_BUF_DATA")));
/* unsigned integer variables section */
unsigned int msg_log_reg = 0;

/* Signed integer variables section */

/* unsigned long integer variables section */
CONNECTIVITY_STATUS pcs_connectivity_status; //Added by DK on 6 Feb 2015 

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
// BOOL Http_Get_signal_strength(U8 pcs_sock_num, char* ptr);
// BOOL Http_Get_Read_signal_strength(char* pcSrcBuffer, U16 rx_length);
//BOOL modem_login(U8 pcs_sock_num, char* ptr);



BOOL tcp_send_data(U8 tcp_soc, U8* tx_buf, U16 length)
{
	U8* sendbuf;
	U16 maxlen;
	BOOL ret_val = FALSE;
	if(tcp_check_send(tcp_soc))
	{	
		//The socket is ready to send the data
		maxlen=tcp_max_dsize(tcp_soc);
		if(maxlen >= length )
		{
			sendbuf = tcp_get_buf (length);
			memcpy (sendbuf, tx_buf,length);
			ret_val = tcp_send (tcp_soc, sendbuf, length);
			main_TcpNet();			
			#ifdef USB_LOGGING
			
				Write_Debug_Logs_into_file(DEBUG_TCP_EVENT,sendbuf,length);
			#endif
			
		}
		else 
		{
// 			while(length>maxlen)
// 			{
// 				sendbuf = tcp_get_buf (maxlen);
// 				memcpy (sendbuf, tx_buf,maxlen);
// 				ret_val = tcp_send (tcp_soc, sendbuf, maxlen);
// 					#ifdef USB_LOGGING
// 				
// 					Write_Debug_Logs_into_file(DEBUG_TCP_EVENT,sendbuf,length);
// 				#endif
// 				if(ret_val)
// 				{
// 				length -= maxlen;
// 				}
// 				else
// 				{
// 					ret_val = FALSE;
// 				}
// 			}
// 			sendbuf = tcp_get_buf (maxlen);
// 			memcpy (sendbuf, tx_buf,maxlen);
// 			ret_val = tcp_send (tcp_soc, sendbuf, maxlen);
// 			#ifdef USB_LOGGING
// 			
// 				Write_Debug_Logs_into_file(DEBUG_TCP_EVENT,sendbuf,length);
// 			#endif			
// 			
			
			
			
			
			
			sendbuf = tcp_get_buf (maxlen);
			memcpy (sendbuf, tx_buf,maxlen);
			ret_val = tcp_send (tcp_soc, sendbuf, maxlen);
				#ifdef USB_LOGGING
			
				Write_Debug_Logs_into_file(DEBUG_TCP_EVENT,sendbuf,length);
			#endif
			if(ret_val)
			{
				length -= maxlen;
				os_dly_wait(10);			
				maxlen=tcp_max_dsize(tcp_soc);
				if(maxlen > length )
				{
					sendbuf = tcp_get_buf (length);
					memcpy (sendbuf, &tx_buf[maxlen],length);
					ret_val = tcp_send (tcp_soc, sendbuf, length);
					main_TcpNet();			
						#ifdef USB_LOGGING
			
				Write_Debug_Logs_into_file(DEBUG_TCP_EVENT,sendbuf,length);
			#endif
					return ret_val; 
				}
				else 
				{
					ret_val =FALSE;
				}
			}
		}	
	}
	return ret_val; 
}
/*============================================================================
* Function Implementation Section
*===========================================================================*/
///////////////////////////////////////////////////////////////////////////////
//
// Function     : HTTPStrCaseStr
// Purpose      : Same as strstr() only case insensitive
// Returns      : a pointer to the position of the searched string (or 0 on error)
// Last updated : 01/09/2005
//
///////////////////////////////////////////////////////////////////////////////
char *HTTPStrCaseStr(const char *pSrc,U32 nSrcLength,const char *pFind)
{
    const char *ptr = pSrc;
    const char *ptr2;
   // U32 iLength = 0;

    while(1) 
    {
       /* if(iLength >= nSrcLength)
        {
            break;
        }*/
        ptr = strchr(pSrc,toupper(*pFind));
        ptr2 = strchr(pSrc,tolower(*pFind));
        if (!ptr) 
        {
            ptr = ptr2; 
        }
        if (!ptr) 
        {
            break;
        }
        if (ptr2 && (ptr2 < ptr)) {
            ptr = ptr2;
        }
        if (!strncmp(ptr,pFind,strlen(pFind))) 
        {
            return(char *) ptr;
        }
        pSrc = ptr+1;
      //  iLength++;
    }
    return 0;
}


static U16 prvxTCPPortHTTPCallback(U8 soc, U8 event, U8 *ptr, U16 par)
{
    U16 retval = 0;

    switch (event)
    {
    case TCP_EVT_CONREQ:

        break;
    case TCP_EVT_ABORT:

      break;
    case TCP_EVT_CONNECT:
			retval = 1;
      break;
    case TCP_EVT_CLOSE:

      break;
    case TCP_EVT_ACK:
			os_evt_set(EVE_HTTP_ACK,t_http_client);
			retval = 1;
      break;
    case TCP_EVT_DATA:
		  //Check HTTP response from PCS
			if( par < sizeof(HTTP_RX_Buffer))
      {
			  memset(HTTP_RX_Buffer,'\0',sizeof(HTTP_RX_Buffer)); //added by DK on 16 Dec 2014 
				memcpy( HTTP_RX_Buffer, ptr, par );
			  usHttpClientRcvBufferLen = par;
				os_evt_set(EVE_HTTP_RES,t_http_client);
			}
			break;
		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
       break;

  }
  return retval;

}

//BOOL Http_Get_Request(U8 pcs_sock_num) //Commented by DK on 2 Jan 2015 
BOOL Http_Get_Request(U8 pcs_sock_num, char* ptr)  //Added by DK on 2 Jan 2015 
{
//	char *ptr; //Commented by DK on 2 Jan 2015 
	char temp_buff[40];
  U16 length;
	BOOL ret_val;
	
	strcpy(ptr,"GET ");
	
	if(url_redirected ==0)
	{
	strcat(ptr,Http_Host_Name);

	
	strcat(ptr,"/Modem/");
  memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"%d",PCM_ID); //PCM ID 
	strcat(ptr,&temp_buff[0]);
	strcat(ptr,&Get_Req_Header_2[0]); // Configuration 
	strcat(ptr,"Host: ");
	strcat(ptr,&Http_Host_Name[7]);
	strcat(ptr,"\r\n");
	}
	else
	{
	strcat(ptr,redir_url);
// 	strcat(ptr," HTTP/1.1\r\n");
	strcat(ptr,"Host: ");
	strcat(ptr,&Http_Host_Name[7]);
	strcat(ptr,"\r\n");		
	}
	strcat(ptr,&Req_Header_3[0]); //Api Key
	strcat(ptr,&Req_Header_7[0]); //Accept all 
	strcat(ptr,&Req_Header_9[0]); // /r/n
	length= strlen(ptr);
  //Send packet to PCS
	ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,length);
	return ret_val;
}	

BOOL Http_Get_Read_Response(char* pcSrcBuffer, U16 rx_length)
{
	char *ptr,*http_ptr;
	U16 RecvValue=0,i,j,RecvBufPosn;
	BOOL ret_val=FALSE;
  char temp_buff[40];
	unsigned int k;
	U32 address;
	U16 Prev_Id, Cur_Id;
//	token_arr_byte1 = rx_length;			//for testing 14/7/2017
	memset(arcHttpClientRcvBuffer,'\0',sizeof(arcHttpClientRcvBuffer));
	
//	memcpy(arcHttpClientRcvBuffer,pcSrcBuffer,rx_length);
	for(k = 0;k<rx_length;k++)
	{
		arcHttpClientRcvBuffer[k] = pcSrcBuffer[k];
	}
	
	if(0 ==(ptr = HTTPStrCaseStr(arcHttpClientRcvBuffer,sizeof(Res_Header_1),Res_Header_1))) //no string "HTTP/1.1 200 OK"
	{
									
	}	
	if(0 ==(ptr = HTTPStrCaseStr(arcHttpClientRcvBuffer,sizeof(Res_Header_7),Res_Header_7))) //no string "Status":"ok""
	{
									
	}	
	else if(0 == (ptr = HTTPStrCaseStr(arcHttpClientRcvBuffer,sizeof(Res_Header_2),Res_Header_2))) //no "ModemId" string								
	{
									
									
	}
	else 
	{
		RecvValue=0;
		ptr+=(sizeof(Res_Header_2) + 1);
		memset(temp_buff,'\0',sizeof(temp_buff));
		for(i=0;i<3;i++)
		{
			if(*(ptr+i)!=',')
			{
				temp_buff[i]=*(ptr+i);
			}
			else break;
		}	
		RecvValue=atoi(temp_buff);								
	}

	if((RecvValue == PCM_ID) && (0!=(http_ptr = HTTPStrCaseStr(arcHttpClientRcvBuffer,sizeof(Res_Header_3),Res_Header_3)))) 
	{
		RecvBufPosn=0;
		Total_Scales=0;
		Prev_Id=0;
		Cur_Id=0;
		while(1)
		{
			RecvValue=0;
			if(0 !=(ptr = HTTPStrCaseStr( http_ptr ,sizeof(Res_Header_4),Res_Header_4)))// "ScaleId" string								
			{
				ptr+=(sizeof(Res_Header_4) + 1);
				http_ptr = ptr; //added by DK on 8 Dec 2014 
				RecvBufPosn += (sizeof(Res_Header_4) + 1 );
				memset(temp_buff,'\0',sizeof(temp_buff));
				//for(j=0;j<3;j++)
				for(j=0;j<6;j++) //Modified by DK on 19May2016 for PCS is using up to 6 digits scale ID
				{
				//	RecvBufPosn +=j;  
					if(*(ptr+j)!=',')
					{
						temp_buff[j]=*(ptr+j);
					}
					else break;
				}	
				RecvValue=atoi(temp_buff);								
			}else break;
			
			if(RecvValue > 0) //Changed on 7 Jan2016 by DK 
			{
				Cur_Id=RecvValue;
				if(Cur_Id !=Prev_Id)
				{	
					Modbus_Tcp_Slaves[Total_Scales].Scale_ID = RecvValue;
					Total_Scales+=1;
				}	
			}else break;	
										
			if(Cur_Id !=Prev_Id)
			{	
				Prev_Id = Cur_Id;
				RecvValue=0;
				if(0 !=(ptr = HTTPStrCaseStr( http_ptr,sizeof(Res_Header_5),Res_Header_5)))// "IpAddress" string								
				{
					ptr+=(sizeof(Res_Header_5) + 2);
				  http_ptr = ptr; //added by DK on 8 Dec 2014 	
					RecvBufPosn += (sizeof(Res_Header_5) + 2);
					memset(temp_buff,'\0',sizeof(temp_buff));
					for(j=0;j<20;j++)
					{
						if((*(ptr+j)!='"') && (*(ptr+j)!=','))
						{
							temp_buff[j]=*(ptr+j);
						}
						else break;
					}	
					if((inet_aton(temp_buff, &address)) && Total_Scales > 0)
					{	
						Modbus_Tcp_Slaves[Total_Scales -1].IP_ADRESS[0]= ((address >> 24) & 0x000000FF); 
						Modbus_Tcp_Slaves[Total_Scales -1].IP_ADRESS[1]= ((address >> 16) & 0x000000FF);  
						Modbus_Tcp_Slaves[Total_Scales -1].IP_ADRESS[2]= ((address >> 8) & 0x000000FF);  
						Modbus_Tcp_Slaves[Total_Scales -1].IP_ADRESS[3]= (address  & 0x000000FF);  								
					}// else break;	 //Remove this comment in final firmware 
				}	//end of if(0 !=(ptr = HTTPStrCaseStr( (arcHttpClientRcvBuffer + RecvBufPosn),sizeof(Res_Header_5),Res_Header_5)))// "IpAddress:" string								
				
				RecvValue=0;
				if(0 !=(ptr = HTTPStrCaseStr( http_ptr ,sizeof(Res_Header_6),Res_Header_6)))// "ModbusSlaveAddress" string								
				{
					ptr+=(sizeof(Res_Header_6) + 2);
					http_ptr = ptr; //added by DK on 8 Dec 2014 	
					RecvBufPosn += (sizeof(Res_Header_6) + 1 );
					memset(temp_buff,'\0',sizeof(temp_buff));
					for(j=0;j<3;j++)
					{
						if((*(ptr+j)!=',') && (*(ptr+j)!='}'))//Changed by DK on 13 Jan2015 for ppp testing 
						{
							temp_buff[j]=*(ptr+j);
						}
						else break;
					}	
					RecvValue=atoi(temp_buff);								
				}else break;

				if(RecvValue > 0 && RecvValue < 255)
				{
					Modbus_Tcp_Slaves[Total_Scales - 1].Slave_ID = RecvValue;
				}else break;	
			
			} //end of if(Cur_Id !=Prev_Id)
			
			if(Total_Scales > (MAX_NO_OF_INTEGRATORS -1))//Changed by DK on 23 Feb 2015 	
			break;	
		}	//end of while(1) //for(i=0; i <TotalNoOfScales; i++)
		
		if((Total_Scales > 0) && (Total_Scales < MAX_NO_OF_INTEGRATORS)) //Changed by DK on 23 Feb 2015 	
		{
			ret_val=TRUE;
		}
	} //end of if((RecvValue == PCM_ID) && (0!=(http_ptr = HTTPStrCaseStr(arcHttpClientRcvBuffer,sizeof(Res_Header_3),Res_Header_3)))) 	
	return ret_val;
}	


BOOL Http_Post_Request(U8 pcs_sock_num, U16 ScaleNum, char* ptr) //Added by DK on 2 Jan 2015 
{
	char *http_ptr; //Added by DK on 2 Jan 2015 
	char temp_buff[40];
  U16 maxlen,time_out_counter; //added by DK on 30 sep 2015 
	U8* sendbuf; //added by DK on 30 sep 2015 
	U16 length,http_content_length,http_header_length,i;
	BOOL ret_val=FALSE;
	INT32_DATA itemp1,itemp2;
	FLOAT_UNION temp;
	DOUBLE_UNION temp3;
	LONG_LONG_UNION temp4;
	SINT32_UNION temp5;
	strcpy(ptr,"POST ");
	strcat(ptr,Http_Host_Name);
	strcat(ptr,"/Measurement HTTP/1.1\r\n");
	strcat(ptr,"Host: ");
	strcat(ptr,&Http_Host_Name[7]);
	strcat(ptr,"\r\n");
	strcat(ptr,&Req_Header_3[0]);
	strcat(ptr,&Req_Header_4[0]);
	strcat(ptr,&Req_Header_5[0]);
	strcat(ptr,&Req_Header_6[0]);
	strcat(ptr,&Req_Header_7[0]);
	strcat(ptr,&Req_Header_8[0]);
	strcat(ptr,&Req_Header_9[0]);
	
	http_header_length = strlen(ptr);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"ScaleId=%d",Modbus_Tcp_Slaves[ScaleNum].Scale_ID); //Changed on 7 Jan2016 by DK 
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&Function=%d",Scale_Record_Mb_Reg[ScaleNum].Function_Code.uint_val);
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&RecordNumber=%d",Scale_Record_Mb_Reg[ScaleNum].Record_Hash.uint_val);
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&Date=%04d-%02d-%02d",Scale_Record_Mb_Reg[ScaleNum].Scale_Date.int_array[1],
																		 (Scale_Record_Mb_Reg[ScaleNum].Scale_Date.int_array[0] & 0x00FF),
																		 (Scale_Record_Mb_Reg[ScaleNum].Scale_Date.int_array[0] >> 8 )); 
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&Time=%02d:%02d:%02d",(Scale_Record_Mb_Reg[ScaleNum].Scale_Time.int_array[1] & 0x00FF),
																		 (Scale_Record_Mb_Reg[ScaleNum].Scale_Time.int_array[0] & 0x00FF ),
																			(Scale_Record_Mb_Reg[ScaleNum].Scale_Time.int_array[0] >> 8));
	itemp1.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].acc_wt.s_array[1];
	itemp1.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].acc_wt.s_array[0];
	itemp2.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].acc_wt.s_array[3];
	itemp2.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].acc_wt.s_array[2];
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&AccumulatedWeight=%d.%06d",(int)itemp1.value,(int)itemp2.value);
//	sprintf(temp_buff,"&AccumulatedWeight=%f",f_accum_wt_tmp);

	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&WeightUnit=%d",Scale_Record_Mb_Reg[ScaleNum].accu_wt_unit.uint_val);
	strcat(ptr,&temp_buff[0]);
	temp.f_array[0]=Scale_Record_Mb_Reg[ScaleNum].scale_rate.f_array[1];
	temp.f_array[1]=Scale_Record_Mb_Reg[ScaleNum].scale_rate.f_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&Rate=%f",temp.float_val);
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&RateUnit=%d",Scale_Record_Mb_Reg[ScaleNum].rate_time_unit.uint_val);
	strcat(ptr,&temp_buff[0]);
	temp.f_array[0]=Scale_Record_Mb_Reg[ScaleNum].scale_load_perc.f_array[1];
	temp.f_array[1]=Scale_Record_Mb_Reg[ScaleNum].scale_load_perc.f_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&LoadPercent=%f",temp.float_val);
	strcat(ptr,&temp_buff[0]);
	temp.f_array[0]=Scale_Record_Mb_Reg[ScaleNum].scale_angle.f_array[1];
	temp.f_array[1]=Scale_Record_Mb_Reg[ScaleNum].scale_angle.f_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&Angle=%f",temp.float_val);
	strcat(ptr,&temp_buff[0]);
	temp.f_array[0]=Scale_Record_Mb_Reg[ScaleNum].belt_speed.f_array[1];
	temp.f_array[1]=Scale_Record_Mb_Reg[ScaleNum].belt_speed.f_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&BeltSpeed=%f",temp.float_val);
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&SpeedUnit=%d",Scale_Record_Mb_Reg[ScaleNum].belt_speed_unit.uint_val);
	strcat(ptr,&temp_buff[0]);
	itemp1.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].daily_wt.s_array[1];
	itemp1.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].daily_wt.s_array[0];
	itemp2.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].daily_wt.s_array[3];
	itemp2.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].daily_wt.s_array[2];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&DailyWeight=%d.%06d",(int)itemp1.value,(int)itemp2.value);
	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&DailyWeightUnit=%d",Scale_Record_Mb_Reg[ScaleNum].accu_wt_unit.uint_val);
	strcat(ptr,&temp_buff[0]);
	//by megha on 31/8/2017 for addition of master total,daily total,zero number,speed feet/min,rate pounds/hour
	temp4.ulong_array[3]=Scale_Record_Mb_Reg[ScaleNum].master_total.ulong_array[3];
	temp4.ulong_array[2]=Scale_Record_Mb_Reg[ScaleNum].master_total.ulong_array[2];
	temp4.ulong_array[1]=Scale_Record_Mb_Reg[ScaleNum].master_total.ulong_array[1];
	temp4.ulong_array[0]=Scale_Record_Mb_Reg[ScaleNum].master_total.ulong_array[0];
//	strcat(ptr,&temp_buff[0]);
//	f_accum_wt_tmp = temp4.double_val;
	memset(temp_buff,'\0',sizeof(temp_buff));
//	sprintf(temp_buff,"&MasterTotal=%d.%06d",(int)itemp1.value,(int)itemp2.value);
	sprintf(temp_buff,"&MasterTotal=%lld",temp4.ulong_val);

	strcat(ptr,&temp_buff[0]);
	
	temp4.ulong_array[3]=Scale_Record_Mb_Reg[ScaleNum].daily_total.ulong_array[3];
	temp4.ulong_array[2]=Scale_Record_Mb_Reg[ScaleNum].daily_total.ulong_array[2];
	temp4.ulong_array[1]=Scale_Record_Mb_Reg[ScaleNum].daily_total.ulong_array[1];
	temp4.ulong_array[0]=Scale_Record_Mb_Reg[ScaleNum].daily_total.ulong_array[0];
//	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&DailyTotal=%lld",temp4.ulong_val);
//	sprintf(temp_buff,"&DailyTotal=%lf",Scale_Record_Mb_Reg[ScaleNum].daily_total.double_val);
	
	strcat(ptr,&temp_buff[0]);	

	temp5.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].zero_number.int_array[1];
	temp5.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].zero_number.int_array[0];
	
// 	temp4.ulong_array[3]=Scale_Record_Mb_Reg[ScaleNum].zero_number.ulong_array[3];
// 	temp4.ulong_array[2]=Scale_Record_Mb_Reg[ScaleNum].zero_number.ulong_array[2];
// 	temp4.ulong_array[1]=Scale_Record_Mb_Reg[ScaleNum].zero_number.ulong_array[1];
// 	temp4.ulong_array[0]=Scale_Record_Mb_Reg[ScaleNum].zero_number.ulong_array[0];
//	strcat(ptr,&temp_buff[0]);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&ZeroNumber=%d",temp5.int_val);
//	sprintf(temp_buff,"&ZeroNumber=%lf",Scale_Record_Mb_Reg[ScaleNum].zero_number.double_val);

	strcat(ptr,&temp_buff[0]);

	temp5.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].speed_feet_min.int_array[1];
	temp5.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].speed_feet_min.int_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&SpeedFeetPerHour=%d",temp5.int_val);  
	strcat(ptr,&temp_buff[0]);
	
// 	itemp1.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[1];
// 	itemp1.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[0];	
// 	memset(temp_buff,'\0',sizeof(temp_buff));
// 	sprintf(temp_buff,"&InstRatePoundsHour=%d",itemp1.value); 
// 	strcat(ptr,&temp_buff[0]);	

 	temp4.ulong_array[3]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.ulong_array[3];
 	temp4.ulong_array[2]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.ulong_array[2];
 	temp4.ulong_array[1]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.ulong_array[1];
 	temp4.ulong_array[0]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.ulong_array[0];
//	temp5.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[1];
//	temp5.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&InstRatePoundsHour=%lld",temp4.ulong_val);
	strcat(ptr,&temp_buff[0]);

	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&USBHealthStatus=%d",Scale_Record_Mb_Reg[ScaleNum].scale_usb_health.uint_val);
	strcat(ptr,&temp_buff[0]);

 	temp4.ulong_array[3]=Scale_Record_Mb_Reg[ScaleNum].job_total_pounds.ulong_array[3];
 	temp4.ulong_array[2]=Scale_Record_Mb_Reg[ScaleNum].job_total_pounds.ulong_array[2];
 	temp4.ulong_array[1]=Scale_Record_Mb_Reg[ScaleNum].job_total_pounds.ulong_array[1];
 	temp4.ulong_array[0]=Scale_Record_Mb_Reg[ScaleNum].job_total_pounds.ulong_array[0];
//	temp5.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[1];
//	temp5.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&JobTotalPounds=%lld",temp4.ulong_val);
	strcat(ptr,&temp_buff[0]);
	
 	temp4.ulong_array[3]=Scale_Record_Mb_Reg[ScaleNum].accumulated_weight_pounds.ulong_array[3];
 	temp4.ulong_array[2]=Scale_Record_Mb_Reg[ScaleNum].accumulated_weight_pounds.ulong_array[2];
 	temp4.ulong_array[1]=Scale_Record_Mb_Reg[ScaleNum].accumulated_weight_pounds.ulong_array[1];
 	temp4.ulong_array[0]=Scale_Record_Mb_Reg[ScaleNum].accumulated_weight_pounds.ulong_array[0];
//	temp5.int_array[0]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[1];
//	temp5.int_array[1]=Scale_Record_Mb_Reg[ScaleNum].inst_rate_pounds_hour.int_array[0];
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&AccumulatedWeightPounds=%lld",temp4.ulong_val);
	strcat(ptr,&temp_buff[0]);	

	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&RecorID=%ld",Scale_Record_Mb_Reg[ScaleNum].Record_id.value); 
	strcat(ptr,&temp_buff[0]);
	
	length= strlen(ptr);
	http_content_length = length - http_header_length;
	if(http_content_length != 213)
	{
		if(0!= (http_ptr = HTTPStrCaseStr(ptr,15,"Content-Length:")))// "Content-Length:" string	
		{
			http_ptr+=16;
			memset(temp_buff,'\0',sizeof(temp_buff));
			sprintf(temp_buff, "%d",http_content_length);
			for(i=0;i<3;i++)
			{
				if(http_ptr[i] == '\r') break;
				http_ptr[i] = temp_buff[i];
			}	
		}
	}		
	//Send packet to PCS
	//Commented by DK on 30 Sep 2015 
	/*
	ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,length);
	return ret_val;
	*/
	//Added by DK on 30 sep 2015
  time_out_counter = 0; 	
	i = 0; 
	while(1)
	{
		if(tcp_check_send(pcs_sock_num))
		{	
			//The socket is ready to send the data
			maxlen=tcp_max_dsize(pcs_sock_num);
			if(maxlen >= length )
			{
				sendbuf = tcp_get_buf (length);
				memcpy (sendbuf, (ptr + i),length);
				ret_val = tcp_send (pcs_sock_num, sendbuf, length);
				main_TcpNet();			 
				break;
			}
			else
			{
				sendbuf = tcp_get_buf (maxlen);
			  memcpy (sendbuf, (ptr + i),maxlen);
			  ret_val = tcp_send (pcs_sock_num, sendbuf, maxlen);
				main_TcpNet();			
				if(ret_val)
				{
					length -= maxlen;
					i += maxlen; 
					
					if(time_out_counter > 0)
					time_out_counter--;
				}
			}	
		}
		os_dly_wait(100); //100 ms 
		if(time_out_counter++ > 30) //3 second timeout
		{
			ret_val = FALSE;
			break;
    }			
	}	//end of while(1)
	return ret_val;
}

BOOL Http_Post_File(U8 pcs_sock_num, U16 ScaleNum,unsigned int FileAvailable) 
{
	char *ptr,*http_ptr;
	char temp_buff[90];
	int file_size; 
  U32 http_length,length,bytes_to_read,i,file_offset,maxlen,time_out_counter;
	BOOL ret_val=FALSE;
	FILE *fp;
	
	memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); //added by DK on 16 Dec 2014 
	ptr = &HTTP_TX_Buffer[0]; 
	//end  point is "POST http://beltwayapi.jwrob.com/Measurement/file HTTP/1.1\r\n";  
	strcpy(ptr,"POST ");
	strcat(ptr,Http_Host_Name);
	strcat(ptr,"/Measurement/file HTTP/1.1\r\n");
	strcat(ptr,"Host: ");
	strcat(ptr,&Http_Host_Name[7]);
	strcat(ptr,"\r\n");
	strcat(ptr,&Req_Header_3[0]);  //ApiKey:	LStgRGV2yyCBib8E3t8z\r\n
	strcat(ptr,&Req_Header_5[0]);  //"Cache-Control: no-cache\r\n";
	strcat(ptr,&Req_Header_6[0]);  //"postman token";
	strcat(ptr,&File_Req_Header_4[0]); //"Content-Length: 123456\r\n";
	strcat(ptr,&File_Req_Header_1[0]); //"Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW\r\n";
	strcat(ptr,&File_Req_Header_3[0]); //"Accept: */*\r\n\r\n";

	length = strlen(ptr);
	strcat(ptr,&File_Req_Header_2[0]);  //------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
	strcat(ptr,&File_Req_Header_5[0]); //	Content-Disposition: form-data; name=	
	memset(temp_buff,'\0',sizeof(temp_buff));
	temp_buff[0]='"';
	memcpy(&temp_buff[1],File_Req_Header_6,sizeof(File_Req_Header_6)); //FileDate
	strcat(ptr,temp_buff);
	memset(temp_buff,'\0',sizeof(temp_buff));
	temp_buff[0]='"';
	strcat(ptr,temp_buff);
	strcat(ptr,"\r\n");
	strcat(ptr,"\r\n");
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"%04d-%02d-%02d",File_request_data.file_year,File_request_data.file_month,\
	        File_request_data.file_day); //Added by DK on 11 Feb2015 
	strcat(ptr,&temp_buff[0]);
	strcat(ptr,"\r\n");
	strcat(ptr,&File_Req_Header_2[0]);  //------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
	strcat(ptr,&File_Req_Header_5[0]); //	Content-Disposition: form-data; name=	
	memset(temp_buff,'\0',sizeof(temp_buff));
	temp_buff[0]='"';
	memcpy(&temp_buff[1],File_Req_Header_7,sizeof(File_Req_Header_7)); //ScaleId
	strcat(ptr,temp_buff);
	memset(temp_buff,'\0',sizeof(temp_buff));
	temp_buff[0]='"';
	strcat(ptr,temp_buff);
	strcat(ptr,"\r\n");
	strcat(ptr,"\r\n");
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"%d",Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
	strcat(ptr,temp_buff);
	strcat(ptr,"\r\n");
	strcat(ptr,&File_Req_Header_2[0]);  //------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
	strcat(ptr,&File_Req_Header_5[0]); //	Content-Disposition: form-data; name=	
	memset(temp_buff,'\0',sizeof(temp_buff));
	temp_buff[0]='"';
	memcpy(&temp_buff[1],File_Req_Header_8,sizeof(File_Req_Header_8)); //FileStatus
	strcat(ptr,temp_buff);
	memset(temp_buff,'\0',sizeof(temp_buff));
	temp_buff[0]='"';
	strcat(ptr,temp_buff);
	strcat(ptr,"\r\n");  //Added by DK on 5 Feb 2015 
	strcat(ptr,"\r\n");  //Added by DK on 5 Feb 2015 
	
	if(FileAvailable == 1)
	strcat(ptr,&File_Req_Header_12[0]); //	FileIncluded 	 //Added by DK on 5 Feb 2015 
	else if(FileAvailable == 0) 
	strcat(ptr,&File_Req_Header_13[0]); //	FileNotPresent	
	else if(FileAvailable == 3) 
	strcat(ptr,&File_Req_Header_15[0]); //	invalid scale id in file request//added on 30/11/2017 by megha		
	else
	{
		strcat(ptr,&File_Req_Header_14[0]); //	ScaleNotAvailable 		
		FileAvailable = 0;
	}
	if(FileAvailable == 1)
	{	
		strcat(ptr,&File_Req_Header_2[0]);  //------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n  //Added by DK on 5 Feb 2015 
		strcat(ptr,&File_Req_Header_5[0]); //	Content-Disposition: form-data; name=	 //Added by DK on 5 Feb 2015 
		memset(temp_buff,'\0',sizeof(temp_buff));  //Added by DK on 5 Feb 2015 
		temp_buff[0]='"';  //Added by DK on 5 Feb 2015 
		strcpy(&temp_buff[1],"TheFile"); //Added by DK on 5 Feb 2015 
		strcat(ptr,temp_buff);  //Added by DK on 5 Feb 2015 
		memset(temp_buff,'\0',sizeof(temp_buff));  //Added by DK on 5 Feb 2015 
		temp_buff[0]='"';  //Added by DK on 5 Feb 2015 
		temp_buff[1]=';';  //Added by DK on 5 Feb 2015 
		temp_buff[2]=' ';  //Added by DK on 5 Feb 2015 
		strcat(ptr,temp_buff);  //Added by DK on 5 Feb 2015 
		strcat(ptr,&File_Req_Header_9[0]); //	filename=
		memset(temp_buff,'\0',sizeof(temp_buff));
		temp_buff[0]='"';
		strcat(ptr,temp_buff);
		memset(temp_buff,'\0',sizeof(temp_buff));
		//Added by DK on 11 Feb2015 
		sprintf(temp_buff,"%04d-%02d-%02d_periodic_log.txt",File_request_data.file_year,\
		File_request_data.file_month,File_request_data.file_day);		//Suvrat Folder structure
		strcat(ptr,temp_buff);
		memset(temp_buff,'\0',sizeof(temp_buff));
		temp_buff[0]='"';
		strcat(ptr,temp_buff);
		strcat(ptr,"\r\n");
		strcat(ptr,&File_Req_Header_10[0]); //	Content-Type:  text/plain\r\n\r\n
		http_length = strlen(ptr);
		
		os_mut_wait (&usb_log_mutex, 0xffff);	
		sprintf(&system_log.log_buf[0],"Uploading file  %s",temp_buff);
		log_file_write(system_log1);
		os_mut_release (&usb_log_mutex);
}
	file_size = 0;
	bytes_to_read  =0;
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff, "Scale_%d\\%04d-%02d-%02d.txt",File_request_data.Scale_ID,File_request_data.file_year,\
			File_request_data.file_month,File_request_data.file_day);



	fp = fopen (temp_buff, "r");
	file_offset=0;
	if(fp != NULL)
	{
		if(!fseek(fp,file_offset,SEEK_END))
		{
			file_size = ftell(fp);
		}	
		fclose(fp);
		#ifdef DEBUG_PORT
		os_mut_wait (&debug_uart_mutex, 0xffff);		
		memset(Debug_Buf,'\0',sizeof(Debug_Buf));
		sprintf(Debug_Buf, "HTTP_POST_FILE file size %d of scale ID%d\r\n",file_size,Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
		Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
		memset(Debug_Buf,'\0',sizeof(Debug_Buf));
		sprintf(Debug_Buf, "HTTP_POST_FILE Header Length %d\r\n",http_length);
		Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
		os_mut_release (&debug_uart_mutex);
		#endif
		
		if(0!= (http_ptr = HTTPStrCaseStr(ptr,15,"Content-Length:")))// "Content-Length:" string	
		{
				http_ptr+=16;
				memset(temp_buff,'\0',sizeof(temp_buff));
				sprintf(temp_buff, "%6d",((http_length - length) + file_size + (sizeof(File_Req_Header_11) - 1)));
				for(i=0;i<6;i++)
				{
					if((http_ptr[i] == '\r') || (http_ptr[i] == '\n'))break;
					else http_ptr[i] = temp_buff[i];
				}	
		}		
	}	
	http_length = strlen(ptr);

  if((FileAvailable == 0)||((FileAvailable == 2))||((FileAvailable == 3)))  //Added by DK on 5 Feb 2015 
	{
		if(FileAvailable==0)
		{
			memset(temp_buff,'\0',sizeof(temp_buff));
			sprintf(temp_buff, "Scale_%d\\%04d-%02d-%02d.txt",File_request_data.Scale_ID,File_request_data.file_year,\
					File_request_data.file_month,File_request_data.file_day);			
		os_mut_wait (&usb_log_mutex, 0xffff);	
		sprintf(&system_log.log_buf[0],"File not available for  %s",temp_buff);
		log_file_write(system_log1);
		os_mut_release (&usb_log_mutex);
		}
		if(FileAvailable==2)
		{
			memset(temp_buff,'\0',sizeof(temp_buff));
			sprintf(temp_buff, "Scale_%d",Modbus_Tcp_Slaves[ScaleNum].Scale_ID);				
		os_mut_wait (&usb_log_mutex, 0xffff);	
		sprintf(&system_log.log_buf[0],"%s is disconnected ",temp_buff);
		log_file_write(system_log1);
		os_mut_release (&usb_log_mutex);
		}
		if(FileAvailable==3)
		{
			memset(temp_buff,'\0',sizeof(temp_buff));
			sprintf(temp_buff, "Scale_%d\\%04d-%02d-%02d.txt",File_request_data.Scale_ID,File_request_data.file_year,\
					File_request_data.file_month,File_request_data.file_day);					
		os_mut_wait (&usb_log_mutex, 0xffff);	
		sprintf(&system_log.log_buf[0],"Invalid Scale ID for file request %s",temp_buff);
		log_file_write(system_log1);
		os_mut_release (&usb_log_mutex);
		}
		
		
		file_size = 0;
		if(0!= (http_ptr = HTTPStrCaseStr(ptr,15,"Content-Length:")))// "Content-Length:" string	
		{
				http_ptr+=16;
				memset(temp_buff,'\0',sizeof(temp_buff));
				sprintf(temp_buff, "%6d",((http_length - length) + file_size + (sizeof(File_Req_Header_11) - 1)));
				for(i=0;i<6;i++)
				{
					if((http_ptr[i] == '\r') || (http_ptr[i] == '\n'))break;
					else http_ptr[i] = temp_buff[i];
				}	
		}
		strcat(ptr,&File_Req_Header_11[0]); //------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n
		http_length=strlen(ptr);
		if(tcp_check_send(pcs_sock_num))
			ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);	
		
		#ifdef DEBUG_PORT
		os_mut_wait (&debug_uart_mutex, 0xffff);		
		memset(Debug_Buf,'\0',sizeof(Debug_Buf));
		sprintf(Debug_Buf, "HTTP_POST_FILE file not present response sending for scale ID %d \r\n",Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
		Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
		os_mut_release (&debug_uart_mutex);
		#endif
		
		return ret_val;
	}
//   if(FileAvailable == 2)  //Added by megha on 30/11/2017
// 	{
// 		file_size = 0;
// 		if(0!= (http_ptr = HTTPStrCaseStr(ptr,15,"Content-Length:")))// "Content-Length:" string	
// 		{
// 				http_ptr+=16;
// 				memset(temp_buff,'\0',sizeof(temp_buff));
// 				sprintf(temp_buff, "%6d",((http_length - length) + file_size + (sizeof(File_Req_Header_11) - 1)));
// 				for(i=0;i<6;i++)
// 				{
// 					if((http_ptr[i] == '\r') || (http_ptr[i] == '\n'))break;
// 					else http_ptr[i] = temp_buff[i];
// 				}	
// 		}
// 		strcat(ptr,&File_Req_Header_11[0]); //------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n
// 		http_length=strlen(ptr);
// 		if(tcp_check_send(pcs_sock_num))
// 			ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);	
// 		
// 		#ifdef DEBUG_PORT
// 		os_mut_wait (&debug_uart_mutex, 0xffff);		
// 		memset(Debug_Buf,'\0',sizeof(Debug_Buf));
// 		sprintf(Debug_Buf, "HTTP_POST_FILE Scale not available response sending for scale ID %d \r\n",Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
// 		Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
// 		os_mut_release (&debug_uart_mutex);
// 		#endif
// 		
// 		return ret_val;
// 	}		
//   if(FileAvailable == 3)  //Added by megha on 30/11/2017
// 	{
// 		file_size = 0;
// 		if(0!= (http_ptr = HTTPStrCaseStr(ptr,15,"Content-Length:")))// "Content-Length:" string	
// 		{
// 				http_ptr+=16;
// 				memset(temp_buff,'\0',sizeof(temp_buff));
// 				sprintf(temp_buff, "%6d",((http_length - length) + file_size + (sizeof(File_Req_Header_11) - 1)));
// 				for(i=0;i<6;i++)
// 				{
// 					if((http_ptr[i] == '\r') || (http_ptr[i] == '\n'))break;
// 					else http_ptr[i] = temp_buff[i];
// 				}	
// 		}
// 		strcat(ptr,&File_Req_Header_11[0]); //------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n
// 		http_length=strlen(ptr);
// 		if(tcp_check_send(pcs_sock_num))
// 			ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);	
// 		
// 		#ifdef DEBUG_PORT
// 		os_mut_wait (&debug_uart_mutex, 0xffff);		
// 		memset(Debug_Buf,'\0',sizeof(Debug_Buf));
// 		sprintf(Debug_Buf, "HTTP_POST_FILE Invalid Scale ID response sending for scale ID %d \r\n",Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
// 		Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
// 		os_mut_release (&debug_uart_mutex);
// 		#endif
// 		
// 		return ret_val;
// 	}	
	file_size = 0;
	bytes_to_read  =0;
	memset(temp_buff,'\0',sizeof(temp_buff));
//	sprintf(temp_buff, "Scale_%d.txt",Modbus_Tcp_Slaves[ScaleNum].Slave_ID);
	sprintf(temp_buff, "Scale_%d\\%04d-%02d-%02d.txt",File_request_data.Scale_ID,File_request_data.file_year,\
			File_request_data.file_month,File_request_data.file_day);
	fp = fopen (temp_buff, "r");
	file_offset=0;
	if(fp != NULL)
	{
		if(!fseek(fp,file_offset,SEEK_END))
		{
			file_size = ftell(fp);
		}
		
		time_out_counter=0;
		while(1)
		{	
			i = (sizeof(HTTP_TX_Buffer)- (http_length + sizeof(File_Req_Header_11)));
			file_offset =0;
			fseek(fp,file_offset,SEEK_SET);
			if(file_size > i)
			{
				bytes_to_read = fread(&HTTP_TX_Buffer[http_length],1,i,fp);		
//				strcat(ptr,&File_Req_Header_11[0]); //------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n//added by megha on 2/12/2017
				http_length = strlen(ptr);	
			//Send packet to PCS
				/*#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "HTTP_POST_FILE sending packet1 of size %d\r\n",http_length);
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif*/
				ret_val =FALSE;
				if(tcp_check_send(pcs_sock_num))
				ret_val = FALSE;
				ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);	
				
				if(ret_val == TRUE)
				{
					file_offset +=(bytes_to_read );
					file_size -=(bytes_to_read );
					fseek(fp,file_offset,SEEK_SET);
				
					/*#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "HTTP_POST_FILE sent packet1 of size %d\r\n",http_length);
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif*/
//					if(ppp_enable == 0)
					if(Pcm_var.PCM_connection_select == Screen213_str1)
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,2000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}
					if((Pcm_var.PCM_connection_select == Screen213_str5)||((Pcm_var.PCM_connection_select == Screen213_str3)))
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,5000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}					
// 					else if(Pcm_var.PCM_connection_select == Screen213_str3)
// 					{
// 						os_evt_wait_or(EVE_PPP_ACK,5000);
// 						os_evt_clr(EVE_PPP_ACK,t_ppp_client);
// 					}	
					break;
				}
				else //Added by DK on 30 January 2015 
				{
					ret_val=FALSE;
					fclose(fp);
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "Returning 1 from POST File of scale ID %d\r\n",Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					return ret_val;
				}	
			}	//end of if(file_size > i)
			else if(tcp_check_send(pcs_sock_num))
			{
				fread(&HTTP_TX_Buffer[http_length],1,file_size,fp);			
				strcat(ptr,&File_Req_Header_11[0]); //------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n
				http_length = strlen(ptr);	
		
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "HTTP_POST_FILE sending only one packet of size %d of scale ID%d\r\n",http_length,Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
			
				//Send packet to PCS
				ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);	
				if(ret_val == TRUE)
				{
					file_offset +=(bytes_to_read );
					file_size =0;
					fseek(fp,file_offset,SEEK_SET);
				
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "HTTP_POST_FILE sent with only one packet of size %d of Scale ID%d\r\n",http_length,Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
				
// 					if(ppp_enable == 0)
// 					{	
// 						os_evt_wait_or(EVE_HTTP_ACK,2000);
// 						os_evt_clr(EVE_HTTP_ACK,t_http_client);
// 					}
// 					else 
// 					{
// 						os_evt_wait_or(EVE_PPP_ACK,5000);
// 						os_evt_clr(EVE_PPP_ACK,t_ppp_client);
// 					}
					if(Pcm_var.PCM_connection_select == Screen213_str1)
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,2000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}
					if((Pcm_var.PCM_connection_select == Screen213_str5)||((Pcm_var.PCM_connection_select == Screen213_str3)))
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,5000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}					
// 					else if(Pcm_var.PCM_connection_select == Screen213_str3)
// 					{
// 						os_evt_wait_or(EVE_PPP_ACK,5000);
// 						os_evt_clr(EVE_PPP_ACK,t_ppp_client);
// 					}	
				
					fclose(fp); //Added by DK on 2 Feb 2015 
					return ret_val; //Added by DK on 2 Feb 2015 
				}
				else //Added by DK on 30 January 2015 
				{
					ret_val=FALSE;
					fclose(fp);
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "Returning 2 from POST File\r\n");
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					return ret_val;
				}
			}
			else
			{
				os_dly_wait(10);			//100 by megha for delay of updating records previous 
				if(time_out_counter++ > 20)
				{
					ret_val=FALSE;
					fclose(fp);
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "Returning 3 from POST File\r\n");
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					return ret_val;
				}
			}	
		} //end of while(1)//this was for 1st packet which includes header. packet = (header+ file data)
		
  	i=1;
		time_out_counter=0;
		while(1)//this loop is for middle packets which does not includes data   ..packet =  file data only.
		{	
			ret_val=FALSE;
			if((tcp_check_send(pcs_sock_num)) && (file_size > 0))
			{	
				fseek(fp,file_offset,SEEK_SET);
				memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); 
				ptr = &HTTP_TX_Buffer[0]; 
				maxlen = tcp_max_dsize (pcs_sock_num);
				bytes_to_read = fread(ptr,1,maxlen - 4,fp);
				http_length = strlen(ptr);
				ret_val = tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);	

				if(ret_val == TRUE)
				{	
					i++;
					if(bytes_to_read == (maxlen - 4))//(bytes_to_read == 1 )
					{
						file_offset +=(maxlen - 4);
						file_size -= (maxlen - 4);	
					}
					else
					{
						file_offset +=http_length;
						file_size -=http_length;	
					}
          
				/*	#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "file size:%d offset:%d bytes-read:%d\r\n",file_size,file_offset,maxlen-4);
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif	
					
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "HTTP_POST_FILE sent packet %d of size %d\r\n",i,http_length);
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif*/
					
// 					if(ppp_enable == 0)
// 					{	
// 						os_evt_wait_or(EVE_HTTP_ACK,2000);
// 						os_evt_clr(EVE_HTTP_ACK,t_http_client);
// 					}
// 					else 
// 					{
// 						os_evt_wait_or(EVE_PPP_ACK,5000);
// 						os_evt_clr(EVE_PPP_ACK,t_ppp_client);
// 					}
					if(Pcm_var.PCM_connection_select == Screen213_str1)
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,2000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}
					if((Pcm_var.PCM_connection_select == Screen213_str5)||((Pcm_var.PCM_connection_select == Screen213_str3)))
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,5000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}					
// 					else if(Pcm_var.PCM_connection_select == Screen213_str3)
// 					{
// 						os_evt_wait_or(EVE_PPP_ACK,5000);
// 						os_evt_clr(EVE_PPP_ACK,t_ppp_client);
// 					}	
					
					if(time_out_counter > 0)
					time_out_counter--;
					
				}//end of if(ret_val == TRUE)
				else //Added by DK on 30 January 2015 
				{
					ret_val=FALSE;
					fclose(fp);
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "Returning 4 from POST File\r\n");
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					return ret_val;
				}
			} //end of if((tcp_check_send(pcs_sock_num)) && (file_size > 0))
			else if((file_size <= 0 ) && (tcp_check_send(pcs_sock_num)))
			{
				memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); 
				ptr = &HTTP_TX_Buffer[0]; 
				strcat(ptr, "\r\n");
				strcat(ptr,File_Req_Header_11); //------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n
				http_length = strlen(ptr);
				ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);	
				
				if(ret_val == TRUE)
				{
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "HTTP_POST_FILE sent last packet of Scale ID%d\r\n",Modbus_Tcp_Slaves[ScaleNum].Scale_ID);
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					
// 					if(ppp_enable == 0)
// 					{	
// 						os_evt_wait_or(EVE_HTTP_ACK,2000);
// 						os_evt_clr(EVE_HTTP_ACK,t_http_client);
// 					}
// 					else 
// 					{
// 						os_evt_wait_or(EVE_PPP_ACK,5000);
// 						os_evt_clr(EVE_PPP_ACK,t_ppp_client);
// 					}
					if(Pcm_var.PCM_connection_select == Screen213_str1)
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,2000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}
					if((Pcm_var.PCM_connection_select == Screen213_str5)||((Pcm_var.PCM_connection_select == Screen213_str3)))
					
					{	
						os_evt_wait_or(EVE_HTTP_ACK,5000);
						os_evt_clr(EVE_HTTP_ACK,t_http_client);
					}					
// 					else if(Pcm_var.PCM_connection_select == Screen213_str3)
// 					{
// 						os_evt_wait_or(EVE_PPP_ACK,5000);
// 						os_evt_clr(EVE_PPP_ACK,t_ppp_client);
// 					}						
					if(time_out_counter > 0)
					time_out_counter--;
					
					break;
				}
        else //Added by DK on 30 January 2015 
				{
					ret_val=FALSE;
					fclose(fp);
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "Returning 5 from POST File\r\n");
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					return ret_val;
				}				
			}
			else 
			{ 
				os_dly_wait(10);			//100 by megha for delay of updating records 
				if(time_out_counter++ > 20)
				{
					ret_val=FALSE;
					break;
				}	
			}	
		}	//end of while(1)
		fclose(fp);
	} //end of if(fp != NULL)
	else 
	{	
		strcat(ptr,&File_Req_Header_11[0]); //------WebKitFormBoundary7MA4YWxkTrZu0gW--\r\n
		http_length = strlen(ptr);
		if(0!= (http_ptr = HTTPStrCaseStr(ptr,15,"Content-Length:")))// "Content-Length:" string	
		{
			http_ptr+=16;
			memset(temp_buff,'\0',sizeof(temp_buff));
			sprintf(temp_buff, "%6d",(http_length - length));
			for(i=0;i<6;i++)
			{
				if((http_ptr[i] == '\r') || (http_ptr[i] == '\n'))break;
				else http_ptr[i] = temp_buff[i];
			}	
		}
		//Send packet to PCS
		ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,http_length);
	}
	
	return ret_val;
}

//Added by DK on 6 Feb 2015 for connectivity status update
BOOL Http_Post_Connectivity_Status(U8 pcs_sock_num, char* ptr) 
{
	char *http_ptr; 
	char temp_buff[60];
  U16 length,http_content_length,http_header_length,i;
	BOOL ret_val;
	U32 index;
	
	strcpy(ptr,"POST ");
	strcat(ptr,Http_Host_Name);
	strcat(ptr,"/Modem/");
  memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"%d",PCM_ID); //PCM ID 
	strcat(ptr,&temp_buff[0]);
	strcat(ptr,"/Status HTTP/1.1\r\n");
	strcat(ptr,"Host: ");
	strcat(ptr,&Http_Host_Name[7]);
	strcat(ptr,"\r\n");
	strcat(ptr,&Req_Header_3[0]);  //ApiKey: LStgRGV2yyCBib8E3t8z\r\n
	strcat(ptr,&Connectivity_Req_Header_4[0]); //Content-Length:
	strcat(ptr,&Req_Header_5[0]);  //Cache-Control: no-cache\r\n
	strcat(ptr,&Req_Header_8[0]);  //Content-Type: application/x-www-form-urlencoded
	strcat(ptr,"\r\n");  //\r\n
	//strcat(ptr,"\r\n");  //\r\n  
	http_header_length = strlen(ptr);

	//Request body 
  memset(temp_buff,'\0',sizeof(temp_buff));
  sprintf(temp_buff,"ConnectivityStatus=%d",pcs_connectivity_status); 
	strcat(ptr,temp_buff);
	memset(temp_buff,'\0',sizeof(temp_buff));
	temp_buff[0]='&';
	sprintf(&temp_buff[1],"ScaleStatusList=");
	strcat(ptr,temp_buff);
	for(index=0; index < Total_Scales; index++) //Changed by DK on 23 Feb 2015 	
	{
		if(Total_Scales > (MAX_NO_OF_INTEGRATORS -1))//Changed by DK on 23 Feb 2015 	
		break;	
		
		memset(temp_buff,'\0',sizeof(temp_buff));
		sprintf(temp_buff,"%d%%2C%d%%2C%02x.%02x",\
		                   Modbus_Tcp_Slaves[index].Scale_ID,\
		                   Modbus_Tcp_Slaves[index].scale_connectivity_status,\
		                   Modbus_Tcp_Slaves[index].fw_ver[0],\
		                   Modbus_Tcp_Slaves[index].fw_ver[1]
// 												Modbus_Tcp_Slaves[index].IP_ADRESS[3],
// 												Modbus_Tcp_Slaves[index].IP_ADRESS[2],
// 												Modbus_Tcp_Slaves[index].IP_ADRESS[1],
// 												Modbus_Tcp_Slaves[index].IP_ADRESS[0],
// 											 Scale_Record_Mb_Reg[index].Scale_Date.int_array[1],
// 		  								 (Scale_Record_Mb_Reg[index].Scale_Date.int_array[0] & 0x00FF),
// 											 (Scale_Record_Mb_Reg[index].Scale_Date.int_array[0] >> 8 ),
// 											(Scale_Record_Mb_Reg[index].Scale_Time.int_array[1] & 0x00FF),
// 											(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0] & 0x00FF ),
// 											(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0] >> 8)		
		);
		strcat(ptr,&temp_buff[0]);
		if(index != (Total_Scales -1)) //Changed by DK on 23 Feb 2015 	
		strcat(ptr,"%7C");	
	}	
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&CellModemSignal=%d",cell_modem_strength);
	strcat(ptr,temp_buff);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"&PcmVersion=%s",&FirmwareVer[1]);
	strcat(ptr,temp_buff);
	strcat(ptr,"\r\n");
	length= strlen(ptr);
	http_content_length = length - http_header_length;
	if(0!= (http_ptr = HTTPStrCaseStr(ptr,15,"Content-Length:")))// "Content-Length:" string	
	{
		http_ptr+=16;
		memset(temp_buff,'\0',sizeof(temp_buff));
		sprintf(temp_buff, "%d",http_content_length);
		if(http_content_length<100)
		{
			for(i=0;i<2;i++)
			{
				if(http_ptr[i] == '\r') break;
					http_ptr[i] = temp_buff[i];
			}				
		}
		else
		{
			for(i=0;i<3;i++)
			{
				if(http_ptr[i] == '\r') break;
					http_ptr[i] = temp_buff[i];
			}	
		}
	}
 //Send packet to PCS
	ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,length);
	return ret_val;
}

//by MSA on 6th jan 2017 for sending heartbeat msg to server at every specified time
BOOL Http_Post_Heartbeat_message(U8 pcs_sock_num, char* ptr) 
{
	char *http_ptr; 
	char temp_buff[60];
  U16 length,http_content_length,http_header_length,i;
	BOOL ret_val;
	U8 index;
	char line_break[2];
	unsigned int current_sec;

	strcpy(ptr,"POST ");
	strcat(ptr,Http_Host_Name);
	strcat(ptr,"/Measurement/HeartBeat?");
	strcat(ptr,"modemId=");	
  memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"%d",PCM_ID); //PCM ID 
	strcat(ptr,&temp_buff[0]);
	
	strcat(ptr,"&modemName=");	
  memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"BWS1"); //PCM ID 
	strcat(ptr,&temp_buff[0]);
	
	strcat(ptr,"&beatTime=");
  memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"%d",HEARTBEAT_COUNT); //PCM ID 
	strcat(ptr,&temp_buff[0]);
	strcat(ptr,"&method=");
	
	//Request body 
  memset(temp_buff,'\0',sizeof(temp_buff));
  sprintf(temp_buff,"Connectivity_status%d",pcs_connectivity_status); //ConnectivityStatus=
	strcat(ptr,temp_buff);
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(&temp_buff[0],",Total_scales%d,",Total_Scales);	//Total Scales Connected = 
	strcat(ptr,temp_buff);	
	memset(temp_buff,'\0',sizeof(temp_buff));

	for(index=0; index < Total_Scales; index++) //Changed by DK on 23 Feb 2015 	
	{
		if(Total_Scales > (MAX_NO_OF_INTEGRATORS -1))//Changed by DK on 23 Feb 2015 	
		break;	
		
		memset(temp_buff,'\0',sizeof(temp_buff));
		sprintf(temp_buff,"@@@Scale%d=",(index +1)); //scale index=
		strcat(ptr,&temp_buff[0]);
		
		memset(temp_buff,'\0',sizeof(temp_buff));
		sprintf(temp_buff,"%d,",Modbus_Tcp_Slaves[index].Scale_ID); //scale id=
		strcat(ptr,&temp_buff[0]);		

		memset(temp_buff,'\0',sizeof(temp_buff));
		sprintf(temp_buff,"%d,",Modbus_Tcp_Slaves[index].scale_connectivity_status); //scale connectivity status=
		strcat(ptr,&temp_buff[0]);
		
		memset(temp_buff,'\0',sizeof(temp_buff));
		sprintf(temp_buff,"%x.%x,",Modbus_Tcp_Slaves[index].fw_ver[0],Modbus_Tcp_Slaves[index].fw_ver[1]); //scale fw version
		strcat(ptr,&temp_buff[0]);
		
			memset(temp_buff,'\0',sizeof(temp_buff));
			sprintf(temp_buff,"IP=%03d.%03d.%03d.%03d,", Modbus_Tcp_Slaves[index].IP_ADRESS[3],
																									Modbus_Tcp_Slaves[index].IP_ADRESS[2],
																									Modbus_Tcp_Slaves[index].IP_ADRESS[1],
																									Modbus_Tcp_Slaves[index].IP_ADRESS[0]);	
			strcat(ptr,&temp_buff[0]);
			memset(temp_buff,'\0',sizeof(temp_buff));	
			sprintf(temp_buff,"DT=%04d-%02d-%02d|%02d:%02d:%02d", 
																							 Scale_Record_Mb_Reg[index].Scale_Date.int_array[1],
																							 (Scale_Record_Mb_Reg[index].Scale_Date.int_array[0] & 0x00FF),
																							 (Scale_Record_Mb_Reg[index].Scale_Date.int_array[0] >> 8 ),
																							(Scale_Record_Mb_Reg[index].Scale_Time.int_array[1] & 0x00FF),
																							(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0] & 0x00FF ),
																							(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0] >> 8)			);
			strcat(ptr,&temp_buff[0]);
//		if(index != (Total_Scales -1)) //Changed by DK on 23 Feb 2015 	
//		strcat(ptr,"%7C");	
	}	
	
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"@@@UTC_DT%04d-%02d-%02d|%02d:%02d:%02d",disp_utc_time_year,disp_utc_time_mon,disp_utc_time_day,
																																	disp_utc_time_hr,disp_utc_time_min,disp_utc_time_sec);
	strcat(ptr,temp_buff);	
	
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"@@@PCM_IP=%03d.%03d.%03d.%03d,",Pcm_var.IP_Addr.Addr1,Pcm_var.IP_Addr.Addr2,Pcm_var.IP_Addr.Addr3,Pcm_var.IP_Addr.Addr4);
	
	strcat(ptr,&temp_buff[0]);	
	
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"@@@ModemDT%04d-%02d-%02d|%02d:%02d:%02d",Current_time.RTC_Year,Current_time.RTC_Mon,Current_time.RTC_Mday,
																																	Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec);
	strcat(ptr,temp_buff);	
	
	memset(temp_buff,'\0',sizeof(temp_buff));
	if(Pcm_var.PCM_connection_select==Screen213_str1)
	{
		sprintf(temp_buff,"@@@PCMConnection=LAN");

	}
	else if(Pcm_var.PCM_connection_select==Screen213_str3)
	{
		sprintf(temp_buff,"@@@PCMConnection=MultitechHSPA");

	}
	else if(Pcm_var.PCM_connection_select==Screen213_str5)
	{
		sprintf(temp_buff,"@@@PCMConnection=Verizon4GLTE");

	}
	

//	sprintf(temp_buff,"@@@ConnectionType%s",Pcm_var.PCM_connection_select);
			strcat(ptr,temp_buff);
			memset(temp_buff,'\0',sizeof(temp_buff));
			if((Pcm_var.PCM_connection_select==Screen213_str5)||(Pcm_var.PCM_connection_select ==Screen213_str3))
			{
					sprintf(temp_buff,"@@@MODEM_IP=%03d.%03d.%03d.%03d,", Pcm_var.FourG_IP_Addr.Addr1,Pcm_var.FourG_IP_Addr.Addr2,
																											Pcm_var.FourG_IP_Addr.Addr3,Pcm_var.FourG_IP_Addr.Addr4);
			}
			strcat(ptr,temp_buff);
			memset(temp_buff,'\0',sizeof(temp_buff));

			if(Pcm_var.PCM_web_select==Screen212_str1)
			{
				sprintf(temp_buff,"@@@PCSWebsite=ProductionSite");

			}
			else
			{
				sprintf(temp_buff,"@@@PCSWebsite=TestSite");

			}	
//	sprintf(temp_buff,"@@@PCSWebsite%d",Pcm_var.PCM_web_select);
	strcat(ptr,temp_buff);

				
	memset(temp_buff,'\0',sizeof(temp_buff));
//	sprintf(temp_buff,"@@@PPPStatus%d",cell_modem_strength);
	if(Pcm_var.PPP_Status == Screen24_str10)
	{
		sprintf(temp_buff,"@@@PPPStatus=Disable");

	}
	else
	{
		sprintf(temp_buff,"@@@PPPStatus=Enable");

	}
			
	strcat(ptr,temp_buff);
	
	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"@@@PPPDialNo=%s",Pcm_var.PPP_dial_no);
	strcat(ptr,temp_buff);

	memset(temp_buff,'\0',sizeof(temp_buff));
		if(Pcm_var.DST_Status == Screen25_str8)
		{
			sprintf(temp_buff,"@@@DSTStatus=%s","OFF");
		}
		else
		{
			sprintf(temp_buff,"@@@DSTStatus=%s","ON");
		}
	
	strcat(ptr,temp_buff);

	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"@@@GMT=%.02f",Pcm_var.GMT_value);
	strcat(ptr,temp_buff);
	
	memset(temp_buff,'\0',sizeof(temp_buff));
	if((Pcm_var.PCM_connection_select ==Screen213_str5)||(Pcm_var.PCM_connection_select ==Screen213_str3))
	{
	sprintf(temp_buff,"@@@SignalStrength=%s",signal_strength_buf);
	}

	strcat(ptr,temp_buff);

	memset(temp_buff,'\0',sizeof(temp_buff));
	if((Pcm_var.PCM_connection_select ==Screen213_str5)||(Pcm_var.PCM_connection_select ==Screen213_str3))
	{
	sprintf(temp_buff,"@@@IMEI=%s",imei_buffer);
	}	
	strcat(ptr,temp_buff);

	memset(temp_buff,'\0',sizeof(temp_buff));
	sprintf(temp_buff,"@@@Version%05s",&FirmwareVer[1]);
	strcat(ptr,temp_buff);

 	strcat(ptr," HTTP/1.1\r\n");
	strcat(ptr,"Host: ");
	strcat(ptr,&Http_Host_Name[7]);
	strcat(ptr,"\r\n");	
	strcat(ptr,&Req_Header_3[0]);
//	strcat(ptr,&Req_Header_7[0]);
	strcat(ptr,"Content-Length: 0\r\n");	
	strcat(ptr,&Req_Header_5[0]);
	strcat(ptr,&Req_Header_6[0]);
	strcat(ptr,&Req_Header_7[0]);
	strcat(ptr,&Req_Header_8_1[0]);
	strcat(ptr,&Req_Header_9[0]);	

	strcat(ptr,"\r\n");  //\r\n
	length= strlen(ptr);
	
 //Send packet to PCS
	ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,length);
	return ret_val;
}
// BOOL modem_login(unsigned char pcs_sock_num, char* ptr)
// {
// 	char temp_buff[40];
//   U16 length;
// 	BOOL ret_val;

// 	
// 	strcpy(ptr,"GET ");
// 	
// 	if(url_redirected ==0)
// 	{
// 	strcat(ptr,"http://192.168.2.1");

// 	
// 	strcat(ptr,"/api/login?username=admin&amp;password=admin HTTP/1.1");
// 	strcat(ptr,"\r\n");
// // 	strcat(ptr,"Host: ");
// // 	strcat(ptr,"192.168.2.1");
// // 	strcat(ptr,"\r\n");
// 	}
// 	else
// 	{
// 	strcat(ptr,redir_url);
// // 	strcat(ptr," HTTP/1.1\r\n");
// 	
// 	}
// 	strcat(ptr,&Req_Header_5[0]); //Cache-Control
// 	strcat(ptr,&Req_Header_6[0]); //Postman-Token
// //	strcat(ptr,"User-Agent: PostmanRuntime/3.0.11-hotfix.2\r\n");
// 	strcat(ptr,&Req_Header_7[0]); //Accept all 
// 	
// 	strcat(ptr,"Host: ");
// 	strcat(ptr,"192.168.2.1");
// 	strcat(ptr,"\r\n");		
// 	
// 	strcat(ptr,&Req_Header_9[0]); // /r/n
// 	length= strlen(ptr);
//   //Send packet to PCS
// 	ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,length);
// 	return ret_val;
// }
//by megha on 19/5/2017 to read signal stregth of 4G modem
// BOOL Http_Get_signal_strength(U8 pcs_sock_num, char* ptr)
// {
// 	char temp_buff[40];
//   U16 length;
// 	BOOL ret_val;

// 	
// 	strcpy(ptr,"GET ");
// 	
// 	if(url_redirected ==0)
// 	{
// 	strcat(ptr,"http://192.168.2.1");

// 	
// 	strcat(ptr,"/api/stats/radio HTTP/1.1");
// 	strcat(ptr,"\r\n");
// // 	strcat(ptr,"Host: ");
// // 	strcat(ptr,"192.168.2.1");
// // 	strcat(ptr,"\r\n");
// 	}
// 	else
// 	{
// 	strcat(ptr,redir_url);
// // 	strcat(ptr," HTTP/1.1\r\n");
// 	
// 	}
// 	strcat(ptr,&Req_Header_5[0]); //Cache-Control
// 	strcat(ptr,"Postman-Token: ");
// 	strcat(ptr,&token_fourG[0]); //Postman-Token
// //	strcat(ptr,"User-Agent: PostmanRuntime/3.0.11-hotfix.2\r\n");
// 	strcat(ptr,&Req_Header_7[0]); //Accept all 
// 	
// 	strcat(ptr,"Host: ");
// 	strcat(ptr,"192.168.2.1");
// 	strcat(ptr,"\r\n");		
// 	
// 	strcat(ptr,&Req_Header_9[0]); // /r/n
// 	length= strlen(ptr);
//   //Send packet to PCS
// 	ret_val=tcp_send_data(pcs_sock_num, (U8*)ptr,length);
// 	return ret_val;
// }

// BOOL Http_Get_Read_signal_strength(char* pcSrcBuffer, U16 rx_length)
// {
// 	char *ptr,*http_ptr;
// 	U16 RecvValue=0,i,j,RecvBufPosn;
// 	BOOL ret_val=FALSE;
//   char temp_buff[40];
// 	return(0);
// }

static void dns_cbfunc (U8 event, U8 *ip) {
  switch (event) {
    case DNS_EVT_SUCCESS:
				PCS_IpAdr[0] = ip[0];
				PCS_IpAdr[1] = ip[1];
				PCS_IpAdr[2] = ip[2];
				PCS_IpAdr[3] = ip[3];
		os_evt_set(EVE_DNS_IP_RESOLVED,t_http_client); //Added by DK on 10 Dec 2014 
		break;
    case DNS_EVT_NONAME:
  //    printf("Host name does not exist.\n");
		#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "Host name does not exist\r\n");
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
		os_evt_set(EVE_DNS_IP_NONAME,t_http_client); 
      break;
    case DNS_EVT_TIMEOUT:
    //  printf("DNS Resolver Timeout expired, Host IP not resolved.\n");
		#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "DNS Resolver Timeout expired, Host IP not resolved.\n");
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
		  os_evt_set(EVE_DNS_IP_TIMEOUT,t_http_client); 
      break;
    case DNS_EVT_ERROR:
   //   printf("DNS Resolver Error, check the host name, labels, etc.\n");
		#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "DNS Resolver Error, check the host name, labels, etc.\n");
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
			os_evt_set(EVE_DNS_IP_ERROR,t_http_client); 
      break;
  }
}
int             pcs_sock_num=0;
/*****************************************************************************
* @note       Function name  : __task void http_client (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 5 November 2014
* @brief      Description    : The HTTP Measurement server(PCS) is a web application built to record measurement data 
														 : from PCM. PCM implements HTTP client & connects to PCS to push the data records.
*                            : 
*                            : 
* @note       Notes          : None
*****************************************************************************/
__task void http_client (void)
{
	unsigned char pcs_connect=0,pcs_sock_state=0,power_on,dns_ip_resolved;
	int            /* pcs_sock_num=0*/i,j;
	unsigned int pcs_no_of_retry=0; //,pcs_wait_Count=0; //Commented by DK on 6 Feb 2015 for PCM to PCS connectivity status update
	OS_RESULT result;
	char *ptr,*http_ptr;
	char temp_buf[30];
	U16 evt_flags;
	unsigned int type = 0;
	unsigned char invalid_scale_id;
	FILE *fp;
	char ftp_file_name[80];  	
	for(i=0; i< MAX_NO_OF_INTEGRATORS; i++)
	{	
		Modbus_Tcp_Slaves[i].prev_record = 0;
		Modbus_Tcp_Slaves[i].cur_record = 0;
	}
	power_on = 1; //first get request
  usHttpClientRcvBufferLen=0;
  dns_ip_resolved =0;
	pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 
//	GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);
	//Added by DK on 11 Feb2015 
	if(dhcp_enable == 1)
	{
		do 
		{	
			os_itv_set(HTTP_CLIENT_INTERVAL);
			os_itv_wait();
		}while(dhcp_tout);
	}	
	
	while(1)
	{
		os_itv_set(HTTP_CLIENT_INTERVAL);
		os_itv_wait();
		LPC_RTC->GPREG4 |= (0x01<<9);
// 			if((read_signal_strength_fg == 1) /*&& (pcs_sock_state == TCP_STATE_CONNECT)*/&&(Pcm_var.PCM_connection_select == Screen213_str5))  //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
// 			{

// //				os_mut_wait (&tcp_mutex, 0xffff);
// 				
// 				LTE_login();
// 			}
		
		if(pcs_connect == 0) //Added by DK on 6 Feb 2015 for PCS connectivity status update
		{
//			Task_section = 1;
			if(dns_ip_resolved == 0)
			{	
				
				os_evt_clr(EVE_DNS_IP_RESOLVED | EVE_DNS_IP_NONAME | EVE_DNS_IP_TIMEOUT | EVE_DNS_IP_ERROR,t_http_client); 
				#ifdef DEBUG_PORT
			
				os_mut_wait (&debug_uart_mutex, 0xffff);		
			
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "\r\n Starts the DNS client to resolve host name : %s\
															Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
															&Http_Host_Name[7],Current_time.RTC_Mon,Current_time.RTC_Mday,\
														 Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														 Current_time.RTC_Sec);	
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
				os_evt_clr(  EVE_DNS_IP_TIMEOUT ,t_http_client); 	
LPC_RTC->ALSEC = 1;				
				get_host_by_name ((U8*)&Http_Host_Name[7],dns_cbfunc);
LPC_RTC->ALSEC = 2;				
					Flags_struct.Plant_connect_record_flag = PCS_SERVER_IP_RESOLVE_PROCESS_FLAG;		//by megha on 20/1/2017
					GUI_data_nav.Error_wrn_msg_flag[PCS_SERVER_IP_RESOLVE_PROCESS_MSG_TIME] = 0;	

				strcpy(Debug_Buf, "SERVER IP RESOLUTION IN PROCESS......");
				GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (0*16)));		
						os_mut_wait (&usb_log_mutex, 0xffff);							
					if((1<<0)!= (msg_log_reg & (1<<0)))
					{	
						msg_log_reg |= 1<<0;
						sprintf(&system_log.log_buf[0],"Server IP Resolution in process......");
						log_file_write(system_log1);	
					}	
						os_mut_release (&usb_log_mutex);
				LPC_RTC->ALSEC = 3;	
				result = os_evt_wait_or(EVE_DNS_IP_RESOLVED | EVE_DNS_IP_NONAME | EVE_DNS_IP_TIMEOUT | EVE_DNS_IP_ERROR,800); //Added by DK on 10 Dec 2014. //By DK on 10 March 2015 changed timing from 2000 to 3000
				if(result != OS_R_EVT)  //Added by DK on 5 Jan 2015 
				{	
				LPC_RTC->ALSEC = 4;
					
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);
					
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "didn't resolve PCS IP!\
															Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
															Current_time.RTC_Mon,Current_time.RTC_Mday,\
														 Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														 Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					Flags_struct.Plant_connect_record_flag &= ~PCS_SERVER_IP_RESOLVED_OK_FLAG;
					GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] = 0;
					
					Flags_struct.Plant_connect_record_flag = SERVER_IP_RESOLUTION_FAIL_FG;					
					GUI_data_nav.Error_wrn_msg_flag[SERVER_IP_RESOLVE_FAIL_MSG_TIME] = 0;
					
					strcpy(Debug_Buf, "SERVER IP RESOLUTION FAILED");
					GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0,(DISP_LINE5_POS_Y0 + (1*16)));		
//				LPC_RTC->ALSEC = 5;
						os_mut_wait (&usb_log_mutex, 0xffff);							
//					LPC_RTC->ALSEC = 6;	
					if(1!= (msg_log_reg & (1<<1)))
					{	
						msg_log_reg |= 1<<1;
						sprintf(&system_log.log_buf[0],"Server IP resolution failed");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);					
				}
				else 
				{
				LPC_RTC->ALSEC = 5;
					
					evt_flags = os_evt_get();
				LPC_RTC->ALSEC = 6;

					switch (evt_flags)
					{
						case EVE_DNS_IP_RESOLVED:
							os_evt_clr(EVE_DNS_IP_RESOLVED,t_http_client); //Added by DK on 17 March 2015 				
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Resolved PCS_IP is %d:%d:%d:%d\
															Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
															PCS_IpAdr[0],PCS_IpAdr[1],PCS_IpAdr[2],PCS_IpAdr[3],\
															Current_time.RTC_Mon,Current_time.RTC_Mday,\
															Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
															Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
							dns_ip_resolved = 1; //Added by DK on 10 March 2015 
							if(disp_power_on_screen_timeout_fg == 1)
							{
								disp_power_on_screen_timeout_fg = 0;//BY MEGHA 0N 11/5/2017
							}
					Flags_struct.Plant_connect_record_flag &= ~SERVER_IP_RESOLUTION_FAIL_FG;
					GUI_data_nav.Error_wrn_msg_flag[SERVER_IP_RESOLVE_FAIL_MSG_TIME] = 0;
						
							Flags_struct.Plant_connect_record_flag = PCS_SERVER_IP_RESOLVED_OK_FLAG;
							GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] = 0;

							strcpy(Debug_Buf, "SERVER IP RESOLVED OK                    ");
							GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0,(DISP_LINE5_POS_Y0 + (1*16)));	
//LPC_RTC->ALSEC = 7;
							os_mut_wait (&usb_log_mutex, 0xffff);		
//LPC_RTC->ALSEC = 8;	
					if((1<<2)!= (msg_log_reg & (1<<2)))
					{	
						msg_log_reg |= 1<<2;	

						sprintf(&system_log.log_buf[0],"Server IP resolved OK                         ");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);							

						 break;
						 
						case EVE_DNS_IP_NONAME:
						os_evt_clr(EVE_DNS_IP_NONAME,t_http_client); //Added by DK on 17 March 2015 				
						break;
						
						case EVE_DNS_IP_TIMEOUT:
//							LPC_RTC->ALSEC = 9;
							os_mut_wait (&usb_log_mutex, 0xffff);		
//							LPC_RTC->ALSEC = 10;
					if((1<<3)!= (msg_log_reg & (1<<3)))
					{	
						msg_log_reg |= 1<<3;	

						sprintf(&system_log.log_buf[0],"Server IP resolution time out...                 ");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);								
						os_evt_clr(EVE_DNS_IP_TIMEOUT,t_http_client); //Added by DK on 17 March 2015 				
						break;
						
						case EVE_DNS_IP_ERROR:
						os_evt_clr(EVE_DNS_IP_ERROR,t_http_client); //Added by DK on 17 March 2015 				
						break;
						
						default :
										 break;
						}
				}	
			}//end of if(dns_ip_resolved == 0)	
			
			//connect to PCS server
			if(dns_ip_resolved == 1) //Added by DK on 10 March 2015 	
			{
				msg_log_reg &=0x0ffffff0;
				//Added by DK on 13Ma3rch2015 
					Flags_struct.Plant_connect_record_flag = SOCKET_CONNECTION_PROCESS_FLAG;		//by megha on 20/1/2017
					GUI_data_nav.Error_wrn_msg_flag[SOCKET_CONNECTION_PROCESS_MSG_TIME] = 0;		
					strcpy(Debug_Buf, "SOCKET CONNECTION IN PROCESS......");
					GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (2*16)));
//LPC_RTC->ALSEC = 11;
							os_mut_wait (&usb_log_mutex, 0xffff);		
//LPC_RTC->ALSEC = 12;	
					if((1<<4)!= (msg_log_reg & (1<<4)))
					{	
						msg_log_reg |= 1<<4;

						sprintf(&system_log.log_buf[0],"Socket connection in process......");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);
	LPC_RTC->ALSEC = 7;
				
				pcs_sock_num = tcp_get_socket( (TCP_TYPE_CLIENT | TCP_TYPE_KEEP_ALIVE),0,30,prvxTCPPortHTTPCallback);
	LPC_RTC->ALSEC = 8;
				
// 					os_dly_wait(2000); 
// 					os_dly_wait(2000); 
// 					os_dly_wait(2000); 
// 					os_dly_wait(2000); 
// 					os_dly_wait(2000); 
				
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "Connecting to PCS server over Ethernet:Socket number%d!\
															Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
															pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
															Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
															Current_time.RTC_Sec);	
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
				LPC_RTC->ALSEC = 9;
					
				if(!tcp_connect( pcs_sock_num,PCS_IpAdr,http_port_number,HTTP_CLIENT_LOCAL_PORT))
				{
				LPC_RTC->ALSEC = 10;
					
					Flags_struct.Plant_connect_record_flag = PCS_SOCKET_CONNECTION_FAIL_FLAG;	//BY MEGHA ON 11/1/2017
					GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_FAIL_MSG_TIME] = 0;
						strcpy(Debug_Buf, "TCP SOCKET STATUS : NOT CONNECTED");
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (3*16)));
//					LPC_RTC->ALSEC = 13;
							os_mut_wait (&usb_log_mutex, 0xffff);		
//					LPC_RTC->ALSEC = 14;	
					if((1<<5)!= (msg_log_reg & (1<<5)))
					{	
						msg_log_reg |= 1<<5;
						sprintf(&system_log.log_buf[0],"TCP Socket failed to connect......");
						log_file_write(system_log1);
					}
						os_mut_release (&usb_log_mutex);
					
					tcp_abort(pcs_sock_num);  //Added by DK on 19 Dec 2014 
					tcp_close( pcs_sock_num);
					tcp_release_socket(pcs_sock_num);  
				}
				else
				{
				LPC_RTC->ALSEC = 11;
					
					os_dly_wait(200); //by megha on 14/6/2017 for quick response on ethernet connection
					
					pcs_sock_state = tcp_get_state (pcs_sock_num);  
					if(pcs_sock_state == TCP_STATE_CONNECT)  //PCM is connected to PCS 
					{	
				LPC_RTC->ALSEC = 12;
						
						Flags_struct.Plant_connect_record_flag = PCS_SOCKET_CONNECTION_OK_FLAG;		//BY MEGHA ON 11/1/2017
						GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_MSG_TIME] = 0;	
						strcpy(Debug_Buf, "TCP SOCKET STATUS : CONNECTED        ");				
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (3*16)));	
//LPC_RTC->ALSEC = 15;
							os_mut_wait (&usb_log_mutex, 0xffff);		
//LPC_RTC->ALSEC = 16;	
					if((1<<6)!= (msg_log_reg & (1<<6)))
					{	
						msg_log_reg |= 1<<6;						
						sprintf(&system_log.log_buf[0],"TCP Socket connected successfully......");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);
						
						pcs_connect = 1;
						pcs_connect_disp_fg = 1;
						
					//Added by DK on 6 Feb 2015 for PCS connectivity status update
						if(pcs_no_of_retry < 4)
						{
							pcs_connectivity_status = CONNECTED_IN_THREE_RETRIES;
						}	
						else if(pcs_no_of_retry < 6)
						{
							pcs_connectivity_status = CONNECTED_IN_3TO5_RETRIES;
						}
						else 
						{
							pcs_connectivity_status = CONNECTED_IN_5TO10_RETRIES;
						}
						pcs_no_of_retry =0; 	
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "Connecting to PCS-socket%d!\
															Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
															pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
															Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
															Current_time.RTC_Sec);	
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
					}
					else
					{
				LPC_RTC->ALSEC = 13;
						
						Flags_struct.Plant_connect_record_flag = PCS_SOCKET_STATUS_NC_FLAG;
						GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_STATUS_NC_MSG_TIME] = 0;		
						strcpy(Debug_Buf, "TCP SOCKET STATUS : NOT CONNECTED");
						GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (3*16)));	
//					LPC_RTC->ALSEC = 17;	
							os_mut_wait (&usb_log_mutex, 0xffff);		
//					LPC_RTC->ALSEC = 18;	
					if((1<<5)!= (msg_log_reg & (1<<5)))
					{	
						msg_log_reg |= 1<<5;						
						sprintf(&system_log.log_buf[0],"TCP Socket failed to connect......");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);						
						
						tcp_abort(pcs_sock_num);  //Added by DK on 19 Dec 2014 
						tcp_close(pcs_sock_num);  
						tcp_release_socket(pcs_sock_num);  
						pcs_no_of_retry++;
						#ifdef DEBUG_PORT
					
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "not connected to PCS.Closing socket%d!\
															Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
															pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
															Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
															Current_time.RTC_Sec);	
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
					}  
				} //end of else of if(!tcp_connect( pcs_sock_num,PCS_IpAdr,http_port_number,HTTP_CLIENT_LOCAL_PORT))
			}  //end of if(dns_ip_resolved == 1) //Added by DK on 10 March 2015 	
		
		}//end of if((pcs_connect == 0) && (pcs_no_of_retry == 0))
		
		//If pcs is connected to PCM & PCM is powered on, then load the scales configurations using GET request
    if ((pcs_connect == 1) && ((power_on == 1)||(scale_config_req_fg == 1)))
		{
//			Task_section = 2;
			msg_log_reg &=0x0fffff0f;
			pcs_sock_state = tcp_get_state (pcs_sock_num);  
			if(pcs_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS 
			{
				LPC_RTC->ALSEC = 14;
						os_mut_wait (&usb_log_mutex, 0xffff);							
//				LPC_RTC->ALSEC = 20;
						sprintf(&system_log.log_buf[0],"Modem is disconnected from server");
						log_file_write(system_log1);	
						os_mut_release (&usb_log_mutex);

				pcs_connect=0;
			
				tcp_abort(pcs_sock_num); //Added by DK on 19 Dec 2014 
				tcp_close(pcs_sock_num);  
				tcp_release_socket(pcs_sock_num); 
				pcs_no_of_retry=0;
				pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 
// 			for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 			{	
// 				Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 				Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 			}	
				//pcs_wait_Count=0; //Commented by DK on 6 Feb 2015 for PCM to PCS connectivity status update
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);		
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "Get Request-Disconnected from PCS.Closing socket%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
			}
		//Prepare packet as per HTTP message(Java Script Object Notation)
			//else if(Http_Get_Request(pcs_sock_num))	//Commented by DK on 2 Jan 2015 
			else 
			{	
				LPC_RTC->ALSEC = 15;
				
				
			//Added by DK on 2 Jan 2015 
			memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer));
			os_evt_clr(EVE_HTTP_RES,t_http_client);				 //Added by DK on 6 Feb 2015 	
				
				strcpy(Debug_Buf, "REQUESTING SCALE CONFIGURATION FROM SERVER....");				
				GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (4*16)));	
//LPC_RTC->ALSEC = 21;
							os_mut_wait (&usb_log_mutex, 0xffff);			
//LPC_RTC->ALSEC = 22;	
					if((1<<8)!= (msg_log_reg & (1<<8)))
					{	
						msg_log_reg |= 1<<8;				
						sprintf(&system_log.log_buf[0],"Requesting scale configuration from server......");
						log_file_write(system_log1);	
					}
						os_mut_release (&usb_log_mutex);	
			memset(HTTP_RX_Buffer,'\0',sizeof(HTTP_RX_Buffer));
			LPC_RTC->ALSEC = 16;
	
			if(Http_Get_Request(pcs_sock_num, &HTTP_TX_Buffer[0]))
			{
				
				//wait for response from PCS on scales configurations
				Flags_struct.Plant_connect_record_flag = PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG;
				GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;	
			LPC_RTC->ALSEC = 17;
				result = os_evt_wait_or(EVE_HTTP_RES,1000);
			LPC_RTC->ALSEC = 18;	
				os_evt_clr(EVE_HTTP_RES,t_http_client);				
				if(result == OS_R_EVT)
				{	
				
					if(usHttpClientRcvBufferLen > 0)
					{
				LPC_RTC->ALSEC = 19;
						
						if(Http_Get_Read_Response(HTTP_RX_Buffer, usHttpClientRcvBufferLen))
						{
				LPC_RTC->ALSEC = 20;
							
							Flags_struct.Plant_connect_record_flag = PCM_SCALE_CONFIGURATION_RECEIVED_OK_FLAG;
							GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME] = 0;							
							strcpy(Debug_Buf, "SCALE CONFIGURATION RECEIVED OK");
							GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (5*16)));	
//LPC_RTC->ALSEC = 25;
							os_mut_wait (&usb_log_mutex, 0xffff);				
//LPC_RTC->ALSEC = 26;
					if((1<<9)!= (msg_log_reg & (1<<9)))
					{	
						msg_log_reg |= 1<<9;								
						sprintf(&system_log.log_buf[0],"Scale configuration received ok......");
						log_file_write(system_log1);	
						msg_log_reg &=0x00ff0fff;
					}
						os_mut_release (&usb_log_mutex);							
							power_on =0;	
							Scale_configuration_received_ok_fg=1;
							power_on_fg=0;
							if(scale_config_req_fg == 1 )			//by megha on 17/7/2017 for scales reconnect on ethernet connection
							{
								scale_config_req_fg = 0;
							}
							if(disp_power_on_screen_timeout_fg == 1)
							{
								disp_power_on_screen_timeout_fg = 0;//BY MEGHA 0N 11/5/2017
							}							
							usHttpClientRcvBufferLen=0;
							os_evt_set(EVE_GET_RES,t_modbus_tcp_master);
							#ifdef DEBUG_PORT
							
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Recd Get Response from PCS!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
						}
						else //Added by DK on 7 Jan2015 for proxy server redirecting URL 
						{
				LPC_RTC->ALSEC = 21;
						
							ptr = &HTTP_RX_Buffer[0];  
							if(0!= (http_ptr = HTTPStrCaseStr(ptr,12,"HTTP/1.1 301")))
							{
								if(0!= (http_ptr = HTTPStrCaseStr(ptr,8,"https://")))
								{
									memset(redir_url,'\0',sizeof(redir_url));
									i=0;
									while(1)
									{
										if((*(http_ptr + i) == '\r') || (*(http_ptr + i) == '\n'))
										{
											redir_url[i++] = ' ';
											strcat(redir_url,"HTTP/1.1\r\n");
											break;
										}	
										redir_url[i]= *(http_ptr + i);
										i++;
										if(i > (sizeof(redir_url) -1))break;
									}	
									url_redirected =1; 
								
									
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									sprintf(Debug_Buf, "Proxy server redirected URL to :");
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									strcpy(Debug_Buf, redir_url);
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif
								}	
							}	
							
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);	
						
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "HTTP Get Response from PCS!\r\n");
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							strncpy(Debug_Buf,HTTP_RX_Buffer,sizeof(Debug_Buf)); 
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							if(usHttpClientRcvBufferLen > sizeof(Debug_Buf))
							{
								usHttpClientRcvBufferLen -= sizeof(Debug_Buf);
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								strncpy(Debug_Buf,&HTTP_RX_Buffer[sizeof(Debug_Buf)],sizeof(Debug_Buf)-4);
								strcat(Debug_Buf,"\r\n");
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							}	
							os_mut_release (&debug_uart_mutex);
							#endif
							
						}		
					}
				}
				else 
				{
									LPC_RTC->ALSEC = 22;

					Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG;
					GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;	
//					LPC_RTC->ALSEC = 27;
						os_mut_wait (&usb_log_mutex, 0xffff);					
//				LPC_RTC->ALSEC = 28;
					if((1<<10)!= (msg_log_reg & (1<<10)))
					{	
						msg_log_reg |= 1<<10;						
						sprintf(&system_log.log_buf[0],"Modem is disconnected from server due to no response of 'GET configuration' post" );
						log_file_write(system_log1);					
					}
						os_mut_release (&usb_log_mutex);

					pcs_connect=0;
// 					for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 					{	
// 						Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 						Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 					}
					tcp_abort(pcs_sock_num);  //Added by DK on 19 Dec 2014 
					tcp_close(pcs_sock_num);  
					tcp_release_socket(pcs_sock_num); 
					pcs_no_of_retry=0;
					pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 
					#ifdef DEBUG_PORT
					
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "Get Rsp Timeout from PCS.Closing socket%d!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
							
				}	
			}	//end of else if(Http_Get_Request(pcs_sock_num))
			else
			{
				Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG;
				GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;
			}
			
			}
		}//end of  if ((pcs_connect == 1) && (power_on == 1))		
		
		if ((pcs_connect == 1) && ((power_on ==0) &&(scale_config_req_fg == 0)) ) //PCM is connected to PCS. Send records to PCS 
		{
		LPC_RTC->ALSEC = 23;
////			os_mut_wait (&tcp_mutex, 0xffff);	 //commented by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015  PCM firmware V1.33
//		LPC_RTC->ALSEC = 30;	
			for(i=0; i < Total_Scales; i++)
			{	
				if(i > (MAX_NO_OF_INTEGRATORS - 1))	//Changed by DK on 23 Feb 2015 	
				break; 
				LPC_RTC->ALSEC = 24;

				pcs_sock_state = tcp_get_state (pcs_sock_num);  
				LPC_RTC->ALSEC = 25;

			//	if(pcs_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS Commented by DK on 18 Sep2015 
				if((pcs_sock_state == TCP_STATE_CLOSED)/*||(restore_factory_set_fg == 1)*/)  //PCM is disconnected from PCS  Added by DK on 18 Sep2015 
				{
//					restore_factory_set_fg = 0;
				LPC_RTC->ALSEC = 26;
					
//					pcs_no_of_retry++;  //Commented by DK on 18 Sep2015  
//					if(pcs_no_of_retry > 3) //Commented by DK on 18 Sep2015  
					{	
						pcs_connect=0;
// 						for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 						{	
// 							Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 							Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 						}	
						tcp_abort(pcs_sock_num);   //Added by DK on 19 Dec 2014 
						tcp_close(pcs_sock_num);  
 						tcp_release_socket(pcs_sock_num);  
						pcs_no_of_retry=0;
						pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 
// 						if(restore_factory_set_fg == 1)		//by megha on 7/10/2017 for disconnecting scales on factory reset
// 						{
// 							for(i=0; i < Total_Scales; i++)
// 							{
// 								Modbus_Tcp_Slaves[i].Connect = FALSE;					
// 							}
// 						}						
//						Modbus_Tcp_Slaves[i].NoOfRetry++;		//by megha on 2/10/2017 to disconnect scales when socket is closed
						#ifdef DEBUG_PORT
					
						os_mut_wait (&debug_uart_mutex, 0xffff);		
					
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "\r\n Post Record Req-Disconnected from PCS.Closing socket%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
//						break;
					}	
				}
				else if(pcs_sock_state == TCP_STATE_CONNECT)  //PCM is connected to PCS  Added by DK on 18 Sep2015 
				{	
				LPC_RTC->ALSEC = 27;
					
					
					if((Modbus_Tcp_Slaves[i].Connect == 1) && (Modbus_Tcp_Slaves[i].cur_record != Modbus_Tcp_Slaves[i].prev_record))//if records updated & connection with Scale is ok.
					{	
//						os_mut_wait (&tcp_mutex, 0xffff);	 //commented by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015  PCM firmware V1.33
						
//						Task_section = 1;
//LPC_RTC->ALSEC = 31;						
							os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALSEC = 32;						
						sprintf(&system_log.log_buf[0],"Posting records of scale %d ",Modbus_Tcp_Slaves[i].Scale_ID);
						log_file_write(system_log1);	
							os_mut_release (&usb_log_mutex);
						
//							Task_section = 2;
						os_evt_clr(EVE_HTTP_RES,t_http_client);				
						//Preapre packet as per HTTP message(Java Script Object Notation)
					  //Added by DK on 2 Jan 2015 
						memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); //added by DK on 16 Dec 2014 

						//if(Http_Post_Request(pcs_sock_num, i)) //Commented by DK on 2 Jan 2015 
				LPC_RTC->ALSEC = 28;
						
						if(Http_Post_Request(pcs_sock_num, i,&HTTP_TX_Buffer[0])) //Added by DK on 2 Jan 2015 
						{
							//Check repsonse from PCS If it is OK then only go for next record.
LPC_RTC->ALSEC = 29;							
							result = os_evt_wait_or(EVE_HTTP_RES,3000); 	//5000 by megha on 29/8/2017 for delay in records updation	
LPC_RTC->ALSEC = 30;							
							if(result == OS_R_EVT)
							{	
							LPC_RTC->ALSEC = 31;

								os_evt_clr(EVE_HTTP_RES,t_http_client);	//Added by DK on 6 Feb 2015 			
								if(usHttpClientRcvBufferLen > 0)
								{
LPC_RTC->ALSEC = 32;									
									os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALSEC = 36;									
								sprintf(&system_log.log_buf[0],"Posting records of scale %d is succesful",Modbus_Tcp_Slaves[i].Scale_ID);
								log_file_write(system_log1);	
								os_mut_release (&usb_log_mutex);
									
									Flags_struct.Plant_connect_record_flag = PCM_SCALE_DATA_UPDATE_OK_FLAG;
									GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME] = 0;	
									disp_rx_scale_id = i+1;
									strcpy(Debug_Buf, "SCALE DATA UPDATED ON SERVER ");				
									GUI_DispStringAt(Debug_Buf, WINDOW_HEADER_TEXT1_POS_X0, (DISP_LINE5_POS_Y0 + (12*16)));

									ptr = &HTTP_RX_Buffer[0];  
									if(0!= (http_ptr = HTTPStrCaseStr(ptr,11,"FileRequest")))
									{
										if(0!= (ptr = HTTPStrCaseStr(http_ptr,4,"Date")))
										{
										/*
											#ifdef DEBUG_PORT
											os_mut_wait (&debug_uart_mutex, 0xffff);		
											memset(Debug_Buf,'\0',sizeof(Debug_Buf));
											sprintf(Debug_Buf, "File Post request received from PCS\r\n");	
											Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
											os_mut_release (&debug_uart_mutex);
											#endif
										*/
											
											Modbus_Tcp_Slaves[i].file_scale_id=0;
											ptr+=7;
											memset(temp_buf,'\0',sizeof(temp_buf));
											j=0;
											while(1)
											{
												if((*ptr == '\r') || (*ptr == '\n') || (*ptr == '"') || (*ptr == ','))
												{
													break;
												}	
												if(*ptr !='/')
												temp_buf[j]= *ptr;
												else 
												{	
													Modbus_Tcp_Slaves[i].file_month = atoi(temp_buf);		
													j++;
													ptr++;
													break;
												}
												j++;
												ptr++;
												if(j > 2)break;
											}
											j=0;
											memset(temp_buf,'\0',sizeof(temp_buf));
											while(1)
											{
												if((*ptr == '\r') || (*ptr == '\n') || (*ptr == '"') || (*ptr == ','))
												{
													break;
												}	
												if(*ptr !='/')
												temp_buf[j]= *ptr;
												else 
												{	
													Modbus_Tcp_Slaves[i].file_day = atoi(temp_buf);		
													j++;
													ptr++;
													break;
												}
												j++;
												ptr++;
												if(j > 2)break;
											}
											
											j=0;
											memset(temp_buf,'\0',sizeof(temp_buf));
											while(1)
											{
												if((*ptr == '\r') || (*ptr == '\n') || (*ptr == '"') || (*ptr == ','))
												{
													break;
												}	
												temp_buf[j]= *ptr;
												j++;
												ptr++;
												if(j > 4)break;
											}
											if(j == 4 )
											Modbus_Tcp_Slaves[i].file_year = atoi(temp_buf);			
										}	//end of if(0!= (http_ptr = HTTPStrCaseStr(http_ptr,7,"Date")))
										else
										{
// 											os_mut_wait (&usb_log_mutex, 0xffff);							
// 											
// 											sprintf(&system_log.log_buf[0],"No date field in FTP request from server ");
// 											log_file_write(system_log1);
// 											os_mut_release (&usb_log_mutex);
											
										}
										
										if(0!= (http_ptr = HTTPStrCaseStr(ptr,7,"ScaleId")))
										{
											http_ptr+=9;
											j=0;
											memset(temp_buf,'\0',sizeof(temp_buf));
											while(1)
											{
												if((*http_ptr == '\r') || (*http_ptr == '\n') || (*http_ptr == '"') || (*http_ptr == ',') || (*http_ptr == '}'))
												{
													break;
												}	
												temp_buf[j]= *http_ptr;
												j++;
												http_ptr++;
												if(j > 5)break;
											}
											
											if(j > 0 )
											Modbus_Tcp_Slaves[i].file_scale_id = atoi(temp_buf);	
											
											//if(Modbus_Tcp_Slaves[i].file_scale_id == Modbus_Tcp_Slaves[i].Scale_ID)
											{
												for(j=0; j < Total_Scales; j++)
												{
													if((Modbus_Tcp_Slaves[i].file_scale_id == Modbus_Tcp_Slaves[j].Scale_ID)&&(file_request_in_process == 0))
													{
														file_request_in_process=1;
														Modbus_Tcp_Slaves[j].ftp_file_not_available=0;
														Modbus_Tcp_Slaves[j].get_file=1;
														Modbus_Tcp_Slaves[j].file_day 	= Modbus_Tcp_Slaves[i].file_day;
														Modbus_Tcp_Slaves[j].file_month = Modbus_Tcp_Slaves[i].file_month;
														Modbus_Tcp_Slaves[j].file_year 	= Modbus_Tcp_Slaves[i].file_year;
														File_request_data.file_day = Modbus_Tcp_Slaves[i].file_day;
														File_request_data.file_month=Modbus_Tcp_Slaves[i].file_month;
														File_request_data.file_year=Modbus_Tcp_Slaves[i].file_year;
														File_request_data.Scale_ID=Modbus_Tcp_Slaves[i].file_scale_id;
														os_mut_wait (&usb_log_mutex, 0xffff);							
													
														sprintf(&system_log.log_buf[0],"FTP request to SCALE %d of %2d-%2d-%4d from server",Modbus_Tcp_Slaves[j].Scale_ID,
														Modbus_Tcp_Slaves[i].file_month,Modbus_Tcp_Slaves[i].file_day,Modbus_Tcp_Slaves[i].file_year);
														log_file_write(system_log1);		
														os_mut_release (&usb_log_mutex);
														
														break;
													}
													else 
													{
// 														os_mut_wait (&usb_log_mutex, 0xffff);							
// 														
// 														sprintf(&system_log.log_buf[0],"Invalid(NULL) Scale ID for FTP request from server ");
// 														log_file_write(system_log1);
// 														os_mut_release (&usb_log_mutex);
														
													}
												}
												if((j == Total_Scales)&&(file_request_in_process==0))
												{
													invalid_scale_id = 1;
												}
												#ifdef DEBUG_PORT
											
												os_mut_wait (&debug_uart_mutex, 0xffff);
											
												memset(Debug_Buf,'\0',sizeof(Debug_Buf));
												sprintf(Debug_Buf, "\r\n File Post request received for scale Id %d,year%04d,month%02d,date%02d\r\n",\
												        Modbus_Tcp_Slaves[i].file_scale_id,Modbus_Tcp_Slaves[i].file_year,\
												        Modbus_Tcp_Slaves[i].file_month,Modbus_Tcp_Slaves[i].file_day);	
												Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
												os_mut_release (&debug_uart_mutex);
												#endif
											}	
											
										}	//end of if(0!= (http_ptr = HTTPStrCaseStr(ptr,7,"ScaleId")))
										else
										{
// 														os_mut_wait (&usb_log_mutex, 0xffff);							
// 											
// 														sprintf(&system_log.log_buf[0],"NO Scale ID for FTP request from server ");
// 														log_file_write(system_log1);
// 														os_mut_release (&usb_log_mutex);
											
										}
									}  //end of if(0!= (http_ptr = HTTPStrCaseStr(ptr,11,"FileRequest")))
									else
									{
// 														os_mut_wait (&usb_log_mutex, 0xffff);							
// 										
// 													sprintf(&system_log.log_buf[0],"NO FTP request from server ");
// 													log_file_write(system_log1);
// 														os_mut_release (&usb_log_mutex);
										
									}									
									
									//Added by DK on 12 March 2015 for Connectivity Status Request from PCS 
									ptr = &HTTP_RX_Buffer[0];  
									memset(temp_buf,'\0',sizeof(temp_buf));
									temp_buf[0]='"';
									strcpy(&temp_buf[1],"NeedsStatus");
									temp_buf[12]='"';
									strcpy(&temp_buf[13],":true");
									if(0!= (http_ptr = HTTPStrCaseStr(ptr,strlen(temp_buf),temp_buf)))
									{
										report_connectivity_status = 1; 
									}	//end of if(0!= (http_ptr = HTTPStrCaseStr(ptr,11,"NeedsStatus")))
									
									usHttpClientRcvBufferLen=0;	
									Modbus_Tcp_Slaves[i].prev_record = Modbus_Tcp_Slaves[i].cur_record;
									pcs_no_of_retry=0;
								}  //end of if(usHttpClientRcvBufferLen > 0)  
							} //end of if(result == OS_R_EVT)
							else //added on 4 Dec 2014 
							{
								LPC_RTC->ALSEC = 33;

								pcs_no_of_retry++;  
								if(pcs_no_of_retry > 1) //3
								{	
									Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_DATA_UPDATE_OK_FLAG;
									GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME] = 0;									
									pcs_connect=0;
// 									for(i=0; i < MAX_NO_OF_INTEGRATORS; i++)
// 									{	
// 										Modbus_Tcp_Slaves[i].Connect == 0;				//by megha on 29/6/2017 disconnecting scales if modem is disconnected from server 	
// 										Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
// 									}									
									tcp_abort(pcs_sock_num);   //Added by DK on 19 Dec 2014 
									tcp_close(pcs_sock_num);  
									tcp_release_socket(pcs_sock_num);  
									pcs_no_of_retry=0;
									pcs_connectivity_status = NOT_CONNECTED; //Added by DK on 6 Feb 2015 
//									Modbus_Tcp_Slaves[i].NoOfRetry++; //by megha on 2/10/2017 to disconnect scales when socket is closed
									#ifdef DEBUG_PORT
								
									os_mut_wait (&debug_uart_mutex, 0xffff);
							
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									sprintf(Debug_Buf, "Post Rsp Timeout from PCS.Closing socket%d!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																pcs_sock_num,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif
//LPC_RTC->ALSEC = 37;									
								os_mut_wait (&usb_log_mutex, 0xffff);	
//LPC_RTC->ALSEC = 38;								
								sprintf(&system_log.log_buf[0],"Posting records of scale %d is failed,closing socket.",Modbus_Tcp_Slaves[i].Scale_ID);
								log_file_write(system_log1);				
								os_mut_release (&usb_log_mutex);
								
//									break;
								}	
							}	
						} //end  of if(Http_Post_Request(pcs_sock_num, i))
//						os_mut_release (&tcp_mutex);
					}//end of checking records update & connection with Scale
					
					if((Modbus_Tcp_Slaves[i].Connect == 1) && (Modbus_Tcp_Slaves[i].ftp_file_recd == 1))
					{
//			os_mut_wait (&tcp_mutex, 0xffff);	 //commented by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015  PCM firmware V1.33
							LPC_RTC->ALSEC = 34;

//						Task_section = 2;
						if(Modbus_Tcp_Slaves[i].file_poll_complete >= 30)
						{//Preapre packet as per HTTP message(Java Script Object Notation)
								LPC_RTC->ALSEC = 35;

							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Posting a File of Scale Id %d to PCS!\
																	Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																	Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																	Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																	Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
//LPC_RTC->ALSEC = 39;							
									os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALSEC = 40;							
									sprintf(&system_log.log_buf[0],"Posting a file of scale %d to server",Modbus_Tcp_Slaves[i].Scale_ID);
									log_file_write(system_log1);		
								os_mut_release (&usb_log_mutex);
							
							os_evt_clr(EVE_HTTP_RES,t_http_client);		
							LPC_RTC->ALSEC = 36;
	
							if(Http_Post_File(pcs_sock_num, i,1))
							{
								LPC_RTC->ALSEC = 37;
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								sprintf(Debug_Buf, "Posted a File of Scale Id %d to PCS!\
																	Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																	Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																	Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																	Current_time.RTC_Sec);	
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
//LPC_RTC->ALSEC = 41;								
									os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALSEC = 42;								
									sprintf(&system_log.log_buf[0],"Posted a file of scale to server Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[i].Scale_ID,Modbus_Tcp_Slaves[i].file_year,Modbus_Tcp_Slaves[i].file_month,Modbus_Tcp_Slaves[i].file_day);
									log_file_write(system_log1);	
								os_mut_release (&usb_log_mutex);
								
								//Check repsonse from PCS If it is OK then only go for next record.
LPC_RTC->ALSEC = 38;								
								result = os_evt_wait_or(EVE_HTTP_RES,9000); 	
LPC_RTC->ALSEC = 39;								
								if(result == OS_R_EVT)
								{
									LPC_RTC->ALSEC = 40;
									os_evt_clr(EVE_HTTP_RES,t_http_client);	//Added by DK on 6 Feb 2015 			 
									if((usHttpClientRcvBufferLen > 0) && (usHttpClientRcvBufferLen < HTTP_RX_BUFFER_LENGTH))
									{
										LPC_RTC->ALSEC = 41;
										#ifdef DEBUG_PORT
										os_mut_wait (&debug_uart_mutex, 0xffff);		
										memset(Debug_Buf,'\0',sizeof(Debug_Buf));
										sprintf(Debug_Buf, "Recd Resp for file of Scale ID %d from PCS!\
																	Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																	Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																	Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																	Current_time.RTC_Sec);	
										Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
										os_mut_release (&debug_uart_mutex);
										#endif 
//LPC_RTC->ALSEC = 45;										
									os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALSEC = 46;										
									sprintf(&system_log.log_buf[0],"Received resp. for file of Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[i].Scale_ID,Modbus_Tcp_Slaves[i].file_year,Modbus_Tcp_Slaves[i].file_month,Modbus_Tcp_Slaves[i].file_day);
									log_file_write(system_log1);			
									os_mut_release (&usb_log_mutex);
										
									}	
									Modbus_Tcp_Slaves[i].ftp_file_recd=0;
									Modbus_Tcp_Slaves[i].file_poll_complete = 0;
									//Added by DK on 18May2016 to fix issue of PCM sometimes does not POST the upload to the PCS
								  Modbus_Tcp_Slaves[i].get_file = 0; 
									file_request_in_process = 0;
				memset(ftp_file_name,'\0',sizeof(ftp_file_name));
									
				sprintf(ftp_file_name, "Scale_%d\\%04d-%02d-%02d.txt",File_request_data.Scale_ID,File_request_data.file_year,File_request_data.file_month,File_request_data.file_day);
									
									fp = fopen(ftp_file_name,"r");
									if(fp!=0)
									{
										fclose(fp);
										fdelete(ftp_file_name);
									}
								}
								else
								{
									os_mut_wait (&usb_log_mutex, 0xffff);							
					
									sprintf(&system_log.log_buf[0],"No Response Received for file upload of Scale_%d\\%04d-%02d-%02d.txt",Modbus_Tcp_Slaves[i].Scale_ID,Modbus_Tcp_Slaves[i].file_year,Modbus_Tcp_Slaves[i].file_month,Modbus_Tcp_Slaves[i].file_day);
									log_file_write(system_log1);			
									os_mut_release (&usb_log_mutex);
								}	
												

							}
							else 
							{
								LPC_RTC->ALSEC = 42;
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									sprintf(Debug_Buf, "Not able to Post file of scale ID %d to PCS!\r\n",\
													Modbus_Tcp_Slaves[i].Scale_ID);
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif
									Modbus_Tcp_Slaves[i].ftp_file_recd=0;
									Modbus_Tcp_Slaves[i].file_poll_complete = 0;
								  //Added by DK on 18May2016 to fix issue of PCM sometimes does not POST the upload to the PCS
								  Modbus_Tcp_Slaves[i].get_file = 0; 
									file_request_in_process = 0;
									memset(ftp_file_name,'\0',sizeof(ftp_file_name));
									sprintf(ftp_file_name, "Scale_%d\\%04d-%02d-%02d.txt",File_request_data.Scale_ID,File_request_data.file_year,File_request_data.file_month,File_request_data.file_day);
									fp = fopen(ftp_file_name,"r");
									if(fp!=0)
									{
										fclose(fp);
										fdelete(ftp_file_name);
									}								
//LPC_RTC->ALSEC = 47;								
									os_mut_wait (&usb_log_mutex, 0xffff);		
//LPC_RTC->ALSEC = 48;								
									sprintf(&system_log.log_buf[0],"Not able to post a file of scale %d",Modbus_Tcp_Slaves[i].Scale_ID);
									log_file_write(system_log1);	
									os_mut_release (&usb_log_mutex);
								
							}	
						}
						else
						{
							Modbus_Tcp_Slaves[i].file_poll_complete++;
						}
//						os_mut_release (&tcp_mutex);
					}	//end of if((Modbus_Tcp_Slaves[i].Connect == 1) && (i==0) && (Modbus_Tcp_Slaves[i].ftp_file_recd == 1))	
					
					//Added by DK on 5 Feb 2015 
					if(((Modbus_Tcp_Slaves[i].Connect == 1) && (Modbus_Tcp_Slaves[i].ftp_file_not_available == 1))
						|| ((Modbus_Tcp_Slaves[i].Connect == 0) && (Modbus_Tcp_Slaves[i].get_file == 1))||
								 (invalid_scale_id == 1))			//by megha on 30/11/2017 for invalid scale id in file request
					{
						LPC_RTC->ALSEC = 43;
//			os_mut_wait (&tcp_mutex, 0xffff);	 //commented by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015  PCM firmware V1.33
						
//						Task_section = 3;
						//Preapre packet as per HTTP message(Java Script Object Notation)
						#ifdef DEBUG_PORT
						os_mut_wait (&debug_uart_mutex, 0xffff);		
						memset(Debug_Buf,'\0',sizeof(Debug_Buf));
						sprintf(Debug_Buf, "Posting a File Not found response of Scale Id %d to PCS!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
						Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						os_mut_release (&debug_uart_mutex);
						#endif
						os_evt_clr(EVE_HTTP_RES,t_http_client);			
							if((Modbus_Tcp_Slaves[i].Connect == 1) && (Modbus_Tcp_Slaves[i].ftp_file_not_available == 1))//(Modbus_Tcp_Slaves[i].Connect == 1)
							{type = 0;}
							else if ((Modbus_Tcp_Slaves[i].Connect == 0) && (Modbus_Tcp_Slaves[i].get_file == 1))
							{type = 2;}
							else if(invalid_scale_id == 1)
							{
								type = 3;
								invalid_scale_id = 0;
							}	
							LPC_RTC->ALSEC = 44;
						if(Http_Post_File(pcs_sock_num, i,type))
						{
							LPC_RTC->ALSEC = 45;
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Posted a File Not found response of Scale Id %d to PCS!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif
							//Check repsonse from PCS If it is OK then only go for next record.
LPC_RTC->ALSEC = 46;							
							result = os_evt_wait_or(EVE_HTTP_RES,3000); 	
LPC_RTC->ALSEC = 47;							
							if(result == OS_R_EVT)
							{
								LPC_RTC->ALSEC = 48;
								os_evt_clr(EVE_HTTP_RES,t_http_client);	//Added by DK on 6 Feb 2015 			 
								if((usHttpClientRcvBufferLen > 0) && (usHttpClientRcvBufferLen < HTTP_RX_BUFFER_LENGTH))
								{
									LPC_RTC->ALSEC = 49;
									#ifdef DEBUG_PORT
									os_mut_wait (&debug_uart_mutex, 0xffff);		
									memset(Debug_Buf,'\0',sizeof(Debug_Buf));
									sprintf(Debug_Buf, "Recd Resp for file Not found response of Scale ID %d from PCS!\
					                      Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
																Modbus_Tcp_Slaves[i].Scale_ID,Current_time.RTC_Mon,Current_time.RTC_Mday,\
																Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
																Current_time.RTC_Sec);	
									Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
									os_mut_release (&debug_uart_mutex);
									#endif 
								}	
								Modbus_Tcp_Slaves[i].ftp_file_not_available=0;
								Modbus_Tcp_Slaves[i].get_file=0;
								Modbus_Tcp_Slaves[i].file_poll_complete = 0;
								file_request_in_process = 0;

							}	
						} //end of if(Http_Post_File(pcs_sock_num, i,0))
						else 
						{
LPC_RTC->ALSEC = 50;							
								#ifdef DEBUG_PORT
								os_mut_wait (&debug_uart_mutex, 0xffff);		
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								sprintf(Debug_Buf, "Not able to Post file Not found response of scale ID %d to PCS!\r\n",\
							          Modbus_Tcp_Slaves[i].Scale_ID);
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
								os_mut_release (&debug_uart_mutex);
								#endif
//by megha on 12/12/2017							
// 								Modbus_Tcp_Slaves[i].ftp_file_not_available=0;
// 							  Modbus_Tcp_Slaves[i].get_file=0;
// 							Modbus_Tcp_Slaves[i].file_poll_complete = 0;
						}	
					}	//end of if((Modbus_Tcp_Slaves[i].Connect == 1) && (i==0) && (Modbus_Tcp_Slaves[i].ftp_file_not_available == 1))	
					else if(Modbus_Tcp_Slaves[i].Connect == 0)
					{
//						Modbus_Tcp_Slaves[i].rtc_update = 0;		//by megha on 26/5 2017
						Modbus_Tcp_Slaves[i].scale_connectivity_status = NOT_CONNECTED;
					}
//				os_mut_release (&tcp_mutex);
					
				} //end of else of if(pcs_sock_state != TCP_STATE_CONNECT)  //i.e. PCM is connected to PCS 
			}//end of for (i=0;i<MAX_NO_OF_INTEGRATORS;i++)

			//Added by DK on 12 March 2015 
			
			pcs_sock_state = tcp_get_state (pcs_sock_num);  
			if(((report_connectivity_status == 1)) && (pcs_sock_state == TCP_STATE_CONNECT))  //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
			{
//			os_mut_wait (&tcp_mutex, 0xffff);	 //commented by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015  PCM firmware V1.33
				
//				Task_section = 2;
//				send_heartbeat_msg_fg = 0;;
				os_evt_clr(EVE_HTTP_RES,t_http_client);		
				memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); //Added by DK on 11 March 2015 						
//				if(Http_Post_Connectivity_Status(pcs_sock_num,&HTTP_TX_Buffer[0]))	Http_Post_Heartbeat_message
				LPC_RTC->ALSEC = 51;
				if(Http_Post_Connectivity_Status(pcs_sock_num,&HTTP_TX_Buffer[0]))	
				
				{
					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "\r\n Posted connectivity status to PCS!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					//Check repsonse from PCS If it is OK 
LPC_RTC->ALSEC = 52;					
					result = os_evt_wait_or(EVE_HTTP_RES,3000); 	
LPC_RTC->ALSEC = 53;					
					if(result == OS_R_EVT)
					{
						os_evt_clr(EVE_HTTP_RES,t_http_client);				
						if((usHttpClientRcvBufferLen > 0) && (usHttpClientRcvBufferLen < HTTP_RX_BUFFER_LENGTH))
						{
							LPC_RTC->ALSEC = 56;
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Recd Resp for connectivity status from PCS!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif 
							//ptr = &HTTP_RX_Buffer[0];  
							//if(0!= (http_ptr = HTTPStrCaseStr(ptr,2,"Ok"))) 
							{
									report_connectivity_status=0;
							}	
						}	
					}	
				} //end of if(Http_Post_Connectivity_Status(pcs_sock_num,&HTTP_TX_Buffer[0]))	
//				os_mut_release (&tcp_mutex);
			}	//end of if(report_connectivity_status == 1) //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update


			
////			os_mut_release (&tcp_mutex);   //commented by DK on 23 Dec 2014 //was Commented by DK on 29 sep 2015 PCM firmware V1.33 
		}	//end of if (pcs_connect == 1) && (power_on ==0)
		
		  pcs_sock_state = tcp_get_state (pcs_sock_num); 
			if((send_heartbeat_msg_fg == 1) && (pcs_sock_state == TCP_STATE_CONNECT))  //Added by megha on 6/1/2017 for sending heartbeat msg to server
			{
//				send_heartbeat_msg_fg = 0;;
//				Task_section = 4;
////				os_mut_wait (&tcp_mutex, 0xffff);
				os_evt_clr(EVE_HTTP_RES,t_http_client);		
				memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); //Added by DK on 11 March 2015 						
//				if(Http_Post_Connectivity_Status(pcs_sock_num,&HTTP_TX_Buffer[0]))	Http_Post_Heartbeat_message
//LPC_RTC->ALSEC = 53;				
				os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALSEC = 54;				
				sprintf(&system_log.log_buf[0],"Posting heartbeat message ");
				log_file_write(system_log1);			
				os_mut_release (&usb_log_mutex);
	LPC_RTC->ALSEC = 54;			
				if(Http_Post_Heartbeat_message(pcs_sock_num,&HTTP_TX_Buffer[0]))	
				
				{
					send_heartbeat_msg_fg=0;
//LPC_RTC->ALSEC = 57;						
							os_mut_wait (&usb_log_mutex, 0xffff);							
//LPC_RTC->ALSEC = 58;						
				sprintf(&system_log.log_buf[0],"Posted heartbeat message successfully ");
				log_file_write(system_log1);			
				os_mut_release (&usb_log_mutex);					

					#ifdef DEBUG_PORT
					os_mut_wait (&debug_uart_mutex, 0xffff);		
					memset(Debug_Buf,'\0',sizeof(Debug_Buf));
					sprintf(Debug_Buf, "\r\n Posted heartbeat message to PCS!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
					Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
					os_mut_release (&debug_uart_mutex);
					#endif
					//Check repsonse from PCS If it is OK 
LPC_RTC->ALSEC = 55;					
					result = os_evt_wait_or(EVE_HTTP_RES,3000); 	
LPC_RTC->ALSEC = 56;					
					if(result == OS_R_EVT)
					{
					
						os_evt_clr(EVE_HTTP_RES,t_http_client);				
						if((usHttpClientRcvBufferLen > 0) && (usHttpClientRcvBufferLen < HTTP_RX_BUFFER_LENGTH))
						{
							LPC_RTC->ALSEC = 57;							
							#ifdef DEBUG_PORT
							os_mut_wait (&debug_uart_mutex, 0xffff);		
							memset(Debug_Buf,'\0',sizeof(Debug_Buf));
							sprintf(Debug_Buf, "Recd Resp for connectivity status from PCS!\
														Month:%02d Date:%02d Year:%04d Hour:%02d Minute:%02d Second:%02d\r\n",\
														Current_time.RTC_Mon,Current_time.RTC_Mday,\
														Current_time.RTC_Year,Current_time.RTC_Hour,Current_time.RTC_Min,\
														Current_time.RTC_Sec);	
							Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
							os_mut_release (&debug_uart_mutex);
							#endif 
						}	
					}	
					
				} //end of if(Http_Post_Heartbeat_message(pcs_sock_num,&HTTP_TX_Buffer[0]))	
				else
				{
					os_mut_wait (&usb_log_mutex, 0xffff);	
//LPC_RTC->ALSEC = 60;
					sprintf(&system_log.log_buf[0],"Failed to Post heartbeat message ");
					log_file_write(system_log1);	
					os_mut_release (&usb_log_mutex);
					
				}
////				os_mut_release (&tcp_mutex);
			}//end of if((send_heartbeat_msg_fg == 1) 		
			
			
		  pcs_sock_state = tcp_get_state (pcs_sock_num); 
			if((read_signal_strength_fg == 1) /*&& (pcs_sock_state == TCP_STATE_CONNECT)*/&&(Pcm_var.PCM_connection_select == Screen213_str5))  //Added by DK on 6 Feb 2015 for PCM to Scale connectivity status update
			{
// 				LTE_IpAdr[0] = Pcm_var.FourG_IP_Addr.Addr1;
// 				LTE_IpAdr[1] = Pcm_var.FourG_IP_Addr.Addr2;
// 				LTE_IpAdr[2] = Pcm_var.FourG_IP_Addr.Addr3;
// 				LTE_IpAdr[3] = Pcm_var.FourG_IP_Addr.Addr4;				
// 	sprintf(&LTE_host_name[0], "http://%d.%d.%d.%d",Pcm_var.FourG_IP_Addr.Addr1,Pcm_var.FourG_IP_Addr.Addr2,\
// 									Pcm_var.FourG_IP_Addr.Addr3,Pcm_var.FourG_IP_Addr.Addr4);					
// 				if(!tcp_connect( pcs_sock_num,LTE_IpAdr,http_port_number,HTTP_CLIENT_LOCAL_PORT))
// 				{
// 				memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer));
// 				os_evt_clr(EVE_LTE_RES,t_http_client);					
// 				if(Http_Get_LTE_login(pcs_sock_num,&HTTP_TX_Buffer[0]))
// //				if(Http_Get_signal_strength(LTE_sock_num,&HTTP_TX_Buffer[0]))
// 					result = os_evt_wait_or(EVE_HTTP_RES,5000);
// 					os_evt_clr(EVE_HTTP_RES,t_http_client);	
// LPC_RTC->ALSEC = 59;					
// 					
// 					if(result == OS_R_EVT)
// 					{
// 						if(usHttpClientRcvBufferLen > 0)
// 						{

// 								ptr = &HTTP_RX_Buffer[0];  
// 								if(0!= (http_ptr = HTTPStrCaseStr(ptr,sizeof(Res_Header_3_1),Res_Header_3_1)))
// 								{	
// //									power_on_lte_login_fg = 0;
// 									os_mut_wait (&usb_log_mutex, 0xffff);		
// 						if((1<<25)!= (msg_log_reg & (1<<25)))
// 						{		
// 							msg_log_reg |= 1<<25;												
// 									sprintf(&system_log.log_buf[0],"4G LTE login successful......");
// 									log_file_write(system_log1);	
// 							
// 						}
// 									os_mut_release (&usb_log_mutex);										
// 									http_ptr+=(sizeof(Res_Header_3_1)+3 );
// 									for(i=0;i<35;i++)
// 									{
// 										if(*http_ptr != ',')
// 										{
// 										token_fourG[i] = *http_ptr;
// 										http_ptr++;
// 										}
// 										else
// 										{
// 											token_fourG[i-1] = '\0';
// 											break;
// 										}
// 									}
// // ptr = &HTTP_RX_Buffer[0];
// // 								if(0!= (http_ptr = HTTPStrCaseStr(ptr,sizeof(Res_Header_3),Res_Header_3)))
// // 								{
// // 									for(i=0;i<40;i++)
// // 									{
// // 										if(*http_ptr != ',')
// // 										{
// // 										system_log.log_buf[i] = *http_ptr;
// // 										http_ptr++;
// // 										}
// // 										else
// // 										{
// // 											system_log.log_buf[i] = '\0';
// // 											break;
// // 										}									
// // 									}										
// // 								}	
// 									
// // 							os_mut_wait (&usb_log_mutex, 0xffff);							
// // 							strcpy(&system_log.log_buf[0],&token_fourG[0]);
// // 							log_file_write(system_log1);	
// // 							os_mut_release (&usb_log_mutex);										
// 								}	
// 								else
// 								{

// 								}
// 						}						
// 					
// 					}				
// 				
// 				{
// 				}					

// 				}					
		
				
				
				
				
//				os_mut_wait (&tcp_mutex, 0xffff);
//				LTE_login();
// 				os_evt_clr(EVE_HTTP_RES,t_http_client);		
// 				memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); //Added by DK on 11 March 2015 						
// //				if(Http_Post_Connectivity_Status(pcs_sock_num,&HTTP_TX_Buffer[0]))	Http_Post_Heartbeat_message
// 				os_mut_wait (&usb_log_mutex, 0xffff);							
// 				
// 				sprintf(&system_log.log_buf[0],"Reading signal strength ");
// 				log_file_write(system_log1);			
// 				os_mut_release (&usb_log_mutex);
// 				
// 				if(modem_login(pcs_sock_num, &HTTP_TX_Buffer[0]))
// 				{
// 					result = os_evt_wait_or(EVE_HTTP_RES,10000);
// 					os_evt_clr(EVE_HTTP_RES,t_http_client);				
// 					if(result == OS_R_EVT)
// 					{	
// 						if(usHttpClientRcvBufferLen > 0)
// 						{
// 									ptr = &HTTP_RX_Buffer[0];  
// 									if(0!= (http_ptr = HTTPStrCaseStr(ptr,5,"token")))
// 									{	
// 										ptr+=8;
// 										for(i=0;i<32;i++)
// 										{
// 											token_fourG[i] = *ptr;
// 											ptr++;
// 										}
// 										
// 										
// 									}
// 						}
// 					}					
// 				os_evt_clr(EVE_HTTP_RES,t_http_client);		
// 				memset(HTTP_TX_Buffer,'\0',sizeof(HTTP_TX_Buffer)); //Added by DK on 11 March 2015 
// 			if(Http_Get_signal_strength(pcs_sock_num, &HTTP_TX_Buffer[0]))
// 			{
// 				result = os_evt_wait_or(EVE_HTTP_RES,10000);
// 				os_evt_clr(EVE_HTTP_RES,t_http_client);				
// 				if(result == OS_R_EVT)
// 				{	
// 					if(usHttpClientRcvBufferLen > 0)
// 					{
// 						if(Http_Get_Read_signal_strength(HTTP_RX_Buffer, usHttpClientRcvBufferLen))
// 						{
// 							read_signal_strength_fg = 0;
// 							os_mut_wait (&usb_log_mutex, 0xffff);							
// 						sprintf(&system_log.log_buf[0],"Modem signal strength info. received ok......");
// 						log_file_write(system_log1);	
// 						os_mut_release (&usb_log_mutex);							
// 							usHttpClientRcvBufferLen=0;
// 							os_evt_set(EVE_GET_RES,t_modbus_tcp_master);
// 						}
// 						else //Added by DK on 7 Jan2015 for proxy server redirecting URL 
// 						{
// 							ptr = &HTTP_RX_Buffer[0];  
// 							if(0!= (http_ptr = HTTPStrCaseStr(ptr,12,"HTTP/1.1 301")))
// 							{
// 								if(0!= (http_ptr = HTTPStrCaseStr(ptr,8,"https://")))
// 								{
// 									memset(redir_url,'\0',sizeof(redir_url));
// 									i=0;
// 									while(1)
// 									{
// 										if((*(http_ptr + i) == '\r') || (*(http_ptr + i) == '\n'))
// 										{
// 											redir_url[i++] = ' ';
// 											strcat(redir_url,"HTTP/1.1\r\n");
// 											break;
// 										}	
// 										redir_url[i]= *(http_ptr + i);
// 										i++;
// 										if(i > (sizeof(redir_url) -1))break;
// 									}	
// 									url_redirected =1; 
// 								
// 									
// 								}	
// 							}	
// 			
// 						}		
// 					}
// 				}
// 	
// 			}	//end of else if(Http_Get_Request(pcs_sock_num))
// 				else
// 				{
// 					os_mut_wait (&usb_log_mutex, 0xffff);							
// 					sprintf(&system_log.log_buf[0],"Failed to read signal strength ");
// 					log_file_write(system_log1);	
// 					os_mut_release (&usb_log_mutex);
// 					
// 				}				
// 			}
//				os_mut_release (&tcp_mutex);
							

			}//end of if((send_heartbeat_msg_fg == 1) 			
		   //commented by DK on 23 Dec 2014 
			LPC_RTC->GPREG4 &= ~(0x01<<9);
//		WDTFeed();
	}	//end of while(1);
}	

/*****************************************************************************
* End of file
*****************************************************************************/
