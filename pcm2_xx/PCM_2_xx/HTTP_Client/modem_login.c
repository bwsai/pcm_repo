#include <ctype.h>
#include <stdlib.h>
#include "math.h"
#include "Global_ex.h"
#include "Modbus_Tcp_Master.h"
#include "Http_Client.h"
#include "RTOS_main.h"
#include "mbport.h"
#include "Main.h"
#include "File_update.h"
#include "../GUI/Application/Screen_global_ex.h"

unsigned char LTE_host_name[30];
char token_fourG[35] ;
char LTE_HTTP_RX_Buffer[HTTP_RX_BUFFER_LENGTH];
char LTE_HTTP_TX_Buffer[HTTP_TX_BUFFER_LENGTH];
static U16 usLteClientRcvBufferLen;
unsigned char LTE_IpAdr[4];
unsigned char signal_strength_buf[8],rssi_buf[5],imei_buffer[20];
static char arcLTEClientRcvBuffer[HTTP_CLIENT_BUFFER_LENGTH];
BOOL Http_Get_LTE_login(U8 LTE_sock_num, char* ptr);
BOOL Http_Get_signal_strength(U8 LTE_sock_num, char* ptr);
BOOL Http_Get_Read_signal_strength(char* pcSrcBuffer, U16 rx_length);
BOOL LTE_connect = 0,LTE_login_fg = 0,LTE_ip_resolved = 0;
static const char Res_Header_1[] = "HTTP/1.1 200 OK";
static const char Res_Header_2[] = "\"rssidBm\"";
static const char Res_Header_5[] = "\"imei\"";
static const char Res_Header_3[] = "\"token\"";
static const char Res_Header_4[] = "\"rssi\"";
#define LTE_REM_PORT					1001 //53593
//unsigned char token_arr[700] = {0x48,0x54,0x54,0x50,0x2f,0x31,0x2e,0x31,0x20,0x32,0x30,0x30,0x20,0x4f,0x4b,0x0d,0x0a,0x53,0x65,0x74,0x2d,0x43,0x6f,0x6f,0x6b,0x69,0x65,0x3a,0x20,0x74,0x6f,0x6b,0x65,0x6e,0x3d,0x36,0x34,0x36,0x34,0x38,0x42,0x42,0x41,0x37,0x37,0x41,0x37,0x37,0x36,0x34,0x46,0x35,0x36,0x41,0x30,0x45,0x39,0x32,0x34,0x41,0x38,0x37,0x35,0x44,0x3b,0x20,0x4d,0x61,0x78,0x2d,0x41,0x67,0x65,0x3d,0x33,0x30,0x30,0x3b,0x20,0x50,0x61,0x74,0x68,0x3d,0x2f,0x3b,0x20,0x53,0x65,0x63,0x75,0x72,0x65,0x0d,0x0a,0x43,0x61,0x63,0x68,0x65,0x2d,0x43,0x6f,0x6e,0x74,0x72,0x6f,0x6c,0x3a,0x20,0x6e,0x6f,0x2d,0x63,0x61,0x63,0x68,0x65,0x0d,0x0a,0x43,0x6f,0x6e,0x74,0x65,0x6e,0x74,0x2d,0x74,0x79,0x70,0x65,0x3a,0x20,0x61,0x70,0x70,0x6c,0x69,0x63,0x61,0x74,0x69,0x6f,0x6e,0x2f,0x6a,0x73,0x6f,0x6e,0x0d,0x0a,0x54,0x72,0x61,0x6e,0x73,0x66,0x65,0x72,0x2d,0x45,0x6e,0x63,0x6f,0x64,0x69,0x6e,0x67,0x3a,0x20,0x63,0x68,0x75,0x6e,0x6b,0x65,0x64,0x0d,0x0a,0x44,0x61,0x74,0x65,0x3a,0x20,0x46,0x72,0x69,0x2c,0x20,0x30,0x32,0x20,0x4a,0x75,0x6e,0x20,0x32,0x30,0x31,0x37,0x20,0x31,0x33,0x3a,0x31,0x36,0x3a,0x31,0x36,0x20,0x47,0x4d,0x54,0x0d,0x0a,0x53,0x65,0x72,0x76,0x65,0x72,0x3a,0x20,0x72,0x63,0x65,0x6c,0x6c,0x0d,0x0a,0x0d,0x0a,0x31,0x30,0x37,0x0d,0x0a,0x7b,0x0a,0x20,0x20,0x20,0x22,0x63,0x6f,0x64,0x65,0x22,0x20,0x3a,0x20,0x32,0x30,0x30,0x2c,0x0a,0x20,0x20,0x20,0x22,0x72,0x65,0x73,0x75,0x6c,0x74,0x22,0x20,0x3a,0x20,0x7b,0x0a,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x61,0x64,0x64,0x72,0x65,0x73,0x73,0x22,0x20,0x3a,0x20,0x22,0x31,0x39,0x32,0x2e,0x31,0x36,0x38,0x2e,0x32,0x2e,0x31,0x36,0x36,0x22,0x2c,0x0a,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x70,0x65,0x72,0x6d,0x69,0x73,0x73,0x69,0x6f,0x6e,0x22,0x20,0x3a,0x20,0x22,0x61,0x64,0x6d,0x69,0x6e,0x22,0x2c,0x0a,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x70,0x6f,0x72,0x74,0x22,0x20,0x3a,0x20,0x22,0x35,0x33,0x35,0x39,0x33,0x22,0x2c,0x0a,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x74,0x69,0x6d,0x65,0x73,0x74,0x61,0x6d,0x70,0x22,0x20,0x3a,0x20,0x22,0x31,0x33,0x3a,0x38,0x3a,0x31,0x38,0x3a,0x33,0x37,0x38,0x22,0x2c,0x0a,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x74,0x6f,0x6b,0x65,0x6e,0x22,0x20,0x3a,0x20,0x22,0x36,0x34,0x36,0x34,0x38,0x42,0x42,0x41,0x37,0x37,0x41,0x37,0x37,0x36,0x34,0x46,0x35,0x36,0x41,0x30,0x45,0x39,0x32,0x34,0x41,0x38,0x37,0x35,0x44,0x22,0x2c,0x0a,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x75,0x73,0x65,0x72,0x22,0x20,0x3a,0x20,0x22,0x61,0x64,0x6d,0x69,0x6e,0x22,0x0a,0x20,0x20,0x20,0x7d,0x2c,0x0a,0x20,0x20,0x20,0x22,0x73,0x74,0x61,0x74,0x75,0x73,0x22,0x20,0x3a,0x20,0x22,0x73,0x75,0x63,0x63,0x65,0x73,0x73,0x22,0x0a,0x7d,0x0a,0x0d,0x0a,0x30,0x0d,0x0a,0x0d,0x0a};
//unsigned char token_arr[1500] = {0x27,0x45,0x29,0x74,0x40,0x40,0x3D,0x50,0x59,0x78,0x45,0x2C,0x50,0x72,0x7B,0x48,0x54,0x54,0x50,0x2F,0x31,0x2E,0x31,0x20,0x32,0x30,0x30,0x20,0x4F,0x4B,0x0D,0x0A,0x53,0x65,0x74,0x2D,0x43,0x6F,0x6F,0x6B,0x69,0x65,0x3A,0x20,0x74,0x6F,0x6B,0x65,0x6E,0x3D,0x31,0x45,0x34,0x35,0x31,0x36,0x41,0x34,0x34,0x36,0x42,0x45,0x32,0x42,0x39,0x33,0x31,0x46,0x36,0x30,0x39,0x33,0x43,0x43,0x37,0x44,0x46,0x39,0x45,0x32,0x41,0x3B,0x20,0x4D,0x61,0x78,0x2D,0x41,0x67,0x65,0x3D,0x33,0x30,0x30,0x3B,0x20,0x50,0x61,0x74,0x68,0x3D,0x2F,0x3B,0x20,0x53,0x65,0x63,0x75,0x72,0x65,0x0D,0x0A,0x43,0x61,0x63,0x68,0x65,0x2D,0x43,0x6F,0x6E,0x74,0x72,0x6F,0x6C,0x3A,0x20,0x6E,0x6F,0x2D,0x63,0x61,0x63,0x68,0x65,0x0D,0x0A,0x43,0x6F,0x6E,0x74,0x65,0x6E,0x74,0x2D,0x74,0x79,0x70,0x65,0x3A,0x20,0x61,0x70,0x70,0x6C,0x69,0x63,0x61,0x74,0x69,0x6F,0x6E,0x2F,0x6A,0x73,0x6F,0x6E,0x0D,0x0A,0x54,0x72,0x61,0x6E,0x73,0x66,0x65,0x72,0x2D,0x45,0x6E,0x63,0x6F,0x64,0x69,0x6E,0x67,0x3A,0x20,0x63,0x68,0x75,0x6E,0x6B,0x65,0x64,0x0D,0x0A,0x44,0x61,0x74,0x65,0x3A,0x20,0x54,0x68,0x75,0x2C,0x20,0x30,0x31,0x20,0x4A,0x75,0x6E,0x20,0x32,0x30,0x31,0x37,0x20,0x31,0x31,0x3A,0x33,0x39,0x3A,0x33,0x36,0x20,0x47,0x4D,0x54,0x0D,0x0A,0x53,0x65,0x72,0x76,0x65,0x72,0x3A,0x20,0x72,0x63,0x65,0x6C,0x6C,0x0D,0x0A,0x0D,0x0A,0x31,0x30,0x39,0x0D,0x0A,0x7B,0x0D,0x0A,0x20,0x20,0x20,0x22,0x63,0x6F,0x64,0x65,0x22,0x20,0x3A,0x20,0x32,0x30,0x30,0x2C,0x0D,0x0A,0x20,0x20,0x20,0x22,0x72,0x65,0x73,0x75,0x6C,0x74,0x22,0x20,0x3A,0x20,0x7B,0x0D,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x61,0x64,0x64,0x72,0x65,0x73,0x73,0x22,0x20,0x3A,0x20,0x22,0x31,0x39,0x32,0x2E,0x31,0x36,0x38,0x2E,0x32,0x2E,0x31,0x36,0x36,0x22,0x2C,0x0D,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x70,0x65,0x72,0x6D,0x69,0x73,0x73,0x69,0x6F,0x6E,0x22,0x20,0x3A,0x20,0x22,0x61,0x64,0x6D,0x69,0x6E,0x22,0x2C,0x0D,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x70,0x6F,0x72,0x74,0x22,0x20,0x3A,0x20,0x22,0x35,0x33,0x35,0x39,0x33,0x22,0x2C,0x0D,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x74,0x69,0x6D,0x65,0x73,0x74,0x61,0x6D,0x70,0x22,0x20,0x3A,0x20,0x22,0x31,0x33,0x3A,0x36,0x3A,0x34,0x30,0x3A,0x38,0x31,0x39,0x22,0x2C,0x0D,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x74,0x6F,0x6B,0x65,0x6E,0x22,0x20,0x3A,0x20,0x22,0x31,0x45,0x34,0x35,0x31,0x36,0x41,0x34,0x34,0x36,0x42,0x45,0x32,0x42,0x39,0x33,0x31,0x46,0x36,0x30,0x39,0x33,0x43,0x43,0x37,0x44,0x46,0x39,0x45,0x32,0x41,0x22,0x2C,0x0D,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x22,0x75,0x73,0x65,0x72,0x22,0x20,0x3A,0x20,0x22,0x61,0x64,0x6D,0x69,0x6E,0x22,0x0D,0x0A,0x20,0x20,0x20,0x7D,0x2C,0x0D,0x0A,0x20,0x20,0x20,0x22,0x73,0x74,0x61,0x74,0x75,0x73,0x22,0x20,0x3A,0x20,0x22,0x73,0x75,0x63,0x63,0x65,0x73,0x73,0x22,0x0D,0x0A,0x7D,0x0D,0x0A,0x0D,0x0A,0x30};
//unsigned int token_arr_byte; 
int LTE_sock_num = 0;
unsigned int socket_number = 0;
unsigned int lte_no_of_retry = 0;
void LTE_login (void);

static void dns_cbfunc1 (U8 event, U8 *ip) {
  switch (event) {
    case DNS_EVT_SUCCESS:
				LTE_IpAdr[0] = ip[0];
				LTE_IpAdr[1] = ip[1];
				LTE_IpAdr[2] = ip[2];
				LTE_IpAdr[3] = ip[3];
		os_evt_set(EVE_DNS_IP_RESOLVED,t_4G_modem); 
		break;
    case DNS_EVT_NONAME:
  //    printf("Host name does not exist.\n");

		os_evt_set(EVE_DNS_IP_NONAME,t_4G_modem); 
      break;
    case DNS_EVT_TIMEOUT:
    //  printf("DNS Resolver Timeout expired, Host IP not resolved.\n");

		  os_evt_set(EVE_DNS_IP_TIMEOUT,t_4G_modem); 
      break;
    case DNS_EVT_ERROR:
   //   printf("DNS Resolver Error, check the host name, labels, etc.\n");
			os_evt_set(EVE_DNS_IP_ERROR,t_4G_modem); 
      break;
  }
}

static U16 prvxTCPPortHTTPCallback1(U8 soc, U8 event, U8 *ptr, U16 par)
{
    U16 retval = 0;

    switch (event)
    {
    case TCP_EVT_CONREQ:

        break;
    case TCP_EVT_ABORT:

      break;
    case TCP_EVT_CONNECT:
			retval = 1;
      break;
    case TCP_EVT_CLOSE:

      break;
    case TCP_EVT_ACK:
			os_evt_set(EVE_LTE_ACK,t_4G_modem);
			retval = 1;
      break;
    case TCP_EVT_DATA:
		  //Check HTTP response from PCS
			if( par < sizeof(LTE_HTTP_RX_Buffer))
      {
			  memset(LTE_HTTP_RX_Buffer,'\0',sizeof(LTE_HTTP_RX_Buffer)); //added by DK on 16 Dec 2014 
				memcpy( LTE_HTTP_RX_Buffer, ptr, par );
			  usLteClientRcvBufferLen = par;
				os_evt_set(EVE_LTE_RES,t_4G_modem);
			}
			break;
		case TCP_STATE_LAST_ACK:
// 			tcp_close(socket_num);
       break;

  }
  return retval;

}

__task void fourG_modem (void)
{
			memset(rssi_buf,'\0',sizeof(rssi_buf));
		memset(signal_strength_buf,'\0',sizeof(signal_strength_buf));		
		memset(imei_buffer,'\0',sizeof(imei_buffer));		
	while(1)
	{
		os_itv_set(HTTP_CLIENT_INTERVAL*100);
		os_itv_wait();
		LPC_RTC->GPREG4 |= (0x01<<9);
		if((Pcm_var.PCM_connection_select == Screen213_str5)||(Pcm_var.PCM_connection_select ==Screen213_str3))
		{

			LTE_login();
		}
	}
}


void LTE_login (void)
{
	unsigned char /*temp_buff[100],*/*ptr,lte_sock_state,*http_ptr;
	unsigned int i;
  U16 length;
	BOOL ret_val ;
	OS_RESULT result;
	U16 evt_flags;
	ptr = &LTE_HTTP_RX_Buffer[0];

// 			memset(rssi_buf,'\0',sizeof(rssi_buf));
// 		memset(signal_strength_buf,'\0',sizeof(signal_strength_buf));		
// 		memset(imei_buffer,'\0',sizeof(imei_buffer));	
	
	sprintf(&LTE_host_name[0], "http://%d.%d.%d.%d",Pcm_var.FourG_IP_Addr.Addr1,Pcm_var.FourG_IP_Addr.Addr2,\
									Pcm_var.FourG_IP_Addr.Addr3,Pcm_var.FourG_IP_Addr.Addr4);	
//	strcpy(LTE_host_name,"http://192.168.2.2");
//	sprintf(LTE_host_name,"http://%d.%d.%d.%d",Pcm_var.Gateway.Addr1,Pcm_var.Gateway.Addr2,Pcm_var.Gateway.Addr3,Pcm_var.Gateway.Addr4);
	
//	Http_Get_Read_signal_strength(HTTP_RX_Buffer, 1000);
// 								ptr = &HTTP_RX_Buffer[0];  
// 								if(0!= (http_ptr = HTTPStrCaseStr(ptr,sizeof(Res_Header_3),Res_Header_3)))
// 								{	
// 									http_ptr+=(sizeof(Res_Header_3) + 3);
// 									for(i=0;i<35;i++)
// 									{
// 										if(*http_ptr != ',')
// 										{
// 										token_fourG[i] = *http_ptr;
// 										http_ptr++;
// 										}
// 										else
// 										{
// 											token_fourG[i-1] = '\0';
// 											break;
// 										}
// 									}
// 							os_mut_wait (&usb_log_mutex, 0xffff);							
// 							strcpy(&system_log.log_buf[0],&token_fourG[0]);
// 							log_file_write(system_log1);	
// 							os_mut_release (&usb_log_mutex);										
// 									
// 								}	
//  								Http_Get_signal_strength(LTE_sock_num,&HTTP_TX_Buffer[0]);
	if(four_g_ip_change_fg==1)
	{
		four_g_ip_change_fg = 0;
		LTE_connect = 0;
		LTE_ip_resolved = 0;
		LTE_login_fg = 0;
		memset(rssi_buf,'\0',sizeof(rssi_buf));
		memset(signal_strength_buf,'\0',sizeof(signal_strength_buf));		
	}
	if((LTE_connect==0)&&(LTE_ip_resolved == 0))
	{
//								strcpy(HTTP_RX_Buffer,"token:1E4516A446BE2B931F6093CC7DF9E2A,.user");
								

			os_evt_clr(EVE_DNS_IP_RESOLVED | EVE_DNS_IP_NONAME | EVE_DNS_IP_TIMEOUT | EVE_DNS_IP_ERROR,t_4G_modem);					
			os_mut_wait (&usb_log_mutex, 0xffff);	
						if((1<<18)!= (msg_log_reg & (1<<18)))
						{		
							msg_log_reg |= 1<<18;					
			sprintf(&system_log.log_buf[0],"DNS IP Resolution of 4GLTE in process......");
			log_file_write(system_log1);	
						}
			os_mut_release (&usb_log_mutex);		
		get_host_by_name ((U8*)&LTE_host_name[7],dns_cbfunc1);
		
	
			result = os_evt_wait_or(EVE_DNS_IP_RESOLVED | EVE_DNS_IP_NONAME | EVE_DNS_IP_TIMEOUT | EVE_DNS_IP_ERROR,800); 
			if(result != OS_R_EVT)  //Added by DK on 5 Jan 2015 
			{
			os_mut_wait (&usb_log_mutex, 0xffff);		
						if((1<<19)!= (msg_log_reg & (1<<19)))
						{		
							msg_log_reg |= 1<<19;					
			sprintf(&system_log.log_buf[0],"DNS IP Resolution of 4G LTE  failed......");
			log_file_write(system_log1);	
						}
			os_mut_release (&usb_log_mutex);
			}
			else 
			{
				evt_flags = os_evt_get();
				switch (evt_flags)
				{
					case EVE_DNS_IP_RESOLVED:
						os_evt_clr(EVE_DNS_IP_RESOLVED,t_4G_modem); 				
						LTE_ip_resolved = 1; 
			os_mut_wait (&usb_log_mutex, 0xffff);	
						if((1<<20)!= (msg_log_reg & (1<<20)))
						{		
							msg_log_reg |= 1<<20;						
			sprintf(&system_log.log_buf[0],"DNS IP of 4G LTE Resolved ok......");
			log_file_write(system_log1);	
						}
			os_mut_release (&usb_log_mutex);				

					 break;
					 
					case EVE_DNS_IP_NONAME:
					os_evt_clr(EVE_DNS_IP_NONAME,t_4G_modem); 		
					break;
					
					case EVE_DNS_IP_TIMEOUT:
					os_evt_clr(EVE_DNS_IP_TIMEOUT,t_4G_modem); 			
					break;
					
					case EVE_DNS_IP_ERROR:
					os_evt_clr(EVE_DNS_IP_ERROR,t_4G_modem); 				
					break;
					
					default :
									 break;
					}
			}
		}	
	if((LTE_connect==0)&&(LTE_ip_resolved == 1))
	{
					
							os_mut_wait (&usb_log_mutex, 0xffff);	
						if((1<<21)!= (msg_log_reg & (1<<21)))
						{		
							msg_log_reg |= 1<<21;			
						sprintf(&system_log.log_buf[0],"Socket connection of 4G LTE in process......");
						log_file_write(system_log1);	
						}
						os_mut_release (&usb_log_mutex);		
			LTE_sock_num = tcp_get_socket( (TCP_TYPE_CLIENT | TCP_TYPE_KEEP_ALIVE),0,30,prvxTCPPortHTTPCallback1);
			
			if(!tcp_connect( LTE_sock_num,LTE_IpAdr,80,LTE_REM_PORT))
			{
				os_mut_wait (&usb_log_mutex, 0xffff);	
						if((1<<22)!= (msg_log_reg & (1<<22)))
						{		
							msg_log_reg |= 1<<22;							
				sprintf(&system_log.log_buf[0],"TCP Socket failed to connect to 4G LTE......");
				log_file_write(system_log1);	
						}
				os_mut_release (&usb_log_mutex);	
				
				tcp_abort(LTE_sock_num);   
				tcp_close( LTE_sock_num);
				tcp_release_socket(LTE_sock_num); 
					if(socket_number > 1)
					{	
					socket_number-=1;
					}						
			}
			else
			{
				os_dly_wait(100); 
				lte_sock_state = tcp_get_state (LTE_sock_num);  
				if(lte_sock_state == TCP_STATE_CONNECT)  //PCM is connected to PCS 
				{	
					os_mut_wait (&usb_log_mutex, 0xffff);		
						if((1<<23)!= (msg_log_reg & (1<<23)))
						{		
							msg_log_reg |= 1<<23;								
					sprintf(&system_log.log_buf[0],"TCP Socket connected to 4G LTE successfully......");
					log_file_write(system_log1);	
						}
					os_mut_release (&usb_log_mutex);					
					LTE_connect = 1;
					socket_number+=1;	
				}
				else
				{
					os_mut_wait (&usb_log_mutex, 0xffff);		
						if((1<<22)!= (msg_log_reg & (1<<22)))
						{		
							msg_log_reg |= 1<<22;								
					sprintf(&system_log.log_buf[0],"TCP Socket failed to connect to 4G LTE......");
					log_file_write(system_log1);	
						}
					os_mut_release (&usb_log_mutex);						
					tcp_abort(LTE_sock_num);  //Added by DK on 19 Dec 2014 
					tcp_close(LTE_sock_num);  
					tcp_release_socket(LTE_sock_num); 
					if(socket_number > 1)
					{	
					socket_number-=1;
					}
						
				}  
			} //end of else of if(!tcp_connect( pcs_sock_num,PCS_IpAdr,http_port_number,HTTP_CLIENT_LOCAL_PORT))
	}
// LTE_connect = 1;
// LTE_login = 0;		
	if((LTE_connect == 1)&&(LTE_login_fg == 0))
	{
		
////	os_mut_wait (&tcp_mutex, 0xffff);
	//			Task_section = 2;
			lte_sock_state = tcp_get_state (LTE_sock_num);  
//		lte_sock_state = TCP_STATE_CONNECT;
		
			if(lte_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS 
			{
					if(socket_number > 1)
					{	
					socket_number-=1;
					}				
				LTE_connect=0;
				tcp_abort(LTE_sock_num); //Added by DK on 19 Dec 2014 
				tcp_close(LTE_sock_num);  
				tcp_release_socket(LTE_sock_num); 
				lte_no_of_retry = 0;

			}
		//Prepare packet as per HTTP message(Java Script Object Notation)
			//else if(Http_Get_Request(pcs_sock_num))	//Commented by DK on 2 Jan 2015 
			else 
			{	
				memset(LTE_HTTP_TX_Buffer,'\0',sizeof(LTE_HTTP_TX_Buffer));
				os_evt_clr(EVE_LTE_RES,t_4G_modem);		
				
					os_mut_wait (&usb_log_mutex, 0xffff);	
						if((1<<24)!= (msg_log_reg & (1<<24)))
						{		
							msg_log_reg |= 1<<24;							
				sprintf(&system_log.log_buf[0],"4G LTE login in process......");
				log_file_write(system_log1);	
						}
				os_mut_release (&usb_log_mutex);	
LPC_RTC->ALSEC = 58;					
				
				if(Http_Get_LTE_login(LTE_sock_num,&LTE_HTTP_TX_Buffer[0]))
//				if(Http_Get_signal_strength(LTE_sock_num,&HTTP_TX_Buffer[0]))
				
				{
					result = os_evt_wait_or(EVE_LTE_RES,5000);
					os_evt_clr(EVE_LTE_RES,t_4G_modem);	
LPC_RTC->ALSEC = 59;					
					
					if(result == OS_R_EVT)
					{
						if(usLteClientRcvBufferLen > 0)
						{

								ptr = &LTE_HTTP_RX_Buffer[0];  
								if(0!= (http_ptr = HTTPStrCaseStr(ptr,sizeof(Res_Header_3),Res_Header_3)))
								{	
//									power_on_lte_login_fg = 0;
									os_mut_wait (&usb_log_mutex, 0xffff);		
						if((1<<25)!= (msg_log_reg & (1<<25)))
						{		
							msg_log_reg |= 1<<25;												
									sprintf(&system_log.log_buf[0],"4G LTE login successful......");
									log_file_write(system_log1);	
							
						}
									os_mut_release (&usb_log_mutex);										
									http_ptr+=(sizeof(Res_Header_3)+3 );
									for(i=0;i<35;i++)
									{
										if(*http_ptr != ',')
										{
										token_fourG[i] = *http_ptr;
										http_ptr++;
										}
										else
										{
											token_fourG[i-1] = '\0';
											break;
										}
									}
// ptr = &HTTP_RX_Buffer[0];
// 								if(0!= (http_ptr = HTTPStrCaseStr(ptr,sizeof(Res_Header_3),Res_Header_3)))
// 								{
// 									for(i=0;i<40;i++)
// 									{
// 										if(*http_ptr != ',')
// 										{
// 										system_log.log_buf[i] = *http_ptr;
// 										http_ptr++;
// 										}
// 										else
// 										{
// 											system_log.log_buf[i] = '\0';
// 											break;
// 										}									
// 									}										
// 								}	
									
// 							os_mut_wait (&usb_log_mutex, 0xffff);							
// 							strcpy(&system_log.log_buf[0],&token_fourG[0]);
// 							log_file_write(system_log1);	
// 							os_mut_release (&usb_log_mutex);										
								LTE_login_fg = 1;	
								}	
								else
								{

								}
							lte_no_of_retry = 0;	
						}						
					
					}
					else
					{
// 									lte_no_of_retry++;  
// 									if(lte_no_of_retry > 3) 
// 									{	
										LTE_connect=0;
										tcp_abort(LTE_sock_num); //Added by DK on 19 Dec 2014 
										tcp_close(LTE_sock_num);  
										tcp_release_socket(LTE_sock_num); 
										lte_no_of_retry = 0;
//									}									
						
					}					
				}

				
			}				
////	os_mut_release (&tcp_mutex);

	}

// LTE_connect = 1;
// LTE_login = 1;		
	if((LTE_connect == 1)&&(LTE_login_fg == 1))
	{
////			os_mut_wait (&tcp_mutex, 0xffff);
			lte_sock_state = tcp_get_state (LTE_sock_num);  
//		lte_sock_state = TCP_STATE_CONNECT;
			if(lte_sock_state != TCP_STATE_CONNECT)  //PCM is disconnected from PCS 
			{
			
				LTE_connect=0;
//				LTE_login_fg=0;
				tcp_abort(LTE_sock_num); //Added by DK on 19 Dec 2014 
				tcp_close(LTE_sock_num);  
				tcp_release_socket(LTE_sock_num); 
					if(socket_number > 1)
					{	
					socket_number-=1;
					}	
			}
		//Prepare packet as per HTTP message(Java Script Object Notation)
			else 
			{
				os_dly_wait(100); 		//2000
//				os_mut_wait (&tcp_mutex, 0xffff);			
				memset(LTE_HTTP_TX_Buffer,'\0',sizeof(LTE_HTTP_TX_Buffer));
				memset(LTE_HTTP_RX_Buffer,'\0',sizeof(LTE_HTTP_RX_Buffer));

				os_evt_clr(EVE_LTE_RES,t_4G_modem);		
				os_mut_wait (&usb_log_mutex, 0xffff);							
			sprintf(&system_log.log_buf[0],"Reading signal strength of 4G LTE in process......");
			log_file_write(system_log1);	
			os_mut_release (&usb_log_mutex);		
LPC_RTC->ALSEC = 60;					
				
				if(Http_Get_signal_strength(LTE_sock_num,&LTE_HTTP_TX_Buffer[0]))
				{
						result = os_evt_wait_or(EVE_LTE_RES,5000);
						os_evt_clr(EVE_LTE_RES,t_4G_modem);			
LPC_RTC->ALSEC = 61;					
					
						if(result == OS_R_EVT)
						{	
							if(usLteClientRcvBufferLen > 0)
							{
								if(Http_Get_Read_signal_strength(LTE_HTTP_RX_Buffer, usLteClientRcvBufferLen))
								{
									os_mut_wait (&usb_log_mutex, 0xffff);							
								sprintf(&system_log.log_buf[0],"Reading signal strength of 4G LTE is successful......");
								log_file_write(system_log1);	
								os_mut_release (&usb_log_mutex);									
									read_signal_strength_fg = 0;
								}
								lte_no_of_retry = 0;
							}
						}	
						else
						{
// 										lte_no_of_retry++;  
// 										if(lte_no_of_retry > 3) 
// 										{	
											LTE_connect=0;
											tcp_abort(LTE_sock_num); //Added by DK on 19 Dec 2014 
											tcp_close(LTE_sock_num);  
											tcp_release_socket(LTE_sock_num); 
											lte_no_of_retry = 0;
//										}
						}
						
				}	

					
			}	
////os_mut_release (&tcp_mutex);
	}		

//}
//}

}

BOOL Http_Get_LTE_login(U8 LTE_sock_num, char* ptr)
{
	char temp_buff[40];
  U16 length;
	BOOL ret_val;	
		strcpy(ptr,"GET ");
//		strcat(ptr,&LTE_host_name[0]);//"http://192.168.0.116");

		
		strcat(ptr,"/api/login?username=admin&password=admin123 HTTP/1.1");
		strcat(ptr,"\r\n");
		strcat(ptr,"Host: ");
		strcat(ptr,&LTE_host_name[7]);
		strcat(ptr,"\r\n");	
		strcat(ptr,"\r\n");
		length= strlen(ptr);
		//Send packet to LTE modem
		ret_val=tcp_send_data(LTE_sock_num, (U8*)ptr,length);

}

//by megha on 19/5/2017 to read signal stregth of 4G modem
BOOL Http_Get_signal_strength(U8 LTE_sock_num, char* ptr)
{
	char temp_buff[40];
  U16 length;
	BOOL ret_val;
	
	strcpy(ptr,"GET ");
//	strcat(ptr,&LTE_host_name[0]);
	strcat(ptr,"/api/stats/radio?");
	strcat(ptr,"token=");
	strcat(ptr,&token_fourG[0]); //Postman-Token
 	strcat(ptr," HTTP/1.1\r\n");
	strcat(ptr,"Host: ");
	strcat(ptr,&LTE_host_name[7]);
	strcat(ptr,"\r\n");	
	strcat(ptr,"\r\n");
	length= strlen(ptr);
  //Send packet to LTE modem
// 							os_mut_wait (&usb_log_mutex, 0xffff);							
// 							strcpy(&system_log.log_buf[0],&ptr[0]);
// 							log_file_write(system_log1);	
// 							os_mut_release (&usb_log_mutex);	
	ret_val=tcp_send_data(LTE_sock_num, (U8*)ptr,length);
	return ret_val;
}

BOOL Http_Get_Read_signal_strength(char* pcSrcBuffer, U16 rx_length)
{
	unsigned char *ptr,*http_ptr;
	U16 RecvValue=0,i,j,RecvBufPosn;
	BOOL ret_val=FALSE;
  unsigned char temp_buff[40];

	memcpy(arcLTEClientRcvBuffer,pcSrcBuffer,rx_length);
	
		memset(imei_buffer,'\0',sizeof(imei_buffer));		

	if(0 ==(ptr = HTTPStrCaseStr(arcLTEClientRcvBuffer,sizeof(Res_Header_1),Res_Header_1))) //no string "HTTP/1.1 200 OK"
	{
									
	}	
	else if(0 == (ptr = HTTPStrCaseStr(arcLTEClientRcvBuffer,sizeof(Res_Header_4),Res_Header_4))) //no "rssidBm" string								
	{
									
									
	}
	else 
	{

		RecvValue=0;
		ptr+=(sizeof(Res_Header_4)+2);
		memset(rssi_buf,'\0',sizeof(rssi_buf));	
		for(i=0;i<5;i++)
		{
			if(*(ptr+i)!=',')
			{
				rssi_buf[i]=*(ptr+i);
			}
			else 
			{
				
				rssi_buf[i] = '\0';
//				cell_modem_strength = atoi(signal_strength_buf);
//				strcat(signal_strength_buf,"dBm");
				break;
			}
		}	
							os_mut_wait (&usb_log_mutex, 0xffff);							
							strcpy(&system_log.log_buf[0],&rssi_buf[0]);
							log_file_write(system_log1);	
							os_mut_release (&usb_log_mutex);		
if(0 != (ptr = HTTPStrCaseStr(arcLTEClientRcvBuffer,sizeof(Res_Header_2),Res_Header_2))) //no "rssidBm" string								
	{		
				ptr+=(sizeof(Res_Header_2)+3);
		memset(signal_strength_buf,'\0',sizeof(signal_strength_buf));		
				for(i=0;i<3;i++)
				{
					if(*(ptr+i)!=',')
					{
						signal_strength_buf[i]=*(ptr+i);
					}
					else 
					{
						
						signal_strength_buf[i-1] = '\0';
		//				cell_modem_strength = atoi(signal_strength_buf);
		//				strcat(signal_strength_buf,"dBm");
						break;
					}
				}	
				signal_strength_buf[i] = '\0';
			}
if(0 != (ptr = HTTPStrCaseStr(arcLTEClientRcvBuffer,sizeof(Res_Header_5),Res_Header_5))) //no "rssidBm" string								
	{		
				ptr+=(sizeof(Res_Header_5)+3);
				memset(imei_buffer,'\0',sizeof(imei_buffer));		
				for(i=0;i<18;i++)
				{
					if(*(ptr+i)!=',')
					{
						imei_buffer[i]=*(ptr+i);
					}
					else 
					{
						
						imei_buffer[i-1] = '\0';
		//				cell_modem_strength = atoi(signal_strength_buf);
		//				strcat(signal_strength_buf,"dBm");
						break;
					}
				}	
			}			
							os_mut_wait (&usb_log_mutex, 0xffff);							
							strcpy(&system_log.log_buf[0],&signal_strength_buf[0]);
			
							log_file_write(system_log1);	
							os_mut_release (&usb_log_mutex);					
// 					if((i > 0) && (i < 4))
// 					{
// 						i = atoi(temp_buff);
// 						switch(i)
// 						{
// 							case 0 : //Found string "0" RSSI -115dBm or less
// 											cell_modem_strength = 1;
// 							break;
// 						
// 							case 1 : //Found string "10" RSSI -111dBm 
// 											cell_modem_strength = 2;
// 							break;
// 							
// 							case (31) : //Found string "31" RSSI -52dBm or Greater
// 											cell_modem_strength = 4;
// 							break;
// 							
// 							case (99) : //Found string "99" RSSI not known 
// 											cell_modem_strength = 5;
// 							break;
// 							
//               default:  //Found string "2...30" RSSI -110dBm ...-54dBm
// 										if((i > 1) && (i < 31))		
// 									cell_modem_strength = 3;
// 							break;
// 						}	
// 					}			
		
		
		
		
		
		

// //		RecvValue=atoi(temp_buff);	
// 		strcpy(signal_strength_buf,temp_buff);
// //			sprintf(&disp[0],"PCM Web: %s",Http_Host_Name);
		return(1);
	}	
	
	
	return(0);
}

