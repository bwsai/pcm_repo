/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     : Beltway
*
* @file Filename       : HTTP_Client.h
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid 
*
* @date Created        : November Wednesday,5 2014  <Nov 5, 2014>
* @date Last Modified  : November Wednesday,5 2014  <Nov 5, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __HTTP_CLIENT_H
#define __HTTP_CLIENT_H
/*============================================================================*/
#include "Modbus_Tcp_Master.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define HTTP_TX_BUFFER_LENGTH 1460
#define HTTP_RX_BUFFER_LENGTH 1460
#define HTTP_CLIENT_BUFFER_LENGTH 2048
#define HTTP_CLIENT_LOCAL_PORT 100 
#define PPP_CLIENT_LOCAL_PORT 10 
//Added by DK on 2 Jan 2015 
#define PPP_TX_BUFFER_LENGTH 1460  
#define PPP_RX_BUFFER_LENGTH 1460
#define LocM   localm[NETIF_ETH]
/*============================================================================
* Public Data Types
*===========================================================================*/


/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern unsigned char PCM_ID;
extern unsigned char PCS_IpAdr[4];

extern char HTTP_TX_Buffer[HTTP_TX_BUFFER_LENGTH];
extern char HTTP_RX_Buffer[HTTP_RX_BUFFER_LENGTH];

/* unsigned integer variables section */
extern CONNECTIVITY_STATUS pcs_connectivity_status; //Added by DK on 6 Feb 2015 
extern unsigned char Scale_configuration_received_ok_fg;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/

extern __task void http_client (void);
extern char *HTTPStrCaseStr(const char *pSrc,U32 nSrcLength,const char *pFind);
//extern BOOL Http_Get_Request(U8 pcs_sock_num); //Commented by DK on 2 Jan 2015 
BOOL Http_Get_Request(U8 pcs_sock_num, char* ptr);  //Added by DK on 2 Jan 2015 
extern BOOL Http_Get_Read_Response(char* pcSrcBuffer, U16 rx_length);
//extern BOOL Http_Post_Request(U8 pcs_sock_num, U16 ScaleNum); //Commented by DK on 2 Jan 2015 
extern BOOL Http_Post_Request(U8 pcs_sock_num, U16 ScaleNum, char* ptr); //Commented by DK on 2 Jan 2015 
extern BOOL Http_Post_File(U8 pcs_sock_num, U16 ScaleNum,unsigned int FileAvailable); //Added by DK on 2 Jan 2015 
extern __task void ppp_client (void);
extern BOOL Http_Post_Connectivity_Status(U8 pcs_sock_num, char* ptr); //Added by DK on 6 Feb 2015 for connectivity status update 
extern BOOL Http_Post_Heartbeat_message(U8 pcs_sock_num, char* ptr) ;//Added by Megha on 6 jan 2017 for sending heartbeat msg to server


#endif /*__HTTP_CLIENT_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
