/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     : Beltway
*
* @file Filename 	     : RTOS_main.h
* @brief			         : Controller Board
*
* @author			         : Dnyaneshwar Kashid 
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : December Thursday, 2014 <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 		       : 
* @internal 		       :
*
*****************************************************************************/
#ifndef __RTOS_MAIN_H
#define __RTOS_MAIN_H

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#define USB_TASK_PRIORITY                 3//1 
#define RTC_TASK_PRIORITY                 4//2 
#define DEBUG_TASK_PRIORITY								1//12

#ifdef REBOOT_EN
#define TIMEOUT_TASK_PRIORITY             3   //Added by DK on 3 May2016 to reset PCM every 45 minutes (i.e. 45 * 60 seconds) 
#endif 

#define TCP_MAIN_TASK_PRIORITY            6 
#define TCP_TICK_TASK_PRIORITY            7 
#define FTP_TASK_PRIORITY                 8 
#define MODBUS_TCP_MASTER_PRIORITY        11//9 
#define MB_TCP_M_THREAD_PRIORITY          10 
#define HTTP_CLIENT_PRIORITY      			  9//11
#define FOURG_MODEM_TASK_PRIORITY					12
#define PPP_CLIENT_PRIORITY      			  	9//11
#define GUI_TASK_PRIORITY                  5
#define KEYPAD_TASK_PRIORITY               6

#define FLASH_MUTEX_TIMEOUT 							50
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern unsigned char send_heartbeat_msg_fg;
extern OS_MUT FLash_Mutex;
/* unsigned integer variables section */
#ifdef USB
  extern OS_TID t_usb;
#endif
extern OS_TID  t_4G_modem;
extern OS_TID t_http_client;
extern OS_TID t_modbus_tcp_master;   
extern OS_TID t_rtc_tick;
extern OS_TID t_ppp_client;
extern OS_MUT debug_uart_mutex,usb_log_mutex;
extern OS_MUT tcp_mutex; //commented by DK on 23 Dec 2014 
extern OS_TID t_rtc_tick;
extern OS_TID t_ftp_client;  
extern OS_TID t_keypad;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern __task void init (void);
extern __task void ftp_client (void);
extern __task void fourG_modem (void);


#endif /*RTOS_MAIN*/
/*****************************************************************************
* End of file
*****************************************************************************/
