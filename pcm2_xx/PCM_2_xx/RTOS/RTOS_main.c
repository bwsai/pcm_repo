/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem 
* @detail Customer     : Beltway
*
* @file Filename       : Rtos_main.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid 
*
* @date Created        : October Tuesday, 2014  <Oct 07, 2014>
* @date Last Modified  : December Thursday, 2014 <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "RTOS_main.h"
#include "USB_main.h"
#include "rtc.h"
#include "Modbus_Tcp_Master.h"
#include "Ext_flash_high_level.h"
#include "EEPROM_high_level.h"
#include "Main.h"
#include "EMAC_LPC177x_8x.h"
#include "Ext_flash_low_level.h"
#include "HTTP_Client.h"
#include "sntp.h"
#include "mbport.h"
#include "mbm.h"
#include "I2C_driver.h"
#include "../GUI/Display_task.h"
#include "../GUI/Application/Screen_global_ex.h"
#include "../gui/application/Screen_data_enum.h"
#include "File_update.h"


#ifdef RTOS
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/
void log_edit_para(void);
/*============================================================================
*Public Variables
*===========================================================================*/
U8 disp_power_on_screen_timeout_fg,Power_on_screen_disp_cntr,FTP_msg_cntr;

/* Constants section */

/* Boolean variables section */

/* Character variables section */
unsigned char send_heartbeat_msg_fg,read_signal_strength_fg;
int sntp_cntr,signal_strength_cntr = 0;
unsigned int disp_utc_time_hr,disp_utc_time_min,disp_utc_time_sec;
unsigned int disp_utc_time_day,disp_utc_time_mon,disp_utc_time_year;

/*
#ifdef DEBUG_PORT
 __align(4) char Rbuff[100] __attribute__ ((section ("PCM_CFG_BUFF")));
#endif 
*/

/* unsigned integer variables section */
unsigned int Heartbeat_msg_cntr;
/* Signed integer variables section */
#ifdef MODBUS_TCP
OS_TID t_modbus_tcp_master;  
static U64 modbus_tcp_master_stack[10240/8] __attribute__ ((section ("MB_TCP_STACK")));  
OS_TID t_http_client;  
static U64 http_client_stack[10240/8] __attribute__ ((section ("HTTPC_STACK")));  
OS_TID t_ppp_client;  
static U64 ppp_client_stack[10240/8] __attribute__ ((section ("PPPC_STACK")));  
OS_TID t_ftp_client;  
static U64 ftp_client_stack[10240/8] __attribute__ ((section ("FTPC_STACK")));  
OS_TID t_4G_modem;  
static U64 fourG_modem_stack[10240/8] __attribute__ ((section ("FOURG_STACK")));

#endif //#ifdef MODBUS_TCP


#ifdef ETHERNET
OS_TID t_tick;
OS_TID t_tcp_task_main;
static U64 tcp_stack[10240/8] __attribute__ ((section ("TCP_STACK"))); 
#endif

#ifdef RTC
OS_TID t_rtc_tick;
static U64 rtc_stack[8192/8] __attribute__ ((section ("RTC_STACK")));
#endif

#ifdef USB
OS_TID t_usb;
static U64 usb_stack[8192/8] __attribute__ ((section ("USB_STACK"))); 
#endif

#ifdef DEBUG_TASK
OS_TID t_DebugTask_tick;
static U64 DebugTask_stack[1024/8] __attribute__ ((section ("DEBUG_TASK_STACK")));
#endif

#ifdef GUI
//static U64 gui_stack[1600/4];
static U64 gui_stack[8192/8] __attribute__ ((section ("GUI_TASK_STACK")));
OS_TID t_lcd_update;
#endif
#ifdef KEYPAD
//static U64 keypad_stack[1024/8];
OS_TID t_keypad;
static U64 keypad_stack[1024 / 8] __attribute__ ((section ("KEYPAD_TASK_STACK")));
#endif

OS_MUT debug_uart_mutex,usb_log_mutex;
OS_MUT tcp_mutex; 
OS_MUT FLash_Mutex;				//MSA on 13/12/16
/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
#ifdef REBOOT_EN
//Added by DK on 3 May2016 to reset PCM every 45 minutes (i.e. 45 * 60 seconds) 
//U32 SecondsCounter =0;
OS_TID timeout_task_id;
RTCTime Tmout_time;

__task void timeout_task (void)
{
  while(1) 
	{
		os_itv_set(TIMEOUT_TASK_INTERVAL);
		os_itv_wait();
	
		//Added by DK on 15 Jan2015 
		Tmout_time = RTCGetTime();
	
		//Added by DK on 5 May2016 to reset PCM every RTC time xx:45:30  
		if((Tmout_time.RTC_Min == 45) && (Tmout_time.RTC_Sec == 30))  
		{
		 //Added by DK on 18 May2016 to resolve issue of PCM lockup if cell mdoem is connected
      os_tsk_prio_self(HTTP_CLIENT_PRIORITY + 5);
			if(ppp_enable == 1 )
			{
        ppp_close();
				modem_init();
			}	
      tsk_lock();
			__disable_irq();
			LPC_EMAC->IntEnable &= ~INT_RX_DONE; //Disable EMAC rx int
			LPC_EMAC->IntEnable &= ~INT_TX_DONE; //Disable EMAC tx int
		  LPC_EMAC->Command &= (~CR_RX_EN); //Disable EMAC receiption 
			LPC_EMAC->Command &= (~CR_TX_EN); //Disable EMAC Transmission 
			LPC_EMAC->MAC1     &= ~MAC1_REC_EN;
			LPC_EMAC->IntClear  = 0xFFFF;     // Reset all interrupts 
			LPC_SC->RSTCON0 |= (1 << 3); // UART0 reset control bit
			LPC_SC->RSTCON0 |= (1 << 4); // UART1 reset control bit
			LPC_SC->RSTCON0 |= (1 << 30); // Ethernet block reset control bit
			NVIC_SystemReset();
	   	while(1);
		}
  }	
}
#endif 

/*****************************************************************************
* @note       Function name  : void change_ip_address (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 9th Dec 2014
* @brief      Description    : Copies the ip address, gateway address and subnet mask
*                            : to the ethernet structure. This will modify the IP address
*                            : and other parameters on the go.
* @note       Notes          : None
*****************************************************************************/
void change_ip_address (void)
{
		mem_copy ((U8 *)&localm[NETIF_ETH].IpAdr, (U8 *)&Pcm_var.IP_Addr, sizeof(Pcm_var.IP_Addr));
	  mem_copy ((U8 *)&localm[NETIF_ETH].NetMask, (U8 *)&Pcm_var.Subnet_Mask, sizeof(Pcm_var.Subnet_Mask));
    mem_copy ((U8 *)&localm[NETIF_ETH].DefGW, (U8 *)&Pcm_var.Gateway, sizeof(Pcm_var.Gateway));
	
// 	 mem_copy ((U8 *)&ip_config.IpAdr,(U8 *)&Pcm_var.IP_Addr, sizeof(Pcm_var.IP_Addr));
// 	 mem_copy ((U8 *)&ip_config.NetMask,(U8 *)&Pcm_var.Subnet_Mask, sizeof(Pcm_var.Subnet_Mask));
// 	 mem_copy ((U8 *)&ip_config.DefGW, (U8 *)&Pcm_var.Gateway,sizeof(Pcm_var.Gateway));	
    return;
}

/*****************************************************************************
* @note       Function name  : void dhcp_check (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 9th Dec 2014
* @brief      Description    : Copies the ip address, gateway address and subnet mask
*                            : to the ethernet structure. This will modify the IP address
*                            : and other parameters on the go.
* @note       Notes          : None
*****************************************************************************/
void dhcp_check (void)
{
    /* Monitor DHCP IP address assignment. */
    if (dhcp_tout == 0)
    {
       return;
    }
    if ((mem_test (&localm[NETIF_ETH].IpAdr, 0, IP_ADRLEN) == __FALSE )&& !(dhcp_tout & 0x80000000))
    {
       /* Success, DHCP has already got the IP address. */
			 mem_copy ((U8 *)&ip_config.IpAdr, (U8 *)&localm[NETIF_ETH].IpAdr,sizeof(ip_config.IpAdr));
			 mem_copy ((U8 *)&ip_config.NetMask,(U8 *)&localm[NETIF_ETH].NetMask, sizeof(ip_config.NetMask));
			 mem_copy ((U8 *)&ip_config.DefGW, (U8 *)&localm[NETIF_ETH].DefGW, sizeof(ip_config.DefGW));
			 mem_copy ((U8 *)&ip_config.PriDNS, (U8 *)&localm[NETIF_ETH].PriDNS, sizeof(ip_config.PriDNS));
			 mem_copy ((U8 *)&ip_config.SecDNS, (U8 *)&localm[NETIF_ETH].SecDNS, sizeof(ip_config.SecDNS));

			 mem_copy ((U8 *)&Pcm_var.IP_Addr,(U8 *)&ip_config.IpAdr, sizeof(ip_config.IpAdr));
			 mem_copy ((U8 *)&Pcm_var.Subnet_Mask,(U8 *)&ip_config.NetMask, sizeof(ip_config.NetMask));
			 mem_copy ((U8 *)&Pcm_var.Gateway,(U8 *)&ip_config.DefGW, sizeof(ip_config.DefGW));
			 mem_copy ((U8 *)&Pcm_var.PriDNS,(U8 *)&ip_config.PriDNS, sizeof(ip_config.PriDNS));
			 mem_copy ((U8 *)&Pcm_var.SecDNS,(U8 *)&ip_config.SecDNS, sizeof(ip_config.SecDNS));

			
			
			//Added by DK on 11 Feb 2015 for workaround as TCPnet goes in auto ip mode if doesn't receive IP address from 
			//DHCP server in 60 seconds 
			if(ip_config.IpAdr[0] !=169) //non loopback address 
			{	
				dhcp_tout = 0;
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);	
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "PCM IP %d.%d.%d.%d!\r\n",ip_config.IpAdr[0],ip_config.IpAdr[1],ip_config.IpAdr[2],ip_config.IpAdr[3]);
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "PCM NetMask %d.%d.%d.%d!\r\n",ip_config.NetMask[0],ip_config.NetMask[1],ip_config.NetMask[2],ip_config.NetMask[3]);
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			  memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "PCM DefGW  %d.%d.%d.%d!\r\n",ip_config.DefGW[0],ip_config.DefGW[1],ip_config.DefGW[2],ip_config.DefGW[3]);
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			  memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "PCM PriDNS %d.%d.%d.%d!\r\n",ip_config.PriDNS[0],ip_config.PriDNS[1],ip_config.PriDNS[2],ip_config.PriDNS[3]);
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				sprintf(Debug_Buf, "PCM SecDNS %d.%d.%d.%d!\r\n",ip_config.SecDNS[0],ip_config.SecDNS[1],ip_config.SecDNS[2],ip_config.SecDNS[3]);
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
			  os_mut_release (&debug_uart_mutex);
				#endif
			  return;
			}	
    }
    if (--dhcp_tout == 0)
    {
       /* A timeout, disable DHCP and use static IP address. */
       dhcp_disable();
       change_ip_address();
       //dhcp_tout = 30 | 0x80000000; //commented by DK on 11 Feb 2015 
			 dhcp_tout = 0x80000000;  //Added by DK on 11 Feb 2015 
       return;
    }
    if (dhcp_tout == 0x80000000)
    {
       dhcp_tout = 0;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void dhcp_chk_enable_disable (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Dnyaneshwar Kashid 
* @date       date created   : 9th Dec 2014
* @brief      Description    : Copies the ip address, gateway address and subnet mask
*                            : to the ethernet structure. This will modify the IP address
*                            : and other parameters on the go.
* @note       Notes          : None
*****************************************************************************/
void dhcp_chk_enable_disable (void)
{
    //if DHCP is enabled
	  if(Pcm_var.DHCP_Status == Screen22_str14) 
    {
       dhcp_disable();//disable dhcp
       dhcp_init(); //restart dhcp
       dhcp_tout = DHCP_TOUT;
				dhcp_enable =1;

    }
    else if(Pcm_var.DHCP_Status == Screen22_str15) //if DHCP is disabled
    {
       dhcp_disable(); //disable dhcp
       change_ip_address(); //initialize with the default IP address, subnet mask and gateway address
			dhcp_enable =0;
			
    }
    return;
}

#ifdef RTC
/*****************************************************************************
* @note       Function name: __task void get_time (void)
* @returns    returns      : None
* @param      arg1         : None
* @param      arg2         : None
* @author                  : Dnyaneshwar Kashid 
* @date       date created :
* @brief      Description  : Gets the updated time from the RTC, populates all
*                          : reports every minute, updates the periodic log
*                          : according to the time specified (if logging is enabled)
*                          : and also updates the reports depending on the time.
* @note       Notes        : None
*****************************************************************************/
unsigned char task1_fg;
__task void get_time (void)
{
	int index;
	static RTCTime Prev_time;
	unsigned int Periodic_log_interval_1Sec;
	unsigned int prev_day, prev_week, prev_month, prev_year;
	//  static double prev_weight=0;
	//int i=0;	
	Current_time = RTCGetTime();
	Prev_time = Current_time;
	//SKS ->new variables added to be saved in
	//Genral purpose registers with power backup (RTC VBAT) <--
	prev_day = LPC_RTC->GPREG3;
	prev_week = LPC_RTC->GPREG4;
	prev_month = LPC_RTC->ALDOM;
	prev_year = LPC_RTC->ALYEAR;
	while(1)
  {


// 		prev_week = LPC_RTC->GPREG4;

// 		prev_month = LPC_RTC->ALMON;
// 		prev_year = LPC_RTC->ALYEAR;
			if(!task1_fg)
		{
			task1_fg = 1;
		}
		else
		{
			task1_fg = 0;
		}		
		
		os_itv_set(RTC_GET_TIME_TASK_INTERVAL);
		os_itv_wait();
		LPC_RTC->GPREG4 |= (0x01<<3);
			if(!task1_fg)
		{
			task1_fg = 1;
		}
		else
		{
			task1_fg = 0;
		}
		//Added by DK on 15 Jan2015 
		Current_time = RTCGetTime();
		Pcm_var.Config_Time.Sec = Current_time.RTC_Sec;
		Pcm_var.Config_Time.Min = Current_time.RTC_Min;
		Pcm_var.Config_Time.Hr = Current_time.RTC_Hour;		
		Pcm_var.Config_Date.Day = Current_time.RTC_Mday;
		Pcm_var.Config_Date.Mon = Current_time.RTC_Mon;
		Pcm_var.Config_Date.Year = Current_time.RTC_Year;
		
		//if((Current_time.RTC_Hour == 0) && (Current_time.RTC_Min == 0) && (Current_time.RTC_Sec == 0)) //Commented by DK on 10 March 2015 
		if((Current_time.RTC_Hour == 2) && (Current_time.RTC_Min == 0) && (Current_time.RTC_Sec > 30)) //Added by DK on 10 March 2015 
		{
			if(sntp_cntr == 0)
			{
				get_sntp_time=1;
			}
			sntp_cntr++;
		}
		if((Current_time.RTC_Hour == 3) && (Current_time.RTC_Min == 0))// && (Current_time.RTC_Sec == 0)) //Added by DK on 10 March 2015 
		{
				sntp_cntr = 0;
		}			
//MSA send heartbeat message to server for every minute	5/1/2017
		if((Current_time.RTC_Min != Prev_time.RTC_Min)) //Added by DK on 10 March 2015 
		{
//			if(read_signal_strength_fg==0)
			{
//				read_signal_strength_fg = 1;
			}
			if((power_on_fg == 1)&&(Scale_configuration_received_ok_fg==0))
			{
				if(Power_on_screen_disp_cntr < POWER_ON_SCREEN_DISP_COUNT)
				{
					Power_on_screen_disp_cntr++;
				}
				else
				{
					power_on_fg = 0;
					disp_power_on_screen_timeout_fg = 1;

				}
			}
			if(FTP_msg_cntr < FTP_MSG_COUNT)
			{
				FTP_msg_cntr++;
				
			}
			else
			{
				FTP_msg_cntr = 0;
				if(Modbus_Tcp_Slaves[1].rtc_update == 0)
				{
//					Modbus_Tcp_Slaves[1].rtc_update = 1;
				}
				if(!ftp_send_fg)
				{
			//		ftp_send_fg = 1;
				}				
			}			

			
			if(Heartbeat_msg_cntr < HEARTBEAT_COUNT)
			{
				Heartbeat_msg_cntr++;
				
			}
			else
			{
				Heartbeat_msg_cntr = 0;
// added by megha to display utc in heartbeat msg	

					if(sntp_request1())
					{
						get_utc();

					}	
						send_heartbeat_msg_fg = 1;
					
			}
			if(signal_strength_cntr < READ_SIGNAL_STRENGTH_COUNT)
			{
					signal_strength_cntr++;
			}
			else
			{
					signal_strength_cntr = 0;
//					read_signal_strength_fg = 1;
			}
			
		}
		// read RTC from SNTP server  
	 	if(get_sntp_time == 1)
		{
			if(sntp_request())
			{
//				Current_time = RTCGetTime();
							
				
				#ifdef DEBUG_PORT
				os_mut_wait (&debug_uart_mutex, 0xffff);	
				memset(Debug_Buf,'\0',sizeof(Debug_Buf));
				//strcpy(Debug_Buf, "PCM RTC Updated!\r\n"); //commented by DK on 15 Jan2015 
				//Added by DK on 15 Jan2015 
				sprintf(Debug_Buf, "PCM RTC Updated! Month:%02d Date:%02d Year:%04d \
                      Hour:%02d  Minute:%02d Second:%02d\r\n",Current_time.RTC_Mon,\
                      Current_time.RTC_Mday,Current_time.RTC_Year,Current_time.RTC_Hour,\
                      Current_time.RTC_Min,Current_time.RTC_Sec);
				
				Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
				os_mut_release (&debug_uart_mutex);
				#endif
				get_sntp_time=0;
				for(index=0; index < MAX_NO_OF_INTEGRATORS; index++)
					Modbus_Tcp_Slaves[index].rtc_update = 1;
			}
		}  
	
		
		
		
		
		//Commented by DK on 15 Jan2015 
	/*	else 
		{
			Current_time = RTCGetTime();
			if((Current_time.RTC_Hour == 0) && (Current_time.RTC_Min == 0) && (Current_time.RTC_Sec == 0))
			{
					get_sntp_time=1;
			}	
		}*/
		Prev_time = Current_time;
		LPC_RTC->GPREG4 &= ~(0x01<<3);
//		WDTFeed();	
	} //end of while(1)
}
#endif



#ifdef DEBUG_TASK
/*****************************************************************************
* @note       Function name: __task void debug_task (void)
* @returns    returns      : None
* @param      arg1         : None
* @param      arg2         : None
* @author                  : Aniket Mullur 
* @date       date created :
* @brief      Description  : Gets the updated time from the RTC, populates all
*                          : reports every minute, updates the periodic log
*                          : according to the time specified (if logging is enabled)
*                          : and also updates the reports depending on the time.
* @note       Notes        : None
*****************************************************************************/
__task void debug_task (void)
{
	int index;

	while(1)
  {
		
		os_itv_set(DEBUG_TASK_RUN_INTERVAL);
		os_itv_wait();
	
//		Write_Debug_Logs_into_file(1,(unsigned char *)1,1);
		
	} //end of while(1)
}
#endif



#ifdef ETHERNET
/*****************************************************************************
* @note       Function name: __task void tcp_task_main (void)
* @returns    returns      : None
* @param      arg1         : None
* @param      arg2         : None
* @author                  : Dnyaneshwar Kashid 
* @date       date created : Sep 2014
* @brief      Description  : task runs the main thread for RL-TCPnet
* @note       Notes        : None
*****************************************************************************/
__task void tcp_task_main (void)
{
	  U32 regv;
	  static U8 link_up = 0;

	  dhcp_tout = DHCP_TOUT;  
	  while (1)
    {

        os_itv_set(ETHERNET_MAIN_TASK_INTERVAL);
        os_itv_wait();
			LPC_RTC->GPREG4 |= (0x01<<2);			
			Task_section = 1;
	
        main_TcpNet();
			
			//if DHCP is enabled
			  if(Pcm_var.DHCP_Status == Screen22_str14) 
				{
					dhcp_enable = 1;
					 dhcp_check();
				}
				Task_section = 2;
				regv = read_PHY (PHY_REG_BSR);
				Task_section = 3;
				if ((regv & 0x0004) && (link_up == 0))
				{
					Task_section = 4;
					/* Link is on. */
					dhcp_chk_enable_disable();  
					link_up = 1;
				}
				else if (!(regv & 0x0004)) //link is down
				{
					Task_section = 5;
					link_up = 0;
				}
//				WDTFeed();	
				LPC_RTC->GPREG4 &= ~(0x01<<2);
    }
}

/*****************************************************************************
* @note       Function name: __task void tcp_tick (void)
* @returns    returns      : None
* @param      arg1         : None
* @param      arg2         : None
* @author                  : Dnyaneshwar Kashid 
* @date       date created : Sep 2014
* @brief      Description  : task generates periodic ticks for RL-TCPnet
* @note       Notes        : None
*****************************************************************************/
__task void tcp_tick (void)
{
	LPC_RTC->GPREG4;
	
    while (1)
    {
			
		
        os_itv_set(ETHERNET_TCP_TICK_TASK_INTERVAL);
        os_itv_wait();
LPC_RTC->GPREG4 |= (0x01<<1);			
        /* Timer tick every xxx ms */
        timer_tick ();
			if (GUI_data_nav.GUI_structure_backup == 1) {
//				log_edit_para();
				if (OS_R_OK == os_mut_wait(&FLash_Mutex, FLASH_MUTEX_TIMEOUT)) {
					//	if(enable_eeprom_struc_write_flag == 1) //Added on 27 April 2016
					{
						eeprom_struct_backup();
						Restore_values_to_original_pcm_variables();
						//os_mut_release (&FLash_Mutex);
						GUI_data_nav.GUI_structure_backup = 0;
						//		enable_eeprom_struc_write_flag = 0; //Added on 27 April 2016
					}
				}
				os_mut_release(&FLash_Mutex);
			}	
	//WWDT_Cmd(DISABLE);
// WDTFeed();			
			LPC_RTC->GPREG4 &= ~(0x01<<1);
    }
}
#endif

/*****************************************************************************
* @note       Function name: __task void init (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Dnyaneshwar Kashid 
* @date       date created :
* @brief      Description  : Creates all the tasks with respective priorities
* @note       Notes        : None
*****************************************************************************/
__task void init (void)
{
	LPC_RTC->GPREG4 =0;
	
	WDTInit();			
	LPC_RTC->GPREG4 |= (0x01<<0);
	get_sntp_time=1;
	read_signal_strength_fg = 1;
	os_mut_init (&debug_uart_mutex);
  os_mut_release (&debug_uart_mutex);
	os_mut_init (&usb_log_mutex);
  os_mut_release (&usb_log_mutex);	
	os_mut_init (&tcp_mutex);  
  os_mut_release (&tcp_mutex); 
	
	#ifdef REBOOT_EN
	//Added by DK on 3 May2016 to reset PCM every 45 minutes (i.e. 45 * 60 seconds) 
  timeout_task_id = os_tsk_create (timeout_task, TIMEOUT_TASK_PRIORITY);
	#endif 
	
	#ifdef ETHERNET
	t_tick = os_tsk_create (tcp_tick, TCP_TICK_TASK_PRIORITY);
	t_tcp_task_main = os_tsk_create_user (tcp_task_main, TCP_MAIN_TASK_PRIORITY, &tcp_stack, sizeof(tcp_stack));
	#endif

	#ifdef USB
  t_usb = os_tsk_create_user (usb_task, USB_TASK_PRIORITY, &usb_stack, sizeof(usb_stack));
	
	#endif
	
	#ifdef MODBUS_TCP
  t_modbus_tcp_master = os_tsk_create_user (modbus_tcp_master, MODBUS_TCP_MASTER_PRIORITY, &modbus_tcp_master_stack, sizeof(modbus_tcp_master_stack));
//	if(ppp_enable == 0 )
//	if((Pcm_var.PCM_connection_select == Screen213_str1)||(Pcm_var.PCM_connection_select == Screen213_str5))//LAN or Verizon 4G selection
	t_http_client = os_tsk_create_user (http_client, HTTP_CLIENT_PRIORITY, &http_client_stack, sizeof(http_client_stack));
//	else if(Pcm_var.PCM_connection_select == Screen213_str3)//2G hspa select
//	t_ppp_client = os_tsk_create_user (ppp_client, PPP_CLIENT_PRIORITY, &ppp_client_stack, sizeof(ppp_client_stack));
	#endif
	if((Pcm_var.PCM_connection_select == Screen213_str5)||(Pcm_var.PCM_connection_select == Screen213_str3))
	{
		t_4G_modem = os_tsk_create_user (fourG_modem, FOURG_MODEM_TASK_PRIORITY, &fourG_modem_stack, sizeof(fourG_modem_stack));

	}
	#ifdef RTC
	t_rtc_tick = os_tsk_create_user (get_time, RTC_TASK_PRIORITY, &rtc_stack, sizeof(rtc_stack));
	#endif
	
	#ifdef  DEBUG_TASK
	t_DebugTask_tick = os_tsk_create_user (debug_task, DEBUG_TASK_PRIORITY, &DebugTask_stack, sizeof(DebugTask_stack));
	#endif
	
	t_ftp_client = os_tsk_create_user (ftp_client, FTP_TASK_PRIORITY, &ftp_client_stack, sizeof(ftp_client_stack));
#ifdef GUI
    t_lcd_update = os_tsk_create_user (update_lcd, GUI_TASK_PRIORITY, &gui_stack, sizeof(gui_stack));		//added by megha on 28/10/2016
#endif	
	
#ifdef KEYPAD
	t_keypad = os_tsk_create_user(Keypad_task, KEYPAD_TASK_PRIORITY,
			&keypad_stack, sizeof(keypad_stack));
#endif	
	OS_Init_Done_Flag = 1;	
//  WDTFeed();			
	LPC_RTC->GPREG4 &= (~(0x01<<0));	
	os_tsk_delete_self();

	
}
void log_edit_para(void)
{
	os_mut_wait (&usb_log_mutex, 0xffff);							
	
	switch(parameter_code)
	{
 		case	PCM_ID_CODE:
						sprintf(&edit_log.edit_log_buf[0],"PCM ID = %d",Pcm_var.PCM_id);
						log_file_write(edit_log1);	

 						break;
		case	PCM_WEB_SELECT_CODE:
	
						if(Pcm_var.PCM_web_select==Screen212_str1)
						{
							sprintf(&edit_log.edit_log_buf[0],"Web site selected =  http://plantconnect-api-p.azurewebsites.net");

						}
						else
						{
							sprintf(&edit_log.edit_log_buf[0],"Web site selected =  http://plantconnect-api.azurewebsites.net");

						}
						log_file_write(edit_log1);	
						break;
		case	PCM_CONNECT_SELECT_CODE:
	
						if(Pcm_var.PCM_connection_select==Screen213_str1)
						{
							sprintf(&edit_log.edit_log_buf[0],"PCM connection = LAN");

						}
						else if(Pcm_var.PCM_connection_select==Screen213_str3)
						{
							sprintf(&edit_log.edit_log_buf[0],"PCM connection = Multitech HSPA");

						}
						else if(Pcm_var.PCM_connection_select==Screen213_str5)
						{
							sprintf(&edit_log.edit_log_buf[0],"PCM connection = Verizon 4g LTE");

						}
						log_file_write(edit_log1);	
						break;	
 		case	IP_ADDR_CODE:
						sprintf(&edit_log.edit_log_buf[0],"IP address = %03d:%03d:%03d:%03d",
										Pcm_var.IP_Addr.Addr1,Pcm_var.IP_Addr.Addr2,Pcm_var.IP_Addr.Addr3,Pcm_var.IP_Addr.Addr4);
						log_file_write(edit_log1);	

 						break;						
 		case	SUBNET_MASK_CODE:
						sprintf(&edit_log.edit_log_buf[0],"Subnet Mask = %03d:%03d:%03d:%03d",
										Pcm_var.Subnet_Mask.Addr1,Pcm_var.Subnet_Mask.Addr2,Pcm_var.Subnet_Mask.Addr3,Pcm_var.Subnet_Mask.Addr4);
						log_file_write(edit_log1);	

 						break;	
 		case	GATEWAY_CODE:
						sprintf(&edit_log.edit_log_buf[0],"Gateway address = %03d:%03d:%03d:%03d",
										Pcm_var.Gateway.Addr1,Pcm_var.Gateway.Addr2,Pcm_var.Gateway.Addr3,Pcm_var.Gateway.Addr4);
						log_file_write(edit_log1);	

 						break;	
 		case	PRI_DNS_CODE:
						sprintf(&edit_log.edit_log_buf[0],"Primary DNS address = %03d:%03d:%03d:%03d",
										Pcm_var.PriDNS.Addr1,Pcm_var.PriDNS.Addr2,Pcm_var.PriDNS.Addr3,Pcm_var.PriDNS.Addr4);
						log_file_write(edit_log1);	

 						break;		
 		case	SEC_DNS_CODE:
						sprintf(&edit_log.edit_log_buf[0],"Secondary DNS address = %03d:%03d:%03d:%03d",
										Pcm_var.SecDNS.Addr1,Pcm_var.SecDNS.Addr2,Pcm_var.SecDNS.Addr3,Pcm_var.SecDNS.Addr4);
						log_file_write(edit_log1);	

 						break;
 		case	PPP_STATUS_CODE:
						if(Pcm_var.PPP_Status==Screen24_str10)
						{			
							sprintf(&edit_log.edit_log_buf[0],"PPP Status = Disable");
						}
						else
						{
							sprintf(&edit_log.edit_log_buf[0],"PPP Status = Enable");

						}
						log_file_write(edit_log1);	

 						break;	
 		case	PPP_DIAL_CODE:
						sprintf(&edit_log.edit_log_buf[0],"PPP Dial no. = %s",Pcm_var.PPP_dial_no);
						log_file_write(edit_log1);	

 						break;	
 		case	DST_STATUS_CODE:
						if(Pcm_var.DST_Status==Screen25_str8)
						{			
							sprintf(&edit_log.edit_log_buf[0],"DST Status = Disable");
						}
						else
						{
							sprintf(&edit_log.edit_log_buf[0],"DST Status = Enable");

						}
						log_file_write(edit_log1);	

 						break;
 		case	GMT_CODE:
						sprintf(&edit_log.edit_log_buf[0],"GMT = %f",Pcm_var.GMT_value);
						log_file_write(edit_log1);	

 						break;	
 		case	DATE_CODE:
						sprintf(&edit_log.edit_log_buf[0],"DATE = %02d-%02d-%04d",Pcm_var.Config_Date.Day,Pcm_var.Config_Date.Mon,
																																			Pcm_var.Config_Date.Year);
						log_file_write(edit_log1);	

 						break;		
 		case	TIME_CODE:
						sprintf(&edit_log.edit_log_buf[0],"DATE = %02d-%02d-%02d",Pcm_var.Config_Time.Hr,Pcm_var.Config_Time.Min,
																																			Pcm_var.Config_Time.Sec);
						log_file_write(edit_log1);	

 						break;		
 		case	DATE_FORMAT_CODE:
						if(Pcm_var.Current_Date_Format==MMDDYYYY)
						{			
							sprintf(&edit_log.edit_log_buf[0],"Date Format = MMDDYYYY");
						}
						else
						{
							sprintf(&edit_log.edit_log_buf[0],"Date Format = DDMMYYYY");

						}
						log_file_write(edit_log1);	

 						break;	
			
 		case	DHCP_STATUS_CODE:
						if(Pcm_var.DHCP_Status==Screen22_str14)
						{			
							sprintf(&edit_log.edit_log_buf[0],"DHCP Enable");
						}
						else
						{
							sprintf(&edit_log.edit_log_buf[0],"DHCP Disable");

						}
						log_file_write(edit_log1);	

 						break;	
 		case	MAC_ID_CODE:
						sprintf(&edit_log.edit_log_buf[0],"MAC ID = %s",Mac_id);
						log_file_write(edit_log1);	

 						break;							
				default:
						break;

	}
	parameter_code = 0;
	os_mut_release (&usb_log_mutex);	
}

#endif /*#ifdef RTOS*/
/*****************************************************************************
* End of file
*****************************************************************************/
