/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Keypad.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 16, 2012>
* @date Last Modified  : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "../GUI/Application/Screen_global_ex.h"
#include "RTOS_main.h"
#include "File_update.h"
#include "../GUI/LCDConf.h"
#include "../GUI/Application/Screen_data_enum.h"
//#include "Screenshot_bmp.h"
#include "soft_stack.h"
//#include "Modbus_tcp.h"
#include "Keypad.h"
#include "Modbus_Tcp_Master.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#ifdef KEYPAD
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL ftp_send_fg;
/* Character variables section */
	U8 Keypad_flag = 0;
	U8 Zero_key_press_flag = 0, Clr_totals_key_press_flag = 0xFF;
  U8 Keypad_Timeout_Indication = LCD_TURNED_ON_INIT;
  U8 key_to_process = NO_KEY_PRESSED_CODE;
	
/* unsigned integer variables section */
  U16 Keypad_Timeout = KBD_TIMEOUT;

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void Keypad_Timeout_LCDOff(void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: void keypad_init (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 28th August 2012
* @brief      Description  : Initialize the scan and return lines of the keypad
* @note       Notes        : None
*****************************************************************************/
void keypad_init (void)
{
  //  U32 status;

  /*Initialize rows as scanlines(outputs)*/
  LPC_IOCON->PIN_ROW0 = 0x20;                      /* P3.25 is GPIO-KeyRow0*/
  LPC_IOCON->PIN_ROW1 = 0x20;                      /* P3.26 is GPIO-KeyRow1*/
  LPC_IOCON->PIN_ROW2 = 0x20;                      /* P3.27 is GPIO-KeyRow2*/
  LPC_IOCON->PIN_ROW3 = 0x20;                      /* P3.28 is GPIO-KeyRow3*/

  /* set the pins as outputs and set the mask register */
  ROW_PORT->DIR |= ROW_MASK;

  /* output 1 */
  ROW_PORT->SET = ROW_MASK;

  /*Initialize columns as return lines(inputs)*/
  LPC_IOCON->PIN_COL0 = 0x20;                      /* P3.21 is GPIO-KeyCol0*/
  LPC_IOCON->PIN_COL1 = 0x20;                      /* P3.22 is GPIO-KeyCol1*/
  LPC_IOCON->PIN_COL2 = 0x20;                      /* P3.23 is GPIO-KeyCol2*/
  LPC_IOCON->PIN_COL3 = 0x20;                      /* P3.24 is GPIO-KeyCol3*/

  /* set the pins as inputs and set the mask register*/
  COL_PORT->DIR &= ~COL_MASK;

  /*Initialize  keypad interrupt if defiend*/
  #ifdef KEYPAD_INT_BASED
  ROW_PORT->CLR = ROW_MASK;
  LPC_IOCON->KEYPAD_INT_PORT |= 0x80;              /* P0.26 is GPIO-Interrupt*/

  LPC_GPIOINT->IO0IntEnR |= (1UL<<KEYPAD_INT_PIN); /* P0.26 - rising edge interrupt enabled*/
  LPC_GPIOINT->IO0IntClr |= (1UL<<KEYPAD_INT_PIN);     /* P0.26 - clears interrupt */
	NVIC_ClearPendingIRQ (GPIO_IRQn);
 // NVIC_EnableIRQ(GPIO_IRQn);
  #endif
}
/*****************************************************************************
* @note       Function name: U8 keypad_scan (U8 row_num)
* @returns    returns      : The number of buttons pressed in the scanned row
* @param      arg1         : The number of the row to be scanned
* @author                  : Anagha Basole
* @date       date created : 28th August 2012
* @brief      Description  : Scan a particular row and check which buttons
                             pressed by reading the return lines
* @note       Notes        : None
*****************************************************************************/
U8 keypad_scan (U8 row_num)
{
  U8 status = NO_KEY_PRESSED_CODE;
  U32 pinVal = 0;
//	unsigned int j;
  /* reset the scan lines to output 1 */

  ROW_PORT->SET = ROW_MASK;
  switch (row_num)
  {
    case KEY_ROW0:
      /*Check if any button in row0 is pressed*/
      ROW_PORT->CLR = (1UL<<IO_ROW0);
      status = (1<<4);
      break;

    case KEY_ROW1:
      /*Check if any button in row1 is pressed*/
      ROW_PORT->CLR = (1UL<<IO_ROW1);
      status = (1<<5);
      break;

    case KEY_ROW2:
      /*Check if any button in row2 is pressed*/
      ROW_PORT->CLR = (1UL<<IO_ROW2);
      status = (1<<6);
      break;

    case KEY_ROW3:
      /*Check if any button in row3 is pressed*/
      ROW_PORT->CLR = (1UL<<IO_ROW3);
      status = (1<<7);
      break;

    default:
      break;
  }
  os_dly_wait(1);

  pinVal = (COL_PORT->PIN);          /*Read the input register for column values*/
  pinVal = pinVal;

  if (((pinVal & (1UL<<IO_COL0)) == 0) ||
        ((pinVal & (1UL<<IO_COL1)) == 0) ||
        ((pinVal & (1UL<<IO_COL2)) == 0) ||
        ((pinVal & (1UL<<IO_COL3)) == 0))
  {
    /*Read the input register AGAIN for column values*/
    if ((pinVal & (1UL<<IO_COL0)) == 0) /*Check if any button in col0 is pressed*/
    {
      status |= (1<<0);
    }
    if ((pinVal & (1UL<<IO_COL1)) == 0) /*Check if any button in col1 is pressed*/
    {
      status |= (1<<1);
    }
    if ((pinVal & (1UL<<IO_COL2)) == 0) /*Check if any button in col2 is pressed*/
    {
      status |= (1<<2);
    }
    if ((pinVal & (1UL<<IO_COL3)) == 0) /*Check if any button in col3 is pressed*/
    {
      status |= (1<<3);
    }
  }
  else
  {
    status = NO_KEY_PRESSED_CODE;
  }

  return(status);
}

/*****************************************************************************
* @note       Function name: U8 keypad_analyze (U8 status)
* @returns    returns      : result of the analysis
* @param      arg1         : The row and column data is passed for analysis
* @author                  : Anagha Basole
* @date       date created : 28th August 2012
* @brief      Description  : Analyze the row and column numbers and perform the
                             required action
* @note       Notes        : None
*****************************************************************************/
U8 keypad_analyze (U8 status)
{
  U8 i;
	int sel;
	RTCTime Curr_time1;
  static U8 clear_key_time;
	static U8 Clear_count = 0x00; // Every time when Clear Button is pressed this variable is incremented
	#if 1
	if (status == 0x22)
	{
		i = keypad_scan(KEY_ROW1);
		if (i != status)
		{
			status = i;
		}
	}
	#endif

  switch (status)
  {
    /********************Row 0********************/
    case 0x10: /*no key pressed in Row0*/
      break;

    case 0x11: /*Row 0, Col 0*/
      break;

    case 0x12: /*Row 0, Col 1*/
      break;

    case 0x14: /*Row 0, Col 2*/
      break;

    case 0x18: /*Row 0, Col 3*/
      break;

    /********************Row 1*******************/
    case 0x20: /*no key pressed in Row1*/
      break;

    case 0x21: /*Row 1, Col 0*/
      /*---------------------------- RUN_MODE ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", home");
      #endif
		  Init_STACK(wiz_nav_stack);
		  GUI_data_nav.Back_one_level = 0;
		  parent_wizard.w_info = NULL;
		  parent_wizard.w_index = 0;
		
					GUI_data_nav.New_screen_num[0] = '8';
					GUI_data_nav.New_screen_num[1] = '0';
					for (i = 2; i < 6; i++)
					{
						 GUI_data_nav.New_screen_num[i] = '0';
					}
					GUI_data_nav.Change = 1;
					if (GUI_data_nav.In_wizard == 1)
					{
						GUI_data_nav.In_wizard = __FALSE;
						GUI_data_nav.Wizard_screen_index = __FALSE;
						GUI_data_nav.Cancel_operation = 0;
						GUI_data_nav.Setup_wizard = 0;
						GUI_data_nav.wizard_screen = NULL;
					}
			//Added by PVK on 17 May 2016
//			Restore_Calib_Value_Upon_Cancel_Calibration_Process();
      break;

    case 0x22: /*Row 1, Col 1*/
      /*---------------------------- MAIN MENU ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", main menu");
      #endif
			Init_STACK(wiz_nav_stack);
		  GUI_data_nav.Back_one_level = 0;
  	  Zero_key_press_flag  = 0;
			Clr_totals_key_press_flag = 0xFF;
		  parent_wizard.w_info = NULL;
			parent_wizard.w_index = 0;
//		  Run_Screen_Flags = 0;
      if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAIN_MENU_ORG_NO))
      {		
					GUI_data_nav.New_screen_num[0] = '0';
					GUI_data_nav.New_screen_num[1] = '0';
					for (i = 2; i < 6; i++)
					{
						 GUI_data_nav.New_screen_num[i] = '0';
					}
					GUI_data_nav.Change = 1;
        if (GUI_data_nav.In_wizard == 1)
        {
          GUI_data_nav.In_wizard = __FALSE;
          GUI_data_nav.Wizard_screen_index = __FALSE;
          GUI_data_nav.Cancel_operation = 0;
          GUI_data_nav.Setup_wizard = 0;
          GUI_data_nav.wizard_screen = NULL;
        }					
			}
			//Added by PVK on 18 May 2016
//			Restore_Calib_Value_Upon_Cancel_Calibration_Process();
      break;

    case 0x24: /*Row 1, Col 2*/
      /*---------------------------- ZERO CAL ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", zero cal");
      #endif
	
      break;
			
    case 0x28: /*Row 1, Col 3*/
      /*---------------------------- BACK ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", back");
      #endif
      if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PCM_SCREEN_ORG_NO) != 0)
			{
				GUI_StoreKeyMsg(GUI_KEY_BACK, 1);
			}
      break;

    /*******************Row 2*******************/
    case 0x40: /*no key pressed in Row2*/
      break;

    case 0x41: /*Row 2, Col 0*/
      /*---------------------------- UP ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", up");
      #endif
			GUI_StoreKeyMsg(GUI_KEY_UP, 1);
      GUI_StoreKeyMsg(GUI_KEY_UP_CUSTOM, 1);
      break;

    case 0x42: /*Row 2, Col 1*/
      /*---------------------------- LEFT ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", left");
      #endif

      GUI_StoreKeyMsg(GUI_KEY_LEFT_CUSTOM, 1);
      GUI_StoreKeyMsg(GUI_KEY_LEFT, 1);
      break;

    case 0x44: /*Row 2, Col 2*/
      /*---------------------------- PRINT ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", print");
      #endif
		//if printing is enabled then send an event to the printer task
//       if (Setup_device_var.Printer_status == Screen411_str1)
//       {

//       }
// 			else
// 			{
// 				#ifdef SCREENSHOT	
// 					Take_Screen_Shot_Flag = __TRUE;
// 				#endif
// 			}	
// 			if(!ftp_send_fg)
// 			{
// 				ftp_send_fg = 1;
// 				GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);				
// 				GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);

// 				GUI_DispStringAt("Message: Requesting file from scale...", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);
// 			}
      break;

    case 0x48: /*Row 2, Col 3*/
      /*---------------------------- DOWN ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", down");
      #endif
		GUI_StoreKeyMsg(GUI_KEY_DOWN, 1);
      break;

    /********************Row 3********************/
    case 0x80: /*no key pressed in Row3*/
      break;

    case 0x81: /*Row 3, Col 0*/
      break;

    case 0x82: /*Row 3, Col 1*/
      /*---------------------------- ENTER ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", enter");
      #endif

			 GUI_StoreKeyMsg(GUI_KEY_ENTER, 1);
      break;

    case 0x84: /*Row 3, Col 2*/
      /*---------------------------- CLEAR WEIGHT ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", clear weight");
      #endif
//		 GUI_StoreKeyMsg(GUI_KEY_CLEAR, 1);
// 			Modbus_Tcp_Slaves[0].rtc_update = 1;	
// 			GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);
// 			GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);

// 			GUI_DispStringAt("Message: set date time at scale...", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);
		
      break;

    case 0x88: /*Row 3, Col 3*/
      /*---------------------------- RIGHT ----------------------------*/
      #ifdef LOG_KEY_STATUS
      debug_log_write(", right");
      #endif
		GUI_StoreKeyMsg(GUI_KEY_RIGHT_CUSTOM, 1);
      GUI_StoreKeyMsg(GUI_KEY_RIGHT, 1);
      break;

    default:
      break;
  }
  key_to_process = status;
  return (status);
}

/*****************************************************************************
* @note       Function name: void Keypad_task (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Anagha Basole
* @date       date created : 28th August 2012
* @brief      Description  : Keypad task to check for key press and analyze the
                             input
* @note       Notes        : None
*****************************************************************************/
__task void Keypad_task (void)
{
  #ifdef KEYPAD_INT_BASED
  U8 status1 = 0;
  #else
  U8 Toggle = 1;
  #endif
//  NVIC_EnableIRQ(GPIO_IRQn);
  while (1)
  {

    #ifdef KEYPAD_INT_BASED
    os_evt_wait_or (KEYPAD_EVENT_FLAG, 0xffff);   /* wait for an event flag 0x0001*/
		LPC_RTC->GPREG4 |= (0x01<<8);
    //scan first row
    status1 = keypad_scan(1);
    //if no key pressed on first row scan next row
    if(status1 == NO_KEY_PRESSED_CODE)
    {
      //scan second row
      status1 = keypad_scan(2);
      if(status1 == NO_KEY_PRESSED_CODE)
      {
        //scan second row
        status1 = keypad_scan(3);
        if(status1 == NO_KEY_PRESSED_CODE)
        {
          status1 = NO_KEY_PRESSED_CODE;
        }
        else //keypad_scan(3)
        {
          keypad_analyze(status1);
        }
      }
      else //keypad_scan(2)
      {
        keypad_analyze(status1);
      }
    }
    else //keypad_scan(1)
    {
      keypad_analyze(status1);
    }

    /* clear the scan lines  and enable ISR*/
    ROW_PORT->CLR = ROW_MASK;
    /* P0.26 - clear keypad interrupt as we are getting interrupt during keypress*/
		os_evt_clr (KEYPAD_EVENT_FLAG, t_keypad);
    LPC_GPIOINT->IO0IntClr |= (1UL << KEYPAD_INT_PIN);
    NVIC_ClearPendingIRQ (GPIO_IRQn);
    //NVIC_EnableIRQ(GPIO_IRQn);
		NVIC->ISER[((uint32_t)(GPIO_IRQn) >> 5)] = (1 << ((uint32_t)(GPIO_IRQn) & 0x1F)); /* enable interrupt */

  #else
      os_itv_set(KEYPAD_TASK_INTERVAL);
      os_itv_wait();

      if(Toggle)
      {
        ROW_PORT->CLR = ROW_MASK;
        COL_PORT->CLR = COL_MASK;
        Toggle = 0;
      }
      else
      {
        ROW_PORT->SET = ROW_MASK;
        COL_PORT->SET = COL_MASK;
        Toggle = 1;
      }

    #if 0
      //scan first row
      status1 = keypad_scan(1);
      if( (Keypad_flag == 1) && (prvKeyStat == status1))
      {
        keypad_analyze(status1);
      }

      //if no key pressed on first row scan next row
      if(status1 == NO_KEY_PRESSED_CODE/*0x20*/)
      {
        //scan second row
        status1 = keypad_scan(2);
        if( (Keypad_flag == 1) && (prvKeyStat == status1))
        {
          keypad_analyze(status1);
        }
        //if no key pressed on second row scan next row
        if(status1 == NO_KEY_PRESSED_CODE/*0x40*/)
        {
          //scan thrid row
          status1 = keypad_scan(3);
          if( (Keypad_flag == 1) && (prvKeyStat == status1))
          {
            keypad_analyze(status1);
          }
          else if(status1 == NO_KEY_PRESSED_CODE)
          {
            //Reset
            Keypad_flag = 0x0;
            prvKeyStat = NO_KEY_PRESSED_CODE;
          }
        }
      }
      //if((status1 & 0x0F) || (status2 & 0x0F) || (status3 & 0x0F))
      if(status1 & 0x0F)
      {
          //Key pressed start debounse time
          if( Keypad_flag == 0)
          {
            prvKeyStat = status1;
            Keypad_flag = 1;
          }
          //Vaild Key pressed and processed - debounse time over
          //Key not released - ignore the next key pressed action
          else
          {
            prvKeyStat = NO_KEY_PRESSED_CODE;
            Keypad_flag = 2;
          }
      }
      else
      {
        //No key pressed
        prvKeyStat = NO_KEY_PRESSED_CODE;
        Keypad_flag = 0;
      }
    #endif
    #endif
  	LPC_RTC->GPREG4 &= ~(0x01<<8);
//		WDTFeed();
}

}
/*****************************************************************************
* @note       Function name: void GPIO_IRQHandler (void)
* @returns    returns       : None
* @param      arg1         : None
* @author                   : Anagha Basole
* @date       date created : 28th August 2012
* @brief      Description   : Interrupt handler for a key press
* @note       Notes         : None
*****************************************************************************/
#ifdef KEYPAD_INT_BASED
void GPIO_IRQHandler (void)
{
  U32 pinVal = 0;
  pinVal = (LPC_GPIOINT->PORT_INT_STATUS);
  if ((pinVal & (1UL<< KEYPAD_INT_PIN)) == (1UL<< KEYPAD_INT_PIN))
  {
    NVIC_DisableIRQ(GPIO_IRQn);
		if (Keypad_Timeout_Indication != LCD_TURNED_OFF_AFTER_TIMEOUT)
		{
			 isr_evt_set (KEYPAD_EVENT_FLAG, t_keypad);
		}
		else
		{
			 //Reload the count
       Keypad_Timeout = KBD_TIMEOUT;

       //Turn on the backlight
       LPC_GPIO2->SET |= (1 << LCD_BACKLIGHT_BIT); // Set backlight to on

       key_to_process = NO_KEY_PRESSED_CODE;
       //Keypad timout flag
       Keypad_Timeout_Indication = LCD_ON_TIMEOUT_START;
			 NVIC_ClearPendingIRQ (GPIO_IRQn);
       NVIC_EnableIRQ(GPIO_IRQn);
		}
  }
  LPC_GPIOINT->IO0IntClr |= (1UL << KEYPAD_INT_PIN); /* P0.26 - clears interrupt */
}
#endif
/*****************************************************************************
* @note       Function name: void Keypad_Timeout_LCDOff(void)
* @returns    returns       : None
* @param      arg1         : None
* @author                   : Pandurang Khutal
* @date       date created : 28th August 2012
* @brief      Description   : This function genrate 30sec time out for KBD entry
* @note       Notes         : None
*****************************************************************************/
void Keypad_Timeout_LCDOff(void)
{
  //Don't time out for diagonostic and run mode screens
  if((strncmp(RUN_MODE_ORG_NO, GUI_data_nav.Current_screen_info->Screen_org_no,1) != 0) &&
     (strncmp(DIAG_SCREEN_ORG_NO, GUI_data_nav.Current_screen_info->Screen_org_no,2) != 0)&&
     (strncmp(TEST_WEIGHT_ENTRY_ORG_NO, GUI_data_nav.Current_screen_info->Screen_org_no,1) != 0))
  {
    if(key_to_process != NO_KEY_PRESSED_CODE)
    {
        //Reload the count
        Keypad_Timeout = KBD_TIMEOUT;

        if((Keypad_Timeout_Indication == LCD_TURNED_ON_INIT) ||
					 (Keypad_Timeout_Indication == LCD_TURNED_OFF_AFTER_TIMEOUT))
        {
          //Turn on the backlight
          LPC_GPIO2->SET |= (1 << LCD_BACKLIGHT_BIT); // Set backlight to on
        }

        key_to_process = NO_KEY_PRESSED_CODE;
        //Keypad timout flag
        Keypad_Timeout_Indication = LCD_ON_TIMEOUT_START;
    }
    else
    {
        if(Keypad_Timeout_Indication == LCD_ON_TIMEOUT_START)
        {
          //wait for time out
          if(Keypad_Timeout <= 0)
          {
            //Turn off the backlight
            LPC_GPIO2->CLR |= (1 << LCD_BACKLIGHT_BIT); // Set backlight to off

            //Keypad timout flag
            Keypad_Timeout_Indication = LCD_TURNED_OFF_AFTER_TIMEOUT;
          }

        }
    }//else
 }//if
} //Keypad_Timeout_LCDOff

#endif  //#ifdef KEYPAD
/*****************************************************************************
* End of file
*****************************************************************************/
