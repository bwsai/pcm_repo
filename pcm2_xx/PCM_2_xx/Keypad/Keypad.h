/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Keypad.h
* @brief			         : Includes all keypad related functionality
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 12, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
//#ifndef __KEYPAD_H
//#define __KEYPAD_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"

#ifdef KEYPAD
/*============================================================================
* Public Macro Definitions*/
#define KEY_ROW0               0
#define KEY_ROW1               1
#define KEY_ROW2               2
#define KEY_ROW3               3

#define KEY_COL0               0
#define KEY_COL1               1
#define KEY_COL2               2
#define KEY_COL3               3

#define IO_ROW0                25   /*P3.25*/
#define IO_ROW1                26   /*P3.26*/
#define IO_ROW2                27   /*P3.27*/
#define IO_ROW3                28   /*P3.28*/

#define IO_COL0                21   /*P3.21*/
#define IO_COL1                22   /*P3.22*/
#define IO_COL2                23   /*P3.23*/
#define IO_COL3                24   /*P3.24*/

#define PIN_ROW0               P3_25   /*P3.25*/
#define PIN_ROW1               P3_26   /*P3.26*/
#define PIN_ROW2               P3_27   /*P3.27*/
#define PIN_ROW3               P3_28   /*P3.28*/

#define PIN_COL0               P3_21   /*P3.21*/
#define PIN_COL1               P3_22   /*P3.22*/
#define PIN_COL2               P3_23   /*P3.23*/
#define PIN_COL3               P3_24   /*P3.24*/

#define NO_KEY_PRESSED_CODE    0x00

#define ROW_PORT               LPC_GPIO3
#define COL_PORT               LPC_GPIO3

#define ROW_MASK               ((1<<IO_ROW0) | (1<<IO_ROW1) | (1<<IO_ROW2) | (1<<IO_ROW3))
#define COL_MASK               ((1<<IO_COL0) | (1<<IO_COL1) | (1<<IO_COL2) | (1<<IO_COL3))

#define KEYPAD_INT_PORT        P0_26
#define KEYPAD_INT_PIN         26
#define PORT_INT_STATUS        IO0IntStatR


//#define LOG_KEY_STATUS
#define KEYPAD_INT_BASED
/*===========================================================================*/
/* Defines Section */
#define THIRTY_SEC_TICKS  3000
#define FIFTEEN_SEC_TICKS 1500
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern U8 Keypad_Timeout_Indication ;
/* unsigned integer variables section */
extern  U16 Keypad_Timeout;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void keypad_init (void);
extern U8 keypad_scan (U8 row_num);
extern __task void Keypad_task (void);
extern void Keypad_Timeout_LCDOff(void);

#endif /*#ifdef KEYPAD*/

//#endif /*__KEYPAD_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
