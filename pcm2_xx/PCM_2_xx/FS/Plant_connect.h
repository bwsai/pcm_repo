/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : Plant_connect.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 16, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __PLANT_CONNECT_H
#define __PLANT_CONNECT_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "rtc.h"

#ifdef MODBUS_TCP
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
 #define PLANT_CONNECT_DATA_RCRD_FLAG              0x01
 #define PLANT_CONNECT_ZERO_RCRD_FLAG              0x02
 #define PLANT_CONNECT_CLEAR_RCRD_FLAG             0x03
 #define PLANT_CONNECT_CAL_RCRD_FLAG               0x04
 #define PLANT_CONNECT_ERROR_RCRD_FLAG             0x05

 #define PLANT_CONNECT_LOW_BELT_SPEED_ERR_FLAG     0x01
 #define PLANT_CONNECT_HIGH_BELT_SPEED_ERR_FLAG    0x02
 #define PLANT_CONNECT_BELT_SPEED_ERR_FLAG         0x03
 #define PLANT_CONNECT_LOAD_CELL_ERR_FLAG          0x04
 #define PLANT_CONNECT_ANGLE_SENSOR_ERR_FLAG       0x05
 #define PLANT_CONNECT_ZERO_CAL_ERR_FLAG           0x06
 #define PLANT_CONNECT_CAL_FACTOR_ERR_FLAG         0x07
 #define PLANT_CONNECT_LOW_RATE_ERR_FLAG           0x08
 #define PLANT_CONNECT_HIGH_RATE_ERR_FLAG          0x09
 #define PLANT_CONNECT_SEN_BRD_COM_ERR_FLAG        0x0A
 
 #define PLANT_CONNECT_NO_ERROR_FLAG               0x00
 #define PLANT_CONNECT_NO_CAL_FLAG                 0x00
 
 #define PLANT_CONNECT_TST_WT_FLAG                 0x01
 #define PLANT_CONNECT_MAT_WT_FLAG                 0x02
 #define PLANT_CONNECT_MANUAL_FLAG                 0x03
/*============================================================================
* Public Data Types
*===========================================================================*/		
	/* Plant Connect Structure*/
	typedef struct
	{
		  U16 Record_type;
		  U32 Unique_id;
		  DATE_STRUCT Date;
		  TIME_STRUCT Time;
		  double Accumulated_weight; // float Accumulated_weight; 
		  U16 Weight_unit;
		  float Tons_per_hour;
		  U16 Rate_unit;
		  float Load_percentage;
		  float Angle;
		  float Speed;
		  U16 Speed_unit;
		  float Old_zero_value;
		  float New_zero_value;
		  U32 Nos_of_pulses;
		  float Time_duration;
		  double Accumulated_weight_before_clr; // float Accumulated_weight_before_clr;
			DATE_STRUCT Clear_Date;
		  TIME_STRUCT Clear_Time;
			float Clear_Speed;
		  U16 Calibration_type;
		  U16 Load_cell_capacity;
		  U16 Load_cell_unit;
		  U16 Distance_unit;
		  /*U16 Idler_distanceA;
		  U16 Idler_distanceB;
		  U16 Idler_distanceC;
		  U16 Idler_distanceD;
		  U16 Idler_distanceE;
		  */
			float Idler_distanceA;
		  float Idler_distanceB;
		  float Idler_distanceC;
		  float Idler_distanceD;
		  float Idler_distanceE;
			float Conveyor_angle;
		  float Test_weight_value;
		  double Accumulated_weight_during_cal; // float Accumulated_weight_during_cal;
		  float Certified_weight;
		  float Prev_cal_factor;
		  float New_cal_factor;
		  U16 Error_code1;
		  U16 Error_code2;
		  U16 Error_code3;
		  U32 Old_record_request;
			U8 EOR;
	}PLANT_CONNECT_STRUCT;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
 extern PLANT_CONNECT_STRUCT Plant_connect_var, Plant_connect_var_read;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void populate_error_code_plant_connect_var(U16 error_code);
extern void load_cell_size_unit_plant_connect(void);
extern void weight_unit_plant_connect(void);
extern void rate_unit_plant_connect(void);
extern void speed_distance_plant_connect_unit(void);
extern U8 plant_connect_struct_write(float wt_bfr_clr, U16 cal_type, U16 err_code1, U16 err_code2, U16 err_code3, U16 rcrd_type);
extern U8 plant_connect_struct_read (void);

#endif /*ifdef MODBUS_TCP*/

#endif /*__PLANT_CONNECT_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
