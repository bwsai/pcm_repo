/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : File_update.c
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 26, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 26, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     :
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "File_update.h"
#include <RTL.h>
#include <float.h>
#include <string.h>
//#include "Screen_global_ex.h"  //Commented by DK on 12 sep 2014 for PCM 
#include "EEPROM_high_level.h"
#include "lpc177x_8x_iap.h"
//Commented by DK on 12 sep 2014 for PCM 
#include "../gui/application/Screen_global_ex.h"
//#include "Screen_data_enum.h"


//#include "Screen_global_data.h"
#ifdef FILE_SYS
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define FLASH_CONFIG_AREA       0x0000A000
#define FLASH_CONFIG_SECTOR      10
extern prop_file_ini prop_file;
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
FLAGS_STRUCT_DEF Flags_struct;
__align(4) FILE_HEADER_STRUCT 		fheader;
__align(4) PERIODIC_LOG_STRUCT 		periodic_data;
__align(4) SET_ZERO_LOG_STRUCT 		set_zero_data_log;
__align(4) CLR_DATA_LOG_STRUCT 		clear_data_log;
__align(4) CAL_DATA_LOG_STRUCT 		calib_data_log;
__align(4) LENGTH_DATA_LOG_STRUCT length_cal_data_log;
__align(4) ERR_DATA_LOG_STRUCT 		error_data_log;
__align(4) FREQUENT_REPORT_STRUCT daily_rprt, weekly_rprt, monthly_rprt;
__align(4) ERR_REPORT_STRUCT 			error_rprt;
__align(4) CAL_REPORT_STRUCT 			calibration_rprt;
__align(4) SYSTEM_LOG_STRUCT 			system_log;
__align(4) EDIT_LOG_STRUCT		edit_log;
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
__align(4) unsigned char ConfigBuf[256];
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
char FwUpgrade(char *HexFilename, char length);

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: U8 log_file_init(void)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 27/7/12
* @brief      Description	 : Creates the files and write the headers to the file
* @note       Notes		     : None
*****************************************************************************/
U8 log_file_init(char file_name[30])
{
	FILE *fp;
	U8 err = 0;

	/*Create and initialize the header of the periodic log file*/
	fp = fopen (file_name, "w");
	if(fp == NULL)
	{
		err = 1;
	}
	else
	{
		fprintf(fp, " %d,", fheader.File_hdr.Id);
		fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
																			fheader.File_hdr.Date.Mon,
																			fheader.File_hdr.Date.Day);
		fprintf(fp, " %02d:%02d:%02d,", fheader.File_hdr.Time.Hr,
																			fheader.File_hdr.Time.Min,
																			fheader.File_hdr.Time.Sec);
//Commented by DK on 12 sep 2014 for PCM 
		//		fprintf(fp, " %s,",Admin_var.Scale_Name);
	//	fprintf(fp, " %s,",Admin_var.Plant_Name);
	//	fprintf(fp, " %s\r\n",Admin_var.Product_Name);	
		
		if(strcmp("calib_log.txt",file_name) == 0)		
		{
			fprintf(fp,"Type,ID#, Date, Time, Scale name, Plant name, Product name, Cal pulse count,Cal duration (Min), Belt Speed, Belt Speed Unit, Prev. zero value, New zero value, Angle,Accumulated Cal Weight, Accumulated weight unit, Test Weight, Test Weight unit,Certified scale weight, Certified Scale Unit, Belt Scale Weight, Belt Scale weight Unit,Previous TRIM factor, New Trim factor, Idler Distance1, Idler Distance2,Unit");
		}
		else if(strcmp("error_log.txt",file_name) == 0)
		{
			;
		}
		else if(strcmp("system_log.txt",file_name) == 0)
		{
			fprintf(fp,"Record #, Date, Time, Event log\r\n");

		}
		else
			fprintf(fp,"Function, Record #, Date, Time, Accumulated Weight, Weight Unit, Rate, Rate Unit, Load Percent, Angle, Belt speed, Speed unit\r\n");
		if(ferror(fp))
		{
//			printf("\n File write error-%s", file_name);
		}
	}	

	if(0 != fclose(fp))
	{
//		printf ("\n %s file could not be closed!", file_name);
	}
	fflush (stdout);
	return (err);
}


/*****************************************************************************
* @note       Function name: U8 debug_log_write(const U8 *buf)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : pointer to the data to be written to the file
* @author			             : Anagha Basole
* @date       date created : 15/2/13
* @brief      Description	 : Creates the file if not present and writes the
*														 debug messages to it
* @note       Notes		     : None
*****************************************************************************/
U8 debug_log_write(const U8 *buf)
{
	FILE *fp;
	static U8 file_error = 0;

	if (Flags_struct.Connection_flags & USB_CON_FLAG)
	//if(con)
	{
		fp = fopen ("debug_log.txt", "a");
		if(fp == NULL)
		{
			if (file_error == 0)
			{
//				printf("\n Error in opening debug file");
			}
			file_error = 1;
		}
		else
		{
			file_error = 0;
			//fwrite(buf, 1, sizeof(buf), fp);
			fprintf(fp,"%s",buf);
			if(ferror(fp))
			{
//				printf("\n Debug file write error");
			}
		}
		if(0 != fclose(fp))
		{
//			printf ("\nDebug file could not be closed!\n");
			file_error = 1;
		}
		fflush (stdout);
	}
	return (file_error);
}

/*****************************************************************************
* @note       Function name: U8 log_file_write(U8 log_type)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : type of data log to be written
* @author			             : Anagha Basole
* @date       date created : 26/7/12
* @brief      Description	 : Writes the current data log in the file
* @note       Notes		     : None
*****************************************************************************/
U8 log_file_write(U8 log_type)
{
	FILE *fp;
	U8 err = 0;
	U8 u8File_exists = 0;
	char file_name[40]={0};
	int size = 0;
	//write the structure in the file
	if ((Flags_struct.Connection_flags & USB_CON_FLAG) == 0)
	return 0;

 	switch(log_type)

	{
		case system_log1:
				system_log.Id++;
				LPC_RTC->GPREG2 = system_log.Id;
				system_log.Date.Year = (U16) Current_time.RTC_Year;
				system_log.Date.Mon = (U8) Current_time.RTC_Mon;
				system_log.Date.Day = (U8) Current_time.RTC_Mday;	
				system_log.Time.Sec = Current_time.RTC_Sec;
				system_log.Time.Min = Current_time.RTC_Min;
				system_log.Time.Hr = Current_time.RTC_Hour;		
		
			strcpy(file_name, "System_Log.txt");
			fp = fopen(file_name, "r");
			//sks->
		if (fp != NULL) {						//added 08-11-2016
													//resolved issue: unit resets if blank USB drive is inserted.
				fseek(fp, -1L, SEEK_END); // Read the last character from file
				size = ftell(fp); // take a position of file pointer and get size of variable
				if (prop_file.Max_size_int == 0)
					prop_file.Max_size_int = 1000000;// by megha on 22/9/2017 for scales disconnect problem if system_log.txt is larger than 1 mb.
					// it takes time to open bigger file 6000000;
				if (size > prop_file.Max_size_int) {   				//62939136
					fclose(fp);
					fp = fopen("System_Log.bak", "r");
					if (fp != NULL) {
						fclose(fp);
						fdelete("System_Log.bak");
					} else {
						if (frename("System_Log.txt", "System_Log.bak") == 0) {
							strcpy(file_name, "System_Log.txt");
							fp = fopen(file_name, "a");
							fprintf(fp, " %d,", fheader.File_hdr.Id);
							fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
																								fheader.File_hdr.Date.Mon,
																								fheader.File_hdr.Date.Day);
							fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
																								fheader.File_hdr.Time.Min,
																								fheader.File_hdr.Time.Sec);		
							fprintf(fp,"Record #, Date, Time, Event log\r\n");							
							fclose(fp);
							fp = fopen(file_name, "r");
						}
					}
				}
			}		
		//Check for file present
		if (fp != NULL) {
			u8File_exists = 1;
			fclose(fp);
		} else {
			u8File_exists = 0;
//			fclose(fp);
		}
//		fflush(fp);

		//Open file in read or write mode
		if (u8File_exists == 0) {
			fp = fopen(file_name, "w");
		} else {
			fp = fopen(file_name, "r");
		}		

		if (fp == NULL) 
		{
			return 0;
		}
		else
		{
			if(u8File_exists == 0)
			{
						fprintf(fp, " %d,", fheader.File_hdr.Id);
						fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
																							fheader.File_hdr.Date.Mon,
																							fheader.File_hdr.Date.Day);
						fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
																							fheader.File_hdr.Time.Min,
																							fheader.File_hdr.Time.Sec);		
						fprintf(fp,"Record #, Date, Time, Event log\r\n");
				
			}
			else
			{
				//Close the file and open it in append mode
				if (0 != fclose(fp)) {
					//		printf ("\nFile could not be closed!\n");
				}
//				fflush(fp);

				//Open file in append mode to write events
				fp = fopen(file_name, "a");
				if (fp == NULL) {
					return 0;
				}
				
			}
			fprintf(fp, "\r\n%d,",system_log.Id);
			fprintf(fp, " %d/%d/%d,", system_log.Date.Year,
																				system_log.Date.Mon,
																				system_log.Date.Day);
			fprintf(fp, " %02d:%02d:%02d,", system_log.Time.Hr,
																				system_log.Time.Min,
																				system_log.Time.Sec);				
			fprintf(fp, "	%s", system_log.log_buf);
	
			if (ferror(fp)) {
				//printf("\n Error log file write error");
			}

			if (0 != fclose(fp)) {
				//		printf ("\nFile could not be closed!\n");
			}

//			fflush(fp);
			memset(file_name, 0, sizeof(file_name));
		}
		break;
		
	case edit_log1:
		
				edit_log.Id++;
				LPC_RTC->GPREG3 = edit_log.Id;
				edit_log.Date.Year = (U16) Current_time.RTC_Year;
				edit_log.Date.Mon = (U8) Current_time.RTC_Mon;
				edit_log.Date.Day = (U8) Current_time.RTC_Mday;	
				edit_log.Time.Sec = Current_time.RTC_Sec;
				edit_log.Time.Min = Current_time.RTC_Min;
				edit_log.Time.Hr = Current_time.RTC_Hour;		
		
			strcpy(file_name, "Edit_Log.txt");
			fp = fopen(file_name, "r");
			//sks->
		if (fp != NULL) {						//added 08-11-2016
													//resolved issue: unit resets if blank USB drive is inserted.
				fseek(fp, -1L, SEEK_END); // Read the last character from file
				size = ftell(fp); // take a position of file pointer and get size of variable
				if (prop_file.Max_size_int == 0)
					prop_file.Max_size_int = 6000000;
				if (size > prop_file.Max_size_int) {   				//62939136
					fclose(fp);
					fp = fopen("Edit_Log.bak", "r");
					if (fp != NULL) {
						fclose(fp);
						fdelete("Edit_Log.bak");
					} else {
						if (frename("Edit_Log.txt", "Edit_Log.bak") == 0) {
							strcpy(file_name, "Edit_Log.txt");
							fp = fopen(file_name, "a");
							fclose(fp);
							fp = fopen(file_name, "r");
						}
					}
				}
			}		
		//Check for file present
		if (fp != NULL) {
			u8File_exists = 1;
			fclose(fp);
		} else {
			u8File_exists = 0;
//			fclose(fp);
		}
//		fflush(fp);

		//Open file in read or write mode
		if (u8File_exists == 0) {
			fp = fopen(file_name, "w");
		} else {
			fp = fopen(file_name, "r");
		}		

		if (fp == NULL) 
		{
			return 0;
		}
		else
		{
			if(u8File_exists == 0)
			{
				
						fprintf(fp, " %d,", fheader.File_hdr.Id);
						fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
																							fheader.File_hdr.Date.Mon,
																							fheader.File_hdr.Date.Day);
						fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
																							fheader.File_hdr.Time.Min,
																							fheader.File_hdr.Time.Sec);		
						fprintf(fp,"Record #, Date, Time, Edit log\r\n");
				edit_log.Id = 1;
				LPC_RTC->GPREG3 = edit_log.Id;				
			}
			else
			{
				//Close the file and open it in append mode
				if (0 != fclose(fp)) {
					//		printf ("\nFile could not be closed!\n");
				}
//				fflush(fp);

				//Open file in append mode to write events
				fp = fopen(file_name, "a");
				if (fp == NULL) {
					return 0;
				}
				else
				{
//					edit_log.Id++;
				}
				
			}
			fprintf(fp, "\r\n%d,",edit_log.Id);
			fprintf(fp, " %d/%d/%d,", edit_log.Date.Year,
																				edit_log.Date.Mon,
																				edit_log.Date.Day);
			fprintf(fp, " %02d:%02d:%02d,", edit_log.Time.Hr,
																				edit_log.Time.Min,
																				edit_log.Time.Sec);				
			fprintf(fp, "	%s", edit_log.edit_log_buf);
	
// 			if (ferror(fp)) {
// 				//printf("\n Error log file write error");
// 			}

			if (0 != fclose(fp)) {
				//		printf ("\nFile could not be closed!\n");
			}

//			fflush(fp);
			memset(file_name, 0, sizeof(file_name));
		}
			break;
		default:
				break;
		
	}
	log_type = 0;
	return 0;
// 	switch(log_type)
// 	{
// 		/*if the log is a periodic data log*/
// //by megha for events log
// 		case system_log1:
// 			
// 				system_log.Id++;
// 				system_log.Date.Year = (U16) Current_time.RTC_Year;
// 				system_log.Date.Mon = (U8) Current_time.RTC_Mon;
// 				system_log.Date.Day = (U8) Current_time.RTC_Mday;	
// 				system_log.Time.Sec = Current_time.RTC_Sec;
// 				system_log.Time.Min = Current_time.RTC_Min;
// 				system_log.Time.Hr = Current_time.RTC_Hour;		
// 				sprintf(&file_name[0],"%04d-%02d-%02d_system_log.txt",system_log.Date.Year,
// 				system_log.Date.Mon ,system_log.Date.Day);	
// 				fp = fopen (file_name, "r");			
// 				if (fp != NULL)
// 				{
// 						u8File_exists = 1;
// 						fclose(fp);
// 				}
// 				else
// 						u8File_exists = 0;

// 					fp = fopen (file_name, "a");
// 					if(fp == NULL)
// 					{
// 		//				printf("\n Error in opening periodic log file");
// 						err = 1;
// 					}
// 					else
// 					{
// 						if(u8File_exists == 0)
// 						{
// 									fprintf(fp, " %d,", fheader.File_hdr.Id);
// 									fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
// 																										fheader.File_hdr.Date.Mon,
// 																										fheader.File_hdr.Date.Day);
// 									fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
// 																										fheader.File_hdr.Time.Min,
// 																										fheader.File_hdr.Time.Sec);		
// 									fprintf(fp,"Record #, Date, Time, Event log\r\n");
// 							
// 						}
// 						fprintf(fp, "\r\n%d,",system_log.Id);
// 						fprintf(fp, " %d/%d/%d,", system_log.Date.Year,
// 																							system_log.Date.Mon,
// 																							system_log.Date.Day);
// 						fprintf(fp, " %02d:%02d:%02d,", system_log.Time.Hr,
// 																							system_log.Time.Min,
// 																							system_log.Time.Sec);				
// 						fprintf(fp, "	%s\r\n", system_log.log_buf);
// 						if(ferror(fp))
// 						{
// 		//					printf("\n Periodic log file write error");
// 						}				
// 					}
// 					
// 		break;			
// 		
// //****************************		
// 		
// 		case periodic_log_stop:
// 		case periodic_log_start:
// 		case periodic_log:		
// 		case periodic_log_stop_at_start:
// 			sprintf(&file_name[0],"%04d-%02d-%02d_periodic_log.txt",periodic_data.Periodic_hdr.Date.Year,
// 		periodic_data.Periodic_hdr.Date.Mon,periodic_data.Periodic_hdr.Date.Day);
// 			fp = fopen (file_name, "r");			
//     if (fp != NULL)
//     {
// 				u8File_exists = 1;
//         fclose(fp);
//     }
// 		else
// 				u8File_exists = 0;

// 			fp = fopen (file_name, "a");
// 			if(fp == NULL)
// 			{
// //				printf("\n Error in opening periodic log file");
// 				err = 1;
// 			}
// 			else
// 			{
// 				if(u8File_exists == 0)
// 				{
// 							fprintf(fp, "%s,",fheader.File_hdr.Data_type);
// 							fprintf(fp, " %d,", fheader.File_hdr.Id);
// 							fprintf(fp, " %d/%d/%d,", fheader.File_hdr.Date.Year,
// 																								fheader.File_hdr.Date.Mon,
// 																								fheader.File_hdr.Date.Day);
// 							fprintf(fp, " %d:%d:%d,", fheader.File_hdr.Time.Hr,
// 																								fheader.File_hdr.Time.Min,
// 																								fheader.File_hdr.Time.Sec);
// 					//Commented by DK on 12 sep 2014 for PCM 
// /*					
// 					fprintf(fp, " %s,",Admin_var.Scale_Name);
// 							fprintf(fp, " %s,",Admin_var.Plant_Name);
// 							fprintf(fp, " %s\r\n",Admin_var.Product_Name);		
// */
// 					fprintf(fp,"Function, Record #, Date, Time, Accumulated Weight, Weight Unit, Rate, Rate Unit, Load Percent, Angle, Belt speed, Speed unit,Daily weight, Daily Weight Unit\r\n");
// 	
// 					}
// 				fprintf(fp, "\r\n%s,", periodic_data.Periodic_hdr.Data_type);
// 				fprintf(fp, " %d,", periodic_data.Periodic_hdr.Id);
// 				fprintf(fp, " %d/%d/%d,", periodic_data.Periodic_hdr.Date.Year,
// 																					periodic_data.Periodic_hdr.Date.Mon,
// 																					periodic_data.Periodic_hdr.Date.Day);
// 				fprintf(fp, " %02d:%02d:%02d,", periodic_data.Periodic_hdr.Time.Hr,
// 																					periodic_data.Periodic_hdr.Time.Min,
// 																					periodic_data.Periodic_hdr.Time.Sec);
// 				fprintf(fp, " %11.1f,", periodic_data.Accum_weight);
// 				fprintf(fp, " %s,", periodic_data.Accum_weight_unit);
// 				fprintf(fp, " %11.0f,",periodic_data.Rate);
// 				fprintf(fp, " %s/%s,",periodic_data.Accum_weight_unit,periodic_data.Rate_unit);
// 				fprintf(fp, " %f,", periodic_data.Inst_load_pcnt);
// 				fprintf(fp, " %f,", periodic_data.Inst_conv_angle);
// 				fprintf(fp, " %11.0f,", periodic_data.Inst_conv_speed);
// 	//Commented by DK on 12 sep 2014 for PCM 
// 					//			fprintf(fp, " %s", Strings[Units_var.Unit_speed].Text);
//   //			fprintf(fp, " %11.1f,", Rprts_diag_var.daily_Total_weight);			
// 				
// 					fprintf(fp, " %s,", periodic_data.Accum_weight_unit);		
// 				
// 				if(ferror(fp))
// 				{
// //					printf("\n Periodic log file write error");
// 				}
// 			}
// 			break;

// 		/*if the log is a set zero function data log*/
// 		case set_zero_log:
// 		case calib_log:
// 		case length_log:			
// 		case digital_log:
// 		case mat_test_log:
// 		case tst_wt_log:	
// 			fp = fopen ("calib_log.txt", "a");
// 			//fp = fopen ("set_zero_log.txt", "a");
// 			if(fp == NULL)
// 			{
// //				printf("\n Error in opening calib log file");
// 				//printf("\n Error in opening set zero file");
// 				err = 1;
// 			}
// 			else
// 			{
// 				if(log_type == set_zero_log)
// 				{
// 					fprintf(fp, "\r\n ZER,");
// 					fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
// 				}
// 				else if(log_type== length_log)
// 				{
// 					fprintf(fp, "\r\nLEN,");
// 					fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
// 				}
// 				else if(log_type == calib_log)
// 				{
// 					fprintf(fp, "\r\nDIG,");
// 					fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);					
// 				}
// 				else if(log_type == digital_log)
// 				{
// 					fprintf(fp, "\r\n DIG,");
// 					fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);					
// 				}
// 				else if(log_type == mat_test_log)
// 				{
// 					fprintf(fp, "\r\n MAT,");
// 					fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);					
// 				}
// 				else if(log_type == tst_wt_log)
// 				{
// 					fprintf(fp, "\r\n TST,");
// 					fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);					
// 				}

// 				
// 				fprintf(fp, "%d/%d/%d,", calibration_rprt.Cal_rprt_header.Date.Year,
// 																						calibration_rprt.Cal_rprt_header.Date.Mon,
// 																						calibration_rprt.Cal_rprt_header.Date.Day);
// 				fprintf(fp, "%02d:%02d:%02d,", calibration_rprt.Cal_rprt_header.Time.Hr,
// 																							calibration_rprt.Cal_rprt_header.Time.Min,
// 																							calibration_rprt.Cal_rprt_header.Time.Sec);
// //Commented by DK on 12 sep 2014 for PCM 
// 				/*				fprintf(fp, "%s,",Admin_var.Scale_Name);
// 				fprintf(fp, "%s,",Admin_var.Plant_Name);
// 				fprintf(fp, "%s,",Admin_var.Product_Name);
// 	*/			
// 				fprintf(fp, "%d,", length_cal_data_log.New_nos_pulses);
// 				fprintf(fp, "%f,", length_cal_data_log.New_time_duration);
// 				
// 				//Commented by DK on 12 sep 2014 for PCM 
// 			/*	fprintf(fp, "%f,", Calculation_struct.Belt_speed);
// 				fprintf(fp, "%s/min,", Strings[Scale_setup_var.Speed_unit].Text);*/
// 				
// 				if(log_type == set_zero_test_report)
// 				{
// 					fprintf(fp, "%f,", set_zero_data_log.Prev_set_zero_val);
// 					fprintf(fp, "%f,", set_zero_data_log.New_set_zero_val);
// 					fprintf(fp, "%f,", set_zero_data_log.Set_zero_accum_weight);
// 					//fprintf(fp, "%s,", Strings[Scale_setup_var.Weight_unit].Text);  //Commented by DK on 12 sep 2014 for PCM 
// 				}
// 				else
// 				{
// 					fprintf(fp, "NA,");
// 					fprintf(fp, "NA,");
// 					fprintf(fp, "NA,");
// 					fprintf(fp, "NA,");					
// 				}
// 				fprintf(fp, "%f,", set_zero_data_log.Inst_conv_angle);

// 				fprintf(fp, "%11.1f,", calib_data_log.Accum_weight);
// 				//fprintf(fp, "%s,", Strings[Scale_setup_var.Weight_unit].Text);  //Commented by DK on 12 sep 2014 for PCM 
// 				if(log_type == set_zero_log)
// 				{
// 					//Commented by DK on 12 sep 2014 for PCM 
// 					//fprintf(fp, "%f,", Calibration_var.Test_weight);
// 					//fprintf(fp, "%s,", Strings[Calibration_var.Test_zero_weight_unit].Text);
// 				}
// 				else
// 				{
// 					fprintf(fp, "NA,");
// 					fprintf(fp, "NA,");					
// 				}
// 				if(log_type == mat_test_log)
// 				{
// 					fprintf(fp, "%f,", calib_data_log.Cert_weight);
// 					//Commented by DK on 12 sep 2014 for PCM 
// 					/*
// 					fprintf(fp, "%s,", Strings[Calibration_var.Cert_scale_unit].Text);
// 					fprintf(fp, "%f,", Calibration_var.Belt_scale_weight);
// 					fprintf(fp, "%s,", Strings[Calibration_var.Belt_scale_unit].Text);*/
// 				}
// 				else
// 				{
// 					fprintf(fp, "NA,");
// 					fprintf(fp, "NA,");
// 					fprintf(fp, "NA,");
// 					fprintf(fp, "NA,");					
// 				}
// 				fprintf(fp, "%f,", calib_data_log.Prev_span);
// 				fprintf(fp, "%f,", calib_data_log.New_span);
// 				fprintf(fp, "%f,", calib_data_log.Idler_spacing1);
// 				fprintf(fp, "%f,", calib_data_log.Idler_spacing2);
// 				fprintf(fp, "%f,", calib_data_log.Idler_spacing3);




// // 				fprintf(fp, "\r\n%s,", set_zero_data_log.Set_zero_hdr.Data_type);
// // 				fprintf(fp, " %d,", set_zero_data_log.Set_zero_hdr.Id);
// // 				fprintf(fp, " %d/%d/%d,", set_zero_data_log.Set_zero_hdr.Date.Year,
// // 																					set_zero_data_log.Set_zero_hdr.Date.Mon,
// // 																					set_zero_data_log.Set_zero_hdr.Date.Day);
// // 				fprintf(fp, " %02d:%02d:%02d,", set_zero_data_log.Set_zero_hdr.Time.Hr,
// // 																					set_zero_data_log.Set_zero_hdr.Time.Min,
// // 																					set_zero_data_log.Set_zero_hdr.Time.Sec);
// // 				fprintf(fp, " %f,", set_zero_data_log.Prev_set_zero_val);
// // 				fprintf(fp, " %f,", set_zero_data_log.New_set_zero_val);
// // 				fprintf(fp, " %11.1f,", set_zero_data_log.belt_length);
// // 				fprintf(fp, " %s,", set_zero_data_log.belt_length_unit);
// // 				fprintf(fp, " %f", set_zero_data_log.Inst_conv_angle);
// 				if(ferror(fp))
// 				{
// //					printf("\n Set zero write error");
// 				}
// 			}
// 			break;

// 		/*if the log is a clear data function data log*/
// 		case clear_log:
// 			sprintf(&file_name[0],"%04d-%02d-%02d_periodic_log.txt",periodic_data.Periodic_hdr.Date.Year,
// 			periodic_data.Periodic_hdr.Date.Mon,periodic_data.Periodic_hdr.Date.Day);		
// 			fp = fopen (file_name, "a");
// 		//fp = fopen ("clr_data_log.txt", "a");
// 			if(fp == NULL)
// 			{
// //				printf("\n Error in opening periodic log file");
// 				//printf("\n Error in opening clear data log file");
// 				err = 1;
// 			}
// 			else
// 			{
// 				fprintf(fp, "\r\n%s,", clear_data_log.Clr_data_hdr.Data_type);
// 				fprintf(fp, " %d,", clear_data_log.Clr_data_hdr.Id);
// 				fprintf(fp, " %04d/%02d/%02d,", periodic_data.Periodic_hdr.Date.Year,
// 										periodic_data.Periodic_hdr.Date.Mon,periodic_data.Periodic_hdr.Date.Day);
// 				fprintf(fp, " %02d:%02d:%02d,", periodic_data.Periodic_hdr.Time.Hr,
// 																					periodic_data.Periodic_hdr.Time.Min,
// 																					periodic_data.Periodic_hdr.Time.Sec);
// 				fprintf(fp, " %11.1f",clear_data_log.Accum_weight);
// 				fprintf(fp, " %s", clear_data_log.Accum_weight_unit);				
// 				fprintf(fp, " %11.0f,",periodic_data.Rate);
// 				fprintf(fp, " %s/%s,",periodic_data.Accum_weight_unit,periodic_data.Rate_unit);
// 				fprintf(fp, " %f,", periodic_data.Inst_load_pcnt);
// 				fprintf(fp, " %f,", periodic_data.Inst_conv_angle);
// 				fprintf(fp, " %11.0f,", periodic_data.Inst_conv_speed);
// 		//Commented by DK on 12 sep 2014 for PCM 
// 				//		fprintf(fp, " %s", Strings[Units_var.Unit_speed].Text);
//   		//	fprintf(fp, " %11.1f,", Rprts_diag_var.daily_Total_weight);			
// 				fprintf(fp, " %s,", periodic_data.Accum_weight_unit);						
// 				if(ferror(fp))
// 				{
// //					printf("\n Clear data write error");
// 				}
// 			}
// 			break;

// 		/*if the log is a calibration function data log*/
// // 		case calib_log:
// // 			fp = fopen ("calib_log.txt", "a");
// // 			if(fp == NULL)
// // 			{
// // //				printf("\n Error in opening calib log file");
// // 				err = 1;
// // 			}
// // 			else
// // 			{
// // 				fprintf(fp, "\r\n%s,", calib_data_log.Cal_data_hdr.Data_type);
// // 				fprintf(fp, " %d,", calib_data_log.Cal_data_hdr.Id);
// // 				fprintf(fp, " %d/%d/%d,", calib_data_log.Cal_data_hdr.Date.Year,
// // 																					calib_data_log.Cal_data_hdr.Date.Mon,
// // 																					calib_data_log.Cal_data_hdr.Date.Day);
// // 				fprintf(fp, " %d:%d:%d,", calib_data_log.Cal_data_hdr.Time.Hr,
// // 																					calib_data_log.Cal_data_hdr.Time.Min,
// // 																					calib_data_log.Cal_data_hdr.Time.Sec);
// // 				  //Calculation of Accumilated weight during Calibration
// // 				calib_data_log.Accum_weight = Calculation_struct.Total_weight - calib_data_log.Accum_weight;
// // 				fprintf(fp, " %11.1f,", calib_data_log.Accum_weight);
// // 				fprintf(fp, " %s,", calib_data_log.Accum_weight_unit);
// // 				fprintf(fp, " %f,", calib_data_log.Cert_weight);
// // 				fprintf(fp, " %s,", calib_data_log.Cert_weight_unit);
// // 				fprintf(fp, " %f,", calib_data_log.Prev_span);
// // 				fprintf(fp, " %f,", calib_data_log.New_span);
// // 				fprintf(fp, " %f,", calib_data_log.Idler_spacing1);
// // 				fprintf(fp, " %f,", calib_data_log.Idler_spacing2);
// // 				fprintf(fp, " %f,", calib_data_log.Idler_spacing3);
// // 				fprintf(fp, " %d", calib_data_log.Cal_type);
// // 				if(ferror(fp))
// // 				{
// // //					printf("\n Calib log write error");
// // 				}
// // 			}
// // 			break;

// 		/*if the log is a error function data log*/
// 		case error_log:
// 			fp = fopen ("error_log.txt", "a");
// 			if(fp == NULL)
// 			{
// //				printf("\n Error in opening error log file");
// 				err = 1;
// 			}
// 			else
// 			{
// 				fprintf(fp, "\r\n%s,", error_data_log.Err_data_hdr.Data_type);
// 				fprintf(fp, " %d,", error_data_log.Err_data_hdr.Id);
// 				fprintf(fp, " %d/%d/%d,", error_data_log.Err_data_hdr.Date.Year,
// 																					error_data_log.Err_data_hdr.Date.Mon,
// 																					error_data_log.Err_data_hdr.Date.Day);
// 				fprintf(fp, " %d:%d:%d,", error_data_log.Err_data_hdr.Time.Hr,
// 																					error_data_log.Err_data_hdr.Time.Min,
// 																					error_data_log.Err_data_hdr.Time.Sec);
// 				fprintf(fp, " %s,",error_data_log.Error_code);
// 				fprintf(fp, " %s",error_data_log.Error_desc);
// 				if(ferror(fp))
// 				{
// //					printf("\n Error log file write error");
// 				}
// 			}
// 			break;

// 		/*if the log is a length function data log*/
// // 		case length_log:
// // 			fp = fopen ("calib_log.txt", "a");
// // 			if(fp == NULL)
// // 			{
// // //				printf("\n Error in opening calib log file");
// // 				err = 1;
// // 			}
// // 			else
// // 			{
// // 				fprintf(fp, "\r\n%s,", length_cal_data_log.Len_data_hdr.Data_type);
// // 				fprintf(fp, " %d,", length_cal_data_log.Len_data_hdr.Id);
// // 				fprintf(fp, " %d/%d/%d,", length_cal_data_log.Len_data_hdr.Date.Year,
// // 																					length_cal_data_log.Len_data_hdr.Date.Mon,
// // 																					length_cal_data_log.Len_data_hdr.Date.Day);
// // 				fprintf(fp, " %d:%d:%d,", length_cal_data_log.Len_data_hdr.Time.Hr,
// // 																					length_cal_data_log.Len_data_hdr.Time.Min,
// // 																					length_cal_data_log.Len_data_hdr.Time.Sec);
// // 				fprintf(fp, " %d,",length_cal_data_log.Prev_nos_pulses);
// // 				fprintf(fp, " %f,",length_cal_data_log.Prev_time_duration);
// // 				fprintf(fp, " %d,",length_cal_data_log.New_nos_pulses);
// // 				fprintf(fp, " %f",length_cal_data_log.New_time_duration);
// // 				if(ferror(fp))
// // 				{
// // //					printf("\n Length cal write error");
// // 				}
// // 			}
// // 			break;

// 		default:
// 			break;
// 	}

}





/*****************************************************************************
* @note       Function name: U8 report_file_write (U8 report_type)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : type of report to be written to be written
* @author			             : Anagha Basole
* @date       date created : 26/7/12
* @brief      Description	 : Writes the current report to the file
* @note       Notes		     : None
*****************************************************************************/
U8 report_file_write (U8 report_type)
{
	FILE *fp;
	U8 err = 0;
	char file_name[ARRAY_SIZE]={0};
	//write the structure in the file
	switch(report_type)
	{
		/*if the report is calibration report*/
		case calib_report:
		case material_test_report:
		case digital_test_report:
		case set_zero_test_report:
		case tst_wt_report:
		case length_cal_report:
			fp = fopen ("calib_report.txt", "a");
			if(fp == NULL)
			{
//				printf("\n Error in opening Cal report file");
				err = 1;
			}
			else
			{
				//fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", calibration_rprt.Cal_rprt_header.Data_type);
				fprintf(fp, "Report Type\t\t\t");
				if(report_type == calib_report)
					fprintf(fp, "DIG\r\n");
				else if(report_type == material_test_report)
					fprintf(fp, "MAT\r\n");
				else if(report_type == digital_test_report)
					fprintf(fp, "DIG\r\n");
				else if(report_type == set_zero_test_report)
					fprintf(fp, "ZER\r\n");		
				else if(report_type == length_cal_report)		
					fprintf(fp, "LEN\r\n");		
				else if(report_type == tst_wt_report)		
					fprintf(fp, "TST\r\n");							
				fprintf(fp, "Date\t\t\t\t%d/%d/%d\r\n", calibration_rprt.Cal_rprt_header.Date.Year,
																						calibration_rprt.Cal_rprt_header.Date.Mon,
																						calibration_rprt.Cal_rprt_header.Date.Day);
				fprintf(fp, "Time\t\t\t\t%02d:%02d:%02d\r\n", calibration_rprt.Cal_rprt_header.Time.Hr,
																							calibration_rprt.Cal_rprt_header.Time.Min,
																							calibration_rprt.Cal_rprt_header.Time.Sec);
				//Commented by DK on 12 sep 2014 for PCM 
				/*
				fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
				fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
				fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);*/
				
				//fprintf(fp, "NO of pulses in 1 revolution\t\t%d\r\n", length_cal_data_log.New_nos_pulses);
				fprintf(fp, "Time for 1 revolution\t\t%f\r\n", length_cal_data_log.New_time_duration);
				
				fprintf(fp, "Belt Length\t\t\t%11.1f\r\n", set_zero_data_log.belt_length);
				fprintf(fp, "Belt Length Unit\t\t\t%s\r\n", set_zero_data_log.belt_length_unit);
				
				//Commented by DK on 12 sep 2014 for PCM 
		//		fprintf(fp, "Belt Speed\t\t%f\r\n", Calculation_struct.Belt_speed);
			//	fprintf(fp, "Belt Speed Unit\t\t%s/min\r\n", Strings[Scale_setup_var.Speed_unit].Text);
				
				fprintf(fp, "Conveyor Angle(Degrees)\t\t\t%f\r\n", set_zero_data_log.Inst_conv_angle);
				
				//Commented by DK on 12 sep 2014 for PCM 
				/*
				fprintf(fp, "No of scale idlers\t\t\t%d\r\n",Scale_setup_var.Number_of_idlers);
				if(Scale_setup_var.Number_of_idlers>=1)
					fprintf(fp, "Idler_spacing1\t\t%f\r\n", calib_data_log.Idler_spacing1);
				else
					fprintf(fp, "Idler_spacing1\t\tNA\r\n");
				if(Scale_setup_var.Number_of_idlers>=1)
					fprintf(fp, "Idler_spacing2\t\t%f\r\n", calib_data_log.Idler_spacing2);
				else
					fprintf(fp, "Idler_spacing2\t\tNA\r\n");
				if(Scale_setup_var.Number_of_idlers>=2)
					fprintf(fp, "Idler_spacing3\t\t%f\r\n", calib_data_log.Idler_spacing3);
				else
					fprintf(fp, "Idler_spacing3\t\tNA\r\n");				
				if(Scale_setup_var.Number_of_idlers>=3)
					fprintf(fp, "Idler_spacing4\t\t%f\r\n", calib_data_log.Idler_spacing2);
				else
					fprintf(fp, "Idler_spacing4\t\tNA\r\n");				
				if(Scale_setup_var.Number_of_idlers>=4)
					fprintf(fp, "Idler_spacing5\t\t%f\r\n", calib_data_log.Idler_spacing3);				
				else
					fprintf(fp, "Idler_spacing5\t\tNA\r\n");
				*/
				if(report_type == set_zero_test_report)
				{
					fprintf(fp, "Prev set zero value\t\t%f\r\n", set_zero_data_log.Prev_set_zero_val);
					fprintf(fp, "New set zero value\t\t%f\r\n", set_zero_data_log.New_set_zero_val);
					fprintf(fp, "Accumulated Weight\t%f\r\n", set_zero_data_log.Set_zero_accum_weight);
		//			fprintf(fp, "Accumulated weight unit %s\r\n\r\n", Strings[Scale_setup_var.Weight_unit].Text);  //Commented by DK on 12 sep 2014 for PCM 
				}

				if(report_type == tst_wt_report)
				{
		//Commented by DK on 12 sep 2014 for PCM 
					//			fprintf(fp, "Total Test Weight \t\t%f\r\n", Calibration_var.Test_weight);
			//		fprintf(fp, "Total Test Weight Unit %s\r\n", Strings[Calibration_var.Test_zero_weight_unit].Text);
					fprintf(fp, "Previous Calibration TRIM factor\t%f\r\n", calib_data_log.Prev_span);
					fprintf(fp, "New Calibration TRIM factor\t\t%f\r\n", calib_data_log.New_span);
				}
				if(report_type == material_test_report)
				{
					fprintf(fp, "Certified Scale weight\t\t%f\r\n", calib_data_log.Cert_weight);
					//Commented by DK on 12 sep 2014 for PCM 
					//fprintf(fp, "Certified Scale weight unit %s\r\n", Strings[Calibration_var.Cert_scale_unit].Text);
					//fprintf(fp, "Belt Scale weight\t\t%f\r\n", Calibration_var.Belt_scale_weight);
					//fprintf(fp, "Belt Scale weight unit %s\r\n", Strings[Calibration_var.Belt_scale_unit].Text);
					fprintf(fp, "Previous Calibration TRIM factor\t%f\r\n", calib_data_log.Prev_span);
					fprintf(fp, "New Calibration TRIM factor\t\t%f\r\n", calib_data_log.New_span);	
					//fprintf(fp, "TRIM Difference\t\t%f\r\n\r\n",Calibration_var.span_diff);					
				}
				if(report_type == digital_test_report || report_type == calib_report)
				{
					fprintf(fp, "Previous Calibration TRIM factor\t%f\r\n", calib_data_log.Prev_span);
					fprintf(fp, "New Calibration TRIM factor\t\t%f\r\n", calib_data_log.New_span);					
		//			fprintf(fp, "TRIM Difference\t\t%f\r\n\r\n",Calibration_var.span_diff);
				}

				if(ferror(fp))
				{
//					printf("\n Cal report file write error");
				}
			}
			break;

		/*if the report is error report*/
		case 2:
			fp = fopen ("error_report.txt", "a");
			if(fp == NULL)
			{
//				printf("\n Error in opening error report file");
				err = 1;
			}
			else
			{
				fprintf(fp, "\r\n\nData Type\t%s\r\n", error_rprt.Err_rprt_header.Data_type);
				fprintf(fp, "Report Type\t%s\r\n", error_rprt.Err_rprt_header.Report_type);
				fprintf(fp, "Date\t\t%d/%d/%d\r\n", error_rprt.Err_rprt_header.Date.Year,
																						error_rprt.Err_rprt_header.Date.Mon,
																						error_rprt.Err_rprt_header.Date.Day);
				fprintf(fp, "Time\t\t%d:%d:%d\r\n", error_rprt.Err_rprt_header.Time.Hr,
																							error_rprt.Err_rprt_header.Time.Min,
																							error_rprt.Err_rprt_header.Time.Sec);
			//	fprintf(fp, "Scale name\t%s\r\n",Admin_var.Scale_Name);
			//	fprintf(fp, "Plant name\t%s\r\n",Admin_var.Plant_Name);
			//	fprintf(fp, "Product name\t%s\r\n",Admin_var.Product_Name);
				fprintf(fp, "Error code\t%s\r\n",error_data_log.Error_code);
				fprintf(fp, "Error Desc:\t%s\r\n",error_data_log.Error_desc);
				if(ferror(fp))
				{
//					printf("\n Error report file write error");
				}
			}
			break;

		/*if the report is daily report*/
		case 3:
			sprintf(&file_name[0],"%04d-%02d-%02d_daily_report.txt",daily_rprt.freq_rprt_header.Date.Year,
			daily_rprt.freq_rprt_header.Date.Mon,daily_rprt.freq_rprt_header.Date.Day);
			fp = fopen (file_name, "w");
			if(fp == NULL)
			{
//				printf("\n Error in opening daily report file");
				err = 1;
			}
			else
			{
				fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", daily_rprt.freq_rprt_header.Data_type);
				fprintf(fp, "Report Type\t\t\t%s\r\n", daily_rprt.freq_rprt_header.Report_type);
				fprintf(fp, "Report Date\t\t\t\t%d/%d/%d\r\n", daily_rprt.freq_rprt_header.Date.Year,
																						daily_rprt.freq_rprt_header.Date.Mon,
																						daily_rprt.freq_rprt_header.Date.Day);
				fprintf(fp, "Power ON Time\t\t\t\t%02d:%02d:%02d\r\n", daily_rprt.freq_rprt_header.Time.Hr,
																							daily_rprt.freq_rprt_header.Time.Min,
																							daily_rprt.freq_rprt_header.Time.Sec);
		//		fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
			//	fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
				//fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);
				if(daily_rprt.Start_time.Hr == 24)
				{
					fprintf(fp, "Belt Start Time\t\t\tNo Start Detected\r\n");					
				}
				else
				{					
					fprintf(fp, "Belt Start Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.Start_time.Hr,
																									 daily_rprt.Start_time.Min,
																									 daily_rprt.Start_time.Sec);					
				}
				if(daily_rprt.Start_load_time.Hr == 24)
				{
					fprintf(fp, "Load Start Time\t\t\tNo Start Load Detected\r\n");										
				}
				else
				{
				fprintf(fp, "Load Start Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.Start_load_time.Hr,
																												daily_rprt.Start_load_time.Min,
																												daily_rprt.Start_load_time.Sec);
				}
				if(daily_rprt.End_load_time.Hr == 24)
				{
					fprintf(fp, "Load Stop Time\t\t\tNo End Load Detected\r\n");										
				}
				else
				{
				fprintf(fp, "Load Stop Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.End_load_time.Hr,
																											daily_rprt.End_load_time.Min,
																											daily_rprt.End_load_time.Sec);
				}
				if(daily_rprt.End_time.Hr == 24)
				{
					fprintf(fp, "Belt Stop Time\t\t\tNo End Detected\r\n");					
				}
				else
				{
					fprintf(fp, "Belt Stop Time\t\t\t%02d:%02d:%02d\r\n",daily_rprt.End_time.Hr,
																									 daily_rprt.End_time.Min,
																									 daily_rprt.End_time.Sec);
				}
				fprintf(fp, "Loaded Run Time(Hours)\t\t\t%4.3f\r\n",(float)(daily_rprt.Run_time/60.0));
				fprintf(fp, "Running Empty + Stopped Time(Hours)\t\t\t%4.3f\r\n",(float)(daily_rprt.Down_time/60.0));
				fprintf(fp, "Total Report Time(Hours)\t\t\t%4.3f\r\n",(float)(daily_rprt.Total_time/60.0));
				fprintf(fp, "Number of Belt Stops\t\t%d\r\n",daily_rprt.Cnt_zero_speed);
				fprintf(fp, "Report Period Average.Production Rate\t\t\t%11.2f\r\n",daily_rprt.Average_rate);
				fprintf(fp, "Rate Unit\t\t\t%s/%s\r\n", daily_rprt.Total_weight_unit,daily_rprt.Total_rate_unit);
				fprintf(fp, "Total Weight\t\t\t%11.2f\r\n",daily_rprt.Total_weight);
				fprintf(fp, "Weight Unit\t\t\t%s\r\n", daily_rprt.Total_weight_unit);
				if(ferror(fp))
				{
//					printf("\n Daily report file write error");
				}
				//reset the values after writing them on the USB drive
				daily_rprt.Run_time = daily_rprt.Down_time = daily_rprt.Total_time = daily_rprt.Cnt_zero_speed = 0;
				daily_rprt.Start_time.Hr = daily_rprt.Start_time.Min = daily_rprt.Start_time.Sec = 0;
				daily_rprt.Start_load_time.Hr = daily_rprt.Start_load_time.Min = daily_rprt.Start_load_time.Sec = 0;
				daily_rprt.End_load_time.Hr = daily_rprt.End_load_time.Min = daily_rprt.End_load_time.Sec = 0;
				daily_rprt.End_time.Hr = daily_rprt.End_time.Min = daily_rprt.End_time.Sec = 0;
			}
			break;

		/*if the report is weekly report*/
		case 4:
			fp = fopen ("weekly_report.txt", "a");
			if(fp == NULL)
			{
//				printf("\nError in opening weekly report file");
				err = 1;
			}
			else
			{
				fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", weekly_rprt.freq_rprt_header.Data_type);
				fprintf(fp, "Report Type\t\t\t%s(Sun-Sat)\r\n", weekly_rprt.freq_rprt_header.Report_type);
				fprintf(fp, "Report Date\t\t\t\t%d/%d/%d\r\n", weekly_rprt.freq_rprt_header.Date.Year,
																						weekly_rprt.freq_rprt_header.Date.Mon,
																						weekly_rprt.freq_rprt_header.Date.Day);
				fprintf(fp, "Power ON Time\t\t\t\t%02d:%02d:%02d\r\n", weekly_rprt.freq_rprt_header.Time.Hr,
																							weekly_rprt.freq_rprt_header.Time.Min,
																							weekly_rprt.freq_rprt_header.Time.Sec);
		//		fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
			//	fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
				//fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);
				fprintf(fp, "Loaded Run Time(Hours)\t\t\t%4.3f\r\n",(float)(weekly_rprt.Run_time/60.0));
				fprintf(fp, "Running Empty + Stopped Time(Hours)\t\t\t%4.3f\r\n",(float)(weekly_rprt.Down_time/60.0));
				fprintf(fp, "Total Report Time(Hours)\t\t\t%4.3f\r\n",(float)(weekly_rprt.Total_time/60.0));
				fprintf(fp, "Number of belt stops\t\t%d\r\n",weekly_rprt.Cnt_zero_speed);
				fprintf(fp, "Report Period Avg.Production Rate\t\t\t%11.2f\r\n",weekly_rprt.Average_rate);
				fprintf(fp, "Rate Unit\t\t\t%s/%s\r\n", weekly_rprt.Total_weight_unit,weekly_rprt.Total_rate_unit);
				fprintf(fp, "Total Weight\t\t\t%11.2f\r\n",weekly_rprt.Total_weight);
				fprintf(fp, "Weight Unit\t\t\t%s\r\n", weekly_rprt.Total_weight_unit);
				if(ferror(fp))
				{
//					printf("\n Weekly report file write error");
				}
				//reset the values after writing them on the USB drive
				weekly_rprt.Run_time = weekly_rprt.Down_time = weekly_rprt.Total_time = weekly_rprt.Cnt_zero_speed = 0;
			}
			break;

		/*if the report is monthly report*/
		case 5:
			fp = fopen ("monthly_report.txt", "a");
			if(fp == NULL)
			{
//				printf("\nError in opening monthly report file");
				err = 1;
			}
			else
			{
				fprintf(fp, "\r\n\nData Type\t\t\t%s\r\n", monthly_rprt.freq_rprt_header.Data_type);
				fprintf(fp, "Report Type\t\t\t%s\r\n", monthly_rprt.freq_rprt_header.Report_type);
				fprintf(fp, "Report Date\t\t\t\t%d/%d/%d\r\n", monthly_rprt.freq_rprt_header.Date.Year,
																						monthly_rprt.freq_rprt_header.Date.Mon,
																						monthly_rprt.freq_rprt_header.Date.Day);
				fprintf(fp, "Power ON Time\t\t\t\t%02d:%02d:%02d\r\n", monthly_rprt.freq_rprt_header.Time.Hr,
																						monthly_rprt.freq_rprt_header.Time.Min,
																						monthly_rprt.freq_rprt_header.Time.Sec);
	//			fprintf(fp, "Scale name\t\t\t%s\r\n",Admin_var.Scale_Name);
		//		fprintf(fp, "Plant name\t\t\t%s\r\n",Admin_var.Plant_Name);
			//	fprintf(fp, "Product name\t\t\t%s\r\n",Admin_var.Product_Name);
				fprintf(fp, "Loaded Run Time(Hours)\t\t\t%4.3f\r\n",(float)(monthly_rprt.Run_time/60.0));
				fprintf(fp, "Running Empty + Stopped Time(Hours)\t\t\t%4.3f\r\n",(float)(monthly_rprt.Down_time/60.0));
				fprintf(fp, "Total Report Time(Hours)\t\t\t%4.3f\r\n",(float)(monthly_rprt.Total_time/60.0));
				fprintf(fp, "Number of Belt stops\t\t%d\r\n",monthly_rprt.Cnt_zero_speed);
				fprintf(fp, "Report Period Average.Production Rate\t\t\t%11.2f\r\n",monthly_rprt.Average_rate);
				fprintf(fp, "Rate Unit\t\t\t%s/%s\r\n", monthly_rprt.Total_weight_unit,monthly_rprt.Total_rate_unit);
				fprintf(fp, "Total Weight\t\t\t%11.2f\r\n",monthly_rprt.Total_weight);
				fprintf(fp, "Weight Unit\t\t\t%s\r\n", monthly_rprt.Total_weight_unit);
				if(ferror(fp))
				{
//					printf("\n Monthly report file write error");
				}
				//reset the values after writing them on the USB drive
				monthly_rprt.Run_time = monthly_rprt.Down_time = monthly_rprt.Total_time = monthly_rprt.Cnt_zero_speed = 0;
			}
			break;

		default:
			break;
	}
	if(0 != fclose(fp))
	{
//		printf ("\nFile could not be closed!\n");
	}
	fflush (stdout);
	return (err);
}

/*****************************************************************************
* @note       Function name: U8 config_backup_write (char * stored_file_name)
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : pointer to the variable where file name is to be stored
* @author			             : Anagha Basole
* @date       date created : 26/7/12
* @brief      Description	 : Writes the current report to the file
* @note       Notes		     : None
*****************************************************************************/
U8 config_backup_write (char * stored_file_name)
{
		FILE *fp;
		U8 err = 0;
		U8 i;
		char file_name[ARRAY_SIZE];

		fheader.File_hdr.Date.Day = (U8) Current_time.RTC_Mday;
		fheader.File_hdr.Date.Mon = (U8) Current_time.RTC_Mon;
		fheader.File_hdr.Date.Year = (U16) Current_time.RTC_Year;
		//fheader.File_hdr.Time.Hr = (U8) Current_time.RTC_Hour;
//		if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)  //Commented by DK on 12 sep 2014 for PCM 
			fheader.File_hdr.Time.Hr   = (U8) Current_time.RTC_Hour;
		/*else if(Admin_var.Current_Time_Format == TWELVE_HR)
		{
			if(Admin_var.AM_PM == AM_TIME_FORMAT)
			{
				if((U8) Current_time.RTC_Hour ==12)
					fheader.File_hdr.Time.Hr   = 0;			
				else
					fheader.File_hdr.Time.Hr   = (U8) Current_time.RTC_Hour;			
			}
			else
			{
				fheader.File_hdr.Time.Hr   = (U8) Current_time.RTC_Hour + 12;			
			}		
		}	
*/		
		fheader.File_hdr.Time.Min = (U8) Current_time.RTC_Min;

		/*for(i=0; Admin_var.Scale_Name[i] != '\0'; i++)
		{
			file_name[i] = Admin_var.Scale_Name[i];
		}*/

		sprintf(&file_name[i], "%d", fheader.File_hdr.Date.Year);
		i=i+4;
		sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Date.Mon);
		i+=2;

		sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Date.Day);
		i+=2;

		sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Time.Hr);
		i+=2;

		sprintf(&file_name[i], "%02d", (U16)fheader.File_hdr.Time.Min);
		i+=2;

		strcat(&file_name[i], ".bat");

    strcpy(stored_file_name, file_name);
		//fp = fopen ("config_backup.bat", "w");
		fp = fopen (file_name, "wb");
		if(fp == NULL)
		{
//			printf("\n error in opening backup file");
			err = 1;
		}
		else
		{
			/*write the LCD structure to a file*/
			//Commented by DK on 12 sep 2014 for PCM 
			/*
			fwrite((U16 *)&Scale_setup_var, 1, sizeof(Scale_setup_var), fp);

			fwrite((U16 *)&Calibration_var, 1, sizeof(Calibration_var), fp);

			fwrite((U16 *)&Setup_device_var, 1, sizeof(Setup_device_var), fp);

			fwrite((U16 *)&Rprts_diag_var, 1, sizeof(Rprts_diag_var), fp);

			fwrite((U16 *)&Admin_var, 1, sizeof(Admin_var), fp); */
			if(ferror(fp))
			{
//				printf("\n Backup file write error");
			}
		}
		if(0 != fclose(fp))
		{
//			printf ("\nBackup file could not be closed!\n");
		}

		fflush (stdout);
		return (err);
}

/*****************************************************************************
* @note       Function name: U8 config_backup_read (char filename[16])
* @returns    returns			 : '0' if file update was successful
*													 : '1' if file open failed
* @param      arg1			   : file to be read back
* @author			             : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description	 : Reads and loads the current configuration from the file
* @note       Notes		     : None
*****************************************************************************/
U8 config_backup_read (char file_name[25])
{
	FILE *fp;
	U8 err = 0;

	fp = fopen (file_name, "rb");
	if(fp == NULL)
	{
//		printf("\n error in opening file");
		err = 1;
	}
	else
	{
		/*read the LCD structure from the file*/
//Commented by DK on 12 sep 2014 for PCM 
/*		
		fread((U16 *)&Scale_setup_var, 1, sizeof(Scale_setup_var), fp);

		fread((U16 *)&Calibration_var, 1, sizeof(Calibration_var), fp);

		fread((U16 *)&Setup_device_var, 1, sizeof(Setup_device_var), fp);

		fread((U16 *)&Rprts_diag_var, 1, sizeof(Rprts_diag_var), fp);

		fread((U16 *)&Admin_var, 1, sizeof(Admin_var), fp);*/
		//eeprom_struct_backup();  //Commented by DK on 12 Sep 2014 
		if(ferror(fp))
		{
//			printf("\n Backup file read error");
		}
		if(0 != fclose(fp))
		{
//				printf ("\nBackup file could not be closed!\n");
		}
	}

	fflush (stdout);
	return (err);
}


/*****************************************************************************
 * @note       Function name  : char FwUpgrade(char *HexFilename, char length)
 * @returns    returns        : status of the firmware upgrade
 * @param      arg1           : name of the file to be used for upgrade
 *             arg2           : length of the file name
 * @author                    : Shweta Pimple
 * @date       date created   : 6th March 2013
 * @brief      Description    : Upgrade the firmware of integrator board
 * @note       Notes          :
 *****************************************************************************/
char FwUpgrade(char *HexFilename, char length) {
	unsigned char *pFlash;
	IAP_STATUS_CODE status;
	int i = 0;
	// U16 * read_flash_addr;
	//union double_union buffer;
	//U16 * u16Var;
	FILE *fp;
	int Msg_Log_No = 0;


	// Backup of all GUI structures and Accumilated weight
	Struct_backup_fg = 1;
	eeprom_struct_backup();
	Struct_backup_fg = 0;

	i = 1000;
	while (i--)
		;

	MATRIX_ARB |= 0x00010000;

	//some delay
	i = 10;
	while (i--)
		;

	memset(ConfigBuf, 0xFF, sizeof(ConfigBuf));
	memcpy(&ConfigBuf[4], HexFilename, length);
	//Store the .hex file name into config area of flash
	pFlash = (unsigned char*) (FLASH_CONFIG_AREA);

//	Write_Debug_Logs_into_file(Msg_Log_No++);
	status = EraseSector(FLASH_CONFIG_SECTOR, FLASH_CONFIG_SECTOR);
//	Write_Debug_Logs_into_file(Msg_Log_No++);

	if (status != CMD_SUCCESS) {
		os_tsk_prio_self(2);
		return _FALSE;
	}

//	Write_Debug_Logs_into_file(Msg_Log_No++);
	status = CopyRAM2Flash(pFlash, ConfigBuf, IAP_WRITE_256);
//	Write_Debug_Logs_into_file(Msg_Log_No++);

	if (status != CMD_SUCCESS) {
		os_tsk_prio_self(2);
		return _FALSE;
	}
// 	system_log.Id = LPC_RTC->GPREG2 = 0;
// 	edit_log.Id = LPC_RTC->GPREG3 = 0;
	// __enable_irq();

//**********
	funinit("U0:"); 								//uninit file system

	__disable_irq();
__DSB();
    LPC_IOCON->P0_23 &=(~0x3<<3) ;
    LPC_IOCON->P0_23 |=0x01<<3 ;
    LPC_IOCON->P3_30 &=(~0x3<<3) ;
    LPC_IOCON->P3_30 |=0x01<<3 ;
    LPC_GPIO3->DIR |=0x01<<30;
    LPC_GPIO3->SET |=0x01<<30;
	
    DelayMs(100);
    LPC_GPIO3->CLR |=0x01<<30;
    DelayMs(100);
    LPC_GPIO3->SET |=0x01<<30;																														
__DSB(); 	
	LPC_EMC->Control = 0x0000; // EMC disable, Normal memory map
	LPC_SC->PCONP &= ~(0x80000800); /* Disable USB and EMC Interface */
	//********************

	//reset the system
	NVIC_SystemReset();

	while (1) {
		;
	}

	//os_tsk_prio_self(3);
	//return _TRUE;
}

/*
void _WriteByte2File(U8 Data, void * p) 
{
	int WR_Count = 0;
	int Wait_Count =5000;
	//WR_Count = fwrite(&Data, sizeof(Data), 1, p);
	WR_Count = fputc(Data, p);
	while(Wait_Count)
	{
		Wait_Count = Wait_Count -1;
	}
		
	
	 if(WR_Count != Data)
	 {
		WR_Count = WR_Count;
	 }		
}
*/
#endif /*#ifdef FILE_SYS*/
/*****************************************************************************
* End of file
*****************************************************************************/
