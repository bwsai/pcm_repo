/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : File_update.h
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 16, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __FILE_UPDATE_H
#define __FILE_UPDATE_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "rtc.h"

#ifdef FILE_SYS
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */		

/*============================================================================
* Public Data Types
*===========================================================================*/	
	/*Data log header*/
	typedef struct
	{
			U8 Data_type[5];		                      
			U16 Id;        												 /*Incremental Number to indicate unique records and order*/
			DATE_STRUCT Date;     
			TIME_STRUCT Time;
	}DATA_LOG_HEADER_STRUCT;

	/*Header information for Data storage*/
	typedef struct
	{
			DATA_LOG_HEADER_STRUCT File_hdr;              /*= HDR*/
	}FILE_HEADER_STRUCT;

	/*Periodic Data Log
		 These items will be stored every 1, 10, 60 minutes based on what is selected via the User Interface*/
	typedef struct
	{
			DATA_LOG_HEADER_STRUCT Periodic_hdr;	 /*Data type = DAT*/
			float Accum_weight;									 /*Weight Accumulated Since Last "CLR"*/			
			float Rate;													 /*Instantaneous Rate*/			
			float Inst_load_pcnt;								 /*Instantaneous Load Percentage*/
			float Inst_conv_angle; 							 /*Instantaneous Angle of the conveyor*/
			float Inst_conv_speed;								 /*Instantaneous Speed of the conveyor*/
			char Speed_unit[15];							 		 /*Speed unit*/
			char Accum_weight_unit[15];						 /*Accumulated weight unit*/
			char Rate_unit[15];										 /*Rate Unit*/
		  U8 Dummy_var1;
			U8 Report_type;											   /*Used to decide the size of the report-fixed for a particular report*/
	}PERIODIC_LOG_STRUCT;

	typedef struct
	{
//			DATA_LOG_HEADER_STRUCT System_log_hdr;	 /*Data type = DAT*/
			U16 Id;        												 /*Incremental Number to indicate unique records and order*/
			DATE_STRUCT Date;     
			TIME_STRUCT Time;		
			char log_buf[700];						 

	}SYSTEM_LOG_STRUCT;		
	typedef struct
	{
//			DATA_LOG_HEADER_STRUCT System_log_hdr;	 /*Data type = DAT*/
			U16 Id;        												 /*Incremental Number to indicate unique records and order*/
			DATE_STRUCT Date;     
			TIME_STRUCT Time;		
			char edit_log_buf[100];						 

	}EDIT_LOG_STRUCT;	
	
	/*Set Zero Function Data Log	
	These items will be stored only when a Set Zero Function has completed successfully	*/
	typedef struct
	{
			DATA_LOG_HEADER_STRUCT Set_zero_hdr;	         /*Data type = ZER*/
			float Prev_set_zero_val;							         /*The set zero weight before the set zero function*/
			float New_set_zero_val;							           /*The new set zero weight calculated after the set zero function*/
			float Set_zero_accum_weight;					         /*Weight Accumulated of only through the Set Zero function*/	
      float belt_length;	                           /*Belt length*/
			float Inst_conv_angle; 							           /*Instantaneous Angle of the conveyor*/
			char belt_length_unit[15];						         /*Belt length unit*/
		  char Accum_weight_unit[15];						         /*Accumulated weight unit*/
			//U32 Dummy_var1;
			//U16 Dummy_var2;
			U8 Report_type;											           /*Used to decide the size of the report-fixed for a particular report*/
	}SET_ZERO_LOG_STRUCT;

	/*Clear Data Function Data Log	
	These items will be stored only when a Clear Function has completed successfully*/
	typedef struct
	{
			DATA_LOG_HEADER_STRUCT Clr_data_hdr;	         /*Data type = CLR*/
			float Accum_weight;									           /*Weight Accumulated Up to the Clear Function*/
			char Accum_weight_unit[15];						         /*Accumulated weight unit*/
			//U32 Dummy_var1;
			//U16 Dummy_var2;
			U8 Report_type;											           /*Used to decide the size of the report-fixed for a particular report*/
	}CLR_DATA_LOG_STRUCT;

	/*Length Function Data Log	
	These items will be stored only when a Length Function has completed successfully	*/
	typedef struct
	{
			DATA_LOG_HEADER_STRUCT Len_data_hdr;					 /*Data type = CAL*/
			float Prev_time_duration;						         /*Previous Time Duration*/
			float New_time_duration;						           /*New Time Duration*/
			U16 Prev_nos_pulses;								           /*Previous Number of Pulses*/
			U16 New_nos_pulses;								             /*New Number of Pulses*/
			U16 Dummy_var1;
			U8 Dummy_var2;
			U8 Report_type;											           /*Used to decide the size of the report-fixed for a particular report*/
	}LENGTH_DATA_LOG_STRUCT;
	
	/*Calibration Function Data Log	
	These items will be stored only when a Calibration Function has completed successfully	*/
	typedef struct
	{
			DATA_LOG_HEADER_STRUCT Cal_data_hdr;					 /*Data type = CAL*/
			float Accum_weight;									           /*Weight Accumulated during the Calibration function*/			
			float Cert_weight;										         /*Weight entered by the user of the certified weight*/						
			float Idler_spacing1;								           /*Distance between idler 1 and 2*/
			float Idler_spacing2;								           /*Distance between idler 2 and 3*/
			float Idler_spacing3;								           /*Distance between idler 3 and 4*/			
			float Prev_span;												       /*The Calibration Factor prior to the new Calibration Function*/
			float	New_span;													       /*Number used to adjust the calibration*/
			U16 Cal_type;													         /*Type of calibration conducted*/
			char Accum_weight_unit[15];						         /*Accumulated weight unit*/
			char Cert_weight_unit[15];						         /*Certified weight unit*/
		  U16 Dummy_var1;
			U8 Dummy_var2;
			U8 Report_type;											           /*Used to decide the size of the report-fixed for a particular report*/
	}CAL_DATA_LOG_STRUCT;

	/*Error Function Data Log	
	These items will be stored only when an error occurs*/	
	typedef struct
	{
			DATA_LOG_HEADER_STRUCT Err_data_hdr;					 /*Data type = ERR*/
			char Error_code[5];
			char Error_desc[82];
			U8 Report_type;											           /*Used to decide the size of the report-fixed for a particular report*/
	}ERR_DATA_LOG_STRUCT;

	/*Report header, common for all reports*/
	typedef struct
	{
			U8 Data_type[6];
			U8 Report_type[15];
			DATE_STRUCT Date;
			TIME_STRUCT Time;
	}REPORT_HEADER_STRUCT;

	/*Calibration Report	*/
	typedef struct
	{
			REPORT_HEADER_STRUCT Cal_rprt_header;			     /*Data type = REP, Calibration Report*/
	}CAL_REPORT_STRUCT;

	/*Error Report	*/
	typedef struct
	{
			REPORT_HEADER_STRUCT Err_rprt_header;				   /*Data type = REP, Error Report*/
	}ERR_REPORT_STRUCT;

	/*Daily, Weekly, Monthly Report*/
	typedef struct
	{
			REPORT_HEADER_STRUCT freq_rprt_header;				 /*Data type = REP */
			TIME_STRUCT Start_time;											   /*Time Belt Starts = Speed > 0*/
			TIME_STRUCT Start_load_time;									 /*Time Belt Load > Downtime rate*/
			TIME_STRUCT End_load_time;										 /*Time of Last Belt Load > Downtime*/
			TIME_STRUCT End_time;												   /*Last time Belt Stops*/
			float Average_rate;											       /*The total accumulated weight divided by the total time*/			
			double Total_weight;											       /*The total accumulated weight*/
			U32 Run_time;													         /*Total Run Time*/
			U32 Down_time;												         /*Time spent below downtime rate*/
			U32 Total_time;												         /*Total time*/ 			
			U32 Cnt_zero_speed;										         /*Number of times the speed goes to 0*/
			char Total_rate_unit[15];
			char Total_weight_unit[15];
		  U16 Dummy_var1;
	}FREQUENT_REPORT_STRUCT;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
	extern FREQUENT_REPORT_STRUCT daily_rprt, weekly_rprt, monthly_rprt;
	extern ERR_REPORT_STRUCT 			error_rprt;
	extern CAL_REPORT_STRUCT 			calibration_rprt;
	extern ERR_DATA_LOG_STRUCT 		error_data_log;
	extern CAL_DATA_LOG_STRUCT 		calib_data_log;
	extern CLR_DATA_LOG_STRUCT 		clear_data_log;
	extern SET_ZERO_LOG_STRUCT 		set_zero_data_log;
	extern PERIODIC_LOG_STRUCT 		periodic_data;
	extern FILE_HEADER_STRUCT 		fheader;
	extern LENGTH_DATA_LOG_STRUCT length_cal_data_log;
	extern SYSTEM_LOG_STRUCT		system_log;
	extern EDIT_LOG_STRUCT		edit_log;
	typedef enum 
							{
								periodic_log = 1,
								set_zero_log,
								clear_log,
								calib_log,
								error_log,
								length_log,
								periodic_log_start,
								periodic_log_stop,
								periodic_log_stop_at_start,
								digital_log,
								mat_test_log,
								tst_wt_log,
								system_log1,
								edit_log1,
							}TYPE_OF_LOG;
							
	typedef enum 
							{
								calib_report = 1,
								error_report,
								daily_report,
								weekly_report,
								monthly_report,								
								populate_regular_reports,
								material_test_report,
								digital_test_report,
								set_zero_test_report,
								tst_wt_report,
								length_cal_report,
							}TYPE_OF_REPORT;
/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern U8 log_file_init(char file_name[25]);
extern U8 report_file_write (U8 report_type);
extern U8 log_file_write(U8 log_type);
extern U8 config_backup_write (char * stored_file_name);
extern U8 config_backup_read (char filename[25]);
extern U8 debug_log_write(const U8 *buf);
//extern void _WriteByte2File(U8 Data, void * p);
extern char FwUpgrade(char *HexFilename, char length);
							
#endif /*ifdef FILE_SYS*/

#endif /*__FILE_UPDATE_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
