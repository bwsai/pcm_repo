/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : File_update.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 26, 2012>
* @date Last Modified  : July Thursday, 2012  <July 26, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "Plant_connect.h"
#include <RTL.h>
#include <float.h>
#include <string.h>

//Commented by DK on 12 sep 2014 for PCM 
//#include "Screen_global_ex.h"
//#include "Screen_data_enum.h"
//#include "Conversion_functions.h"

#ifdef MODBUS_TCP
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
 #define PLANT_CONNECT_METERS_FLAG      2
 #define PLANT_CONNECT_FEET_FLAG        1
 
 #define PLANT_CONNECT_MINUTES_FLAG     2
 #define PLANT_CONNECT_HOURS_FLAG       1
 
 #define PLANT_CONNECT_KG_FLAG          5
 #define PLANT_CONNECT_TONNE_FLAG       4
 #define PLANT_CONNECT_LBS_FLAG         3
 #define PLANT_CONNECT_LONG_TON_FLAG    2
 #define PLANT_CONNECT_SHORT_TON_FLAG   1
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
 PLANT_CONNECT_STRUCT Plant_connect_var, Plant_connect_var_read;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
void populate_error_code_plant_connect_var(U16 error_code)
{
	 if (Plant_connect_var.Error_code1 == 0)
	 {
		  plant_connect_struct_write(0, PLANT_CONNECT_NO_CAL_FLAG, error_code, 0, 0, PLANT_CONNECT_ERROR_RCRD_FLAG);
	 }
	 else if (Plant_connect_var.Error_code2 == 0)
	 {
		  plant_connect_struct_write(0, PLANT_CONNECT_NO_CAL_FLAG, 0, error_code, 0, PLANT_CONNECT_ERROR_RCRD_FLAG);
	 }
	 else if (Plant_connect_var.Error_code3 == 0)
	 {
		  plant_connect_struct_write(0, PLANT_CONNECT_NO_CAL_FLAG, 0, 0, error_code, PLANT_CONNECT_ERROR_RCRD_FLAG);
	 }
	 return;
}
/*****************************************************************************
* @note       Function name: void load_cell_size_unit_plant_connect(void)
* @returns    returns      : '0' if file update was successful
*                          : '1' if file open failed
* @param      arg1         : file to be read back
* @author                  : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description  : Reads and loads the current configuration from the file
* @note       Notes        : None
*****************************************************************************/
void load_cell_size_unit_plant_connect(void)
{
//Commented by DK on 12 sep 2014 for PCM 
	/*
	if (Scale_setup_var.Load_cell_size == Screen23_str2) //45kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_45KG;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str5) //50kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_50KG;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str8) //100kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_100KG;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str11) //150kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_150KG;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str14) //200kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_200KG;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str17) //350kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_350KG;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str20) //500kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_500KG;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str23) //1000kg
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_1000KG_LBS;
			 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str26) //1000lbs
    {
       Plant_connect_var.Load_cell_capacity = LOADCELL_CAPACITY_1000KG_LBS;
			 Plant_connect_var.Load_cell_unit = 2; //load cell unit in pounds
    }
    else if (Scale_setup_var.Load_cell_size == Screen23_str29) //custom loadcell */
    {
      /*
			if(Scale_setup_var.Custom_LC_unit == Screen23121_str5) //custom loadcell capacity in kg
      {
         Plant_connect_var.Load_cell_capacity = Scale_setup_var.Custom_LC_capacity;
				 Plant_connect_var.Load_cell_unit = 1; //load cell unit in kg
      }
      else*/
      {
//         Plant_connect_var.Load_cell_capacity = Scale_setup_var.Custom_LC_capacity;
				 Plant_connect_var.Load_cell_unit = 2; //load cell unit in pounds
      }
    }
		return;
}

/*****************************************************************************
* @note       Function name: void weight_unit_plant_connect(void)
* @returns    returns      : '0' if file update was successful
*                          : '1' if file open failed
* @param      arg1         : file to be read back
* @author                  : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description  : Reads and loads the current configuration from the file
* @note       Notes        : None
*****************************************************************************/
void weight_unit_plant_connect(void)
{
//Commented by DK on 12 sep 2014 for PCM 
	/*
	  if (Scale_setup_var.Weight_unit == TONS) //short ton
    {
       Plant_connect_var.Weight_unit = PLANT_CONNECT_SHORT_TON_FLAG;
    }
    else if (Scale_setup_var.Weight_unit == LONG_TON) //long ton
    {
       Plant_connect_var.Weight_unit = PLANT_CONNECT_LONG_TON_FLAG;
    }
    else if (Scale_setup_var.Weight_unit == LBS) //lbs
    {
       Plant_connect_var.Weight_unit = PLANT_CONNECT_LBS_FLAG;
    }
    else if (Scale_setup_var.Weight_unit == TONNE) //metric ton or tonne
    {
       Plant_connect_var.Weight_unit = PLANT_CONNECT_TONNE_FLAG;
    }
    else if (Scale_setup_var.Weight_unit == KG) //kg */
    {
       Plant_connect_var.Weight_unit = PLANT_CONNECT_KG_FLAG;
    }
		return;
}   

/*****************************************************************************
* @note       Function name: void rate_unit_plant_connect(void)
* @returns    returns      : '0' if file update was successful
*                          : '1' if file open failed
* @param      arg1         : file to be read back
* @author                  : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description  : Reads and loads the current configuration from the file
* @note       Notes        : None
*****************************************************************************/
void rate_unit_plant_connect(void)
{
//Commented by DK on 12 sep 2014 for PCM 
/*	
  if (Scale_setup_var.Rate_time_unit == Screen26_str2) //hours
    {
			 Plant_connect_var.Rate_unit = PLANT_CONNECT_HOURS_FLAG;
		}
		else*/
		{
			 Plant_connect_var.Rate_unit = PLANT_CONNECT_MINUTES_FLAG;
		}
		return;
}

/*****************************************************************************
* @note       Function name: void speed_distance_plant_connect_unit(void)
* @returns    returns      : '0' if file update was successful
*                          : '1' if file open failed
* @param      arg1         : file to be read back
* @author                  : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description  : Reads and loads the current configuration from the file
* @note       Notes        : None
*****************************************************************************/
void speed_distance_plant_connect_unit(void)
{
	//Commented by DK on 12 sep 2014 for PCM 
/*  
	if(Scale_setup_var.Distance_unit == Screen24_str5)//If Metric = distance_unit
    {
			 Plant_connect_var.Speed_unit = Plant_connect_var.Distance_unit = PLANT_CONNECT_METERS_FLAG; //meters
		}
		else*/
		{
			 Plant_connect_var.Speed_unit = Plant_connect_var.Distance_unit = PLANT_CONNECT_FEET_FLAG; //feet
		}
		return;
}

/*****************************************************************************
* @note       Function name: U8 plant_connect_struct_write(float wt_dur_cal, float wt_bfr_clr, U16 cal_type,
*                              U16 err_code1, U16 err_code2, U16 err_code3, U16 rcrd_type)
* @returns    returns      : '0' if file update was successful
*                          : '1' if file open failed
* @param      arg1         : file to be read back
* @author                  : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description  : Reads and loads the current configuration from the file
* @note       Notes        : None
*****************************************************************************/
U8 plant_connect_struct_write(float wt_bfr_clr, U16 cal_type,
                              U16 err_code1, U16 err_code2, U16 err_code3, U16 rcrd_type)
{
    FILE *fp;
		U8 err = 0;
	  Plant_connect_var.EOR = '\n';
    //Plant_connect_var.Accumulated_weight_during_cal = Calculation_struct.Weight_during_cal;  //Commented by DK on 12 sep 2014 for PCM 
	
    Plant_connect_var.Accumulated_weight_before_clr = wt_bfr_clr;
    Plant_connect_var.Clear_Date.Year = Current_time.RTC_Year;
	  Plant_connect_var.Clear_Date.Mon = Current_time.RTC_Mon;
	  Plant_connect_var.Clear_Date.Day = Current_time.RTC_Mday;
	  Plant_connect_var.Clear_Time.Hr = Current_time.RTC_Hour;
	  Plant_connect_var.Clear_Time.Min = Current_time.RTC_Min;
	  Plant_connect_var.Clear_Time.Sec = Current_time.RTC_Sec;	
	//  Plant_connect_var.Clear_Speed = Calculation_struct.Belt_speed;  //Commented by DK on 12 sep 2014 for PCM 
    Plant_connect_var.Calibration_type = cal_type;
	  if(err_code1 != 0)
		{
			 Plant_connect_var.Error_code1 = err_code1;
		}
    if(err_code2 != 0)
		{
			 Plant_connect_var.Error_code2 = err_code2;
		}
    if(err_code3 != 0)
		{
			 Plant_connect_var.Error_code3 = err_code3;
		}
    //Plant_connect_var.Old_record_request = 0;
	  Plant_connect_var.Record_type = rcrd_type;

    Plant_connect_var.Unique_id++;
    Plant_connect_var.Date.Year = Current_time.RTC_Year;
	  Plant_connect_var.Date.Mon = Current_time.RTC_Mon;
	  Plant_connect_var.Date.Day = Current_time.RTC_Mday;
	  Plant_connect_var.Time.Hr = Current_time.RTC_Hour;
	  Plant_connect_var.Time.Min = Current_time.RTC_Min;
	  Plant_connect_var.Time.Sec = Current_time.RTC_Sec;
		//Commented by DK on 12 sep 2014 for PCM 
		/*
    Plant_connect_var.Accumulated_weight = Calculation_struct.Total_weight;
    Plant_connect_var.Load_percentage = Calculation_struct.Load_cell_perc;
    Plant_connect_var.Angle = Plant_connect_var.Conveyor_angle = Calculation_struct.Angle;
    Plant_connect_var.Speed = Calculation_struct.Belt_speed;
    Plant_connect_var.Old_zero_value = Calibration_var.Old_zero_value;
    Plant_connect_var.New_zero_value = Calibration_var.New_zero_value;
    Plant_connect_var.Nos_of_pulses = Sens_brd_param.No_of_pulses;
    Plant_connect_var.Time_duration = Sens_brd_param.Time;
		Plant_connect_var.Idler_distanceA = Scale_setup_var.Idler_distanceA;
    Plant_connect_var.Idler_distanceB = Scale_setup_var.Idler_distanceB;
    Plant_connect_var.Idler_distanceC = Scale_setup_var.Idler_distanceC;
    Plant_connect_var.Idler_distanceD = Scale_setup_var.Idler_distanceD;
    Plant_connect_var.Idler_distanceE = Scale_setup_var.Idler_distanceE;
    Plant_connect_var.Test_weight_value = Calibration_var.Test_weight;
    Plant_connect_var.Certified_weight = Calibration_var.Cert_scale_weight;
    Plant_connect_var.Prev_cal_factor = Calibration_var.old_span_value;
    Plant_connect_var.New_cal_factor = Calibration_var.new_span_value;
		Plant_connect_var.Tons_per_hour = Calculation_struct.Rate;
*/
	  //----------------------------weight unit-------------------------------------
    weight_unit_plant_connect();

		//----------------------------rate unit-------------------------------------------
    rate_unit_plant_connect();

		//----------------------speed and distance units-----------------------------------------
		speed_distance_plant_connect_unit();

		//----------------------------------load cell size and unit----------------------------------------
	  load_cell_size_unit_plant_connect();

		if (Flags_struct.Connection_flags & USB_CON_FLAG)
		{
				fp = fopen ("Plant_connect.txt", "a");
				if(fp == NULL)
				{
//					 printf("\n error in opening plant connect file");
					 err = 1;
				}
				else
				{
					 fwrite((U16 *)&Plant_connect_var, 1, sizeof(Plant_connect_var), fp);
					 if(ferror(fp))
					 {
//						  printf("\n Plant connect file read error");
						  err = 1;
					 }
					 if(0 != fclose(fp))
					 {
//						  printf ("\nPlant connect file could not be closed!\n");
						  err = 1;
					 }
				}
		}
	  return(err);
}

/*****************************************************************************
* @note       Function name: U8 plant_connect_read (void)
* @returns    returns      : '0' if file update was successful
*                          : '1' if file open failed
* @param      arg1         : file to be read back
* @author                  : Anagha Basole
* @date       date created : 13/2/13
* @brief      Description  : Reads and loads the current configuration from the file
* @note       Notes        : None
*****************************************************************************/
U8 plant_connect_struct_read (void)
{
   FILE *fp;
   U8 err = 0;

	 if (Flags_struct.Connection_flags & USB_CON_FLAG)
	 {
			 fp = fopen ("Plant_connect.txt", "r"); // "rb" testing
			 if(fp == NULL)
			 {
//					printf("\n error in opening file");
					err = 1;
			 }
			 else
			 {
				  Plant_connect_var_read.Unique_id = 0;
				  while (!feof(fp))
					{
						  //read till the record is found
							if (Plant_connect_var_read.Unique_id != Plant_connect_var.Old_record_request)
							{
								 /*read the plant connect structure from the file*/
								 fread((U16 *)&Plant_connect_var_read, 1, sizeof(Plant_connect_var_read), fp);

								 if(ferror(fp))
								 {
//										printf("\n Plant connect file read error");
									  err = 1;
										break;
								 }
							}
							if (Plant_connect_var_read.Unique_id == Plant_connect_var.Old_record_request)
							{
								 //the requested record has been found
								 memcpy(&Plant_connect_var, &Plant_connect_var_read, sizeof(Plant_connect_var));
								 break;
							}
					}
					if(0 != fclose(fp))
					{
//						 printf ("\nPlant connect file could not be closed!\n");
					}
			 }
	 }
   Flags_struct.Plant_connect_record_flag &= ~REGISTER_ADDR_ENTERED_FLAG;
   fflush (stdout);
   return (err);
}

#endif /*#ifdef MODBUS_TCP*/
/*****************************************************************************
* End of file
*****************************************************************************/
