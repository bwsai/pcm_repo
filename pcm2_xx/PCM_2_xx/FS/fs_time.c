/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : fs_time.c
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 16, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "file_config.h"
#include "rtc.h"
#include "File_update.h"

//Commented by DK on 12 sep 2014 for PCM 
//#include "Screen_global_ex.h"
//#include "Screen_data_enum.h"

#ifdef FILE_SYS
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: U32 fs_get_date (void)
* @returns    returns		   : The current date is returned
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 29th July 2012
* @brief      Description	 : The current RTC data is read and date is returned
* @note       Notes		     : None
*****************************************************************************/
U32 fs_get_date (void) 
{
  U32 d,m,y,date;
	
  d = Current_time.RTC_Mday;         /* Day:   1 - 31      */
  m = Current_time.RTC_Mon;          /* Month: 1 - 12      */
  y = Current_time.RTC_Year;         /* Year:  1980 - 2107 */

  date = (y << 16) | (m << 8) | d;
  return (date);
}

/*****************************************************************************
* @note       Function name: U32 fs_get_time (void)
* @returns    returns		   : The current time is returned
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 29th July 2012
* @brief      Description	 : The current RTC data is read and time is returned
* @note       Notes		     : None
*****************************************************************************/
U32 fs_get_time (void) 
{
  U32 h,m,s,time;
//Commented by DK on 12 sep 2014 for PCM 
	//	if(Admin_var.Current_Time_Format == TWENTYFOUR_HR)
	{				
			h = (U8) Current_time.RTC_Hour;
	}
	//Commented by DK on 12 sep 2014 for PCM 
	/*
	else if(Admin_var.Current_Time_Format == TWELVE_HR)
	{
		if(Admin_var.AM_PM == AM_TIME_FORMAT)
		{
			if(Current_time.RTC_Hour == 12)
				h = 0;
			else
				h = (U8) Current_time.RTC_Hour;	
		}
		else
		{
			h = (U8) Current_time.RTC_Hour+12;				
		}
	}
*/	
  m = Current_time.RTC_Min;  /* Minutes: 0 - 59 */
  s = Current_time.RTC_Sec;  /* Seconds: 0 - 59 */

  time = (h << 16) | (m << 8) | s;
  return (time);
}
#endif //#ifdef FILE_SYS

/*****************************************************************************
* End of file
*****************************************************************************/
