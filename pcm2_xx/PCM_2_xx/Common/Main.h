/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : Main.h
* @brief			         : Controller Board
*
* @author			         : Dnyaneshwar Kashid
*
* @date Created		     : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified	 : December Thursday, 2014 <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __MAIN_H
#define __MAIN_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "rtc.h"
#include <Net_Config.h>
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */
extern BOOL OS_Init_Done_Flag ;
/* Boolean variables section */
extern BOOL Lst_Sign, dst_on_off,ppp_enable;
extern BOOL dhcp_enable;
extern BOOL Power_Down_Set_Flag ;

/* Character variables section */
extern char Http_Host_Name[];
extern char Http_Port_Number[];
extern unsigned char SNTP_Server_IpAdr[]; 
extern char sntp_host_name[];
//Added by DK on 29 January 2015 
extern __align(4) char ppp_dial_number[20] __attribute__ ((section ("PPP_BUF_DATA")));
extern __align(4) char ppp_user_name[100] __attribute__ ((section ("PPP_BUF_DATA")));
extern __align(4) char ppp_password[100] __attribute__ ((section ("PPP_BUF_DATA")));
extern __align(4) char ppp_apn[80] __attribute__ ((section ("PPP_BUF_DATA")));

extern U8 cell_modem_strength;
/* unsigned integer variables section */
extern U16 http_port_number;
/* Signed integer variables section */

/* unsigned long integer variables section */
extern U32 get_sntp_time;
extern U32 LstHour, LstMinutes;
/* signed long integer variables section */

/* unsigned long long integer variables section */
#ifdef ETHERNET
  extern U32  dhcp_tout;
#endif
/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
#ifdef ETHERNET
 extern LOCALM localm[];
 extern LOCALM ip_config; 
#endif
/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif /*__MAIN_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
