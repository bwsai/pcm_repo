/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : soft_stack.c
* @brief               : Controller Board
*
* @author              : R. Venkata Krishna Rao
*
* @date Created        : 20th  November wed, 2013  <Nov 20, 2013>
* @date Last Modified  : 20th  November wed, 2013  <Nov 20, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "soft_stack.h"
#include "../GUI/Application/Screen_global_ex.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifdef _SOFT_STACK_H_


/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
Soft_STACK wiz_nav_stack;
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: Push_STACK
* @returns    returns      : 0 if Push operation is a success
                             -1 if Push operation is a failure
* @param      arg1         : a pointer to Soft_STACK Structure
* @param      arg2         : a float value which is to be pushed in STACK                             
* @author                  : R. Venkata Krishna Rao
* @date       date created : Nov 20, 2013
* @brief      Description  : It pushes one value of type int into STACK
* @note       Notes        : None
*****************************************************************************/

int Push_STACK(Soft_STACK *t_STACK,unsigned int element )
{  
    if(!(Is_STACK_Full(t_STACK))) // If STACK is not FULL
    {
        t_STACK->contents[t_STACK->top] = element;
		t_STACK->top++;
		return 0;
    }
    else 
	  return -1;
}
/*****************************************************************************
* @note       Function name: Pop_STACK
* @returns    returns      : Previous screen org no if Pop operation is a success
                             Wizard Cancel screen if Pop operation is a failure
* @param      arg1         : a pointer to Soft_STACK Structure                          
* @author                  : R. Venkata Krishna Rao
* @date       date created : Nov 20, 2013
* @brief      Description  : It Pop's one value of type int out of STACK
* @note       Notes        : None
*****************************************************************************/
unsigned int Pop_STACK(Soft_STACK *t_STACK )
{
	unsigned int temp;
	if(!(Is_STACK_Empty(t_STACK))) // // If STACK is not Empty
	{
	   temp = t_STACK->contents[t_STACK->top-1];
	   t_STACK->contents[t_STACK->top-1] = 0;
	   t_STACK->top--;
	   return temp;
	}
	else 
	{
		 temp = atoi(GUI_data_nav.wizard_screen->Cancel);
	   return temp;
	}
	
}

#endif


