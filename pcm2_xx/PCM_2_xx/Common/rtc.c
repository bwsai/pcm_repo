/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : rtc.c
* @brief			         : Controller Board
*
* @author			         : Dnyaneshwar Kashid
*
* @date Created		     : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified	 : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "rtc.h"
#include "Serial_LPC178x.h"
#include "sntp.h"
#include "main.h"
#include "Global_ex.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/
const char *DayOfWeek[]={"Sunday", "Monday","Tuesday", "Wednseday","Thursday","Friday","Saturday"};
const char *DayOfMonth[]={"January", "February","March", "April","May","June","July", "August", "September","October","November","December"};


#ifdef RTC
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
	volatile uint32_t alarm_on = 0;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

RTCTime Current_time;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void RTC_IRQHandler (void);
void RTCInit( void );
void RTCStart( void );
void RTCStop( void );
void RTC_CTCReset( void );
void RTCSetTime( RTCTime );
RTCTime RTCGetTime( void );
void RTCSetAlarm( RTCTime );
void RTCSetAlarmMask( U32 AlarmMask );

void Set_rtc_date(DATE_STRUCT date);
void Set_rtc_time(TIME_STRUCT time);
void Set_rtc_dow(unsigned int dow);
U16 Get_Dow_String_Enum(U16 dow);
void get_utc(void);

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name: void RTC_IRQHandler (void) 
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : RTC interrupt handler, it executes based on the
*								 						 the alarm setting
* @note       Notes		     : None
*****************************************************************************/
void RTC_IRQHandler (void) 
{  
  LPC_RTC->ILR |= ILR_RTCCIF;		/* clear interrupt flag */
 // alarm_on = 1;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCInit( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Initialize RTC timer
* @note       Notes		     : None
*****************************************************************************/
void RTCInit( void )
{
  alarm_on = 0;

  /* Enable CLOCK into RTC */
  LPC_SC->PCONP |= (1 << 9);

  /* If RTC is stopped, clear STOP bit. */
  if ( LPC_RTC->RTC_AUX & (0x1<<4) )
  {
	LPC_RTC->RTC_AUX |= (0x1<<4);	
  }
  
  /*--- Initialize registers ---*/    
  LPC_RTC->AMR = 0;
  LPC_RTC->CIIR = 0;
  LPC_RTC->CCR = 0;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCStart( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Start RTC timer
* @note       Notes		     : None
*****************************************************************************/
void RTCStart( void ) 
{
  /*--- Start RTC counters ---*/
  LPC_RTC->CCR |= CCR_CLKEN;
  LPC_RTC->ILR = ILR_RTCCIF;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCStop( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Stop RTC timer
* @note       Notes		     : None
*****************************************************************************/
void RTCStop( void )
{   
  /*--- Stop RTC counters ---*/
  LPC_RTC->CCR &= ~CCR_CLKEN;
  return;
} 

/*****************************************************************************
* @note       Function name: void RTC_CTCReset( void )
* @returns    returns		   : None
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Reset RTC clock tick counter
* @note       Notes		     : None
*****************************************************************************/
void RTC_CTCReset( void )
{   
  /*--- Reset CTC ---*/
  LPC_RTC->CCR |= CCR_CTCRST;
  return;
}

/*****************************************************************************
* @note       Function name: void RTCSetTime( RTCTime Time ) 
* @returns    returns		   : None
* @param      arg1			   : Time - time to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC timer value
* @note       Notes		     : None
*****************************************************************************/
void RTCSetTime( RTCTime Time ) 
{
  LPC_RTC->SEC = Time.RTC_Sec;
  LPC_RTC->MIN = Time.RTC_Min;
  LPC_RTC->HOUR = Time.RTC_Hour;
  LPC_RTC->DOM = Time.RTC_Mday;
  LPC_RTC->DOW = Time.RTC_Wday;
  LPC_RTC->DOY = Time.RTC_Yday;
  LPC_RTC->MONTH = Time.RTC_Mon;
  LPC_RTC->YEAR = Time.RTC_Year;    
  return;
}

/*****************************************************************************
* @note       Function name: void Set_rtc_date(DATE_STRUCT date) 
* @returns    returns		   : None
* @param      arg1			   : date to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC date
* @note       Notes		     : None
*****************************************************************************/
void Set_rtc_date(DATE_STRUCT date) 
{
  LPC_RTC->DOM = date.Day;
  LPC_RTC->MONTH = date.Mon;
  LPC_RTC->YEAR = date.Year;    
  return;
}

/*****************************************************************************
* @note       Function name: void Set_rtc_time(TIME_STRUCT time)
* @returns    returns		   : None
* @param      arg1			   : time to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC hour min sec value
* @note       Notes		     : Venkata Modified this function on 9thNov2013, 
                             included AM and PM related logic
*****************************************************************************/
void Set_rtc_time(TIME_STRUCT time) 
{
	//Commented by DK on 12 sep 2014 for PCM 
	/*
	if(Admin_var.Current_Time_Format == TWELVE_HR)
  {
	   if(Admin_var.AM_PM == AM_TIME_FORMAT)
		 { 
			  if(time.Hr == 12)
			      LPC_RTC->HOUR = 0;
			  else
				    LPC_RTC->HOUR = (U32)time.Hr;
		 }
	   else
		 {
			  if(time.Hr == 12)
			      LPC_RTC->HOUR = 12 ;
				else
				    LPC_RTC->HOUR = (U32)time.Hr + 12;
		 }
	}	
	else*/ 
	{	
    LPC_RTC->HOUR = (U32)time.Hr;
	}
	LPC_RTC->SEC = (U32)time.Sec;
  LPC_RTC->MIN = (U32)time.Min;
  return;
}

/*****************************************************************************
* @note       Function name: void Set_rtc_dow(unsigned int dow) 
* @returns    returns		   : None
* @param      arg1			   : Day of the week
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Set the rtc day of the week
* @note       Notes		     : None
*****************************************************************************/
//Commented by DK on 12 sep 2014 for PCM 
/*
void Set_rtc_dow(unsigned int dow) 
{
	if (dow == SUNDAY) //Sunday
	{
		 LPC_RTC->DOW = 0;
	}
	else if (dow == MONDAY) //Monday
	{
		 LPC_RTC->DOW = 1;
	}
	else if (dow == TUESDAY) //Tuesday
	{
		 LPC_RTC->DOW = 2;
	}
	else if ( dow == WEDNESDAY) //Wednesday
	{
		 LPC_RTC->DOW = 3;
	}
	else if ( dow == THURSDAY ) //Thursday
	{
		 LPC_RTC->DOW = 4;
	}
	else if ( dow == FRIDAY) //Friday
	{
		 LPC_RTC->DOW = 5;
	}
	else if ( dow == SATURDAY) //Saturday
	{
		 LPC_RTC->DOW = 6;
	}
  return;
}
*/
/*****************************************************************************
* @note       Function name: U16 Get_Dow_String_Enum(U16 dow) 
* @returns    returns		   : enum corresponding to day of week string(sunday,saturday,etc) or 0 on invalid argument.
* @param      arg1			   : Day of the week
* @author			             : R Venkata Krishna Rao
* @date       date created : 
* @brief      Description	 : gets the enum of day of week string
* @note       Notes		     : None
*****************************************************************************/
//Commented by DK on 12 sep 2014 for PCM 
/*
U16 Get_Dow_String_Enum(U16 dow) 
{
   switch(dow)
	 {
		  case 0:  return SUNDAY;
			case 1:  return MONDAY;
			case 2:  return TUESDAY;
			case 3:  return WEDNESDAY;
			case 4:  return THURSDAY;
			case 5:  return FRIDAY;
			case 6:  return SATURDAY;
			default: return 0;
	 }
}
*/
/*****************************************************************************
* @note       Function name: void RTCSetTime( RTCTime Time ) 
* @returns    returns		   : None
* @param      arg1			   : Time - time to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Setup RTC timer value
* @note       Notes		     : None
*****************************************************************************/
/*
void RTCSetTime( RTCTime Time ) 
{
  LPC_RTC->SEC = Time.RTC_Sec;
  LPC_RTC->MIN = Time.RTC_Min;
  LPC_RTC->HOUR = Time.RTC_Hour;
  LPC_RTC->DOM = Time.RTC_Mday;
  LPC_RTC->DOW = Time.RTC_Wday;
  LPC_RTC->DOY = Time.RTC_Yday;
  LPC_RTC->MONTH = Time.RTC_Mon;
  LPC_RTC->YEAR = Time.RTC_Year;    
  return;
}
*/
/*****************************************************************************
* @note       Function name: void RTCSetAlarm( RTCTime Alarm )
* @returns    returns		   : None
* @param      arg1			   : Alarm - Time at which alarm is to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Inittialize the alarm time
														 SysTick interrupt happens every 10 ms
* @note       Notes		     : None
*****************************************************************************/
/*void RTCSetAlarm( RTCTime Alarm ) 
{   
  LPC_RTC->ALSEC = Alarm.RTC_Sec;
  LPC_RTC->ALMIN = Alarm.RTC_Min;
  LPC_RTC->ALHOUR = Alarm.RTC_Hour;
  LPC_RTC->ALDOM = Alarm.RTC_Mday;
  LPC_RTC->ALDOW = Alarm.RTC_Wday;
  LPC_RTC->ALDOY = Alarm.RTC_Yday;
  LPC_RTC->ALMON = Alarm.RTC_Mon;
  LPC_RTC->ALYEAR = Alarm.RTC_Year;    
  return;
}*/

/*****************************************************************************
* @note       Function name: RTCTime RTCGetTime( void )
* @returns    returns		   : The current time is returned
* @param      arg1			   : None
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Get RTC timer value
* @note       Notes		     : Venkata Modified this function on 9thNov2013, 
                             included AM and PM related logic
*****************************************************************************/
RTCTime RTCGetTime( void ) 
{
  RTCTime LocalTime;
  //Commented by DK on 12 sep 2014 for PCM 
	/*
	if( Admin_var.Current_Time_Format == TWELVE_HR)
    {
        if(LPC_RTC->HOUR <12)  // 0hrs to 11hrs i.e AM time period
				{
				   Admin_var.AM_PM = AM_TIME_FORMAT; // AM
           if(LPC_RTC->HOUR == 0) 
  					  LocalTime.RTC_Hour = 12; 
					 else
						  LocalTime.RTC_Hour = LPC_RTC->HOUR;
				}
				else   // 12hrs to 23hrs i.e PM time period
        {
				   Admin_var.AM_PM = PM_TIME_FORMAT; // PM
					  if(LPC_RTC->HOUR == 12)
						  LocalTime.RTC_Hour = 12; 
					  else
              LocalTime.RTC_Hour = LPC_RTC->HOUR - 12;
        }
	}
  else*/
  {
		LocalTime.RTC_Hour = LPC_RTC->HOUR;
	}

  LocalTime.RTC_Sec = LPC_RTC->SEC;
  LocalTime.RTC_Min = LPC_RTC->MIN;
  LocalTime.RTC_Mday = LPC_RTC->DOM;
  LocalTime.RTC_Wday = LPC_RTC->DOW;
  LocalTime.RTC_Yday = LPC_RTC->DOY;
  LocalTime.RTC_Mon = LPC_RTC->MONTH;
  LocalTime.RTC_Year = LPC_RTC->YEAR;
  return ( LocalTime );    
}

/*****************************************************************************
* @note       Function name: void RTCSetAlarmMask( uint32_t AlarmMask )
* @returns    returns		   : None
* @param      arg1			   : Alarm mask needs to be set
* @author			             : Anagha Basole
* @date       date created : 
* @brief      Description	 : Set RTC timer alarm mask
* @note       Notes		     : None
*****************************************************************************/
/*void RTCSetAlarmMask( uint32_t AlarmMask ) 
{
  //--- Set alarm mask ---
  LPC_RTC->AMR = AlarmMask;
  return;
}
*/


void update_rtc(void)
{
	char temp_buff[60];
  char *pChar;
	int i;
	
//	static char U8Second_Sunday = 0;
//	static char U8DST_applied = 0;
	//update local RTC by SNTP time
		while(!time_sync_fg);
		msec_cntr = 0;
		time_sync_fg = 0;
	
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	pChar = &utc_time[0];
	for(i=0;i<7;i++)
	{
		if(!strncmp(pChar,(const char*)DayOfWeek[i],3))
		{
			Current_time.RTC_Wday = i;
			break;
		}
	}
	pChar+=4; 
	for(i=0;i<12;i++)
	{
		if(!strncmp(pChar,(const char*)DayOfMonth[i],3))
		{
			Current_time.RTC_Mon = i + 1;
			break;
		}
	}	
	pChar+=4; 
	strncpy(temp_buff,pChar,2);
	Current_time.RTC_Mday= atoi(temp_buff);
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,2);
	Current_time.RTC_Hour= atoi(temp_buff);
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,2);
	Current_time.RTC_Min= atoi(temp_buff);
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,2);
	Current_time.RTC_Sec= atoi(temp_buff);
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,4);
	Current_time.RTC_Year= atoi(temp_buff);
		if(Current_time.RTC_Sec < 59)
		{
			Current_time.RTC_Sec = Current_time.RTC_Sec +1;
		}
		else 
		{
			Current_time.RTC_Sec = 0;
			if(Current_time.RTC_Min < 59)
			{
				Current_time.RTC_Min =  Current_time.RTC_Min+1;

			}
			else
			{
				Current_time.RTC_Min =  0;
				if(Current_time.RTC_Hour < 23)
				{
					Current_time.RTC_Hour =  Current_time.RTC_Hour+1;

				}
				else
				{
						Current_time.RTC_Hour = 0;
				}				

			}
		}
	RTCSetTime(Current_time);
}

/*
void display_rtc(void)
{
	char temp_buff[60];
//  char *pChar;
	int i;
	BOOL status; 
	
	if(Current_time.RTC_Wday > 6)
	{
		Current_time.RTC_Wday =0; 
	}	
	if((Current_time.RTC_Mon == 0) || (Current_time.RTC_Mon > 12))
	{
		Current_time.RTC_Mon =1; 
	}
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	sprintf(temp_buff,"Date: %s,%s, %d, %d ",DayOfWeek[Current_time.RTC_Wday],DayOfMonth[Current_time.RTC_Mon-1],Current_time.RTC_Mday,Current_time.RTC_Year); 
	strcat(temp_buff,"\r\n");
	for(i=0; i < (sizeof(temp_buff) -1);i++)
	{
		status = com_putchar(temp_buff[i]);
		if(status == 0)
		break;	
		if(temp_buff[i] == '\0')
		break;	
	}
	
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	sprintf(temp_buff,"Time: %d : %d : %d ", Current_time.RTC_Hour,Current_time.RTC_Min,Current_time.RTC_Sec); 
	strcat(temp_buff,"\r\n");
	for(i=0; i < (sizeof(temp_buff) -1);i++)
	{
		status = com_putchar(temp_buff[i]);
		if(status == 0)
		break;	
		if(temp_buff[i] == '\0')
		break;	
	}

}
*/
void get_utc(void)
{
	char temp_buff[60];
  char *pChar;
	int i;
//	static char U8Second_Sunday = 0;
//	static char U8DST_applied = 0;
	//update local RTC by SNTP time
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	pChar = &utc_time_disp[0];
	for(i=0;i<7;i++)
	{
		if(!strncmp(pChar,(const char*)DayOfWeek[i],3))
		{
//			Current_time.RTC_Wday = i;
			break;
		}
	}
	pChar+=4; 
 	for(i=0;i<12;i++)
 	{
 		if(!strncmp(pChar,(const char*)DayOfMonth[i],3))
 		{
 			disp_utc_time_mon = i+1 ;
 			break;
 		}
 	}	
	
	pChar+=4; 
	strncpy(temp_buff,pChar,2);
//	Current_time.RTC_Mday= atoi(temp_buff);
		disp_utc_time_day= atoi(temp_buff);
	
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,2);
	disp_utc_time_hr= atoi(temp_buff);
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,2);
	disp_utc_time_min= atoi(temp_buff);
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,2);
	disp_utc_time_sec= atoi(temp_buff);
	pChar+=3;
	memset(temp_buff, '\0',sizeof(temp_buff)); 
	strncpy(temp_buff,pChar,4);
	disp_utc_time_year= atoi(temp_buff);

//	RTCSetTime(Current_time);
}

/*****************************************************************************
**                            End Of File
******************************************************************************/
#endif
