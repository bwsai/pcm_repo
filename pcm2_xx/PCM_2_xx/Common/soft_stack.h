/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : soft_stack.h
* @brief               : Controller Board
*
* @author              : R. Venkata Krishna Rao
*
* @date Created        : 20th  November wed, 2013  <Nov 20, 2013>
* @date Last Modified  : 20th  November wed, 2013  <Nov 20, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef _SOFT_STACK_H_
#define _SOFT_STACK_H_
/*============================================================================
* Include Header Files
*===========================================================================*/


/*============================================================================
* Public Macro Definitions
*===========================================================================*/
#define Init_STACK(t_STACK) memset(&t_STACK,0,sizeof(t_STACK));
#define Is_STACK_Empty(t_STACK) (((t_STACK)->top==0)?1:0)
#define Is_STACK_Full(t_STACK)  (((t_STACK)->top==SOFT_STACK_SIZE)?1:0)
/* Defines Section */
#define SOFT_STACK_SIZE 40
/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
typedef struct
{
	unsigned int contents[SOFT_STACK_SIZE];
	unsigned char top;
}Soft_STACK;
extern Soft_STACK wiz_nav_stack;

/*============================================================================
* Public Function Declarations
*===========================================================================*/

extern int Push_STACK(Soft_STACK*,unsigned int);
extern unsigned int Pop_STACK(Soft_STACK* );

#endif
