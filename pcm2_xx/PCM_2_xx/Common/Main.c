/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename       : Main.c
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : December Thursday, 2014 <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "RTOS_main.h"
#include "Main.h"
#include "USB_main.h"
#include "Emc_config.h"
#include "Ext_flash_low_level.h"
#include "Ext_flash_high_level.h"
#include "Ext_Data_flag.h"
#include "EEPROM_high_level.h"
#include "lpc177x_8x_bod.h"
#include "Http_Client.h"
#include "modbus_tcp_master.h"
#include "I2C_driver.h"
#include "DebugLog.h"
//#include "../GUI/Application/Screen_global_data.h"
#include "../GUI/Application/Screen_global_ex.h"
#include "../gui/LCDConf.h"
#include "../gui/Display_task.h"
#include "../gui/application/Screen_data_enum.h"
#include "math.h"
#include "File_update.h"
#include "../wdt.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define NVIC_VTOR           (*(volatile U32*)(0xE000ED08))

#define EXT_INT					//MSA

#ifdef EXT_INT
#if 1
#define TEST_GPIO_P2_31                     0x80000000
#endif
U16 * read_flash;
union ulong_union buffer_long;
extern int con1;
#endif

#define DEBUG_PORT	//sks
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
BOOL Lst_Sign, dst_on_off,ppp_enable;
/* Character variables section */
//char Http_Host_Name[90];  //commented by DK on 22 Dec 2014 
//#ifdef DEBUG_PORT
__align(4) char Debug_Buf[200] __attribute__ ((section ("PCM_CFG_BUFF")));  //Added by DK on 15 Jan 2015  
//#endif

__align(4) char Http_Host_Name[90] __attribute__ ((section ("PCM_CFG_BUFF")));  //Added by DK on 22 Dec 2014  
char Http_Port_Number[10];
U8 USB_Connection;
unsigned char SNTP_Server_IpAdr[4]; 
//char sntp_host_name[40]={"0.us.pool.ntp.org"};  //http://  //commented by DK on 22 Dec 2014 
__align(4) char sntp_host_name[100] __attribute__ ((section ("PCM_CFG_BUFF")));  //Added by DK on 22 Dec 2014

//Added by DK on 29 January 2015 
__align(4) char ppp_dial_number[20] __attribute__ ((section ("PPP_BUF_DATA")));
__align(4) char ppp_user_name[100] __attribute__ ((section ("PPP_BUF_DATA")));
__align(4) char ppp_password[100] __attribute__ ((section ("PPP_BUF_DATA")));
__align(4) char ppp_apn[80] __attribute__ ((section ("PPP_BUF_DATA")));

U8 cell_modem_strength; //Added by DK on 6 Feb 2015 for connectivity status update

/* unsigned integer variables section */
U16 http_port_number;
/* Signed integer variables section */

/* unsigned long integer variables section */
U32 get_sntp_time;
U32 LstHour, LstMinutes;
/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern SYS_CFG sys_config;
/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
 BOOL OS_Init_Done_Flag ;
BOOL Power_Down_Set_Flag = 0; 
/* Character variables section */
  U8 con, con_ex;
	U8 Task_section = 0;
	U8 Ready_Write = 0;
	U8 Error_Task = 0;		
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */
#ifdef ETHERNET
  U32  dhcp_tout;
	BOOL dhcp_enable;
#endif
/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
LOCALM ip_config;


/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
extern void Restore_values_to_original_pcm_variables(void);
void Create_Errorlog_file_and_Log_events(void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/



/*****************************************************************************
 * @note       Function name: void EINT0_IRQHandler (void)
 * @returns    returns      : None
 * @param      arg1         : None
 * @author                  : Anagha Basole
 * @date       date created : July 25, 2013
 * @brief      Description  : Brownout Interrupt Handler
 * @note       Notes        : None
 *****************************************************************************/
#ifdef EXT_INT
void EINT0_IRQHandler(void) {
	U16 * read_flash_addr;
	union double_union buffer;
	union float_union buffer_float;
	U16 * u16Var, Block_Length, Calculated_CRC = 0;
	int i;
	U32 addr;

	LPC_SC->EXTINT = 0x00000001; /* clear interrupt */
	Power_Down_Set_Flag = 1; //SKS -> resolving issue of totals losing on power cycle
	if (Ready_Write == 1) {
		//Turn off the backlight
		LPC_GPIO2->CLR |= (1 << LCD_BACKLIGHT_BIT); // Set backlight to off
		__disable_irq();
		//NVIC_DisableIRQ(EINT0_IRQn);
		MATRIX_ARB |= 0x00010000;

		OS_Init_Done_Flag = 0; //By DK on 4 May2016 for resolving issue of totals losing on power cycle
		read_flash_addr = (U16 *) EXT_FLASH_LAST_SECTOR_START_ADDR;
		ExtFlash_writeWord((uint32_t) read_flash_addr, BACKUP_PRESENT_FLAG);
	}
	while (1) {
		// if normal condition is restored then reset the controller
		if ((LPC_GPIO0->PIN & EXT_INT_PIN) != 0) {
			//Resets the Controller through Watchdog timer method
			NVIC_SystemReset();
			break;
		}
	}
	__enable_irq();	
}
#endif
void HardFault_Handler(void) {

	char buf[30];
	OS_TID err_task;
	memset(buf, 0, sizeof(buf));
	err_task = isr_tsk_get();
	Error_Task = err_task;
	sprintf(buf, "\r\nHardfault,TSK_ID:%d, TSK_SEC:%d\n", err_task,
			Task_section);

#ifdef DEBUG_PORT
	Uart_i2c_data_send((uint8_t *) buf, strlen(buf));
#endif
	NVIC_SystemReset();
	while (1)
		;
}

/*****************************************************************************
* @note       Function name: int main (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Dnyaneshwar Kashid
* @date       date created : Sep 15, 2014
* @brief      Description  :
* @note       Notes        : None
*****************************************************************************/
// volatile uint32_t wdt_counter;
// void WDT_IRQHandler(void) 
// {
//   LPC_WDT->MOD &= ~WDTOF;		/* clear the time-out terrupt flag */
//   wdt_counter++;
//   return;
// }
int WDT_EN =0;
void peripheral_reset(void){
    //  LPC_GPIO3->DIR |=0x01<<30;
    //  LPC_GPIO3->SET |=0x01<<30;
    LPC_IOCON->P0_23 &=(~0x3<<3) ;
    LPC_IOCON->P0_23 |=0x01<<3 ;
    LPC_IOCON->P3_30 &=(~0x3<<3) ;
    LPC_IOCON->P3_30 |=0x01<<3 ;
    if (((LPC_GPIO0->PIN) &(0x01<<23))==(0x01<<23))
    {
        WDT_EN =1;
       WDTInit();
        //	LPC_RTC->ALDOY &=(~0x01);
    }
    else
    {
        WDT_EN =0;
        return;
    }
    LPC_GPIO3->DIR |=0x01<<30;
    LPC_GPIO3->SET |=0x01<<30;
    DelayMs(100);
    LPC_GPIO3->CLR |=0x01<<30;
    DelayMs(100);
    LPC_GPIO3->SET |=0x01<<30;
//SKS 18/07/2017
    LPC_RTC->CALIBRATION =0;
    LPC_RTC->CCR&=~(0x01<<4);
	
	
}
int main (void)
{
//	U8 mac_id[6];	
	U16 Data_valid;
	U16 Block_Length, Calculated_CRC = 0;
	U32 addr;	
	int tmp_ascii,tmp_ascii_1;
	int i,j;
	char temp_buff[80]; 
  char	*cptr,tm_buf[3];
	U32 address;
  int Ip_Address_Valid;
#ifdef EXT_INT
	U16 read_buffer[30];
	union double_union buffer;
	union ulong_union buffer_long;
	union float_union buffer_float;
	FILE *fp;
	char file_name[FILE_NAME_SIZE];
	U32 addr_flash = 0;
	U16 record_flag = 0;
#else
	U16 * read_flash;

	
//	IoRequest_Para.PulseCount = 0;
	Ready_Write = 0;
//	daily_rprt.Dummy_var1 = 0x0101;
#endif
	Power_Down_Set_Flag = 0;		
	
	
	MATRIX_ARB = 0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
               | (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
               | (2 <<  2)  // PRI_DCODE : D-Code bus priority.
               | (0 <<  4)  // PRI_SYS   : System bus priority.
               | (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
               | (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
               | (3 << 10)  // PRI_LCD   : LCD DMA priority.
               | (0 << 12)  // PRI_USB   : USB DMA priority.
               | (1 << 16)  // ROM_LAT   : ROM latency select.
               ;

/**************************************************************/
// Software Stack Initilization
//  Init_FIFO((Rate_blend_load.blend_FIFO));  // Initilization of Blending FIFO  //Commented by DK on 11 sep 2014 for PCM 
	OS_Init_Done_Flag = 0;
	peripheral_reset();

#ifdef EXT_INT
	LPC_SC->EXTINT = 0x00000001; /*Clear interrupt*/
	PINSEL_ConfigPin(0, 29, 2);
	LPC_GPIOINT->IO0IntEnF = 0x20000000; /* Port0.29 is falling edge. */
	LPC_SC->EXTMODE = 0x00000001; /* INT0 edge trigger */
	LPC_SC->EXTPOLAR = 0; /* INT0 is falling edge by default */
	NVIC_SetPriority(ENET_IRQn, 0x01);
	NVIC_EnableIRQ(EINT0_IRQn);
	LPC_SC->EXTINT = 0x00000001; /* clear interrupt */
#endif
/**************************************************************/
restore_factory_set_fg=0;
for(addr=0; addr < 100000; addr++); 
for(addr=0; addr < 100000; addr++); 
for(addr=0; addr < 100000; addr++); 
for(addr=0; addr < 100000; addr++);
for(addr=0; addr < 100000; addr++); 
for(addr=0; addr < 100000; addr++); 

//#ifdef DEBUG_PORT
	I2C_high_level_init(100000);
	WDTFeed();
//#endif
Strings = Strings_english;
#ifdef KEYPAD
	keypad_init();
#endif


#ifdef USB
  USB_Connection=0; 
// 	con = init_msd ("U0:");
// 	
//   con_ex = con | 0x80;
    WDTFeed();
    for(addr=0; addr < 100000; addr++); //Added by DK on 3 March 2016
    usbh_engine_all(); //Added by DK on 3 March 2016
    WDTFeed();
    for(addr=0; addr < 100000; addr++); //Added by DK on 3 March 2016
		
		/*
		CHECK FOR A PREVIOUS USB FAULT AS WELL AS A WATCHDOG RESET
		*/
		//        BAD USB                                   WATCHDOG RESET
	 if ((((LPC_RTC->ALDOW) &(0x01<<2))==(0x01<<2))&&(((LPC_SC->RSID) & 0x04) == 0x04)){
	 con =0;					//DISABLE ACCESS TO USB
	 corrupt_usb_Flag =1;		//TO BE USED TO SHOW USB ICON AND USB WARNING
	 }
		else{
		corrupt_usb_Flag =0;
		LPC_RTC->ALDOW |=0x01<<2;				//BAD USB
    finit("U0:");
//		LPC_RTC->ALDOW =0;//&=~(0x01<<2);		//GOOD USB
		}
    WDTFeed();
    usbh_engine_all();
	//	 usbh_engine_all();
            con = usbh_msc_status(0, 0);
						USB_err_no = usbh_msc_get_last_error(0,0);	//ERROR DURING LAST USB EVENT
// con = init_msd ("U0:");
 //   if ((LPC_RTC->ALDOY &(0x01<<1))==(0x01<<1))con =0;
  //  else
	if ((con==1)&&(USB_err_no ==0))
    {
        con =1;
        con_ex = con | 0x80;
    }
		else if(USB_err_no !=0){
		con =0;
		corrupt_usb_Flag =1;
			finit("U0:");
		con1 = usbh_msc_status(0, 0);	
		}
  if (con == 1)
  {
		USB_Connection|= USB_CON_FLAG;
		Flags_struct.Connection_flags |= USB_CON_FLAG;
  }
  else
  {
		USB_Connection&= ~USB_CON_FLAG;
		Flags_struct.Connection_flags &= ~USB_CON_FLAG;
  }
	WDTFeed();
#endif /*#ifdef USB*/
#ifdef RTC
  // Initialize RTC module 
  RTCInit();
  RTCStart();
	Current_time = RTCGetTime();
	strcpy((char *) fheader.File_hdr.Data_type, "HDR");	
	fheader.File_hdr.Date.Year = Current_time.RTC_Year;
	fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
	fheader.File_hdr.Date.Day = Current_time.RTC_Mday;	
	WDTFeed();
#endif

get_sntp_time = 1;
	
//Added by DK on 11 sep 2014 for PCM 
#ifdef EEPROM
 	//Initialize On chip EEPROM
	EEPROM_Init();
	WDTFeed();
//	MAC_Programming_Error=eeprom_program_MAC_id();
	Flags_struct.MAC_Programming_Error=eeprom_program_MAC_id();

	
	if(Flags_struct.MAC_Programming_Error <= 1)
	{
		// Copy the MAC ID into String Array for display
		for(i=0,j=0;i<ETH_ADRLEN;i++,j+=2)
	  {
//			sprintf(MAC_String+j,"%0.2x",own_hw_adr[i]);		//by megha on 23/1/2017 for editable mac id
			tmp_ascii = own_hw_adr[i];

			tmp_ascii_1= ((tmp_ascii & 0xf0)>>4) ;
			if((tmp_ascii_1 >= 0x0a)&&(tmp_ascii_1 <= 0x0f))
			{
				tmp_ascii_1 += 0x37;
			}
			else
			{
				tmp_ascii_1 += 0x30;
			}
			Mac_id[i*3] = tmp_ascii_1;
			
			tmp_ascii_1= ((tmp_ascii & 0x0f)) ;
			if((tmp_ascii_1 >= 0x0a)&&(tmp_ascii_1 <= 0x0f))
			{
				tmp_ascii_1 += 0x37;
			}
			else
			{
				tmp_ascii_1 += 0x30;
			}
			
			Mac_id[(i*3)+1] = tmp_ascii_1;
			if(i!=5)
			{			
				Mac_id[(i*3)+2] = ':';
			}
		}
	}
	WDTFeed();
#endif
	//Read FW version
	Read_FW_Ver_From_EEPOM();
	WDTFeed();	
	strcpy(&Pcm_var.Int_firmware_version[0], FirmwareVer);
	strcpy(&Pcm_var.Int_update_version[0], FirmwareVer);
//Added by DK on 15 Jan2015 
	Current_time = RTCGetTime();	
	//#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
  sprintf(Debug_Buf, "PCM firmware Version %s Month:%02d Date:%02d Year:%04d \
                      Hour:%02d  Minute:%02d Second:%02d\r\n",FirmwareVer,Current_time.RTC_Mon,\
                      Current_time.RTC_Mday,Current_time.RTC_Year,Current_time.RTC_Hour,\
                      Current_time.RTC_Min,Current_time.RTC_Sec);
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	//#endif
	Total_Scales = 0;
  Current_TCP_Slave=0;	
	//Added by DK on 13 Nov2014 for PCM configuration via USB pen drive
	Ip_Address_Valid=0;
  //Added by DK on 29 January 2015 	
	memset(ppp_dial_number,'\0',sizeof(ppp_dial_number));
	memset(ppp_user_name,'\0',sizeof(ppp_user_name));
	memset(ppp_password,'\0',sizeof(ppp_password));
	memset(ppp_apn,'\0',sizeof(ppp_apn));
					system_log.Id = LPC_RTC->GPREG2;
					edit_log.Id = LPC_RTC->GPREG3;	
//	ppp_enable =0;	
	//sys_config.NetCfg |= (0<<3); //enabling Use Default Gateway on remote Network i.e. #define PPP_DEFGW      0
	WDTFeed();
		if (__TRUE == ExtFlash_Init()) 
		{
			WDTFeed();
			read_flash = (U16 *) FLASH_BASE_ADDRESS;
			Data_valid = *read_flash;
			if (Data_valid != BACKUP_PRESENT_FLAG)// && Data_valid_1!= BACKUP_PRESENT_FLAG)1
			{	
				WDTFeed();
				Create_Errorlog_file_and_Log_events();
				WDTFeed();
				screen_variables_init(); /*Initialize with default values*/
				system_log.Id = 0;
//by megha on 1/3/2017
				Current_time = RTCGetTime();
//				strcpy((char *) fheader.File_hdr.Data_type, "HDR");
				fheader.File_hdr.Id = 0;
				fheader.File_hdr.Date.Year = Current_time.RTC_Year;
				fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
				fheader.File_hdr.Date.Day = Current_time.RTC_Mday;				
	
//*********************************		
				WDTFeed();
				eeprom_first_time_write();
				Restore_values_to_original_pcm_variables();
// 				addr_flash = flash_para_record_search(EXT_FLASH_PARA_BACKUP_START_ADDR);
// 				flash_para_backup();	
				read_flash = (U16 *) FLASH_BASE_ADDRESS;
				ExtFlash_writeWord_before_task((uint32_t) read_flash,	BACKUP_PRESENT_FLAG);				
				NVIC_SystemReset();
				while (1);				

			}
			else 
			{
				WDTFeed();
				Create_Errorlog_file_and_Log_events();
				WDTFeed();
				EEP_load_good_data();	
				Restore_values_to_original_pcm_variables();
				WDTFeed();
//by megha on 1/3/2017
				Current_time = RTCGetTime();
//				strcpy((char *) fheader.File_hdr.Data_type, "HDR");
				fheader.File_hdr.Id = 0;
				fheader.File_hdr.Date.Year = Current_time.RTC_Year;
				fheader.File_hdr.Date.Mon = Current_time.RTC_Mon;
				fheader.File_hdr.Date.Day = Current_time.RTC_Mday;				

//*********************************				
				
/*
				if (con == 1)
				{
					fp = fopen ("PCM_Config.txt", "r");
					if(fp != NULL)
					{
						dhcp_enable=0;
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(temp_buff[0] == '0')
							dhcp_enable =0;
							else 
							dhcp_enable =1;	
						}	
							
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(inet_aton(temp_buff, &address))
							{
								ip_config.IpAdr[3] = ((address >> 24) & 0x000000FF); 
								ip_config.IpAdr[2]= ((address >> 16) & 0x000000FF);  
								ip_config.IpAdr[1]= ((address >> 8) & 0x000000FF);  
								ip_config.IpAdr[0]= (address  & 0x000000FF);
								
								Ip_Address_Valid=1;
							}
							else Ip_Address_Valid=0;	
						}
						
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(inet_aton(temp_buff, &address))
							{
								ip_config.NetMask[3] = ((address >> 24) & 0x000000FF); 
								ip_config.NetMask[2]= ((address >> 16) & 0x000000FF);  
								ip_config.NetMask[1]= ((address >> 8) & 0x000000FF);  
								ip_config.NetMask[0]= (address  & 0x000000FF);
								Ip_Address_Valid=1;
							}else Ip_Address_Valid=0;	
						}
						
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(inet_aton(temp_buff, &address))
							{
								ip_config.DefGW[3] = ((address >> 24) & 0x000000FF); 
								ip_config.DefGW[2]= ((address >> 16) & 0x000000FF);  
								ip_config.DefGW[1]= ((address >> 8) & 0x000000FF);  
								ip_config.DefGW[0]= (address  & 0x000000FF);
								Ip_Address_Valid=1;	
							}else Ip_Address_Valid=0;		
						}
						
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(inet_aton(temp_buff, &address))
							{
								ip_config.PriDNS[3] = ((address >> 24) & 0x000000FF); 
								ip_config.PriDNS[2]= ((address >> 16) & 0x000000FF);  
								ip_config.PriDNS[1]= ((address >> 8) & 0x000000FF);  
								ip_config.PriDNS[0]= (address  & 0x000000FF);
								Ip_Address_Valid=1;	
							}else Ip_Address_Valid=0;		
						}
						
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(inet_aton(temp_buff, &address))
							{
								ip_config.SecDNS[3] = ((address >> 24) & 0x000000FF); 
								ip_config.SecDNS[2]= ((address >> 16) & 0x000000FF);  
								ip_config.SecDNS[1]= ((address >> 8) & 0x000000FF);  
								ip_config.SecDNS[0]= (address  & 0x000000FF);
								Ip_Address_Valid=1;	
							}else Ip_Address_Valid=0;		
						}
						
						//PCM & PCS configurations 	
						//URL   http://beltwayapi.jwrob.com
						memset(temp_buff,'\0',sizeof(temp_buff));
						memset(Http_Host_Name,'\0',sizeof(Http_Host_Name));
						memset(Http_Port_Number,'\0',sizeof(Http_Port_Number));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							memcpy(Http_Host_Name,temp_buff,sizeof(temp_buff));
							//if(0 !=(cptr = HTTPStrCaseStr(Http_Host_Name,4,"\r\n")))  //commented by DK on 10 March 2015 
							if((0 !=(cptr = HTTPStrCaseStr(Http_Host_Name,1,"\r"))) || \
								 (0 !=(cptr = HTTPStrCaseStr(Http_Host_Name,1,"\n"))))  //Added by DK on 10 March 2015 
							{
								memset(cptr,'\0',1);
							}		
						}
						
						http_port_number =80;
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							memcpy(Http_Port_Number,temp_buff,sizeof(Http_Port_Number));
							http_port_number=atoi(Http_Port_Number);
						}
						
						//UTC to local time diff in Hr:Mn
						Lst_Sign =0;
						LstHour =0;
						LstMinutes =0; 
						
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(temp_buff[0] == '+')
							Lst_Sign =0;
							else 
							Lst_Sign =1;	
							
							memset(tm_buf,'\0',sizeof(tm_buf));
							strncpy(tm_buf,&temp_buff[1],2);
							LstHour = atoi(tm_buf);
							
							memset(tm_buf,'\0',sizeof(tm_buf));
							strncpy(tm_buf,&temp_buff[4],2);
							LstMinutes = atoi(tm_buf);
						}
						
						dst_on_off =0;
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(temp_buff[0] == '0')
							dst_on_off =0;
							else 
							dst_on_off =1;	
						}	
						
						address = 1;
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							address = atoi(temp_buff);
						}
						PCM_ID = address; 

						memset(temp_buff,'\0',sizeof(temp_buff));
						memset(sntp_host_name,'\0',sizeof(sntp_host_name));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							strncpy(sntp_host_name, temp_buff, sizeof(sntp_host_name));		
							for(i=0;i<sizeof(sntp_host_name);i++)
							{
								if((sntp_host_name[i] == '\r') || (sntp_host_name[i] == '\n'))
								{
									sntp_host_name[i] = '\0';
								}	
							}	
						}

						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							if(temp_buff[0] == '1')
							{
								ppp_enable = 1;	
			//					sys_config.NetCfg |= (1<<3); //enabling Use Default Gateway on remote Network i.e. #define PPP_DEFGW      1
			//   ========================================
			//   <i> This option only applies when both Ethernet and PPP Dial-up
			//   <i> are used. If checked, data that cannot be sent to local LAN
			//   <i> is forwarded to Dial-up network instead.
							
					//			#ifdef DEBUG_PORT
								memset(Debug_Buf,'\0',sizeof(Debug_Buf));
								sprintf(Debug_Buf, "PPP Client Functionality enabled\r\n");
								Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
						//		#endif
							}	
						}
						
						//Added by DK on 29 January 2015 
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							strncpy(ppp_dial_number, temp_buff, sizeof(ppp_dial_number));		
							for(i=0;i<sizeof(ppp_dial_number);i++)
							{
								if((ppp_dial_number[i] == '\r') || (ppp_dial_number[i] == '\n'))
								{
									ppp_dial_number[i] = '\0';
								}	
							}	
						}
						
						//Added by DK on 29 January 2015 
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							strncpy(ppp_user_name, temp_buff, sizeof(ppp_user_name));		
							for(i=0;i<sizeof(ppp_user_name);i++)
							{
								if((ppp_user_name[i] == '\r') || (ppp_user_name[i] == '\n'))
								{
									ppp_user_name[i] = '\0';
								}	
							}	
						}
						
						//Added by DK on 29 January 2015 
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							strncpy(ppp_password, temp_buff, sizeof(ppp_password));		
							for(i=0;i<sizeof(ppp_password);i++)
							{
								if((ppp_password[i] == '\r') || (ppp_password[i] == '\n'))
								{
									ppp_password[i] = '\0';
								}	
							}	
						}
						
						//Added by DK on 29 January 2015 
						memset(temp_buff,'\0',sizeof(temp_buff));
						if(fgets(temp_buff,sizeof(temp_buff),fp))
						{
							strncpy(ppp_apn, temp_buff, sizeof(ppp_apn));		
							for(i=0;i<sizeof(ppp_apn);i++)
							{
								if((ppp_apn[i] == '\r') || (ppp_apn[i] == '\n'))
								{
									ppp_apn[i] = '\0';
								}	
							}	
						}
						
						fclose(fp);
				}	
					//	fclose(fp);							 
			}
*/			
		}
	}
	else
	{
		screen_variables_init(); /*Initialize with default values*/
		Restore_values_to_original_pcm_variables();
		WDTFeed();
	}
				if (Flags_struct.Connection_flags & USB_CON_FLAG)
				{
//					sprintf(&file_name[0],"system_log.txt");
//					system_log.Id = 0;
//					log_file_init(file_name);

// 					sprintf(&system_log.log_buf[0],"SYSTEM POWER ON");
// 					log_file_write(system_log1);
				}	

  //Added by DK on 19 May2016 to resolve issue of PCM lockup if cell mdoem is connected
	if(ppp_enable == 1 )
	{
//		modem_init();
  }	
			
	cell_modem_strength = 0; //Added by DK on 6 Feb 2015 for connectivity status update
 // if((Ip_Address_Valid ==1) && (dhcp_enable == 0))
  if(Ip_Address_Valid ==1) 
	mem_copy ((U8 *)&localm[NETIF_ETH], (U8 *)&ip_config, sizeof(ip_config));
	
	#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
	sprintf(Debug_Buf, "\r\nInitializing TCPnet!");
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	#endif
	
// Enable and Disable Ethernet
#ifdef ETHERNET
	WDTFeed();	
 init_TcpNet ();
	
#endif
//	Write_Debug_Logs_into_file(1,0,0);
	GUI_data_nav.GUI_structure_backup = 0;

	Ready_Write = 1;	
	
	


// w.wdtReset = 0;
// w.wdtProtect = 0;
// w.wdtWarningVal = 100;
// w.wdtTmrConst = 10000;


#ifdef RTOS
  os_sys_init (init);                   /* Initialize RTX and start init */
#endif /*#ifdef RTOS*/
}


void Restore_values_to_original_pcm_variables(void)
{
	double GMT_tmp ,GMT_tmp1;	
	int i, Ip_Address_Valid;;
	  if(Pcm_var.DHCP_Status == Screen22_str14) 
    {
				dhcp_enable =1;
		}
		else
		{
				dhcp_enable =0;
		}
		ip_config.IpAdr[0] = Pcm_var.IP_Addr.Addr1;
		ip_config.IpAdr[1] = Pcm_var.IP_Addr.Addr2;
		ip_config.IpAdr[2] = Pcm_var.IP_Addr.Addr3;
		ip_config.IpAdr[3] = Pcm_var.IP_Addr.Addr4;	
	
		ip_config.NetMask[0] = Pcm_var.Subnet_Mask.Addr1;
		ip_config.NetMask[1] = Pcm_var.Subnet_Mask.Addr2;
		ip_config.NetMask[2] = Pcm_var.Subnet_Mask.Addr3;
		ip_config.NetMask[3] = Pcm_var.Subnet_Mask.Addr4;
		
		ip_config.DefGW[0] = Pcm_var.Gateway.Addr1;
		ip_config.DefGW[1] = Pcm_var.Gateway.Addr2;
		ip_config.DefGW[2] = Pcm_var.Gateway.Addr3;
		ip_config.DefGW[3] = Pcm_var.Gateway.Addr4;	
	
		ip_config.PriDNS[0] = Pcm_var.PriDNS.Addr1;
		ip_config.PriDNS[1] = Pcm_var.PriDNS.Addr2;
		ip_config.PriDNS[2] = Pcm_var.PriDNS.Addr3;
		ip_config.PriDNS[3] = Pcm_var.PriDNS.Addr4;
		
		ip_config.SecDNS[0] = Pcm_var.SecDNS.Addr1;
		ip_config.SecDNS[1] = Pcm_var.SecDNS.Addr2;
		ip_config.SecDNS[2] = Pcm_var.SecDNS.Addr3;
		ip_config.SecDNS[3] = Pcm_var.SecDNS.Addr4;

		http_port_number = Pcm_var.Http_Port_Number;
//		Pcm_var.Http_host_addr[HTTP_HOST_NOC - 1] = '\0';
//		strncpy(Http_Host_Name, Pcm_var.Http_host_addr, sizeof(Pcm_var.Http_host_addr));

		if(Pcm_var.PCM_web_select==Screen212_str1)
		{
			strcpy(Http_Host_Name,"http://plantconnect-api-p.azurewebsites.net");
			
		}
		else if(Pcm_var.PCM_web_select==Screen212_str3)
		{
			strcpy(Http_Host_Name,"http://plantconnect-api.azurewebsites.net");

		}
		if(Pcm_var.DST_Status == Screen25_str8)
		{
			dst_on_off = 0;
		}
		else
		{
			dst_on_off = 1;
		}
		PCM_ID = Pcm_var.PCM_id;
		
//		if(Pcm_var.PPP_Status == Screen24_str10)
		if(Pcm_var.PCM_connection_select == Screen213_str3)
		
		{
			if(Pcm_var.PPP_Status == Screen24_str9)
			{
//				ppp_enable = 1;

			}	
		}
		else
		{
			ppp_enable = 0;
			Pcm_var.PPP_Status = Screen24_str10;
		}
		Pcm_var.PPP_dial_no[10] = '\0';
		strncpy(ppp_dial_number,Pcm_var.PPP_dial_no,sizeof(Pcm_var.PPP_dial_no));
		Ip_Address_Valid = 1;
  if(Ip_Address_Valid ==1) 
	mem_copy ((U8 *)&localm[NETIF_ETH], (U8 *)&ip_config, sizeof(ip_config));	
	strncpy(&Pcm_var.Int_firmware_version[0], FirmwareVer,sizeof(FirmwareVer));	
	strcpy(sntp_host_name, "0.us.pool.ntp.org");	
	strcpy(&Pcm_var.Sntp_host_addr[0],sntp_host_name);	
	
	if(Pcm_var.GMT_value < 0.0)
	{
		Lst_Sign =1;
	}
	else
	{
		Lst_Sign =0;
	}
	GMT_tmp = fabs(Pcm_var.GMT_value);
						LstHour = (unsigned int)GMT_tmp;
						GMT_tmp = modf(GMT_tmp,&GMT_tmp1); 	
						LstMinutes = GMT_tmp * 100;
}



void Create_Errorlog_file_and_Log_events(void) {
	FILE *fp;
	static char file_name[FILE_NAME_SIZE];
	static char /*temp_buff[200],*/Buff1[10] = "\t";
	char *Ptr;
	U8 u8File_exists = 0, chError = 0;
	U16 Data_valid;

	unsigned int uiRecord_No = 0;
	float Temp_g_current_fw_ver;
	
	//---by megha on 27/7/2017 for corrupted USB on power on
// 			if ((fp = fopen("temp","w"))==0)
// 		{
// 				con =0;
// 				corrupt_usb_Flag =1;

// 		}
// 		else
// 		{
// 		 fclose (fp);
// 				con =1;
// 				corrupt_usb_Flag =0;
// 				Flags_struct.Connection_flags/*.Communication_flags*/|=  USB_CON_FLAG;
// 		}	
	//-------------
    if (con == 1)
    {
//		fp = fopen ("error_log.txt", "a");
//		if (fp==NULL)
//			return;
//		else{
				//find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
			//Brown Out Detect (BOD), system reset, and lockup)
				memset(file_name, 0, sizeof(file_name));
// 				sprintf(&system_log.log_buf[0],"System Reset Due to WDT RESET event with  Task identifier  %x\r\n",LPC_RTC->GPREG4);
// 				log_file_write(system_log1);	
	
				//if(((LPC_SC->RSID & 0x01) == 0x01) || ((LPC_SC->RSID & 0x02) == 0x02))
				if ((LPC_SC->RSID & 0x01) == 0x01) {
					//strcpy(file_name,"Power On Or External Reset");
					strcpy(file_name, "Power On Reset");
					LPC_SC->RSID |= 0x01;
//						sprintf(&system_log.log_buf[0],"System Reset Due to %s Tid=%d/r/n",file_name,LPC_RTC->GPREG4);
						sprintf(&system_log.log_buf[0],"System Reset Due to %s  , %d , %d , %d , %d, %d",\
																	file_name,LPC_RTC->ALHOUR,LPC_RTC->ALMIN,LPC_RTC->ALSEC,LPC_RTC->ALDOY,LPC_RTC->ALDOW);
	//tasks: get_time,modbus_tcp_master,HTTP_client,ftp_client,usb_main				
				log_file_write(system_log1);
// 				LPC_RTC->ALHOUR = 0;
// 				LPC_RTC->ALMIN = 0;
// 				LPC_RTC->ALSEC = 0;
// 				LPC_RTC->ALDOY = 0;
	#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
	sprintf(Debug_Buf, "\r\nPower On Reset!");
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	#endif					
				} else if ((LPC_SC->RSID & 0x02) == 0x02) {
					strcpy(file_name, "External Reset");
					LPC_SC->RSID |= 0x02;
//						sprintf(&system_log.log_buf[0],"System Reset Due to %s Tid=%d/r/n",file_name,LPC_RTC->GPREG4);
						sprintf(&system_log.log_buf[0],"System Reset Due to %s  , %d , %d , %d , %d, %d",\
																	file_name,LPC_RTC->ALHOUR,LPC_RTC->ALMIN,LPC_RTC->ALSEC,LPC_RTC->ALDOY,LPC_RTC->ALDOW);
	//tasks: get_time,modbus_tcp_master,HTTP_client,ftp_client,usb_main						
				log_file_write(system_log1);	
// 				LPC_RTC->ALHOUR = 0;
// 				LPC_RTC->ALMIN = 0;
// 				LPC_RTC->ALSEC = 0;
// 				LPC_RTC->ALDOY = 0;					
	#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
	sprintf(Debug_Buf, "\r\nPower On Reset!");
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	#endif					
				}

				if ((LPC_SC->RSID & 0x04) == 0x04) {
					strcpy(file_name, "Watchdog Timeout");
				//	fprintf(fp,"System Reset Due to WDT RESET event with  Task identifier  %x\r\n",LPC_RTC->GPREG4);
//						sprintf(&system_log.log_buf[0],"System Reset Due to WDT RESET event with  Task identifier  %x\r\n",LPC_RTC->GPREG4);
						sprintf(&system_log.log_buf[0],"System Reset Due to %s  , %d , %d , %d , %d, %d",\
																	file_name,LPC_RTC->ALHOUR,LPC_RTC->ALMIN,LPC_RTC->ALSEC,LPC_RTC->ALDOY,LPC_RTC->ALDOW);
	//tasks: get_time,modbus_tcp_master,HTTP_client,ftp_client,usb_main						
				log_file_write(system_log1);
//  				LPC_RTC->ALHOUR = 0;
//  				LPC_RTC->ALMIN = 0;
//  				LPC_RTC->ALSEC = 0;
//  				LPC_RTC->ALDOY = 0;	
// 				LPC_RTC->ALDOW = 0;		

					//fclose(fp);
					LPC_SC->RSID |= 0x04;
	#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
	sprintf(Debug_Buf, "\r\nWatchdog Timeout!");
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	#endif					
				}

				if ((LPC_SC->RSID & 0x08) == 0x08) {
					strcpy(file_name, "Brownout Detection");
					LPC_SC->RSID |= 0x08;
						sprintf(&system_log.log_buf[0],"System Reset Due to %s  , %d , %d , %d , %d, %d",\
																	file_name,LPC_RTC->ALHOUR,LPC_RTC->ALMIN,LPC_RTC->ALSEC,LPC_RTC->ALDOY,LPC_RTC->ALDOW);
	//tasks: get_time,modbus_tcp_master,HTTP_client,ftp_client,usb_main					
				log_file_write(system_log1);		
// 				LPC_RTC->ALHOUR = 0;
// 				LPC_RTC->ALMIN = 0;
// 				LPC_RTC->ALSEC = 0;
// 				LPC_RTC->ALDOY = 0;
// 				LPC_RTC->ALDOW = 0;		
	
	#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
	sprintf(Debug_Buf, "\r\nBrownout Detection!");
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	#endif					
				}

				if ((LPC_SC->RSID & 0x10) == 0x10) {
					strcpy(file_name, "System Reset Request");
					LPC_SC->RSID |= 0x10;
//						sprintf(&system_log.log_buf[0],"System Reset Due to %s Tid=%d/r/n",file_name,LPC_RTC->GPREG4);
						sprintf(&system_log.log_buf[0],"System Reset Due to %s  , %d , %d , %d , %d, %d",\
																	file_name,LPC_RTC->ALHOUR,LPC_RTC->ALMIN,LPC_RTC->ALSEC,LPC_RTC->ALDOY,LPC_RTC->ALDOW);
	//tasks: get_time,modbus_tcp_master,HTTP_client,ftp_client,usb_main						
				log_file_write(system_log1);	
// 				LPC_RTC->ALHOUR = 0;
// 				LPC_RTC->ALMIN = 0;
// 				LPC_RTC->ALSEC = 0;
// 				LPC_RTC->ALDOY = 0;	
// 				LPC_RTC->ALDOW = 0;		

	#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
	sprintf(Debug_Buf, "\r\nSystem Reset Requestt!");
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	#endif					
				}

				if ((LPC_SC->RSID & 0x20) == 0x20) {
					strcpy(file_name, "Hard Fault");
					LPC_SC->RSID |= 0x20;
//						sprintf(&system_log.log_buf[0],"System Reset Due to %s Tid=%d/r/n",file_name,LPC_RTC->GPREG4);
						sprintf(&system_log.log_buf[0],"System Reset Due to %s  , %d , %d , %d , %d, %d",\
																	file_name,LPC_RTC->ALHOUR,LPC_RTC->ALMIN,LPC_RTC->ALSEC,LPC_RTC->ALDOY,LPC_RTC->ALDOW);
	//tasks: get_time,modbus_tcp_master,HTTP_client,ftp_client,usb_main						
				log_file_write(system_log1);	
// 				LPC_RTC->ALHOUR = 0;
// 				LPC_RTC->ALMIN = 0;
// 				LPC_RTC->ALSEC = 0;
// 				LPC_RTC->ALDOY = 0;
// 				LPC_RTC->ALDOW = 0;		
				
	#ifdef DEBUG_PORT
	memset(Debug_Buf,'\0',sizeof(Debug_Buf));
	sprintf(Debug_Buf, "\r\nHard Fault!");
	Uart_i2c_data_send((uint8_t *)Debug_Buf, strlen(Debug_Buf));
	#endif					
				}
// BY MEGHA ON 26/7/2017 TO LOG USB AND WATCHDOG STATUS 
                if ((LPC_RTC->ALDOW &(0x01<<2))==(0x01<<2))
                {
                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    sprintf(&system_log.log_buf[0], "CORRUPT USB!");
										log_file_write(system_log1);
                };				
               if (WDT_EN ==0)
                {
                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    sprintf(&system_log.log_buf[0], "WATCHDOG DISABLED! NECESSARY WIREMOD NOT DETECTED!");
										log_file_write(system_log1);
                }
                else
                {
                    //find out reset source from 6 sources (the RESET pin, Watchdog Reset, Power On Reset (POR), \
                    //Brown Out Detect (BOD), system reset, and lockup)
                    sprintf(&system_log.log_buf[0], "WATCHDOG ENABLED! RST WIREMOD  DETECTED!");
										log_file_write(system_log1);
                }				
				
			}
	//		}

}
/*****************************************************************************
* End of file
*****************************************************************************/
