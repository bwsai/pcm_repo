/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename 		   : rtc.c
* @brief			         : Controller Board
*
* @author			         : Anagha Basole
*
* @date Created		     : July Thursday, 2012  <July 16, 2012>
* @date Last Modified	 : July Thursday, 2012  <July 16, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "rtc.h"
//#include "Scoreboard.h"  //Commented by DK on 12Sep 2014
//#include "Printer_high_level.h"  

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
U8 Run_Screen_Flags;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
FLAGS_STRUCT_DEF Flags_struct;

/*============================================================================
* Public Function Declarations
*===========================================================================*/
void set_uart_param(unsigned int value, U8 parameter, U8 printer_scoreboard);

void set_uart_param(unsigned int value, U8 parameter, U8 printer_scoreboard)
{
	  UART_CFG_Type * uart_struct;
	
		if(printer_scoreboard == Printer)
		{
//			uart_struct = &PRINTER_ConfigStruct;  //Commented by DK on 12 sep 2014 for PCM 
		}
		else //scoreboard
		{
	//		uart_struct = &Scoreboard_ConfigStruct;  //Commented by DK on 12 sep 2014 for PCM 
		}
		switch (parameter)
		{
				case set_baud_rate:
					uart_struct->Baud_rate = value;
					break;
				case set_data_bits:
					if(value == 8)
					{
						uart_struct->Databits = UART_DATABIT_8;
					}
					else
					{
						uart_struct->Databits = UART_DATABIT_7;
					}
					break;
				case set_stop_bits:
					if(value == 1)
					{
						uart_struct->Stopbits = UART_STOPBIT_1;
					}
					else
					{
						uart_struct->Stopbits = UART_STOPBIT_2;
					}
					break;
				case set_flow_control:
					if(value == 0)
					{
						uart_struct->Parity = UART_PARITY_NONE;
					}
					break;
		}
		return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
