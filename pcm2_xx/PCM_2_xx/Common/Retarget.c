/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : Retarget.c
* @brief			         : Retarget low level functions
*
* @author			         : Dnyaneshwar Kashid
*
* @date Created		     : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified	 : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     :
* @internal 			     :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <rt_sys.h>
#include <File_Config.h>
#include "Global_ex.h"
#include "I2C_driver.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
#define STDIO
/* The following macro definitions may be used to translate this file:

  STDIO - use standard Input/Output device
          (default is NOT used)
 */

/* Standard IO device handles. */
#define STDIN   0x8001
#define STDOUT  0x8002
#define STDERR  0x8003

/* Standard IO device name defines. */
const char __stdin_name[]  = "STDIN";
const char __stdout_name[] = "STDOUT";
const char __stderr_name[] = "STDERR";

struct __FILE { int handle; /* Add whatever you need here */ };
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name:
* @returns    returns		   : None
* @param      arg1			   : None
* @param      arg2			   : None
* @author			             : Anagha Basole
* @date       date created :
* @brief      Description	 : Systick Interrupt Handler
														 SysTick interrupt happens every 10 ms
* @note       Notes		     : None
*****************************************************************************/

#pragma import(__use_no_semihosting_swi)

#ifdef STDIO
#ifndef EVALUATION_BOARD
/*------------------------------------------------------------------------------
  Write character to the Serial Port
 *----------------------------------------------------------------------------*/
int sendchar (int ch)
{
 #ifdef DEBUG_PORT
  if (ch == '\n')
	{
//    Debugport_putchar ('\r');
  }
  //Debugport_putchar (ch);
 #endif

  return (ch);
}


/*------------------------------------------------------------------------------
  Read character from the Serial Port
 *----------------------------------------------------------------------------*/
int getkey (void)
{
	#ifdef DEBUG_PORT
  int ch = 0;//Debugport_getchar();

  if (ch < 0) {
    return 0;
  }
  return ch;
	#else
	return 0;
	#endif
}
#endif //#ifdef STDIO1

/*--------------------------- _ttywrch ---------------------------------------*/

void _ttywrch (int ch) {
#ifdef STDIO
  sendchar (ch);
#endif
}
#endif //#if EVALUATION_BOARD
/*--------------------------- _sys_open --------------------------------------*/

FILEHANDLE _sys_open (const char *name, int openmode)
{
  /* Register standard Input Output devices. */
  if (strcmp(name, "STDIN") == 0)
	{
    return (STDIN);
  }
  if (strcmp(name, "STDOUT") == 0)
	{
    return (STDOUT);
  }
  if (strcmp(name, "STDERR") == 0)
	{
    return (STDERR);
  }
  return (__sys_open (name, openmode));
}

/*--------------------------- _sys_close -------------------------------------*/

int _sys_close (FILEHANDLE fh)
{
  if (fh > 0x8000)
	{
    return (0);
  }
  return (__sys_close (fh));
}

/*--------------------------- _sys_write -------------------------------------*/

int _sys_write (FILEHANDLE fh, const U8 *buf, U32 len, int mode)
{

#ifdef STDIO
  if (fh == STDOUT)
	{
			Uart_i2c_data_send((uint8_t *)buf, len);
		  Uart_i2c_data_send((uint8_t *)"\r\n", 2);
			//Uart_i2c_data_send(buffer, 40);
    /* Standard Output device. */
			return (0);
  }
#endif
  if (fh > 0x8000)
	{
    return (-1);
  }
  return (__sys_write (fh, buf, len));
}

/*--------------------------- _sys_read --------------------------------------*/

int _sys_read (FILEHANDLE fh, U8 *buf, U32 len, int mode)
{
#ifdef STDIO
  if (fh == STDIN)
	{
    /* Standard Input device. */
		#if EVALUATION_BOARD
    for (  ; len; len--)
		{
      *buf++ = getkey ();
    }
		#endif
    return (0);
  }
#endif
  if (fh > 0x8000)
	{
    return (-1);
  }
  return (__sys_read (fh, buf, len));
}

/*--------------------------- _sys_istty -------------------------------------*/

int _sys_istty (FILEHANDLE fh)
{
  if (fh > 0x8000)
	{
    return (1);
  }
  return (0);
}

/*--------------------------- _sys_seek --------------------------------------*/

int _sys_seek (FILEHANDLE fh, long pos)
{
  if (fh > 0x8000)
	{
    return (-1);
  }
  return (__sys_seek (fh, pos));
}

/*--------------------------- _sys_ensure ------------------------------------*/

int _sys_ensure (FILEHANDLE fh)
{
  if (fh > 0x8000)
	{
    return (-1);
  }
  return (__sys_ensure (fh));
}

/*--------------------------- _sys_flen --------------------------------------*/

long _sys_flen (FILEHANDLE fh)
{
  if (fh > 0x8000)
	{
    return (0);
  }
  return (__sys_flen (fh));
}

/*--------------------------- _sys_tmpnam ------------------------------------*/

int _sys_tmpnam (char *name, int sig, unsigned maxlen)
{
  return (1);
}

/*--------------------------- _sys_command_string ----------------------------*/

char *_sys_command_string (char *cmd, int len)
{
  return (cmd);
}

/*--------------------------- _sys_exit --------------------------------------*/

void _sys_exit (int return_code)
{
  /* Endless loop. */
  while (1);
}

/*****************************************************************************
* End of file
*****************************************************************************/
