/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename 		   : rtc.h
* @brief			         : Controller Board
*
* @author			         : Dnyaneshwar Kashid
*
* @date Created        : September Monday, 2014  <Sep 15, 2014>
* @date Last Modified  : November Monday, 2014  <Nov 24, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 			     : 
* @internal 			     :
*
*****************************************************************************/
#ifndef __RTC_H 
#define __RTC_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>
#include "Global_ex.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifdef RTC
	#define IMSEC		0x00000001
	#define IMMIN		0x00000002
	#define IMHOUR		0x00000004
	#define IMDOM		0x00000008
	#define IMDOW		0x00000010
	#define IMDOY		0x00000020
	#define IMMON		0x00000040
	#define IMYEAR		0x00000080

	#define AMRSEC		0x00000001  /* Alarm mask for Seconds */
	#define AMRMIN		0x00000002  /* Alarm mask for Minutes */
	#define AMRHOUR		0x00000004  /* Alarm mask for Hours */
	#define AMRDOM		0x00000008  /* Alarm mask for Day of Month */
	#define AMRDOW		0x00000010  /* Alarm mask for Day of Week */
	#define AMRDOY		0x00000020  /* Alarm mask for Day of Year */
	#define AMRMON		0x00000040  /* Alarm mask for Month */
	#define AMRYEAR		0x00000080  /* Alarm mask for Year */

	#define PREINT_RTC	0x000001C8  /* Prescaler value, integer portion, 
							PCLK = 15Mhz */
	#define PREFRAC_RTC	0x000061C0  /* Prescaler value, fraction portion, 
							PCLK = 15Mhz */
	#define ILR_RTCCIF	0x01
	#define ILR_RTCALF	0x02

	#define CCR_CLKEN	0x01
	#define CCR_CTCRST	0x02
	#define CCR_CLKSRC	0x10
		
/*============================================================================
* Public Data Types
*===========================================================================*/
typedef struct {
    U32 RTC_Sec;     /* Second value - [0,59] */
    U32 RTC_Min;     /* Minute value - [0,59] */
    U32 RTC_Hour;    /* Hour value - [0,23] */
    U32 RTC_Mday;    /* Day of the month value - [1,31] */
    U32 RTC_Mon;     /* Month value - [1,12] */
    U32 RTC_Year;    /* Year value - [0,4095] */
    U32 RTC_Wday;    /* Day of week value - [0,6] */
    U32 RTC_Yday;    /* Day of year value - [1,365] */
} RTCTime;

/*Time in 24 hour format*/
	typedef struct 
	{  
			U8  Hr;                                /* Hours    [0..23]                  */
			U8  Min;                               /* Minutes  [0..59]                  */
			U8  Sec;                               /* Seconds  [0..59]                  */
	}TIME_STRUCT;

	/*Date is YYYY-MM-DD format*/
	typedef struct 
	{ 
			U16 Year;                              /* Year     [1980..2107]             */
			U8  Mon;                               /* Month    [1..12]                  */
			U8  Day;                               /* Day      [1..31]                  */
	}DATE_STRUCT;
	
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
#ifdef RTC
	extern RTCTime Current_time;
#endif
//	extern FLAGS_STRUCT_DEF Flags_struct;

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern void RTC_IRQHandler (void);
extern void RTCInit( void );
extern void RTCStart( void );
extern void RTCStop( void );
extern void RTC_CTCReset( void );
extern void RTCSetTime( RTCTime );
extern RTCTime RTCGetTime( void );
extern void RTCSetAlarm( RTCTime );
extern void RTCSetAlarmMask( U32 AlarmMask );
extern void Set_rtc_date(DATE_STRUCT date);
extern void Set_rtc_time(TIME_STRUCT time);
extern void Set_rtc_dow(unsigned int dow);
extern U16 Get_Dow_String_Enum(U16 dow);
extern void update_rtc(void);

#endif /*ifdef RTC*/

#endif /* end __RTC_H */
/*****************************************************************************
**                            End Of File
******************************************************************************/
