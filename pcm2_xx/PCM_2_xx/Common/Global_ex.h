/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Plant Connect Modem
* @detail Customer     : Beltway
*
* @file Filename       : Global_ex.h
* @brief               : Controller Board
*
* @author              : Dnyaneshwar Kashid
*
* @date Created        : September 15th Monday, 2014
* @date Last Modified  : December Thursday, 2014 <Dec 11, 2014>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __GLOBAL_EX_H
#define __GLOBAL_EX_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <RTL.h>
#include "../GUI/GUI.h"
#include "LPC177x_8x.h"                      /* LPC177x_8x definitions*/
#include "lpc177x_8x_i2c.h"
#include "lpc177x_8x_clkpwr.h"
#include "lpc177x_8x_pinsel.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
//#define REBOOT_EN   //commented by megha on 30th dec 2016 //uncomment this for enabling rebooting of PCM after every 45 minutes

#define MATRIX_ARB      (*(volatile U32*)(0x400FC188))
#define ARRAY_SIZE      45
#define FILE_NAME_SIZE	40

/*
TASK ENTRY NUMBER TO SAVED IN RTC SCRATCHPAD AREA
0	INIT
1	TCP_TICK
2	TCP_TASK_MAIN
3	GET_TIME
4	USB_TASK
5	PPP_CLIENT
6	MB_TCP_MASTER_THREAD
7	MODBUS_TCP_MASTER
8	KEYPAD_TASK
9	HTTP_CLIENT
10	UPDATE_LCD
11	FTP_CLIENT
*/
#define	INIT									0
#define	TCP_TICK							1
#define	TCP_TASK_MAIN					2
#define	GET_TIME							3
#define	USB_TASK							4
#define	PPP_CLIENT						5
#define	MB_TCP_MASTER_THREAD	6
#define	MODBUS_TCP_MASTER			7
#define	KEYPAD_TASK						8
#define	HTTP_CLIENT						9
#define	UPDATE_LCD						10
#define	FTP_CLIENT						11
//Enable / Disable macros to activate Functions
#define RTOS
#define RTC
#define EXT_FLASH
#define FILE_SYS
#define USB
#define EEPROM
#define MODBUS_TCP  
#define ETHERNET
//#define DEBUG_PORT
#define DEBUG_TASK
#define GUI
#define KEYPAD
#define MVT_TABLE
#include  "DebugLog.h"

//CPU CLK
#define __CPUCLK                                               120000000

//Task Time - All task intervals are in 10mSec resolution
//Task Time - All task intervals are in 10mSec resolution
#define FREE_RUN_TIMER_TASK_INTERVAL                           5
#define KEYPAD_TASK_INTERVAL                                   15
#define USB_CON_TASK_INTERVAL                                  2
#define RTC_GET_TIME_TASK_INTERVAL                             5//50 //100 changed by DK from 100 to 50 on 15 Jan2015
#define DEBUG_TASK_RUN_INTERVAL																 6000

#ifdef  REBOOT_EN
#define TIMEOUT_TASK_INTERVAL                                  50 //Added by DK on 3 May2016 to reset PCM every 45 minutes (i.e. 45 * 60 seconds) 
#endif 

#define ETHERNET_MAIN_TASK_INTERVAL                            1//  on 3/3/2017
#define ETHERNET_TCP_TICK_TASK_INTERVAL                        5
#define MODBUS_TCP_MASTER_INTERVAL 			                       10//100  
#define HTTP_CLIENT_INTERVAL 			                       			 10  
#define PPP_CLIENT_INTERVAL 			                       			 100 
#define FTP_TASK_INTERVAL                       					     100
#define DHCP_TOUT                             								 500   //5000   /* DHCP timeout 5 seconds      */
#define LCD_TASK_INTERVAL                                      20
/*Connection flags bit definitions*/
#define USB_CON_FLAG                                    0x01
#define USB_CON_EX_FLAG                                 0x02
#define ETH_CON_FLAG                                    0x04
/*Log flags bit definitions*/
#define ERROR_LOG_WR_FLAG                               0x10
#define LOGS_WR_FLAG                                    0x40


/*Error flags bit definitions*/
#define EEPROM_WRITE_ERR                                0x80

#define MAC_OCTETS		6


#define LCD_TURNED_ON_INIT                                    0
#define LCD_ON_TIMEOUT_START                                  1
#define LCD_TURNED_OFF_AFTER_TIMEOUT                          2

#define KEPAD_TIMEOUT_COUNT                             10000 //60Sec
#define KBD_TIMEOUT                                     (KEPAD_TIMEOUT_COUNT / FREE_RUN_TIMER_TASK_INTERVAL)
#define KEYPAD_EVENT_FLAG                               0x0080

#define EXT_INT_PIN													1<<29

/*Plant connect flag definitions*/
#define PCS_SERVER_IP_RESOLVED_OK_FLAG                  0x01
#define PCS_SOCKET_CONNECTION_OK_FLAG                   0x02
#define PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG         0x04
#define PCM_SCALE_CONFIGURATION_RECEIVED_OK_FLAG				0x08
#define PCM_SCALE_DATA_UPDATE_OK_FLAG										0X10
#define SCALE_DATA_RECEIVE_OK_FLAG											0x20
#define PCS_SOCKET_CONNECTION_FAIL_FLAG									0x40
#define PCS_SOCKET_STATUS_NC_FLAG												0x80
#define PCS_SERVER_IP_RESOLVE_PROCESS_FLAG							0x100
#define SOCKET_CONNECTION_PROCESS_FLAG									0X200
#define FILE_WRITING_TO_USB_FG													0X400
#define SERVER_IP_RESOLUTION_FAIL_FG										0X800
#define MODBUS_TCP_CONNECTION_FAIL_FG										0X1000

/*Error and warning logs,MESSAGES display array definitions*/
#define PCS_IP_RESOLVED_MSG_TIME                        0
#define PCS_SOCKET_CONNECTION_MSG_TIME									1
#define PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME				2
#define PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME				3
#define PCM_SCALE_DATA_UPDATE_MSG_TIME									4
#define SCALE_DATA_RECEIVE_OK_MSG_TIME									5
#define PCS_SOCKET_CONNECTION_FAIL_MSG_TIME							6	
#define PCS_SOCKET_STATUS_NC_MSG_TIME										7
#define PCS_SERVER_IP_RESOLVE_PROCESS_MSG_TIME					8
#define SOCKET_CONNECTION_PROCESS_MSG_TIME							9
#define FILE_WRITING_TO_USB_MSG_TIME										10
#define SERVER_IP_RESOLVE_FAIL_MSG_TIME									11
#define MODBUS_TCP_FAIL_MSG_TIME												12

#define HEARTBEAT_COUNT																	5
#define POWER_ON_SCREEN_DISP_COUNT											1			
#define FTP_MSG_COUNT																		10
#define READ_SIGNAL_STRENGTH_COUNT											1


#define PCM_ID_CODE																			1				//5
#define PCM_WEB_SELECT_CODE															2			//3
#define PCM_CONNECT_SELECT_CODE													3			//3
#define IP_ADDR_CODE																		4				//5
#define SUBNET_MASK_CODE																5				//5
#define GATEWAY_CODE																		6				//5
#define PRI_DNS_CODE																		7				//5
#define SEC_DNS_CODE																		8				//5
#define PPP_STATUS_CODE																	9			//3
#define PPP_DIAL_CODE																		10				//6
#define DST_STATUS_CODE																	11		//3
#define GMT_CODE																				12			//5
#define SNTP_HOST_ADDR_CODE															13				//6
#define DATE_CODE																				14			//5
#define TIME_CODE																				15			//5
#define DATE_FORMAT_CODE																16		//3
#define	DHCP_STATUS_CODE																17		//3
#define MAC_ID_CODE																			18				//6

/*============================================================================
* Public Data Types
*===========================================================================*/

typedef struct
  {
    U8 Connection_flags;           /*Bit 0-USB,
                                    Bit 1-USB_con_ex,
                                    Bit 2-Modbus 1,
																		Bit 3-Modbus 2*/
    U8 Communication_flags;       /*Bit 0-Integrator board,
                                    Bit 1-Sensor board,
                                    Bit 2-IO board*/
    U8 Report_flags;              /*Flags for report to be written to USBdrive
                                    Bit 0-Calib report,
                                    Bit 1-Error report,
                                    Bit 2-Daily report,
                                    Bit 3-Weekly report,
                                    Bit 4-Monthly report*/
    U8 Log_flags;                 /*Flags for report to be written to USBdrive
                                    Bit 0-Periodic log,
                                    Bit 1-Set Zero log,
                                    Bit 2-Clear log,
                                    Bit 3-Calib log,
                                    Bit 4-Error log,
                                    Bit 5-Length log
                                    Bit 6-Logs in flash to be written to the USB*/
    U8 Calibration_flags;         /*Flags set if calibration needs to be performed
                                    Bit 0-Test weight cal,
                                    Bit 1-Material test cal
                                    Bit 2-Digital cal,
                                    Bit 3-Belt length cal,
                                    Bit 4-Zero cal*/
    U8 Error_flags;                /*Bit 0- Belt speed is zero,
                                    Bit 1 - Load cell error,
                                    Bit 2 - Angle sensor error,
                                    Bit 3 - Sensor board communication error,
                                    Bit 4 - Scale communication error,
                                    Bit 5 - Rate negative for x seconds,
                                    Bit 6 - IO Board communciation error,
															      Bit 7 - EEPROM writes finished, so generate an error*/
    U8 Warning_flags;              /*Bit 0-Low speed warning,
                                    Bit 1-High speed warning,
                                    Bit 2-Zero cal changed significantly warning,
                                    Bit 3-Sensor board 1st missed communication warning,
                                    Bit 4-1st missed communication with scale warning,
                                    Bit 5-Low rate warning,
                                    Bit 6-High rate warning*/
    U16 Err_Wrn_log_written;       /*Bit 0-Belt speed is zero,
                                    Bit 1 - Load cell error,
                                    Bit 2 - Angle sensor error,
                                    Bit 3 - Sensor board communication error,
                                    Bit 4 - Scale communication error,
                                    Bit 5 - Rate negative for x seconds,
                                    Bit 6 - IO Board communciation error
                                    Bit 7 - Low speed warning,
                                    Bit 8 - High speed warning,
                                    Bit 9 - Zero cal changed significantly warning,
                                    Bit 10- Sensor board 1st missed communication warning,
                                    Bit 11- 1st missed communication with scale warning,
                                    Bit 12- Low rate warning,
                                    Bit 13- High rate warning*/
//		U8 Plant_connect_record_flag; /* Flag to indicate Plant connect record flag
		U32 Plant_connect_record_flag; //by megha on 11/1/2017 to display PCM com progress
																		/* Flag to indicate Plant connect record flag

		                                Bit 0 - Flag for new record available
																		Bit 1 - Flag for old record to be read
																		Bit 2 - Flag to indicate that the old record request has been entered
																		Bit 3 - */
		U8 MAC_Programming_Error;      
		                                   //0x01 ----  NO Error
                                       //0x02 ----  MAC ERROR:MACfile.txt file not found
                                       //0x03 ----  MAC ERROR:USB Key not found
																	     //0x04 ----  MAC ERROR:Data in file is invalid
                                       //0x05 ----  MAC ERROR:Verification Failed */
  }FLAGS_STRUCT_DEF;
extern FLAGS_STRUCT_DEF Flags_struct;
	
typedef struct {
	int Max_count_int ;
	int Max_size_int ;
	int Def_size_int ;
}prop_file_ini;

 union uint_union  {
   unsigned int  uint_val;
   unsigned char uint_array[2];
 };

 union ulong_union  {
   unsigned long ulong_val;
   unsigned char ulong_array[4];
};

 union float_union  {
   float         float_val;
   unsigned char f_array[4];
 };
 
 union double_union  {
   double         double_val;
   unsigned char  d_array[8];
 };
typedef union 
{
	  unsigned int value;
    unsigned char int_array[4];
}UINT32_DATA;


extern BOOL Flash_Write_In_progress ; //Added on 27 April 2016 
extern U8 Run_Screen_Flags;
extern U8 g_Q_addition_prog ;
extern U16 g_last_periodic_id;
extern float g_old_fw_ver;
extern float g_current_fw_ver;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
//extern	unsigned char pcs_connect,pcs_sock_state,power_on,dns_ip_resolved;
//extern	int             pcs_sock_num;
//extern	unsigned int pcs_no_of_retry; //,pcs_wait_Count=0; //Commented by DK on 6 Feb 2015 for PCM to PCS connectivity status update


extern const char FirmwareVer[6];
extern U8 con, con_ex;
extern U8 USB_Connection;
extern U8 Task_section; // testing pupose						
//extern int MAC_ID[6];
extern char Mac_id[17];
extern char MAC_ID[6];
extern unsigned int Heartbeat_msg_cntr;
extern unsigned char signal_strength_buf[],rssi_buf[],imei_buffer[];
extern unsigned char power_on_fg,pcs_connect_disp_fg,scale_config_req_fg;
 ;
extern U8 disp_power_on_screen_fg,disp_power_on_screen_timeout_fg,Power_on_screen_disp_cntr,ftp_send_ctr;
extern BOOL ftp_send_fg;
extern unsigned char task1_fg,task2_fg,task3_fg;
/* unsigned integer variables section */
extern unsigned int disp_rx_scale_id;
extern unsigned int msg_log_reg;

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern GUI_CONST_STORAGE GUI_FONT GUI_FontArial_Unicode_MS16_Bold;



extern __task void Keypad_task (void);
extern void Restore_values_to_original_pcm_variables(void);
extern U8 populate_error_message(char * error_msg);
extern void get_utc(void);
extern unsigned int calc_curr_time_in_seconds(void);

extern unsigned int disp_utc_time_hr,disp_utc_time_min,disp_utc_time_sec;
extern unsigned int disp_utc_time_day,disp_utc_time_mon,disp_utc_time_year;
extern unsigned char time_sync_fg,read_signal_strength_fg;
extern unsigned char four_g_ip_change_fg;
extern int corrupt_usb_Flag;
extern unsigned int USB_err_no;
extern unsigned int read_msec_cntr,parameter_code;
extern volatile unsigned int msec_cntr;
extern unsigned char Total_Scales;
extern unsigned char LTE_host_name[];
extern unsigned char restore_factory_set_fg;

/* msg_log_reg bit 				message
0			Server IP Resolution in process.....
1			Server IP resolution failed
2			Server IP resolved OK
3			Server IP resolution time out

4			Socket connection in process
5			TCP Socket failed to connect
6			TCP Socket connected successfully
7

8			Requesting scale configuration from server
9			Scale configuration received ok.
10		Modem is disconnected from server due to no response of 'GET configuration' post
11

12		Initializing ModbusTCP
13		Failed to initialize ModbusTCP
14		ModbusTCP initialization OK
15		Connecting to ModbusTCP slave

16		Connected to ModbusTCP slave successfully
17		Failed to connect to ModbusTCP slave
18		DNS IP Resolution of 4GLTE in process
19		DNS IP Resolution of 4G LTE  failed

20		DNS IP of 4G LTE Resolved ok
21		Socket connection of 4G LTE in process
22		TCP Socket failed to connect to 4G LTE
23		TCP Socket connected to 4G LTE successfully

24		4G LTE login in process
25		4G LTE login successful
26		Reading signal strength of 4G LTE in process
27		Reading signal strength of 4G LTE is successful

28		Reading signal strength of 4G LTE is failed..
29

*/

#endif /*__GLOBAL_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
