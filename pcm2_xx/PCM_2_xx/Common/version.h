/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Belt-Way Scales Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Belt-Way Scales Inc 
* @copyright in this software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Belt-Way Scales Inc.
*
* @detail Project         : Belt Scale Weighing Product, Plant Connect Modem 
* @detail Customer        : Belt-Way Scales Inc
*
* @file Filename 		   		: Version.h
* @brief			       			: 
*
* @author			       			: Dnyaneshwar Kashid 
* 
* @date Created		       	: Month Day, Year  <Sep 15, 2014>
* @date Last Modified	   	: Month Day, Year  <Nov 3, 2014>
*
* @internal Change Log	  :
* @internal 			   			: <YYYY-MM-DD>
* @internal 			   			:
*
* Version History:

* Version 0.1		First PCM firmware version with MB TCP comm with two integrators & HTTP server sent to Beltscale

*****************************************************************************/
#ifndef __VERSION_H 
#define __VERSION_H
/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
/* Defines Section */
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
#define FLASH_BASE_ADDRESS 0x00000000

#define FLASH_LAST_SEC_ADDRESS 0x00078000 // Flash Last sector address  i.e. SECTOR_29_START

// #ifdef REBOOT_EN
// #define FIRMWARE_MAJOR 0x01
// #define FIRMWARE_VER 	0x42	//This must be BCD number for e.g. 34 for PCM firmware v1.34  
// #else 
#define FIRMWARE_MAJOR 0x02
#define FIRMWARE_VER 	0x41	//This must be BCD number for e.g. 01 for PCM firmware v2.01  
// #endif 

const char GUI_PARAM_firmVerstring[] __attribute__((at(FLASH_BASE_ADDRESS + FLASH_LAST_SEC_ADDRESS))) = 
{
    0x00, 0x00,FIRMWARE_MAJOR, FIRMWARE_VER
};


/* Constants section */
// #ifdef REBOOT_EN
// const char FirmwareVer[] = "02.01";
// #else 
const char FirmwareVer[] = "V2.41";
// #endif 

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
#endif 
/*****************************************************************************
**                            End Of File
******************************************************************************/
