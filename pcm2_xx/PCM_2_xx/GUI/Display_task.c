/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project            : Beltscale Weighing Product - Integrator Board
* @detail Customer           : Beltway
*
* @file Filename             : Display_task.c
* @brief                     : Controller Board
*
* @author                    : Anagha Basole
*
* @date Created              : November, 2012
* @date Last Modified        : November, 2012
*
* @internal Change Log       : <YYYY-MM-DD>
* @internal                  :
* @internal                  :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "stddef.h"
#include "GUI.h"
#include "Global_ex.h"
#include "../GUI/DIALOG.h"
#include "Display_task.h"
#include "rtc.h"
#include "RTOS_main.h"
#include <RTL.h>
#include "../GUI/Application/Screen_global_ex.h"
#include "Http_Client.h"
#include <Net_Config.h>
#include "Main.h"
#include "Serial_LPC178x.h"
#include "mbport.h"
#include "math.h"
#include "../Keypad/Keypad.h"
#include "../GUI/Application/Screen_common_func.h"
#include "../gui/application/Screen_data_enum.h"
//#include "../GUI/Application/Screen_global_data.h"
//#include "../GUI/Application/Screen_structure.h"
#include "soft_stack.h"
#ifdef GUI
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/
U8 disp_power_on_screen_fg;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */
unsigned char power_on_fg = 1;
#define RUNSCREEN_TEXT_EDIT_POS_Y     0

/* Character variables section */
// char *Units[] = {
// 	Units_var.Unit_rate_variable,
// 	Units_var.Unit_speed_variable,
// };
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */
I32 used;
/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
__task void update_lcd (void);
extern WM_HWIN Screen_1(void);
void Disp_time(void);
extern  LOCALM localm[];
void init_Admin_var(void);

static void init_Pcm_var (void);
void init_EEP_conf_vars (void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : void screen_variables_init(void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 26th December 2012
* @brief      Description    : Initializes the GUI variables to their default values.
* @note       Notes          : None
*****************************************************************************/





/*****************************************************************************
* @note       Function name  : __task void update_lcd (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 26th December 2012
* @brief      Description    : Task to update the screen based on user input and
*                            : if there is any update in the screen data.
* @note       Notes          : None
*****************************************************************************/
__task void update_lcd (void)
{
	Screen_org_data * current_screen_org_no_main;
	int i = 0, screen_to_be_redrawn = 0;	
	char disp[100];

	init_Admin_var();

    GUI_Init();

/*    MATRIX_ARB = 0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
               | (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
               | (2 <<  2)  // PRI_DCODE : D-Code bus priority.
               | (0 <<  4)  // PRI_SYS   : System bus priority.
               | (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
               | (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
               | (3 << 10)  // PRI_LCD   : LCD DMA priority.
               | (0 << 12)  // PRI_USB   : USB DMA priority.
               | (1 << 16)  // ROM_LAT   : ROM latency select.
               ;
	*/

    GUI_UC_SetEncodeUTF8(); // required only once to activate UTF-8
    GUI_Clear();

    GUI_SetBkColor(GUI_BLACK);
		GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);

		WM_SetCreateFlags(WM_CF_MEMDEV_ON_REDRAW); /* Use memory devices on all windows to avoid flicker */
		WM_MULTIBUF_Enable(1);		
		GUI_data_nav.Up_right_key_pressed = __FALSE;
		GUI_data_nav.Back_one_level = __FALSE;
		GUI_data_nav.Cancel_operation = __FALSE;
		GUI_data_nav.Child_present = __FALSE;
		GUI_data_nav.Change = __FALSE;
		GUI_data_nav.Setup_wizard = __FALSE;
		GUI_data_nav.Key_press = __FALSE;		
		NVIC_EnableIRQ(GPIO_IRQn);
		
		
    while(1)
    {

		if(power_on_fg == 1)
		{
			Task_section = 1;
				sprintf(&disp[0], "IP Addr: %03d.%03d.%03d.%03d",Pcm_var.IP_Addr.Addr1,Pcm_var.IP_Addr.Addr2,Pcm_var.IP_Addr.Addr3,Pcm_var.IP_Addr.Addr4);
						GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE1_POS_Y0);
						sprintf(&disp[0],"MAC Addr: %s",Mac_id);	
						GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1, DISP_LINE1_POS_Y0);
			/*************line 2**************/		
				sprintf(&disp[0], "Gateway Addr: %03d.%03d.%03d.%03d",Pcm_var.Gateway.Addr1,Pcm_var.Gateway.Addr2,Pcm_var.Gateway.Addr3,Pcm_var.Gateway.Addr4);
						GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE2_POS_Y0);	
				sprintf(&disp[0], "SUBMask: %03d.%03d.%03d.%03d",Pcm_var.Subnet_Mask.Addr1,Pcm_var.Subnet_Mask.Addr2,Pcm_var.Subnet_Mask.Addr3,Pcm_var.Subnet_Mask.Addr4);	
						GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1, DISP_LINE2_POS_Y0);
			/*************line 3**************/		
							
		//				sprintf(&disp[0],"Signal Strength: %d",cell_modem_strength);
						sprintf(&disp[0],"PCM Web: %s",Http_Host_Name);
						GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE3_POS_Y0);
						sprintf(&disp[0],"PCM ID: %d",PCM_ID);	
						GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1+100, DISP_LINE3_POS_Y0);
						os_dly_wait(10);
			Task_section = 2;
		}
		if((power_on_fg == 0)&&(disp_power_on_screen_fg == 0))
		{
			Task_section = 3;
			os_dly_wait(500);
			GUI_Clear();			
			current_screen_org_no_main = screen_data_create(PCM_SCREEN_ORG_NO);
			Task_section = 4;
			screen_display(current_screen_org_no_main);
			Task_section = 5;
	//		Create_pcm_main_screen(CREATE_SCREEN);	
	
			disp_power_on_screen_fg = 1;
		}
		else if(disp_power_on_screen_fg == 1)
		{
		os_itv_set(LCD_TASK_INTERVAL);
    os_itv_wait();
			LPC_RTC->GPREG4 |= (0x01<<10);	
Task_section = 6;			
		if ((Keypad_Timeout_Indication == LCD_TURNED_ON_INIT)
				|| (Keypad_Timeout_Indication == LCD_ON_TIMEOUT_START)) 
		{
				GUI_Exec();		
			if (GUI_data_nav.Change == __TRUE) 
			{
Task_section = 7;					
				current_screen_org_no_main = screen_data_create(GUI_data_nav.New_screen_num);
Task_section = 8;					
				screen_to_be_redrawn = __TRUE;
				GUI_data_nav.Change = __FALSE;
				GUI_data_nav.Key_press = __FALSE;
			}
			if (screen_to_be_redrawn == __TRUE) 
			{	
Task_section = 9;					
				screen_display(current_screen_org_no_main);
Task_section = 10;					
				screen_to_be_redrawn = __FALSE;
			}
				
				
				i++;

				if ((i % 5) == 0) 
				{
					Display_Header(1);
					Display_Footer(1);
					if(strncmp(PCM_SCREEN_ORG_NO, /*current_screen_org_no_main*/
						GUI_data_nav.Current_screen_info->Screen_org_no, 1) == 0)
					{
Task_section = 11;							
						Create_pcm_main_screen(UPDATE_SCREEN);
Task_section = 12;							
					}

				
					i = 0;
					
					
				}
				if(GUI_data_nav.Cancel_operation == __TRUE)
				{
						Init_STACK(wiz_nav_stack);
						GUI_data_nav.Back_one_level = 0;
						GUI_data_nav.In_wizard = __FALSE;
						GUI_data_nav.Wizard_screen_index = __FALSE;
						GUI_data_nav.Cancel_operation = __FALSE;
						screen_to_be_redrawn = __TRUE;					
				}
				if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,INT_UPDT_WIZARD_ORG_NO) == 0)||
						// FOR PASSWORD FLAG Will be set in screen2.c call back function.
						(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,BACKUP_USB_WIZARD_ORG_NO) == 0)||
						(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,RESTORE_USB_WIZARD_ORG_NO) == 0)) 
				{
					GUI_data_nav.In_wizard = __TRUE;
				}
				if (GUI_data_nav.Current_screen_info->screens != NULL) {

					if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,
							PASSWORD_SCREEN_ORG_NO) != 0)
							|| (wiz_nav_stack.top == 1)) {
						GUI_data_nav.wizard_screen =
								GUI_data_nav.Current_screen_info->screens;
						GUI_data_nav.Child_present = __FALSE;
						if ((GUI_data_nav.Current_screen_info->Screen_org_no[0]	== '3')) 
						{
							GUI_data_nav.Wizard_screen_index = 1;
						} else 
						{
							GUI_data_nav.Setup_wizard = __TRUE;
							GUI_data_nav.Wizard_screen_index = 1;
						}
					}
				}				

				if (GUI_data_nav.Key_press == __TRUE) //for wizard screens
				{
					if (GUI_data_nav.Child_present != __TRUE) {
						//if the setup wizard end screen is being displayed then on enter go to run mode screen

						if ((GUI_data_nav.In_wizard == __TRUE) && (GUI_data_nav.Cancel_operation == __FALSE)) 
						{
							if ((unsigned int) GUI_data_nav.Wizard_screen_index	< GUI_data_nav.wizard_screen->wizard_size) 
							{
								current_screen_org_no_main =	screen_data_create(	GUI_data_nav.wizard_screen->Next[GUI_data_nav.Wizard_screen_index].number);
								GUI_data_nav.Wizard_screen_index++;
								screen_to_be_redrawn = __TRUE;
							}
							//if this is the last screen in the wizard then figure out which screen needs to be displayed
							else if ((unsigned int) GUI_data_nav.Wizard_screen_index == GUI_data_nav.wizard_screen->wizard_size) 
							{
								//if the user is in setup wizard then figure out the next screen organisation number
								//depending on the structure entry
								if ((GUI_data_nav.Setup_wizard == __TRUE)	&& (password_struct.password_to_be_entered	!= __TRUE)) 
								{
										current_screen_org_no_main =	screen_data_create(	GUI_data_nav.wizard_screen->setup_wizard_org_no);
										GUI_data_nav.wizard_screen =	current_screen_org_no_main->screens;
										GUI_data_nav.Wizard_screen_index = 0;
								}
								//if user is in the normal wizard then create the end screen and reset the flags
								else 
								{ // If we Initiate Zero calibration through ZERO Key then End is Runmode screen

									current_screen_org_no_main =screen_data_create(GUI_data_nav.wizard_screen->End);
									GUI_data_nav.In_wizard = __FALSE;
									GUI_data_nav.Wizard_screen_index = 0;
									GUI_data_nav.wizard_screen = NULL;
								}
								screen_to_be_redrawn = __TRUE;
							} 
							else 
							{
								GUI_data_nav.In_wizard = __FALSE;
								GUI_data_nav.Wizard_screen_index = __FALSE;
							}
						}  //in_wizard && cancel
						else if ((GUI_data_nav.In_wizard == __TRUE)	&& (GUI_data_nav.Cancel_operation == __TRUE)) 
						{
							Init_STACK(wiz_nav_stack);
							GUI_data_nav.Back_one_level = 0;
							GUI_data_nav.In_wizard = __FALSE;
							GUI_data_nav.Wizard_screen_index = __FALSE;
							GUI_data_nav.Cancel_operation = __FALSE;
							GUI_data_nav.Setup_wizard = __FALSE;
							// If we Initiate Zero calibration through ZERO Key then pressing cancel should bring to Runmode screen

							current_screen_org_no_main = screen_data_create(GUI_data_nav.wizard_screen->Cancel);
							GUI_data_nav.wizard_screen = NULL;
							screen_to_be_redrawn = __TRUE;

						}
					} //child_present
					else {
						GUI_data_nav.Child_present = __FALSE;
					}
					GUI_data_nav.Key_press = 0;
				} //if (Key_press == __TRUE)

				
			
		}
	
		}
	LPC_RTC->GPREG4 &= ~(0x01<<10);
//		WDTFeed();
}
}

void Disp_time(void)
{
    char disp[100];	
    memset(disp, '\0', sizeof(disp));	
		sprintf(&disp[0], "%02d/%02d/%04d %02d:%02d:%2d "	,LPC_RTC->MONTH,\
                      LPC_RTC->DOM,LPC_RTC->YEAR,LPC_RTC->HOUR,\
                      LPC_RTC->MIN,LPC_RTC->SEC	);
    GUI_SetTextAlign(GUI_TA_TOP | GUI_TA_RIGHT);
    GUI_DispStringAt(disp, WINDOW_HEADER_TEXT3_POS_X0, WINDOW_HEADER_TEXT3_POS_Y0);
	
}

void screen_variables_init(void)
{
//	U8 i = 0;

	init_EEP_conf_vars();
}

void init_EEP_conf_vars(void) 
{
	init_Pcm_var();
}

void init_Pcm_var(void) 
{
	Pcm_var.PCM_id = 40;
	Pcm_var.DHCP_Status = Screen22_str15;
	Pcm_var.FourG_IP_Addr.Addr1 = 192;
	Pcm_var.FourG_IP_Addr.Addr2 = 168;
	Pcm_var.FourG_IP_Addr.Addr3 = 1;
	Pcm_var.FourG_IP_Addr.Addr4 = 1;
	
	sprintf(&LTE_host_name[0], "http://%d.%d.%d.%d",Pcm_var.FourG_IP_Addr.Addr1,Pcm_var.FourG_IP_Addr.Addr2,\
									Pcm_var.FourG_IP_Addr.Addr3,Pcm_var.FourG_IP_Addr.Addr4);
	
	Pcm_var.IP_Addr.Addr1 = 192;
	Pcm_var.IP_Addr.Addr2 = 168;
	Pcm_var.IP_Addr.Addr3 = 1;
	Pcm_var.IP_Addr.Addr4 = 100;	
	Pcm_var.Gateway.Addr1 = 192;
	Pcm_var.Gateway.Addr2 = 168;
	Pcm_var.Gateway.Addr3 = 1;
	Pcm_var.Gateway.Addr4 = 1;
	Pcm_var.Subnet_Mask.Addr1 = 255;
	Pcm_var.Subnet_Mask.Addr2 = 255;
	Pcm_var.Subnet_Mask.Addr3 = 255;
	Pcm_var.Subnet_Mask.Addr4 = 0;
	
	Pcm_var.PriDNS.Addr1 = 8;
	Pcm_var.PriDNS.Addr2 = 8;
	Pcm_var.PriDNS.Addr3 = 8;
	Pcm_var.PriDNS.Addr4 = 8;
	
	Pcm_var.SecDNS.Addr1 = 8;
	Pcm_var.SecDNS.Addr2 = 8;
	Pcm_var.SecDNS.Addr3 = 4;
	Pcm_var.SecDNS.Addr4 = 4;	
	
	Pcm_var.Http_Port_Number = 80;
	Pcm_var.PCM_web_select= Screen212_str3;
//	strcpy(&Pcm_var.Http_host_addr[0], "http://plantconnect-api.azurewebsites.net");
	Pcm_var.PCM_connection_select=Screen213_str1;
	Pcm_var.PPP_Status = Screen24_str10;
	strcpy(&Pcm_var.PPP_dial_no[0],"8156255573");
	Pcm_var.DST_Status = Screen25_str8;
	Pcm_var.GMT_value = -5.0;
	strcpy(&Pcm_var.Sntp_host_addr[0],"0.us.pool.ntp.org");
	strcpy(&Pcm_var.Int_firmware_version[0], FirmwareVer);
	strcpy(&Pcm_var.Int_update_version[0], FirmwareVer);	
	Pcm_var.Current_Date_Format = MMDDYYYY;
	Pcm_var.Current_Time_Format = TWENTYFOUR_HR;	
	memset(Pcm_var.Int_update_file_name, 0,
	sizeof(Pcm_var.Int_update_file_name));	
}
void init_Admin_var(void) {

//	Pcm_var.DHCP_Status = 1;
	Admin_var.Gateway.Addr1 = 192;
	Admin_var.Gateway.Addr2 = 168;
	Admin_var.Gateway.Addr3 = 1;
	Admin_var.Gateway.Addr4 = 1;
	strcpy(&Admin_var.Int_firmware_version[0], FirmwareVer);
	strcpy(&Admin_var.Int_update_version[0], FirmwareVer);
	Admin_var.IP_Addr.Addr1 = 192;
	Admin_var.IP_Addr.Addr2 = 168;
	Admin_var.IP_Addr.Addr3 = 1;
	Admin_var.IP_Addr.Addr4 = 100;


	strcpy(Admin_var.Scale_Name, "PCM Ver0.4");


	Admin_var.Subnet_Mask.Addr1 = 255;
	Admin_var.Subnet_Mask.Addr2 = 255;
	Admin_var.Subnet_Mask.Addr3 = 255;
	Admin_var.Subnet_Mask.Addr4 = 0;

	Admin_var.MBS_TCP_slid = 0;
	strcpy(Admin_var.New_Password, "NEW PASSWORD");
	strcpy(Admin_var.Verify_New_Password, "VERIFY PASSWORD");
	memset(Admin_var.Int_update_file_name, 0,
			sizeof(Admin_var.Int_update_file_name));
	//---------added on 11th september 2014

//		Admin_var.Language_select = Screen61B1_str0;
	strcpy(&Admin_var.Int_firmware_version[0], FirmwareVer);
	strcpy(&Admin_var.Int_update_version[0], FirmwareVer);

//	CalcCRC((unsigned char*) &Admin_var, (unsigned char*) &Admin_var.CRC_ADMIN,
//			(unsigned int) offsetof(ADMIN_STRUCT, CRC_ADMIN));
}

#endif
/*****************************************************************************
* End of file
*****************************************************************************/



