/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen5_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN5_DATA
#define  SCREEN5_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

__align(4) SCREEN_TYPE5_DATA Org_211000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen21_str2, 0,  &Pcm_var.PCM_id, NULL, unsigned_char_type1, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};
__align(4) SCREEN_TYPE5_DATA Org_222000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen22_str4, 0,  &Pcm_var.IP_Addr, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_223000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen22_str6, 0,  &Pcm_var.Subnet_Mask, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_224000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen22_str8, 0,  &Pcm_var.Gateway, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};
__align(4) SCREEN_TYPE5_DATA Org_226000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen22_str10, 0,  &Pcm_var.PriDNS, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};
__align(4) SCREEN_TYPE5_DATA Org_227000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen22_str12, 0,  &Pcm_var.SecDNS, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};
__align(4) SCREEN_TYPE5_DATA Org_243000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen24_str12, 0,  &Pcm_var.FourG_IP_Addr, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},	
};


__align(4) SCREEN_TYPE5_DATA Org_231000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen23_str2, 0,  &Pcm_var.Http_Port_Number, NULL, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};
__align(4) SCREEN_TYPE5_DATA Org_252000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen25_str4, 0,  &Pcm_var.GMT_value, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};
// __align(4) SCREEN_TYPE5_DATA Org_242000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
//     {Screen24_str4, 0,  &Pcm_var.PPP_dial_no,NULL,unsigned_char_type, 0},
//     {Current_value_string, 0, NULL, NULL, no_variable, 0},
// };
	
// __align(4) SCREEN_TYPE5_DATA Org_312000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
//     {Screen31_str10, 0,  NULL, NULL, NULL, 0},
//     {Current_value_string, 0, NULL, NULL, NULL, 0},
// };



__align(4) SCREEN_TYPE5_DATA Org_231100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen2311_str1, Screen2311_str2,  &Scale_setup_var.Custom_LC_capacity,  &Scale_setup_var.Custom_LC_unit,
      unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_231300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen2313_str1, Screen2313_str2,  &Scale_setup_var.Custom_LC_output, NULL, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_272000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen271_str1,  Screen271_str2,   &Scale_setup_var.Conveyor_angle, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_281000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen281_str1, Screen281_str2,   &Scale_setup_var.Idler_distanceA,  &Scale_setup_var.Idler_unit, float_type1, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_282000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen282_str1, Screen282_str2,   &Scale_setup_var.Idler_distanceB,  &Scale_setup_var.Idler_unit, float_type1, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_283000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen283_str1, Screen283_str2,   &Scale_setup_var.Idler_distanceC,  &Scale_setup_var.Idler_unit, float_type1, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_284000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen284_str1, Screen284_str2,   &Scale_setup_var.Idler_distanceD,  &Scale_setup_var.Idler_unit, float_type1, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_285000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen285_str1, Screen285_str2,  &Scale_setup_var.Idler_distanceE,  &Scale_setup_var.Idler_unit, float_type1, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_291000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen29_str1, Screen291_str1,  &Scale_setup_var.Wheel_diameter,  &Scale_setup_var.Wheel_diameter_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_292000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen29_str4, Screen291_str1,  &Scale_setup_var.Pulses_Per_Revolution, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_293100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen2931_str1, Screen2931_str2,  &Scale_setup_var.User_Belt_Speed,  &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

  __align(4) SCREEN_TYPE5_DATA Org_312000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen31_str9, Screen31_str10,  &Pcm_var.Config_Date,  NULL, date_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
 }; 

  __align(4) SCREEN_TYPE5_DATA Org_321000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen32_str1, Screen32_str2,  &Pcm_var.Config_Time,  NULL, time_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
 };  
 
// __align(4) SCREEN_TYPE5_DATA Org_320000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen321_str1, Screen321_str2,  &Calibration_var.Belt_scale_weight,  &Calibration_var.Belt_scale_unit, float_type, 0},
//     {Current_value_string, 0, NULL, NULL, no_variable, 0},
// };

__align(4) SCREEN_TYPE5_DATA Org_322000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen323_str1, Screen323_str2,  &Calibration_var.Cert_scale_weight,  &Calibration_var.Cert_scale_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_343100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen3431_str1, Screen3431_str2,  &Calibration_var.New_belt_length,  &Scale_setup_var.Belt_length_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_421100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4211_str1, Screen4211_str2,  &Setup_device_var.Scoreboard_alt_delay, NULL, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_432100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4321_str1, Screen4321_str2,  &Setup_device_var.Periodic_log_interval, NULL, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442112_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442112_str1, Screen442112_str2,  &Setup_device_var.Pulse_on_time_1,  &Units_var.Unit_pulse_on_time_str, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442141_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442141_str1, Screen442141_str2,  &Setup_device_var.MinMax_Speed_Setpoint_1,   &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442151_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442151_str1, Screen442151_str2,  &Setup_device_var.MinMax_Rate_Setpoint_1,  &Units_var.Unit_rate, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442212_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442112_str1, Screen442112_str2,  &Setup_device_var.Pulse_on_time_2, &Units_var.Unit_pulse_on_time_str, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442241_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442141_str1, Screen442141_str2,  &Setup_device_var.MinMax_Speed_Setpoint_2, &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442251_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442151_str1, Screen442151_str2,  &Setup_device_var.MinMax_Rate_Setpoint_2, &Units_var.Unit_rate, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442312_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442112_str1, Screen442112_str2,  &Setup_device_var.Pulse_on_time_3, &Units_var.Unit_pulse_on_time_str, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442341_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442141_str1, Screen442141_str2,  &Setup_device_var.MinMax_Speed_Setpoint_3, &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_442351_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442151_str1, Screen442151_str2,  &Setup_device_var.MinMax_Rate_Setpoint_3, &Units_var.Unit_rate, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_443121_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442141_str1, Screen443121_str1,  &Setup_device_var.MinMax_Speed_Setpoint_4,  &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_443131_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442151_str1, Screen443131_str1,  &Setup_device_var.MinMax_Rate_Setpoint_4, &Units_var.Unit_rate, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_443221_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442141_str1, Screen443121_str1,  &Setup_device_var.MinMax_Speed_Setpoint_5,  &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_443231_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442151_str1, Screen443131_str1,  &Setup_device_var.MinMax_Rate_Setpoint_5, &Units_var.Unit_rate, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_443321_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442141_str1, Screen443121_str1,  &Setup_device_var.MinMax_Speed_Setpoint_6,  &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_443331_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen442151_str1, Screen443131_str1,  &Setup_device_var.MinMax_Rate_Setpoint_6, &Units_var.Unit_rate, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_444120_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen44412_str1, Screen44412_str2,  &Setup_device_var.Analog_Output_1_Setpoint, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_444130_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen44413_str1, Screen44413_str2,  &Setup_device_var.Analog_Output_1_Maxrate, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_444140_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen44414_str1, Screen44414_str2,  &Setup_device_var.Analog_Output_1_ZeroCal, NULL, float_type2, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_444220_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen44422_str1, Screen44422_str2,  &Setup_device_var.Analog_Output_2_Setpoint, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_444230_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen44423_str1, Screen44423_str2,  &Setup_device_var.Analog_Output_2_Maxrate, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_444240_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen44424_str1, Screen44424_str2,  &Setup_device_var.Analog_Output_2_ZeroCal, NULL, float_type2, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_451100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4511_str1, Screen4511_str2,  &Setup_device_var.PID_Channel, NULL, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_451310_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen45131_str1, Screen45131_str2,  &Setup_device_var.PID_Local_Setpoint, &Units_var.Unit_rate, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};
__align(4) SCREEN_TYPE5_DATA Org_451320_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen45133_str1, Screen45133_str2,  &Setup_device_var.Analog_Input_Maxrate, &Units_var.Unit_rate, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_451400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen45141_str1, Screen45141_str2,  &Setup_device_var.PID_P_Term, NULL, unsigned_int_type, 0},
    {Screen45142_str1, Screen45142_str2,  &Setup_device_var.PID_I_Term, NULL, unsigned_int_type, 0},
    {Screen45143_str1, Screen45143_str2,  &Setup_device_var.PID_D_Term, NULL, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4521_str1,  Screen4521_str2,   &Setup_device_var.Load_weight_1,  &Scale_setup_var.Weight_unit, float_type2, 0},
    {Screen4521_str3,  Screen4521_str4,   &Setup_device_var.Cutoff_1,       &Scale_setup_var.Weight_unit, float_type2, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4522_str1,  Screen4522_str2,   &Setup_device_var.Load_weight_2,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4522_str3,  Screen4522_str4,   &Setup_device_var.Cutoff_2,       &Scale_setup_var.Weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4523_str1,  Screen4523_str2,   &Setup_device_var.Load_weight_3,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4523_str3,  Screen4523_str4,   &Setup_device_var.Cutoff_3,       &Scale_setup_var.Weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4524_str1,  Screen4524_str2,   &Setup_device_var.Load_weight_4,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4524_str3,  Screen4524_str4,   &Setup_device_var.Cutoff_4,       &Scale_setup_var.Weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452500_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4525_str1,  Screen4525_str2,   &Setup_device_var.Load_weight_5,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4525_str3,  Screen4525_str4,   &Setup_device_var.Cutoff_5,       &Scale_setup_var.Weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452600_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4526_str1,  Screen4526_str2,   &Setup_device_var.Load_weight_6,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4526_str3,  Screen4526_str4,   &Setup_device_var.Cutoff_6,       &Scale_setup_var.Weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452700_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4527_str1,  Screen4527_str2,   &Setup_device_var.Load_weight_7,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4527_str3,  Screen4527_str4,   &Setup_device_var.Cutoff_7,       &Scale_setup_var.Weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_452800_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4528_str1,  Screen4528_str2,   &Setup_device_var.Load_weight_8,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4528_str3,  Screen4528_str4,   &Setup_device_var.Cutoff_8,       &Scale_setup_var.Weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

#if 0
__align(4) SCREEN_TYPE5_DATA Org_452900_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4529_str1,  Screen4529_str2,   &Setup_device_var.Load_weight_9,  &Scale_setup_var.Weight_unit, float_type, 0},
    {Screen4529_str3,  Screen4529_str4,   &Setup_device_var.Cutoff_9,       &Scale_setup_var.Weight_unit, float_type, 0},
};
#endif

__align(4) SCREEN_TYPE5_DATA Org_453100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen453_str1, Screen4531_str2,  &Setup_device_var.Network_Addr, NULL, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};


__align(4) SCREEN_TYPE5_DATA Org_453200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen453_str5, 0,  &Setup_device_var.Percent_Ingredient, NULL, float_type2, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_453300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen453_str7, Screen4534_str2,  &Setup_device_var.Feed_Delay,  &Units_var.Unit_delay_secs, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_453400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen453_str9, Screen4535_str2,  &Setup_device_var.Preload_Delay,  &Units_var.Unit_delay_secs, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_453500_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen453_str11, Screen4536_str2,  &Setup_device_var.Feeder_Max_Rate, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_454100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4541_str1, Screen4541_str2,  &Setup_device_var.Target_rate,  &Units_var.Unit_rate, float_type2, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_454200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen453_str9, Screen4535_str2,  &Setup_device_var.Preload_Delay,  &Units_var.Unit_delay_secs, unsigned_int_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_455100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4551_str1, Screen4551_str2,  &Setup_device_var.Target_Load, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_455200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4552_str1, Screen4552_str2,  &Setup_device_var.Min_Belt_Speed,   &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_455300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4553_str1, Screen4553_str2,  &Setup_device_var.Preload_Distance,  &Scale_setup_var.Belt_length_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_455400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen4554_str1, Screen4554_str2,  &Setup_device_var.Empty_Belt_Speed,  &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_614100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen6141_str1, Screen6141_str2,  &Admin_var.Config_Date, NULL, date_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_541000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen54_str0, Screen54_str1,  &Rprts_diag_var.Calib_err_pct, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_615100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen6151_str1, Screen6151_str2,  &Admin_var.Config_Time, NULL, time_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_616000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen616_str1, Screen616_str2,  &Admin_var.Auto_zero_tolerance, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_617200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen617_str4, Screen617_str5,  &Admin_var.Zero_rate_limit, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_618100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen618_str2, Screen618_str3,  &Admin_var.Negative_rate_limit, NULL, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_618200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen618_str4, Screen618_str5,  &Admin_var.Negative_rate_time, &Units_var.Unit_delay_secs, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_651000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen651_str1, 0,  &Calibration_var.new_span_value, NULL, float_type6, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_652000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen652_str1, 0,  /*&Calculation_struct.Zero_weight*/&Calibration_var.New_zero_value,  &Calibration_var.Test_zero_weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_653000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen653_str1, 0,  &Calibration_var.New_belt_length,  &Scale_setup_var.Belt_length_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_662000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen662_str1, Screen662_str2,  &Admin_var.IP_Addr, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_663000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen663_str1, Screen663_str2,  &Admin_var.Subnet_Mask, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_664000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen664_str1, 0,  &Admin_var.Gateway, NULL, ip_struct, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

//PVK 12 May 2016 - changed unsigned_int_type to unsigned_char_type1 
__align(4) SCREEN_TYPE5_DATA Org_665000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    //{Screen66500_str2, Screen66500_str3,  &Admin_var.MBS_TCP_slid, NULL, unsigned_int_type, 0},
	  {Screen66500_str2, Screen66500_str3,  &Admin_var.MBS_TCP_slid, NULL, unsigned_char_type1, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_61A110_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen61A11_str1, Screen61A11_str2,  &Admin_var.Test_speed, &Units_var.Unit_speed, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE5_DATA Org_61A210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen61A21_str1, Screen61A21_str2,  &Admin_var.Test_load, &Calibration_var.Test_zero_weight_unit, float_type, 0},
    {Current_value_string, 0, NULL, NULL, no_variable, 0},
};


/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
