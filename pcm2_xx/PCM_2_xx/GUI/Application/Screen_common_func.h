/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen_common_func.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef __SCREEN_COMMON_FUNC_H
#define __SCREEN_COMMON_FUNC_H

#if defined(__cplusplus)
extern "C" {     /* Make sure we have C-declarations in C++ programs */
#endif

#include "../GUI.h"
#include "Screen_structure.h"

#if GUI_WINSUPPORT
  #include "../WM.h"
  #include "../PROGBAR.h"
  #include "../TEXT.h"
  #include "../BUTTON.h"
  #include "../ICONVIEW.h"
#endif

#if defined(__cplusplus)
  }
#endif

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern Screen_org_data * screen_data_create (char screen_org_num[6]);
extern void screen_display (Screen_org_data * current_screen_org_no);

extern void Display_Header(int Display_Text_Once);
extern void Display_Footer(int Display_Text_Once);
extern void Convert_Time(void);
extern unsigned int getSpeed_unit(void);
extern unsigned int getRate_unit(void);
extern void string_of_Weight(char *buf, double *value);
/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif // end of __SCREEN_COMMON_FUNC_H
/*****************************************************************************
* End of file
*****************************************************************************/
