/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen_data_var.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : July Thursday, 2012  <July 12, 2012>
* @date Last Modified  : July Thursday, 2012  <July 12, 2012>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef  SCREEN_DATA_VAR
#define  SCREEN_DATA_VAR

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stddef.h>
#include <string.h>
#include "rtc.h"
#include "Screen_data_var.h"
#include "Screen_structure.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

#define INT_FIRM_VER_NOC				6
#define INT_UPD_VER_NOC 				6
#define SCALE_NAME_NOC 					24
#define PLANT_NAME_NOC 					24
#define PRODUCT_NAME_NOC 				24
#define PASSWD_NOC 							15
#define NEW_PASSWD_NOC 					15
#define VERIFY_PASSWD_NOC 			15
#define INT_UPD_FILE_NAME_NOC 	30
#define RELOAD_FILE_NAME_NOC 		30
#define BACKUP_FILE_NAME_NOC 		30
#define CONTACT_LEN             24
#define HTTP_HOST_NOC						42
#define	PPP_DIAL_LEN						11
#define PPP_USER_NAME_NOC				16
#define	PPP_PASSWORD_NOC				7
#define SNTP_HOST_NOC						18
#define VERSION_NOC							6

#define ADMIN_TOTAL_NOC         (INT_FIRM_VER_NOC + INT_UPD_VER_NOC + SCALE_NAME_NOC + PLANT_NAME_NOC +PRODUCT_NAME_NOC + PASSWD_NOC + NEW_PASSWD_NOC + VERIFY_PASSWD_NOC + INT_UPD_FILE_NAME_NOC + RELOAD_FILE_NAME_NOC + BACKUP_FILE_NAME_NOC)
/*============================================================================
* Public Data Types
*===========================================================================*/
typedef struct
                {
										unsigned int Language_select;
								}MISC_STRUCT;
typedef struct
                {
									  DATE_STRUCT Config_Date;
                    TIME_STRUCT Config_Time;
                    IP_STRUCT IP_Addr;
                    IP_STRUCT Subnet_Mask;
                    IP_STRUCT Gateway;
                    float Auto_zero_tolerance;
                    float Negative_rate_limit;
                    float Negative_rate_time;
                    float Zero_rate_limit;
                    char Int_firmware_version[INT_FIRM_VER_NOC];
                    char Int_update_version[INT_UPD_VER_NOC];
                    char Scale_Name[SCALE_NAME_NOC];
                    char Plant_Name[PLANT_NAME_NOC];
                    char Product_Name[PRODUCT_NAME_NOC];
                    char Password[PASSWD_NOC];
                    char New_Password[NEW_PASSWD_NOC];
                    char Verify_New_Password[VERIFY_PASSWD_NOC];
                    char Int_update_file_name[INT_UPD_FILE_NAME_NOC];
                    char Reload_file_name[RELOAD_FILE_NAME_NOC];
                    char Backup_file_name[BACKUP_FILE_NAME_NOC];
                    unsigned int Admin_Locked;
                    unsigned int AM_PM;
                    unsigned int Calibration_Locked;
                    unsigned int Clear_Weight_Locked;
                    unsigned int Current_Time_Format;
                    unsigned int Current_Date_Format;
                    unsigned int Day_of_week;
                    unsigned int DHCP_Status;
                    unsigned int Setup_Devices_Locked;
                    unsigned int Setup_Wizard_Locked;
                    unsigned int Zero_Calibration_Locked;
                    unsigned int Zero_rate_status;
										U8 MBS_TCP_slid;
                    unsigned int Test_speed_status; //------------added on 11th september 2014
                    unsigned int Test_load_status; //------------added on 11th september 2014
                    float Test_load; //----added on 11th september 2014
                    float Test_speed; //----added on 11th september 2014
										
										U16 CRC_ADMIN;
                }ADMIN_STRUCT;


typedef struct
               {
                    float Angle_Sensor;
                    float Load_cell_supply;
                    float Load_Cell_1_Output;
                    float Load_Cell_2_Output;
                    float Load_Cell_3_Output;
                    float Load_Cell_4_Output;
                    float Load_Cell_5_Output;
                    float Load_Cell_6_Output;
                    float Load_Cell_7_Output;
                    float Load_Cell_8_Output;
                    float Speed_Sensor;
                    float Five_volt_supply;
                    unsigned int Analog_Output_1_Status;
                    unsigned int Analog_Output_2_Status;
                    unsigned int Digital_Input_1_Status;
                    unsigned int Digital_Input_2_Status;
                    unsigned int Digital_Input_3_Status;
                    unsigned int Digital_Input_4_Status;
                    unsigned int Digital_Output_1_Status;
                    unsigned int Digital_Output_2_Status;
                    unsigned int Digital_Output_3_Status;
                    unsigned int Ethernet_Status;     //"Integrator to Remote"
                    unsigned int PID_Set_Rate;
                    unsigned int Relay_Output_1_Status;
                    unsigned int Relay_Output_2_Status;
                    unsigned int Relay_Output_3_Status;
                    unsigned int RS485_1_Status;      //"Sensor to Integrator"
                    unsigned int RS485_2_Status;      //"Integrator to Integrator"
                    unsigned int RS232_1_Status;      //"Printer"
                    unsigned int RS232_2_Status;      //"Scoreboard Display"
                    unsigned int SPI_Status;        //"Integrator to I/O"
										unsigned int Speed_In_Hz;
										double daily_Total_weight;     // dailly accumulated weight
										double weekly_Total_weight;    // weeky accumulated weight
										double monthly_Total_weight;   // monthly accumulated weight
										double yearly_Total_weight;    // Yearly accumulated weight
										double Job_Total;              // An independent total which is relaed to a perticular job
							      double Master_total;           // Lift Time total.
                    float Calib_err_pct; //------------added on 11th september 2014
                    float Live_weight_disp; //------------added on 11th september 2014
										float Tolerance_err;	//-------------added on 5th November 2014
										double LastClearedTotal[8];		//Added 2-Jan-15 for 8 last clears
										float Avg_rate_for_mode[8];		//Added 2-Jan-15 for 8 last clears
										float RunTime[8];							//Added 2-Jan-15 for 8 last clears
										DATE_STRUCT Clear_Date[8];		//Added 2-Jan-15 for 8 last clears
										TIME_STRUCT Clear_Time[8];		//Added 2-Jan-15 for 8 last clears
										unsigned int LastCleared_Weight_unit[8];	//Added on 17th Nov 2015
                    unsigned int LastCleared_Rate_time_unit[8];	//Added on 17th Nov 2015
										float Trim_live_weight_disp; //Added on 30th september 2015
                }RPRTS_DIAG_STRUCT;


typedef struct
                {
                    //------analog output-----
                    float Analog_Output_1_Setpoint;
                    float Analog_Output_1_Maxrate;
                    float Analog_Output_1_ZeroCal;
                    float Analog_Output_2_Setpoint;
                    float Analog_Output_2_Maxrate;
                    float Analog_Output_2_ZeroCal;
									  float Analog_Input_Maxrate;
                    //--preset cutoff
                    float Cutoff_1;
                    float Cutoff_2;
                    float Cutoff_3;
                    float Cutoff_4;
                    float Cutoff_5;
                    float Cutoff_6;
                    float Cutoff_7;
                    float Cutoff_8;
                    //---preset load
                    float Load_weight_1;
                    float Load_weight_2;
                    float Load_weight_3;
                    float Load_weight_4;
                    float Load_weight_5;
                    float Load_weight_6;
                    float Load_weight_7;
                    float Load_weight_8;
                    //----min max rate setpoint-----
                    float MinMax_Rate_Setpoint_1;
                    float MinMax_Rate_Setpoint_2;
                    float MinMax_Rate_Setpoint_3;
                    float MinMax_Rate_Setpoint_4;
                    float MinMax_Rate_Setpoint_5;
                    float MinMax_Rate_Setpoint_6;
                    //--min max speed setpoint-----
                    float MinMax_Speed_Setpoint_1;
                    float MinMax_Speed_Setpoint_2;
                    float MinMax_Speed_Setpoint_3;
                    float MinMax_Speed_Setpoint_4;
                    float MinMax_Speed_Setpoint_5;
                    float MinMax_Speed_Setpoint_6;
                    float Min_Belt_Speed;
                    float Empty_Belt_Speed;
                    float Feeder_Max_Rate;
                    float Preload_Distance;
                    float Target_rate;
                    float Target_Load;
										float Analog_Output1;
										float Analog_Output2;
                    //------------------------------
                    //----analog output-----
                    unsigned int Analog_Output_1_Function;
                    unsigned int Analog_Output_2_Function;
                    //-------IO - inputs ------
                    unsigned int Digital_Input_1_Function;
                    unsigned int Digital_Input_2_Function;
                    unsigned int Digital_Input_3_Function;
                    unsigned int Digital_Input_4_Function;
                    //------IO - outputs ------
                    unsigned int Digital_Output_1_Function;
                    unsigned int Digital_Output_2_Function;
                    unsigned int Digital_Output_3_Function;
                    //---error alarm-------
                    unsigned int error_alarm_6;
                    unsigned int error_alarm_5;
                    unsigned int error_alarm_4;
                    unsigned int error_alarm_3;
                    unsigned int error_alarm_2;
                    unsigned int error_alarm_1;
                    //-----USB-----
                    unsigned int Log_cal_data;
                    unsigned int Log_run_time_data;
                    unsigned int Log_err_data;
                    unsigned int Log_zero_cal_data;
                    unsigned int Log_clear_wt_data;
                    unsigned int PID_Action;
                    unsigned int PID_Setpoint_Type;
                    unsigned int Printer_flow_control;
                    unsigned int Printer_status;
                    unsigned int Printer_type;
                    // rate output types
                    unsigned int Rate_Output_Type_1;
                    unsigned int Rate_Output_Type_2;
                    unsigned int Rate_Output_Type_3;
                    unsigned int Rate_Output_Type_4;
                    unsigned int Rate_Output_Type_5;
                    unsigned int Rate_Output_Type_6;
                    //----relay---
                    unsigned int Relay_Output_1_Function;
                    unsigned int Relay_Output_2_Function;
                    unsigned int Relay_Output_3_Function;
                    //speed output types
                    unsigned int Speed_Output_Type_1;
                    unsigned int Speed_Output_Type_2;
                    unsigned int Speed_Output_Type_3;
                    unsigned int Speed_Output_Type_4;
                    unsigned int Speed_Output_Type_5;
                    unsigned int Speed_Output_Type_6;
                    unsigned int Scoreboard_display_data;
                    unsigned int Scoreboard_type;
										unsigned int Scoreboard_TMMode;
                    unsigned int Units_Per_Pulse_1;
                    unsigned int Units_Per_Pulse_2;
                    unsigned int Units_Per_Pulse_3;
										//------------------------------
										//----blending setup----
                    unsigned int Feed_Delay;
                    unsigned int Network_Addr;
                    //unsigned int Percent_Ingredient;
										float Percent_Ingredient;
                    unsigned int Preload_Delay;
                    //----- periodic log interval-----
                    unsigned int Periodic_log_interval;
                    //----PID----
                    unsigned int PID_Channel;
                    unsigned int PID_P_Term;
                    unsigned int PID_I_Term;
                    unsigned int PID_D_Term;
                    float PID_Local_Setpoint;
                    //-----printer
                    unsigned int Printer_ticket_num;
                    unsigned int Printer_baud;
                    unsigned int Printer_data_bits;
                    unsigned int Printer_stop_bits;
                    //pulse on times
                    unsigned int Pulse_on_time_1;
                    unsigned int Pulse_on_time_2;
                    unsigned int Pulse_on_time_3;
                    //-----scoreboard
                    unsigned int Scoreboard_alt_delay;
                    unsigned int Scoreboard_baud;
                    unsigned int Scoreboard_data_bits;
                    unsigned int Scoreboard_stop_bits;
                    //------------------------------
                    char Addr1[CONTACT_LEN];
                    char Addr2[CONTACT_LEN];
                    char Addr3[CONTACT_LEN];
                    char Company_name[CONTACT_LEN];
                    char Phone[CONTACT_LEN];
										U16 CRC_SETUP_DEVICES;
                }SETUP_DEVICES_STRUCT;

typedef struct
                {
                    float Test_weight;
                    float Belt_scale_weight;
                    float Cert_scale_weight;
                    float Old_belt_length;
                    float New_belt_length;
                    float Belt_length_diff;
                    float Old_zero_value;
                    float New_zero_value;
                    float Zero_diff;
                    float old_span_value;
                    float new_span_value;
                    float span_diff;
                    float real_time_rate;
                    unsigned int Belt_scale_unit;
                    unsigned int Cert_scale_unit;
                    unsigned int Test_zero_weight_unit;
										U16 CRC_CALIBRATION_STRUCT;
                }CALIBRATION_STRUCT;

typedef struct
                {
                  float Conveyor_angle;
                    float Idler_distance;
                    float Wheel_diameter;
                    float Pulses_Per_Revolution;
                    float User_Belt_Speed;
                    unsigned int Run_mode;
                    unsigned int Load_cell_size;
                    unsigned int Distance_unit;
                    unsigned int Weight_unit;
                    unsigned int Rate_time_unit;
                    unsigned int Custom_LC_unit;
                    unsigned int Idler_unit;
                    unsigned int Wheel_diameter_unit;
                    unsigned int Belt_length_unit;
                    unsigned int Speed_unit;
										unsigned int AngleSensor_Install_status;
                    unsigned int Number_of_idlers;
                    unsigned int Custom_LC_capacity;
                    unsigned int Custom_LC_output;
										unsigned int Decimal_digits;   // Number of Decimal digits for Display of Weight.
									  unsigned int IO_board_install; // IO board installation status.
										float Idler_distanceA;
                    float Idler_distanceB;
                    float Idler_distanceC;
                    float Idler_distanceD;
                    float Idler_distanceE;
										float User_Belt_Speeed_WD;
										U16 CRC_SCALE_SETUP;
                }SCALE_SETUP_STRUCT;
typedef struct { // This structure contains unit strings used in display.
									unsigned int  Unit_pulse_on_time_str;
	                unsigned int  Unit_speed_time_str;
	                unsigned int  Unit_speed;
	                unsigned int  Unit_rate;
	                unsigned int  Unit_delay_secs;
               }UNITS_STRUCT;

typedef struct { //All totals for internal use
										double daily_Total_weight;     // dailly accumulated weight
										double weekly_Total_weight;    // weeky accumulated weight
										double monthly_Total_weight;   // monthly accumulated weight
										double yearly_Total_weight;    // Yearly accumulated weight
										double Job_Total;              // An independent total which is relaed to a perticular job
							      double Master_total;           // Lift Time total.
										unsigned int Weight_unit;
							 }TOTALS_STRUCT;


typedef struct
                {
										U8 PCM_id;
										unsigned int PCM_web_select;
										unsigned int PCM_connection_select;
                    IP_STRUCT IP_Addr;
                    IP_STRUCT Subnet_Mask;
                    IP_STRUCT Gateway;	
										IP_STRUCT PriDNS;
										IP_STRUCT SecDNS;
										unsigned int Http_Port_Number;
//										char Http_host_addr[HTTP_HOST_NOC];
										unsigned int PPP_Status;
										char PPP_dial_no[PPP_DIAL_LEN]; 
										unsigned int DST_Status;
										float GMT_value;
										char Sntp_host_addr[SNTP_HOST_NOC];
									  DATE_STRUCT Config_Date;
                    TIME_STRUCT Config_Time;	
                    unsigned int Current_Date_Format;
										unsigned int Current_Time_Format;	
										IP_STRUCT FourG_IP_Addr;
										unsigned int DHCP_Status;
										char Int_firmware_version[VERSION_NOC];
                    char Int_update_version[INT_UPD_VER_NOC];	
                    char Int_update_file_name[INT_UPD_FILE_NAME_NOC];
										 
										U16 CRC_PCM_VAR;
								}PCM_STRUCT;

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/

