/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen2_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN2_DATA
#define  SCREEN2_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "File_update.h"
#include "Screen_global_ex.h"
#include "main.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
extern char IOboard_fw_ver[6];

unsigned char PCM_website[41] = {"http://plantconnect-api.azurewebsites.net"};
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
 __align(4) SCREEN_TYPE2_DATA Org_210000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
     {Screen21_str1,  Screen21_str2,  NULL,       0, &Pcm_var.PCM_id,
         unsigned_char_type1,       0, 1},
      {Screen21_str3,  Screen21_str4, &Pcm_var.PCM_web_select,NULL ,                            NULL, 
         no_variable, 0, 2},
      {Screen21_str5,  Screen21_str6, &Pcm_var.PCM_connection_select,NULL ,                            NULL, 
         no_variable, 0, 3},				 
 };
 __align(4) SCREEN_TYPE2_DATA Org_220000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
     {Screen22_str1,  Screen22_str2,  &Pcm_var.DHCP_Status, NULL, NULL,no_variable, 0, 1},
     {Screen22_str3,  Screen22_str4,  NULL, NULL, &Pcm_var.IP_Addr,ip_struct, 0, 2},
     {Screen22_str5,  Screen22_str6,  NULL, NULL, &Pcm_var.Subnet_Mask,ip_struct, 0, 3},
     {Screen22_str7,  Screen22_str8,  NULL, NULL, &Pcm_var.Gateway,ip_struct, 0, 4},
    {Screen66_str9,  Str_NULL      ,NULL  ,NULL, Mac_id, char_array,  0, 5},// &Pcm_var.Mac_id,mac_id_struct
     {Screen22_str9,  Screen22_str10,  NULL, NULL, &Pcm_var.PriDNS,ip_struct, 0, 6},
     {Screen22_str11,  Screen22_str12,  NULL, NULL, &Pcm_var.SecDNS,ip_struct, 0, 7},		 
				
 };
 __align(4) SCREEN_TYPE2_DATA Org_230000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
     {Screen23_str1,  Screen23_str2,  NULL, NULL, &Pcm_var.Http_Port_Number,unsigned_int_type, 0, 1},
//     {Screen23_str3,  Screen23_str4,  NULL, NULL, &Pcm_var.Http_host_addr,char_array, 0, 2},
 };
  __align(4) SCREEN_TYPE2_DATA Org_240000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
     {Screen24_str1,  Screen24_str2,  &Pcm_var.PPP_Status, NULL, NULL,no_variable, 0, 1},
     {Screen24_str3,  Screen24_str4,  NULL, NULL, &Pcm_var.PPP_dial_no,char_array, 0, 2},
     {Screen24_str12,  Screen24_str13,  NULL, NULL, &Pcm_var.FourG_IP_Addr,ip_struct, 0, 3},
		 
//     {Screen24_str5,  Screen24_str6,  NULL, NULL, NULL,no_variable, 0, 3},
//     {Screen24_str7,  Screen24_str8,  NULL, NULL, NULL,no_variable, 0, 4},		 
 };
 __align(4) SCREEN_TYPE2_DATA Org_250000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
			{Screen25_str1,  Screen25_str2,  &Pcm_var.DST_Status, NULL, NULL,no_variable, 0, 1},
			{Screen25_str3,  Screen25_str4,  NULL, NULL, &Pcm_var.GMT_value,float_type, 0, 2},
     {Screen25_str5,  Screen25_str6,  NULL, NULL, &Pcm_var.Sntp_host_addr,char_array, 0, 0},

 }; 
  __align(4) SCREEN_TYPE2_DATA Org_300000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
			{Screen3_str0,  Screen3_str1,  NULL, NULL, &Pcm_var.Config_Date,current_date_type, 0, 1},
			{Screen3_str2,  Screen3_str3,  NULL, NULL, &Pcm_var.Config_Time,current_time_type, 0, 2},
     {Screen3_str4,  Screen3_str5,  NULL, NULL, NULL,NULL, 0, 3},
		 {Screen3_str6,  Screen3_str7,  NULL, NULL, NULL,NULL, 0, 4},

 }; 
 
 
  __align(4) SCREEN_TYPE2_DATA Org_310000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
			{Screen31_str1,  Screen31_str2,  &Pcm_var.Current_Date_Format, NULL,NULL,no_variable, 0, 1},
			{Screen31_str9,  Screen31_str10,  NULL, NULL, &Pcm_var.Config_Date,current_date_type, 0, 2},

 };  
 
  __align(4) SCREEN_TYPE2_DATA Org_320000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
			{Screen32_str1,  Screen32_str2,  NULL, NULL, &Pcm_var.Config_Time,current_time_type, 0, 1},


 }; 
 __align(4) SCREEN_TYPE2_DATA Org_332100_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen64131_str1, Screen64131_str2, NULL, NULL,  &Pcm_var.Int_update_file_name, char_array,  0, 0},
};


 
// __align(4) SCREEN_TYPE2_DATA Org_270000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
// 	 {Screen27_M_str1, Screen27_M_str2, &Scale_setup_var.AngleSensor_Install_status, NULL, NULL,
// 		no_variable,      0, 1},
// //	 {Screen27_M_str3, Screen27_M_str4, NULL, NULL, &Scale_setup_var.Conveyor_angle,
// //		float_type,       0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_410000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen41_str1, Screen41_str2,  &Setup_device_var.Printer_status, NULL, NULL,
//         no_variable,       0, 1},
//     {Screen41_str3, Screen41_str4,  &Setup_device_var.Printer_type,   NULL, NULL,
//         no_variable,       0, 2},
//     {Screen41_str5, Screen41_str6,  NULL,                             NULL, NULL,
//         no_variable,       0, 3},
//     {Screen41_str7, Screen41_str8,  NULL,	NULL, &Setup_device_var.Printer_ticket_num,
//         unsigned_int_type,       0, 4},
// 				
// };

// __align(4) SCREEN_TYPE2_DATA Org_412200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen41221_str1, Screen41221_str2, NULL,                                   NULL,
//         &Setup_device_var.Printer_baud,      unsigned_int_type,  0, 1},
//     {Screen41221_str3, Screen41221_str4, NULL,                                   NULL,
//          &Setup_device_var.Printer_data_bits, unsigned_int_type,  0, 2},
//     {Screen41221_str5, Screen41221_str6, NULL,                                   NULL,
//         &Setup_device_var.Printer_stop_bits, unsigned_int_type,  0, 3},
//     {Screen41221_str7, Screen41221_str8, &Setup_device_var.Printer_flow_control, NULL,
//          NULL,                               no_variable,        0, 4},
// };

// __align(4) SCREEN_TYPE2_DATA Org_420000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen42_str1, Screen42_str2, &Setup_device_var.Scoreboard_display_data, NULL, NULL, no_variable, 0, 1},
//     {Screen42_str3, Screen42_str4, &Setup_device_var.Scoreboard_type,         NULL, NULL, no_variable, 0, 2},
// 		{Screen42_str5, Screen42_str6, &Setup_device_var.Scoreboard_TMMode,       NULL, NULL, no_variable, 0, 3},
// };

// __align(4) SCREEN_TYPE2_DATA Org_422210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen42221_str1, Screen42221_str2, NULL, NULL,  &Setup_device_var.Scoreboard_baud,      unsigned_int_type, 0, 1},
//     {Screen42221_str3, Screen42221_str4, NULL, NULL,  &Setup_device_var.Scoreboard_data_bits, unsigned_int_type, 0, 2},
//     {Screen42221_str5, Screen42221_str6, NULL, NULL,  &Setup_device_var.Scoreboard_stop_bits, unsigned_int_type, 0, 3},
// };

// __align(4) SCREEN_TYPE2_DATA Org_430000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen43_str1, Screen43_str2, &Setup_device_var.Log_cal_data,       NULL, NULL, no_variable, 0, 1},
//     {Screen43_str3, Screen43_str2, &Setup_device_var.Log_run_time_data,  NULL, NULL, no_variable, 0, 2},
//     {Screen433_str0, Screen43_str2, &Setup_device_var.Log_err_data,       NULL, NULL, no_variable, 0, 3},
//     {Screen43_str5, Screen43_str2, &Setup_device_var.Log_zero_cal_data,  NULL, NULL, no_variable, 0, 4},
//     {Screen435_str0, Screen43_str2, &Setup_device_var.Log_clear_wt_data,  NULL, NULL, no_variable, 0, 5},
// 		{Screen436_str0,Screen436_str1,NULL,NULL,NULL,no_variable, 0, 6}
// };

// __align(4) SCREEN_TYPE2_DATA Org_441000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen441_str1, Screen441_str2, &Setup_device_var.Digital_Input_1_Function, NULL, NULL, no_variable, 0, 1},
//     {Screen441_str3, Screen441_str4, &Setup_device_var.Digital_Input_2_Function, NULL, NULL, no_variable, 0, 2},
//     {Screen441_str5, Screen441_str6, &Setup_device_var.Digital_Input_3_Function, NULL, NULL, no_variable, 0, 3},
//     {Screen441_str7, Screen441_str8, &Setup_device_var.Digital_Input_4_Function, NULL, NULL, no_variable, 0, 4},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen442_str1, Screen442_str2, &Setup_device_var.Digital_Output_1_Function, NULL, NULL, no_variable, 0, 1},
//     {Screen442_str3, Screen442_str2, &Setup_device_var.Digital_Output_2_Function, NULL, NULL, no_variable, 0, 2},
//     {Screen442_str4, Screen442_str2, &Setup_device_var.Digital_Output_3_Function, NULL, NULL, no_variable, 0, 3},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442110_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44211_str1, Screen44211_str2, NULL, NULL, &GUI_data_nav.Unit_per_pulse_str,
//         char_array,        0, 1},
//     {Screen44211_str3, Screen44211_str2, &Units_var.Unit_pulse_on_time_str, NULL, &Setup_device_var.Pulse_on_time_1,
//         unsigned_int_type, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442140_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44214_str1, Screen44214_str2, &Scale_setup_var.Speed_unit, &Units_var.Unit_speed_time_str,
//         &Setup_device_var.MinMax_Speed_Setpoint_1, float_type,  0, 1},
//     {Screen44214_str3, Screen44214_str4, &Setup_device_var.Speed_Output_Type_1, NULL,
//         NULL,                                      no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442150_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44215_str1, Screen44215_str2, &Scale_setup_var.Weight_unit, &Scale_setup_var.Rate_time_unit,
//         &Setup_device_var.MinMax_Rate_Setpoint_1, float_type, 0, 1},
//     {Screen44215_str3, Screen44215_str4, &Setup_device_var.Rate_Output_Type_1, NULL, NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44211_str1, Screen44211_str2, NULL, NULL, &GUI_data_nav.Unit_per_pulse_str,
//         char_array,        0, 1},
//     {Screen44211_str3, Screen44211_str2,  &Units_var.Unit_pulse_on_time_str,   NULL,
//         &Setup_device_var.Pulse_on_time_2, unsigned_int_type, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442240_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44214_str1, Screen44214_str2,  &Scale_setup_var.Speed_unit,  &Units_var.Unit_speed_time_str,
//         &Setup_device_var.MinMax_Speed_Setpoint_2, float_type, 0, 1},
//     {Screen44214_str3, Screen44214_str4,  &Setup_device_var.Speed_Output_Type_2, NULL, NULL,
//         no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442250_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44215_str1, Screen44215_str2,  &Scale_setup_var.Weight_unit, &Scale_setup_var.Rate_time_unit,
//         &Setup_device_var.MinMax_Rate_Setpoint_2, float_type, 0, 1},
//     {Screen44215_str3, Screen44215_str4,  &Setup_device_var.Rate_Output_Type_2, NULL, NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442310_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44211_str1, Screen44211_str2, NULL, NULL, &GUI_data_nav.Unit_per_pulse_str,
//         char_array,        0, 1},
//     {Screen44211_str3, Screen44211_str2,   &Units_var.Unit_pulse_on_time_str, NULL,
//         &Setup_device_var.Pulse_on_time_3, unsigned_int_type, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442340_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44214_str1, Screen44214_str2,  &Scale_setup_var.Speed_unit, &Units_var.Unit_speed_time_str,
//          &Setup_device_var.MinMax_Speed_Setpoint_3, float_type, 0, 1},
//     {Screen44214_str3, Screen44214_str4,  &Setup_device_var.Speed_Output_Type_3, NULL,
//         NULL,                                      no_variable,       0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_442350_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44215_str1, Screen44215_str2,  &Scale_setup_var.Weight_unit, &Scale_setup_var.Rate_time_unit,
//         &Setup_device_var.MinMax_Rate_Setpoint_3, float_type, 0, 1},
//     {Screen44215_str3, Screen44215_str4,  &Setup_device_var.Rate_Output_Type_3, NULL, NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_443000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen443_str1, Screen443_str2, &Setup_device_var.Relay_Output_1_Function, NULL, NULL, no_variable, 0, 1},
//     {Screen443_str3, Screen443_str2, &Setup_device_var.Relay_Output_2_Function, NULL, NULL, no_variable, 0, 2},
//     {Screen443_str4, Screen443_str2, &Setup_device_var.Relay_Output_3_Function, NULL, NULL, no_variable, 0, 3},
// };

// __align(4) SCREEN_TYPE2_DATA Org_443120_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44214_str1, Screen44312_str1,  &Scale_setup_var.Speed_unit, &Units_var.Unit_speed_time_str,
//         &Setup_device_var.MinMax_Speed_Setpoint_4, float_type, 0, 1},
//     {Screen44214_str3, Screen44214_str4,  &Setup_device_var.Speed_Output_Type_4,     NULL,
//         NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_443130_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44215_str1, Screen44313_str1,  &Scale_setup_var.Weight_unit, &Scale_setup_var.Rate_time_unit,
//         &Setup_device_var.MinMax_Rate_Setpoint_4, float_type, 0, 1},
//     {Screen44215_str3, Screen44215_str4,  &Setup_device_var.Rate_Output_Type_4, NULL, NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_443220_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44214_str1, Screen44312_str1,  &Scale_setup_var.Speed_unit,&Units_var.Unit_speed_time_str,
//         &Setup_device_var.MinMax_Speed_Setpoint_5, float_type, 0, 1},
//     {Screen44214_str3, Screen44214_str4,  &Setup_device_var.Speed_Output_Type_5, NULL,
//         NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_443230_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44215_str1, Screen44313_str1,  &Scale_setup_var.Weight_unit, &Scale_setup_var.Rate_time_unit,
//         &Setup_device_var.MinMax_Rate_Setpoint_5,  float_type, 0, 1},
//     {Screen44215_str3, Screen44215_str4,  &Setup_device_var.Rate_Output_Type_5, NULL, NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_443320_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44214_str1, Screen44312_str1,  &Scale_setup_var.Speed_unit,&Units_var.Unit_speed_time_str,
//         &Setup_device_var.MinMax_Speed_Setpoint_6, float_type, 0, 1},
//     {Screen44214_str3, Screen44214_str4,  &Setup_device_var.Speed_Output_Type_6, NULL,
//         NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_443330_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen44215_str1, Screen44313_str1,  &Scale_setup_var.Weight_unit,&Scale_setup_var.Rate_time_unit,
//         &Setup_device_var.MinMax_Rate_Setpoint_6, float_type, 0, 1},
//     {Screen44215_str3, Screen44215_str4,  &Setup_device_var.Rate_Output_Type_6, NULL, NULL, no_variable, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_444100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen4441_str1,  Screen4441_str2,   &Setup_device_var.Analog_Output_1_Function, NULL,
//         NULL,                                       no_variable, 0, 1},
//     {Screen44412_str1, Screen44412_str2,  NULL,                                       NULL,
//         &Setup_device_var.Analog_Output_1_Setpoint, float_type,  0, 2},
//     {Screen44413_str1, Screen44413_str2,  NULL,                                       NULL,
//         &Setup_device_var.Analog_Output_1_Maxrate,  float_type,  0, 3},
//     {Screen44414_str1, Screen44414_str2,  NULL,                                       NULL,
//         &Setup_device_var.Analog_Output_1_ZeroCal,  float_type2,  0, 4},
// 				
// };

// #if 0
// __align(4) SCREEN_TYPE2_DATA Org_444110_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen4441_str1,   Screen4441_str2,  &Setup_device_var.Analog_Output_1_Function, NULL, NULL, no_variable, 1},
// };
// #endif

// __align(4) SCREEN_TYPE2_DATA Org_444200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen4442_str1,  Screen4442_str2,   &Setup_device_var.Analog_Output_2_Function, NULL,
//             NULL,                                   no_variable, 0, 1},
//     {Screen44422_str1, Screen44422_str2,  NULL,                                       NULL,
//         &Setup_device_var.Analog_Output_2_Setpoint, float_type,  0, 2},
//     {Screen44423_str1, Screen44423_str2,  NULL,                                       NULL,
//         &Setup_device_var.Analog_Output_2_Maxrate,  float_type,  0, 3},
//     {Screen44424_str1, Screen44424_str2,  NULL,                                       NULL,
//         &Setup_device_var.Analog_Output_2_ZeroCal,  float_type2,  0, 4},
// };

// #if 0
// __align(4) SCREEN_TYPE2_DATA Org_444210_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen4442_str1,  Screen4442_str2,   &Setup_device_var.Analog_Output_2_Function, NULL, NULL, no_variable, 1},
// };
// #endif

// __align(4) SCREEN_TYPE2_DATA Org_451000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//  {Screen451_str1,  Str_NULL,   NULL,                                NULL,  &Setup_device_var.PID_Channel,
//      unsigned_int_type, 0, 1},
//  {Screen451_str2,  Str_NULL,   &Setup_device_var.PID_Action,        NULL, NULL,
//      no_variable,       0, 2},
//  {Screen451_str3,  Str_NULL,   &Setup_device_var.PID_Setpoint_Type, NULL, NULL,
//      no_variable,       0, 3},
//  {Screen451_str4,  Str_NULL,   NULL,                                NULL, NULL,
//      no_variable,       0, 4},
// };

// __align(4) SCREEN_TYPE2_DATA Org_452000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen452_str1,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 1},
//     {Screen452_str3,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 2},
//     {Screen452_str4,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 3},
//     {Screen452_str5,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 4},
//     {Screen452_str6,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 5},
//     {Screen452_str7,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 6},
//     {Screen452_str8,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 7},
//     {Screen452_str9,  Screen452_str2, NULL, NULL, NULL,  no_variable, 0, 8},
// };

// __align(4) SCREEN_TYPE2_DATA Org_453000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen453_str1,  Screen453_str2,   NULL, NULL,  &Setup_device_var.Network_Addr,       unsigned_int_type, 0, 1},
//     {Screen453_str5,  Screen453_str6,   NULL, NULL,  &Setup_device_var.Percent_Ingredient, float_type2, 0, 2},
//     {Screen453_str7,  Screen453_str8,   NULL, NULL,  &Setup_device_var.Feed_Delay,         unsigned_int_type, 0, 3},
//     {Screen453_str9,  Screen453_str10,  NULL, NULL,  &Setup_device_var.Preload_Delay,      unsigned_int_type, 0, 4},
//     {Screen453_str11, Screen453_str12,  NULL, NULL,  &Setup_device_var.Feeder_Max_Rate,    float_type, 0, 5},
//     {Screen45_str1,   Screen453_str13,  NULL, NULL, NULL,                                 no_variable,       0, 6},
// };

// __align(4) SCREEN_TYPE2_DATA Org_454000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen454_str1,  Screen454_str2,  &Scale_setup_var.Weight_unit, &Scale_setup_var.Rate_time_unit, &Setup_device_var.Target_rate,   float_type2,        0, 1},
//     {Screen453_str9,  Screen453_str10,  NULL, NULL, &Setup_device_var.Preload_Delay, unsigned_int_type, 0, 2},
//     {Screen45_str1,   Screen454_str3,   NULL, NULL, NULL,                            no_variable,       0, 3},
// };

// __align(4) SCREEN_TYPE2_DATA Org_455000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen455_str1,  Screen455_str2,   NULL, NULL, &Setup_device_var.Target_Load,      float_type, 0, 1},
//     {Screen455_str3,  Screen455_str4,   NULL, NULL, &Setup_device_var.Min_Belt_Speed,   float_type, 0, 2},
//     {Screen455_str5,  Screen455_str6,   NULL, NULL, &Setup_device_var.Preload_Distance, float_type, 0, 3},
//     {Screen455_str7,  Screen455_str8,   NULL, NULL, &Setup_device_var.Empty_Belt_Speed, float_type, 0, 4},
// };

// __align(4) SCREEN_TYPE2_DATA Org_510000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
// 	  {Screen51_str1,  Screen51_str2,  &Scale_setup_var.Weight_unit, NULL, &Rprts_diag_var.Job_Total,           double_type, 0, 0},
//     {Screen51_str3,  Screen51_str4,  &Scale_setup_var.Weight_unit, NULL, &Rprts_diag_var.daily_Total_weight,  double_type, 0, 0},
//     {Screen51_str5,  Screen51_str6,  &Scale_setup_var.Weight_unit, NULL, &Rprts_diag_var.weekly_Total_weight, double_type, 0, 0},
//     {Screen51_str7,  Screen51_str8,  &Scale_setup_var.Weight_unit, NULL, &Rprts_diag_var.monthly_Total_weight,double_type, 0, 0},
//     {Screen51_str9,  Screen51_str10, &Scale_setup_var.Weight_unit, NULL, &Rprts_diag_var.yearly_Total_weight, double_type, 0, 0},
//     {Screen51_str11, Screen51_str12, &Scale_setup_var.Weight_unit, NULL, &Rprts_diag_var.Master_total,        double_type, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_521000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen521_str1, Screen521_str4,  NULL,                            NULL,
//         &Scale_setup_var.Number_of_idlers, unsigned_int_type, 0, 0},
//     {Screen521_str3, Screen521_str4,  &Scale_setup_var.Load_cell_size, NULL,
//         NULL,                              no_variable,       0, 0},
//     {Screen521_str5, Screen521_str4,  NULL,                            NULL,
//         &Scale_setup_var.Custom_LC_output, unsigned_int_type, 0, 0},
//     {Screen521_str6, Screen521_str4,  NULL,                            NULL,
//         &Scale_setup_var.Conveyor_angle,   float_type,        0, 0},
//     {Screen521_str7, Screen521_str4,  NULL,                            NULL,
//         &Scale_setup_var.Idler_distance,   float_type,        0, 0},
//     //{Screen521_str8, Screen521_str4,  NULL,                 NULL, NULL, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_522000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen522_str1,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_1_Output,
//         float_type, 0, 0},
//     {Screen522_str2,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_2_Output,
//         float_type, 0, 0},
//     {Screen522_str3,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_3_Output,
//         float_type, 0, 0},
//     {Screen522_str4,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_4_Output,
//         float_type, 0, 0},
//     {Screen522_str5,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_5_Output,
//         float_type, 0, 0},
//     {Screen522_str6,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_6_Output,
//         float_type, 0, 0},
//     {Screen522_str7,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_7_Output,
//         float_type, 0, 0},
//     {Screen522_str8,  Screen522_str13,  NULL,                        NULL, &Rprts_diag_var.Load_Cell_8_Output,
//         float_type, 0, 0},
//     {Screen522_str9,  Screen522_str11,  &Rprts_diag_var.Speed_In_Hz, NULL, &Rprts_diag_var.Speed_Sensor,
//         float_type, 0, 0},
//     {Screen522_str10, Screen522_str12,  NULL,                        NULL, &Rprts_diag_var.Angle_Sensor,
//         float_type, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_523100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen5231_str1,  Screen5231_str11,  &Setup_device_var.Digital_Input_1_Function,
//        &Rprts_diag_var.Digital_Input_1_Status, NULL, no_variable, 0, 0},
//     {Screen5231_str2,  Screen5231_str11,  &Setup_device_var.Digital_Input_2_Function,
//                 &Rprts_diag_var.Digital_Input_2_Status, NULL, no_variable, 0, 0},
//     {Screen5231_str3,  Screen5231_str11,  &Setup_device_var.Digital_Input_3_Function,
//                 &Rprts_diag_var.Digital_Input_3_Status, NULL, no_variable, 0, 0},
//     {Screen5231_str4,  Screen5231_str11,  &Setup_device_var.Digital_Input_4_Function,
//                 &Rprts_diag_var.Digital_Input_4_Status, NULL, no_variable, 0, 0},
// };


// __align(4) SCREEN_TYPE2_DATA Org_523200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen5233_str1,  Screen5231_str11,  &Setup_device_var.Digital_Output_1_Function,
//         &Rprts_diag_var.Digital_Output_1_Status, NULL, no_variable, 0, 0},
//     {Screen5233_str2,  Screen5231_str11,  &Setup_device_var.Digital_Output_2_Function,
//         &Rprts_diag_var.Digital_Output_2_Status, NULL, no_variable, 0, 0},
//     {Screen5233_str3,  Screen5231_str11,  &Setup_device_var.Digital_Output_3_Function,
//         &Rprts_diag_var.Digital_Output_3_Status, NULL, no_variable, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_523300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen5234_str1,  Screen5231_str11,  &Setup_device_var.Relay_Output_1_Function,
//         &Rprts_diag_var.Relay_Output_1_Status, NULL, no_variable, 0, 0},
//     {Screen5234_str2,  Screen5231_str11,  &Setup_device_var.Relay_Output_2_Function,
//         &Rprts_diag_var.Relay_Output_2_Status, NULL, no_variable, 0, 0},
//     {Screen5234_str3,  Screen5231_str11,  &Setup_device_var.Relay_Output_3_Function,
//         &Rprts_diag_var.Relay_Output_3_Status, NULL, no_variable, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_523400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen5235_str1,  Screen521_str2,  NULL, NULL, &Setup_device_var.Analog_Output1, float_type, 0, 0},
//     {Screen5235_str2,  Screen521_str2,  NULL, NULL, &Setup_device_var.Analog_Output2, float_type, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_523500_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen5236_str1,  Screen521_str4,  NULL,                                NULL,
//          &Setup_device_var.PID_Channel, unsigned_int_type, 0, 0},
//     {Screen5236_str2,  Screen521_str4,  &Setup_device_var.PID_Action,        NULL,
//          NULL,                          no_variable,       0, 0},
//     {Screen5236_str3,  Screen521_str4,  &Setup_device_var.PID_Setpoint_Type, NULL,
//          NULL,                          no_variable,       0, 0},
//     {Screen5236_str4,  Screen521_str2,  &Rprts_diag_var.PID_Set_Rate,        NULL,
//          NULL,                          no_variable,       0, 0},
//     {Screen5236_str5,  Screen521_str4,  NULL,                                NULL,
//          &Setup_device_var.PID_P_Term,  unsigned_int_type, 0, 0},
//     {Screen5236_str6,  Screen521_str4,  NULL,                                NULL,
//          &Setup_device_var.PID_I_Term,  unsigned_int_type, 0, 0},
//     {Screen5236_str7,  Screen521_str4,  NULL,                                NULL,
//          &Setup_device_var.PID_D_Term,  unsigned_int_type, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_524000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen524_str1,  Screen524_str3,  NULL, NULL, &Rprts_diag_var.Load_cell_supply, float_type, 0, 0},
//     {Screen524_str2,  Screen524_str3,  NULL, NULL, &Rprts_diag_var.Five_volt_supply, float_type, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_525000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen525_str1,  Screen525_str2,   &Rprts_diag_var.RS485_1_Status,  NULL, NULL, no_variable, 0, 0},
//     {Screen525_str3,  Screen525_str4,   &Rprts_diag_var.RS485_2_Status,  NULL, NULL, no_variable, 0, 0},
//     {Screen525_str5,  Screen525_str6,   &Rprts_diag_var.RS232_1_Status,  NULL, NULL, no_variable, 0, 0},
//     {Screen525_str7,  Screen525_str8,   &Rprts_diag_var.RS232_2_Status,  NULL, NULL, no_variable, 0, 0},
//     {Screen525_str9,  Screen525_str10,  &Rprts_diag_var.Ethernet_Status, NULL, NULL, no_variable, 0, 0},
//     {Screen525_str11, Screen525_str12,  &Rprts_diag_var.SPI_Status,      NULL, NULL, no_variable, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_526000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen526_str1,  Screen521_str4, NULL,                                   NULL,
//           &Calibration_var.new_span_value,  float_type6, 0, 0},
//     {Screen526_str2,  Screen521_str4, &Calibration_var.Test_zero_weight_unit, NULL,
//           /*&Calculation_struct.Zero_weight*/&Calibration_var.New_zero_value,  float_type, 0, 0},
//     {Screen526_str3,  Screen521_str4, &Scale_setup_var.Belt_length_unit,      NULL,
//           &Calibration_var.New_belt_length, float_type, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_530000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen53_str1,  Screen53_str4,  NULL, NULL, NULL,         NULL,          0, 0},
//     {Screen53_str2,  Screen53_str4,  NULL, NULL, NULL,      NULL,          0, 0},
//     {Screen53_str3,  Screen53_str4,  NULL, NULL, NULL,      NULL,          0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_610000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen61_str1,  Screen61_str2,  NULL,                        NULL, &Admin_var.Scale_Name,
//           char_array,          0, 1},
//     {Screen61_str3,  Screen61_str4,  NULL,                        NULL, &Admin_var.Plant_Name,
//           char_array,          0, 2},
//     {Screen61_str5,  Screen61_str6,  NULL,                        NULL, &Admin_var.Product_Name,
//           char_array,          0, 3},
//     {Screen61_str7,  Screen61_str8,  NULL,                        NULL, &Admin_var.Config_Date,
//           current_date_type,   0, 4},
//     {Screen61_str9,  Screen61_str10, NULL,                        NULL, &Admin_var.Config_Time,
//           current_time_type,   0, 5},
//     {Screen61_str11, Screen61_str12, NULL,                        NULL, &Admin_var.Auto_zero_tolerance,
//           float_type,         0, 6},
//     {Screen61_str13, Screen61_str14,  		&Admin_var.Zero_rate_status, NULL, NULL,
//           unsigned_char_type,  0, 7},
//     {Screen61_str15, Screen61_str16, NULL,                        NULL, &Admin_var.Negative_rate_limit,
//           float_type,          0, 8},
//     {Screen61_str17, Screen61_str18, NULL,                        NULL, NULL,
//           no_variable,         0, 9},
//     {Screen61A_str0, Screen61_strA, NULL,                        NULL, NULL,
//           no_variable,         0, 10},
//     {Screen61B_str0, Screen61B_str1, &Misc_var.Language_select, 0, NULL,
//           unsigned_char_type,  0, 11},					
// };

// __align(4) SCREEN_TYPE2_DATA Org_614000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
// 	  {Screen6142_str1,  Screen6142_str2, &Admin_var.Current_Date_Format, NULL, NULL,
//           no_variable,       0, 2},
//     {Screen6141_str1,  Screen6141_str2, NULL,                           NULL, &Admin_var.Config_Date,
//           current_date_type, 0, 1},
//     {Screen6143_str1,  Screen6143_str2, &Admin_var.Day_of_week,         NULL, NULL,
//           no_variable,       0, 3},
// };

// __align(4) SCREEN_TYPE2_DATA Org_615000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen6151_str1, Screen6151_str2, NULL,                           NULL,  &Admin_var.Config_Time,
//           current_time_type, 0, 1},
//   //Commented by PVK 20 May 2016 to do not show those option as there is issue in time format
// 	 /*  
// 		{Screen6152_str1, Screen6152_str2, &Admin_var.AM_PM,               NULL, NULL,
//           no_variable,       0, 2},
//     {Screen6153_str1, Screen6153_str2, &Admin_var.Current_Time_Format, NULL, NULL,
//           no_variable,       0, 3},
// 	*/				
// };

// __align(4) SCREEN_TYPE2_DATA Org_619211_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen61922_str1, Screen619221_str1, NULL, NULL, &Admin_var.Reload_file_name, char_array,  0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_630000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen63_str1,  Screen63_str2,  &Admin_var.Setup_Wizard_Locked,     NULL, NULL, no_variable, 0, 1},
//     {Screen63_str3,  Screen63_str2,  &Admin_var.Calibration_Locked,      NULL, NULL, no_variable, 0, 2},
//     {Screen63_str4,  Screen63_str2,  &Admin_var.Zero_Calibration_Locked, NULL, NULL, no_variable, 0, 3},
//     {Screen63_str5,  Screen63_str2,  &Admin_var.Setup_Devices_Locked,    NULL, NULL, no_variable, 0, 4},
//     {Screen63_str6,  Screen63_str2,  &Admin_var.Admin_Locked,            NULL, NULL, no_variable, 0, 5},
//     {Screen63_str7,  Screen63_str2,  &Admin_var.Clear_Weight_Locked,     NULL, NULL, no_variable, 0, 6},
//     {Screen63_str8,  Screen63_str9, NULL,                               NULL, NULL, no_variable, 0, 7}
// };

// __align(4) SCREEN_TYPE2_DATA Org_642100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen64131_str1, Screen64131_str2, NULL, NULL,  &Admin_var.Int_update_file_name, char_array,  0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_660000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen66_str1,  Screen66_str2,  &Admin_var.DHCP_Status, NULL, NULL,                   no_variable, 0, 1},
//     {Screen66_str3,  Screen66_str4, NULL,                   NULL,  &Admin_var.IP_Addr,     ip_struct,   0, 2},
//     {Screen66_str5,  Screen66_str6, NULL,                   NULL,  &Admin_var.Subnet_Mask, ip_struct,   0, 3},
//     {Screen66_str7,  Screen66_str8, NULL,                   NULL,  &Admin_var.Gateway,     ip_struct,   0, 4},
//     {Screen66_str9,  Str_NULL     , NULL,                   NULL,  MAC_String,             char_array,  0, 0},
//     //PVK 12 May 2016 - changed unsigned_int_type to unsigned_char_type1
//     //{Screen66500_str1, Str_NULL   , NULL,                   NULL,  &Admin_var.MBS_TCP_slid, unsigned_int_type,0,5}
// 		{Screen66500_str1, Str_NULL   , NULL,                   NULL,  &Admin_var.MBS_TCP_slid, unsigned_char_type1,0,5}
// };

// __align(4) SCREEN_TYPE2_DATA Org_61A000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen61A_str1, Screen61A_str2,  &Admin_var.Test_speed_status, NULL, &Admin_var.Test_speed, float_type, 0, 1},
//     {Screen61A_str3, Screen61A_str4,  &Admin_var.Test_load_status, NULL, &Admin_var.Test_load, float_type, 0, 2},
// };

// __align(4) SCREEN_TYPE2_DATA Org_550000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen55_str0, Screen55_str1, &Calibration_var.Test_zero_weight_unit, NULL, &Rprts_diag_var.Live_weight_disp, float_type, 0, 0},
//     {Screen55_str2, Screen55_str3, &Calibration_var.Test_zero_weight_unit, NULL, &Rprts_diag_var.Trim_live_weight_disp, float_type, 0, 0},
// };

// __align(4) SCREEN_TYPE2_DATA Org_540000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {Screen11_str5, Screen11_str5, &Calibration_var.Test_zero_weight_unit  , NULL, &Calibration_var.Test_weight, float_type, 0,0},
//     {Screen55_str0, Screen55_str1, &Calibration_var.Test_zero_weight_unit, NULL, &Rprts_diag_var.Live_weight_disp, float_type, 0, 0},
// 		{Screen541000_str1, Screen541000_str3,NULL, NULL, &Rprts_diag_var.Tolerance_err,float_type, 0,0},
// 		{Screen541000_str2, Screen54_str1,NULL, NULL, &Rprts_diag_var.Calib_err_pct,float_type, 0,1},
// };
#endif
/*****************************************************************************
* End of file
*****************************************************************************/
