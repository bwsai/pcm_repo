/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project         : Beltscale Weighing Product - Integrator Board
* @detail Customer        : Beltway
*
* @file Filename          : Screen__common_func.c
* @brief                  : Controller Board
*
* @author                 : Anagha Basole
*
* @date Created           : November, 2012
* @date Last Modified     : November, 2012
*
* @internal Change Log    : <YYYY-MM-DD>
* @internal               :
* @internal               :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdlib.h>
#include <string.h>
#include "rtc.h"
#include "RTOS_main.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "Global_ex.h"
#include "File_Config.h"
#include "Modbus_uart_low_level.h"
#include "Main.h"
#include "soft_stack.h"
//#include "Screen_global_data.h"
#include "Screen_data_var.h"
#include "soft_stack.h"
#include "Screen_common_func.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define ASSIGN_OUTPUT1             1
#define ASSIGN_OUTPUT2             2
#define ASSIGN_OUTPUT3             3
#define ASSIGN_RELAY1              4
#define ASSIGN_RELAY2              5
#define ASSIGN_RELAY3              6

#define THREE_SEC_TIMEOUT_SWITCH   1//(200 / LCD_TASK_INTERVAL) //task called every 200ms

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void Display_Header(int Display_Text_Once);
void Display_Footer(int Display_Text_Once);
unsigned int getSpeed_unit(void);
unsigned int getRate_unit(void);
void Convert_Time(void);
void read_files_on_usb (int type);
void reset_error_warning_message_timers(void);
/*============================================================================
* Function Implementation Section
*===========================================================================*/


/*****************************************************************************
* @note       Function name  : void go_back_one_level(void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 3rd April 2013
* @brief      Description    : From the current screen organisation number the
*                            : previous one level up org number is calculated.
* @note       Notes          : None
*****************************************************************************/
void go_back_one_level(void)
{
    int i;
	  // if stack is empty then reset related flags 
	  if(wiz_nav_stack.top == 0)
		{
        GUI_data_nav.In_wizard = __FALSE;
				GUI_data_nav.Wizard_screen_index = 0;
				GUI_data_nav.Cancel_operation = __FALSE;
				GUI_data_nav.Setup_wizard = __FALSE;
//				Calib_struct.Start_flag = __FALSE;
				GUI_data_nav.wizard_screen = NULL;
		}			
    if(GUI_data_nav.In_wizard == __TRUE && (strncmp(GUI_data_nav.Current_screen_info->Screen_org_no, "351",3) != 0 ))
		{
			GUI_data_nav.Back_one_level = GO_BACK_FLAG;
//			sprintf(GUI_data_nav.New_screen_num,"%d",Pop_STACK(&wiz_nav_stack));
			GUI_data_nav.Change = 1;
			if((strncmp(GUI_data_nav.Current_screen_info->Screen_org_no,GUI_data_nav.wizard_screen->Next[GUI_data_nav.Wizard_screen_index-1].number,6) == 0))
			{
				GUI_data_nav.Wizard_screen_index--;
				if(GUI_data_nav.Wizard_screen_index == -1 || GUI_data_nav.Wizard_screen_index == 0)
				{
					GUI_data_nav.Wizard_screen_index = 1;
				}
			}
		}
		else
		{
				for (i = 0; i < 6; i++)
				{
						GUI_data_nav.New_screen_num[i] = '0';
				}
				for (i = 0; i < 6; i++)
				{
						if (GUI_data_nav.Current_screen_info->Screen_org_no[i] != '0')
						{
								GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->Screen_org_no[i];
								if (i == 5)
								{
										GUI_data_nav.New_screen_num[i] = '0';
										GUI_data_nav.Change = 1;
								}
						}
						else
						{
								GUI_data_nav.New_screen_num[i-1] = '0';
								GUI_data_nav.Change = 1;
								break;
						}
				}
			}
		// Below logic is for two special screens which are "2A0000" and "2B0000", these screens have charecters in its org numbers
		if(strcmp(GUI_data_nav.New_screen_num, "2") == 0)
		{
			 if(wiz_nav_stack.top == 6)
			 {
				 strcpy(GUI_data_nav.New_screen_num, DECIMAL_PLACES_ORG_NO);
			 }
			 else
			 {
				 strcpy(GUI_data_nav.New_screen_num, OPTIONAL_IO_BOARD_INTALL_ORG_NO);
			 }
		}
}
/*****************************************************************************
* @note       Function name  : void format_date(char * char_data)
* @returns    returns        : None
* @param      arg1           : pointer to the date string
* @author                    : Anagha Basole
* @date       date created   : 26th March 2013
* @brief      Description    : Formats the date according to the DD/MM/YYYY or
*                            : the MM/DD/YYYY format. Also checks if the date is
*                            : correct w.r.t the month(days) and the year(leap year).
*                            : If the date is incorrect, then it restores the date
*                            : to the default value.
* @note       Notes          : None
*****************************************************************************/
void format_date(char * char_data)
{
    int year = 0, month = 0, day = 0, format = 0, date_change = 0;
    char ip_ptr[10];

	  memset(ip_ptr, 0, sizeof(ip_ptr));
    /*MM/DD/YYYY format*/
    if(Pcm_var.Current_Date_Format == MMDDYYYY)
    {
        strncpy(ip_ptr, &char_data[0], 2);
			  *(ip_ptr+2) = '\0';
        month = atoi(ip_ptr);

        strncpy(ip_ptr, &char_data[3], 2);
			  *(ip_ptr+2) = '\0';
        day = atoi(ip_ptr);

        format = 1;
    }
    else/*DD/MM/YYYY format*/
    {
        strncpy(ip_ptr, &char_data[0], 2);
			  *(ip_ptr+2) = '\0';
        day = atoi(ip_ptr);

        strncpy(ip_ptr, &char_data[3], 2);
			  *(ip_ptr+4) = '\0';
        month = atoi(ip_ptr);
    }
    strncpy(ip_ptr, &char_data[6], 4);
		*(ip_ptr+4) = '\0';
    year = atoi(ip_ptr);

   #if 0
    if((month < 1) || (month > 12))
    {
        if(format == 1)/*MM/DD/YYYY format*/
        {
            strncpy(&char_data[0], "01", 2);
        }
        else/*DD/MM/YYYY format*/
        {
            strncpy(&char_data[3], "01", 2);
        }
    }
   #endif

    //switch according to the current month(changed date).
    switch(month)
    {
        case 2: //ensure either 28/29 days for february
            /* February */
            if(((year %   4) == 0) && //for the current year(changed year) not stored year
               ((year % 400) != 0))
            {
                /* leap-year */
                if(day > 29)   //till 29 days
                {
                    day = 1;
                    date_change = 1;
                }
            }
            else
            {
                /* normal year */
                if(day > 28)   //till 28 days
                {
                    day = 1;
                    date_change = 1;
                }
            }
            break;

        case 4:
        case 6:
        case 9:
        case 11:
            if(day > 30)    //till 30 days
            {
                day = 1;
                date_change = 1;
            }
            break;

        default:
            if(day > 31)   //till 31 days
            {
                day = 1;
                date_change = 1;
            }
            break;
    }
    if(date_change == 1)
    {
        if(format == 1)/*MM/DD/YYYY format*/
        {
            strncpy(&char_data[3], "01", 2);
        }
        else/*DD/MM/YYYY format*/
        {
            strncpy(&char_data[0], "01", 2);
        }
        date_change = 0;
    }

    //check the if the entered year is valid
    if((year < 2012) || (year > 2050))
    {
        strncpy(&char_data[6], "2012", 4);
    }

    return;
}

/*****************************************************************************
* @note       Function name  : void format_time(char * char_data)
* @returns    returns        : None
* @param      arg1           : pointer to the time string
* @author                    : Anagha Basole
* @date       date created   : 26th March 2013
* @brief      Description    : Formats the time according to the 12 Hour or the
*                            : 24 hour format. If the time is incorrect, then
*                            : it restores the time to the default value.
* @note       Notes          : None
*****************************************************************************/
void format_time(char * char_data)
{
    int hour = 0, min = 0;
    char ip_ptr[10];

	  memset(ip_ptr, 0, sizeof(ip_ptr));
    strncpy(ip_ptr, &char_data[0], 2);
	  *(ip_ptr+2) = '\0';
    hour = atoi(ip_ptr);

    strncpy(ip_ptr, &char_data[3], 2);
	  *(ip_ptr+2) = '\0';
    min = atoi(ip_ptr);

    /*12 Hour Format*/
    if(Pcm_var.Current_Time_Format == TWELVE_HR)
    {
        if(hour > 12 || hour < 0) //from 0 till 12 hours
        {
            strncpy(&char_data[0], "12", 2);
        }
    }
    else/*24 Hour Format*/
    {
        if(hour >= 24 || hour < 0)   //from 0 till 23 hours
        {
            strncpy(&char_data[0], "00", 2);
        }
    }
    if(min >= 60 || min < 0)  //from 0 till 59 minutes
    {
        strncpy(&char_data[3], "00", 2);
    }
    return;

}

/*****************************************************************************
* @note       Function name  : void custom_sprintf(int data_type, void * data,
*                            :                     char * char_data1, int size)
* @returns    returns        : None
* @param      arg1           : type of data to be printed
*             arg2           : pointer to the variable from which data is to be copied(source)
*             arg3           : pointer to the string in which data is to be printed(destination)
*             arg4           : size of the destination string
* @author                    : Anagha Basole
* @date       date created   : 26th December 2012
* @brief      Description    : Copies the data depending on the variable type into the
*                            : destination string. This is required since different data
*                            : formats need to be displayed on screen.
* @note       Notes          : None
*****************************************************************************/

void custom_sprintf(int data_type, void * data, char * char_data1, int size)
{
    TIME_STRUCT time;
    DATE_STRUCT date;
    IP_STRUCT ip;
		MAC_ID_STRUCT *mid;
    char *dchar_data;
    int sprintf_ret_val = 0, i = 0;
    switch(data_type)
    {
        case no_variable:
            break;

        case unsigned_char_type:
            sprintf_ret_val = sprintf(char_data1,"%c" ,*(unsigned char *)data);
            break;

        case signed_char_type:
            sprintf_ret_val = sprintf(char_data1, "%c", *(signed char *)data);
            break;
				
				//PVK 12 May 2016 Added to support integer 1 byte data type
				case unsigned_char_type1:
            sprintf_ret_val = sprintf(char_data1,"%d" ,*(unsigned char *)data);
            break;

        case unsigned_int_type:
            sprintf_ret_val = sprintf(char_data1, "%d", *(unsigned int *)data);
            break;

        case signed_int_type:
            sprintf_ret_val = sprintf(char_data1, "%d", *(signed int *)data);
            break;

        case float_type:
			      if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WHEEL_DIA_ENTER_ORG_NO) == 0 || 
							 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_ENTRY_ORG_NO) == 0)//|| 
						)
            {
               sprintf_ret_val = sprintf(char_data1, "%0.3f", *(float *)data);
							 
            }
            else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DIAG_SCREEN_LIVE_VOLTAGE_DATA_ORG_NO) == 0)||
                     (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DIAG_SCREEN_LIVE_VOLTAGE_DATA_ORG_NO) == 0)||
										(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SNTP_ORG_NO) == 0)
										)                                                         
            {
               sprintf_ret_val = sprintf(char_data1, "%0.2f", *(float *)data);
            }
            else
            {
               sprintf_ret_val = sprintf(char_data1, "%0.1f", *(float *)data);
            }
            break;
				case float_type1: // A new case is added to display floats with 3 decimal values. Add by pandurang on 11/11/2013
							sprintf_ret_val = sprintf(char_data1, "%0.2f", *(float *)data);	
            break;
				case float_type2: // A new case is added to display floats with 3 decimal values. Add by Venkata Krishna on 25/9/2013
							sprintf_ret_val = sprintf(char_data1, "%0.3f", *(float *)data);	
            break;
				case float_type6: // A new case is added to display floats with 6 decimal values. Add by Venkata Krishna on 25/9/2013
							sprintf_ret_val = sprintf(char_data1, "%0.6f", *(float *)data);	
            break;
        case double_type:
            /*if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_ENTRY_ORG_NO) == 0) ||
                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DIAG_SCREEN_LIVE_VOLTAGE_DATA_ORG_NO) == 0))
            {
               sprintf_ret_val = sprintf(char_data1, "%0.2lf", *(double *)data);
            }
            else*/
            {
               sprintf_ret_val = sprintf(char_data1, "%0.1lf", *(double *)data);
            }
            break;

        case date_struct:
            date = *(DATE_STRUCT * )data;
            if(Pcm_var.Current_Date_Format == MMDDYYYY)
            {
                sprintf_ret_val = sprintf(char_data1, "%02d/%02d/%d", date.Mon, date.Day, date.Year);
            }
            else/*DD/MM/YYYY format*/
            {
                sprintf_ret_val = sprintf(char_data1, "%02d/%02d/%d", date.Day, date.Mon, date.Year);
            }
            break;

        case time_struct:
            time = *(TIME_STRUCT * )data;
            sprintf_ret_val = sprintf(char_data1, "%02d:%02d", time.Hr, time.Min);
            break;

        case ip_struct:
            ip = *(IP_STRUCT * )data;
            sprintf_ret_val = sprintf(char_data1, "%d.%d.%d.%d", ip.Addr1, ip.Addr2, ip.Addr3, ip.Addr4);
            break;
				
        case mac_id_struct:					//by megha on 20/1/2017
            mid = (MAC_ID_STRUCT * )data;
            sprintf_ret_val = sprintf(char_data1, "%x:%x:%x:%x:%x:%x", mid->Addr1, mid->Addr2, mid->Addr3, mid->Addr4,mid->Addr5,mid->Addr6);
            break;				

        case char_array:
            strcpy(char_data1, data);
            break;

        case unit:
            dchar_data = data;
            strcpy(char_data1, dchar_data);
            break;

        case current_date_type:
            if(Pcm_var.Current_Date_Format == MMDDYYYY)
            {
                sprintf_ret_val = sprintf(char_data1, "%02d/%02d/%d", Current_time.RTC_Mon, Current_time.RTC_Mday, Current_time.RTC_Year);
            }
            else/*DD/MM/YYYY format*/
            {
                sprintf_ret_val = sprintf(char_data1, "%02d/%02d/%d", Current_time.RTC_Mday, Current_time.RTC_Mon, Current_time.RTC_Year);
            }
            break;

        case current_time_type:
            sprintf_ret_val = sprintf(char_data1, "%02d:%02d", Current_time.RTC_Hour, Current_time.RTC_Min);
            break;
    }
    if (sprintf_ret_val < 0)
    {
       char_data1[size - 1] = '\0';
    }
    else
    {
       for(i=0; i<size; i++)
       {
          if(char_data1[i] == '\0')
          {
            break;
          }
       }
       if (i == size)
       {
          char_data1[size - 1] = '\0';
       }
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void custom_sscanf(int data_type, void * data, char * char_data1)
* @returns    returns        : None
* @param      arg1           : type of data to be scanned
*             arg2           : pointer to the variable in which data is to be copied(destination)
*             arg3           : pointer to the string from which data is to be copied(source)
* @author                    : Anagha Basole
* @date       date created   : 26th December 2012
* @brief      Description    : Copies the data depending on the variable type from the character
*                            : string into the destination variable. This is required
*                            : since different data formats are input by the user.
* @note       Notes          : None
*****************************************************************************/
void custom_sscanf(int data_type, void * data, char * char_data1)
{
    char **dchar_data;
    double var_double;
    float var_float;
    TIME_STRUCT * time;
    DATE_STRUCT * date;
    IP_STRUCT * ip;
		MAC_ID_STRUCT *mid;	
    int num;
    char ip_ptr[6],mac_id_ptr[6];
		
	  unsigned int Dummy;
    
	  memset(ip_ptr, 0, sizeof(ip_ptr));
    switch(data_type)
    {
        case no_variable:
            break;

        case unsigned_char_type:
            dchar_data = data;
            *dchar_data = char_data1;
            break;

        case signed_char_type:
            dchar_data = data;
            *dchar_data = char_data1;
            break;

				 //PVK 12 May 2016 Added to support integer 1 byte data type				
				 case unsigned_char_type1:
				    sscanf(char_data1,"%d", &Dummy);
				    *(unsigned char *)data = (unsigned char)Dummy;
            break;
				 
        case unsigned_int_type:
            sscanf(char_data1,"%d", (unsigned int *)data);
            break;

        case signed_int_type:
            sscanf(char_data1,"%d", (signed int *)data);
            break;

        case float_type:
				case float_type1:
				case float_type2:	
        case float_type6:					
            sscanf(char_data1,"%f" ,&var_float);
            *(float *)data = var_float;
            break;

        case double_type:
            sscanf(char_data1,"%lf" ,&var_double);
            *(double *)data = var_double;
            break;

        case date_struct:
            date = (DATE_STRUCT * )data;
            /*MM/DD/YYYY format*/
            if(Pcm_var.Current_Date_Format == MMDDYYYY)
            {
                strncpy(ip_ptr, &char_data1[0], 2);
                ip_ptr[2] = '\0';
                num = atoi(ip_ptr);
                date->Mon = num;

                strncpy(ip_ptr, &char_data1[3], 2);
                ip_ptr[2] = '\0';
                num = atoi(ip_ptr);
                date->Day = num;
            }
            else/*DD/MM/YYYY format*/
            {
                strncpy(ip_ptr, &char_data1[0], 2);
                ip_ptr[2] = '\0';
                num = atoi(ip_ptr);
                date->Day = num;

                strncpy(ip_ptr, &char_data1[3], 2);
                ip_ptr[2] = '\0';
                num = atoi(ip_ptr);
                date->Mon = num;
            }
            strncpy(ip_ptr, &char_data1[6], 4);
						ip_ptr[4] = '\0';
            num = atoi(ip_ptr);
            date->Year = num;
            break;

        case time_struct:
            time = (TIME_STRUCT * )data;
            strncpy(ip_ptr, &char_data1[0], 2);
            ip_ptr[2] = '\0';
            num = atoi(ip_ptr);
            time->Hr = num;

            strncpy(ip_ptr, &char_data1[3], 2);
            ip_ptr[2] = '\0';
            num = atoi(ip_ptr);
            time->Min = num;
            break;

        case ip_struct:
            ip = (IP_STRUCT *) data;
            strncpy(ip_ptr, char_data1, 3);
            ip_ptr[3] = '\0';
            num = atoi(ip_ptr);
            char_data1 += 4;
            ip->Addr1 = num;

            strncpy(ip_ptr, char_data1, 3);
            ip_ptr[3] = '\0';
            num = atoi(ip_ptr);
            char_data1 += 4;
            ip->Addr2 = num;

            strncpy(ip_ptr, char_data1, 3);
            ip_ptr[3] = '\0';
            num = atoi(ip_ptr);
            char_data1 += 4;
            ip->Addr3 = num;

            strncpy(ip_ptr, char_data1, 3);
            ip_ptr[3] = '\0';
            num = atoi(ip_ptr);
            ip->Addr4 = num;
            break;
        case 	mac_id_struct :						//by megha on 20/1/2017
            mid = (MAC_ID_STRUCT *) data;
            strncpy(mac_id_ptr, char_data1, 2);
            mac_id_ptr[2] = '\0';
						num = atoi(mac_id_ptr);
            mid->Addr1 = num;
						char_data1 += 3;
				
            strncpy(mac_id_ptr, char_data1, 2);
            mac_id_ptr[2] = '\0';
						num = atoi(mac_id_ptr);
            mid->Addr2 = num;
						char_data1 += 3;
				
            strncpy(mac_id_ptr, char_data1, 2);
            mac_id_ptr[2] = '\0';
						num = atoi(mac_id_ptr);
            mid->Addr3 = num;
						char_data1 += 3;
				
            strncpy(mac_id_ptr, char_data1, 2);
            mac_id_ptr[2] = '\0';
						num = atoi(mac_id_ptr);
            mid->Addr4 = num;
						char_data1 += 3;
						
            strncpy(mac_id_ptr, char_data1, 2);
            mac_id_ptr[2] = '\0';
						num = atoi(mac_id_ptr);
            mid->Addr5 = num;
						char_data1 += 3;
						
            strncpy(mac_id_ptr, char_data1, 2);
            mac_id_ptr[2] = '\0';
						num = atoi(mac_id_ptr);
            mid->Addr6 = num;
            break;
         case char_array:
            strcpy(data, char_data1);
            break;

        case unit:
            strcpy(data, char_data1);
            break;

        case current_date_type:
            break;

        case current_time_type:
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void multiedit_insert_txt(int type, void * data)
* @returns    returns        : None
* @param      arg1           : type of data to be scanned
*             arg2           : pointer to the string from which data is to be copied(source)
* @author                    : Anagha Basole
* @date       date created   : 26th December 2012
* @brief      Description    : Inserts the data depending on the variable type from the varible
*                            : into the multiedit window in a textbox in yellow color.
* @note       Notes          : None
*****************************************************************************/
void multiedit_insert_txt(int type, void * data)
{
    char char_data1[ARRAY_SIZE], space_char[3];
    char *dchar_data;
    int pxPos = 0, pyPos = 0, y_size = 0, x_size = 0, i=0, len = 0;

    space_char[0] = ' ';
    space_char[1] = '\0';
    memset(char_data1, 0, sizeof(char_data1));
    y_size = GUI_GetYSizeOfFont(&GUI_FontArial_Unicode_MS16_Bold);
    MULTIEDIT_AddText(GUI_data_nav.Multi_text, space_char);
    MULTIEDIT_GetCursorPixelPos(GUI_data_nav.Multi_text, &pxPos, &pyPos);
    space_char[0] = ' ';
    space_char[1] = ' ';
    space_char[2] = '\0';

    switch(type)
    {
        case no_variable:
            break;
        case unsigned_char_type:
            dchar_data = data;

            x_size = GUI_GetStringDistX(dchar_data);
            len = strlen(dchar_data);

            GUI_data_nav.Text_widget = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget, dchar_data);
            break;

        case signed_char_type:
            dchar_data = data;

            x_size = GUI_GetStringDistX(dchar_data);
            len = strlen(dchar_data);

            GUI_data_nav.Text_widget = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget, dchar_data);
            break;

        case unsigned_int_type:
            sprintf(char_data1,"%d" ,*(unsigned int *)data);

            x_size = GUI_GetStringDistX(char_data1);
            len = strlen(char_data1);

            GUI_data_nav.Text_widget1 = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget1, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget1, char_data1);
            break;

        case signed_int_type:
            sprintf(char_data1,"%d" ,*(signed int *)data);

            x_size = GUI_GetStringDistX(char_data1);
            len = strlen(char_data1);

            GUI_data_nav.Text_widget1 = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget1, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget1, char_data1);
            break;

        case float_type:
            if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_VER_ENTRY_ORG_NO) == 0)
            {
               sprintf(char_data1,"%0.2f" ,*(float *)data);
						}
						else
						{
               sprintf(char_data1,"%0.3f" ,*(float *)data);
						}

            x_size = GUI_GetStringDistX(char_data1);
            len = strlen(char_data1);

            GUI_data_nav.Text_widget1 = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget1, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget1, char_data1);
            break;

        case double_type:
            /*if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_VER_ENTRY_ORG_NO) == 0)
            {
               sprintf(char_data1, "%0.2lf", *(double *)data);
            }
            else*/
            {
               sprintf(char_data1,"%0.1lf" ,*(double *)data);
            }

            x_size = GUI_GetStringDistX(char_data1);
            len = strlen(char_data1);

            GUI_data_nav.Text_widget1 = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget1, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget1, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget1, char_data1);
            break;

        case date_struct:
            break;

        case time_struct:
            break;

        case ip_struct:
            break;

        case char_array:
            len = strlen(data);
            x_size = GUI_GetStringDistX(data);

            GUI_data_nav.Text_widget = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget, data);
            break;

        case unit:
            dchar_data = data;
            strcpy(char_data1, dchar_data);

            x_size = GUI_GetStringDistX(char_data1);
            len = strlen(char_data1);

            GUI_data_nav.Text_widget = TEXT_CreateEx(pxPos, pyPos, x_size, y_size, \
                                                     GUI_data_nav.Multi_text, WM_CF_SHOW, 0, GUI_ID_TEXT5, NULL);
            TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_YELLOW);
            TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);
            TEXT_SetText(GUI_data_nav.Text_widget, char_data1);
            break;

    case current_date_type:
      break;

    case current_time_type:
      break;
    }
    for(i=0; i<len; i++)
    {
        MULTIEDIT_AddText(GUI_data_nav.Multi_text, space_char);
    }

    return;
}


/*****************************************************************************
* @note       Function name  : U8 security_bit_set(void)
* @returns    returns        : 1 if the screen data is read only/locked
*                            : 0 if the screen data is writable/unlocked
* @param      arg1           : none
* @author                    : Anagha Basole
* @date       date created   : 19th March 2013
* @brief      Description    : Checks if the screen is view only or modification
*                            : is allowed, depending on the status of the lock
*                            : bits defined by the user in the admin structure
*                            : and the current screen organisation number.
* @note       Notes          : None
*****************************************************************************/
U8 security_bit_set(void)
{
    U8 bit_set = 0;

    switch (GUI_data_nav.Current_screen_info->Screen_org_no[0])
    {
        case '1': //setup wizard is locked
				case '2': //scale setup is also locked
							
            if(Admin_var.Setup_Wizard_Locked == LOCK_ENABLE_STR)
            {
                bit_set = 1;
            }
            break;

        case '3': //calibration is locked
            if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, ZERO_CAL_WIZARD_ORG_NO) == 0)
            {
                if(Admin_var.Zero_Calibration_Locked == LOCK_ENABLE_STR)
                {
                    bit_set = 1;
                }
            }
            else if((Admin_var.Calibration_Locked == LOCK_ENABLE_STR) &&
                    strncmp(GUI_data_nav.Current_screen_info->Screen_org_no, ZERO_CAL_WIZARD_ORG_NO, 2))
            {
                bit_set = 1;
            }
            break;

        case '4': //setup devices is locked
            if(Admin_var.Setup_Devices_Locked == LOCK_ENABLE_STR)
            {
                bit_set = 1;
            }
            break;

        case '6': //admin is locked
            if(Admin_var.Admin_Locked == LOCK_ENABLE_STR)
            {
                bit_set = 1;
            }
            break;
    }
    return (bit_set);
}

/*****************************************************************************
* @note       Function name    : void CreateWindow_runmode_screen(int create_update)
* @returns    returns          : None
* @param      arg1             : create_update_flag
* @author                      : Pandurang Khutal
* @date       date created     : 28th December 2012
* @brief      Description      : Create or update the screen according to the org nos.
* @note       Notes            : None
*****************************************************************************/
void CreateWindow_runmode_screen(int create_update)
{
    int Run_Screen_org_no;

    Run_Screen_org_no = atoi(GUI_data_nav.Current_screen_info->Screen_org_no);

    /*If a new window is to be created*/
    switch (Run_Screen_org_no)
    {
        case WEIGHT_RUN_MODE_SCREEN_ORG_NO:
        case RATE_RUN_MODE_SCREEN_ORG_NO:
//             Create_Weight_Run_mode_screen(create_update);
             break;
        case LOADOUT_RUN_MODE_SCREEN_ORG_NO:
//             Create_loadout_screen(create_update);
             break;

        case BLENDING_RUN_MODE_SCREEN_ORG_NO:
//					   Create_blending_load_rate_ctrl_screen(create_update);
				     //set the flag
//				     Rate_blend_load.Calculation_flags |= BLEND_CONTROL_SCREEN_FLAG;
					   break;

        case RATE_CONTROL_RUN_MODE_SCREEN_ORG_NO:
//					   Create_blending_load_rate_ctrl_screen(create_update);
				     //set the flag
//				     Rate_blend_load.Calculation_flags |= RATE_CONTROL_SCREEN_FLAG;
					   break;

        case LOAD_CONTROL_RUN_MODE_SCREEN_ORG_NO:
 //            Create_blending_load_rate_ctrl_screen(create_update);
				     //set the flag
//						 Rate_blend_load.Calculation_flags |= LOAD_CONTROL_SCREEN_FLAG;
             break;
				
				case WEIGHT_PREVIOUS_RUN_MODE_SCREEN_ORG_NO:
				case WEIGHT_PREV1_RUN_MODE_ORG_NO:
				case WEIGHT_PREV2_RUN_MODE_ORG_NO:
				case WEIGHT_PREV3_RUN_MODE_ORG_NO:
				case WEIGHT_PREV4_RUN_MODE_ORG_NO:
				case WEIGHT_PREV5_RUN_MODE_ORG_NO:
				case WEIGHT_PREV6_RUN_MODE_ORG_NO:
				case WEIGHT_PREV7_RUN_MODE_ORG_NO:
				case WEIGHT_PREV8_RUN_MODE_ORG_NO:					
//						 Create_Weight_Run_mode_screen(create_update);
						 break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void delete_widgets_mem_free (void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 5th March 2013
* @brief      Description    : Deallocates the memory used by the respentive widget.
*                            : Also resets the flag to zero, so as not cause an error,
*                            : due to deallocation of memory not allocated.
* @note       Notes          : None
*****************************************************************************/
void delete_widgets_mem_free (void)
{
    if (GUI_data_nav.Listbox != 0)
    {
        WM_DeleteWindow(GUI_data_nav.Listbox);
        GUI_data_nav.Listbox = 0;
    }
    if (GUI_data_nav.Multi_text != 0)
    {
        WM_DeleteWindow(GUI_data_nav.Multi_text);
        GUI_data_nav.Multi_text = 0;
    }
    if (GUI_data_nav.Icon_screen != 0)
    {
        WM_DeleteWindow(GUI_data_nav.Icon_screen);
        GUI_data_nav.Icon_screen = 0;
    }
    if (GUI_data_nav.Keypad != 0)
    {
        WM_DeleteWindow(GUI_data_nav.Keypad);
        GUI_data_nav.Keypad = 0;
    }
    if (GUI_data_nav.Numpad != 0)
    {
        WM_DeleteWindow(GUI_data_nav.Numpad);
        GUI_data_nav.Numpad = 0;
    }
    return;
}



/*****************************************************************************
* @note       Function name  : void Display_Header(int Display_Text_Once)
* @returns    returns        : None
* @param      arg1           : Flag to indicate the data to be updated once
* @author                    : Pandurang Khutal
* @date       date created   : 26th March 2013
* @brief      Description    : Displays the header of the screen, based on the
*                            : title, current date and time and also the name of
*                            : the scale. The date and time are updated periodically
* @note       Notes          : None
*****************************************************************************/
void Display_Header(int Display_Text_Once)
{
    char disp[ARRAY_SIZE];
    memset(disp, '\0', sizeof(disp));
    GUI_SetColor(GUI_WHITE);
    GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);
    GUI_SetTextAlign(GUI_TA_TOP | GUI_TA_LEFT);

    if(Display_Text_Once == 0)
    {
        //Display Scale name
        GUI_DispStringAtCEOL(Pcm_var.Int_firmware_version, WINDOW_HEADER_TEXT1_POS_X0, WINDOW_HEADER_TEXT1_POS_Y0);
        GUI_SetTextAlign(GUI_TA_TOP | GUI_TA_HCENTER);

        GUI_DrawRect(WINDOW_RECT_POS_X0,WINDOW_RECT_POS_Y0,WINDOW_RECT_POS_X1,WINDOW_RECT_POS_Y1);
        GUI_DrawLine(WINDOW_HEADER_LINE_POS_X0,WINDOW_HEADER_LINE_POS_Y0,WINDOW_HEADER_LINE_POS_X1,WINDOW_HEADER_LINE_POS_Y1);

        //Display Menu option
        switch (GUI_data_nav.Current_screen_info->Screen_org_no[0])
        {
            case '0':
                GUI_DispStringAt(Strings[Screen0_str0].Text, (WINDOW_HEADER_TEXT2_POS_X0 + 20), WINDOW_HEADER_TEXT2_POS_Y0);
                break;

            case '1':
                GUI_DispStringAt(Strings[Screen0_str1].Text, WINDOW_HEADER_TEXT2_POS_X0, WINDOW_HEADER_TEXT2_POS_Y0);
                break;

            case '2':
                GUI_DispStringAt(Strings[Screen0_str2].Text, WINDOW_HEADER_TEXT2_POS_X0, WINDOW_HEADER_TEXT2_POS_Y0);
                break;

            case '3':
                GUI_DispStringAt(Strings[Screen0_str3].Text, WINDOW_HEADER_TEXT2_POS_X0, WINDOW_HEADER_TEXT2_POS_Y0);
                break;

            case '4':
                GUI_DispStringAt(Strings[Screen0_str4].Text, (WINDOW_HEADER_TEXT2_POS_X0 - 10), WINDOW_HEADER_TEXT2_POS_Y0);
                break;

            case '5':
                GUI_DispStringAt(Strings[Screen0_str5].Text, WINDOW_HEADER_TEXT2_POS_X0, WINDOW_HEADER_TEXT2_POS_Y0);
                break;

            case '6':
                GUI_DispStringAt(Strings[Screen0_str6].Text, WINDOW_HEADER_TEXT2_POS_X0, WINDOW_HEADER_TEXT2_POS_Y0);
                break;

            case '7':
								break;
						case '8':
                GUI_DispStringAt(Strings[Screen0_str7].Text, WINDOW_HEADER_TEXT2_POS_X0, WINDOW_HEADER_TEXT2_POS_Y0);
							
                break;

            default:
                GUI_DispStringAt(Strings[Screen0_str0].Text, (WINDOW_HEADER_TEXT2_POS_X0), WINDOW_HEADER_TEXT2_POS_Y0);
                break;
        }
        if (GUI_data_nav.Current_screen_info->Screen_org_no[1] != '0')
        {
                GUI_DispString(">");
                GUI_DispString(Strings[GUI_data_nav.Current_screen_info->Screen_name_index].Text);
        }
				
    }
		
		if((GUI_data_nav.Current_screen_info->Screen_org_no[2]>'0') && (GUI_data_nav.Current_screen_info->Screen_org_no[0] =='7')  && (GUI_data_nav.Current_screen_info->Screen_org_no[1]== '6'))
		{
				GUI_SetColor(GUI_YELLOW);

				if(Admin_var.Current_Date_Format == MMDDYYYY)/*MM/DD/YYYY format*/
				{
						sprintf(&disp[0], "%02d/%02d/%04d %02d:%02d ",Rprts_diag_var.Clear_Date[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Mon,
																													Rprts_diag_var.Clear_Date[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Day,
																													Rprts_diag_var.Clear_Date[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Year,
																													Rprts_diag_var.Clear_Time[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Hr,
																													Rprts_diag_var.Clear_Time[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Min);
				}
				else /*DD/MM/YYYY format*/
				{
						sprintf(&disp[0], "%02d/%02d/%04d %02d:%02d ",Rprts_diag_var.Clear_Date[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Day, 
																													Rprts_diag_var.Clear_Date[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Mon,
																													Rprts_diag_var.Clear_Date[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Year,
																													Rprts_diag_var.Clear_Time[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Hr,
																													Rprts_diag_var.Clear_Time[GUI_data_nav.Current_screen_info->Screen_org_no[2]-0x30-1].Min);
				}			
		}
		else
		{
				 //Display date time
				if(Pcm_var.Current_Date_Format == MMDDYYYY)/*MM/DD/YYYY format*/
				{
						sprintf(&disp[0], "%02d/%02d/%04d %02d:%02d:%02d ", Current_time.RTC_Mon,
																													 Current_time.RTC_Mday,
																													Current_time.RTC_Year,
																													Current_time.RTC_Hour & 0x001F, 
																													Current_time.RTC_Min & 0x003F,Current_time.RTC_Sec & 0x003F);
				}
				else /*DD/MM/YYYY format*/
				{
						sprintf(&disp[0], "%02d/%02d/%04d %02d:%02d:%02d ", Current_time.RTC_Mday, 
																													 Current_time.RTC_Mon, 
																													Current_time.RTC_Year, 
																													Current_time.RTC_Hour&0x001F, 
																													Current_time.RTC_Min &0x003F,Current_time.RTC_Sec&0x003F);
				}
		}
    GUI_SetTextAlign(GUI_TA_TOP | GUI_TA_RIGHT);
    GUI_DispStringAt(disp, WINDOW_HEADER_TEXT3_POS_X0, WINDOW_HEADER_TEXT3_POS_Y0);
    return;
}

//#if 0

/*****************************************************************************
* @note       Function name  : void reset_error_warning_message_timers(void)
* @returns    returns        : None
* @param      arg1           : None
* @author                    : Anagha Basole
* @date       date created   : 27th June 2013
* @brief      Description    : Resets the error and warning flag counters
* @note       Notes          : None
*****************************************************************************/
void reset_error_warning_message_timers(void)
{
   if(GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
      GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
      GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
      GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
      GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
			Flags_struct.Plant_connect_record_flag &= ~PCM_SCALE_DATA_UPDATE_OK_FLAG;
      GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[SCALE_DATA_RECEIVE_OK_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
			Flags_struct.Plant_connect_record_flag &= ~SCALE_DATA_RECEIVE_OK_FLAG;
      GUI_data_nav.Error_wrn_msg_flag[SCALE_DATA_RECEIVE_OK_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_FAIL_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
			Flags_struct.Plant_connect_record_flag &= ~PCS_SOCKET_CONNECTION_FAIL_FLAG;
      GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_FAIL_MSG_TIME] = 0;
   }	
	 
   if(GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_STATUS_NC_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
			Flags_struct.Plant_connect_record_flag &= ~PCS_SOCKET_STATUS_NC_FLAG;
      GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_STATUS_NC_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[PCS_SERVER_IP_RESOLVE_PROCESS_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
			Flags_struct.Plant_connect_record_flag &= ~PCS_SERVER_IP_RESOLVE_PROCESS_FLAG;
      GUI_data_nav.Error_wrn_msg_flag[PCS_SERVER_IP_RESOLVE_PROCESS_MSG_TIME] = 0;
   }
   if(GUI_data_nav.Error_wrn_msg_flag[SOCKET_CONNECTION_PROCESS_MSG_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
   {
			Flags_struct.Plant_connect_record_flag &= ~SOCKET_CONNECTION_PROCESS_FLAG;
      GUI_data_nav.Error_wrn_msg_flag[SOCKET_CONNECTION_PROCESS_MSG_TIME] = 0;
   }	 
//    if(GUI_data_nav.Error_wrn_msg_flag[IO_BRD_COMM_ERR_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[IO_BRD_COMM_ERR_TIME] = 0;
//    }
//    if(GUI_data_nav.Error_wrn_msg_flag[LOW_SPEED_WRN_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[LOW_SPEED_WRN_TIME] = 0;
//    }
//    if(GUI_data_nav.Error_wrn_msg_flag[HIGH_SPEED_WRN_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[HIGH_SPEED_WRN_TIME] = 0;
//    }
//    if(GUI_data_nav.Error_wrn_msg_flag[ZERO_CAL_CHANGE_WRN_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[ZERO_CAL_CHANGE_WRN_TIME] = 0;
//    }
//    if(GUI_data_nav.Error_wrn_msg_flag[SEN_BRD_COMM_WRN_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[SEN_BRD_COMM_WRN_TIME] = 0;
//    }
//    if(GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_WRN_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_WRN_TIME] = 0;
//    }
//    if(GUI_data_nav.Error_wrn_msg_flag[LOW_RATE_WRN_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[LOW_RATE_WRN_TIME] = 0;
//    }
//    if(GUI_data_nav.Error_wrn_msg_flag[HIGH_RATE_WRN_TIME] >= THREE_SEC_TIMEOUT_SWITCH)
//    {
//       GUI_data_nav.Error_wrn_msg_flag[HIGH_RATE_WRN_TIME] = 0;
//    }
//    return;
}
//#endif
/*****************************************************************************
* @note       Function name  : U8 populate_error_message(char * error_msg)
* @returns    returns        : 1 if a error message is present
*                            : 0 if there is no error message
* @param      arg1           : pointer to the location where the string is to be
*                            : stored.
* @author                    : Anagha Basole
* @date       date created   : 19th March 2013
* @brief      Description    : Checks the various error flags and copies the corr.
*                            : text to the final location. Also sets the flag to
*                            : indicate that a error message is present. The flags
*                            : are checked according to their priorities.
* @note       Notes          : None
*****************************************************************************/
U8 populate_error_message(char * error_msg)
{
    char disp_value[ARRAY_SIZE];
    U8 err_msg_present = 0;
    //U8 Scale_number = 1;

    memset(disp_value, 0, sizeof(disp_value));
	  if(Flags_struct.MAC_Programming_Error <= 0x01 )
		{
			if ((Flags_struct.Plant_connect_record_flag & PCS_SERVER_IP_RESOLVED_OK_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					//strncpy(error_msg, Strings[Error_1].Text, strlen(Strings[Error_1].Text)+1);
					strcpy(error_msg, "PCS IP RESOLVED OK");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCS_IP_RESOLVED_MSG_TIME]++;
			}
			else if((Flags_struct.Plant_connect_record_flag & PCS_SOCKET_CONNECTION_OK_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
//					strncpy(error_msg, Strings[Error_2].Text, strlen(Strings[Error_2].Text));
					strcpy(error_msg, "PCS SOCKET STATUS : CONNECTED");				
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_MSG_TIME]++;
			}
			else if((Flags_struct.Plant_connect_record_flag & PCM_SCALE_CONFIGURATION_REQUEST_OK_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
//					strncpy(error_msg, Strings[Error_3].Text, strlen(Strings[Error_3].Text)+1);
					strcpy(error_msg, "PCM POWER ON SCALE CONFIG REQUEST OK");				
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_REQUEST_MSG_TIME]++;
			}
			else if((Flags_struct.Plant_connect_record_flag & PCM_SCALE_CONFIGURATION_RECEIVED_OK_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					strcpy(error_msg, "SCALE CONFIGURATION RECEIVED OK");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_CONFIGURATION_RECEIVE_MSG_TIME]++;
			}
			else if((Flags_struct.Plant_connect_record_flag & PCM_SCALE_DATA_UPDATE_OK_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
//					strncpy(error_msg, Strings[Error_3].Text, strlen(Strings[Error_3].Text)+1);
					sprintf(error_msg, "DATA OF SCALE %02d UPDATED ON SERVER ",disp_rx_scale_id);				
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCM_SCALE_DATA_UPDATE_MSG_TIME]++;
			}
			else if((Flags_struct.Plant_connect_record_flag & SCALE_DATA_RECEIVE_OK_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[SCALE_DATA_RECEIVE_OK_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					sprintf(error_msg, "RECEIVED DATA OF SCALE %02d ON PCM",disp_rx_scale_id);
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[SCALE_DATA_RECEIVE_OK_MSG_TIME]++;
			}	
			else if((Flags_struct.Plant_connect_record_flag & PCS_SOCKET_CONNECTION_FAIL_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_FAIL_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					strcpy(error_msg, "TCP SOCKET CONNECTION FAILED");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_CONNECTION_FAIL_MSG_TIME]++;
			}	
	
			else if((Flags_struct.Plant_connect_record_flag & PCS_SOCKET_STATUS_NC_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_STATUS_NC_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					strcpy(error_msg, "PCS SOCKET STATUS : NOT CONNECTED");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCS_SOCKET_STATUS_NC_MSG_TIME]++;
			}		
			else if((Flags_struct.Plant_connect_record_flag & PCS_SERVER_IP_RESOLVE_PROCESS_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[PCS_SERVER_IP_RESOLVE_PROCESS_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					strcpy(error_msg, "SERVER IP RESOLUTION IN PROCESS......");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[PCS_SERVER_IP_RESOLVE_PROCESS_MSG_TIME]++;
			}	
			else if((Flags_struct.Plant_connect_record_flag & SOCKET_CONNECTION_PROCESS_FLAG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[SOCKET_CONNECTION_PROCESS_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					strcpy(error_msg, "SOCKET CONNECTION IN PROCESS......");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[SOCKET_CONNECTION_PROCESS_MSG_TIME]++;
			}
			else if((Flags_struct.Plant_connect_record_flag & SERVER_IP_RESOLUTION_FAIL_FG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[SERVER_IP_RESOLVE_FAIL_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					strcpy(error_msg, "SERVER IP RESOLUTION FAILED......");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[SERVER_IP_RESOLVE_FAIL_MSG_TIME]++;
			}	
			else if((Flags_struct.Plant_connect_record_flag & MODBUS_TCP_CONNECTION_FAIL_FG) &&
					 (GUI_data_nav.Error_wrn_msg_flag[MODBUS_TCP_FAIL_MSG_TIME] < THREE_SEC_TIMEOUT_SWITCH))
			{
					strcpy(error_msg, "MODBUS TCP CONNECTION FAILED......");
					err_msg_present++;
					GUI_data_nav.Error_wrn_msg_flag[SERVER_IP_RESOLVE_FAIL_MSG_TIME]++;
			}				
			
// 			else if((Flags_struct.Error_flags & SCALE_COMM_ERR) &&
// 					 (GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_ERR_TIME] < THREE_SEC_TIMEOUT_SWITCH))
// 			{
// 					//copy till the string 'XXX.' and replace with the actual value
// 					strncpy(error_msg, Strings[Error_5].Text, 19);
// 					sprintf(disp_value, "%d", Setup_device_var.Network_Addr);
// 					strcat(error_msg, disp_value);
// 					err_msg_present++;
// 					GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_ERR_TIME]++;
// 			}
// 			else if((Flags_struct.Error_flags & NEGATIVE_RATE_ERR) &&
// 					 (GUI_data_nav.Error_wrn_msg_flag[NEGATIVE_RATE_ERR_TIME] < THREE_SEC_TIMEOUT_SWITCH))
// 			{
// 					//copy till the string 'XXX.' and replace with the actual value
// 					strncpy(error_msg, Strings[Error_6].Text, 27);
// 					sprintf(disp_value, "%0.0f", Admin_var.Negative_rate_time);
// 					strcat(error_msg, disp_value);
// 					strcat(error_msg, " seconds");
// 					err_msg_present++;
// 					GUI_data_nav.Error_wrn_msg_flag[NEGATIVE_RATE_ERR_TIME]++;
// 				  //Flags_struct.Error_flags &= ~NEGATIVE_RATE_ERR;
// 			}
// 			else if((Flags_struct.Error_flags & IO_BRD_COMM_ERR) &&
// 					 (GUI_data_nav.Error_wrn_msg_flag[IO_BRD_COMM_ERR_TIME] < THREE_SEC_TIMEOUT_SWITCH))
// 			{
// 					strncpy(error_msg, Strings[Error_7].Text, strlen(Strings[Error_7].Text) + 1);
// 					err_msg_present++;
// 					GUI_data_nav.Error_wrn_msg_flag[IO_BRD_COMM_ERR_TIME]++;
// 			}
		}
		else
		{
			switch(Flags_struct.MAC_Programming_Error)
		  {
				case 0x02:  strncpy(error_msg, Strings[mac_error1].Text, strlen(Strings[mac_error1].Text) + 1);
					          err_msg_present++;
				            break;
				case 0x03:  strncpy(error_msg, Strings[mac_error2].Text, strlen(Strings[mac_error2].Text) + 1);
					          err_msg_present++;
				            break;
				case 0x04:  strncpy(error_msg, Strings[mac_error3].Text, strlen(Strings[mac_error3].Text) + 1);
					          err_msg_present++;
				            break;
				case 0x05:  strncpy(error_msg, Strings[mac_error4].Text, strlen(Strings[mac_error4].Text) + 1);
					          err_msg_present++;
				            break;
				default:    break;
			}
		}

    return (err_msg_present);
}

/*****************************************************************************
* @note       Function name  : U8 populate_warning_message(char * warning_msg)
* @returns    returns        : 1 if a warning message is present
*                            : 0 if there is no warning message
* @param      arg1           : pointer to the location where the string is to be
*                            : stored.
* @author                    : Anagha Basole
* @date       date created   : 19th March 2013
* @brief      Description    : Checks the various warning flags and copies the corr.
*                            : text to the final location. Also sets the flag to
*                            : indicate that a warning message is present. The flags
*                            : are checked according to their priorities.
* @note       Notes          : None
*****************************************************************************/
U8 populate_warning_message(char * warning_msg)
{
    char disp_value[ARRAY_SIZE];
    U8 warn_msg_present = 0;
    U8 Scale_number = 1;
/*
    memset(disp_value, 0, sizeof(disp_value));
    if((Flags_struct.Warning_flags & LOW_SPEED_WARNING) &&
         (GUI_data_nav.Error_wrn_msg_flag[LOW_SPEED_WRN_TIME] < THREE_SEC_TIMEOUT_SWITCH))
    {
        strncpy(warning_msg, Strings[Warning_1].Text, strlen(Strings[Warning_1].Text)+1);
        warn_msg_present++;
        GUI_data_nav.Error_wrn_msg_flag[LOW_SPEED_WRN_TIME]++;
    }
    else if((Flags_struct.Warning_flags & HIGH_SPEED_WARNING) &&
         (GUI_data_nav.Error_wrn_msg_flag[HIGH_SPEED_WRN_TIME] < THREE_SEC_TIMEOUT_SWITCH))
    {
        strncpy(warning_msg, Strings[Warning_2].Text, strlen(Strings[Warning_2].Text)+1);
        warn_msg_present++;
        GUI_data_nav.Error_wrn_msg_flag[HIGH_SPEED_WRN_TIME]++;
    }
    else if((Flags_struct.Warning_flags & ZERO_CAL_CHANGE_WARNING) &&
         (GUI_data_nav.Error_wrn_msg_flag[ZERO_CAL_CHANGE_WRN_TIME] < THREE_SEC_TIMEOUT_SWITCH))
    {
        strncpy(warning_msg, Strings[Warning_3].Text, strlen(Strings[Warning_3].Text)+1);
        warn_msg_present++;
        GUI_data_nav.Error_wrn_msg_flag[ZERO_CAL_CHANGE_WRN_TIME]++;
    }
    else if((Flags_struct.Warning_flags & SEN_BRD_COMM_WARNING) &&
         (GUI_data_nav.Error_wrn_msg_flag[SEN_BRD_COMM_WRN_TIME] < THREE_SEC_TIMEOUT_SWITCH))
    {
        strncpy(warning_msg, Strings[Warning_4].Text, strlen(Strings[Warning_4].Text)+1);
        warn_msg_present++;
        GUI_data_nav.Error_wrn_msg_flag[SEN_BRD_COMM_WRN_TIME]++;
    }
    else if((Flags_struct.Warning_flags & SCALE_COMM_WARNING) &&
         (GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_WRN_TIME] < THREE_SEC_TIMEOUT_SWITCH))
    {
        //copy till the string 'XXX.' and replace with the actual value
        strncpy(warning_msg, Strings[Warning_5].Text, 25);
        sprintf(disp_value, "%d", Scale_number);
        strcat(warning_msg, disp_value);
        warn_msg_present++;
        GUI_data_nav.Error_wrn_msg_flag[SCALE_COMM_WRN_TIME]++;
    }
    else if((Flags_struct.Warning_flags & LOW_RATE_WARNING) &&
         (GUI_data_nav.Error_wrn_msg_flag[LOW_RATE_WRN_TIME] < THREE_SEC_TIMEOUT_SWITCH))
    {
        strncpy(warning_msg, Strings[Warning_6].Text, strlen(Strings[Warning_6].Text)+1);
        warn_msg_present++;
        GUI_data_nav.Error_wrn_msg_flag[LOW_RATE_WRN_TIME]++;
    }
    else if((Flags_struct.Warning_flags & HIGH_RATE_WARNING) &&
         (GUI_data_nav.Error_wrn_msg_flag[HIGH_RATE_WRN_TIME] < THREE_SEC_TIMEOUT_SWITCH))
    {
        strncpy(warning_msg, Strings[Warning_7].Text, strlen(Strings[Warning_7].Text)+1);
        warn_msg_present++;
        GUI_data_nav.Error_wrn_msg_flag[HIGH_RATE_WRN_TIME]++;
    }
*/
    return(warn_msg_present);
}

/*****************************************************************************
* @note       Function name  : void Display_Footer(int Display_Text_Once)
* @returns    returns        : None
* @param      arg1           : Flag to indicate the data to be updated once
* @author                    : Pandurang Khutal
* @date       date created   : 26th March 2013
* @brief      Description    : Displays the error/warning messages according to
*                            : the flags in the message corner. Also displays the
*                            : icons depending on the current status of the USB
*                            : and ethernet connections.
* @note       Notes          : None
*****************************************************************************/
void Display_Footer(int Display_Text_Once)
{
    char disp[100];
    U8 wrn_msg_nos = 0;
    U8 err_msg_nos = 0;
    int i;
    static U16 IO_Err_ackn_timer = 0;
    GUI_SetColor(GUI_WHITE);
    GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold);
    GUI_SetTextAlign(GUI_TA_TOP | GUI_TA_LEFT);

    if(Display_Text_Once == 0)
    {
        GUI_DispStringAt("Message:", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);
        GUI_DrawLine(WINDOW_FOOTER_LINE_POS_X0,WINDOW_FOOTER_LINE_POS_Y0,WINDOW_FOOTER_LINE_POS_X1,WINDOW_FOOTER_LINE_POS_Y1);
    }

    memset(disp, 0, sizeof(disp));
    err_msg_nos = populate_error_message(&disp[0]);
    if (err_msg_nos == 0)
    {
        wrn_msg_nos = populate_warning_message(&disp[0]);
    }
    if((err_msg_nos == 0) && (wrn_msg_nos == 0))
    {
        reset_error_warning_message_timers();
        for(i=0; i<sizeof(GUI_data_nav.Error_wrn_msg_flag); i++)
        {
           GUI_data_nav.Error_wrn_msg_flag[i] = 0;
        }
    }
    GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);

    if(((err_msg_nos >= 1) || (wrn_msg_nos >= 1)))// && (IO_board_param.Error_ack_flag == __FALSE))
    {
       GUI_DispStringAt(disp, WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
    }
    else
    {
       GUI_DispStringAt(disp, WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
    }
// 		if(IO_board_param.Error_ack_flag == __TRUE)
// 		{
// 			IO_Err_ackn_timer++;
// 			if(IO_Err_ackn_timer == 1)
// 			{
// 				GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
// 				memset(GUI_data_nav.Error_wrn_msg_flag,0,sizeof(GUI_data_nav.Error_wrn_msg_flag));
// 			}
// 			else if(IO_Err_ackn_timer > ERROR_ACK_TIMER_CYCLE )
// 			{ 
// 				IO_Err_ackn_timer = 0;
// 			}
//     }
// 		else
// 		{
// 		  IO_Err_ackn_timer = 0;
// 		}

//display the ethernet icon if ethernet is connected and initialized
    if(Flags_struct.Connection_flags & ETH_CON_FLAG)
    {
        GUI_DrawBitmap(&bmsmallgreenethernetactivityicon, WINDOW_FOOTER_ICON1_POS_X0, WINDOW_FOOTER_ICON1_POS_Y0);
    }
    else
    {
        GUI_DrawBitmap(&bmblack_image, WINDOW_FOOTER_ICON1_POS_X0, WINDOW_FOOTER_ICON1_POS_Y0);
    }
	
    //display the USB icon if USB is connected and initialized
    //if(con)
    if(Flags_struct.Connection_flags & USB_CON_FLAG)
    {
        GUI_DrawBitmap(&bmUSB_small, WINDOW_FOOTER_ICON2_POS_X0, WINDOW_FOOTER_ICON2_POS_Y0);
    }
    else
    {
        GUI_DrawBitmap(&bmblack_image, WINDOW_FOOTER_ICON2_POS_X0, WINDOW_FOOTER_ICON2_POS_Y0);
				GUI_DrawBitmap(&bmblack_image, WINDOW_FOOTER_ICON2_POS_X0+17, WINDOW_FOOTER_ICON2_POS_Y0);
    }

    return;
}

/*****************************************************************************
* @note       Function name  : void screen_display (Screen_org_data * current_screen_org_no)
* @returns    returns        : None
* @param      arg1           : pointer to the current screen organization number
* @author                    : Anagha Basole
* @date       date created   : 26th December 2012
* @brief      Description    : Populates the screen header and footer as per the
*                            : current screen org number. Also creates the screen
*                            : depending on the type of screen and org number.
*                            : Deallocates the memory used by the previous screen.
*                            :
* @note       Notes          : None
*****************************************************************************/
void screen_display (Screen_org_data * current_screen_org_no)
{
    GUI_data_nav.Current_screen_info = current_screen_org_no;

    delete_widgets_mem_free();

    WM_SelectWindow(WM_HBKWIN);
    GUI_SetBkColor(GUI_BLACK);
    GUI_Clear();

    Display_Header(0);
    Display_Footer(0);

    //Display screen as per type
    switch (current_screen_org_no->Screen_type)
    {
        case Screen1:
					  Init_STACK(wiz_nav_stack);
            CreateWindow_screen1(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen2:
            CreateWindow_screen2(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen3:
            CreateWindow_screen3(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen4:
//            CreateWindow_screen4(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen5:
            CreateWindow_screen5(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen6:
            CreateWindow_screen6(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen9:
            CreateWindow_screen9(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen10:
//            CreateWindow_screen10(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Screen11:
					  GUI_data_nav.Percent_complete = 0;
//            CreateWindow_screen11(current_screen_org_no->screen_data, current_screen_org_no->size_of_screen_data);
            break;

        case Weight_rate_run_mode_screen:
        case Loadout_run_mode_screen:
        case Blending_load_rate_run_mode_screen:
					  Init_STACK(wiz_nav_stack);
//            CreateWindow_runmode_screen(CREATE_SCREEN);
            break;
        case PCM_main_screen:
						Create_pcm_main_screen(CREATE_SCREEN);
						break;
    }

    return;
}

/*****************************************************************************
* @note       Function name  : Screen_org_data * screen_data_create (char screen_org_num[6])
* @returns    returns        : A pointer to the structure containing the screen info
* @param      arg1           : The org number of the screen to be drwan
* @author                    : Anagha Basole
* @date       date created   : 26th November 2012
* @brief      Description    : Starting with the first location, the function traverses
*                            : structure from parent to child/sibling, to find the
*                            : org number of the screen to be drawn. The address of
*                            : structure in which the string is found is returned.
*                            : Maximum 6 levels are traversed.
* @note       Notes          : None
*****************************************************************************/
Screen_org_data * screen_data_create (char screen_org_num[6])
{
    Screen_org_data * current_screen_org_no;	
		unsigned int index = 0;
    if(strncmp(screen_org_num, PCM_SCREEN_ORG_NO, 1) == 0)
    {
 				current_screen_org_no = Org_800000_parent;	       
		}
	
		else
		{
        current_screen_org_no = Org_000000_parent;
        if (screen_org_num[0] != '0') //level 0
        {
            if(screen_org_num[0] >= 65 )
						{
							index = (screen_org_num[0] - 55);
						}
						else
						{
					    index = (screen_org_num[0] - 48);
						}
            if (index > (current_screen_org_no->nos_of_children))
            {
                return(NULL);
            }
            current_screen_org_no = current_screen_org_no->child;
            current_screen_org_no = &current_screen_org_no[index-1];
            if (screen_org_num[1] != '0') //level 1
            {
                if(screen_org_num[1] >= 65 )
						    {
							     index = (screen_org_num[1] - 55);
						    }
						    else
						    {
					         index = (screen_org_num[1] - 48);
						    }
                if (index > (current_screen_org_no->nos_of_children))
                {
                    return(NULL);
                }
                current_screen_org_no = current_screen_org_no->child;
                current_screen_org_no = &current_screen_org_no[index-1];
                if (screen_org_num[2] != '0') //level 2
                {
                     if(screen_org_num[2] >= 65 )
						         {
							        index = (screen_org_num[2] - 55);
						         }
						         else
						         {
					            index = (screen_org_num[2] - 48);
						         }
                    if (index > (current_screen_org_no->nos_of_children))
                    {
                        return(NULL);
                    }
                    current_screen_org_no = current_screen_org_no->child;
                    current_screen_org_no = &current_screen_org_no[index-1];
                    if (screen_org_num[3] != '0') //level 3
                    {
                         if(screen_org_num[3] >= 65 )
						             {
							            index = (screen_org_num[3] - 55);
						             }
						             else
						             {
					                index = (screen_org_num[3] - 48);
						             }
                        if (index > (current_screen_org_no->nos_of_children))
                        {
                            return(NULL);
                        }
                        current_screen_org_no = current_screen_org_no->child;
                        current_screen_org_no = &current_screen_org_no[index-1];
                        if (screen_org_num[4] != '0') //level 4
                        {
                             if(screen_org_num[4] >= 65 )
						                 {
							                 index = (screen_org_num[4] - 55);
						                 }
						                 else
						                 {
					                     index = (screen_org_num[4] - 48);
						                 }
                            if (index > (current_screen_org_no->nos_of_children))
                            {
                                return(NULL);
                            }
                            current_screen_org_no = current_screen_org_no->child;
                            current_screen_org_no = &current_screen_org_no[index-1];
                            if (screen_org_num[5] != '0') //level 5
                            {
                                 if(screen_org_num[5] >= 65 )
						                     {
							                    index = (screen_org_num[5] - 55);
						                     }
						                     else
						                     {
					                        index = (screen_org_num[5] - 48);
						                     }
                                if (index > (current_screen_org_no->nos_of_children))
                                {
                                    return(NULL);
                                }
                                current_screen_org_no = current_screen_org_no->child;
                                current_screen_org_no = &current_screen_org_no[index-1];
                            } //level 5
                        } //level 4
                    }//level 3
                } //level 2
            }//level 1
        } //level 0
				
			}
    return(current_screen_org_no);
}

/*****************************************************************************
* @note       Function name  : int _OwnerDraw_Screen23456(const WIDGET_ITEM_DRAW_INFO * pDrawItemInfo)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to draw rectangle over the listbox and also to add the tooltip
*                            : text. It also changes the color of the text when selected and unselected.
*                            : Also adds the arrows when sub screens are present. Adds variables to the
*                            : screen wherever present. Adds images wherever required to the listbox.
*                            :
* @note       Notes          : This is the owner draw function.
*                            : It allows complete customization of how the items in the listbox are
*                            : drawn. A command specifies what the function should do;
*                            : The minimum is to react to the draw command (WIDGET_ITEM_DRAW);
*                            : If the item x-size differs from the default, then this information
*                            : needs to be returned in reaction to WIDGET_ITEM_GET_XSIZE.
*                            : To insure compatibility with future version, all unhandled commands
*                            :  must call the default routine LISTBOX_OwnerDraw.
*****************************************************************************/
int _OwnerDraw_Screen23456(const WIDGET_ITEM_DRAW_INFO * pDrawItemInfo)
{
    WM_HWIN hWin;
    int Index, sel, small_text_offset;
    char acBuffer[50], Buffer[ARRAY_SIZE], char_data1[ARRAY_SIZE];
    const GUI_FONT * OldFont;
    static SCREEN_TYPE2_DATA * data2;
    static SCREEN_TYPE3_DATA * data3;
    static SCREEN_TYPE4_DATA * data4;
    static SCREEN_TYPE5_DATA * data5;
    static SCREEN_TYPE6_DATA * data6;
    static SCREEN_TYPE9_DATA * data9;
    static SCREEN_TYPE10_DATA * data10;
    static SCREEN_TYPE11_DATA * data11;
    static LOADOUT_RUNMODE_SCREEN_DATA * data13;
    static BLEND_RATE_LOAD_CTRL_RUNMODE_SCREEN_DATA * data14;
    char *dchar_data1;
    //float error_pct = 0;

    hWin     = pDrawItemInfo->hWin;
    Index    = pDrawItemInfo->ItemIndex;

    switch (pDrawItemInfo->Cmd)
    {
      case WIDGET_ITEM_GET_XSIZE:
        return (LISTBOX_RECT_STD_X);

      case WIDGET_ITEM_GET_YSIZE:
        return (LISTBOX_RECT_STD_Y);

      case WIDGET_ITEM_DRAW:

						if (((GUI_data_nav.Current_screen_info->Screen_type == Screen2) &&
               ((unsigned int)pDrawItemInfo->ItemIndex < GUI_data_nav.Current_screen_info->size_of_screen_data)) ||
               ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) == 0) ||
                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) == 0)))
            {
							
                LISTBOX_GetItemText(pDrawItemInfo->hWin, pDrawItemInfo->ItemIndex, acBuffer, sizeof(acBuffer));
                sel = LISTBOX_GetSel(hWin);

                data2 = GUI_data_nav.Current_screen_info->screen_data;


                small_text_offset = GUI_GetFontDistY() + (2 * TEXT_OFFSET);
                GUI_SetBkColor(GUI_BLACK);

                //For each individual rectangle
                GUI_SetColor(GUI_TRANSPARENT);
                GUI_FillRect(pDrawItemInfo->x0, pDrawItemInfo->y0, (pDrawItemInfo->x0 + LISTBOX2_RECT_STD_X),
                            (pDrawItemInfo->y0 + LISTBOX2_RECT_STD_Y));
                GUI_SetColor(GUI_WHITE);
                GUI_DrawRect(pDrawItemInfo->x0 + FILL_RECT_OFFSET, pDrawItemInfo->y0,
                             (pDrawItemInfo->x0 + LISTBOX2_RECT_STD_X),
                            (pDrawItemInfo->y0 + LISTBOX2_RECT_STD_Y - FILL_RECT_OFFSET));

                if ((pDrawItemInfo->ItemIndex == sel))
                {
										GUI_SetBkColor(GUI_LIGHTYELLOW);
                    GUI_SetColor(GUI_LIGHTYELLOW);
                    GUI_FillRect((pDrawItemInfo->x0 + RECTANGLE_OFFSET), (pDrawItemInfo->y0 + RECTANGLE_OFFSET),
                                  (pDrawItemInfo->x0 + LISTBOX2_RECT_FILL_X - FILL_RECT_OFFSET),
                                   (pDrawItemInfo->y0 + LISTBOX2_RECT_FILL_Y + FILL_RECT_OFFSET));
                    GUI_SetColor(GUI_BLACK);
                    GUI_DispStringAt(acBuffer, pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + TEXT_OFFSET);

                    memset(Buffer, '\0', sizeof(Buffer));
                    strcpy(&Buffer[0]," ");
                    if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, ASSIGN_OUTPUT_ORG_NO) == 0)
                    {
                        if (pDrawItemInfo->ItemIndex == 0)
                        {
//                          display_sec_func_string(ASSIGN_OUTPUT1, Buffer);
                        }
                        else if (pDrawItemInfo->ItemIndex == 1)
                        {
//                          display_sec_func_string(ASSIGN_OUTPUT2, Buffer);
                        }
                        else
                        {
//                          display_sec_func_string(ASSIGN_OUTPUT3, Buffer);
                        }
                    }
                    else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, ASSIGN_RELAY_ORG_NO) == 0)
                    {
                        if (pDrawItemInfo->ItemIndex == 0)
                        {
//                          display_sec_func_string(ASSIGN_RELAY1, Buffer);
                        }
                        else if (pDrawItemInfo->ItemIndex == 1)
                        {
//                          display_sec_func_string(ASSIGN_RELAY2, Buffer);
                        }
                        else
                        {
//                          display_sec_func_string(ASSIGN_RELAY3, Buffer);
                        }
                    }
                    else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) != 0) &&
                             (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) != 0))
                    {
                        if (data2[Index].Variable3 != NULL)
                        {
														custom_sprintf(data2[Index].data_type, data2[Index].Variable3, char_data1, sizeof(char_data1));
														strcat(Buffer,char_data1);
                            strcat(Buffer, " ");
                        }
                        if (data2[Index].Variable1 != 0)
                        {
                            dchar_data1 = Strings[*data2[Index].Variable1].Text;
                            strcat(Buffer, dchar_data1);
                            strcat(Buffer, " ");
                        }
                        if (data2[Index].Variable2 != 0)
                        {
                            strcat(Buffer, "/ ");
                            dchar_data1 = Strings[*data2[Index].Variable2].Text;
                            strcat(Buffer, dchar_data1);
                        }
                    }
                    if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) != 0) &&
                        (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) != 0))
                    {
                       GUI_DispStringAt(Buffer, pDrawItemInfo->x0 + VARIABLE_OFFSET, pDrawItemInfo->y0 + TEXT_OFFSET);
                       if (data2[Index].screen_an_option != 0)
                       {
                           GUI_DrawBitmap(&_bmArrowRight1, pDrawItemInfo->x0 + ARROW_OFFSET_X,
                                          pDrawItemInfo->y0 + ARROW_OFFSET_Y);
                       }
                    }
                }
                else
                {
                    GUI_DispStringAt(acBuffer, pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + TEXT_OFFSET);
                    memset(Buffer, '\0', sizeof(Buffer));
                    strcpy(&Buffer[0]," ");

                    if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, ASSIGN_OUTPUT_ORG_NO) == 0)
                    {
                        if (pDrawItemInfo->ItemIndex == 0)
                        {
//                          display_sec_func_string(ASSIGN_OUTPUT1, Buffer);
                        }
                        else if (pDrawItemInfo->ItemIndex == 1)
                        {
//                          display_sec_func_string(ASSIGN_OUTPUT2, Buffer);
                        }
                        else
                        {
//                          display_sec_func_string(ASSIGN_OUTPUT3, Buffer);
                        }
                    }
                    else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, ASSIGN_RELAY_ORG_NO) == 0)
                    {
                        if (pDrawItemInfo->ItemIndex == 0)
                        {
//                          display_sec_func_string(ASSIGN_RELAY1, Buffer);
                        }
                        else if (pDrawItemInfo->ItemIndex == 1)
                        {
//                          display_sec_func_string(ASSIGN_RELAY2, Buffer);
                        }
                        else
                        {
//                          display_sec_func_string(ASSIGN_RELAY3, Buffer);
                        }
                    }
                    else if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) != 0) &&
                             (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) != 0))
                    {
                        if (data2[Index].Variable3 != NULL)
                        {
                           custom_sprintf(data2[Index].data_type, data2[Index].Variable3, char_data1, sizeof(char_data1));
                           strcat(Buffer,char_data1);
                           strcat(Buffer, " ");
                        }
                        if (data2[Index].Variable1 != 0)
                        {
                            dchar_data1 = Strings[*data2[Index].Variable1].Text;
                            strcat(Buffer, dchar_data1);
                            strcat(Buffer, " ");
                        }
                        if (data2[Index].Variable2 != 0)
                        {
                            strcat(Buffer, "/ ");
                            dchar_data1 = Strings[*data2[Index].Variable2].Text;
                            strcat(Buffer, dchar_data1);
                        }
                    }
                    if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) != 0) &&
                        (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) != 0))
                    {
                       GUI_DispStringAt(Buffer, pDrawItemInfo->x0 + VARIABLE_OFFSET, pDrawItemInfo->y0 + TEXT_OFFSET);
                       if (data2[Index].screen_an_option != 0)
                       {
                           GUI_DrawBitmap(&_bmArrowRight1, pDrawItemInfo->x0 + ARROW_OFFSET_X,
                                          pDrawItemInfo->y0 + ARROW_OFFSET_Y);
                       }
                    }
                    GUI_SetColor(GUI_YELLOW);
                }
								if ((GUI_data_nav.Current_screen_info->Screen_org_no[0] == '5')&&
										(GUI_data_nav.Current_screen_info->Screen_org_no[1] == '4')&&
										(GUI_data_nav.Current_screen_info->Screen_org_no[2] == '0')&&
										(pDrawItemInfo->ItemIndex == 2))
								{

													{
															GUI_SetFont(&GUI_FontArial_Unicode_MS24_Bold);
															GUI_SetBkColor(GUI_GREEN);
															GUI_SetColor(GUI_GREEN);

															if (Rprts_diag_var.Tolerance_err < 0)
															{
																	Rprts_diag_var.Tolerance_err *= (-1);
															}
															if (Rprts_diag_var.Tolerance_err > Rprts_diag_var.Calib_err_pct)
															{
																	GUI_SetBkColor(GUI_RED);
																	GUI_SetColor(GUI_RED);
															}
															GUI_FillRect((pDrawItemInfo->x0 + RECTANGLE_OFFSET), (pDrawItemInfo->y0 + RECTANGLE_OFFSET),
																						(pDrawItemInfo->x0 + LISTBOX_RECT_FILL_X - FILL_RECT_OFFSET),
																						(pDrawItemInfo->y0 + LISTBOX_RECT_FILL_Y + FILL_RECT_OFFSET));
															GUI_SetColor(GUI_BLACK);
															GUI_DispStringAt(acBuffer, pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + TEXT_OFFSET);
															OldFont = GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold); // Buffer old font
													}

								}				
								
                OldFont = GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold); // Buffer old font
                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) == 0) ||
                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) == 0))
                {
                    GUI_DispStringAt(Strings[data2[0].Tooltip_index].Text,
                                    pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                    GUI_SetFont(OldFont);
                }
                else if (data2[Index].Tooltip_index != 0)
                {
                    GUI_DispStringAt(Strings[data2[Index].Tooltip_index].Text, pDrawItemInfo->x0 + TEXT_OFFSET,
                                    pDrawItemInfo->y0 + small_text_offset);
                    GUI_SetFont(OldFont);
                }
            }
            else //for screen types other than screen type 2
            {
                LISTBOX_GetItemText(pDrawItemInfo->hWin, pDrawItemInfo->ItemIndex, acBuffer, sizeof(acBuffer));
                sel = LISTBOX_GetSel(hWin);
                small_text_offset = GUI_GetFontDistY() + (2 * TEXT_OFFSET);
                GUI_SetBkColor(GUI_BLACK);

                GUI_SetColor(GUI_TRANSPARENT);
                GUI_FillRect(pDrawItemInfo->x0, pDrawItemInfo->y0, (pDrawItemInfo->x0 + LISTBOX_RECT_STD_X),
                                (pDrawItemInfo->y0 + LISTBOX_RECT_STD_Y));
                GUI_SetColor(GUI_WHITE);
                GUI_DrawRect(pDrawItemInfo->x0 + FILL_RECT_OFFSET, pDrawItemInfo->y0,
                                (pDrawItemInfo->x0 + LISTBOX_RECT_STD_X),
                                (pDrawItemInfo->y0 + LISTBOX_RECT_STD_Y - FILL_RECT_OFFSET));

                if ((pDrawItemInfo->ItemIndex == sel))
                {
                    GUI_SetBkColor(GUI_LIGHTYELLOW);
                    GUI_SetColor(GUI_LIGHTYELLOW);
                    GUI_FillRect((pDrawItemInfo->x0 + RECTANGLE_OFFSET), (pDrawItemInfo->y0 + RECTANGLE_OFFSET),
                                  (pDrawItemInfo->x0 + LISTBOX_RECT_FILL_X - FILL_RECT_OFFSET),
                                  (pDrawItemInfo->y0 + LISTBOX_RECT_FILL_Y + FILL_RECT_OFFSET));
                    GUI_SetColor(GUI_BLACK);
                    GUI_DispStringAt(acBuffer, pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + TEXT_OFFSET);
                }
                else
                {
                    GUI_DispStringAt(acBuffer, pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + TEXT_OFFSET);
                    GUI_SetColor(GUI_YELLOW);
                }
                OldFont = GUI_SetFont(&GUI_FontArial_Unicode_MS16_Bold); // Buffer old font

                switch (GUI_data_nav.Current_screen_info->Screen_type)
                {
                    case Screen3:
                        data3 = GUI_data_nav.Current_screen_info->screen_data;
                        if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_UNIT_SCREEN) == 0) &&
                            (Scale_setup_var.Distance_unit == Screen24_str5))//If Metric = distance_unit
                        {
                            switch(Index)
                            {
                               case 0:
                                 GUI_DispStringAt(Strings[data3[4].Tooltip_index].Text,
                                              pDrawItemInfo->x0 + TEXT_OFFSET,
                                              pDrawItemInfo->y0 + small_text_offset);
                                 break;

                               case 1:
                                 /*GUI_DispStringAt(Strings[data3[5].Tooltip_index].Text,
                                              pDrawItemInfo->x0 + TEXT_OFFSET,
                                              pDrawItemInfo->y0 + small_text_offset);*/
                                 break;
                            }
                        }
                        else if((GUI_data_nav.Current_screen_info->Screen_org_no[0] == '6') &&
                            (GUI_data_nav.Current_screen_info->Screen_org_no[1] == '3') &&
                           ((GUI_data_nav.Current_screen_info->Screen_org_no[2] == '1') ||
                             (GUI_data_nav.Current_screen_info->Screen_org_no[2] == '2') ||
                             (GUI_data_nav.Current_screen_info->Screen_org_no[2] == '3') ||
                             (GUI_data_nav.Current_screen_info->Screen_org_no[2] == '4') ||
                             (GUI_data_nav.Current_screen_info->Screen_org_no[2] == '5') ||
                             (GUI_data_nav.Current_screen_info->Screen_org_no[2] == '6')))
                        {
                            if (pDrawItemInfo->ItemIndex == 0)
                            {
                                GUI_DrawBitmap(&bmredlock, RED_LOCK_IMAGE_X_POS, RED_LOCK_IMAGE_Y_POS);
                            }
                            else
                            {
                                GUI_DrawBitmap(&bmgreenunlockicon, GRN_UNLOCK_IMAGE_X_POS, GRN_UNLOCK_IMAGE_Y_POS);
                            }
                        }
                        else
                        {
                            if (data3[Index+1].Tooltip_index != 0)
                            {
                                GUI_DispStringAt(Strings[data3[Index+1].Tooltip_index].Text,
                                                  pDrawItemInfo->x0 + TEXT_OFFSET,
                                                  pDrawItemInfo->y0 + small_text_offset);
                            }
                        }
                        break;

                    case Screen4:
                        data4 = GUI_data_nav.Current_screen_info->screen_data;
                        if (data4[Index+1].Tooltip_index != 0)
                        {
                            GUI_DispStringAt(Strings[data4[Index+1].Tooltip_index].Text,
                                            pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                        }
                        break;

                    case Screen5:
                        data5 = GUI_data_nav.Current_screen_info->screen_data;
										
                        if (data5[Index].Tooltip_index != 0)
                        {
                            GUI_DispStringAt(Strings[data5[Index].Tooltip_index].Text,
                                            pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                        }
                        break;

                    case Screen6:
                        data6 = GUI_data_nav.Current_screen_info->screen_data;
                        if (data6[Index].Tooltip_index != 0)
                        {
                            GUI_DispStringAt(Strings[data6[Index].Tooltip_index].Text,
                                            pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                        }
                        break;

                    case Screen9:
                        data9 = GUI_data_nav.Current_screen_info->screen_data;
                        if (data9[Index].Tooltip_index != 0)
                        {
                            GUI_DispStringAt(Strings[data9[Index].Tooltip_index].Text,
                                             pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                        }
                        break;

                    case Screen10:
                        data10 = GUI_data_nav.Current_screen_info->screen_data;
                        if (data10[Index].Tooltip_index != 0)
                        {
                            GUI_DispStringAt(Strings[data10[Index].Tooltip_index].Text,
                                            pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                        }
                        break;

                    case Screen11:
                        data11 = GUI_data_nav.Current_screen_info->screen_data;
                        if (data11[Index].Tooltip_index != 0)
                        {
                            GUI_DispStringAt(Strings[data11[Index].Tooltip_index].Text,
                                            pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                        }
                        break;

                    case Loadout_run_mode_screen:
                        data13 = GUI_data_nav.Current_screen_info->screen_data;
                        memset(acBuffer, 0, sizeof(acBuffer));
                        strcpy(acBuffer, Strings[data13[0].Text_index1].Text);
                        switch (Index)
                        {
                            case 0:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_1);
                                break;

                            case 1:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_2);
                                break;

                            case 2:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_3);
                                break;

                            case 3:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_4);
                                break;

                            case 4:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_5);
                                break;

                            case 5:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_6);
                                break;

                            case 6:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_7);
                                break;

                            case 7:
                                sprintf(char_data1, "%0.3f", Setup_device_var.Cutoff_8);
                                break;
                        }
                        strcat(acBuffer, char_data1);
                        strcat(acBuffer, " ");
                        strcat(acBuffer, Strings[Scale_setup_var.Weight_unit].Text);
                        if(Index == 8)
                        {
                          memset(acBuffer, 0, sizeof(acBuffer));
                          strcpy(acBuffer, Strings[data13[1].Text_index2].Text);
                        }
                        GUI_DispStringAt(acBuffer, pDrawItemInfo->x0 + TEXT_OFFSET,
                                                   pDrawItemInfo->y0 + small_text_offset);
                        break;

                    case Blending_load_rate_run_mode_screen:
                        data14 = GUI_data_nav.Current_screen_info->screen_data;
                        if (data14[Index].Tooltip_index != 0)
                        {
                            GUI_DispStringAt(Strings[data14[Index].Tooltip_index].Text,
                                            pDrawItemInfo->x0 + TEXT_OFFSET, pDrawItemInfo->y0 + small_text_offset);
                        }
                        break;
                }
                GUI_SetFont(OldFont);
            }
            break;

        default:
            return LISTBOX_OwnerDraw(pDrawItemInfo);
    }
    return 0;
}
void display_sec_func_string(U8 type, char * data)
{
    char * var_data, * speed_data, * rate_data, * error_data;
    char char_data[ARRAY_SIZE];
    unsigned int speed_setpoint = 0, rate_setpoint = 0, pulse_on_time = 0;

	  memset(char_data, 0, sizeof(char_data));
    switch (type)
    {
        case ASSIGN_OUTPUT1:
            var_data = Strings[Setup_device_var.Digital_Output_1_Function].Text;
            speed_data = Strings[Setup_device_var.Speed_Output_Type_1].Text;
            rate_data = Strings[Setup_device_var.Rate_Output_Type_1].Text;
            error_data = Strings[Setup_device_var.error_alarm_1].Text;
            speed_setpoint = Setup_device_var.MinMax_Speed_Setpoint_1;
            rate_setpoint = Setup_device_var.MinMax_Rate_Setpoint_1;
            pulse_on_time = Setup_device_var.Pulse_on_time_1;
            break;

        case ASSIGN_OUTPUT2:
            var_data = Strings[Setup_device_var.Digital_Output_2_Function].Text;
            speed_data = Strings[Setup_device_var.Speed_Output_Type_2].Text;
            rate_data = Strings[Setup_device_var.Rate_Output_Type_2].Text;
            error_data = Strings[Setup_device_var.error_alarm_2].Text;
            speed_setpoint = Setup_device_var.MinMax_Speed_Setpoint_2;
            rate_setpoint = Setup_device_var.MinMax_Rate_Setpoint_2;
            pulse_on_time = Setup_device_var.Pulse_on_time_2;
            break;

        case ASSIGN_OUTPUT3:
            var_data = Strings[Setup_device_var.Digital_Output_3_Function].Text;
            speed_data = Strings[Setup_device_var.Speed_Output_Type_3].Text;
            rate_data = Strings[Setup_device_var.Rate_Output_Type_3].Text;
            error_data = Strings[Setup_device_var.error_alarm_3].Text;
            speed_setpoint = Setup_device_var.MinMax_Speed_Setpoint_3;
            rate_setpoint = Setup_device_var.MinMax_Rate_Setpoint_3;
            pulse_on_time = Setup_device_var.Pulse_on_time_3;
            break;

        case ASSIGN_RELAY1:
            var_data = Strings[Setup_device_var.Relay_Output_1_Function].Text;
            speed_data = Strings[Setup_device_var.Speed_Output_Type_4].Text;
            rate_data = Strings[Setup_device_var.Rate_Output_Type_4].Text;
            error_data = Strings[Setup_device_var.error_alarm_4].Text;
            speed_setpoint = Setup_device_var.MinMax_Speed_Setpoint_4;
            rate_setpoint = Setup_device_var.MinMax_Rate_Setpoint_4;
            break;

        case ASSIGN_RELAY2:
            var_data = Strings[Setup_device_var.Relay_Output_2_Function].Text;
            speed_data = Strings[Setup_device_var.Speed_Output_Type_5].Text;
            rate_data = Strings[Setup_device_var.Rate_Output_Type_5].Text;
            error_data = Strings[Setup_device_var.error_alarm_5].Text;
            speed_setpoint = Setup_device_var.MinMax_Speed_Setpoint_5;
            rate_setpoint = Setup_device_var.MinMax_Rate_Setpoint_5;
            break;

        case ASSIGN_RELAY3:
            var_data = Strings[Setup_device_var.Relay_Output_3_Function].Text;
            speed_data = Strings[Setup_device_var.Speed_Output_Type_6].Text;
            rate_data = Strings[Setup_device_var.Rate_Output_Type_6].Text;
            error_data = Strings[Setup_device_var.error_alarm_6].Text;
            speed_setpoint = Setup_device_var.MinMax_Speed_Setpoint_6;
            rate_setpoint = Setup_device_var.MinMax_Rate_Setpoint_6;
            break;
    }

    if ((strcmp(var_data, Strings[MIN_MAX_SPEED_STR].Text) == 0) ||
         (strcmp(var_data, Strings[MIN_MAX_RATE_STR].Text) == 0))
    {
        //if the output type is min/max speed, then the string should be specific min speed or max speed
        if (strcmp(var_data, Strings[Screen4421_str11].Text) == 0)
        {
            strcpy(data, speed_data);
            strcat(data, " / ");
            custom_sprintf(unsigned_int_type, &speed_setpoint, char_data, sizeof(char_data));
            strcat(data, char_data);
        }
        //if the output type is min/max rate, then the string should be specific min rate or max rate
        else
        {
            strcpy(data, rate_data);
            strcat(data, " / ");
            custom_sprintf(unsigned_int_type, &rate_setpoint, char_data, sizeof(char_data));
            strcat(data, char_data);
        }
    }
    else
    {
        strcpy(data, var_data);
        if (strcmp(var_data, Strings[PULSED_OUTPUT_STR].Text) == 0) //pulsed_output
        {
            strcat(data, " / ");
            custom_sprintf(unsigned_int_type, &pulse_on_time, char_data, sizeof(char_data));
            strcat(data, char_data);
        }
        if (strcmp(var_data, Strings[EEROR_ALARM_STR].Text) == 0) //error alarm
        {
            strcat(data, " / ");
            strcat(data, error_data);
        }
    }
    return;
}
float Unit_per_pulse_value (char * Unit_per_pulse_string)
{
	  float Wt_per_pulse = 0.01;

		  //0.1 Weight Units
		if (strcmp(Unit_per_pulse_string, Strings[FIFTH_WT_UNIT_PER_PULSE_STR].Text) == 0)
		{
			 Wt_per_pulse = 0.01;
		}
	  //0.1 Weight Units
		else if (strcmp(Unit_per_pulse_string, Strings[FIRST_WT_UNIT_PER_PULSE_STR].Text) == 0)
		{
			 Wt_per_pulse = 0.1;
		}
		//1.0 Weight Units
		else if (strcmp(Unit_per_pulse_string, Strings[SECOND_WT_UNIT_PER_PULSE_STR].Text) == 0)
		{
			 Wt_per_pulse = 1.0;
		}
		//10 Weight Units
		else if(strcmp(Unit_per_pulse_string, Strings[THIRD_WT_UNIT_PER_PULSE_STR].Text) == 0)
		{
			 Wt_per_pulse = 10;
		}
		//100 Weight Units
		else if(strcmp(Unit_per_pulse_string, Strings[FOURTH_WT_UNIT_PER_PULSE_STR].Text) == 0)
		{
			 Wt_per_pulse = 100;
		}
		return(Wt_per_pulse);
}	

void read_files_on_usb (int type)
{
    #if 1
    char *mask;
    FINFO info;
    U16 previous_id = 0;
  
    if (type == 1)
    {
			#ifdef SUPERIOR
       mask = "Superior_Integrator*.hex";			
			#else
       mask = "PCM*.hex";
//			 mask = "Beltway_Integrator*.hex";
			#endif
    }
    else //type = 2
    {
			/*Megha updated file path on 24-Nov-2015*/
       mask = "Backups\\*.bat";
    }

    Filename.count = 0;
		memset(&Filename,0,sizeof(Filename));
    info.fileID  = 0;
    while ((ffind (mask,&info) == 0) &&
           (Filename.count < 6))
    {
      if (info.fileID != previous_id)
      {
          strncpy(Filename.name[Filename.count], (char *)&info.name[0], 30);
          Filename.count++;
          previous_id = info.fileID;
      }
      else
      {
          break;
      }
    }
//     if ((Filename.count == 0) && (type == 1))
//     {
//         strncpy(Filename.name[Filename.count], "Beltway_Integrator.hex", 22);
//         Filename.count++;
//     }
    #else
    strcpy(Filename.name[0], "Beltway_Integrator_0001.hex");
    strcpy(Filename.name[1], "Beltway_Integrator_0002.hex");
    strcpy(Filename.name[2], "Beltway_Integrator_0003.hex");
    strcpy(Filename.name[3], "Beltway_Integrator_0004.hex");
    Filename.count = 4;
    #endif

}
/*****************************************************************************
* End of file
*****************************************************************************/
