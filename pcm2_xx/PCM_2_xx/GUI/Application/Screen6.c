/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen6.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"
#include "Global_ex.h"
#include "EEPROM_high_level.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define TODAY2

#define KEYPAD_BUTTON_1_ID      KEYPAD_GUI_ID_USER + 1 //1
#define KEYPAD_BUTTON_2_ID      KEYPAD_GUI_ID_USER + 2 //2
#define KEYPAD_BUTTON_3_ID      KEYPAD_GUI_ID_USER + 3 //3
#define KEYPAD_BUTTON_4_ID      KEYPAD_GUI_ID_USER + 4 //4
#define KEYPAD_BUTTON_5_ID      KEYPAD_GUI_ID_USER + 5 //5
#define KEYPAD_BUTTON_6_ID      KEYPAD_GUI_ID_USER + 6 //6
#define KEYPAD_BUTTON_7_ID      KEYPAD_GUI_ID_USER + 7 //7
#define KEYPAD_BUTTON_8_ID      KEYPAD_GUI_ID_USER + 8 //8
#define KEYPAD_BUTTON_9_ID      KEYPAD_GUI_ID_USER + 9 //9
#define KEYPAD_BUTTON_0_ID      KEYPAD_GUI_ID_USER + 10 //10
#define KEYPAD_BUTTON_ESC_ID    KEYPAD_GUI_ID_USER + 11 //11

#define KEYPAD_BUTTON_A_ID      KEYPAD_GUI_ID_USER + 12 //12
#define KEYPAD_BUTTON_B_ID      KEYPAD_GUI_ID_USER + 13 //13
#define KEYPAD_BUTTON_C_ID      KEYPAD_GUI_ID_USER + 14 //14
#define KEYPAD_BUTTON_D_ID      KEYPAD_GUI_ID_USER + 15 //15
#define KEYPAD_BUTTON_E_ID      KEYPAD_GUI_ID_USER + 16 //16
#define KEYPAD_BUTTON_F_ID      KEYPAD_GUI_ID_USER + 17 //17
#define KEYPAD_BUTTON_G_ID      KEYPAD_GUI_ID_USER + 18 //18
#define KEYPAD_BUTTON_H_ID      KEYPAD_GUI_ID_USER + 19 //19
#define KEYPAD_BUTTON_I_ID      KEYPAD_GUI_ID_USER + 20 //20
#define KEYPAD_BUTTON_J_ID      KEYPAD_GUI_ID_USER + 21 //21
#define KEYPAD_BUTTON_DEL_ID    KEYPAD_GUI_ID_USER + 22 //22

#define KEYPAD_BUTTON_K_ID      KEYPAD_GUI_ID_USER + 23 //23
#define KEYPAD_BUTTON_L_ID      KEYPAD_GUI_ID_USER + 24 //24
#define KEYPAD_BUTTON_M_ID      KEYPAD_GUI_ID_USER + 25 //25
#define KEYPAD_BUTTON_N_ID      KEYPAD_GUI_ID_USER + 26 //26
#define KEYPAD_BUTTON_O_ID      KEYPAD_GUI_ID_USER + 27 //27
#define KEYPAD_BUTTON_P_ID      KEYPAD_GUI_ID_USER + 28 //28
#define KEYPAD_BUTTON_Q_ID      KEYPAD_GUI_ID_USER + 29 //29
#define KEYPAD_BUTTON_R_ID      KEYPAD_GUI_ID_USER + 30 //30
#define KEYPAD_BUTTON_S_ID      KEYPAD_GUI_ID_USER + 31 //31
#define KEYPAD_BUTTON_T_ID      KEYPAD_GUI_ID_USER + 32 //32
#define KEYPAD_BUTTON_ENTARROW_ID  KEYPAD_GUI_ID_USER + 33 //33

#define KEYPAD_BUTTON_U_ID      KEYPAD_GUI_ID_USER + 34 //34
#define KEYPAD_BUTTON_V_ID      KEYPAD_GUI_ID_USER + 35 //35
#define KEYPAD_BUTTON_W_ID      KEYPAD_GUI_ID_USER + 36 //36
#define KEYPAD_BUTTON_X_ID      KEYPAD_GUI_ID_USER + 37 //37
#define KEYPAD_BUTTON_Y_ID      KEYPAD_GUI_ID_USER + 38 //38
#define KEYPAD_BUTTON_Z_ID      KEYPAD_GUI_ID_USER + 39 //39
#define KEYPAD_BUTTON_COMM_ID   KEYPAD_GUI_ID_USER + 40 //44
#define KEYPAD_BUTTON_DASH_ID   KEYPAD_GUI_ID_USER + 41 //40
#define KEYPAD_BUTTON_DOT_ID    KEYPAD_GUI_ID_USER + 42 //41
#define KEYPAD_BUTTON_SPACE_ID  KEYPAD_GUI_ID_USER + 43 //42
#define KEYPAD_BUTTON_ATRATE_ID KEYPAD_GUI_ID_USER + 44 //42		//replaced by "*" on 2/5/2017
#define KEYPAD_BUTTON_AND_ID    KEYPAD_GUI_ID_USER + 45 //44//


#define BACKDOOR_KEY 9642738
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */
//int MAC_ID[6];
	char Mac_id[17];
	char MAC_ID[6];
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */
/* Dialog resource of Keypad */
const GUI_WIDGET_CREATE_INFO _aDialogKeyPad[] = {
    /*  Function               Text      Id                          Px                     Py                 Dx                    Dy */
    { WINDOW_CreateIndirect,    0,        0,                     0,                   1,           KEYPAD_WINDOW_SIZE_X,      KEYPAD_WINDOW_SIZE_Y-2 },
        //---------------------------------------------------------------ROW 1----------------------------------------------------------------------------
    { BUTTON_CreateIndirect,   "1",      KEYPAD_BUTTON_1_ID,   KEYPAD_COL_POS_1,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "2",      KEYPAD_BUTTON_2_ID,   KEYPAD_COL_POS_2,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "3",      KEYPAD_BUTTON_3_ID,   KEYPAD_COL_POS_3,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "4",      KEYPAD_BUTTON_4_ID,   KEYPAD_COL_POS_4,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "5",      KEYPAD_BUTTON_5_ID,   KEYPAD_COL_POS_5,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "6",      KEYPAD_BUTTON_6_ID,   KEYPAD_COL_POS_6,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "7",      KEYPAD_BUTTON_7_ID,   KEYPAD_COL_POS_7,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "8",      KEYPAD_BUTTON_8_ID,   KEYPAD_COL_POS_8,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "9",      KEYPAD_BUTTON_9_ID,   KEYPAD_COL_POS_9,    KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "0",      KEYPAD_BUTTON_0_ID,   KEYPAD_COL_POS_10,   KEYPAD_ROW_POS_1,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "#",     KEYPAD_BUTTON_ESC_ID,   KEYPAD_COL_POS_11,   KEYPAD_ROW_POS_1,  KEYPAD_DIFF_BUTTON_SIZE_X,    KEYPAD_BUTTON_SIZE_Y},

    //---------------------------------------------------------------ROW 2----------------------------------------------------------------------------

    { BUTTON_CreateIndirect,   "A",      KEYPAD_BUTTON_A_ID,     KEYPAD_COL_POS_1,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "B",      KEYPAD_BUTTON_B_ID,     KEYPAD_COL_POS_2,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "C",      KEYPAD_BUTTON_C_ID,     KEYPAD_COL_POS_3,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "D",      KEYPAD_BUTTON_D_ID,     KEYPAD_COL_POS_4,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "E",      KEYPAD_BUTTON_E_ID,     KEYPAD_COL_POS_5,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "F",      KEYPAD_BUTTON_F_ID,     KEYPAD_COL_POS_6,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "G",      KEYPAD_BUTTON_G_ID,     KEYPAD_COL_POS_7,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "H",      KEYPAD_BUTTON_H_ID,     KEYPAD_COL_POS_8,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,      KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "I",      KEYPAD_BUTTON_I_ID,     KEYPAD_COL_POS_9,    KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "J",      KEYPAD_BUTTON_J_ID,     KEYPAD_COL_POS_10,   KEYPAD_ROW_POS_2,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect, "Del",      KEYPAD_BUTTON_DEL_ID,   KEYPAD_COL_POS_11,   KEYPAD_ROW_POS_2,  KEYPAD_DIFF_BUTTON_SIZE_X,   KEYPAD_BUTTON_SIZE_Y},

    //---------------------------------------------------------------ROW 3----------------------------------------------------------------------------

    { BUTTON_CreateIndirect,   "K",      KEYPAD_BUTTON_K_ID,     KEYPAD_COL_POS_1,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "L",      KEYPAD_BUTTON_L_ID,     KEYPAD_COL_POS_2,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "M",      KEYPAD_BUTTON_M_ID,     KEYPAD_COL_POS_3,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "N",      KEYPAD_BUTTON_N_ID,     KEYPAD_COL_POS_4,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "O",      KEYPAD_BUTTON_O_ID,     KEYPAD_COL_POS_5,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "P",      KEYPAD_BUTTON_P_ID,     KEYPAD_COL_POS_6,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "Q",      KEYPAD_BUTTON_Q_ID,     KEYPAD_COL_POS_7,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,         KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "R",      KEYPAD_BUTTON_R_ID,     KEYPAD_COL_POS_8,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "S",      KEYPAD_BUTTON_S_ID,     KEYPAD_COL_POS_9,    KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "T",      KEYPAD_BUTTON_T_ID,     KEYPAD_COL_POS_10,   KEYPAD_ROW_POS_3,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,    0,  KEYPAD_BUTTON_ENTARROW_ID,   KEYPAD_COL_POS_11,   KEYPAD_ROW_POS_3,  KEYPAD_DIFF_BUTTON_SIZE_X,    KEYPAD_BUTTON_SIZE_Y},

    //---------------------------------------------------------------ROW 4----------------------------------------------------------------------------
    { BUTTON_CreateIndirect,   "U",      KEYPAD_BUTTON_U_ID,     KEYPAD_COL_POS_1,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "V",      KEYPAD_BUTTON_V_ID,     KEYPAD_COL_POS_2,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "W",      KEYPAD_BUTTON_W_ID,     KEYPAD_COL_POS_3,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "X",      KEYPAD_BUTTON_X_ID,     KEYPAD_COL_POS_4,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "Y",      KEYPAD_BUTTON_Y_ID,     KEYPAD_COL_POS_5,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "Z",      KEYPAD_BUTTON_Z_ID,     KEYPAD_COL_POS_6,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   ":",      KEYPAD_BUTTON_COMM_ID,  KEYPAD_COL_POS_7,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "-",      KEYPAD_BUTTON_DASH_ID,  KEYPAD_COL_POS_8,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   ".",      KEYPAD_BUTTON_DOT_ID,   KEYPAD_COL_POS_9,            KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,         KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   " ",      KEYPAD_BUTTON_SPACE_ID, KEYPAD_COL_POS_10,           KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,       KEYPAD_BUTTON_SIZE_Y},

//    { BUTTON_CreateIndirect,   "@",     KEYPAD_BUTTON_ATRATE_ID, KEYPAD_COL_POS_11,           KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,         KEYPAD_BUTTON_SIZE_Y},
		{ BUTTON_CreateIndirect,   "*",     KEYPAD_BUTTON_ATRATE_ID, KEYPAD_COL_POS_11,           KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,         KEYPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "/",      KEYPAD_BUTTON_AND_ID,   KEYPAD_COL_POS_12,           KEYPAD_ROW_POS_4,  KEYPAD_BUTTON_SIZE_X,         KEYPAD_BUTTON_SIZE_Y},

    { EDIT_CreateIndirect,     0,        GUI_ID_EDIT1,           EDIT_WINDOW_XPOS,            EDIT_WINDOW_YPOS,  EDIT_WINDOW_SIZE_X,        EDIT_WINDOW_SIZE_Y,  EDIT_CF_HCENTER | EDIT_CF_VCENTER, 50},

};

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen6(WM_MESSAGE * pMsg);
static void backDoorPass(char *bdpstr);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*****************************************************************************
* @note       Function name  : static void callback_screen6(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen6(WM_MESSAGE * pMsg)
{
    char acBuffer[2], rcvBuffer[ARRAY_SIZE],ip_ptr[6];
    int Id, NCode, sel, Key_entry, i,num,num1;
    Screen_org_data * new_screen_org;
    WM_HWIN hItem, Edit1, Dlg_keypad, first_button;
    static char pwbuffer[ARRAY_SIZE];
	  static char bdpass[8];
    static unsigned int index = 0;
    static SCREEN_TYPE6_DATA * data;
    static const GUI_FONT * OldFont;
    static int counter = 0, pos = 0, dots = 0, nos_of_digits = 0, sign = 0;
    static char local_ip_buff[5];
		int screen_no;
    Dlg_keypad = pMsg->hWin;
    Edit1 = WM_GetDialogItem(Dlg_keypad, GUI_ID_EDIT1);

    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);

	  first_button = WM_GetDialogItem(Dlg_keypad, KEYPAD_BUTTON_1_ID);
    switch (pMsg->MsgId)
    {
    case WM_INIT_DIALOG:
        data = GUI_data_nav.Current_screen_info->screen_data;

        EDIT_SetInsertMode (Edit1, 1);
        EDIT_SetBkColor(Edit1, EDIT_CI_ENABLED, GUI_WHITE);
        EDIT_SetBkColor(Edit1, EDIT_CI_DISABLED, GUI_GRAY);
        EDIT_SetFont(Edit1, &GUI_FontArial_Unicode_MS16_Bold);
        EDIT_SetTextColor(Edit1, EDIT_CI_ENABLED, GUI_BLACK);
        EDIT_SetMaxLen(Edit1, 30);
        EDIT_SetFocussable(Edit1, 0);
        WM_SelectWindow(Dlg_keypad);
        WM_DisableWindow(Edit1);
        index = 0;
        for(i=0; i<sizeof(pwbuffer); i++)
        {
            pwbuffer[i] = '0';
        }
        for(i=0; i<sizeof(rcvBuffer); i++)
        {
            rcvBuffer[i] = 0;
        }

        hItem = WM_GetDialogItem(Dlg_keypad, KEYPAD_BUTTON_1_ID);
        OldFont = BUTTON_GetFont(hItem);
        for (i = (KEYPAD_GUI_ID_USER + 1); i < ((GUI_COUNTOF(_aDialogKeyPad)) + KEYPAD_GUI_ID_USER); i++)
        {
            hItem = WM_GetDialogItem(Dlg_keypad, i);
            BUTTON_SetFocussable(hItem, 1);
            BUTTON_SetFocusColor (hItem, GUI_RED);
            BUTTON_SetBkColor(hItem,BUTTON_CI_PRESSED, GUI_WHITE);
            BUTTON_SetBkColor(hItem,BUTTON_CI_UNPRESSED, GUI_GRAY);
            BUTTON_SetTextColor(hItem,BUTTON_CI_PRESSED, GUI_BLACK);
            BUTTON_SetTextColor(hItem,BUTTON_CI_UNPRESSED, GUI_BLACK);
            switch (i)
            {
                case KEYPAD_BUTTON_ENTARROW_ID:
                    BUTTON_SetBitmapEx(hItem, 0, &_bmArrowEnter, 14, 7);  /* Set bitmap for enter arrow button (unpressed) */
                    BUTTON_SetBitmapEx(hItem, 1, &_bmArrowEnter, 14, 7);  /* Set bitmap for enter arrow button (pressed) */
                    break;
            }
        }
            counter = pos = dots = nos_of_digits = 0;
            memset(acBuffer, 0, sizeof(acBuffer));
            memset(local_ip_buff, 0, sizeof(local_ip_buff));				
        break;

    case WM_NOTIFY_PARENT:
        Id    = WM_GetId(pMsg->hWinSrc);
        NCode = pMsg->Data.v;
        hItem = WM_GetDialogItem(Dlg_keypad, KEYPAD_GUI_ID_USER + Id);

        switch(Id)
        {
            case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                switch(NCode)
                {
                    case WM_NOTIFICATION_CLICKED:
                        break;

                    case WM_NOTIFICATION_RELEASED:
                        break;

                    case WM_NOTIFICATION_SEL_CHANGED:
                        #ifdef TODAY2
                        if ((sel + 1) != GUI_data_nav.Current_screen_info->size_of_screen_data)
                        {
                        #endif
                            if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PASSWORD_SCREEN_ORG_NO) &&
                                strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NEW_PWD_SCREEN_ORG_NO) &&
                                strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_SCREEN_ORG_NO))
                            {
                                EDIT_SetText(Edit1, data[sel].Variable1);
                            }
                        #ifdef TODAY2
                        }
                        #endif
                        WM_SetFocus(GUI_data_nav.Listbox);
                        break;
                }
                break;

            default:
                switch(NCode)
                {
                    case WM_NOTIFICATION_GOT_FOCUS:
                        BUTTON_SetFont(pMsg->hWinSrc, &GUI_FontArial_Unicode_MS16_Bold);
                        BUTTON_SetBkColor(pMsg->hWinSrc, BUTTON_CI_UNPRESSED, GUI_LIGHTGRAY);
                        break;

                    case WM_NOTIFICATION_LOST_FOCUS:
                        BUTTON_SetFont(pMsg->hWinSrc, OldFont);
                        BUTTON_SetBkColor(pMsg->hWinSrc, BUTTON_CI_UNPRESSED, GUI_GRAY);
                        break;

                    case WM_NOTIFICATION_CHILD_DELETED:
                        WM_SetFocus(GUI_data_nav.Listbox);
                        GUI_ClearKeyBuffer();
                        LISTBOX_SetSel(GUI_data_nav.Listbox, 1);
                        break;

                    case WM_NOTIFICATION_RELEASED:
                        if ((Id >= (KEYPAD_GUI_ID_USER + 1)) && (Id <= (KEYPAD_GUI_ID_USER + MAX_KEYPAD_BUTTON)))
                        {
                            BUTTON_GetText(pMsg->hWinSrc, acBuffer, sizeof(acBuffer)); /* Get the text of the button */
                            if (Id == KEYPAD_BUTTON_DEL_ID)
                            {
                                EDIT_AddKey(Edit1, GUI_KEY_BACKSPACE);
                                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PASSWORD_SCREEN_ORG_NO) == 0)  ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NEW_PWD_SCREEN_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_SCREEN_ORG_NO) == 0))
                                {
                                    pwbuffer[index-1] = pwbuffer[index];

                                    if(index != 0)
                                    {
                                        index = index - 1;
                                    }
                                }
                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0) 
                                    {
																			if(counter > 0)
																			{
																					counter --;
																			}
																			if(nos_of_digits > 0)
																			{
																					nos_of_digits--;																			
                                            switch(nos_of_digits)
                                            {
                                                case 1:
                                                    counter = nos_of_digits;
                                                    break;
                                                case 2:
                                                    counter = nos_of_digits;
                                                    dots--;
                                                    break;
                                                case 3:
                                                case 4:
                                                    counter = nos_of_digits - 3;
                                                case 5:
                                                    counter = nos_of_digits - 3;
                                                    dots--;
                                                    break;
                                                case 6:
                                                case 7:
                                                    counter = nos_of_digits - 6;
                                                    break;
                                                case 8:
                                                    counter = nos_of_digits - 6;
                                                    dots--;
                                                    break;
                                                case 9:
                                                case 10:
																										counter = nos_of_digits - 9;
																										break;
                                                case 11:
                                                    counter = nos_of_digits - 9;
																										dots--;
                                                    break;
                                                case 12:
                                                case 13:
																										counter = nos_of_digits - 12;
																										break;
                                                case 14:
                                                    counter = nos_of_digits - 12;
																										dots--;
                                                    break;	
                                                case 15:
                                                case 16:
                                                case 17:
																										counter = nos_of_digits - 15;
																										break;
																								
                                            }	
																					}
																		}																
                            }
                            else if (Id == KEYPAD_BUTTON_ENTARROW_ID)
                            {
                                if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PASSWORD_SCREEN_ORG_NO) == 0)
                                {
																	  backDoorPass(bdpass);
                                    pwbuffer[index] = '\0';
                                    //compare the password
                                    if((strcmp(Admin_var.Password, pwbuffer) == 0) || (strcmp(bdpass, pwbuffer) == 0))
                                    {
                                        //password correct set the password entered flag
                                        if(!GUI_data_nav.In_wizard)
                                        { 
                                            password_struct.password_entered = 1;
                                            strcpy(GUI_data_nav.New_screen_num, GUI_data_nav.Screen_before_pw_org_num->Screen_org_no);
                                            GUI_data_nav.Change = 1;
																					  if(GUI_data_nav.Clr_wt_pw_to_be_entered == CLR_WT_CHK_PW_SCREEN)
                                                GUI_data_nav.Clr_wt_pw_to_be_entered = CLEAR_THE_WEIGHT;
                                        }
                                        else
                                        {
                                            GUI_data_nav.Key_press = 1; //to go to the next screen in case of password change wizard
                                            GUI_data_nav.Child_present = 0;
                                        }
                                    }
                                    else
                                    {
                                        password_struct.password_entered = 0;
                                        //password incorrect go to password error screen
                                        new_screen_org = GUI_data_nav.Current_screen_info->child;
                                        //new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                        strcpy(GUI_data_nav.New_screen_num, new_screen_org->Screen_org_no);
                                        GUI_data_nav.Change = 1;
                                    }
                                }
                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0) 
                                    {
																			EDIT_GetText(Edit1, rcvBuffer, sizeof(rcvBuffer));
                                            pos = counter = 0;
                                            //store the IP address in the proper format
																						if(nos_of_digits < 17)
																						{
																							for(i=nos_of_digits;i<17;i++)
																							rcvBuffer[i] = '0';

																						}
                                            while(counter < 6)
                                            {
                                                strncpy(ip_ptr, &rcvBuffer[pos], 1);
																								ip_ptr[1] = '\0';
																								num = ip_ptr[0];
																								if((num >= 'A')&&(num <='F'))
																								{
																									num = num - 0x37;
																								}
																								else
																								{
																									num =  num - 0x30;
																								}
																								pos++;
                                                strncpy(ip_ptr, &rcvBuffer[pos], 1);
																								ip_ptr[1] = '\0';																								
																								num1 = ip_ptr[0];
																								if((num1 >= 'A')&&(num1 <= 'F'))
																								{
																									num1 = num1 - 0x37;
																								}
																								else
																								{
																									num1 =  num1 - 0x30;
																								}																								
																								num = ((num << 4)| num1);
																						
                                                if((num < 0) || (num > 255))
                                                {
                                                    strncpy(&rcvBuffer[pos], "FF", 2);
																										num = 0xff;
                                                }
																								MAC_ID[counter] = num;
																								pos++;
																								if(counter < 5 )
																								rcvBuffer[pos] = ':';																								
																								num = 0;
																								num1 = 0;
                                                pos++;
					                                      counter++;
                                            }	

																						
//																						GUI_data_nav.GUI_structure_backup = 1;
																						
																						EEPROM_Write (0, MAC_PAGE_NO, MAC_ID, MODE_8_BIT, sizeof(MAC_ID));
																						strcpy(data[sel].Variable1, rcvBuffer);																						
																						go_back_one_level();
																		}																
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NEW_PWD_SCREEN_ORG_NO) == 0)
                                {
                                    pwbuffer[index] = '\0';
                                    strcpy(data[sel].Variable1, pwbuffer);
                                    GUI_data_nav.Key_press = 1; //to go to the next screen in case of password change wizard
                                }
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_SCREEN_ORG_NO) == 0)
                                {
                                    pwbuffer[index] = '\0';
                                    //compare the password
                                    if(strcmp(Admin_var.New_Password, pwbuffer) == 0)
                                    {
                                        //re entered password is correct, so modify the stored password and go to screen
                                        //no.600000
                                        strcpy(Admin_var.Password, Admin_var.New_Password);
                                        //GUI_data_nav.GUI_structure_backup = 1;
                                        GUI_data_nav.Key_press = 1; //to go to the next screen in case of password change wizard
                                        GUI_data_nav.Child_present = 0;
                                        GUI_data_nav.GUI_structure_backup = 1;
                                    }
                                    else
                                    {
                                        //password incorrect go to verify password error screen
                                        new_screen_org = GUI_data_nav.Current_screen_info->child;
                                        //new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                        strcpy(GUI_data_nav.New_screen_num, new_screen_org->Screen_org_no);
                                        GUI_data_nav.Change = 1;
                                    }
                                }
                                else
                                {
                                    EDIT_GetText(Edit1, rcvBuffer, sizeof(rcvBuffer));
                                    strcpy(data[sel].Variable1, rcvBuffer);
																	//by megha on 11/5/2017 for edit log parameter
																		screen_no = atoi(GUI_data_nav.Current_screen_info->Screen_org_no);
																				
																				switch(screen_no)
																				{
																					case 242000:
																												parameter_code = PPP_DIAL_CODE;
																												break;
																					case 253000:
																												parameter_code = SNTP_HOST_ADDR_CODE;
																												break;
																					case 225000:
																												parameter_code = MAC_ID_CODE;
																												break;																					
																					default:
																											break;
																				}
																				log_edit_para();
																				
                                    GUI_data_nav.GUI_structure_backup = 1;
                                    GUI_data_nav.Change = 1;
                                    if ((!GUI_data_nav.In_wizard) &&
                                        (GUI_data_nav.Current_screen_info->size_of_screen_data == 2))
                                    {
                                        //specifically for screen no. 6 to return back to the previous screen
                                        go_back_one_level();
                                    }//if (!in_wizard)
                                }
                                WM_SelectWindow(Dlg_keypad);
                                WM_DisableWindow(Edit1);
                                WM_SetFocus(GUI_data_nav.Listbox);
                            }
//                             else if (Id == KEYPAD_BUTTON_ESC_ID)
//                             {
//                                 if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PASSWORD_SCREEN_ORG_NO)  &&
//                                     strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NEW_PWD_SCREEN_ORG_NO) &&
//                                     strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_SCREEN_ORG_NO))
//                                 {
//                                     EDIT_SetText(Edit1, data[sel].Variable1);
//                                 }
//                                 /*if ((!GUI_data_nav.In_wizard) &&
//                                     (GUI_data_nav.Current_screen_info->size_of_screen_data == 1))
//                                 {
//                                     //specifically for screen no. 6 to return back to the previous screen
//                                     go_back_one_level();
//                                 }*///if (!in_wizard)
// 																if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0) 
// 																{
// 																	counter = 0;	
// 																	nos_of_digits = 0;
// 																	dots = 0;
// 																	
// 																}																	
//                                 WM_DisableWindow(Dlg_keypad);
//                                 WM_DisableWindow(Edit1);
//                                 WM_SetFocus(GUI_data_nav.Listbox);
//                                                 															
//                             }
                            else
                            {
                                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PASSWORD_SCREEN_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NEW_PWD_SCREEN_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_SCREEN_ORG_NO) == 0))
                                {
                                    EDIT_AddKey(Edit1, '*');
                                    pwbuffer[index] = acBuffer[0];
                                    index++;
                                }
																else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0) 
																{
																	if((acBuffer[0] >= '0')&&(acBuffer[0] <= 'F')&&((acBuffer[0] !='@')))
																	{
																	
																		nos_of_digits++;
																		counter++;
																		switch(counter)
																		{
																				case 1:
																						if(acBuffer[0] >= 'F')
																						{
																								acBuffer[0] = 'F';
																						}
																						local_ip_buff[0] = acBuffer[0];
																						break;

																				case 2:
																						if((local_ip_buff[0] == 'F') &&
																								(acBuffer[0] > 'F'))
																						{
																								acBuffer[0] = 'F';
																								EDIT_AddKey(Edit1, GUI_KEY_BACKSPACE);
																								EDIT_AddKey(Edit1, 'F');
																						}
																						break;
																		}
																		if ((Id == KEYPAD_BUTTON_DOT_ID) &&
																				 (nos_of_digits <= 12))
																		{
																			 //one number and dot entered
																			 if (counter == 2)
																			 {
																					EDIT_AddKey(Edit1, GUI_KEY_BACKSPACE);
																					EDIT_AddKey(Edit1, '0');
																					EDIT_AddKey(Edit1, local_ip_buff[0]);
																					nos_of_digits += 1;
																			 }
																			 counter = 0;
																			 dots++;
																			 memset(local_ip_buff, 0, sizeof(local_ip_buff));
																			 EDIT_AddKey(Edit1, acBuffer[0]);
																		}
																		else if ((nos_of_digits <= 17) &&
																				(Id != KEYPAD_BUTTON_DOT_ID))
																		{
																				EDIT_AddKey(Edit1, acBuffer[0]);
																		}
																		else if ((nos_of_digits <= 17) &&
																				(Id == KEYPAD_BUTTON_DOT_ID))
																		{
																				nos_of_digits--;
																				counter--;
																		}
																		if(((nos_of_digits == 2) || (nos_of_digits == 5) || (nos_of_digits == 8)||(nos_of_digits == 11)||((nos_of_digits == 14)))
																				&& (dots < 5))
																		{
																				EDIT_AddKey(Edit1, ':');
																				counter = 0;
																				dots++;
																				nos_of_digits++;
																				memset(local_ip_buff, 0, sizeof(local_ip_buff));
																		}
																		if(nos_of_digits>17)
																			nos_of_digits = 17;
																	}
																}																													
                                else
                                {
                                    EDIT_AddKey(Edit1, acBuffer[0]);
                                }
                            }
                        }
                        break;//case WM_NOTIFICATION_RELEASED:
                }
                break;//case default:
        }
        break; //case WM_NOTIFY_PARENT:

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);

            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_LEFT:
                            ;
                            break;

                        case GUI_KEY_RIGHT:
                            /*EDIT_SetText(Edit1, "");
                            WM_SelectWindow(hDlg_keypad);
                            WM_EnableWindow(Edit1);
                            WM_SetFocus(GUI_data_nav.Keypad);*/
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;

                        case GUI_KEY_ENTER:
                            #ifdef TODAY2
                            if (((sel + 1) == GUI_data_nav.Current_screen_info->size_of_screen_data) &&
                                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PASSWORD_SCREEN_ORG_NO) &&
                                strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NEW_PWD_SCREEN_ORG_NO) &&
                                strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_SCREEN_ORG_NO)))
                            {
                                //specifically for screen no. 6 to return back to the previous screen
                                 if (!GUI_data_nav.In_wizard)
                                {
                                    go_back_one_level();
                                }//if (!in_wizard)
                            }
                            else
                            {
                            #endif
                                GUI_data_nav.Key_press = 1;
                                if ((!security_bit_set()) || (password_struct.password_entered) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PASSWORD_SCREEN_ORG_NO) == 0) ||
																    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NEW_PWD_SCREEN_ORG_NO) == 0) ||
																    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_SCREEN_ORG_NO) == 0)||
																		(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0))
                                {
                                    EDIT_SetText(Edit1, "");
                                    WM_SelectWindow(Dlg_keypad);
                                    WM_EnableWindow(Edit1);
                                    WM_SetFocus(GUI_data_nav.Keypad);
																	  WM_SetFocus(first_button);
                                    GUI_data_nav.Key_press = 0;
                                    index = 0;
                                    for(i=0; i<sizeof(pwbuffer); i++)
                                    {
                                        pwbuffer[i] = '0';
                                    }
                                }
                                else
                                {
                                    //create the password screen, store the current screen org number to come back to it
                                    //in case the password entered is correct
                                    GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info;
                                    //strcpy(GUI_data_nav.Screen_before_pw_org_num->Screen_org_no, GUI_data_nav.Current_screen_info->Screen_org_no);
                                    GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
                                    password_struct.password_to_be_entered = 1;
                                }
                                if (data[sel].screen_an_option == 0)
                                {
                                    GUI_data_nav.Child_present = 0;
                                }
                            #ifdef TODAY2
                            }
                            #endif
                            break;

                        case GUI_KEY_BACK:
                            go_back_one_level();
//                             if(GUI_data_nav.In_wizard)
//                             {
//                                 GUI_data_nav.Back_one_level = GO_BACK_FLAG;
//                             }
                            break;
                    }
                    break;

                case ID_MULTI_TEXT_1:
                    ;
                    break;

                default:
                    switch (Key_entry)
                    {
                        case GUI_KEY_UP:
                            if ((Id >= KEYPAD_GUI_ID_USER)  && (Id <= (KEYPAD_GUI_ID_USER + MAX_KEYPAD_BUTTON)))
                            {
                                for (i = 0; i < KEYPAD_MAX_NUMBER_OF_COL; i++)
                                {
                                    if((Id == (KEYPAD_GUI_ID_USER + MAX_KEYPAD_BUTTON)) && (i==0))
                                    {
                                        //Back one button from bootom to top
                                        GUI_SendKeyMsg(GUI_KEY_BACKTAB,1);
                                    }
                                    if((Id < (KEYPAD_GUI_ID_USER + KEYPAD_MAX_NUMBER_OF_COL)) && (i==0))
                                    {
                                        //Back one button from top to bootom
                                        GUI_SendKeyMsg(GUI_KEY_BACKTAB,1);
                                    }
                                    GUI_SendKeyMsg(GUI_KEY_BACKTAB,1);
                                }
                            }
                            break;

                        case GUI_KEY_LEFT:
                            GUI_SendKeyMsg(GUI_KEY_BACKTAB,1);
                            break;

                        case GUI_KEY_LEFT_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_DOWN:
                            if ((Id >= KEYPAD_GUI_ID_USER) && (Id <= (KEYPAD_GUI_ID_USER + MAX_KEYPAD_BUTTON)))
                            {
                                for (i = 0; i < KEYPAD_MAX_NUMBER_OF_COL; i++)
                                {
                                    if((Id >= (KEYPAD_GUI_ID_USER + (MAX_KEYPAD_BUTTON - (KEYPAD_MAX_NUMBER_OF_COL + 1)))) && (i==0))
                                    {
                                        if((Id != (KEYPAD_GUI_ID_USER + MAX_KEYPAD_BUTTON )) && (i==0))
                                        {
                                            //Back one button from top to bootom
                                            GUI_SendKeyMsg(GUI_KEY_TAB,1);
                                        }
                                    }
                                    GUI_SendKeyMsg(GUI_KEY_TAB,1);
                                }
                            }
                            break;

                        case GUI_KEY_RIGHT:
                            GUI_SendKeyMsg(GUI_KEY_TAB,1);
                            break;

                        case GUI_KEY_ENTER:
                            break;

                        case GUI_KEY_BACK:

                            EDIT_SetText(Edit1, data[sel].Variable1);
														counter = pos = dots = nos_of_digits = 0;
                            WM_SelectWindow(Dlg_keypad);
                            WM_DisableWindow(Edit1);
                            WM_SetFocus(GUI_data_nav.Listbox);
                            memset(acBuffer, 0, sizeof(acBuffer));
                            memset(local_ip_buff, 0, sizeof(local_ip_buff));														
                            break;
                    }
                    break;

            }
            break;

            default:
                WM_DefaultProc(pMsg);
                break;
  }
  return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen6(SCREEN_TYPE5_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets. Also the keypad
*                            : screen is drawn and the default button settings are configured.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen6(SCREEN_TYPE6_DATA *data, int count)
{
    int i;
 
    GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX_SIZE_X, LISTBOX_SIZE_Y,
                                             WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
    for (i = 0; i < count; i++)
    {
        LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
    }

    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
    LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
    LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
    LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
    LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
    LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

    LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

    GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(MULTI_EDIT_START, MULTI_WIN_OFFSET,
                                                 MULTI_EDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                                 WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1,100,NULL);
    MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
    MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
    MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
    MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
    MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);

    i = GUI_COUNTOF(_aDialogKeyPad);
    GUI_data_nav.Keypad = GUI_CreateDialogBox(_aDialogKeyPad, GUI_COUNTOF(_aDialogKeyPad),
                                              callback_screen6, GUI_data_nav.Multi_text, 0, 0);

    WINDOW_SetBkColor(GUI_data_nav.Keypad, GUI_BLACK);

    WM_SetCallback(WM_HBKWIN, callback_screen6);		

    WM_SetFocus(GUI_data_nav.Listbox);
		LISTBOX_SetSel(GUI_data_nav.Listbox, count);
    return;
}
/*****************************************************************************
* @note       Function name  : void backDoorPass(char *bdpstr)
* @returns    returns        : None
* @param      arg1           : pointer to the password string
* @author                    : Racharla Venkata Krishna Rao
* @date       date created   : 6th November 2013
* @brief      Description    : Creates a revolving password (backdoor password) which user can use to 
                               unlock the scale in case if user forgets his password. 
* @note       Notes          :
*****************************************************************************/
void backDoorPass(char *bdpstr)
{
	 unsigned char w1=23,w2=59,w3=32,w4=17; 
	 unsigned int bdp;
	 char t;
	 bdp = BACKDOOR_KEY - (((w1*(LPC_RTC->DOM))*(w2*(LPC_RTC->MONTH))) + (w3*(LPC_RTC->HOUR))+ w4*(LPC_RTC->YEAR));
	 sprintf(bdpstr,"%d",bdp);
	 switch(bdp%10)
	 {
	  case 0:
		case 1:
		case 2: t = *(bdpstr+0);
			    *(bdpstr+0) = *(bdpstr+4);
				*(bdpstr+4) = t;
				t =  *(bdpstr+2);
			    *(bdpstr+2) = *(bdpstr+5);
				*(bdpstr+5) = t;
				t = *(bdpstr+3);
			    *(bdpstr+3) = *(bdpstr+5);
				*(bdpstr+5) = t;
		    break;
	  case 3:
		case 4:
		case 5: t = *(bdpstr+1);
			    *(bdpstr+1) = *(bdpstr+3);
				*(bdpstr+3) = t;
				t =*(bdpstr+0);
			    *(bdpstr+0) = *(bdpstr+5);
				*(bdpstr+5) = t;
				t = *(bdpstr+3);
			    *(bdpstr+3) = *(bdpstr+5);
				*(bdpstr+5) = t;
		    break;
		default:
			    t = *(bdpstr+6);
			   *(bdpstr+6) = *(bdpstr+3);
				*(bdpstr+3) = t;
				t = *(bdpstr+0);
			    *(bdpstr+0) = *(bdpstr+3);
				*(bdpstr+3) = t;
				t = *(bdpstr+4);
			   *(bdpstr+4) = *(bdpstr+1);
				*(bdpstr+1) = t;
		    break;
	 }
	 bdpstr[4] = bdpstr[5] = bdpstr[6] ='\0' ;
}
/*****************************************************************************
* End of file
*****************************************************************************/
