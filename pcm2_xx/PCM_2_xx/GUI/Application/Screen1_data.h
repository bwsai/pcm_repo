/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen_common_func.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN1_DATA
#define  SCREEN1_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Images.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
__align(4) SCREEN_TYPE1_DATA Org_000000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {&bmwizard,              Screen0_str1, 0, 1},
    {&bmsettings,          Screen0_str2, 0, 2},
//     {&bmdevicesetup,         Screen0_str4, 0, 4},
// 		{&bmcalibration,         Screen0_str3, 0, 3},
// 		{&bmreports_diagnostics, Screen0_str5, 0, 5},
    {&bmadmin,               Screen0_str3, 0, 3},
};

 __align(4) SCREEN_TYPE1_DATA Org_200000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
// 	
 	{&bmplantconnect_blackbackgroundnotext, Screen2_str1, 0, 1},
 	{&bmgreenethernetactivityicon, Screen2_str2, 0, 2},
 	{&bmcalibrationcheck, Screen2_str3, 0, 3},
 	{&bmmaterialtest, Screen2_str4, 0, 4},
 	{&bmdigitalcalibration, Screen2_str5, 0, 5},	
 };

// __align(4) SCREEN_TYPE1_DATA Org_300000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {&bmtestweight,         Screen3_str1, 0, 1},
//     {&bmmaterialtest,       Screen3_str2, 0, 2},
//     {&bmdigitalcalibration, Screen3_str3, 0, 3},
//     {&bmlenthcal,           Screen3_str4, 0, 4},
//     {&bmzeroCal,            Screen3_str5, 0, 5},
// };

__align(4) SCREEN_TYPE1_DATA Org_400000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {&bmprinter,                            Screen4_str1, 0, 1},
    {&bmscoreboard,                         Screen4_str2, 0, 2},
    {&bmusb,                                Screen4_str3, 0, 3},
    {&bmI_O,                                Screen4_str4, 0, 4},
    #ifdef PID_CONTROL
    {&bmcontrol,                            Screen4_str5, 0, 5}, 
    #endif
    {&bmplantconnect_blackbackgroundnotext, Screen4_str6, 0, 0},
};

__align(4) SCREEN_TYPE1_DATA Org_440000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {&bmdigitalinputs, Screen44_str1, 0, 1},
    {&bmdigitaloutput, Screen44_str2, 0, 2},
    {&bmrelay,         Screen44_str3, 0, 3},
    {&bmanalogoutput,  Screen44_str4, 0, 4},
};

__align(4) SCREEN_TYPE1_DATA Org_450000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {&bmBeltloadcontrol, Screen45_str1, 0, 1},
    {&bmBeltloadcontrol, Screen45_str2, 0, 2},
    {&bmblending2,       Screen45_str3, 0, 3},
    {&bmratecontrol,     Screen45_str4, 0, 4},
    #ifdef LOAD_CONTROL
    {&bmBeltloadcontrol, Screen45_str5, 0, 5},
    #endif
};

__align(4) SCREEN_TYPE1_DATA Org_500000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {&bmtotals_icon        , Screen5_str1, 0, 1},
    {&bmreports_diagnostics, Screen5_str2, 0, 2},
    {&bmfirmwareversions,    Screen5_str3, 0, 3},
    //{&bmcalibrationcheck, Screen54_str0, 0, 4},
    {&bmliveload, Screen55_str0, 0, 4},
};

// __align(4) SCREEN_TYPE1_DATA Org_510000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {&bmreports_diagnostics, Screen5_str1, 0, 0},
// };

__align(4) SCREEN_TYPE1_DATA Org_520000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {&bmscalesetup,     Screen52_str1, 0, 1},
    {&bmsensors,        Screen52_str2, 0, 2},
    {&bmI_O,            Screen52_str3, 0, 3},
    {&bmvoltages,       Screen52_str4, 0, 4},
    {&bmcommunications, Screen52_str5, 0, 5},
    {&bmcalibration,    Screen0_str3,  0, 6},
};

__align(4) SCREEN_TYPE1_DATA Org_523000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {&bmdigitalinputs,   Screen523_str1, 0, 1},
    {&bmdigitaloutput,   Screen523_str3, 0, 2},
    {&bmrelay,           Screen523_str4, 0, 3},
    {&bmanalogoutput,    Screen523_str5, 0, 4},
    {&bmBeltloadcontrol, Screen523_str6, 0, 5},
    //{&bmAnalogInput,     Screen523_str2, 0, 2},
};

__align(4) SCREEN_TYPE1_DATA Org_600000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {&bmsettings,                  Screen6_str1, 0, 1},
    {&bmrunmode,                   Screen6_str2, 0, 2},
    {&bmsecurity,                  Screen6_str3, 0, 3},
    {&bmfirmware,                  Screen6_str4, 0, 4},
    {&bmcalibration,               Screen6_str5, 0, 5},
    {&bmgreenethernetactivityicon, Screen6_str6, 0, 6},
};

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
