/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen9_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN9_DATA
#define  SCREEN9_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

__align(4) SCREEN_TYPE9_DATA Org_330000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen6411_str1, Screen6411_str2, Screen6411_str3,  &Pcm_var.Int_firmware_version, NULL, char_array,  0},
};

__align(4) SCREEN_TYPE9_DATA Org_333000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen6414_str1, Screen6414_str2, Screen6414_str3,  &Pcm_var.Int_update_version, NULL, char_array, 0},
};

__align(4) SCREEN_TYPE9_DATA Org_335000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen6416_str1, Screen6416_str2, Screen6416_str3,  &Pcm_var.Int_firmware_version, NULL, char_array, 0},
};


__align(4) SCREEN_TYPE9_DATA Org_231200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen2312_str1, Screen2312_str2, Screen2312_str3, NULL,  &Scale_setup_var.Custom_LC_unit, no_variable, 1},
};

__align(4) SCREEN_TYPE9_DATA Org_272100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen2711_str0, Screen2711_str1, Screen2711_str2,  &Scale_setup_var.Conveyor_angle, NULL, float_type, 0},
};




__align(4) SCREEN_TYPE9_DATA Org_323000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen324_str1, Screen324_str2, Screen324_str3, NULL,  &Calibration_var.Cert_scale_unit, no_variable, 1},
};

__align(4) SCREEN_TYPE9_DATA Org_341000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen3411_str1, Screen3411_str2, Screen3411_str3,  NULL,  NULL, float_type, 0},
};


#if 0
__align(4) SCREEN_TYPE9_DATA Org_352000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen352_str1, Screen352_str2, Screen352_str3,  &Calibration_var.Static_zero_cal, NULL, double_type, 0},
};
#else
__align(4) SCREEN_TYPE9_DATA Org_352100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen3521_str1, Screen3521_str2, Screen3521_str3,  &Calibration_var.New_zero_value, NULL, float_type, 0},
};
#endif

__align(4) SCREEN_TYPE9_DATA Org_619110_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen61912_str1, Screen61912_str2, Screen61912_str3, NULL, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE9_DATA Org_619130_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen61914_str1, Screen61914_str2, Screen61914_str3, &Admin_var.Backup_file_name, NULL, char_array, 0},
};

__align(4) SCREEN_TYPE9_DATA Org_619220_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen61912_str1, Screen61912_str2, Screen61923_str1,  &Admin_var.Reload_file_name, NULL, char_array,  0},
};

__align(4) SCREEN_TYPE9_DATA Org_619240_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen61925_str1, Screen61925_str2, Screen61925_str3, &Admin_var.Reload_file_name, NULL, char_array,  0},
};

__align(4) SCREEN_TYPE9_DATA Org_640000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen6411_str1, Screen6411_str2, Screen6411_str3,  &Admin_var.Int_firmware_version, NULL, char_array,  0},
};

__align(4) SCREEN_TYPE9_DATA Org_643000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen6414_str1, Screen6414_str2, Screen6414_str3,  &Admin_var.Int_update_version, NULL, char_array, 0},
};

__align(4) SCREEN_TYPE9_DATA Org_645000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen6416_str1, Screen6416_str2, Screen6416_str3,  Admin_var.Int_firmware_version, NULL, char_array, 0},
};

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
