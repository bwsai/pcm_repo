/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Weight_rate_run_mode_screen.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdlib.h>
#include "Screen_global_ex.h"
//#include "Sensor_board_data_process.h"
//#include "PCM_main_screen.h"
#include "Screen_data_enum.h"
#include "mbport.h"
#include "Http_Client.h"
#include <Net_Config.h>
#include "Main.h"
#include "../GUI/Application/Screen_global_ex.h"
#include "Serial_LPC178x.h"
#include "math.h"
#include "../GUI/Application/Screen_common_func.h"



/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define WM_APP_SHOW_TEXT (WM_USER + 0)
//Acc. weight
#define RUNSCREEN_TEXT_EDIT_SIZE_X    70
#define RUNSCREEN_TEXT_EDIT_SIZE_Y    16
#define RUNSCREEN_TEXT_EDIT_POS_X     WINDOW_HEADER_TEXT1_POS_X0
#define RUNSCREEN_TEXT_EDIT_POS_Y     0
//Variable
#define RUNSCREEN_TEXT_EDIT1_SIZE_X    100
#define RUNSCREEN_TEXT_EDIT1_SIZE_Y    16
#define RUNSCREEN_TEXT_EDIT1_POS_X     RUNSCREEN_TEXT_EDIT_POS_X + RUNSCREEN_TEXT_EDIT_SIZE_X
#define RUNSCREEN_TEXT_EDIT1_POS_Y     20
//Unit
#define RUNSCREEN_TEXT_EDIT2_SIZE_X    40
#define RUNSCREEN_TEXT_EDIT2_SIZE_Y    16
#define RUNSCREEN_TEXT_EDIT2_POS_X     RUNSCREEN_TEXT_EDIT1_POS_X + RUNSCREEN_TEXT_EDIT1_SIZE_X
#define RUNSCREEN_TEXT_EDIT2_POS_Y     80
//Rate
#define RUNSCREEN_TEXT_EDIT3_SIZE_X    70
#define RUNSCREEN_TEXT_EDIT3_SIZE_Y    16
#define RUNSCREEN_TEXT_EDIT3_POS_X     RUNSCREEN_TEXT_EDIT2_POS_X + RUNSCREEN_TEXT_EDIT2_SIZE_X
#define RUNSCREEN_TEXT_EDIT3_POS_Y     130
//Variable
#define RUNSCREEN_TEXT_EDIT4_SIZE_X    80
#define RUNSCREEN_TEXT_EDIT4_SIZE_Y    16
#define RUNSCREEN_TEXT_EDIT4_POS_X     RUNSCREEN_TEXT_EDIT3_POS_X + RUNSCREEN_TEXT_EDIT3_SIZE_X

#define RUNSCREEN_TEXT_EDIT4_POS_Y     RUNSCREEN_TEXT_EDIT3_POS_Y - 30
//Unit
#define RUNSCREEN_TEXT_EDIT5_SIZE_X    100
#define RUNSCREEN_TEXT_EDIT5_SIZE_Y    16
#define RUNSCREEN_TEXT_EDIT5_POS_X     (RUNSCREEN_TEXT_EDIT4_POS_X + RUNSCREEN_TEXT_EDIT4_SIZE_X )
#define RUNSCREEN_TEXT_EDIT5_POS_Y     RUNSCREEN_TEXT_EDIT3_POS_Y
//Speed
#define RUNSCREEN_TEXT_EDIT6_SIZE_X    RUNSCREEN_TEXT_EDIT3_SIZE_X
#define RUNSCREEN_TEXT_EDIT6_SIZE_Y    RUNSCREEN_TEXT_EDIT_SIZE_Y
#define RUNSCREEN_TEXT_EDIT6_POS_X     RUNSCREEN_TEXT_EDIT3_POS_X
#define RUNSCREEN_TEXT_EDIT6_POS_Y     185
//variable
#define RUNSCREEN_TEXT_EDIT7_SIZE_X    RUNSCREEN_TEXT_EDIT4_SIZE_X
#define RUNSCREEN_TEXT_EDIT7_SIZE_Y    RUNSCREEN_TEXT_EDIT1_SIZE_Y
#define RUNSCREEN_TEXT_EDIT7_POS_X     RUNSCREEN_TEXT_EDIT4_POS_X
#define RUNSCREEN_TEXT_EDIT7_POS_Y     RUNSCREEN_TEXT_EDIT6_POS_Y - 30
//unit
#define RUNSCREEN_TEXT_EDIT8_SIZE_X    RUNSCREEN_TEXT_EDIT5_SIZE_X
#define RUNSCREEN_TEXT_EDIT8_SIZE_Y    RUNSCREEN_TEXT_EDIT_SIZE_Y
#define RUNSCREEN_TEXT_EDIT8_POS_X     RUNSCREEN_TEXT_EDIT7_POS_X + (RUNSCREEN_TEXT_EDIT7_SIZE_X + 2)
#define RUNSCREEN_TEXT_EDIT8_POS_Y     RUNSCREEN_TEXT_EDIT6_POS_Y




/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
unsigned char accu_wt_unit[15];

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
U8 msg_count;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
void callback_weight_run_mode_screen(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/
U8 display_pcm_messages(char * error_msg);

/*****************************************************************************
* @note       Function name  : static void callback_weight_run_mode_screen(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Rajeev Gadgil
* @date       date created   : 23rd Dec 2014
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
void callback_weight_run_mode_screen(WM_MESSAGE * pMsg)
{
    int Id, NCode, Key_entry, i;
//    static RUNMODE_SCREEN_DATA *data;
//    Screen_org_data * new_screen_org;
//    static int sel;
		U8 current_level = 0;
		U8 new_level = 0;
  WM_HWIN hDlg;
  WM_HWIN hMulti;
  WM_HWIN hItem;
  hMulti = WM_GetDialogItem(hDlg, ID_MULTI_TEXT_1);

    switch (pMsg->MsgId)
    {
				 case WM_INIT_DIALOG:
							MULTIEDIT_SetAutoScrollV(hMulti, 1);
							hItem = WM_GetDialogItem(hDlg, GUI_ID_BUTTON5);
							BUTTON_SetFocussable(hItem, 0);				 
						break;
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);      /* Id of widget */
            NCode = pMsg->Data.v;                 /* Notification code */
            switch (Id)
            {
                case ID_MULTI_TEXT_1:
                    switch (NCode)
                    {
                        case WM_NOTIFICATION_SEL_CHANGED:
                            /* Change widget text changing the selection */
                            //sel   = ICONVIEW_GetSel(pMsg->hWinSrc);
                            break;
                    }
                    break;
            }
            break;			
        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_MULTI_TEXT_1: // Notifications sent by 'MULTI TEXT'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            break;

                        case GUI_KEY_LEFT_CUSTOM:
																
                            break;													

                        case GUI_KEY_RIGHT:									
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
															
                            break;

                        case GUI_KEY_UP:
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            break;

                        case GUI_KEY_DOWN:
															MULTIEDIT_SetAutoScrollV(hMulti, 1);													
                            break;

                        case GUI_KEY_ENTER:
                            break;

                        case GUI_KEY_BACK:
                            break;
                    }
                 break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}
/*****************************************************************************
* @note       Function name    : void Create_Weight_Run_mode_screen(int create_update_weight_screen_flag)
* @returns    returns          : None
* @param      arg1             : create_update_flag
* @author                      : Pandurang Khutal
* @date       date created     : 28th December 2012
* @brief      Description      : Initialize the scan and return lines of the keypad
* @note       Notes            : None
*****************************************************************************/
void Create_pcm_main_screen(int create_update_weight_screen_flag)
{
 //   char disp[ARRAY_SIZE];
//    RUNMODE_SCREEN_DATA * data;
		extern  LOCALM localm[];	
		int i,index,j;
	char disp[100];
	double accu_wt;
	
//	double accu_wt;
	unsigned int month,date,year;	
    U8 err_msg_nos = 0;	
	memset(disp, '\0', sizeof(disp));

    if (create_update_weight_screen_flag == CREATE_SCREEN)
    {
//				GUI_Clear();
        GUI_SetBkColor(GUI_BLACK);
			
				GUI_DrawRect(WINDOW_RECT_POS_X0,WINDOW_RECT_POS_Y0,WINDOW_RECT_POS_X1,WINDOW_RECT_POS_Y1);
				GUI_DrawLine(WINDOW_HEADER_LINE_POS_X0,WINDOW_HEADER_LINE_POS_Y0,WINDOW_HEADER_LINE_POS_X1,WINDOW_HEADER_LINE_POS_Y1);
	/*************line 1**************/		
//				sprintf(&disp[0], "IP Addr: %03d.%03d.%03d.%03d",LocM.IpAdr[0],LocM.IpAdr[1],LocM.IpAdr[2],LocM.IpAdr[3]);
				sprintf(&disp[0], "IP Addr: %03d.%03d.%03d.%03d",Pcm_var.IP_Addr.Addr1,Pcm_var.IP_Addr.Addr2,Pcm_var.IP_Addr.Addr3,Pcm_var.IP_Addr.Addr4);
			
				GUI_DispStringAt("   ", WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE1_POS_Y0);
				GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE1_POS_Y0);
				sprintf(&disp[0],"PCM ID: %d",PCM_ID);	
				GUI_DispStringAt("  ", DISP_2ND_PARAMETER_POS_X1-50, DISP_LINE1_POS_Y0);
				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1-50, DISP_LINE1_POS_Y0);
				memset(disp,'\0',sizeof(disp));
				if((Pcm_var.PCM_connection_select ==Screen213_str5)||(Pcm_var.PCM_connection_select ==Screen213_str3))
				{
						sprintf(disp,"Signal Strength:%s/%s",rssi_buf,signal_strength_buf);
//						strncpy(disp,disp,22);
//					sprintf(&signal_strength_buf[0],"  ");
//					cell_modem_strength = atoi(signal_strength_buf);
// 				 j= atoi(signal_strength_buf);	
// 						switch(j)
// 						{
// 							case 0 : //Found string "0" RSSI -115dBm or less
// 											cell_modem_strength = 1;
// 							break;
// 						
// 							case 1 : //Found string "10" RSSI -111dBm 
// 											cell_modem_strength = 2;
// 							break;
// 							
// 							case (31) : //Found string "31" RSSI -52dBm or Greater
// 											cell_modem_strength = 4;
// 							break;
// 							
// 							case (99) : //Found string "99" RSSI not known 
// 											cell_modem_strength = 5;
// 							break;
// 							
//               default:  //Found string "2...30" RSSI -110dBm ...-54dBm
// 										if((j > 1) && (j < 31))		
// 									cell_modem_strength = 3;
// 							break;
// 						}	

// 				sprintf(&disp[0],"Signal Strength: %02d ",cell_modem_strength);

				}
				else
				{
				sprintf(&disp[0],"Signal Strength: %02d ",cell_modem_strength);

				}
				GUI_DispStringAt("                       ", DISP_2ND_PARAMETER_POS_X1+70, DISP_LINE1_POS_Y0);
				
				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1+70, DISP_LINE1_POS_Y0);

			/*************line 2**************/		
				sprintf(&disp[0], "Gateway Addr: %03d.%03d.%03d.%03d",Pcm_var.Gateway.Addr1,Pcm_var.Gateway.Addr2,Pcm_var.Gateway.Addr3,Pcm_var.Gateway.Addr4);
				GUI_DispStringAt("  ", WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE2_POS_Y0);	

				GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE2_POS_Y0);	
				sprintf(&disp[0], "SUBMask: %03d.%03d.%03d.%03d",Pcm_var.Subnet_Mask.Addr1,Pcm_var.Subnet_Mask.Addr2,Pcm_var.Subnet_Mask.Addr3,Pcm_var.Subnet_Mask.Addr4);	
				GUI_DispStringAt("  ", DISP_2ND_PARAMETER_POS_X1, DISP_LINE2_POS_Y0);

				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1, DISP_LINE2_POS_Y0);
	/*************line 3**************/		
					
//				sprintf(&disp[0],"Signal Strength: %d",cell_modem_strength);
			sprintf(&disp[0],"PCM Web: %s",Http_Host_Name);
				if(pcs_connectivity_status == NOT_CONNECTED)
				{
					GUI_SetColor(	GUI_RED);
				}
				else
				{
					GUI_SetColor(	GUI_WHITE);
				}
				GUI_DispStringAt("  ", WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE3_POS_Y0);
			
				GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE3_POS_Y0);
				GUI_SetColor(	GUI_WHITE);

				sprintf(&disp[0],"BPS:           %d",Baud_rate);	
				GUI_DispStringAt("  ", DISP_2ND_PARAMETER_POS_X1+100, DISP_LINE3_POS_Y0);
				
				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1+100, DISP_LINE3_POS_Y0);

			
        GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(LISTBOX_START, (DISP_LINE5_POS_Y0 -1),
                                                     LISTBOX2_SIZE_X, (LISTBOX_SIZE_Y - 61), WM_HBKWIN,
                                                     WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
        MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
        MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
        MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
        MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
        MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
        MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);
				MULTIEDIT_SetAutoScrollV(GUI_data_nav.Multi_text,1);
				MULTIEDIT_SetAutoScrollH(GUI_data_nav.Multi_text,0);
				WM_SetFocus(GUI_data_nav.Multi_text);			
//				WM_SetCallback(WM_HBKWIN, callback_weight_run_mode_screen);
			
			
			
		

// 					i = 0;
// 					for(index=0; index < MAX_NO_OF_INTEGRATORS ; index++)						//MAX_NO_OF_INTEGRATORS
// 					{
// 							if(Modbus_Tcp_Slaves[index].Connect== FALSE)
// 							{		
// 								//fprintf(fptr, "Modbus_Tcp_Slaves[%d].Connect = %s\r\n",index,"FALSE");
// 								
// 							}	
// 							else if(Modbus_Tcp_Slaves[index].Connect== TRUE)
// 							{
// 							
// 								GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y + (i*16)),
// 																												 RUNSCREEN_TEXT_EDIT_SIZE_X, RUNSCREEN_TEXT_EDIT_SIZE_Y,
// 																												 GUI_data_nav.Multi_text, WM_CF_SHOW, 
// 																												 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
// 								TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
// 								TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
// 								sprintf(&disp[0],"Scale %d",(index+1));
// 								TEXT_SetText(GUI_data_nav.Text_widget, disp);	

// 						
// 								GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT1_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
// 																												 RUNSCREEN_TEXT_EDIT1_SIZE_X, RUNSCREEN_TEXT_EDIT1_SIZE_Y,
// 																												 GUI_data_nav.Multi_text, WM_CF_SHOW, 
// 																												 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
// 								TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
// 								TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
// 								sprintf(&disp[0],"%d.%d.%d.%d",Modbus_Tcp_Slaves[index].IP_ADRESS[3],Modbus_Tcp_Slaves[index].IP_ADRESS[2],\
// 																							Modbus_Tcp_Slaves[index].IP_ADRESS[1],Modbus_Tcp_Slaves[index].IP_ADRESS[0]);
// 								TEXT_SetText(GUI_data_nav.Text_widget, disp);	
// 								
// 								GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT2_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
// 																												 RUNSCREEN_TEXT_EDIT2_SIZE_X, RUNSCREEN_TEXT_EDIT2_SIZE_Y,
// 																												 GUI_data_nav.Multi_text, WM_CF_SHOW, 
// 																												 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
// 								TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
// 								TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
// 								sprintf(&disp[0],"S%d",(Modbus_Tcp_Slaves[index].Slave_ID));

// 								TEXT_SetText(GUI_data_nav.Text_widget, disp);	
// 								

// 								GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT3_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
// 																												 RUNSCREEN_TEXT_EDIT3_SIZE_X, RUNSCREEN_TEXT_EDIT3_SIZE_Y,
// 																												 GUI_data_nav.Multi_text, WM_CF_SHOW, 
// 																												 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
// 								TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
// 								TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
// 								sprintf(&disp[0],"%d",((Scale_Record_Mb_Reg[index].acc_wt.s_array[0]<<16) | (Scale_Record_Mb_Reg[index].acc_wt.s_array[1])));
// 								TEXT_SetText(GUI_data_nav.Text_widget, disp);	
// 								
// 								
// 								GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT4_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
// 																												 RUNSCREEN_TEXT_EDIT4_SIZE_X, RUNSCREEN_TEXT_EDIT4_SIZE_Y,
// 																												 GUI_data_nav.Multi_text, WM_CF_SHOW, 
// 																												 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
// 								TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
// 								TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
// 								TEXT_SetText(GUI_data_nav.Text_widget, unit);		
// 								
// 								GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT5_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
// 																												 RUNSCREEN_TEXT_EDIT5_SIZE_X, RUNSCREEN_TEXT_EDIT5_SIZE_Y,
// 																												 GUI_data_nav.Multi_text, WM_CF_SHOW, 
// 																												 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
// 								TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
// 								TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);		
// 								date = Scale_Record_Mb_Reg[index].Scale_Date.int_array[0]&0xFF00;
// 								date = date >> 8;
// 								month = Scale_Record_Mb_Reg[index].Scale_Date.int_array[0]&0x00FF;
// 								year = (Scale_Record_Mb_Reg[index].Scale_Date.int_array[1] - 2000);
// 								if(Pcm_var.Current_Date_Format == MMDDYYYY)/*MM/DD/YYYY format*/
// 								{
// 								sprintf(&disp[0],"%02d/%02d/%02d  %02d:%02d",month,date,year ,\
// 																																(Scale_Record_Mb_Reg[index].Scale_Time.int_array[1] ),\
// 																																(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0]&0x003F ));									
// 								}
// 								else
// 								{
// 								sprintf(&disp[0],"%02d/%02d/%02d  %02d:%02d",date,month,year ,\
// 																																(Scale_Record_Mb_Reg[index].Scale_Time.int_array[1] ),\
// 																																(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0]&0x003F ));
// 								}								
// 								TEXT_SetText(GUI_data_nav.Text_widget, disp);									

// 								
//  								i++;
// 							}
// 						
// 					}	
					
    }
    else if(create_update_weight_screen_flag == UPDATE_SCREEN) //update screen
    {

				
	/*************line 1**************/		
				memset(disp,'\0',sizeof(disp));
			
				sprintf(&disp[0], "IP Addr: %03d.%03d.%03d.%03d",Pcm_var.IP_Addr.Addr1,Pcm_var.IP_Addr.Addr2,Pcm_var.IP_Addr.Addr3,Pcm_var.IP_Addr.Addr4);
				GUI_DispStringAt("  ", WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE1_POS_Y0);
				
				GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE1_POS_Y0);
//				sprintf(&disp[0],"PCM ID: %d   FTP_%d",PCM_ID,Modbus_Tcp_Slaves[1].IP_ADRESS[0]);	
				memset(disp,'\0',sizeof(disp));
			
				sprintf(&disp[0],"PCM ID: %d",PCM_ID);	
				GUI_DispStringAt("  ", DISP_2ND_PARAMETER_POS_X1-50, DISP_LINE1_POS_Y0);
				
				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1-50, DISP_LINE1_POS_Y0);
				memset(disp,'\0',sizeof(disp));

				if((Pcm_var.PCM_connection_select ==Screen213_str5)||(Pcm_var.PCM_connection_select ==Screen213_str3))
				{
//				sprintf(&disp[0],"Signal Strength: %4s  dBm ",signal_strength_buf);
						sprintf(&disp[0],"Signal Strength:%s/%s",rssi_buf,signal_strength_buf);
					

//					cell_modem_strength = atoi(signal_strength_buf);
// 				 j= atoi(signal_strength_buf);	
// 						switch(j)
// 						{
// 							case 0 : //Found string "0" RSSI -115dBm or less
// 											cell_modem_strength = 1;
// 							break;
// 						
// 							case 1 : //Found string "10" RSSI -111dBm 
// 											cell_modem_strength = 2;
// 							break;
// 							
// 							case (31) : //Found string "31" RSSI -52dBm or Greater
// 											cell_modem_strength = 4;
// 							break;
// 							
// 							case (99) : //Found string "99" RSSI not known 
// 											cell_modem_strength = 5;
// 							break;
// 							
//               default:  //Found string "2...30" RSSI -110dBm ...-54dBm
// 										if((j > 1) && (j < 31))		
// 									cell_modem_strength = 3;
// 							break;
// 						}	
// 				sprintf(&disp[0],"Signal Strength: %02d ",cell_modem_strength);
						
				}
				else
				{
				sprintf(&disp[0],"Signal Strength: %02d ",cell_modem_strength);

				}
				GUI_DispStringAt("                        ", DISP_2ND_PARAMETER_POS_X1+70, DISP_LINE1_POS_Y0);
				
				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1+70, DISP_LINE1_POS_Y0);
				
	/*************line 2**************/		
				memset(disp,'\0',sizeof(disp));
				
				sprintf(&disp[0], "Gateway Addr: %03d.%03d.%03d.%03d",Pcm_var.Gateway.Addr1,Pcm_var.Gateway.Addr2,Pcm_var.Gateway.Addr3,Pcm_var.Gateway.Addr4);
				GUI_DispStringAt("  ", WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE2_POS_Y0);	
				
				GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE2_POS_Y0);	
				memset(disp,'\0',sizeof(disp));
				
				sprintf(&disp[0], "SUBMask: %03d.%03d.%03d.%03d",Pcm_var.Subnet_Mask.Addr1,Pcm_var.Subnet_Mask.Addr2,Pcm_var.Subnet_Mask.Addr3,Pcm_var.Subnet_Mask.Addr4);	
				GUI_DispStringAt("  ", DISP_2ND_PARAMETER_POS_X1, DISP_LINE2_POS_Y0);
				
				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1, DISP_LINE2_POS_Y0);
	/*************line 3**************/		
					memset(disp,'\0',sizeof(disp));
				
				if(pcs_connectivity_status == NOT_CONNECTED)
				{
					GUI_SetColor(	GUI_RED);
				}
				else
				{
					GUI_SetColor(	GUI_WHITE);
				}
				sprintf(&disp[0],"PCM Web: %s",Http_Host_Name);//Http_Host_Name
				GUI_DispStringAt("  ", WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE3_POS_Y0);
				
				GUI_DispStringAt(disp, WINDOW_HEADER_TEXT1_POS_X0, DISP_LINE3_POS_Y0);
					GUI_SetColor(	GUI_WHITE);
				memset(disp,'\0',sizeof(disp));
				
				sprintf(&disp[0],"BPS:           %d",Baud_rate);	
				GUI_DispStringAt("  ", DISP_2ND_PARAMETER_POS_X1+100, DISP_LINE3_POS_Y0);
				
				GUI_DispStringAt(disp, DISP_2ND_PARAMETER_POS_X1+100, DISP_LINE3_POS_Y0);
// 				GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y ),
// 																								 LISTBOX2_SIZE_X, (LISTBOX_SIZE_Y - 61),
// 																								 GUI_data_nav.Multi_text, WM_CF_SHOW, 
// 																								 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
// 				TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
// 				TEXT_SetText(GUI_data_nav.Text_widget, " ");
        WM_DeleteWindow(GUI_data_nav.Multi_text);
        GUI_data_nav.Multi_text = 0;

        GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(LISTBOX_START, (DISP_LINE5_POS_Y0 -1),
                                                     LISTBOX2_SIZE_X, (LISTBOX_SIZE_Y - 61), WM_HBKWIN,
                                                     WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
        MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
        MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
        MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
        MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
        MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
        MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);
				MULTIEDIT_SetAutoScrollV(GUI_data_nav.Multi_text,1);
				MULTIEDIT_SetAutoScrollH(GUI_data_nav.Multi_text,0);
				WM_SetFocus(GUI_data_nav.Multi_text);		

			if((disp_power_on_screen_timeout_fg == 0)||(Scale_configuration_received_ok_fg==1))
			{		
				i = 0;
				for(index=0; index < Total_Scales; index++)
				{
// 						if(Modbus_Tcp_Slaves[index].Connect== FALSE)
// 						{		
// 							//fprintf(fptr, "Modbus_Tcp_Slaves[%d].Connect = %s\r\n",index,"FALSE");
// 							
// 						}	
						if(1) 
						{
									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y + (i*16)),
																													 RUNSCREEN_TEXT_EDIT_SIZE_X, RUNSCREEN_TEXT_EDIT_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									if(Modbus_Tcp_Slaves[index].Connect== TRUE)
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
									}
									else if((Modbus_Tcp_Slaves[index].Connect== FALSE)||(pcs_connectivity_status == NOT_CONNECTED))
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									}
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
									memset(disp,'\0',sizeof(disp));

									sprintf(&disp[0],"Scale %d",(index+1));
									TEXT_SetText(GUI_data_nav.Text_widget, " ");	
									TEXT_SetText(GUI_data_nav.Text_widget, disp);	

							
									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT1_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
																													 RUNSCREEN_TEXT_EDIT1_SIZE_X, RUNSCREEN_TEXT_EDIT1_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									if(Modbus_Tcp_Slaves[index].Connect== TRUE)
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
									}
									else if((Modbus_Tcp_Slaves[index].Connect== FALSE)||(pcs_connectivity_status == NOT_CONNECTED))
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									}
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);		
									memset(disp,'\0',sizeof(disp));
							
									sprintf(&disp[0],"%d.%d.%d.%d",Modbus_Tcp_Slaves[index].IP_ADRESS[3],Modbus_Tcp_Slaves[index].IP_ADRESS[2],\
																								Modbus_Tcp_Slaves[index].IP_ADRESS[1],Modbus_Tcp_Slaves[index].IP_ADRESS[0]);
									TEXT_SetText(GUI_data_nav.Text_widget, " ");
									TEXT_SetText(GUI_data_nav.Text_widget, disp);	
									
									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT2_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
																													 RUNSCREEN_TEXT_EDIT2_SIZE_X, RUNSCREEN_TEXT_EDIT2_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									if(Modbus_Tcp_Slaves[index].Connect== TRUE)
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
									}
									else if((Modbus_Tcp_Slaves[index].Connect== FALSE)||(pcs_connectivity_status == NOT_CONNECTED))
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									}
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);	
									memset(disp,'\0',sizeof(disp));

									sprintf(&disp[0],"S%d",(Modbus_Tcp_Slaves[index].Slave_ID));
									TEXT_SetText(GUI_data_nav.Text_widget, " ");
									TEXT_SetText(GUI_data_nav.Text_widget, disp);	
									

									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT3_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
																													 RUNSCREEN_TEXT_EDIT3_SIZE_X, RUNSCREEN_TEXT_EDIT3_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									if(Modbus_Tcp_Slaves[index].Connect== TRUE)
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
									}
									else if((Modbus_Tcp_Slaves[index].Connect== FALSE)||(pcs_connectivity_status == NOT_CONNECTED))
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									}
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);	
									sprintf(&disp[0],"          ");
									TEXT_SetText(GUI_data_nav.Text_widget, disp);
									memset(disp,'\0',sizeof(disp));

									sprintf(&disp[0],"%d",((Scale_Record_Mb_Reg[index].acc_wt.s_array[0]<<16) | (Scale_Record_Mb_Reg[index].acc_wt.s_array[1])));
									TEXT_SetText(GUI_data_nav.Text_widget, disp);	
									
									
									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT4_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
																													 RUNSCREEN_TEXT_EDIT4_SIZE_X, RUNSCREEN_TEXT_EDIT4_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									if(Modbus_Tcp_Slaves[index].Connect== TRUE)
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
									}
									else if((Modbus_Tcp_Slaves[index].Connect== FALSE)||(pcs_connectivity_status == NOT_CONNECTED))
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									}
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
									TEXT_SetText(GUI_data_nav.Text_widget, " ");
									switch(Scale_Record_Mb_Reg[index].accu_wt_unit.uint_val)
									{
										case 100:		sprintf(&accu_wt_unit[0],"LBS");
																break;
										case 101:		sprintf(&accu_wt_unit[0],"TONS");
																break;
										case 102:		sprintf(&accu_wt_unit[0],"LongTon");
																break;
										case 103:		sprintf(&accu_wt_unit[0],"Kg");
																break;
										case 104:		sprintf(&accu_wt_unit[0],"Tonne");
																break;
										default	:		break;

									}									
									TEXT_SetText(GUI_data_nav.Text_widget, accu_wt_unit);		
									
									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT5_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(i*16)),
																													 RUNSCREEN_TEXT_EDIT5_SIZE_X, RUNSCREEN_TEXT_EDIT5_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									if(Modbus_Tcp_Slaves[index].Connect== TRUE)
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_WHITE);
									}
									else if((Modbus_Tcp_Slaves[index].Connect== FALSE)||(pcs_connectivity_status == NOT_CONNECTED))
									{
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									}
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);		
									date = Scale_Record_Mb_Reg[index].Scale_Date.int_array[0]&0xFF00;
									date = date >> 8;
									month = Scale_Record_Mb_Reg[index].Scale_Date.int_array[0]&0x00FF;
									year = (Scale_Record_Mb_Reg[index].Scale_Date.int_array[1] - 2000);
									memset(disp,'\0',sizeof(disp));

									if(Pcm_var.Current_Date_Format == MMDDYYYY)/*MM/DD/YYYY format*/
									{
									sprintf(&disp[0],"%02d/%02d/%02d  %02d:%02d",month,date,year ,\
																																	(Scale_Record_Mb_Reg[index].Scale_Time.int_array[1]&0x001F ),\
																																	(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0]&0x003F ));									
									}
									else
									{
									sprintf(&disp[0],"%02d/%02d/%02d  %02d:%02d",date,month,year ,\
																																	(Scale_Record_Mb_Reg[index].Scale_Time.int_array[1] &0x001F ),\
																																	(Scale_Record_Mb_Reg[index].Scale_Time.int_array[0]&0x003F ));
									}	
									TEXT_SetText(GUI_data_nav.Text_widget, disp);						
							i++;
						}
				
				}	
			}
			//BY MEGHA ON 1/2/2017 
			else 							
			{
									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(4*16) ),
																													 RUNSCREEN_TEXT_EDIT_SIZE_X+400, RUNSCREEN_TEXT_EDIT_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
									sprintf(&disp[0],"CHECK PCM ID ,WEB ADDRESS,CONNECTION TYPE ");
									TEXT_SetText(GUI_data_nav.Text_widget, " ");	
									TEXT_SetText(GUI_data_nav.Text_widget, disp);	
				
									GUI_data_nav.Text_widget = TEXT_CreateEx(RUNSCREEN_TEXT_EDIT_POS_X, (RUNSCREEN_TEXT_EDIT_POS_Y+(5*16) ),
																													 RUNSCREEN_TEXT_EDIT_SIZE_X+400, RUNSCREEN_TEXT_EDIT_SIZE_Y,
																													 GUI_data_nav.Multi_text, WM_CF_SHOW, 
																													 TEXT_CF_HCENTER, GUI_ID_TEXT5, NULL);
									TEXT_SetTextColor(GUI_data_nav.Text_widget, GUI_RED);
									TEXT_SetFont(GUI_data_nav.Text_widget, &GUI_FontArial_Unicode_MS16_Bold);							
									sprintf(&disp[0]," AND SCALE CONFIGURATION                    ");
									TEXT_SetText(GUI_data_nav.Text_widget, " ");	
									TEXT_SetText(GUI_data_nav.Text_widget, disp);					
									Flags_struct.Plant_connect_record_flag = 0;
				
// 				if(pcs_connect_disp_fg == 1)
// 				{
// 					pcs_connect_disp_fg = 0;
// 					disp_power_on_screen_timeout_fg = 0;
// 					disp_power_on_screen_fg = 0;
// 					power_on_fg = 1;
// 				}					
			}
			
    }
		
		
    return;
}

		
/*****************************************************************************
* End of file
*****************************************************************************/
