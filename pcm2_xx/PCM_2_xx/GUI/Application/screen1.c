/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen1.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"
#include "Screen_global_data.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen1(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : static void callback_screen1(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen1(WM_MESSAGE * pMsg)
{
    int NCode, Id, i, system_unlocked = 0;
    static int sel = 0;
    static SCREEN_TYPE1_DATA * data;
    Screen_org_data * new_screen_org;

    data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);      /* Id of widget */
            NCode = pMsg->Data.v;                 /* Notification code */
            switch (Id)
            {
                case GUI_ID_ICONVIEW0:
                    switch (NCode)
                    {
                        case WM_NOTIFICATION_SEL_CHANGED:
                            /* Change widget text changing the selection */
                            sel   = ICONVIEW_GetSel(pMsg->hWinSrc);
                            break;
                    }
                    break;
            }
            break;

        case WM_KEY:
            switch (((WM_KEY_INFO*)(pMsg->Data.p))->Key)
            {
                case GUI_KEY_LEFT_CUSTOM:
                    ;
                    break;

                case GUI_KEY_LEFT:
                    ;
                    break;

                case GUI_KEY_RIGHT:
                    ;
                    break;

                case GUI_KEY_RIGHT_CUSTOM:
                    ;
                    break;

                case GUI_KEY_UP:
                    ;
                    break;

                case GUI_KEY_UP_CUSTOM:
                    ;
                    break;

                case GUI_KEY_DOWN:
                    ;
                    break;

                case GUI_KEY_ENTER:
                    sel = ICONVIEW_GetSel(pMsg->hWinSrc);
                    if (data[sel].screen_an_option != 0)
                    {
                        if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAIN_MENU_ORG_NO) == 0) && ((sel == 0))) 
                        {
													break;
											
// 												if(!security_bit_set())
// 												{
// 														system_unlocked = 1;
// 												}
// 												else
// 												{
// 														if(password_struct.password_entered)
// 														{
// 																system_unlocked = 1;
// 														}
// 												}
// 												if(system_unlocked)
// 												{
// 														new_screen_org = GUI_data_nav.Current_screen_info->child;
// 														new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
// 														for (i = 0; i < 6; i++)
// 														{
// 																GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
// 														}
// 														GUI_data_nav.Change = 1;
// 														GUI_data_nav.Child_present = 1;
// 												}
// 												else
// 												{
// 														//create the password screen, store the current screen org number to come back to it
// 														//in case the password entered is correct
// 														GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info->child;
// 														GUI_data_nav.Screen_before_pw_org_num = &GUI_data_nav.Screen_before_pw_org_num[(data[sel].screen_an_option - 1)];
// 														GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
// 														password_struct.password_to_be_entered = 1;
// 												}	
											}
											else
											{
                            new_screen_org = GUI_data_nav.Current_screen_info->child;
                            new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                            for (i = 0; i < 6; i++)
                            {
                                GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                            }
                            GUI_data_nav.Change = 1;
                            GUI_data_nav.Child_present = 1;
												
											}
											
										}
                    else
                    {
                      GUI_data_nav.Child_present = 0;
                    }
                    GUI_data_nav.Key_press = 0;
                    data[0].sel_icon = sel;
                    GUI_ClearKeyBuffer();
                    break;										

                case GUI_KEY_BACK:
                    go_back_one_level();
                    data[0].sel_icon = 0;
                    break;
            }
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen1(SCREEN_TYPE1_DATA *icon_bitmaps, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the icons to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the icon screen and sets the default settings
*                            : of the icons. Also used to reset the flags if user has
*                            : entered the password and security bit is enabled.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen1(SCREEN_TYPE1_DATA *icon_bitmaps, int count)
{
    int i;

    /* Create iconview widget */
    GUI_data_nav.Icon_screen = ICONVIEW_CreateEx(ICONVIEW_START_POS_X0, ICONVIEW_START_POS_Y0, ICONVIEW_START_SIZE_X, ICONVIEW_START_SIZE_Y, WM_HBKWIN, WM_CF_SHOW, ICONVIEW_CF_AUTOSCROLLBAR_V, GUI_ID_ICONVIEW0, 153, 96);

    WM_SetCallback(WM_HBKWIN, callback_screen1);
    for (i = 0; i < count; i++)
    {
        /*Add icons to the widget   */
        ICONVIEW_AddBitmapItem(GUI_data_nav.Icon_screen, icon_bitmaps[i].pBitmap, Strings[icon_bitmaps[i].index].Text);
    }
    ICONVIEW_SetBkColor(GUI_data_nav.Icon_screen, ICONVIEW_CI_BK, GUI_BLACK);
    ICONVIEW_SetBkColor(GUI_data_nav.Icon_screen, ICONVIEW_CI_SEL, GUI_LIGHTYELLOW );
    ICONVIEW_SetTextColor(GUI_data_nav.Icon_screen, ICONVIEW_CI_BK, GUI_WHITE);
    ICONVIEW_SetTextColor(GUI_data_nav.Icon_screen, ICONVIEW_CI_SEL, GUI_BLACK);
    ICONVIEW_SetFont(GUI_data_nav.Icon_screen, &GUI_FontArial_Unicode_MS16_Bold);
    ICONVIEW_SetSpace(GUI_data_nav.Icon_screen, GUI_COORD_X, ICONVIEW_SPACE_X_SIZE);
    ICONVIEW_SetSpace(GUI_data_nav.Icon_screen, GUI_COORD_Y, ICONVIEW_SPACE_Y_SIZE);
    WM_SetFocus(GUI_data_nav.Icon_screen);

    ICONVIEW_SetSel(GUI_data_nav.Icon_screen, icon_bitmaps[0].sel_icon);

    if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, CAL_SCREEN_ORG_NO) == 0) ||
       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SETUP_DEVICES_ORG_NO) == 0) ||
       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, ADMIN_SCREEN_ORG_NO) == 0) ||
       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAIN_MENU_ORG_NO) == 0)		||
			 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PCM_SETUP_ORG_NO) == 0)) 
    {
        if( password_struct.password_entered)
        {
            password_struct.password_entered = 0;
        }
    }
    return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
