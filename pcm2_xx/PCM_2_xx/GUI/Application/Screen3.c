/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen3.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "../GUI/Application/Screen_global_ex.h"
#include "Screen_data_enum.h"
//#include "Calibration.h"
//#include "Conversion_functions.h"
#include "Global_ex.h"
//#include "Rate_blending_load_ctrl_calc.h"
//#include "Firmware_upgrade.h"
#include "File_update.h"
#include "main.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define WEIGHT_RATE Screen21_str2
#define LOAD_OUT 		Screen21_str8
#define BLENDING 		Screen21_str11
#define LOAD_CNTRL	Screen21_str14
#define RATE_CNTRL  Screen21_str17

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
unsigned char restore_factory_set_fg;
/* unsigned integer variables section */
unsigned int parameter_code;
/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */
extern PCM_STRUCT         Pcm_var;
extern unsigned char Struct_backup_fg;
/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen3(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : static void callback_screen3(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen3(WM_MESSAGE * pMsg)
{

    int Id, NCode, sel, Key_entry, i, system_unlocked = 0;
    Screen_org_data * new_screen_org;
    static SCREEN_TYPE3_DATA *data;
    TIME_STRUCT Temp_time;
	  char str_buf[25];
		int screen_no;
    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
    data = GUI_data_nav.Current_screen_info->screen_data;
    
    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            break;

                        case WM_NOTIFICATION_SEL_CHANGED:
                            MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
														if (data[0].Description_text_index != 0)
														{
																MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
																MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Description_text_index].Text);
																MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
														}
														else
														{
																MULTIEDIT_AddText(GUI_data_nav.Multi_text, " ");
														}
														if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_UNIT_SCREEN) == 0) &&
															 (Scale_setup_var.Distance_unit == Screen24_str5)) //If Metric = distance_unit
														{
																MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
																switch(sel)
																{
																	 case 0:
																		 MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[4].Description_text_index].Text);
																		 break;

																	 case 1:
																		 //MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[5].Description_text_index].Text);
																		 break;
																}
														}//if weight screen and metric = distance unit
														else
														{
																if (data[sel+1].Description_text_index != 0)
																{
																	if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PRINTER_TYPE_SELECT_ORG_NO) == 0) && (sel == 1))
																	{
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[Screen4122_str3].Text);
																		sprintf(str_buf,"\n Baud %d\n",Setup_device_var.Printer_baud);
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, str_buf);
																		memset(str_buf,0,sizeof(str_buf));
																		sprintf(str_buf," Data Bits %d\n",Setup_device_var.Printer_data_bits);
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, str_buf);
																		memset(str_buf,0,sizeof(str_buf));
																		sprintf(str_buf," Stop Bits %d\n",Setup_device_var.Printer_stop_bits);
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, str_buf);
																		memset(str_buf,0,sizeof(str_buf));
																		sprintf(str_buf," Flow Control '%s'\n",Strings[Setup_device_var.Printer_flow_control].Text);
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, str_buf);
																	}
																	else
																	{
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
																		MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[sel+1].Description_text_index].Text);
																	}
																}
														}
                            WM_SetFocus(GUI_data_nav.Listbox);
                            break;
                    }
                 break;

                default:
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CHILD_DELETED:
                            WM_SetFocus(GUI_data_nav.Listbox);
                            GUI_ClearKeyBuffer();
                            LISTBOX_SetSel(GUI_data_nav.Listbox, 1);
                            break;
                    }
                 break;
            }
         break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            ;
                           break;

                        case GUI_KEY_LEFT_CUSTOM:
													  if (GUI_data_nav.In_wizard == 1) 
														{
															 GUI_data_nav.Cancel_operation = 1;
															 GUI_data_nav.Key_press = 1;
														}
														else if(!(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SETUP_WIZARD_ORG_NO)))
														{
															 go_back_one_level();
														}
                            break;

                        case GUI_KEY_RIGHT:
                            ;
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            if (GUI_data_nav.In_wizard == 1)
                            {
                                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_START_CON_ORG_NO) == 0))
                                {
                                    GUI_data_nav.Key_press = 1;
                                    GUI_data_nav.Up_right_key_pressed = 1;
                                }
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_OLD_VER_WRN_ORG_NO) == 0)
                                {
                                    strcpy(GUI_data_nav.New_screen_num, INT_UPDT_FILE_VIEW_ORG_NO);
                                    GUI_data_nav.Change = 1;
                                }
                            }
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;

                        case GUI_KEY_ENTER:
                            sel = LISTBOX_GetSel(GUI_data_nav.Listbox);	
												//by megha on 29/09/2017 for restore factory settings....
														if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_FACTORY_SETTINGS_SCREEN) == 0)
														{
																if(sel ==0){
																			GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);

																	GUI_DispStringAt("Message: Factory settings in process.......", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);
																	screen_variables_init(); /*Initialize with default values*/
//																	eeprom_first_time_write();
																	Struct_backup_fg =1;
 																	eeprom_struct_backup();
																	Struct_backup_fg =0;
 																	Restore_values_to_original_pcm_variables();	
//																	restore_factory_set_fg = 1;
																	
// 																	eeprom_first_time_write();
// 																	Restore_values_to_original_pcm_variables();
//																	Backup_var_into_usb();//go_back_one_level();
//																					GUI_data_nav.Back_one_level = GO_BACK_FLAG;														
//																				 GUI_data_nav.Wizard_screen_index = 0;
//																				 GUI_data_nav.Key_press = 1;
//																				 GUI_data_nav.Cancel_operation = 0;														
																	}																			
														}
												
														if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, USB_BACKUP_RESTORE_SCREEN) == 0)
														{
																if(sel ==0){
																			GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);

																	GUI_DispStringAt("Message: Writing to USB.......", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);
//																	Backup_var_into_usb();//go_back_one_level();
//																					GUI_data_nav.Back_one_level = GO_BACK_FLAG;														
//																				 GUI_data_nav.Wizard_screen_index = 0;
//																				 GUI_data_nav.Key_press = 1;
//																				 GUI_data_nav.Cancel_operation = 0;														
																	}																			
														}	
														if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PCM_WEB_SELECT_ORG_NO) == 0)
														{
																if(sel ==0)
																{
																	strcpy(Http_Host_Name,"http://plantconnect-api-p.azurewebsites.net");
		
																}	
																else if(sel == 1)
																{
																	strcpy(Http_Host_Name,"http://plantconnect-api.azurewebsites.net");

																}
														}															
														if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PCM_CONNECTION_SELECT_ORG_NO) == 0)
														{
																if((sel ==0)||(sel ==2))
																{
																	Pcm_var.PPP_Status = Screen24_str10;		
																}	
																else if(sel == 1)
																{
																	Pcm_var.PPP_Status = Screen24_str9;

																}
															
														}
// 														if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PPP_STATUS_ORG_NO) == 0)
// 														{
// 															
// 																if(sel ==0)
// 																{
// 																	if((Pcm_var.PCM_connection_select == Screen213_str1)||(Pcm_var.PCM_connection_select == Screen213_str5))
// 																	{
// 																		Pcm_var.PPP_Status = Screen24_str10;		
// 																	}	
// 																	else if(Pcm_var.PCM_connection_select == Screen213_str3)
// 																	{
// 																		Pcm_var.PPP_Status = Screen24_str9;

// 																	}																	
// 																		
// 																}	
// 																else if(sel == 1)
// 																{
// 																	Pcm_var.PPP_Status = Screen24_str10;
// 																	Pcm_var.PCM_connection_select = Screen213_str1;

// 																}
// 															
// 														}														
	
                            if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PWD_ERROR_ORG_NO) == 0) &&
                                 (GUI_data_nav.In_wizard != 1) && (sel == 1))
                            {
                                //Password entered is incorrect so go back to the previous screen
                                strcpy(GUI_data_nav.New_screen_num, GUI_data_nav.Screen_before_pw_org_num->Screen_org_no);
                                GUI_data_nav.Change = 1;
                                GUI_data_nav.Clr_wt_pw_to_be_entered = CLR_WT_FLAG_RESET;
                            }
                            if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, FIRMWARE_UPGRADE_ORG_NO) == 0)
                            {
                                if(sel != 0)
                                {
                                    //cancel the upgrade
                                    GUI_data_nav.Cancel_operation = 1;
                                }
                                else
                                {
                                    GUI_data_nav.Key_press = 1;
                                }
                            }
														else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, BEGIN_FIRMWARE_UPDT_ORG_NO) == 0)
                            {
                                  //ensure that the logs if present in flash have been updated in the USB
                                  //then allow the flash upgrade to proceed since the external flash will be
                                  //erased completely and the stored data will be lost
                                  if(!(Flags_struct.Log_flags & LOGS_WR_FLAG))
                                  {
                                      //set the flag to upgrade the firmware
																		if(Flags_struct.Connection_flags & USB_CON_FLAG)
																		{
                                      FwUpgrade(Pcm_var.Int_update_file_name, sizeof(Pcm_var.Int_update_file_name));

																			GUI_data_nav.Key_press = 1;																			
                                      GUI_data_nav.Cancel_operation = 1;
																		}
																		else 
																	  {		
																			 GUI_data_nav.Back_one_level = GO_BACK_FLAG;

																				GUI_data_nav.Wizard_screen_index = 0;
																				GUI_data_nav.Key_press = 1;
                                        GUI_data_nav.Cancel_operation = 0;
																		}
                                      
                                  }
                            }
														else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PRINTER_TICKET_NUMBER_RESET) == 0 && (sel == 0))
														{
																Setup_device_var.Printer_ticket_num = 1;
														}
														else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LOAD_CELL_SIZE_SCREEN) == 0 && GUI_data_nav.Setup_wizard == __TRUE && sel == 9)
														{
															parent_wizard.w_info = GUI_data_nav.wizard_screen;
															parent_wizard.w_index = GUI_data_nav.Wizard_screen_index;
															GUI_data_nav.wizard_screen = &Org_230000_screen;
															GUI_data_nav.Wizard_screen_index = 0;
															GUI_data_nav.Key_press = 1;
                              GUI_data_nav.Cancel_operation = 0;
														}
														else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_USB_CHK_WIZARD_ORG_NO) == 0) && 
															     (!(Flags_struct.Connection_flags & USB_CON_FLAG)))
														{
															    GUI_data_nav.Back_one_level = GO_BACK_LOOP_FLAG;
																	GUI_data_nav.Wizard_screen_index = 0;
																	GUI_data_nav.Key_press = 1;
																	GUI_data_nav.Cancel_operation = 0;
																		
														}
                            else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_OLD_VER_WRN_ORG_NO) == 0)
                            {
                                //go to warn data loss version screen
                                strcpy(GUI_data_nav.New_screen_num, FIRMWARE_UPGRADE_ORG_NO);
                                GUI_data_nav.Change = 1;
                            }
                            if (data[sel+1].screen_an_option != 0)
                            {
                                if(!security_bit_set())
                                {
                                    system_unlocked = 1;
                                }
                                else
                                {
                                    if(password_struct.password_entered)
                                    {
                                        system_unlocked = 1;
                                    }
                                }
                                if(system_unlocked)
                                {
                                    new_screen_org = GUI_data_nav.Current_screen_info->child;
                                    new_screen_org = &new_screen_org[(data[sel+1].screen_an_option - 1)];
                                    for (i = 0; i < 6; i++)
                                    {
                                        GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                    }
                                    GUI_data_nav.Change = 1;
                                    GUI_data_nav.Child_present = 1;
                                }
                                else
                                {
                                    //create the password screen, store the current screen org number to come back to it
                                    //in case the password entered is incorrect
																	 if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PWD_ERROR_ORG_NO))
                                      GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info;
                                    GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
                                    password_struct.password_to_be_entered = 1;
                                }
                            }  
														// For wizard Screens in below if block it will fetch new screen from wizard structure.
														else if (GUI_data_nav.Current_screen_info->screens != NULL)
														{
															// For only setup wizard firmware will check for password security if it is enabled in Admin>secrity screen.
															if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SETUP_WIZARD_ORG_NO) == 0) 
															{
																if(!security_bit_set())
																{
                                    system_unlocked = 1;
																}
																else
																{
                                    if(password_struct.password_entered)
                                    {
                                        system_unlocked = 1;
                                    }
																}
															}
															else
															{
																 system_unlocked = 1;
															}
															if(system_unlocked)
															{
															    GUI_data_nav.Change = 1;
													        GUI_data_nav.In_wizard = __TRUE;
																  for (i = 0; i < 6; i++)
                                  {
                                    GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->screens->Next->number[i];
                                  }
															}
															else
                              {
                                    //create the password screen, store the current screen org number to come back to it
                                    //in case the password entered is correct
                                    GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info;
                                    //strcpy(GUI_data_nav.Screen_before_pw_org_num->Screen_org_no, GUI_data_nav.Current_screen_info->Screen_org_no);
                                    GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
                                    password_struct.password_to_be_entered = 1;
                              }
														}
                            else if (!GUI_data_nav.In_wizard)
                            {
																
																//specifically for screen no. 3 to return back to the previous screen
                                go_back_one_level();
                                GUI_data_nav.Child_present = 0;
                            }
                            else
                            {
                              GUI_data_nav.Child_present = 0;
                            }
                            GUI_data_nav.Key_press = 1;
                            //If the variable dependent on screen needs to be modified or the files for firmware
                            //upgrade are displayed.
                            if (data[sel+1].Variable1 != NULL)
                            {
                                if(!security_bit_set())
                                {
                                    system_unlocked = 1;
                                }
                                else
                                {
                                    if(password_struct.password_entered)
                                    {
                                        system_unlocked = 1;
                                    }
                                }
                                if(system_unlocked)
                                {
                                    if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_UNIT_SCREEN) == 0)
                                    {
                                        //Set the weight unit depending on the listbox selection and if distance unit
                                         //is English/Metric
//																				set_weight_unit(sel+1);
                                        //convert the current weight values if the measurement unit has changed
 //                                       Convert_user_weight_values(prev_weight_unit);
                                    }
                                    else
                                    {

                                        //update the respective variable with the listbox selection
																				if(data[sel+1].data_type == unsigned_char_type)
																					*data[sel+1].Variable1 = data[sel+1].Main_text_index;
																				else
                                          custom_sscanf(data[sel+1].data_type, data[sel+1].Variable1, Strings[data[sel+1].Main_text_index].Text);
																				
																				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, AM_PM_SET_ORG_NO) == 0)
																				{
																					 Temp_time.Hr = Current_time.RTC_Hour;
																					 Temp_time.Min = Current_time.RTC_Min;
																					 Temp_time.Sec = Current_time.RTC_Sec;
//																					 Set_rtc_time(Temp_time);																				 
																				}
																				if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LANGUAGE_SELECTION_SCREEN_ORG_NO) == 0)
																				{
																						Misc_var.Language_select = *data[sel+1].Variable1;
																				}
                                    }

																		if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DHCP_ENABLE_ORG_NO) == 0)
																		{
																			//Commented on 3 May2016 to retain the IP address before DHCP en/disbaled
																			/*
																			if(sel == 1) // If DHCP disable option is selected
																			{
																			  Admin_var.Gateway.Addr1 = 192;
																				Admin_var.Gateway.Addr2 = 168;
																				Admin_var.Gateway.Addr3 = 1;
																				Admin_var.Gateway.Addr4 = 1;
																				Admin_var.IP_Addr.Addr1 = 192;
																				Admin_var.IP_Addr.Addr2 = 168;
																				Admin_var.IP_Addr.Addr3 = 1;
																				Admin_var.IP_Addr.Addr4 = 100;
																				Admin_var.Subnet_Mask.Addr1 = 255;
																				Admin_var.Subnet_Mask.Addr2 = 255;
																				Admin_var.Subnet_Mask.Addr3 = 255;
																				Admin_var.Subnet_Mask.Addr4 = 0;
																			}*/
																			dhcp_chk_enable_disable();	
																		}
																		screen_no = atoi(GUI_data_nav.Current_screen_info->Screen_org_no);
																		switch(screen_no)
																		{
																			case 212000:
																										parameter_code = PCM_WEB_SELECT_CODE;
//																										scale_config_req_fg = 1;
																										break;
																			case 213000:
																										parameter_code = PCM_CONNECT_SELECT_CODE;
//																										scale_config_req_fg = 1;
																										break;				
																			case 241000:
																										parameter_code = PPP_STATUS_CODE;
																										break;
																			case 251000:
																										parameter_code = DST_STATUS_CODE;
																										break;																			
																			case 311000:
																										parameter_code = DATE_FORMAT_CODE;
																										break;																			

																			case 221000:
																										parameter_code = DHCP_STATUS_CODE;
																										break;
																		
																											
																		}
																		log_edit_para();
                                    GUI_data_nav.GUI_structure_backup = 1;
                                } //system unlocked
                                else
                                {
                                    //create the password screen, store the current screen org number to come back to it
                                    //in case the password entered is correct
                                    GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info;
                                    //strcpy(GUI_data_nav.Screen_before_pw_org_num->Screen_org_no, GUI_data_nav.Current_screen_info->Screen_org_no);
                                    GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
                                    password_struct.password_to_be_entered = 1;
                                }
                            } //if update variable present
														// if user selects cancel option in password error screen 
														if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PWD_ERROR_ORG_NO) == 0) && sel == 1)
														{
															  for (i = 0; i < 6; i++)
                                {
                                    GUI_data_nav.New_screen_num[i] = GUI_data_nav.Screen_before_pw_org_num->Screen_org_no[i];
                                }
																password_struct.password_to_be_entered = __FALSE;
																GUI_data_nav.Change = 1;
                                GUI_data_nav.Child_present = 1;
														}
														//if angle sensor is installed then use the angle from sensor else use the manual angle entery

	
                            break;

                        case GUI_KEY_BACK:
                            go_back_one_level();
                            break;
                    }
                 break;

                case ID_MULTI_TEXT_1:
                    break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen3(SCREEN_TYPE3_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen3(SCREEN_TYPE3_DATA *data, int count)
{
    int i, selection = 0;
    char * double_ptr;
    char char_data1[ARRAY_SIZE];

    GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX_SIZE_X, LISTBOX_SIZE_Y,
                                             WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
    //populate the file names within the listbox
    if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_UNIT_SCREEN) == 0)
    {
        double_ptr = Strings[*data[1].Variable1].Text;
        if(Scale_setup_var.Distance_unit == Screen24_str5)//If Metric = distance_unit
        {
            LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[4].Main_text_index].Text);
            LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[5].Main_text_index].Text);
            if (strcmp(double_ptr, Strings[data[4].Main_text_index].Text) == 0)
            {
               selection = 0;
            }
            else
            {
               selection = 1;
            }
        }
        else//If English = distance_unit
        {
            LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[1].Main_text_index].Text);
            LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[2].Main_text_index].Text);
            LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[3].Main_text_index].Text);
            if (strcmp(double_ptr, Strings[data[1].Main_text_index].Text) == 0)
            {
               selection = 0;
            }
            else if (strcmp(double_ptr, Strings[data[2].Main_text_index].Text) == 0)
            {
               selection = 1;
            }
            else
            {
               selection = 2;
            }
        }
    }
    else
    {
        switch(data[1].data_type)
        {
            case unsigned_char_type:
              //since the structure stores the address of a character pointer, use a double char pointer
              //to access the variable value
              double_ptr = Strings[*data[1].Variable1].Text;
              break;

            default:
              custom_sprintf(data[1].data_type, data[1].Variable1, char_data1, sizeof(char_data1));
              break;
        }
        for (i = 1; i < count; i++)
        {
            LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
            if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PWD_ERROR_ORG_NO) &&
               strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, VERIFY_PWD_ERROR_ORG_NO) &&
               strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, FIRMWARE_UPGRADE_ORG_NO))
            {
                switch(data[1].data_type)
                {
                    case unsigned_char_type:
                      if (strcmp(double_ptr, Strings[data[i].Main_text_index].Text) == 0)
                      {
                         selection = (i - 1);
                      }
                      break;

                    default:
                      if (strcmp(char_data1, Strings[data[i].Main_text_index].Text) == 0)
                      {
                         selection = (i - 1);
                      }
                      break;
                }
            }
            else
            {
                selection = 0;
            }
        }
    }

    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
    LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
    LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
    LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
    LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
    LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

    WM_SetCallback(WM_HBKWIN, callback_screen3);
    LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

    GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(MULTI_EDIT_START, MULTI_WIN_OFFSET,
                                                 MULTI_EDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                                 WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
    MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
    MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
    MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
    MULTIEDIT_SetAutoScrollV(GUI_data_nav.Multi_text,0);
    MULTIEDIT_SetAutoScrollH(GUI_data_nav.Multi_text,0);
    MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
    MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
    MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);

    MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
    if (data[0].Description_text_index != 0)
    {
      MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
      MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Description_text_index].Text);
      MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
    }
    else
    {
      MULTIEDIT_AddText(GUI_data_nav.Multi_text, " ");
    }
    if (data[1].Description_text_index != 0)
    {
      MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
      if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_UNIT_SCREEN) == 0)
      {
          if(Scale_setup_var.Distance_unit == Screen24_str5)//If Metric = distance_unit
          {
            MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[4].Description_text_index].Text);
          }
          else //distance unit = english unit
          {
            MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[1].Description_text_index].Text);
          }
      }
      else
      {
          MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[1].Description_text_index].Text);
      }
    }

    WM_SetFocus(GUI_data_nav.Listbox);
    LISTBOX_SetSel(GUI_data_nav.Listbox, selection);
    return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
