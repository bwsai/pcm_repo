/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen_global_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN_GLOBAL_DATA
#define  SCREEN_GLOBAL_DATA

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Global_ex.h"
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "../GUI/Application/Screen_strings_english.h"
//#include "Screen_strings_polish.h"
#include "Screen_structure.h"
#include "../gui/application/Screen1_data.h"
#include "../gui/application/Screen2_data.h"
#include "../gui/application/Screen3_data.h"
//#include "../gui/application/Screen4_data.h"
#include "../gui/application/Screen5_data.h"
#include "../gui/application/Screen6_data.h"
#include "../gui/application/Screen9_data.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */
#ifndef GUI_CONST_STORAGE
#define GUI_CONST_STORAGE const
#endif

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */
const STRING_DATA * Strings;
/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */
int PID_Old_Local_Setpoint = -1;
/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

__align(4) SCALE_SETUP_STRUCT   Scale_setup_var __attribute__((section ("GUI_DATA_RAM"), zero_init));
__align(4) CALIBRATION_STRUCT   Calibration_var __attribute__((section ("GUI_DATA_RAM"), zero_init));
__align(4) SETUP_DEVICES_STRUCT Setup_device_var __attribute__((section ("GUI_DATA_RAM"), zero_init));
__align(4) RPRTS_DIAG_STRUCT    Rprts_diag_var __attribute__((section ("GUI_DATA_RAM"), zero_init));
__align(4) ADMIN_STRUCT         Admin_var __attribute__ ((section ("GUI_DATA_RAM"), zero_init));
__align(4) UNITS_STRUCT         Units_var __attribute__ ((section ("GUI_DATA_RAM"), zero_init));
__align(4) UNITS_STRUCT         Units_var;
__align(4) TOTALS_STRUCT    		Totals_var __attribute__((section ("GUI_DATA_RAM"), zero_init));
__align(4) MISC_STRUCT         Misc_var __attribute__ ((section ("GUI_DATA_RAM"), zero_init));
__align(4) PCM_STRUCT         Pcm_var __attribute__ ((section ("PCM_GUI_DATA_RAM"), zero_init));



PASSWORD_STRUCT_DEF password_struct;
FILE_UPDATE_STRUCT Filename;

GUI_DATA_NAV_STRUCT GUI_data_nav;
PARENT_WIZARD parent_wizard;

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
/* Bitmap data for arrow keys */
GUI_CONST_STORAGE GUI_COLOR _aColorsArrow[] =
{
    0x000000, 0x000000
};

GUI_CONST_STORAGE GUI_LOGPALETTE _PalArrow =
{
    2,  /* number of entries */
    1,  /* No transparency */
    &_aColorsArrow[0]
};

GUI_CONST_STORAGE GUI_LOGPALETTE _PalArrow1 =
{
    2,  /* number of entries */
    1,  /* No transparency */
    &_aColorsArrow[0]
};

GUI_CONST_STORAGE GUI_COLOR _aColorsArrow2[] =
{
    0xFFFFFF, 0xFFFFFF
};

GUI_CONST_STORAGE GUI_LOGPALETTE _PalArrow2 =
{
    2,  /* number of entries */
    1,  /* No transparency */
    &_aColorsArrow2[0]
};

GUI_CONST_STORAGE unsigned char _acArrowEnter[] =
{
    ________, ___X____,
    ________, ___X____,
    ____XX__, ___X____,
    __XXXX__, ___X____,
    XXXXXXXX, XXXX____,
    __XXXX__, ________,
    ____XX__, ________
};

GUI_CONST_STORAGE unsigned char _acArrowEnter1[] =
{
    ________, __XXX___,
	  ________, __XXX___,
    ________, __XXX___,
	  ______XX, __XXX___,
    _____XX_, __XXX___,
    ___XXXX_, __XXX___,
    __XXXX__, __XXX___,
    XXXXXXXX, XXXXX___,
	  XXXXXXXX, XXXXX___,
    __XXXX__, ________,
    ___XXXX_, ________,
    _____XX_, ________,
	  ______XX, ________
};

GUI_CONST_STORAGE unsigned char _acArrowRight1[] =
{
    ____XXXX, XX______,
    _____XXX, XXX_____,
    ______XX, XXXX____,
    _______X, XXXXX___,
    ________, XXXXXX__,
    ________, _XXXXXX_,
    ________, __XXXXXX,
    ________, _XXXXXX_,
    ________, XXXXXX__,
    _______X, XXXXX___,
    ______XX, XXXX____,
    _____XXX, XXX_____,
    ____XXXX, XX______
};

GUI_CONST_STORAGE GUI_BITMAP _bmArrowRight1 =
{
    16,           /* XSize */
    13,            /* YSize */
    2,            /* BytesPerLine */
    1,            /* BitsPerPixel */
    _acArrowRight1, /* Pointer to picture data (indices) */
    &_PalArrow1     /* Pointer to palette */
};

GUI_CONST_STORAGE GUI_BITMAP _bmArrowRight2 =
{
    16,           /* XSize */
    13,            /* YSize */
    2,            /* BytesPerLine */
    1,            /* BitsPerPixel */
    _acArrowRight1, /* Pointer to picture data (indices) */
    &_PalArrow2     /* Pointer to palette */
};

GUI_CONST_STORAGE GUI_BITMAP _bmArrowEnter =
{
    12, /* XSize */
    7, /* YSize */
    2, /* BytesPerLine */
    1, /* BitsPerPixel */
    _acArrowEnter, /* Pointer to picture data (indices) */
    &_PalArrow /* Pointer to palette */
};

GUI_CONST_STORAGE GUI_BITMAP _bmArrowEnter1 =
{
    16, /* XSize */
    13, /* YSize */
    2, /* BytesPerLine */
    1, /* BitsPerPixel */
    _acArrowEnter1, /* Pointer to picture data (indices) */
    &_PalArrow1 /* Pointer to palette */
};

GUI_CONST_STORAGE GUI_BITMAP _bmArrowEnter2 =
{
    16, /* XSize */
    13, /* YSize */
    2, /* BytesPerLine */
    1, /* BitsPerPixel */
    _acArrowEnter1, /* Pointer to picture data (indices) */
    &_PalArrow2 /* Pointer to palette */
};


//------------------------------------------------------------------------------------------------
NEXT_ORG_NUM Org_110000_next_num[] =
{
    "210000",
    "220000",
    "230000",
    "240000",
    "250000",
	  "2A0000",
    "260000",
    "270000",
  	"272000",
    "280000",
    "291000",
    /*"292000",*/
	  "2B0000",
    "110000",
    "340000",
};

SETUP_WIZARD_SCREEN Org_110000_screen = { "000000", "000000", "340000", Org_110000_next_num, GUI_COUNTOF(Org_110000_next_num)};


NEXT_ORG_NUM Org_120000_next_num[] =
{
    //"351000",
		"350000",
};

SETUP_WIZARD_SCREEN Org_120000_screen = { "000000", "000000", "130000", Org_120000_next_num, GUI_COUNTOF(Org_120000_next_num)};

NEXT_ORG_NUM Org_130000_next_num[] =
{
    "131000",
    "320000",
    "310000",
    "330000",
};

SETUP_WIZARD_SCREEN Org_130000_screen = { "000000", "000000", "140000", Org_130000_next_num, GUI_COUNTOF(Org_130000_next_num)};
// subwizard testing
NEXT_ORG_NUM Org_230000_next_num[] =
{
    "231100",
	  "231210",
	  "231300",
};

SETUP_WIZARD_SCREEN Org_230000_screen = { "230000", "240000", "240000", Org_230000_next_num, GUI_COUNTOF(Org_230000_next_num)};
// subwizard testing ends
NEXT_ORG_NUM Org_270000_next_num[] =
{
    //"271000",
    "272000",
		//"271200",
};
SETUP_WIZARD_SCREEN Org_270000_screen= { "000000", "000000", "280000", Org_270000_next_num, GUI_COUNTOF(Org_270000_next_num)};




NEXT_ORG_NUM Org_310000_next_num[] =
{
  	"311000",
   	"312000",
    "313000",
    "314000",
    "315000",
   // "318000",
};

SETUP_WIZARD_SCREEN Org_310000_screen = { "300000", "700000", "140000", Org_310000_next_num, GUI_COUNTOF(Org_310000_next_num)};

NEXT_ORG_NUM Org_320000_next_num[] =
{
    "321000",
//     "322000",
//     "323000",
	  "323000",
	  "322000",
    "324000",
    "325000",
    "326000",
  //  "327000",
};

SETUP_WIZARD_SCREEN Org_320000_screen = { "300000", "700000", "140000", Org_320000_next_num, GUI_COUNTOF(Org_320000_next_num)};

// NEXT_ORG_NUM Org_330000_next_num[] =
// {
//     "331000",
//     "332000",
//     "333000",
//     //"334000",
// };

// SETUP_WIZARD_SCREEN Org_330000_screen = { "300000", "700000", "140000", Org_330000_next_num, GUI_COUNTOF(Org_330000_next_num)};

NEXT_ORG_NUM Org_341000_next_num[] =
{
    "341100",
    "341200",
    "341300",
    "341400",
    "341500",
   // "341600",
};

SETUP_WIZARD_SCREEN Org_341000_screen = { "300000", "700000", "120000", Org_341000_next_num, GUI_COUNTOF(Org_341000_next_num)};

NEXT_ORG_NUM Org_342000_next_num[] =
{
    "342100",
    "342200",
    "342300",
    "342400",
   // "342500",
};

SETUP_WIZARD_SCREEN Org_342000_screen = { "300000", "700000", "120000", Org_342000_next_num, GUI_COUNTOF(Org_342000_next_num)};

NEXT_ORG_NUM Org_343000_next_num[] =
{
    "343100",
};

SETUP_WIZARD_SCREEN Org_343000_screen = { "300000", "300000", "120000", Org_343000_next_num, GUI_COUNTOF(Org_343000_next_num)};


NEXT_ORG_NUM Org_351000_next_num[] =
{
		"351100",
    "351200",
    "351300",
};


SETUP_WIZARD_SCREEN Org_351000_screen = { "300000", "700000", "130000", Org_351000_next_num, GUI_COUNTOF(Org_351000_next_num)};
//SETUP_WIZARD_SCREEN Org_351100_screen = { "300000", "700000", "140000", Org_351100_next_num, GUI_COUNTOF(Org_351100_next_num)};

NEXT_ORG_NUM Org_352000_next_num[] =
{
    "352000",
};

SETUP_WIZARD_SCREEN Org_352000_screen = { "300000", "700000", "130000", Org_352000_next_num, GUI_COUNTOF(Org_352000_next_num)};

NEXT_ORG_NUM Org_619100_next_num[] =
{
    "619110",
    "619120",
    "619130",
  //  "619140",
};

SETUP_WIZARD_SCREEN Org_619100_screen = { "600000", "700000", "600000", Org_619100_next_num, GUI_COUNTOF(Org_619100_next_num)};

NEXT_ORG_NUM Org_619200_next_num[] =
{
    "619210",
    "619220",
    "619230",
    "619240",
  //  "619250",
};

SETUP_WIZARD_SCREEN Org_619200_screen = { "600000", "700000", "600000", Org_619200_next_num, GUI_COUNTOF(Org_619200_next_num)};

NEXT_ORG_NUM Org_637000_next_num[] =
{
    "637100",
    "637200",
    "637300",
};

SETUP_WIZARD_SCREEN Org_637000_screen = { "600000", "600000", "600000", Org_637000_next_num, GUI_COUNTOF(Org_637000_next_num)};

NEXT_ORG_NUM Org_330000_next_num[] =
{
    //"641000",
    "331000",
    "332000",
    "333000",
    "334000",
    "335000",
    //"641600",
};

SETUP_WIZARD_SCREEN Org_330000_screen = { "300000", "300000", "300000", Org_330000_next_num, GUI_COUNTOF(Org_330000_next_num)};

//-------------------------scale setup screen structures---------------------------------------------
// Screen_org_data Org_231000_children[] = {
//     {Screen2312_str1,  3, "231210", (void *)Org_231210_data, GUI_COUNTOF(Org_231210_data), NULL, 5, 0 , NULL},
// };

// Screen_org_data Org_231000_sibling[] = {
//     {Screen231_str0, 5, "231100", (void *)Org_231100_data, GUI_COUNTOF(Org_231100_data), NULL,                4, 0,                                NULL},
//     {Screen231_str0, 9, "231200", (void *)Org_231200_data, GUI_COUNTOF(Org_231200_data), Org_231000_children, 4, GUI_COUNTOF(Org_231000_children), NULL},
//     {Screen231_str0, 5, "231300", (void *)Org_231300_data, GUI_COUNTOF(Org_231300_data), NULL,                4, 0,                                NULL},
// };

// Screen_org_data Org_230000_children[] = {
//     {Screen231_str0, 3, "231000", (void *)Org_231000_data, GUI_COUNTOF(Org_231000_data), Org_231000_sibling, 3, GUI_COUNTOF(Org_231000_sibling), NULL},
// };

// Screen_org_data Org_272000_children[] = {
//     {Screen2711_str0, 9, "272100", (void *)Org_272100_data, GUI_COUNTOF(Org_272100_data), NULL, 3, 0, NULL},
// };

// Screen_org_data Org_270000_children[] = {
//   {Screen2_str13, 3, "271000", (void *)Org_271000_data, GUI_COUNTOF(Org_271000_data), NULL, 3, 0, NULL},  
// 	{Screen2_str13, 5, "272000", (void *)Org_272000_data, GUI_COUNTOF(Org_272000_data), Org_272000_children, 3, GUI_COUNTOF(Org_272000_children), NULL},
// };

// Screen_org_data Org_280000_children[] = {
//     {Screen2_str15, 5, "281000", (void *)Org_281000_data, GUI_COUNTOF(Org_281000_data), NULL, 3, 0, NULL},
//     {Screen2_str15, 5, "282000", (void *)Org_282000_data, GUI_COUNTOF(Org_282000_data), NULL, 3, 0, NULL},
//     {Screen2_str15, 5, "283000", (void *)Org_283000_data, GUI_COUNTOF(Org_283000_data), NULL, 3, 0, NULL},
//     {Screen2_str15, 5, "284000", (void *)Org_284000_data, GUI_COUNTOF(Org_284000_data), NULL, 3, 0, NULL},
//     {Screen2_str15, 5, "285000", (void *)Org_285000_data, GUI_COUNTOF(Org_285000_data), NULL, 3, 0, NULL},
// };

// Screen_org_data Org_293100_children[] = {
//     {Screen29_str7, 5, "293100", (void *)Org_293100_data, GUI_COUNTOF(Org_293100_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_290000_children[] = {
//     {Screen29_str1, 5, "291000", (void *)Org_291000_data, GUI_COUNTOF(Org_291000_data), NULL,                3, 0,                                NULL},
//     {Screen29_str4, 5, "292000", (void *)Org_292000_data, GUI_COUNTOF(Org_292000_data), NULL,                3, 0,                                 NULL},
//     {Screen29_str7, 3, "293000", (void *)Org_293000_data, GUI_COUNTOF(Org_293000_data), Org_293100_children, 3, GUI_COUNTOF(Org_293100_children), NULL},
// };
Screen_org_data Org_210000_children[] = {
    {Screen21_str1,5, "211000",  (void *)Org_211000_data,GUI_COUNTOF(Org_211000_data),NULL,3, 0,NULL},
    {Screen21_str3,3, "212000",  (void *)Org_212000_data,GUI_COUNTOF(Org_212000_data),NULL,3, 0,NULL},
    {Screen21_str5,3, "213000",  (void *)Org_213000_data,GUI_COUNTOF(Org_213000_data),NULL,3, 0,NULL},
	
};
Screen_org_data Org_220000_children[] = {
    {Screen22_str1,3, "221000",  (void *)Org_221000_data,GUI_COUNTOF(Org_221000_data),NULL,3, 0,NULL},	
    {Screen22_str3,5, "222000",  (void *)Org_222000_data,GUI_COUNTOF(Org_222000_data),NULL,3, 0,NULL},	
    {Screen22_str5,5, "223000",  (void *)Org_223000_data,GUI_COUNTOF(Org_223000_data),NULL,3, 0,NULL},
    {Screen22_str7,5, "224000",  (void *)Org_224000_data,GUI_COUNTOF(Org_224000_data),NULL,3, 0,NULL},
    {Screen66_str9,6, "225000",  (void *)Org_225000_data,GUI_COUNTOF(Org_225000_data),NULL,3, 0,NULL},	//for mac id editing

    {Screen22_str9,5, "226000",  (void *)Org_226000_data,GUI_COUNTOF(Org_226000_data),NULL,3, 0,NULL},
    {Screen22_str11,5, "227000",  (void *)Org_227000_data,GUI_COUNTOF(Org_227000_data),NULL,3, 0,NULL},
};
Screen_org_data Org_230000_children[] = {
    {Screen23_str1,5, "231000",  (void *)Org_231000_data,GUI_COUNTOF(Org_231000_data),NULL,3, 0,NULL},	
//    {Screen23_str3,6, "232000",  (void *)Org_232000_data,GUI_COUNTOF(Org_232000_data),NULL,3, 0,NULL},	
	
};

Screen_org_data Org_240000_children[] = {
    {Screen24_str1,3, "241000",  (void *)Org_241000_data,GUI_COUNTOF(Org_241000_data),NULL,3, 0,NULL},	
    {Screen24_str3,6, "242000",  (void *)Org_242000_data,GUI_COUNTOF(Org_242000_data),NULL,3, 0,NULL},	
    {Screen24_str5,5, "243000",  (void *)Org_243000_data,GUI_COUNTOF(Org_243000_data),NULL,3, 0,NULL},	
    {Screen24_str7,6, "244000",  (void *)Org_244000_data,GUI_COUNTOF(Org_244000_data),NULL,3, 0,NULL},	
	
};
Screen_org_data Org_250000_children[] = {
    {Screen25_str1,3, "251000",  (void *)Org_251000_data,GUI_COUNTOF(Org_251000_data),NULL,3, 0,NULL},	
    {Screen25_str3,5, "252000",  (void *)Org_252000_data,GUI_COUNTOF(Org_252000_data),NULL,3, 0,NULL},	
	
//    {Screen25_str5,6, "253000",  (void *)Org_253000_data,GUI_COUNTOF(Org_253000_data),NULL,3, 0,NULL},
};

Screen_org_data Org_310000_children[] = {
    {Screen31_str1,3, "311000",  (void *)Org_311000_data,GUI_COUNTOF(Org_311000_data),NULL,3, 0,NULL},	
    {Screen31_str9,5, "312000",  (void *)Org_312000_data,GUI_COUNTOF(Org_312000_data),NULL,3, 0,NULL},	
	
};

Screen_org_data Org_320000_children[] = {
    {Screen32_str1,5, "321000",  (void *)Org_321000_data,GUI_COUNTOF(Org_321000_data),NULL,3, 0,NULL},	
	
};

Screen_org_data Org_332100_children[] = {
    {Screen641311_str0,  3, "332110",  (void *)Org_332110_data,  GUI_COUNTOF(Org_332110_data), NULL, 5, 0, NULL},
    {Screen641312_str0,  3, "332120",  (void *)Org_332120_data,  GUI_COUNTOF(Org_332120_data), NULL, 5, 0, NULL},
};

Screen_org_data Org_332000_children[] = {
    {Screen3_str4,  2, "332100",  (void *)Org_332100_data,  GUI_COUNTOF(Org_332100_data), Org_332100_children, 4, GUI_COUNTOF(Org_332100_children), NULL},
};
Screen_org_data Org_340000_children[] = {
    {Screen34_str0,  3, "340000",  (void *)Org_340000_data,  GUI_COUNTOF(Org_340000_data), NULL, 5, 0, NULL},
};

Screen_org_data Org_330000_sibling[] = {
	//commented below code for elemination of First screen of Update Firmware Wizard screen 640000 on 25/09/2013
  //  {Screen6_str4,  9,  "641100",  (void *)Org_641100_data,  GUI_COUNTOF(Org_641100_data), NULL,                4, 0,                                NULL},
    {Screen3_str4,  3,  "331000",  (void *)Org_331000_data,  GUI_COUNTOF(Org_331000_data), NULL,                3, 0,                                NULL},
    {Screen3_str4,  3,  "332000",  (void *)Org_332000_data,  GUI_COUNTOF(Org_332000_data), Org_332000_children, 3, GUI_COUNTOF(Org_332000_children), NULL},
    {Screen3_str4,  9,  "333000",  (void *)Org_333000_data,  GUI_COUNTOF(Org_333000_data), NULL,                3, 0,                                NULL},
    {Screen3_str4,  3,  "334000",  (void *)Org_334000_data,  GUI_COUNTOF(Org_334000_data), NULL,                3, 0,                                NULL},
    {Screen3_str4,  9,  "335000",  (void *)Org_335000_data,  GUI_COUNTOF(Org_335000_data), NULL,                3, 0,                                NULL},
};

Screen_org_data Org_300000_children[] = {
    {Screen3_str0,2, "310000",  (void *)Org_310000_data,GUI_COUNTOF(Org_310000_data),(void *)Org_310000_children,3, GUI_COUNTOF(Org_310000_children),NULL},
    {Screen3_str2,2, "320000",  (void *)Org_320000_data,GUI_COUNTOF(Org_320000_data),(void *)Org_320000_children,3, GUI_COUNTOF(Org_320000_children),NULL},
    {Screen3_str4,  9,  "330000",  (void *)Org_330000_data, GUI_COUNTOF(Org_330000_data),  Org_330000_sibling,  3, GUI_COUNTOF(Org_330000_sibling), &Org_330000_screen},
    {Screen3_str0,3, "340000",  (void *)Org_340000_data,GUI_COUNTOF(Org_340000_data),(void *)Org_340000_children,3, GUI_COUNTOF(Org_340000_children),NULL},

};

Screen_org_data Org_200000_children[] = {
    {Screen2_str1,2, "210000",  (void *)Org_210000_data,GUI_COUNTOF(Org_210000_data),(void *)Org_210000_children,3, GUI_COUNTOF(Org_210000_children),NULL},
    {Screen2_str2,2, "220000",  (void *)Org_220000_data,GUI_COUNTOF(Org_220000_data),(void *)Org_220000_children,3,  GUI_COUNTOF(Org_220000_children),NULL},
    {Screen2_str3,2, "230000",  (void *)Org_230000_data,GUI_COUNTOF(Org_230000_data),(void *)Org_230000_children,3, GUI_COUNTOF(Org_230000_children),NULL},
    {Screen2_str4,2, "240000",  (void *)Org_240000_data,GUI_COUNTOF(Org_240000_data),(void *)Org_240000_children,3, GUI_COUNTOF(Org_240000_children),NULL},
    {Screen2_str5,2, "250000",  (void *)Org_250000_data,GUI_COUNTOF(Org_250000_data),(void *)Org_250000_children,3, GUI_COUNTOF(Org_250000_children),NULL},    
};

 Screen_org_data Org_800000_parent[] = {
    {Screen0_str7,  15, "800000",  NULL, NULL,  NULL,                0, 0,                                NULL},

};
//--------------------------------main screen structure-------------------------------------
Screen_org_data Org_000000_children[] = {
    {Screen0_str1, 2, "100000",  NULL,  NULL,  NULL,  NULL, NULL,  NULL},
    {Screen0_str2, 1, "200000",  (void *)Org_200000_data,  GUI_COUNTOF(Org_200000_data),  Org_200000_children, 1, GUI_COUNTOF(Org_200000_children), NULL},
    {Screen0_str3, 2, "300000",   (void *)Org_300000_data,  GUI_COUNTOF(Org_300000_data),  Org_300000_children,  1, GUI_COUNTOF(Org_300000_children),  NULL},
};


Screen_org_data Org_000000_parent[] = {
    {Screen0_str0, 1, "000000",  (void *)Org_000000_data,  GUI_COUNTOF(Org_000000_data),  Org_000000_children, 0, GUI_COUNTOF(Org_000000_children), NULL},
};


Screen_org_data Org_322000_parent[] = {
    {Screen0_str0, 1, "000000",  (void *)Org_000000_data,  GUI_COUNTOF(Org_000000_data),  Org_000000_children, 0, GUI_COUNTOF(Org_000000_children), NULL},
};
// //-------------------------------- calibration screen structure-------------------------------------
// Screen_org_data Org_310000_sibling[] = {
//     {Screen311_str0, 9,  "311000", (void *)Org_311000_data, GUI_COUNTOF(Org_311000_data), NULL, 3, 0, NULL},
//     {Screen311_str0, 3,  "312000", (void *)Org_312000_data, GUI_COUNTOF(Org_312000_data), NULL, 3, 0, NULL},
//     {Screen311_str0, 11, "313000", (void *)Org_313000_data, GUI_COUNTOF(Org_313000_data), NULL, 3, 0, NULL},
//     {Screen311_str0, 10, "314000", (void *)Org_314000_data, GUI_COUNTOF(Org_314000_data), NULL, 3, 0, NULL},
//     {Screen311_str0, 3,  "315000", (void *)Org_315000_data, GUI_COUNTOF(Org_315000_data), NULL, 3, 0, NULL},
// };

// Screen_org_data Org_321000_children[] = {
//     {Screen3221_str0, 3, "321100", (void *)Org_321100_data, GUI_COUNTOF(Org_321100_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_323000_children[] = {
//     {Screen3241_str0, 3, "323100", (void *)Org_323100_data, GUI_COUNTOF(Org_323100_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_320000_sibling[] = {
//     {Screen3_str2, 9,  "321000", (void *)Org_321000_data, GUI_COUNTOF(Org_321000_data), Org_321000_children, 3, GUI_COUNTOF(Org_321000_children), NULL},
//     {Screen3_str2, 5,  "322000", (void *)Org_322000_data, GUI_COUNTOF(Org_322000_data), NULL,                3, 0,                                NULL},
//     {Screen3_str2, 9,  "323000", (void *)Org_323000_data, GUI_COUNTOF(Org_323000_data), Org_323000_children, 3, GUI_COUNTOF(Org_323000_children), NULL},
//     {Screen3_str2, 11, "324000", (void *)Org_324000_data, GUI_COUNTOF(Org_324000_data), NULL,                3, 0,                                NULL},
//     {Screen3_str2, 10, "325000", (void *)Org_325000_data, GUI_COUNTOF(Org_325000_data), NULL,                3, 0,                                NULL},
//     {Screen3_str2, 3,  "326000", (void *)Org_326000_data, GUI_COUNTOF(Org_326000_data), NULL,                3, 0,                                NULL},
// };

// Screen_org_data Org_330000_sibling[] = {
//     {Screen3_str3, 11, "331000", (void *)Org_331000_data, GUI_COUNTOF(Org_331000_data), NULL, 3, 0, NULL},
//     {Screen3_str3, 10, "332000", (void *)Org_332000_data, GUI_COUNTOF(Org_332000_data), NULL, 3, 0, NULL},
//     {Screen3_str3, 3,  "333000", (void *)Org_333000_data, GUI_COUNTOF(Org_333000_data), NULL, 3, 0, NULL},
// };

// Screen_org_data Org_341000_sibling[] = {
//     {Screen341_str0, 9,  "341100", (void *)Org_341100_data, GUI_COUNTOF(Org_341100_data), NULL, 4, 0, NULL},
//     {Screen341_str0, 9,  "341200", (void *)Org_341200_data, GUI_COUNTOF(Org_341200_data), NULL, 4, 0, NULL},
//     {Screen341_str0, 10, "341300", (void *)Org_341300_data, GUI_COUNTOF(Org_341300_data), NULL, 4, 0, NULL},
//     {Screen341_str0, 10, "341400", (void *)Org_341400_data, GUI_COUNTOF(Org_341400_data), NULL, 4, 0, NULL},
//     {Screen341_str0, 10, "341500", (void *)Org_341500_data, GUI_COUNTOF(Org_341500_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_342000_sibling[] = {
//     {Screen342_str0, 9,  "342100", (void *)Org_342100_data, GUI_COUNTOF(Org_342100_data), NULL, 4, 0, NULL},
//     {Screen342_str0, 9,  "342200", (void *)Org_342200_data, GUI_COUNTOF(Org_342200_data), NULL, 4, 0, NULL},
//     {Screen342_str0, 10, "342300", (void *)Org_342300_data, GUI_COUNTOF(Org_342300_data), NULL, 4, 0, NULL},
//     {Screen342_str0, 10, "342400", (void *)Org_342400_data, GUI_COUNTOF(Org_342400_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_343000_sibling[] = {
//     {Screen343_str0, 5,  "343100", (void *)Org_343100_data, GUI_COUNTOF(Org_343100_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_340000_children[] = {
// 	  {Screen341_str0,   9, "341000", (void *)Org_341000_data, GUI_COUNTOF(Org_341000_data), Org_341000_sibling, 3, GUI_COUNTOF(Org_341000_sibling), &Org_341000_screen},
//     {Screen342_str0,   9, "342000", (void *)Org_342000_data, GUI_COUNTOF(Org_342000_data), Org_342000_sibling, 3, GUI_COUNTOF(Org_342000_sibling), &Org_342000_screen},
//     {Screen343_str0,   4, "343000", (void *)Org_343000_data, GUI_COUNTOF(Org_343000_data), Org_343000_sibling, 3, GUI_COUNTOF(Org_343000_sibling), &Org_343000_screen},
// };

// Screen_org_data Org_351000_sibling[] = {
//     {Screen351_str0, 11,  "351100", (void *)Org_351100_data, GUI_COUNTOF(Org_351100_data), NULL, 4, 0, NULL},
//     {Screen351_str0, 10,  "351200", (void *)Org_351200_data, GUI_COUNTOF(Org_351200_data), NULL, 4, 0, NULL},
//     {Screen351_str0, 10,  "351300", (void *)Org_351300_data, GUI_COUNTOF(Org_351300_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_352000_sibling[] = {
//     {Screen352_str0, 9,  "352100", (void *)Org_352100_data, GUI_COUNTOF(Org_352100_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_350000_children[] = {
//     {Screen351_str0, 10, "351000", (void *)Org_351000_data, GUI_COUNTOF(Org_351000_data), Org_351000_sibling, 3, GUI_COUNTOF(Org_351000_sibling), &Org_351000_screen},
//     { Screen352_str0, 11, "352000", (void *)Org_352000_data, GUI_COUNTOF(Org_352000_data), Org_352000_sibling, 3, GUI_COUNTOF(Org_352000_sibling), &Org_352000_screen},
// };

// Screen_org_data Org_300000_children[] = {
// 	  {Screen311_str0, 		5, "310000",  (void *)Org_310000_data,  GUI_COUNTOF(Org_310000_data), Org_310000_sibling,  2, GUI_COUNTOF(Org_310000_sibling), &Org_310000_screen},
//     {Screen3_str2, 			5, "320000",  (void *)Org_320000_data, GUI_COUNTOF(Org_320000_data) , Org_320000_sibling,  2, GUI_COUNTOF(Org_320000_sibling),  &Org_320000_screen},
//     {Screen3_str3, 			3, "330000",  (void *)Org_330000_data, GUI_COUNTOF(Org_330000_data),  Org_330000_sibling,  2, GUI_COUNTOF(Org_330000_sibling),  &Org_330000_screen},
//     {Screen3_str4,      3, "340000",  (void *)Org_340000_data,  GUI_COUNTOF(Org_340000_data), Org_340000_children, 2, GUI_COUNTOF(Org_340000_children), NULL},
//     {Screen3_str5,      3, "350000",  (void *)Org_350000_data,  GUI_COUNTOF(Org_350000_data), Org_350000_children, 2, GUI_COUNTOF(Org_350000_children), NULL},
// };

// //--------------------------------setup devices screen structure-------------------------------------
// /*********************************** 450000 ********************************************************/
// Screen_org_data Org_451300_children[] = {
//     {Screen45131_str0,  5, "451310",  (void *)Org_451310_data,  GUI_COUNTOF(Org_451310_data), NULL, 5, 0, NULL},
//     {Screen45133_str1,  5, "451320",  (void *)Org_451320_data,  GUI_COUNTOF(Org_451320_data), NULL, 5, 0, NULL},
// };

// Screen_org_data Org_451000_children[] = {
//     {Screen4511_str0, 5, "451100",  (void *)Org_451100_data,  GUI_COUNTOF(Org_451100_data), NULL,                4, 0,                                NULL},
//     {Screen4512_str0, 3, "451200",  (void *)Org_451200_data,  GUI_COUNTOF(Org_451200_data), NULL,                4, 0,                                NULL},
//     {Screen4513_str0, 3, "451300",  (void *)Org_451300_data,  GUI_COUNTOF(Org_451300_data), Org_451300_children, 4, GUI_COUNTOF(Org_451300_children), NULL},
//     {Screen4514_str0, 5, "451400",  (void *)Org_451400_data,  GUI_COUNTOF(Org_451400_data), NULL,                4, 0,                                NULL},
//     };

// Screen_org_data Org_452000_children[] = {
//     {Screen452_str1,  5, "452100",  (void *)Org_452100_data,  GUI_COUNTOF(Org_452100_data), NULL, 4, 0, NULL},
//     {Screen452_str3,  5, "452200",  (void *)Org_452200_data,  GUI_COUNTOF(Org_452200_data), NULL, 4, 0, NULL},
//     {Screen452_str4,  5, "452300",  (void *)Org_452300_data,  GUI_COUNTOF(Org_452300_data), NULL, 4, 0, NULL},
//     {Screen452_str5,  5, "452400",  (void *)Org_452400_data,  GUI_COUNTOF(Org_452400_data), NULL, 4, 0, NULL},
//     {Screen452_str6,  5, "452500",  (void *)Org_452500_data,  GUI_COUNTOF(Org_452500_data), NULL, 4, 0, NULL},
//     {Screen452_str7,  5, "452600",  (void *)Org_452600_data,  GUI_COUNTOF(Org_452600_data), NULL, 4, 0, NULL},
//     {Screen452_str8,  5, "452700",  (void *)Org_452700_data,  GUI_COUNTOF(Org_452700_data), NULL, 4, 0, NULL},
//     {Screen452_str9,  5, "452800",  (void *)Org_452800_data,  GUI_COUNTOF(Org_452800_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_453000_children[] = {
//     {Screen453_str1,  5, "453100",  (void *)Org_453100_data,  GUI_COUNTOF(Org_453100_data), NULL,                4, 0,                                NULL},
//     {Screen453_str5,  5, "453200",  (void *)Org_453200_data,  GUI_COUNTOF(Org_453200_data), NULL,                4, 0,                                NULL},
//     {Screen453_str7,  5, "453300",  (void *)Org_453300_data,  GUI_COUNTOF(Org_453300_data), NULL,                4, 0,                                NULL},
//     {Screen453_str9,  5, "453400",  (void *)Org_453400_data,  GUI_COUNTOF(Org_453400_data), NULL,                4, 0,                                NULL},
//     {Screen453_str11, 5, "453500",  (void *)Org_453500_data,  GUI_COUNTOF(Org_453500_data), NULL,                4, 0,                                NULL},
//     {Screen45_str1,   2, "451000",  (void *)Org_451000_data,  GUI_COUNTOF(Org_451000_data), Org_451000_children, 3, GUI_COUNTOF(Org_451000_children), NULL},
// };

// Screen_org_data Org_454000_children[] = {
//     {Screen454_str1, 5, "454100",  (void *)Org_454100_data,  GUI_COUNTOF(Org_454100_data), NULL,                4, 0,                                NULL},
//     {Screen453_str9, 5, "454200",  (void *)Org_454200_data,  GUI_COUNTOF(Org_454200_data), NULL,                4, 0,                                NULL},
//     {Screen45_str1,  2, "451000",  (void *)Org_451000_data,  GUI_COUNTOF(Org_451000_data), Org_451000_children, 3, GUI_COUNTOF(Org_451000_children), NULL},
// };

// Screen_org_data Org_455000_children[] = {
//     {Screen455_str1,  5, "455100",  (void *)Org_455100_data,  GUI_COUNTOF(Org_455100_data), NULL,                4, 0,                                NULL},
//     {Screen455_str3,  5, "455200",  (void *)Org_455200_data,  GUI_COUNTOF(Org_455200_data), NULL,                4, 0,                                NULL},
//     {Screen455_str5,  5, "455300",  (void *)Org_455300_data,  GUI_COUNTOF(Org_455300_data), NULL,                4, 0,                                NULL},
//     {Screen455_str7,  5, "455400",  (void *)Org_455400_data,  GUI_COUNTOF(Org_455400_data), NULL,                4, 0,                                NULL},
// };

// Screen_org_data Org_450000_children[] = {
//     {Screen45_str1, 2, "451000", (void *)Org_451000_data, GUI_COUNTOF(Org_451000_data), Org_451000_children, 3, GUI_COUNTOF(Org_451000_children), NULL},
//     {Screen45_str2, 2, "452000", (void *)Org_452000_data, GUI_COUNTOF(Org_452000_data), Org_452000_children, 3, GUI_COUNTOF(Org_452000_children), NULL},
//     {Screen45_str3, 2, "453000", (void *)Org_453000_data, GUI_COUNTOF(Org_453000_data), Org_453000_children, 3, GUI_COUNTOF(Org_453000_children), NULL},
//     {Screen45_str4, 2, "454000", (void *)Org_454000_data, GUI_COUNTOF(Org_454000_data), Org_454000_children, 3, GUI_COUNTOF(Org_454000_children), NULL},
//     {Screen45_str5, 2, "455000", (void *)Org_455000_data, GUI_COUNTOF(Org_455000_data), Org_455000_children, 3, GUI_COUNTOF(Org_455000_children), NULL},
// };

// /*********************************** 440000 ********************************************************/
// //----------------------------------------442100--------------------------------------------
// Screen_org_data Org_442110_children[] = {
//     {Screen442111_str0,  3, "442111",  (void *)Org_442111_data,  GUI_COUNTOF(Org_442111_data), NULL, 6, 0, NULL},
//     {Screen442112_str0,  5, "442112",  (void *)Org_442112_data,  GUI_COUNTOF(Org_442112_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442140_children[] = {
//     {Screen442141_str0,  5, "442141",  (void *)Org_442141_data,  GUI_COUNTOF(Org_442141_data), NULL, 6, 0, NULL},
//     {Screen442142_str0,  3, "442142",  (void *)Org_442142_data,  GUI_COUNTOF(Org_442142_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442150_children[] = {
//     {Screen442151_str0,  5, "442151",  (void *)Org_442151_data,  GUI_COUNTOF(Org_442151_data), NULL, 6, 0, NULL},
//     {Screen442152_str0,  3, "442152",  (void *)Org_442152_data,  GUI_COUNTOF(Org_442152_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442100_children[] = {
//     {Screen44211_str0,  2, "442110",  (void *)Org_442110_data,  GUI_COUNTOF(Org_442110_data), Org_442110_children, 5, GUI_COUNTOF(Org_442110_children), NULL},
//     {Screen44211_str0,  2, "442120",  NULL,                     0,                            NULL,                5, 0,                                NULL},
//     {Screen44213_str0,  3, "442130",  (void *)Org_442130_data,  GUI_COUNTOF(Org_442130_data), NULL,                5, 0,                                NULL},
//     {Screen44214_str1,  2, "442140",  (void *)Org_442140_data,  GUI_COUNTOF(Org_442140_data), Org_442140_children, 5, GUI_COUNTOF(Org_442140_children), NULL},
//     {Screen44215_str0,  2, "442150",  (void *)Org_442150_data,  GUI_COUNTOF(Org_442150_data), Org_442150_children, 5, GUI_COUNTOF(Org_442150_children), NULL},
// };

// //----------------------------------------442200--------------------------------------------
// Screen_org_data Org_442210_children[] = {
//     {Screen442211_str0,  3, "442211",  (void *)Org_442211_data,  GUI_COUNTOF(Org_442211_data), NULL, 6, 0, NULL},
//     {Screen442212_str0,  5, "442212",  (void *)Org_442212_data,  GUI_COUNTOF(Org_442212_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442240_children[] = {
//     {Screen442241_str0, 5, "442241",  (void *)Org_442241_data,  GUI_COUNTOF(Org_442241_data), NULL, 6, 0, NULL},
//     {Screen442242_str0, 3, "442242",  (void *)Org_442242_data,  GUI_COUNTOF(Org_442242_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442250_children[] = {
//     {Screen442251_str0, 5, "442251",  (void *)Org_442251_data,  GUI_COUNTOF(Org_442251_data), NULL, 6, 0, NULL},
//     {Screen442252_str0, 3, "442252",  (void *)Org_442252_data,  GUI_COUNTOF(Org_442252_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442200_children[] = {
//     {Screen44221_str0,  2, "442210",  (void *)Org_442210_data,  GUI_COUNTOF(Org_442210_data), Org_442210_children, 5, GUI_COUNTOF(Org_442210_children), NULL},
//     {Screen44221_str0,  2, "442220",  NULL,                     0,                            NULL,                5, 0,                                NULL},
//     {Screen44223_str0,  3, "442230",  (void *)Org_442230_data,  GUI_COUNTOF(Org_442230_data), NULL,                5, 0,                                NULL},
//     {Screen44224_str0,  2, "442240",  (void *)Org_442240_data,  GUI_COUNTOF(Org_442240_data), Org_442240_children, 5, GUI_COUNTOF(Org_442240_children), NULL},
//     {Screen44225_str0,  2, "442250",  (void *)Org_442250_data,  GUI_COUNTOF(Org_442250_data), Org_442250_children, 5, GUI_COUNTOF(Org_442250_children), NULL},
// };

// //----------------------------------------442300--------------------------------------------
// Screen_org_data Org_442310_children[] = {
//     {Screen442311_str0,  3, "442311",  (void *)Org_442311_data,  GUI_COUNTOF(Org_442311_data), NULL, 6, 0, NULL},
//     {Screen442312_str0,  5, "442312",  (void *)Org_442312_data,  GUI_COUNTOF(Org_442312_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442340_children[] = {
//     {Screen442341_str0, 5, "442341",  (void *)Org_442341_data,  GUI_COUNTOF(Org_442341_data), NULL, 6, 0, NULL},
//     {Screen442342_str0, 3, "442342",  (void *)Org_442342_data,  GUI_COUNTOF(Org_442342_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442350_children[] = {
//     {Screen442351_str0, 5, "442351",  (void *)Org_442351_data,  GUI_COUNTOF(Org_442351_data), NULL, 6, 0, NULL},
//     {Screen442352_str0, 3, "442352",  (void *)Org_442352_data,  GUI_COUNTOF(Org_442352_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_442300_children[] = {
//     {Screen44231_str0,  2, "442310",  (void *)Org_442310_data,  GUI_COUNTOF(Org_442310_data), Org_442310_children, 5, GUI_COUNTOF(Org_442310_children), NULL},
//     {Screen44231_str0,  2, "442320",  NULL,                     0,                            NULL,                5, 0,                                NULL},
//     {Screen44233_str0,  3, "442330",  (void *)Org_442330_data,  GUI_COUNTOF(Org_442330_data), NULL,                5, 0,                                NULL},
//     {Screen44234_str0,  2, "442340",  (void *)Org_442340_data,  GUI_COUNTOF(Org_442340_data), Org_442340_children, 5, GUI_COUNTOF(Org_442340_children), NULL},
//     {Screen44235_str0,  2, "442350",  (void *)Org_442350_data,  GUI_COUNTOF(Org_442350_data), Org_442350_children, 5, GUI_COUNTOF(Org_442350_children), NULL},
// };

// //----------------------------------------442000--------------------------------------------
// Screen_org_data Org_442000_children[] = {
//     {Screen4421_str0, 3, "442100",  (void *)Org_442100_data,  GUI_COUNTOF(Org_442100_data), Org_442100_children, 4, GUI_COUNTOF(Org_442100_children), NULL},
//     {Screen4422_str0, 3, "442200",  (void *)Org_442200_data,  GUI_COUNTOF(Org_442200_data), Org_442200_children, 4, GUI_COUNTOF(Org_442200_children), NULL},
//     {Screen4423_str0, 3, "442300",  (void *)Org_442300_data,  GUI_COUNTOF(Org_442300_data), Org_442300_children, 4, GUI_COUNTOF(Org_442300_children), NULL},
// };


// //----------------------------------------443100--------------------------------------------
// Screen_org_data Org_443120_children[] = {
//     {Screen443121_str0, 5, "443121",  (void *)Org_443121_data,  GUI_COUNTOF(Org_443121_data), NULL, 6, 0, NULL},
//     {Screen443122_str0, 3, "443122",  (void *)Org_443122_data,  GUI_COUNTOF(Org_443122_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_443130_children[] = {
//     {Screen443131_str0, 5, "443131",  (void *)Org_443131_data,  GUI_COUNTOF(Org_443131_data), NULL, 6, 0, NULL},
//     {Screen443132_str0, 3, "443132",  (void *)Org_443132_data,  GUI_COUNTOF(Org_443132_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_443100_children[] = {
//     {Screen44311_str0,  3, "443110",  (void *)Org_443110_data,  GUI_COUNTOF(Org_443110_data), NULL,                5, 0,                                NULL},
//     {Screen44312_str0,  2, "443120",  (void *)Org_443120_data,  GUI_COUNTOF(Org_443120_data), Org_443120_children, 5, GUI_COUNTOF(Org_443120_children), NULL},
//     {Screen44313_str0,  2, "443130",  (void *)Org_443130_data,  GUI_COUNTOF(Org_443130_data), Org_443130_children, 5, GUI_COUNTOF(Org_443130_children), NULL},
// };

// //----------------------------------------443200--------------------------------------------
// Screen_org_data Org_443220_children[] = {
//     {Screen443221_str0, 5, "443221",  (void *)Org_443221_data,  GUI_COUNTOF(Org_443221_data), NULL, 6, 0, NULL},
//     {Screen443222_str0, 3, "443322",  (void *)Org_443222_data,  GUI_COUNTOF(Org_443222_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_443230_children[] = {
//     {Screen443231_str0, 5, "443231",  (void *)Org_443231_data,  GUI_COUNTOF(Org_443231_data), NULL, 6, 0, NULL},
//     {Screen443232_str0, 3, "443232",  (void *)Org_443232_data,  GUI_COUNTOF(Org_443232_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_443200_children[] = {
//     {Screen44321_str0,  3, "443210",  (void *)Org_443210_data,  GUI_COUNTOF(Org_443210_data), NULL,                5, 0,                                NULL},
//     {Screen44322_str0,  2, "443220",  (void *)Org_443220_data,  GUI_COUNTOF(Org_443220_data), Org_443220_children, 5, GUI_COUNTOF(Org_443220_children), NULL},
//     {Screen44323_str0,  2, "443230",  (void *)Org_443230_data,  GUI_COUNTOF(Org_443230_data), Org_443230_children, 5, GUI_COUNTOF(Org_443230_children), NULL},
// };

// //----------------------------------------443300--------------------------------------------
// Screen_org_data Org_443320_children[] = {
//     {Screen443321_str0, 5, "443321",  (void *)Org_443321_data,  GUI_COUNTOF(Org_443321_data), NULL, 6, 0, NULL},
//     {Screen443322_str0, 3, "443322",  (void *)Org_443322_data,  GUI_COUNTOF(Org_443322_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_443330_children[] = {
//     {Screen443331_str0,  5, "443331",  (void *)Org_443331_data,  GUI_COUNTOF(Org_443331_data), NULL, 6, 0, NULL},
//     {Screen443332_str0,  3, "443332",  (void *)Org_443332_data,  GUI_COUNTOF(Org_443332_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_443300_children[] = {
//     {Screen44331_str0,  3, "443310",  (void *)Org_443310_data,  GUI_COUNTOF(Org_443310_data), NULL,                5, 0,                                NULL},
//     {Screen44332_str0,  2, "443320",  (void *)Org_443320_data,  GUI_COUNTOF(Org_443320_data), Org_443320_children, 5, GUI_COUNTOF(Org_443320_children), NULL},
//     {Screen44333_str0,  2, "443330",  (void *)Org_443330_data,  GUI_COUNTOF(Org_443330_data), Org_443330_children, 5, GUI_COUNTOF(Org_443330_children), NULL},
// };

// //----------------------------------------443000--------------------------------------------
// Screen_org_data Org_443000_children[] = {
//     {Screen4431_str0, 3, "443100",  (void *)Org_443100_data,  GUI_COUNTOF(Org_443100_data), Org_443100_children, 4, GUI_COUNTOF(Org_443100_children), NULL},
//     {Screen4432_str0, 3, "443200",  (void *)Org_443200_data,  GUI_COUNTOF(Org_443200_data), Org_443200_children, 4, GUI_COUNTOF(Org_443200_children), NULL},
//     {Screen4433_str0, 3, "443300",  (void *)Org_443300_data,  GUI_COUNTOF(Org_443300_data), Org_443300_children, 4, GUI_COUNTOF(Org_443300_children), NULL},
// };

// //----------------------------------------444000--------------------------------------------
// /*    Screen_org_data Org_444100_children[] = {
//     {Screen44411_str0, 3, "444111",  (void *)Org_444111_data,  GUI_COUNTOF(Org_444111_data), NULL, 6, 0, NULL},
// };
// s
// Screen_org_data Org_444200_children[] = {
//     {Screen44421_str0, 3, "444211",  (void *)Org_444211_data,  GUI_COUNTOF(Org_444211_data), NULL, 6, 0, NULL},
// };*/

// Screen_org_data Org_444100_sibling[] = {
//     //{Screen41_str3, 2, "444110", (void *)Org_444110_data, GUI_COUNTOF(Org_444110_data), Org_444100_children,  5, GUI_COUNTOF(Org_444100_children), NULL},
//     {Screen44411_str0, 		3, "444110",  (void *)Org_444110_data,  GUI_COUNTOF(Org_444110_data), NULL, 5, 0, NULL},
//     {Screen44412_str1,    5, "444120",  (void *)Org_444120_data,  GUI_COUNTOF(Org_444120_data), NULL, 5, 0, NULL},
//     {Screen44413_str1,    5, "444130",  (void *)Org_444130_data,  GUI_COUNTOF(Org_444130_data), NULL, 5, 0, NULL},
//     {Screen44414_str1,    5, "444140",  (void *)Org_444140_data,  GUI_COUNTOF(Org_444140_data), NULL, 5, 0, NULL},
// };

// Screen_org_data Org_444200_sibling[] = {
//     //{Screen41_str3, 2, "444210", (void *)Org_444210_data, GUI_COUNTOF(Org_444210_data), Org_444200_children,  5, GUI_COUNTOF(Org_444200_children), NULL},
//     {Screen44421_str0, 		3, "444210", (void *)Org_444210_data,  GUI_COUNTOF(Org_444210_data), NULL, 5, 0, NULL},
//     {Screen44422_str1,    5, "444220", (void *)Org_444220_data,  GUI_COUNTOF(Org_444220_data), NULL, 5, 0, NULL},
//     {Screen44423_str1,    5, "444230", (void *)Org_444230_data,  GUI_COUNTOF(Org_444230_data), NULL, 5, 0, NULL},
//     {Screen44424_str1,    5, "444240", (void *)Org_444240_data,  GUI_COUNTOF(Org_444240_data), NULL, 5, 0, NULL},
// };

// Screen_org_data Org_444000_children[] = {
//     {Screen4441_str0, 2, "444100",  (void *)Org_444100_data,  GUI_COUNTOF(Org_444100_data), Org_444100_sibling, 4, GUI_COUNTOF(Org_444100_sibling), NULL},
//     {Screen4442_str0, 2, "444200",  (void *)Org_444200_data,  GUI_COUNTOF(Org_444200_data), Org_444200_sibling, 4, GUI_COUNTOF(Org_444200_sibling), NULL},
// };

// Screen_org_data Org_441000_children[] = {
//     {Screen4411_str0, 3, "441100",  (void *)Org_441100_data,  GUI_COUNTOF(Org_441100_data), NULL, 4, 0, NULL},
//     {Screen4412_str0, 3, "441200",  (void *)Org_441200_data,  GUI_COUNTOF(Org_441200_data), NULL, 4, 0, NULL},
//     {Screen4413_str0, 3, "441300",  (void *)Org_441300_data,  GUI_COUNTOF(Org_441300_data), NULL, 4, 0, NULL},
//     {Screen4414_str0, 3, "441400",  (void *)Org_441400_data,  GUI_COUNTOF(Org_441400_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_440000_children[] = {
//     {Screen44_str1, 2, "441000", (void *)Org_441000_data, GUI_COUNTOF(Org_441000_data), Org_441000_children, 3, GUI_COUNTOF(Org_441000_children), NULL},
//     {Screen44_str2, 2, "442000", (void *)Org_442000_data, GUI_COUNTOF(Org_442000_data), Org_442000_children, 3, GUI_COUNTOF(Org_442000_children), NULL},
//     {Screen44_str3, 2, "443000", (void *)Org_443000_data, GUI_COUNTOF(Org_443000_data), Org_443000_children, 3, GUI_COUNTOF(Org_443000_children), NULL},
//     {Screen44_str4, 3, "444000", (void *)Org_444000_data, GUI_COUNTOF(Org_444000_data), Org_444000_children, 3, GUI_COUNTOF(Org_444000_children), NULL},
// };

// /********************************************* 430000 ******************************************************/
// Screen_org_data Org_432100_children[] = {
//     {Screen432_str4, 5, "432100",  (void *)Org_432100_data,  GUI_COUNTOF(Org_432100_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_430000_children[] = {
//     {Screen431_str0, 3, "431000", (void *)Org_431000_data, GUI_COUNTOF(Org_431000_data), NULL,                 3, 0,                                 NULL},
//     {Screen432_str0, 3, "432000", (void *)Org_432000_data, GUI_COUNTOF(Org_432000_data), Org_432100_children, 3, GUI_COUNTOF(Org_432100_children), NULL},
//     {Screen433_str0, 3, "433000", (void *)Org_433000_data, GUI_COUNTOF(Org_433000_data), NULL,                 3, 0,                                NULL},
//     {Screen434_str0, 3, "434000", (void *)Org_434000_data, GUI_COUNTOF(Org_434000_data), NULL,                 3, 0,                                  NULL},
//     {Screen435_str0, 3, "435000", (void *)Org_435000_data, GUI_COUNTOF(Org_435000_data), NULL,                 3, 0,                                NULL},
// 		{Screen436_str0, 3, "436000", (void *)Org_436000_data,  GUI_COUNTOF(Org_436000_data),NULL,  3,0, NULL},
// };

// /********************************************* 420000 ******************************************************/
// Screen_org_data Org_421000_children[] = {
//     {Screen4211_str0, 5, "421100", (void *)Org_421100_data, GUI_COUNTOF(Org_421100_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_422210_children[] = {
//     {Screen422211_str0, 3, "422211", (void *)Org_422211_data, GUI_COUNTOF(Org_422211_data), NULL, 6, 0, NULL},
//     {Screen422212_str0, 3, "422212", (void *)Org_422212_data, GUI_COUNTOF(Org_422212_data), NULL, 6, 0, NULL},
//     {Screen422213_str0, 3, "422213", (void *)Org_422213_data, GUI_COUNTOF(Org_422213_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_422200_children[] = {
//     {Screen422_str0, 2, "422210", (void *)Org_422210_data, GUI_COUNTOF(Org_422210_data), Org_422210_children, 5, GUI_COUNTOF(Org_422210_children), NULL},
// };

// Screen_org_data Org_422000_sibling[] = {
//     {Screen422_str0, 3,  "422100", NULL,                    0,                            NULL,                4, 0,                                NULL},
//     {Screen422_str0, 10, "422200", (void *)Org_422200_data, GUI_COUNTOF(Org_422200_data), Org_422200_children, 4, GUI_COUNTOF(Org_422200_children), NULL},
// };

// Screen_org_data Org_420000_children[] = {
//     {Screen421_str0, 3, "421000", (void *)Org_421000_data, GUI_COUNTOF(Org_421000_data), Org_421000_children, 3, GUI_COUNTOF(Org_421000_children), NULL},
//     {Screen422_str0, 3, "422000", (void *)Org_422000_data, GUI_COUNTOF(Org_422000_data), Org_422000_sibling,  3, GUI_COUNTOF(Org_422000_sibling), NULL},
// 		{Screen4231_str1, 3, "423000", (void *)Org_423000_data, GUI_COUNTOF(Org_423000_data), NULL,  3, 0, NULL},
// };

// /********************************************* 410000 ******************************************************/
// Screen_org_data Org_412200_children[] = {
//     {Screen412211_str0, 3, "412210", (void *)Org_412210_data, GUI_COUNTOF(Org_412210_data), NULL, 6, 0, NULL},
//     {Screen412212_str0, 3, "412220", (void *)Org_412220_data, GUI_COUNTOF(Org_412220_data), NULL, 6, 0, NULL},
//     {Screen412213_str0, 3, "412230", (void *)Org_412230_data, GUI_COUNTOF(Org_412230_data), NULL, 6, 0, NULL},
//     {Screen412214_str0, 3, "412240", (void *)Org_412240_data, GUI_COUNTOF(Org_412240_data), NULL, 6, 0, NULL},
// };

// // Screen_org_data Org_412200_children[] = {
// //     {Screen41_str4, 2, "412210", (void *)Org_412210_data, GUI_COUNTOF(Org_412210_data), Org_412210_children, 5, GUI_COUNTOF(Org_412210_children), NULL},
// // };

// Screen_org_data Org_412000_sibling[] = {
//     {Screen41_str3, 3,  "412100", NULL,                    0,                            NULL,                4, 0,                                NULL},
//     {Screen41_str4, 2,  "412200", (void *)Org_412200_data, GUI_COUNTOF(Org_412200_data), Org_412200_children, 5, GUI_COUNTOF(Org_412200_children), NULL},
//     //{Screen41_str3, 10, "412200", (void *)Org_412200_data, GUI_COUNTOF(Org_412200_data), Org_412200_children, 4, GUI_COUNTOF(Org_412200_children), NULL},
// };

// Screen_org_data Org_410000_children[] = {
//     {Screen41_str1, 3, "411000", (void *)Org_411000_data, GUI_COUNTOF(Org_411000_data), NULL,                3, 0,                              NULL},
//     {Screen41_str3, 3, "412000", (void *)Org_412000_data, GUI_COUNTOF(Org_412000_data), Org_412000_sibling,  3, GUI_COUNTOF(Org_412000_sibling),NULL},
//     {Screen41_str5, 6, "413000", (void *)Org_413000_data, GUI_COUNTOF(Org_413000_data), NULL,                3, 0,                              NULL},
//     {Screen41_str7, 3, "414000", (void *)Org_414000_data, GUI_COUNTOF(Org_414000_data), NULL,  							 3,	0,															NULL},
// };

// /********************************************* 400000 ******************************************************/
// Screen_org_data Org_400000_children[] = {
//     {Screen4_str1,  2, "410000",  (void *)Org_410000_data,  GUI_COUNTOF(Org_410000_data),  Org_410000_children, 2, GUI_COUNTOF(Org_410000_children), NULL},
//     {Screen4_str2,  2, "420000",  (void *)Org_420000_data,  GUI_COUNTOF(Org_420000_data),  Org_420000_children, 2, GUI_COUNTOF(Org_420000_children), NULL},
//     {Screen4_str3,  2, "430000",  (void *)Org_430000_data,  GUI_COUNTOF(Org_430000_data),  Org_430000_children, 2, GUI_COUNTOF(Org_430000_children), NULL},
//     {Screen4_str4,  1, "440000",  (void *)Org_440000_data,  GUI_COUNTOF(Org_440000_data),  Org_440000_children, 2, GUI_COUNTOF(Org_440000_children), NULL},
//     {Screen4_str5,  1, "450000",  (void *)Org_450000_data,  GUI_COUNTOF(Org_450000_data),  Org_450000_children, 2, GUI_COUNTOF(Org_450000_children), NULL},
// 		
//     //{Screen4_str6,  3, "460000",  (void *)Org_460000_data,  GUI_COUNTOF(Org_460000_data),  NULL, 1, 0 , NULL},
// };


// //--------------------------------reports and diag screen structure-------------------------------------
// /********************************************* 500000 ******************************************************/
// Screen_org_data Org_540000_children [] = {
// 		{Screen54_str0, 5, "541000",  (void *)Org_541000_data,  GUI_COUNTOF(Org_541000_data), NULL,									 3, 0,  NULL},                                
// };

// Screen_org_data Org_523000_children[] = {
//     {Screen523_str1,  2, "523100",  (void *)Org_523100_data,  GUI_COUNTOF(Org_523100_data), NULL, 4, 0, NULL},
//     {Screen523_str3,  2, "523200",  (void *)Org_523200_data,  GUI_COUNTOF(Org_523200_data), NULL, 4, 0, NULL},
//     {Screen523_str4,  2, "523300",  (void *)Org_523300_data,  GUI_COUNTOF(Org_523300_data), NULL, 4, 0, NULL},
//     {Screen523_str5,  2, "523400",  (void *)Org_523400_data,  GUI_COUNTOF(Org_523400_data), NULL, 4, 0, NULL},
//     {Screen523_str6,  2, "523500",  (void *)Org_523500_data,  GUI_COUNTOF(Org_523500_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_520000_children[] = {
//     {Screen52_str1,  2, "521000",  (void *)Org_521000_data,  GUI_COUNTOF(Org_521000_data), NULL,                 3, 0,                                NULL},
//     {Screen52_str2,  2, "522000",  (void *)Org_522000_data,  GUI_COUNTOF(Org_522000_data), NULL,                 3, 0,                                NULL},
//     {Screen52_str3,  1, "523000",  (void *)Org_523000_data,  GUI_COUNTOF(Org_523000_data), Org_523000_children,  3, GUI_COUNTOF(Org_523000_children), NULL},
//     {Screen52_str4,  2, "524000",  (void *)Org_524000_data,  GUI_COUNTOF(Org_524000_data), NULL,                 3, 0,                                NULL},
//     {Screen52_str5,  2, "525000",  (void *)Org_525000_data,  GUI_COUNTOF(Org_525000_data), NULL,                 3, 0,                                NULL},
//     {Screen0_str3,   2, "526000",  (void *)Org_526000_data,  GUI_COUNTOF(Org_526000_data), NULL,                 3, 0,                                NULL},
// };
// Screen_org_data Org_510000_children[] = {
//     {Screen51_str1,  3, "511000",  (void *)Org_511000_data,  GUI_COUNTOF(Org_511000_data), NULL,                 3, 0,                                NULL},
// };

// Screen_org_data Org_500000_children[] = {
//     {Screen5_str1,  2, "510000",  (void *)Org_510000_data,  GUI_COUNTOF(Org_510000_data), Org_510000_children, 2, GUI_COUNTOF(Org_510000_children), NULL},
//     {Screen5_str2,  1, "520000",  (void *)Org_520000_data,  GUI_COUNTOF(Org_520000_data), Org_520000_children, 2, GUI_COUNTOF(Org_520000_children), NULL},
//     {Screen5_str3,  2, "530000",  (void *)Org_530000_data,  GUI_COUNTOF(Org_530000_data), NULL,                2, 0,                                NULL},
//  //   {Screen54_str0, 2, "540000",  (void *)Org_540000_data,  GUI_COUNTOF(Org_540000_data), Org_540000_children, 2, GUI_COUNTOF(Org_540000_children), NULL},                                
// 		{Screen55_str0, 2, "540000",  (void *)Org_550000_data,  GUI_COUNTOF(Org_550000_data), NULL,                2, 0,                                NULL},
// };



// //--------------------------------admin screen structure-------------------------------------
// /********************************************* 610000 ******************************************************/

// Screen_org_data Org_614000_sibling[] = {
//     {Screen61_str7,  5, "614100",  (void *)Org_614100_data,  GUI_COUNTOF(Org_614100_data), NULL, 4, 0, NULL},
//     {Screen61_str7,  3, "614200",  (void *)Org_614200_data,  GUI_COUNTOF(Org_614200_data), NULL, 4, 0, NULL},
//     {Screen61_str7,  3, "614300",  (void *)Org_614300_data,  GUI_COUNTOF(Org_614300_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_615000_sibling[] = {
//     {Screen61_str9,  5, "615100",  (void *)Org_615100_data,  GUI_COUNTOF(Org_615100_data), NULL, 4, 0, NULL},
//     {Screen61_str9,  3, "615200",  (void *)Org_615200_data,  GUI_COUNTOF(Org_615200_data), NULL, 4, 0, NULL},
//     {Screen61_str9,  3, "615300",  (void *)Org_615300_data,  GUI_COUNTOF(Org_615300_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_617000_sibling[] = {
//     {Screen617_str1,  3, "617100",  (void *)Org_617100_data,  GUI_COUNTOF(Org_617100_data), NULL, 4, 0, NULL},
//     {Screen61_str13,  5, "617200",  (void *)Org_617200_data,  GUI_COUNTOF(Org_617200_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_618000_sibling[] = {
//     {Screen61_str15,  5, "618100",  (void *)Org_618100_data,  GUI_COUNTOF(Org_618100_data), NULL, 4, 0, NULL},
//     {Screen61_str15,  5, "618200",  (void *)Org_618200_data,  GUI_COUNTOF(Org_618200_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_619210_children[] = {
//     {Screen61922_str1, 2, "619211",  (void *)Org_619211_data,  GUI_COUNTOF(Org_619211_data), NULL, 6, 0, NULL},
// };

// Screen_org_data Org_619200_sibling[] = {
//     {Screen619_str4,  3,  "619210",  (void *)Org_619210_data,  GUI_COUNTOF(Org_619210_data), Org_619210_children, 5, GUI_COUNTOF(Org_619210_children), NULL},
//     {Screen619_str4,  9,  "619220",  (void *)Org_619220_data,  GUI_COUNTOF(Org_619220_data), NULL,                5, 0,                                NULL},
//     {Screen619_str4,  11, "619230",  (void *)Org_619230_data,  GUI_COUNTOF(Org_619230_data), NULL,                5, 0,                                NULL},
//     {Screen619_str4,  9,  "619240",  (void *)Org_619240_data,  GUI_COUNTOF(Org_619240_data), NULL,                5, 0,                                NULL},
// };

// Screen_org_data Org_619100_sibling[] = {
//   //  {Screen619_str2,  3,  "619110",  (void *)Org_619110_data,  GUI_COUNTOF(Org_619110_data), NULL,               5, 0,                                NULL},
//     {Screen619_str2,  9,  "619110",  (void *)Org_619110_data,  GUI_COUNTOF(Org_619110_data), NULL,               5, 0,                                NULL},
//     {Screen619_str2,  11, "619120",  (void *)Org_619120_data,  GUI_COUNTOF(Org_619120_data), NULL,               5, 0,                                NULL},
//     {Screen619_str2,  9,  "619130",  (void *)Org_619130_data,  GUI_COUNTOF(Org_619130_data), NULL,               5, 0,                                NULL},
// };

// Screen_org_data Org_619000_children[] = {
//    // {Screen619_str2,  3, "619100",  (void *)Org_619100_data,  GUI_COUNTOF(Org_619100_data), Org_619100_sibling,  4, GUI_COUNTOF(Org_619100_sibling), &Org_619100_screen},
// 	  {Screen619_str2,  3, "619100",  (void *)Org_619100_data,  GUI_COUNTOF(Org_619100_data), Org_619100_sibling,  4, GUI_COUNTOF(Org_619100_sibling), &Org_619100_screen},
//    // {Screen619_str4,  3, "619200",  (void *)Org_619200_data,  GUI_COUNTOF(Org_619200_data), Org_619200_sibling,  4, GUI_COUNTOF(Org_619200_sibling), &Org_619200_screen},
//     {Screen619_str4,  3, "619200",  (void *)Org_619200_data,  GUI_COUNTOF(Org_619200_data), Org_619200_sibling,  4, GUI_COUNTOF(Org_619200_sibling), &Org_619200_screen},
// };

// Screen_org_data Org_61A100_children[] = {
//    {Screen61A11_str0,  5, "61A110",  (void *)Org_61A110_data,  GUI_COUNTOF(Org_61A110_data), NULL, 5, 0, NULL},
// };

// Screen_org_data Org_61A200_children[] = {
//    {Screen61A21_str0,  5, "61A210",  (void *)Org_61A210_data,  GUI_COUNTOF(Org_61A210_data), NULL, 5, 0, NULL},
// };

// Screen_org_data Org_61A000_children[] = {
//    {Screen61A1_str0,  3, "61A100",  (void *)Org_61A100_data,  GUI_COUNTOF(Org_61A100_data), Org_61A100_children,  4, GUI_COUNTOF(Org_61A100_children), NULL},
//    {Screen61A2_str0,  3, "61A200",  (void *)Org_61A200_data,  GUI_COUNTOF(Org_61A200_data), Org_61A200_children,  4, GUI_COUNTOF(Org_61A200_children), NULL},
// };

// Screen_org_data Org_610000_children[] = {
//     {Screen61_str1,  6, "611000", (void *)Org_611000_data, GUI_COUNTOF(Org_611000_data), NULL,                3, 0,                               NULL},
//     {Screen61_str3,  6, "612000", (void *)Org_612000_data, GUI_COUNTOF(Org_612000_data), NULL,                3, 0,                               NULL},
//     {Screen61_str5,  6, "613000", (void *)Org_613000_data, GUI_COUNTOF(Org_613000_data), NULL,                3, 0,                               NULL},
//     {Screen61_str7,  2, "614000", (void *)Org_614000_data, GUI_COUNTOF(Org_614000_data), Org_614000_sibling,  3, GUI_COUNTOF(Org_614000_sibling), NULL},
//     {Screen61_str9,  2, "615000", (void *)Org_615000_data, GUI_COUNTOF(Org_615000_data), Org_615000_sibling,  3, GUI_COUNTOF(Org_615000_sibling), NULL},
//     {Screen61_str11, 5, "616000", (void *)Org_616000_data, GUI_COUNTOF(Org_616000_data), NULL,                3, 0,                               NULL},
//     {Screen61_str13, 3, "617000", (void *)Org_617000_data, GUI_COUNTOF(Org_617000_data), Org_617000_sibling,  3, GUI_COUNTOF(Org_617000_sibling), NULL},
//     {Screen61_str15, 3, "618000", (void *)Org_618000_data, GUI_COUNTOF(Org_618000_data), Org_618000_sibling,  3, GUI_COUNTOF(Org_618000_sibling), NULL},
//     {Screen61_str17, 3, "619000", (void *)Org_619000_data, GUI_COUNTOF(Org_619000_data), Org_619000_children, 3, GUI_COUNTOF(Org_619000_children), NULL},
//     {Screen61A_str0, 2, "61A000", (void *)Org_61A000_data, GUI_COUNTOF(Org_61A000_data), Org_61A000_children, 3, GUI_COUNTOF(Org_61A000_children), NULL},
// 		{Screen61B_str0, 3, "61B000",	(void *)Org_61B000_data, GUI_COUNTOF(Org_61B000_data), NULL,                3, 0,                               NULL},
// };

// /********************************************* 630000 ******************************************************/
// Screen_org_data Org_637100_children[] = {
//     {Screen63_str8,    6, "637000", (void *)Org_637100_data, GUI_COUNTOF(Org_637100_data), NULL, 4, 0, NULL},
//     {Screen0_str6,     1, "600000", (void *)Org_600000_data, GUI_COUNTOF(Org_600000_data), NULL, 4, 0, NULL},
// };

// Screen_org_data Org_637310_children[] = {
//     {Screen63_str8,    6, "637300", (void *)Org_637300_data, GUI_COUNTOF(Org_637300_data), NULL, 4, 0, NULL},
//     {Screen0_str6,     1, "600000", (void *)Org_600000_data, GUI_COUNTOF(Org_600000_data), NULL, 4, 0, NULL},
// };
// //commented below code for elemination of First screen Wizard screen 637000 on 28/09/2013
// /*Screen_org_data Org_637100_children[] = {
//     {Screen63711_str0,  3, "637110", (void *)Org_637110_data, GUI_COUNTOF(Org_637110_data), Org_637110_children, 5, GUI_COUNTOF(Org_637110_children), NULL},
// };*/

// Screen_org_data Org_637300_children[] = {
//     {Screen63731_str0,  3, "637310", (void *)Org_637310_data, GUI_COUNTOF(Org_637310_data), Org_637310_children, 5, GUI_COUNTOF(Org_637310_children), NULL},
// };

// Screen_org_data Org_637000_sibling[] = {
//   //  {Screen63_str8,  6, "637100", (void *)Org_637100_data, GUI_COUNTOF(Org_637100_data), Org_637100_children, 4, GUI_COUNTOF(Org_637100_children), NULL},
// 	  {Screen63_str8,  3, "637100", (void *)Org_637100_data, GUI_COUNTOF(Org_637100_data), Org_637100_children, 4, GUI_COUNTOF(Org_637100_children), NULL},
//     {Screen63_str8,  6, "637200", (void *)Org_637200_data, GUI_COUNTOF(Org_637200_data), NULL,                4, 0,                                NULL},
//     {Screen63_str8,  6, "637300", (void *)Org_637300_data, GUI_COUNTOF(Org_637300_data), Org_637300_children, 4, GUI_COUNTOF(Org_637300_children), NULL},
// };

// Screen_org_data Org_630000_children[] = {
//     {Screen631_str0,  3, "631000", (void *)Org_631000_data, GUI_COUNTOF(Org_631000_data), NULL,               3, 0,                                NULL},
//     {Screen632_str0,  3, "632000", (void *)Org_632000_data, GUI_COUNTOF(Org_632000_data), NULL,               3, 0,                               NULL},
//     {Screen633_str0,  3, "633000", (void *)Org_633000_data, GUI_COUNTOF(Org_633000_data), NULL,               3, 0,                               NULL},
//     {Screen634_str0,  3, "634000", (void *)Org_634000_data, GUI_COUNTOF(Org_634000_data), NULL,               3, 0,                               NULL},
//     {Screen635_str0,  3, "635000", (void *)Org_635000_data, GUI_COUNTOF(Org_635000_data), NULL,               3, 0,                               NULL},
//     {Screen63_str7,   3, "636000", (void *)Org_636000_data, GUI_COUNTOF(Org_636000_data), NULL,               3, 0,                               NULL},
//     //commented below code for elemination of First screen Wizard screen 637000 on 28/09/2013
//     //{Screen63_str8,   3, "637000", (void *)Org_637000_data, GUI_COUNTOF(Org_637000_data), Org_637000_sibling, 3, GUI_COUNTOF(Org_637000_sibling), &Org_637000_screen},
//     {Screen63_str8,   6, "637000", (void *)Org_637000_data, GUI_COUNTOF(Org_637000_data), Org_637000_sibling, 3, GUI_COUNTOF(Org_637000_sibling), &Org_637000_screen},
// };

// /********************************************* 640000 ******************************************************/
// Screen_org_data Org_642100_children[] = {
//     {Screen641311_str0,  3, "642110",  (void *)Org_642110_data,  GUI_COUNTOF(Org_642110_data), NULL, 5, 0, NULL},
//     {Screen641312_str0,  3, "642120",  (void *)Org_642120_data,  GUI_COUNTOF(Org_642120_data), NULL, 5, 0, NULL},
// };

// Screen_org_data Org_642000_children[] = {
//     {Screen6_str4,  2, "642100",  (void *)Org_642100_data,  GUI_COUNTOF(Org_642100_data), Org_642100_children, 4, GUI_COUNTOF(Org_642100_children), NULL},
// };

// Screen_org_data Org_640000_sibling[] = {
// 	//commented below code for elemination of First screen of Update Firmware Wizard screen 640000 on 25/09/2013
//   //  {Screen6_str4,  9,  "641100",  (void *)Org_641100_data,  GUI_COUNTOF(Org_641100_data), NULL,                4, 0,                                NULL},
//     {Screen6_str4,  3,  "641000",  (void *)Org_641000_data,  GUI_COUNTOF(Org_641000_data), NULL,                3, 0,                                NULL},
//     {Screen6_str4,  3,  "642000",  (void *)Org_642000_data,  GUI_COUNTOF(Org_642000_data), Org_642000_children, 3, GUI_COUNTOF(Org_642000_children), NULL},
//     {Screen6_str4,  9,  "643000",  (void *)Org_643000_data,  GUI_COUNTOF(Org_643000_data), NULL,                3, 0,                                NULL},
//     {Screen6_str4,  3,  "644000",  (void *)Org_644000_data,  GUI_COUNTOF(Org_644000_data), NULL,                3, 0,                                NULL},
//     {Screen6_str4,  9,  "645000",  (void *)Org_645000_data,  GUI_COUNTOF(Org_645000_data), NULL,                3, 0,                                NULL},
// };
// 	//commented below code for elemination of First screen of Update Firmware Wizard screen 640000 on 25/09/2013
// /*Screen_org_data Org_640000_children[] = {
//     {Screen6_str4,    3, "641000", (void *)Org_641000_data, GUI_COUNTOF(Org_641000_data), Org_641000_sibling,  3, GUI_COUNTOF(Org_641000_sibling),  &Org_641000_screen},
// };*/
// /********************************************* 650000 ******************************************************/
// Screen_org_data Org_650000_children[] = {
//     {Screen65_str2,  5, "651000", (void *)Org_651000_data, GUI_COUNTOF(Org_651000_data), NULL, 3, 0, NULL},
//     {Screen65_str5,  5, "652000", (void *)Org_652000_data, GUI_COUNTOF(Org_652000_data), NULL, 3, 0, NULL},
//     {Screen65_str7,  5, "653000", (void *)Org_653000_data, GUI_COUNTOF(Org_653000_data), NULL, 3, 0, NULL},
// };

// /********************************************* 660000 ******************************************************/
// Screen_org_data Org_660000_children[] = {
//     {Screen66_str1,  3, "661000", (void *)Org_661000_data, GUI_COUNTOF(Org_661000_data), NULL, 3, 0, NULL},
//     {Screen66_str3,  5, "662000", (void *)Org_662000_data, GUI_COUNTOF(Org_662000_data), NULL, 3, 0, NULL},
//     {Screen66_str5,  5, "663000", (void *)Org_663000_data, GUI_COUNTOF(Org_663000_data), NULL, 3, 0, NULL},
//     {Screen66_str7,  5, "664000", (void *)Org_664000_data, GUI_COUNTOF(Org_664000_data), NULL, 3, 0, NULL},
//     {Screen66500_str1,5, "665000", (void *)Org_665000_data, GUI_COUNTOF(Org_665000_data), NULL, 3, 0, NULL},
//     
// };
// /********************************************* 600000 ******************************************************/
// Screen_org_data Org_600000_children[] = {
//     {Screen6_str1,  2,  "610000",  NULL, NULL,  NULL, NULL, NULL, NULL},
//     {Screen2_str1,  3,  "210000",  NULL, NULL,  NULL, NULL, NULL, NULL},
//     {Screen6_str3,  2,  "630000",  NULL, NULL,  NULL, NULL, NULL, NULL},
//     {Screen6_str4,  9,  "640000",  NULL, NULL,  NULL, NULL, NULL, NULL},
//     {Screen6_str5,  3,  "650000",  NULL, NULL,  NULL, NULL, NULL, NULL},
//     {Screen6_str6,  2,  "660000",  (void *)Org_660000_data, GUI_COUNTOF(Org_660000_data),  Org_660000_children, 2, GUI_COUNTOF(Org_660000_children), NULL},
// };

// //------------------------------ setup wizard structures---------------------------------------------
// Screen_org_data Org_130000_children[] = {
//     {Screen11_str0,  3, "131000",  (void *)Org_131000_data,  GUI_COUNTOF(Org_131000_data),  NULL, 3, 0, NULL},
// };

// Screen_org_data Org_100000_sibling[] = {
//     {Screen11_str0,  3, "110000",  (void *)Org_110000_data,  GUI_COUNTOF(Org_110000_data),  NULL, 2, 0, NULL},
//     {Screen11_str0,  3, "120000",  (void *)Org_120000_data,  GUI_COUNTOF(Org_120000_data),  NULL, 2, 0, &Org_120000_screen},
//     {Screen11_str0,  3, "130000",  (void *)Org_130000_data,  GUI_COUNTOF(Org_130000_data),  Org_130000_children, 2, GUI_COUNTOF(Org_130000_children), &Org_130000_screen},
//     {Screen11_str0,  3, "140000",  (void *)Org_140000_data,  GUI_COUNTOF(Org_140000_data),  NULL, 2, 0, NULL},
// };
// // Screen_org_data Org_100000_sibling[] = {
// //     {Screen11_str0,  3, "110000",  (void *)Org_110000_data,  GUI_COUNTOF(Org_110000_data),  NULL, 2, 0, NULL},
// //     {Screen11_str0,  3, "120000",  (void *)Org_120000_data,  GUI_COUNTOF(Org_120000_data),  NULL, 2, 0, NULL},
// //     {Screen11_str0,  3, "130000",  (void *)Org_130000_data,  GUI_COUNTOF(Org_130000_data),  NULL, 2, 0, &Org_130000_screen},
// //     {Screen11_str0,  3, "140000",  (void *)Org_140000_data,  GUI_COUNTOF(Org_140000_data),  Org_140000_children, 2, GUI_COUNTOF(Org_140000_children), &Org_140000_screen},
// //     {Screen11_str0,  3, "150000",  (void *)Org_150000_data,  GUI_COUNTOF(Org_150000_data),  NULL, 2, 0, NULL},
// // };



// Screen_org_data Org_720000_children[] = {
//      {Screen45_str2, 2, "452000", (void *)Org_452000_data, GUI_COUNTOF(Org_452000_data), Org_452000_children, 3, GUI_COUNTOF(Org_452000_children), NULL},
// };

// Screen_org_data Org_730000_children[] = {
//      {Screen45_str3,  2, "453000", (void *)Org_453000_data, GUI_COUNTOF(Org_453000_data), Org_453000_children, 3, GUI_COUNTOF(Org_453000_children), NULL},
//      {Screen453_str5, 5, "453200", (void *)Org_453200_data, GUI_COUNTOF(Org_453200_data), NULL,                4, 0,                                NULL},
// 		 //{Screen453_str5, 5, "453200", NULL,0, NULL,                0, 0,                                NULL},
// 		 //{Screen451_str4, 2, "451000", (void *)Org_451000_data, GUI_COUNTOF(Org_451000_data), Org_451000_children, 4,	GUI_COUNTOF(Org_451000_children),	NULL},
// 		 {Screen4514_str0, 5, "451400",  (void *)Org_451400_data,  GUI_COUNTOF(Org_451400_data), NULL,                4, 0,                                NULL},
// };

// Screen_org_data Org_740000_children[] = {
//      {Screen45_str5,  2, "455000", (void *)Org_455000_data, GUI_COUNTOF(Org_455000_data), Org_455000_children, 3, GUI_COUNTOF(Org_455000_children), NULL},
//      {Screen455_str1, 5, "455100", (void *)Org_455100_data, GUI_COUNTOF(Org_455100_data), NULL,                4, 0,                                NULL},
// 		 //{Screen453_str5, 5, "453200", NULL,0, NULL,                0, 0,                                NULL},
// 		 {Screen4514_str0, 5, "451400",  (void *)Org_451400_data,  GUI_COUNTOF(Org_451400_data), NULL,                4, 0,                                NULL},
// };

// Screen_org_data Org_750000_children[] = {
//      {Screen45_str4,  2, "454000", (void *)Org_454000_data, GUI_COUNTOF(Org_454000_data), Org_454000_children, 3, GUI_COUNTOF(Org_454000_children), NULL},
//      {Screen454_str1, 5, "454100", (void *)Org_454100_data, GUI_COUNTOF(Org_454100_data), NULL,                4, 0,                                NULL},
// 		 //{Screen453_str5, 5, "453200", NULL,0, NULL,                0, 0,                                NULL},
// 		 {Screen4514_str0, 5, "451400",  (void *)Org_451400_data,  GUI_COUNTOF(Org_451400_data), NULL,                4, 0,                                NULL},
// };

// Screen_org_data Org_760000_children[] = {
// 	{Screen21_str20,  12, "761000", (void *)Org_761000_data, GUI_COUNTOF(Org_761000_data), NULL, 0, 0, NULL},
// 	{Screen21_str20,  12, "762000", (void *)Org_762000_data, GUI_COUNTOF(Org_762000_data), NULL, 0, 0, NULL},
// 	{Screen21_str20,  12, "763000", (void *)Org_763000_data, GUI_COUNTOF(Org_763000_data), NULL, 0, 0, NULL},
// 	{Screen21_str20,  12, "764000", (void *)Org_764000_data, GUI_COUNTOF(Org_764000_data), NULL, 0, 0, NULL},
// 	{Screen21_str20,  12, "765000", (void *)Org_765000_data, GUI_COUNTOF(Org_765000_data), NULL, 0, 0, NULL},
// 	{Screen21_str20,  12, "766000", (void *)Org_766000_data, GUI_COUNTOF(Org_766000_data), NULL, 0, 0, NULL},
// 	{Screen21_str20,  12, "767000", (void *)Org_767000_data, GUI_COUNTOF(Org_767000_data), NULL, 0, 0, NULL},
// 	{Screen21_str20,  12, "768000", (void *)Org_768000_data, GUI_COUNTOF(Org_768000_data), NULL, 0, 0, NULL},
// };


// Screen_org_data Org_700000_parent[] = {
//     {Screen21_str2,  12, "700000",  (void *)Org_700000_data, GUI_COUNTOF(Org_700000_data),  NULL,                0, 0,                                NULL},
//     {Screen21_str5,  12, "710000",  (void *)Org_710000_data, GUI_COUNTOF(Org_710000_data),  NULL,                0, 0,                                NULL},
//     {Screen21_str8,  13, "720000",  (void *)Org_720000_data, GUI_COUNTOF(Org_720000_data),  Org_720000_children, 0, GUI_COUNTOF(Org_720000_children), NULL},
//     {Screen21_str11, 14, "730000",  (void *)Org_730000_data, GUI_COUNTOF(Org_730000_data),  Org_730000_children, 0, GUI_COUNTOF(Org_730000_children), NULL},
//     {Screen21_str14, 14, "740000",  (void *)Org_740000_data, GUI_COUNTOF(Org_740000_data),  Org_740000_children, 0, GUI_COUNTOF(Org_740000_children), NULL},
//     {Screen21_str17, 14, "750000",  (void *)Org_750000_data, GUI_COUNTOF(Org_750000_data),  Org_750000_children, 0, GUI_COUNTOF(Org_750000_children), NULL},
// 		{Screen21_str20, 12, "760000",	(void *)Org_760000_data, GUI_COUNTOF(Org_760000_data),	Org_760000_children, 0, GUI_COUNTOF(Org_760000_children), NULL},
// };

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
