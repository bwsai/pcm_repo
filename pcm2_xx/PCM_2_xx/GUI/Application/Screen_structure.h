/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen_structure.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN_STRUCTURE
#define  SCREEN_STRUCTURE

#include "../GUI/DIALOG.h"
#include "../GUI/TEXT.h"
#include "../GUI/ICONVIEW.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

#ifndef GUI_CONST_STORAGE
#define GUI_CONST_STORAGE const
#endif

/*============================================================================
* Public Data Types
*===========================================================================*/

typedef struct
              {
                int num;
                char * Text;
              }STRING_DATA;

typedef struct
            {
              GUI_CONST_STORAGE GUI_BITMAP * pBitmap;
              unsigned int index;
              unsigned int sel_icon;
              unsigned int screen_an_option;
            } SCREEN_TYPE1_DATA;

typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 unsigned int * Variable1; // UNITS
                 unsigned int * Variable2; // UNITS
                 void * Variable3;
                 unsigned int data_type;
                 unsigned int selection;
                 unsigned int screen_an_option;
               } SCREEN_TYPE2_DATA;


typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 unsigned int Description_text_index;
                 unsigned int* Variable1;
                 unsigned int data_type;
                 unsigned int screen_an_option;
               } SCREEN_TYPE3_DATA;

typedef struct
               {
                 GUI_CONST_STORAGE GUI_BITMAP * pBitmap;
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 unsigned int Description_text_index;
                 unsigned int *Variable1;
                 unsigned int data_type1;
                 unsigned int screen_an_option;
               } SCREEN_TYPE4_DATA;

typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 void * Variable1;
                 unsigned int* Variable1_unit;
                 unsigned int data_type;
                 unsigned int screen_an_option;
               } SCREEN_TYPE5_DATA;

typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 void * Variable1;
                 unsigned int screen_an_option;
               } SCREEN_TYPE6_DATA;

typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 unsigned int Description_text_index;
                 void * Variable1;
                 unsigned int *Variable2; // Units
                 unsigned int data_type;
                 unsigned int screen_an_option;
               } SCREEN_TYPE9_DATA;

typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 unsigned int Description_text_index;
                 void * Variable1;
                 void * Variable2;
                 void * Variable3;
                 void * Variable4;
                 unsigned int data_type1;
                 unsigned int data_type2;
                 unsigned int data_type3;
                 unsigned int data_type4;
                 unsigned int screen_an_option;
               } SCREEN_TYPE10_DATA;


typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
                 unsigned int Description_text_index;
                 //unsigned int * Percent_complete;
                 unsigned int screen_an_option;
               } SCREEN_TYPE11_DATA;

typedef struct
               {
								 unsigned int Text_index1;
								 unsigned int Text_index2;
								 unsigned int Text_index3;
								 unsigned int Text_index4;
                 double * variable1;  // Weight for Weight Run mode screen and Rate for Rate Run mode screen
                 float * variable2;   // Rate for Weight Run mode screen and Weight for Rate Run mode screen
                 float * variable3;  // Belt speed
								 float * variable4;
                 unsigned int* variable1_unit;
                 unsigned int* variable2_unit;
                 unsigned int* variable3_unit;
								 unsigned int* variable4_unit;
               } RUNMODE_SCREEN_DATA;

typedef struct
               {
                 unsigned int Text_index1; //cutoff
                 unsigned int Text_index2; //weight Accumulated
                 unsigned int Text_index3; //current weight setpoint
                 unsigned int Text_index4; //current rate
                 unsigned int Text_index5; //current speed
                 double * variable1;       //Total accumulated weight
                 float * variable2;       //Target weight load
                 float * variable3;       //Current rate
                 float * variable4;       //Current speed
                 unsigned int screen_an_option;
               } LOADOUT_RUNMODE_SCREEN_DATA;

typedef struct
               {
                 unsigned int Main_text_index;
                 unsigned int Tooltip_index;
								 unsigned int Text_index3; //actual accumulated weight
                 unsigned int Text_index1; //actual load/rate string
                 unsigned int Text_index2; //target load/rate string
								 void * variable4;				 //Accumulated Weight
                 void * variable1;         //Actual load/rate value
                 void * variable2;         //Target load/rate value
                 void * variable3;         //Blending Percentage
                 unsigned int* unit;             //load/rate unit
								 unsigned int* variable3_unit; 	//Accumulated Weight
                 unsigned int screen_an_option;
               } BLEND_RATE_LOAD_CTRL_RUNMODE_SCREEN_DATA;

typedef struct
            {
                char * number;
            }NEXT_ORG_NUM;

typedef struct
            {
               char * Cancel;
               char * End;
               char * setup_wizard_org_no;
               NEXT_ORG_NUM * Next;
               unsigned int wizard_size;
            }SETUP_WIZARD_SCREEN;
typedef struct
            {
               SETUP_WIZARD_SCREEN *w_info;
               int w_index;
            }PARENT_WIZARD;				
typedef struct Screen_org_data Screen_org_data;

typedef struct
            {
                U8 Addr1;
                U8 Addr2;
                U8 Addr3;
                U8 Addr4;
            }IP_STRUCT;
typedef struct
{
                U8 Addr1;
                U8 Addr2;
                U8 Addr3;
                U8 Addr4;
								U8 Addr5;	
								U8 Addr6;	
}MAC_ID_STRUCT;						

typedef struct
            {
                U8  password_to_be_entered;
                U8  password_entered;
            }PASSWORD_STRUCT_DEF;

typedef struct
            {
                U8 count;
                char name[6][30];
            }FILE_UPDATE_STRUCT;

typedef struct
            {
                Screen_org_data * Current_screen_info;
                Screen_org_data * Screen_before_pw_org_num;
                SETUP_WIZARD_SCREEN * wizard_screen;
                WM_HWIN Listbox;  //I32
                WM_HWIN Multi_text;
                WM_HWIN Keypad;
                WM_HWIN Numpad;
                WM_HWIN Icon_screen;
                PROGBAR_Handle Prog_bar;  //I32
                IMAGE_Handle Image_display; //I32
                IMAGE_Handle Image_display1;
                IMAGE_Handle Image_display2;
                IMAGE_Handle Image_display3;
                IMAGE_Handle Image_display4;
                TEXT_Handle Text_widget; //I32
                TEXT_Handle Text_widget1;
                TEXT_Handle Text_widget2;
                TEXT_Handle Text_widget3;
                TEXT_Handle Text_widget4;
                TEXT_Handle Text_widget5;
                TEXT_Handle Text_widget6;
                TEXT_Handle Text_widget7;
                TEXT_Handle Text_widget8;
                TEXT_Handle Text_widget9;
                TEXT_Handle Text_widget10;
                TEXT_Handle Text_widget11;
                EDIT_Handle Edit_widget1;
                EDIT_Handle Edit_widget2;
                EDIT_Handle Edit_widget3;
                int Setup_wizard;
                int Wizard_screen_index;
                unsigned int Key_press;
                unsigned int Change;
                unsigned int In_wizard;
                unsigned int Child_present;
                unsigned int Cancel_operation;
                unsigned int Back_one_level;
                unsigned int Up_right_key_pressed;
                unsigned int Clr_wt_pw_to_be_entered;
								unsigned int Percent_complete;
                char New_screen_num[7];
								char Unit_per_pulse_str[30];
								U8 GUI_structure_backup;
								U8 Error_wrn_msg_flag[16];
            }GUI_DATA_NAV_STRUCT;

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

struct Screen_org_data
                    {
                        unsigned int Screen_name_index; 
                        char Screen_type;
                        char * Screen_org_no;
                        void * screen_data;
                        unsigned int size_of_screen_data;
                        struct Screen_org_data * child;
                        char level;
                        unsigned int nos_of_children;
                        SETUP_WIZARD_SCREEN * screens;
                    };
/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
