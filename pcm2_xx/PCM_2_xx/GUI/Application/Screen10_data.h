/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen10_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN10_DATA
#define  SCREEN10_DATA
/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_structure.h"
#include "Screen_data_enum.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

 __align(4) SCREEN_TYPE10_DATA Org_314000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
  {Screen317_str1, Screen317_str2, Screen317_str3,
       &Calibration_var.old_span_value,
       &Calibration_var.new_span_value,
       &Calibration_var.span_diff,
       &Calibration_var.real_time_rate,
      float_type, float_type, float_type, float_type, 0},
 };

__align(4) SCREEN_TYPE10_DATA Org_325000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen325_str1, Screen325_str2, Screen325_str3,
                                             &Calibration_var.old_span_value,
                                             &Calibration_var.new_span_value,
                                             &Calibration_var.span_diff,
                                             &Calibration_var.real_time_rate,
                                            float_type, float_type, float_type, float_type, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_332000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen333_str1, Screen333_str2, Screen333_str3,
                                             &Calibration_var.old_span_value,
                                             &Calibration_var.new_span_value,
                                             &Calibration_var.span_diff,
                                             &Calibration_var.real_time_rate,
                                            float_type, float_type, float_type, float_type, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_341300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3414_str1, Screen3414_str2, Screen3414_str3,
                                             &Calibration_var.Old_belt_length,
                                             &Calibration_var.New_belt_length,
                                             &Calibration_var.Belt_length_diff,
                                            NULL,
                                            float_type, float_type, float_type, no_variable, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_341400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3415_str1, Screen3415_str2, Screen3415_str3,
                                             &Calibration_var.Old_zero_value,
                                             &Calibration_var.New_zero_value,
                                             &Calibration_var.Zero_diff,
                                             &Calibration_var.real_time_rate,
                                            float_type, float_type, float_type, float_type, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_341500_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3416_str1, Screen3416_str2, Screen3416_str3,
                                             &Calibration_var.New_belt_length,
                                             &Scale_setup_var.Belt_length_unit,
                                             &Calibration_var.New_zero_value,
                                            NULL,
                                            float_type, unsigned_char_type, float_type, no_variable, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_342300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3424_str1, Screen3424_str2, Screen3424_str3,
                                             &Calibration_var.Old_belt_length,
                                             &Calibration_var.New_belt_length,
                                             &Calibration_var.Belt_length_diff,
                                             &Scale_setup_var.Belt_length_unit,
                                            float_type, float_type, float_type, unsigned_char_type, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_342400_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3425_str1, Screen3425_str2, Screen3425_str3,
                                             &Calibration_var.New_belt_length,
                                             &Scale_setup_var.Belt_length_unit,
                                            NULL,
                                            NULL,
                                            float_type, unsigned_char_type, no_variable, no_variable, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_351000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3511_str1, Screen3511_str2, Screen3511_str3,
                                             &Calculation_struct.Belt_speed,
                                             &Scale_setup_var.Speed_unit,
                                            NULL,
                                            NULL,
                                            float_type, unsigned_char_type, no_variable, no_variable, 0},
                                       };



__align(4) SCREEN_TYPE10_DATA Org_351200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3513_str1, Screen3513_str2, Screen3513_str3,
                                             &Calibration_var.Old_zero_value,
                                             &Calibration_var.New_zero_value,
                                             &Calibration_var.Zero_diff,
                                             &Calibration_var.real_time_rate,
                                            float_type, float_type, float_type, float_type, 0},
                                       };

__align(4) SCREEN_TYPE10_DATA Org_351300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen3514_str1, Screen3514_str2, Screen3514_str3,
                                             &Calculation_struct.Belt_speed,
                                             &Scale_setup_var.Speed_unit,
                                             &Calibration_var.real_time_rate,
                                             &Scale_setup_var.Weight_unit,
                                            float_type, unsigned_char_type, float_type, unsigned_char_type, 0},
                                       };

// __align(4) SCREEN_TYPE10_DATA Org_412200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//                                         {Screen4122_str1, Screen4122_str2, Screen4122_str3,
//                                              &Setup_device_var.Printer_baud,
//                                              &Setup_device_var.Printer_data_bits,
//                                              &Setup_device_var.Printer_stop_bits,
//                                              &Setup_device_var.Printer_flow_control,
//                                             unsigned_int_type, unsigned_int_type, unsigned_int_type, unsigned_char_type, 1},
//                                        };

__align(4) SCREEN_TYPE10_DATA Org_422200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
                                        {Screen4222_str1, Screen4222_str2, Screen4222_str3,
                                             &Setup_device_var.Scoreboard_baud,
                                             &Setup_device_var.Scoreboard_data_bits,
                                             &Setup_device_var.Scoreboard_stop_bits,
                                            NULL, unsigned_int_type, unsigned_int_type, unsigned_int_type, no_variable, 1},
                                       };

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
