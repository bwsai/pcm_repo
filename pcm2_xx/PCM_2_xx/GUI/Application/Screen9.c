/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen9.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
//#include "Calibration.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen9(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : static void callback_screen9(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen9(WM_MESSAGE * pMsg)
{

    int Id, NCode, sel,Key_entry;
    //static int first_time = 0;
    static SCREEN_TYPE9_DATA *data;
    int i,  length = 0, count = 0;
    Screen_org_data * new_screen_org;
    char *last_ptr, char_data;
    char *char_data1;

    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
    data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            break;

                        case WM_NOTIFICATION_SEL_CHANGED:
                            count = 2;
                            if (data[sel].Variable2 == NULL)
                            {
                                count = count-1;
                            }
                            if (data[sel].Variable1 == NULL)
                            {
                                count = count-1;
                            }

                            if (data[sel].Description_text_index != 0)
                            {
                                if (count > 0)
                                {
                                    MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
                                    length = strlen(Strings[data[sel].Description_text_index].Text);
                                    for (i = 0; i < length; i++)
                                    {
                                        if(*(Strings[data[sel].Description_text_index].Text + i) == 'X')
                                        {
                                            if((data[sel].Variable1 != NULL) && (data[sel].Variable2 != NULL))
                                            {
                                                switch(count)
                                                {
                                                    case 1:
                                                        char_data1 = Strings[*data[sel].Variable2].Text;
                                                        MULTIEDIT_AddText(GUI_data_nav.Multi_text, char_data1);
                                                        break;
                                                    case 2:
                                                        multiedit_insert_txt(data[sel].data_type ,data[sel].Variable1);
                                                        break;
                                                }
                                                i = i+3;
                                                count--;
                                                if (count == 0)
                                                {
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (data[sel].Variable1 != NULL)
                                                {
                                                    multiedit_insert_txt(data[sel].data_type ,data[sel].Variable1);
                                                }
                                                else
                                                {
                                                    char_data1 = Strings[*data[sel].Variable2].Text;
                                                    MULTIEDIT_AddText(GUI_data_nav.Multi_text, char_data1);
                                                }
                                                break;
                                            }
                                        }
                                        char_data = *(Strings[data[sel].Description_text_index].Text+i);
                                        MULTIEDIT_AddText(GUI_data_nav.Multi_text, &char_data);
                                    }
                                    last_ptr = strrchr(Strings[data[sel].Description_text_index].Text, 'XXX');
                                    MULTIEDIT_AddText(GUI_data_nav.Multi_text, (last_ptr+2));
                                }
                                else
                                {
                                    MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
                                    MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[sel].Description_text_index].Text);
                                }
                            }
                            WM_SetFocus(GUI_data_nav.Listbox);
                            break;
                    }
                 break;

                default:
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CHILD_DELETED:
                            WM_SetFocus(GUI_data_nav.Listbox);
                            GUI_ClearKeyBuffer();
                            LISTBOX_SetSel(GUI_data_nav.Listbox, 1);
                            break;
                    }
                 break;
            }
         break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT:
                            ;
                           break;

                        case GUI_KEY_LEFT_CUSTOM:
                            if (GUI_data_nav.In_wizard == 1)
                            {
                                GUI_data_nav.Cancel_operation = 1;
                                GUI_data_nav.Key_press = 1;
                            }
														//PVK - Added on 2 May 2016 to restore old zero cal value when cancel the static zero calibration
														if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, STATIC_ZEROCAL_COMPLETE) == 0))
														{
															#ifdef CALCULATION
															Calibration_var.New_zero_value = Calibration_var.Old_zero_value;
															#endif //#ifdef CALCULATION
														}
                            if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,CONV_ANGLE_WARNING) == 0)
                            {
                                //restore the default conveyor angle
                                Scale_setup_var.Conveyor_angle = CONVEYOR_ANGLE_DEFAULT;
                                GUI_data_nav.New_screen_num[0] = '2';
                                GUI_data_nav.New_screen_num[1] = '7';
                                GUI_data_nav.New_screen_num[2] = '0';
                                GUI_data_nav.New_screen_num[3] = '0';
                                GUI_data_nav.New_screen_num[4] = '0';
                                GUI_data_nav.New_screen_num[5] = '0';
                                //GUI_data_nav.New_screen_num[6] = '\0';
                                if (GUI_data_nav.In_wizard == 0)
                                {
                                    GUI_data_nav.Change = 1;
                                }
                            }
                            break;

                        case GUI_KEY_RIGHT:
                            ;
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
                            if (GUI_data_nav.In_wizard == 1)
                            {
                                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_CERTIFIED_UNIT_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MATERIAL_TEST_CERTIFIED_UNIT) == 0))
                                {
                                    if (data[sel].screen_an_option != 0)
                                    {
                                        new_screen_org = GUI_data_nav.Current_screen_info->child;
                                        new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                        for (i = 0; i < 6; i++)
                                        {
                                            GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                        }
                                        //GUI_data_nav.New_screen_num[6] = '\0';
                                        GUI_data_nav.Change = 1;
                                        GUI_data_nav.Child_present = 1;
                                    }
                                }
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, STATIC_ZEROCAL_COMPLETE) == 0)
                                {
                                    GUI_data_nav.New_screen_num[0] = '3';
                                    GUI_data_nav.New_screen_num[1] = '5';
                                    GUI_data_nav.New_screen_num[2] = '2';
                                    GUI_data_nav.New_screen_num[3] = '0';
                                    GUI_data_nav.New_screen_num[4] = '0';
                                    GUI_data_nav.New_screen_num[5] = '0';
                                    //GUI_data_nav.New_screen_num[6] = '\0';
                                    GUI_data_nav.Change = 1;
                                    //store the current values and repeat the calibration
                                    #ifdef CALCULATION
                                    Calculation_struct.Zero_weight = Calibration_var.New_zero_value \
                                                                     / Calculation_struct.Zero_value_conv_factor;//Calculation_struct.Weight_conv_factor;
                                    #endif //#ifdef CALCULATION
                                    GUI_data_nav.GUI_structure_backup = 1;
                                }
                            }
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            if (GUI_data_nav.In_wizard == 1)
                            {
                                if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_VER_ENTRY_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, BACKUP_USB_ACK_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_ACK_ORG_NO) == 0) ||
                                    (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_ACK_ORG_NO) == 0))
                                {
                                    GUI_data_nav.Key_press = 1;
                                    GUI_data_nav.Up_right_key_pressed = 1;
                                }
                                else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, STATIC_ZEROCAL_COMPLETE) == 0)
                                {
                                    //re-perform the calibration, without storing the values
                                    GUI_data_nav.New_screen_num[0] = '3';
                                    GUI_data_nav.New_screen_num[1] = '5';
                                    GUI_data_nav.New_screen_num[2] = '2';
                                    GUI_data_nav.New_screen_num[3] = '0';
                                    GUI_data_nav.New_screen_num[4] = '0';
                                    GUI_data_nav.New_screen_num[5] = '0';
                                    //GUI_data_nav.New_screen_num[6] = '\0';
                                    GUI_data_nav.Change = 1;
                                }
                            }
                            if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, CONV_ANGLE_WARNING) == 0)
                            {
                                GUI_data_nav.New_screen_num[0] = '2';
                                GUI_data_nav.New_screen_num[1] = '7';
                                GUI_data_nav.New_screen_num[2] = '2';
                                GUI_data_nav.New_screen_num[3] = '0';
                                GUI_data_nav.New_screen_num[4] = '0';
                                GUI_data_nav.New_screen_num[5] = '0';
                                //GUI_data_nav.New_screen_num[6] = '\0';
                                GUI_data_nav.Change = 1;
                            }
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;

                        case GUI_KEY_ENTER:
													  


                            if ((data[sel].screen_an_option != 0)  &&
                                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TEST_WEIGHT_CERTIFIED_UNIT_ORG_NO)) &&
                                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MATERIAL_TEST_CERTIFIED_UNIT)))
                            {
                                new_screen_org = GUI_data_nav.Current_screen_info->child;
                                new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                for (i = 0; i < 6; i++)
                                {
                                    GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                }
                                //GUI_data_nav.New_screen_num[6] = '\0';
                                GUI_data_nav.Change = 1;
                                GUI_data_nav.Child_present = 1;
                            }
														else if (GUI_data_nav.Current_screen_info->screens != NULL)
														{   
																  GUI_data_nav.Change = 1;
																  for (i = 0; i < 6; i++)
                                  {
                                    GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->screens->Next->number[i];
                                  }
														}
                            else
                            {
                                GUI_data_nav.Child_present = 0;
                            }
  
                            GUI_data_nav.Key_press = 1;
                            break;

                        case GUI_KEY_BACK:
														go_back_one_level();
                           
                            break;
                    }
                    break; //ID_LISTBOX_1
            }
          break; //WM_KEY

        default:
           WM_DefaultProc(pMsg);
          break;
    }//switch (pMsg->MsgId)
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen9(SCREEN_TYPE9_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen9(SCREEN_TYPE9_DATA *data, int count)
{
    int i,k, length = 0;
    char *last_ptr, char_data;
    char *char_data1;
		char cTmp_string[400];
    GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX_SIZE_X, LISTBOX_SIZE_Y,
                                              WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
    for (i = 0; i < count; i++)
    {
        LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
    }

    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
    LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
    LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
    LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
    LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);

    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
    LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);

    WM_SetCallback(WM_HBKWIN, callback_screen9);
    LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);

    GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(MULTI_EDIT_START, MULTI_WIN_OFFSET,
                                                 MULTI_EDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                                 WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 400, NULL);
    MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
    MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY | MULTIEDIT_CI_EDIT, GUI_BLACK);
    MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_EDIT, GUI_YELLOW);
    MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
    MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
    MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
    MULTIEDIT_SetInsertMode(GUI_data_nav.Multi_text, 0);

    count = 2;
//    MULTIEDIT_SetText(GUI_data_nav.Multi_text, "\n");
//    MULTIEDIT_AddText(GUI_data_nav.Multi_text, "\n");
    if (data[0].Variable2 == NULL)
    {
        count = count-1;
    }
    if (data[0].Variable1 == NULL)
    {
        count = count-1;
    }

    if (data[0].Description_text_index != 0)
    {
        if (count > 0)
        {
            length = strlen(Strings[data[0].Description_text_index].Text);
				for (i = 0; i < length; i++)
        {
					char_data = *(Strings[data[0].Description_text_index].Text+i);
					if(char_data != 'X')
					{
						cTmp_string[k] = char_data;
						k++;
						
					}
					else
					{
						cTmp_string[k] = '\0';
						
						MULTIEDIT_AddText(GUI_data_nav.Multi_text,(cTmp_string )) ;
						if((data[0].Variable1 != NULL) && (data[0].Variable2 != NULL))
						{
									switch(count)
									{
										case 1:
												multiedit_insert_txt(unsigned_char_type ,Strings[*data[0].Variable2].Text);
												break;
										case 2:
												multiedit_insert_txt(data[0].data_type ,data[0].Variable1);
												break;
								}
								i = i+4;
								k=0;
								count--;
								if (count == 0)
								{
										break;
								}
						}
						else
						{
								if (data[0].Variable1 != NULL)
								{
										multiedit_insert_txt(data[0].data_type ,data[0].Variable1);
								}
								else
								{
										char_data1 = Strings[*data[0].Variable2].Text;
										MULTIEDIT_AddText(GUI_data_nav.Multi_text, char_data1);
								}
								break;
						}
										
					}
				}
										
/*					
            for (i = 0; i < length; i++)
            {
                if(*(Strings[data[0].Description_text_index].Text + i) == 'X')
                {
                    if((data[0].Variable1 != NULL) && (data[0].Variable2 != NULL))
                    {
                          switch(count)
                          {
                            case 1:
                                multiedit_insert_txt(unsigned_char_type ,Strings[*data[0].Variable2].Text);
                                break;
                            case 2:
                                multiedit_insert_txt(data[0].data_type ,data[0].Variable1);
                                break;
                        }
                        i = i+4;
                        count--;
                        if (count == 0)
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (data[0].Variable1 != NULL)
                        {
                            multiedit_insert_txt(data[0].data_type ,data[0].Variable1);
                        }
                        else
                        {
                            char_data1 = Strings[*data[0].Variable2].Text;
                            MULTIEDIT_AddText(GUI_data_nav.Multi_text, char_data1);
                        }
                        break;
                    }
                }
                char_data = *(Strings[data[0].Description_text_index].Text+i);
                MULTIEDIT_AddText(GUI_data_nav.Multi_text, &char_data);
            }
	*/					
					
            last_ptr = strrchr(Strings[data[0].Description_text_index].Text, 'XXX');
            MULTIEDIT_AddText(GUI_data_nav.Multi_text, (last_ptr+2));
        }
        else
        {
            MULTIEDIT_AddText(GUI_data_nav.Multi_text, Strings[data[0].Description_text_index].Text);
        }
    }

    WM_SetFocus(GUI_data_nav.Listbox);
    return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
