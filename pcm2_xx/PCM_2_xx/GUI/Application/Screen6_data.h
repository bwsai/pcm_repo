/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen6_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN6_DATA
#define  SCREEN6_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

// __align(4) SCREEN_TYPE6_DATA Org_212000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
//     {Screen21_str4, 0,  NULL, 0},
//     {Current_value_string, 0, NULL,0},	
// };
// __align(4) SCREEN_TYPE6_DATA Org_232000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
//     {Screen23_str4, 0,  &Pcm_var.Http_host_addr, 0},
//     {Current_value_string, 0, NULL,0},	
// };

__align(4) SCREEN_TYPE6_DATA Org_242000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen24_str4, 0,  &Pcm_var.PPP_dial_no, 0},
    {Current_value_string, 0, NULL,0},	
};

__align(4) SCREEN_TYPE6_DATA Org_244000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen24_str8, 0,  NULL, 0},
    {Current_value_string, 0, NULL,0},	
};

__align(4) SCREEN_TYPE6_DATA Org_225000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen66_str9, 0,  Mac_id, 0},
    {Current_value_string, 0, NULL, 0},
};

__align(4) SCREEN_TYPE6_DATA Org_253000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {Screen25_str6, 0,  &Pcm_var.Sntp_host_addr, 0},
    {Current_value_string, 0, NULL,0},	
};
__align(4) SCREEN_TYPE6_DATA Org_413000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
		{Screen413_str1,       Screen413_str2,   &Setup_device_var.Company_name, 0},
		{Screen413_str3,       Screen413_str4,   &Setup_device_var.Addr1, 0},
		{Screen413_str5,       Screen413_str6,   &Setup_device_var.Addr2, 0},
		{Screen413_str7,       Screen413_str8,   &Setup_device_var.Addr3, 0},
		{Screen413_str9,       Screen413_str10,  &Setup_device_var.Phone, 0},
		{Current_value_string, 0,               NULL,                    0},
	 };

__align(4) SCREEN_TYPE6_DATA Org_611000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
	  {Screen611_str1,       Screen611_str2,  &Admin_var.Scale_Name, 0},
		{Current_value_string, 0,              NULL,                  0},
   };

__align(4) SCREEN_TYPE6_DATA Org_612000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
		{Screen612_str1,       Screen612_str2,  &Admin_var.Plant_Name, 0},
		{Current_value_string, 0,              NULL,                  0},
	 };

__align(4) SCREEN_TYPE6_DATA Org_613000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
		{Screen613_str1,       Screen613_str2,  &Admin_var.Product_Name, 0},
		{Current_value_string, 0,              NULL,                    0},
	 };

__align(4) SCREEN_TYPE6_DATA Org_637000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
		{Screen637_str1,       Screen637_str2,  &Admin_var.Password,   1},
		//{Current_value_string, 0,              NULL,                  0},
	 };

__align(4) SCREEN_TYPE6_DATA Org_637200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
		{Screen637_str3,       Screen637_str4,  &Admin_var.New_Password,        1},
		//{Current_value_string, 0,              NULL,                           0},
	 };

__align(4) SCREEN_TYPE6_DATA Org_637300_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
		{Screen637_str3,       Screen637_str5,  &Admin_var.Verify_New_Password, 1},
		//{Current_value_string, 0,              NULL,                           0},
	 };

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/

