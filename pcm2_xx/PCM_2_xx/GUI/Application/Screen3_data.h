/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen3_data.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

#ifndef  SCREEN3_DATA
#define  SCREEN3_DATA

#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Screen_global_ex.h"
/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */



__align(4) SCREEN_TYPE3_DATA Org_212000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen21_str3, NULL,      no_variable,        0},
    {Screen212_str1, 0, Screen212_str2,  &Pcm_var.PCM_web_select, unsigned_char_type, 0},
    {Screen212_str3, 0, Screen212_str4,  &Pcm_var.PCM_web_select, unsigned_char_type, 0},
};
__align(4) SCREEN_TYPE3_DATA Org_213000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen21_str5, NULL,      no_variable,        0},
    {Screen213_str1, 0, Screen213_str2,  &Pcm_var.PCM_connection_select, unsigned_char_type, 0},
    {Screen213_str3, 0, Screen213_str4,  &Pcm_var.PCM_connection_select, unsigned_char_type, 0},
    {Screen213_str5, 0, Screen213_str6,  &Pcm_var.PCM_connection_select, unsigned_char_type, 0},

};
__align(4) SCREEN_TYPE3_DATA Org_221000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen22_str1, NULL,      no_variable,        0},
    {Screen22_str14, 0, Screen22_str16,  &Pcm_var.DHCP_Status, unsigned_char_type, 0},
    {Screen22_str15, 0, Screen22_str16,  &Pcm_var.DHCP_Status, unsigned_char_type, 0},
};
__align(4) SCREEN_TYPE3_DATA Org_241000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen24_str1, NULL,    no_variable,        0},
    {Screen24_str9, 0, Screen24_str11,  &Pcm_var.PPP_Status, unsigned_char_type, 0},
    {Screen24_str10, 0, Screen24_str11,  &Pcm_var.PPP_Status, unsigned_char_type, 0},
};
__align(4) SCREEN_TYPE3_DATA Org_251000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen25_str1, NULL,    no_variable,        0},
    {Screen25_str7, 0, 0,  &Pcm_var.DST_Status, unsigned_char_type, 0},
    {Screen25_str8, 0, 0,  &Pcm_var.DST_Status, unsigned_char_type, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_311000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen3_str0, NULL,    no_variable,        0},
    {Screen31_str3, Screen31_str4, Screen31_str5,  &Pcm_var.Current_Date_Format, unsigned_char_type, 0},
    {Screen31_str6, Screen31_str7, Screen31_str8, &Pcm_var.Current_Date_Format, unsigned_char_type, 0},
};



__align(4) SCREEN_TYPE3_DATA Org_331000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen3_str4, NULL,    no_variable,        0},
    {Screen6412_str1, Screen6412_str2, Screen6412_str3, NULL,  no_variable, 0},
};


__align(4) SCREEN_TYPE3_DATA Org_332000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,               0,               0,               NULL,  no_variable, 0},
    {Screen6413_str1, Screen6413_str2, Screen6413_str3, NULL,  no_variable, 1},
};

__align(4) SCREEN_TYPE3_DATA Org_334000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
	  {0,                0,                0,                    NULL,  no_variable, 0},
    {Screen6415_str1, Screen6415_str2, Screen6415_str3, NULL, no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_332110_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,                 0, 0,                 NULL,  no_variable, 0},
    {Screen641311_str0, 0, Screen641311_str1, NULL,  no_variable, 0},
};

__align(4) SCREEN_TYPE3_DATA Org_332120_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,                 0,                 Screen641312_str1, NULL,  no_variable, 0},
    {Screen641312_str2, 0,                 Screen641312_str3, NULL,  no_variable, 0},
    {Screen641312_str4, Screen641312_str5, Screen641312_str6, NULL,  no_variable, 0},
};
__align(4) SCREEN_TYPE3_DATA Org_340000_data[] __attribute__ ((section ("PCM_GUI_DATA_RAM"))) = {
    {0,              0,              Screen3_str6, NULL,    no_variable,        0},
    {Screen34_str0, Screen34_str1, Screen34_str2, NULL,  no_variable, 0},
};

// __align(4) SCREEN_TYPE3_DATA Org_61A100_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {0,              0,                 0,              NULL, no_variable, 0},
//     {Screen431_str2, Screen61A1_str1, Screen61A1_str2,  &Admin_var.Test_speed_status, unsigned_char_type, 1},
//     {Screen431_str5, Screen61A1_str3, Screen61A1_str4,  &Admin_var.Test_speed_status, unsigned_char_type, 0},
// };

// __align(4) SCREEN_TYPE3_DATA Org_61A200_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {0,              0,                 0,              NULL, no_variable, 0},
//     {Screen431_str2, Screen61A2_str1, Screen61A2_str2,  &Admin_var.Test_load_status, unsigned_char_type, 1},
//     {Screen431_str5, Screen61A2_str3, Screen61A2_str4,  &Admin_var.Test_load_status, unsigned_char_type, 0},
// };


// __align(4) SCREEN_TYPE3_DATA Org_61B000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
//     {0,             0,             0, NULL,
//            no_variable,        0},
//     {Screen61B1_str0, Screen61B1_str1, Screen61B1_str2,  &Misc_var.Language_select,
//            unsigned_char_type, 0},
// 					 ////SKS 02-11-2016 BS-241
//   /*  {Screen61B2_str0, Screen61B2_str1, Screen61B2_str2,  &Misc_var.Language_select,
//            unsigned_char_type, 0},*/
// };

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
