/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen5.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <stdlib.h>
#include <math.h>
#include "Screen_global_ex.h"
#include "rtc.h"
#include "Screen_data_enum.h"
#include "Global_ex.h"
#include "Modbus_Tcp_Master.h"
//#include "Rate_blending_load_ctrl_calc.h"
//#include "Calibration.h"
//#include "MVT.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define TODAY

#define NUMPAD_BUTTON_1_ID         NUMPAD_GUI_ID_USER +  1 //1
#define NUMPAD_BUTTON_2_ID         NUMPAD_GUI_ID_USER +  2 //2
#define NUMPAD_BUTTON_3_ID         NUMPAD_GUI_ID_USER +  3 //3
#define NUMPAD_BUTTON_ESC_ID       NUMPAD_GUI_ID_USER +  4 //4

#define NUMPAD_BUTTON_4_ID         NUMPAD_GUI_ID_USER +  5 //5
#define NUMPAD_BUTTON_5_ID         NUMPAD_GUI_ID_USER +  6 //6
#define NUMPAD_BUTTON_6_ID         NUMPAD_GUI_ID_USER +  7 //7
#define NUMPAD_BUTTON_DEL_ID       NUMPAD_GUI_ID_USER +  8 //8

#define NUMPAD_BUTTON_7_ID         NUMPAD_GUI_ID_USER +  9 //9
#define NUMPAD_BUTTON_8_ID         NUMPAD_GUI_ID_USER +  10 //10
#define NUMPAD_BUTTON_9_ID         NUMPAD_GUI_ID_USER +  11 //11
#define NUMPAD_BUTTON_ENTARROW_ID  NUMPAD_GUI_ID_USER +  12 //12

#define NUMPAD_BUTTON_PS_NEG_ID   NUMPAD_GUI_ID_USER +  13 //13
#define NUMPAD_BUTTON_0_ID        NUMPAD_GUI_ID_USER +  14 //14
#define NUMPAD_BUTTON_DOT_ID      NUMPAD_GUI_ID_USER +  15 //15

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */
/* Boolean variables section */

/* Character variables section */
unsigned char four_g_ip_change_fg;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */
/* Dialog resource of numpad */
const GUI_WIDGET_CREATE_INFO _aDialogNumPad[] = {
    /*  Function                 Text      Id                 Px                     Py                 Dx                    Dy */
    { WINDOW_CreateIndirect,   0,        0,            0,                 1,                 NUMPAD_WINDOW_SIZE_X,  NUMPAD_WINDOW_SIZE_Y-2 },

        //---------------------------------------------------------------ROW 1------------------------------------------------------------------
    { BUTTON_CreateIndirect,   "1",      NUMPAD_BUTTON_1_ID,     NUMPAD_COL_POS_1,      NUMPAD_ROW_POS_1,    NUMPAD_BUTTON_SIZE_X,   NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "2",      NUMPAD_BUTTON_2_ID,     NUMPAD_COL_POS_2,      NUMPAD_ROW_POS_1,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "3",      NUMPAD_BUTTON_3_ID,     NUMPAD_COL_POS_3,      NUMPAD_ROW_POS_1,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect, 		"Esc",      NUMPAD_BUTTON_ESC_ID,     NUMPAD_COL_POS_4,      NUMPAD_ROW_POS_1,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},
    //---------------------------------------------------------------ROW 2------------------------------------------------------------------
    { BUTTON_CreateIndirect,   "4",      NUMPAD_BUTTON_4_ID,       NUMPAD_COL_POS_1,     NUMPAD_ROW_POS_2,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "5",      NUMPAD_BUTTON_5_ID,       NUMPAD_COL_POS_2,     NUMPAD_ROW_POS_2,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "6",      NUMPAD_BUTTON_6_ID,       NUMPAD_COL_POS_3,    NUMPAD_ROW_POS_2,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,  "Del",   NUMPAD_BUTTON_DEL_ID,     NUMPAD_COL_POS_4,    NUMPAD_ROW_POS_2,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    //---------------------------------------------------------------ROW 3------------------------------------------------------------------
    { BUTTON_CreateIndirect,   "7",      NUMPAD_BUTTON_7_ID,       NUMPAD_COL_POS_1,    NUMPAD_ROW_POS_3,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "8",      NUMPAD_BUTTON_8_ID,       NUMPAD_COL_POS_2,    NUMPAD_ROW_POS_3,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "9",      NUMPAD_BUTTON_9_ID,       NUMPAD_COL_POS_3,    NUMPAD_ROW_POS_3,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,     0,     NUMPAD_BUTTON_ENTARROW_ID,  NUMPAD_COL_POS_4,    NUMPAD_ROW_POS_3,    NUMPAD_BUTTON_SIZE_X,   /*NUMPAD_BUTTON_SIZE_Y*/  NUMPAD_DIFF_BUTTON_SIZE_Y},
    //---------------------------------------------------------------ROW 4------------------------------------------------------------------
    { BUTTON_CreateIndirect,   "+/-",   NUMPAD_BUTTON_PS_NEG_ID,   NUMPAD_COL_POS_1,    NUMPAD_ROW_POS_4,      NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   "0",      NUMPAD_BUTTON_0_ID,       NUMPAD_COL_POS_2,    NUMPAD_ROW_POS_4,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { BUTTON_CreateIndirect,   ".",      NUMPAD_BUTTON_DOT_ID,     NUMPAD_COL_POS_3,    NUMPAD_ROW_POS_4,    NUMPAD_BUTTON_SIZE_X,    NUMPAD_BUTTON_SIZE_Y},

    { EDIT_CreateIndirect,     0,        GUI_ID_EDIT0,      EDIT_WINDOW_XPOS_KEYPAD,     EDIT_WINDOW_YPOS,      EDIT_WINDOW_SIZE_X_KEYPAD,  EDIT_WINDOW_SIZE_Y, EDIT_CF_HCENTER | EDIT_CF_VCENTER, 50},
    { EDIT_CreateIndirect,     0,        GUI_ID_EDIT2,      EDIT_UNIT_WINDOW_XPOS,       EDIT_UNIT_WINDOW_YPOS, EDIT_UNIT_WINDOW_SIZE_X,    EDIT_UNIT_WINDOW_SIZE_Y, EDIT_CF_HCENTER | EDIT_CF_VCENTER, 50},

};

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen5(WM_MESSAGE * pMsg);

/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : static void callback_screen5(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen5(WM_MESSAGE * pMsg)
{
    char acBuffer[2], rcvBuffer[ARRAY_SIZE], ip_ptr[6];
    int Id, NCode, sel, Key_entry, xPos = 0, i ,j, num;
    Screen_org_data * new_screen_org;
    WM_HWIN hItem, Edit, Dlg_numpad, Edit_Unit, first_button;
    static char local_ip_buff[5];
    static unsigned int nos_of_dist = 1;
    static int counter = 0, pos = 0, dots = 0, nos_of_digits = 0, sign = 0;
    static SCREEN_TYPE5_DATA * data;
    static const GUI_FONT * OldFont;
		float Idler_distance_old = 0.0;
		float Wheel_diameter_old = 0.0;
		float Analog_Output_1_Setpoint_old = 0.0, Analog_Output_2_Setpoint_old = 0.0;
		float Analog_Output_1_Maxrate_old = 0.0, Analog_Output_2_Maxrate_old = 0.0;
		U8 MBS_TCP_slid_old, MBS_TCP_slid_Negative_Flag = 0 ;
		unsigned int Periodic_log_interval_old = 0;
		int screen_no=0,index=0;
    Dlg_numpad = pMsg->hWin;
    Edit = WM_GetDialogItem(Dlg_numpad, GUI_ID_EDIT0);

    Edit_Unit = WM_GetDialogItem(Dlg_numpad, GUI_ID_EDIT2);

    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);

	  first_button = WM_GetDialogItem(Dlg_numpad, NUMPAD_BUTTON_1_ID);

    switch (pMsg->MsgId)
    {
        case WM_INIT_DIALOG:
            for(i=0; i<sizeof(rcvBuffer); i++)
            {
                rcvBuffer[i] = 0;
            }
            data = GUI_data_nav.Current_screen_info->screen_data;
            EDIT_SetInsertMode(Edit, 1);
            EDIT_SetBkColor(Edit, EDIT_CI_DISABLED, GUI_GRAY);
            EDIT_SetBkColor(Edit, EDIT_CI_ENABLED, GUI_WHITE);
            EDIT_SetFont(Edit, &GUI_FontArial_Unicode_MS16_Bold);
            EDIT_SetTextColor(Edit, EDIT_CI_ENABLED, GUI_BLACK);
            EDIT_SetMaxLen(Edit, 30);
            EDIT_SetFocussable(Edit, 0);
						EDIT_EnableBlink(Edit, 100, 1);
            custom_sprintf(data[sel].data_type, data[sel].Variable1, rcvBuffer, sizeof(rcvBuffer));
            EDIT_SetText(Edit, rcvBuffer);

            EDIT_SetBkColor(Edit_Unit, EDIT_CI_DISABLED, GUI_GRAY);
            EDIT_SetBkColor(Edit_Unit, EDIT_CI_ENABLED, GUI_WHITE);
            EDIT_SetFont(Edit_Unit, &GUI_FontArial_Unicode_MS16_Bold);
            EDIT_SetTextColor(Edit_Unit, EDIT_CI_DISABLED, GUI_BLACK);
            EDIT_SetFocussable(Edit_Unit, 0);
            if(data[sel].Variable1_unit != NULL)
            {
                custom_sprintf(unit, Strings[*data[sel].Variable1_unit].Text, rcvBuffer, sizeof(rcvBuffer));
                EDIT_SetText(Edit_Unit, rcvBuffer);
            }
            else
            {
                EDIT_SetText(Edit_Unit, " ");
            }

            WM_DisableWindow(Edit_Unit);
            WM_DisableWindow(Edit);
            WM_SelectWindow(Dlg_numpad);

            custom_sprintf(data[sel].data_type, data[sel].Variable1, rcvBuffer, sizeof(rcvBuffer));

            hItem = WM_GetDialogItem(Dlg_numpad, NUMPAD_BUTTON_1_ID);
            OldFont = BUTTON_GetFont(hItem);
            for (i = (NUMPAD_GUI_ID_USER + 1); i < (GUI_COUNTOF(_aDialogNumPad) + NUMPAD_GUI_ID_USER); i++)
            {
                hItem = WM_GetDialogItem(Dlg_numpad,i);
                BUTTON_SetFocussable(hItem, 1);                       /* Set all buttons non focussable */
                BUTTON_SetFocusColor (hItem, GUI_RED);
                BUTTON_SetBkColor(hItem,BUTTON_CI_PRESSED, GUI_WHITE);
                BUTTON_SetBkColor(hItem,BUTTON_CI_UNPRESSED, GUI_GRAY);
                BUTTON_SetTextColor(hItem,BUTTON_CI_PRESSED, GUI_BLACK);
                BUTTON_SetTextColor(hItem,BUTTON_CI_UNPRESSED, GUI_BLACK);
                switch (i)
                {
                    case NUMPAD_BUTTON_ENTARROW_ID:
                        BUTTON_SetBitmapEx(hItem, 0, &_bmArrowEnter, 9, 24); /* Set bitmap for arrow right button (unpressed) */
                        BUTTON_SetBitmapEx(hItem, 1, &_bmArrowEnter, 9, 24); /* Set bitmap for arrow right button (pressed) */
                        break;

                }
            }
            counter = pos = dots = nos_of_digits = 0;
            memset(acBuffer, 0, sizeof(acBuffer));
            memset(local_ip_buff, 0, sizeof(local_ip_buff));
            break;

        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            hItem = WM_GetDialogItem(Dlg_numpad, NUMPAD_GUI_ID_USER + Id);

            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_CLICKED:
                            break;
                        case WM_NOTIFICATION_RELEASED:
                            break;
                        case WM_NOTIFICATION_SEL_CHANGED:
                            #ifdef TODAY
                            if ((sel + 1) != GUI_data_nav.Current_screen_info->size_of_screen_data)
                            {
                            #endif
                                custom_sprintf(data[sel].data_type, data[sel].Variable1, rcvBuffer, sizeof(rcvBuffer));
                               EDIT_SetText(Edit, rcvBuffer);
                            #ifdef TODAY
                            }
                            #endif
                            WM_SetFocus(GUI_data_nav.Listbox);
                            break;
                    }
                    break;

                default:
                    switch(NCode)
                    {
                        case WM_NOTIFICATION_GOT_FOCUS:
                            BUTTON_SetFont(pMsg->hWinSrc, &GUI_FontArial_Unicode_MS16_Bold);
                            BUTTON_SetBkColor(pMsg->hWinSrc, BUTTON_CI_UNPRESSED, GUI_LIGHTGRAY);
                            break;

                        case WM_NOTIFICATION_LOST_FOCUS:
                            BUTTON_SetFont(pMsg->hWinSrc, OldFont);
                            BUTTON_SetBkColor(pMsg->hWinSrc, BUTTON_CI_UNPRESSED, GUI_GRAY);
                            break;

                        case WM_NOTIFICATION_CHILD_DELETED:
                            WM_SetFocus(GUI_data_nav.Listbox);
                            GUI_ClearKeyBuffer();
                            LISTBOX_SetSel(GUI_data_nav.Listbox,1);
                            break;

                        case WM_NOTIFICATION_RELEASED:
                            if ((Id >= NUMPAD_GUI_ID_USER) && (Id <= (NUMPAD_GUI_ID_USER + MAX_NUMPAD_BUTTON)))
                            {
                                memset(acBuffer, 0, sizeof(acBuffer));//-today
                                BUTTON_GetText(pMsg->hWinSrc, acBuffer, sizeof(acBuffer)); /* Get the text of the button */
                                if (Id == NUMPAD_BUTTON_DEL_ID)
                                {
                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                    if(counter > 0)
                                    {
                                        counter --;
                                    }
                                    if(nos_of_digits > 0)
                                    {
                                        nos_of_digits--;
                                        if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, IP_ADDR_ORG_NO) == 0) ||
                                           (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SUBNET_MASK_ORG_NO) == 0) ||
                                           (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, GATEWAY_ORG_NO) == 0)||
																					 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, FOUR_G_IP_ADDR_ORG_NO) == 0))
                                        {
                                            switch(nos_of_digits)
                                            {
                                                case 1:
                                                case 2:
                                                    counter = nos_of_digits;
                                                    break;
                                                case 3:
                                                    counter = nos_of_digits;
                                                    dots--;
                                                    break;
                                                case 4:
                                                case 5:
                                                case 6:
                                                    counter = nos_of_digits - 4;
                                                case 7:
                                                    counter = nos_of_digits - 4;
                                                    dots--;
                                                    break;
                                                case 8:
                                                case 9:
                                                case 10:
                                                    counter = nos_of_digits - 8;
                                                    break;
                                                case 11:
                                                    counter = nos_of_digits - 8;
                                                    dots--;
                                                    break;
                                                case 12:
                                                case 13:
                                                case 14:
                                                case 15:
                                                    counter = nos_of_digits - 12;
                                                    break;
                                            }
                                        }
                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0) 
                                    {
                                            switch(nos_of_digits)
                                            {
                                                case 1:
                                                case 2:
                                                    counter = nos_of_digits;
                                                    break;
                                                case 3:
                                                    counter = nos_of_digits;
                                                    dots--;
                                                    break;
                                                case 4:
                                                case 5:
                                                case 6:
                                                    counter = nos_of_digits - 4;
                                                case 7:
                                                    counter = nos_of_digits - 4;
                                                    dots--;
                                                    break;
                                                case 8:
                                                case 9:
                                                case 10:
                                                    counter = nos_of_digits - 8;
                                                    break;
                                                case 11:
                                                    counter = nos_of_digits - 8;
                                                    dots--;
                                                    break;
                                                case 12:
                                                case 13:
                                                case 14:
																										counter = nos_of_digits - 12;
																										break;
                                                case 15:
                                                    counter = nos_of_digits - 12;
																										dots--;
                                                    break;
                                                case 16:
                                                case 17:
                                                case 18:
																										counter = nos_of_digits - 12;
																										break;
                                                case 19:
                                                    counter = nos_of_digits - 12;
																										dots--;
                                                    break;	
                                                case 20:
                                                case 21:
                                                case 22:
																								case 23:	
																										counter = nos_of_digits - 12;
																										break;
																								
                                            }																			
																		}																			
                                        else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DATE_ORG_NO) == 0)
                                        {
                                            switch(nos_of_digits)
                                            {
                                                case 2:
                                                case 4:
                                                    dots--;
                                                    break;
                                            }
                                        }
                                        else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TIME_ORG_NO) == 0)
                                        {
                                            switch(nos_of_digits)
                                            {
                                                case 2:
                                                    dots--;
                                                    break;
                                            }
                                        }
                                    }
                                }
                                else if (Id == NUMPAD_BUTTON_PS_NEG_ID)
                                {
                                    xPos = EDIT_GetCursorCharPos(Edit);

                                    if(xPos < 2)
                                    {
                                        if((sign == 0)&&(xPos == 0))
                                        {
                                            if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MIN_BELT_SPEED_ORG_NO))
                                            {
                                                EDIT_AddKey(Edit, '-');
                                                sign++;
                                            }
                                        }
                                        else if((sign == 1)&&(xPos == 1))
                                        {
                                            EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                            EDIT_SetCursorAtChar(Edit, (xPos - 1));
                                            sign = 0;
                                        }
                                    }
                                }
                                else if (Id == NUMPAD_BUTTON_ENTARROW_ID)
                                {
																    Current_time.RTC_Hour = Pcm_var.Config_Time.Hr;
																	  Current_time.RTC_Min  = Pcm_var.Config_Time.Min; 															  
                                        EDIT_GetText(Edit, rcvBuffer, sizeof(rcvBuffer));
																					
                                        if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, IP_ADDR_ORG_NO) == 0) ||
                                           (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SUBNET_MASK_ORG_NO) == 0) ||
                                           (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, GATEWAY_ORG_NO) == 0)||
																					 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, FOUR_G_IP_ADDR_ORG_NO) == 0))
																				
                                        {
                                            pos = counter = 0;
                                            //store the IP address in the proper format
                                            while(counter < 4)
                                            {
                                                strncpy(ip_ptr, &rcvBuffer[pos], 3);
                                                *ip_ptr = '\0';
                                                num = atoi(ip_ptr);

                                                if((num < 0) || (num > 255))
                                                {
                                                    strncpy(&rcvBuffer[pos], "255", 3);
                                                }
                                                pos += 4;
                                                counter++;
                                            }
                                        }
                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0) 
                                    {
                                            pos = counter = 0;
                                            //store the IP address in the proper format
                                            while(counter < 6)
                                            {
                                                strncpy(ip_ptr, &rcvBuffer[pos], 3);
                                                *ip_ptr = '\0';
                                                num = atoi(ip_ptr);

                                                if((num < 0) || (num > 255))
                                                {
                                                    strncpy(&rcvBuffer[pos], "255", 3);
                                                }
                                                pos += 4;
                                                counter++;
                                            }																			
																		}																			
                                        else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DATE_ORG_NO) == 0)
                                        {
                                            format_date(&rcvBuffer[0]);
                                        }
                                        else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TIME_ORG_NO) == 0)
                                        {
                                            format_time(&rcvBuffer[0]);
                                        }
			
																				/* Added on 4th Nov 2015 for BS-92 fix */
																				else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MODBUS_SLAVEID_ORG_NO) == 0))
																				{
																					MBS_TCP_slid_old = Admin_var.MBS_TCP_slid;
																					if(rcvBuffer[0] == 0x2D)		//Check negative sign
																					{
																						MBS_TCP_slid_Negative_Flag = 1;		//Set flag
																						rcvBuffer[0] = 0;									//Do not accept negative value
																					}
																				}
																				/* Added on 4th Nov 2015 for BS-92 fix */
																				else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RUN_LOG_TIME_MODIFY_ORG_NO) == 0))
																				{
																					Periodic_log_interval_old = Setup_device_var.Periodic_log_interval;
																				}

                                        custom_sscanf(data[sel].data_type, data[sel].Variable1, rcvBuffer);

																				
                                        if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, IP_ADDR_ORG_NO) == 0) ||
                                                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SUBNET_MASK_ORG_NO) == 0) ||
                                                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, GATEWAY_ORG_NO) == 0))
                                        {
                                            change_ip_address();
                                        }
                                        if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, FOUR_G_IP_ADDR_ORG_NO) == 0) )
                                        {
                                            four_g_ip_change_fg = 1;
                                        }																				

                                        else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DATE_ORG_NO) == 0)
                                        {
                                            Set_rtc_date(Pcm_var.Config_Date);
																					  Current_time = RTCGetTime();
                                        }
                                        else if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TIME_ORG_NO) == 0)
                                        {
                                            Set_rtc_time(Pcm_var.Config_Time);
																					  Current_time = RTCGetTime();
                                        }
                                        else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NETWORK_ADDR_ORG_NO) == 0)
                                        {
                                            #ifdef MODBUS_RTU_INTEGRATOR
                                            if (prev_network_address != Setup_device_var.Network_Addr)
                                            {
                                                switch_integrator_master_slave_tasks();
                                            }
                                            #endif
                                        }

																				/* Added on 4th Nov 2015 for BS-92 fix */
																				else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MODBUS_SLAVEID_ORG_NO) == 0))
																				{
																					if((Admin_var.MBS_TCP_slid <= 0) || (Admin_var.MBS_TCP_slid > 255) || (MBS_TCP_slid_Negative_Flag == 1))
																						Admin_var.MBS_TCP_slid = MBS_TCP_slid_old;
																				}
																				/* Added on 4th Nov 2015 for BS-92 fix */
																				else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RUN_LOG_TIME_MODIFY_ORG_NO) == 0))
																				{
																					if((Setup_device_var.Periodic_log_interval <= 0) || (Setup_device_var.Periodic_log_interval > 1440)) 
																						Setup_device_var.Periodic_log_interval = Periodic_log_interval_old;
																				}
																				/* Added on 30th Nov 2015 for BS-111 fix */
																				//by megha on 11/5/2017 for log edit parameters
																		screen_no = atoi(GUI_data_nav.Current_screen_info->Screen_org_no);
																				
																				switch(screen_no)
																				{
																					case 211000:
																												parameter_code = PCM_ID_CODE;
																												break;
																					case 222000:
																												parameter_code = IP_ADDR_CODE;
																												break;	
																					case 223000:
																												parameter_code = SUBNET_MASK_CODE;
																												break;
																					case 224000:
																												parameter_code = GATEWAY_CODE;
																												break;
																					case 226000:
																												parameter_code = PRI_DNS_CODE;
																												break;
																					case 227000:
																												parameter_code = SEC_DNS_CODE;
																												break;
																					case 312000:
																												parameter_code = DATE_CODE;
																												break;
																					case 321000:
																												parameter_code = TIME_CODE;
																												break;
																					case 252000:
																												parameter_code = GMT_CODE;
																												break;																				
																													
																				}																				
																				log_edit_para();
                                        GUI_data_nav.GUI_structure_backup = 1;
                                    WM_SelectWindow(Dlg_numpad);
                                    WM_DisableWindow(Edit);

                                    if ((GUI_data_nav.Current_screen_info->Screen_org_no[0] == '2') &&
                                        (GUI_data_nav.Current_screen_info->Screen_org_no[1] == '8'))
                                    {
                                        if (nos_of_dist <= (Scale_setup_var.Number_of_idlers))
                                        {
                                            GUI_data_nav.Change = 1;
                                            for (i = 0; i < 6; i++)
                                            {
//                                                GUI_data_nav.New_screen_num[i] = Org_280000_children[nos_of_dist].Screen_org_no[i];
                                            }
                                            nos_of_dist++;
                                        }
                                        else
                                        {
                                            nos_of_dist = 1;
                                            if ((!GUI_data_nav.In_wizard) &&
                                                (GUI_data_nav.Current_screen_info->size_of_screen_data == 2))
                                            {
                                                go_back_one_level();
                                            }//if (!in_wizard)
                                            else if ((GUI_data_nav.In_wizard) &&
                                                (GUI_data_nav.Current_screen_info->size_of_screen_data == 2))
                                            {
                                                GUI_data_nav.Key_press = 1;
                                            }
                                        }
                                    }//if '2' and '8'
                                    else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,CONV_ANGLE_GREATER_30) == 0) &&
                                            (Scale_setup_var.Conveyor_angle > 30))
                                    {
                                        GUI_data_nav.New_screen_num[0] = '2';
                                        GUI_data_nav.New_screen_num[1] = '7';
                                        GUI_data_nav.New_screen_num[2] = '2';
                                        GUI_data_nav.New_screen_num[3] = '1';
                                        GUI_data_nav.New_screen_num[4] = '0';
                                        GUI_data_nav.New_screen_num[5] = '0';
                                        GUI_data_nav.Change = 1;
                                    }
                                    else
                                    {
                                        //just in case many values have to be entered (i.e listbox has more than 2 items)
                                        //then go back to the previous screen for a screen which is not in a wizard and
                                        //go to the next screen if in a wizard
                                        if ((!GUI_data_nav.In_wizard) &&
                                            (GUI_data_nav.Current_screen_info->size_of_screen_data == 2))
                                        {
                                            go_back_one_level();
                                        }//if (!in_wizard)
                                        else if ((GUI_data_nav.In_wizard) &&
                                                (GUI_data_nav.Current_screen_info->size_of_screen_data == 2))
                                        {
                                             GUI_data_nav.Key_press = 1;
                                        }
                                    }
                                    WM_SetFocus(GUI_data_nav.Listbox);
                                }//if enter
                                else if (Id == NUMPAD_BUTTON_ESC_ID)
                                {
                                    if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, IP_ADDR_ORG_NO) == 0) ||
                                       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SUBNET_MASK_ORG_NO) == 0) ||
                                       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, GATEWAY_ORG_NO) == 0)||
																				(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PRIDNS_ORG_NO) == 0)||
																				(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SECDNS_ORG_NO) == 0)||
 																			 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, FOUR_G_IP_ADDR_ORG_NO) == 0))
																		
                                    {
																			counter = 0;	
																			nos_of_digits = 0;
																			dots = 0;																			
																		}
                                    custom_sprintf(data[sel].data_type, data[sel].Variable1, rcvBuffer, sizeof(rcvBuffer));
                                    EDIT_SetText(Edit, rcvBuffer);
                                    GUI_data_nav.Change = 0;
                                    sign = 0;
                                    WM_DisableWindow(Dlg_numpad);
                                    WM_DisableWindow(Edit);
                                    WM_SetFocus(GUI_data_nav.Listbox);
																	
                                } //if ESC
                                else
                                {
                                    if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, IP_ADDR_ORG_NO) == 0) ||
                                       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SUBNET_MASK_ORG_NO) == 0) ||
                                       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, GATEWAY_ORG_NO) == 0)||
																				(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PRIDNS_ORG_NO) == 0)||
																				(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SECDNS_ORG_NO) == 0)||
																			 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, FOUR_G_IP_ADDR_ORG_NO) == 0))

                                    {
                                        nos_of_digits++;
                                        counter++;
                                        switch(counter)
                                        {
                                            case 1:
                                                if(acBuffer[0] >= '3')
                                                {
                                                    acBuffer[0] = '2';
                                                }
                                                local_ip_buff[0] = acBuffer[0];
                                                break;

                                            case 2:
                                                local_ip_buff[1] = acBuffer[0];
                                                break;

                                            case 3:
                                                if((local_ip_buff[0] == '2') &&
                                                    (local_ip_buff[1] >= '5') &&
                                                    (acBuffer[0] > '5'))
                                                {
                                                    acBuffer[0] = '5';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '5');
                                                }
                                                break;
                                        }
                                        if ((Id == NUMPAD_BUTTON_DOT_ID) &&
                                             (nos_of_digits <= 12))
                                        {
                                           //one number and dot entered
                                           if (counter == 2)
                                           {
                                              EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                              EDIT_AddKey(Edit, '0');
                                              EDIT_AddKey(Edit, '0');
                                              EDIT_AddKey(Edit, local_ip_buff[0]);
                                              nos_of_digits += 2;
                                           }
                                           //two numbers and dot entered
                                           else if (counter == 3)
                                           {
                                              EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                              EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                              EDIT_AddKey(Edit, '0');
                                              EDIT_AddKey(Edit, local_ip_buff[0]);
                                              EDIT_AddKey(Edit, local_ip_buff[1]);
                                              nos_of_digits++;
                                           }
                                           counter = 0;
                                           dots++;
                                           memset(local_ip_buff, 0, sizeof(local_ip_buff));
                                           EDIT_AddKey(Edit, acBuffer[0]);
                                        }
                                        else if ((nos_of_digits <= 15) &&
                                            (Id != NUMPAD_BUTTON_DOT_ID))
                                        {
                                            EDIT_AddKey(Edit, acBuffer[0]);
                                        }
                                        else if ((nos_of_digits <= 15) &&
                                            (Id == NUMPAD_BUTTON_DOT_ID))
                                        {
                                            nos_of_digits--;
                                            counter--;
                                        }
                                        if(((nos_of_digits == 3) || (nos_of_digits == 7) || (nos_of_digits == 11))
                                            && (dots < 3))
                                        {
                                            EDIT_AddKey(Edit, '.');
                                            counter = 0;
                                            dots++;
                                            nos_of_digits++;
                                            memset(local_ip_buff, 0, sizeof(local_ip_buff));
                                        }
																				if(nos_of_digits>15)
																					nos_of_digits = 15;
                                    }


                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MAC_ID_ORG_NO) == 0) 
                                    {
                                        nos_of_digits++;
                                        counter++;
                                        switch(counter)
                                        {
                                            case 1:
                                                if(acBuffer[0] >= '3')
                                                {
                                                    acBuffer[0] = '2';
                                                }
                                                local_ip_buff[0] = acBuffer[0];
                                                break;

                                            case 2:
                                                local_ip_buff[1] = acBuffer[0];
                                                break;

                                            case 3:
                                                if((local_ip_buff[0] == '2') &&
                                                    (local_ip_buff[1] >= '5') &&
                                                    (acBuffer[0] > '5'))
                                                {
                                                    acBuffer[0] = '5';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '5');
                                                }
                                                break;
                                        }
                                        if ((Id == NUMPAD_BUTTON_DOT_ID) &&
                                             (nos_of_digits <= 18))
                                        {
                                           //one number and dot entered
                                           if (counter == 2)
                                           {
                                              EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                              EDIT_AddKey(Edit, '0');
                                              EDIT_AddKey(Edit, '0');
                                              EDIT_AddKey(Edit, local_ip_buff[0]);
                                              nos_of_digits += 2;
                                           }
                                           //two numbers and dot entered
                                           else if (counter == 3)
                                           {
                                              EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                              EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                              EDIT_AddKey(Edit, '0');
                                              EDIT_AddKey(Edit, local_ip_buff[0]);
                                              EDIT_AddKey(Edit, local_ip_buff[1]);
                                              nos_of_digits++;
                                           }
                                           counter = 0;
                                           dots++;
                                           memset(local_ip_buff, 0, sizeof(local_ip_buff));
                                           EDIT_AddKey(Edit, acBuffer[0]);
                                        }
                                        else if ((nos_of_digits <= 23) &&
                                            (Id != NUMPAD_BUTTON_DOT_ID))
                                        {
                                            EDIT_AddKey(Edit, acBuffer[0]);
                                        }
                                        else if ((nos_of_digits <= 23) &&
                                            (Id == NUMPAD_BUTTON_DOT_ID))
                                        {
                                            nos_of_digits--;
                                            counter--;
                                        }
                                        if(((nos_of_digits == 3) || (nos_of_digits == 7) || (nos_of_digits == 11)||(nos_of_digits == 15)||((nos_of_digits == 19)))
                                            && (dots < 5))
                                        {
                                            EDIT_AddKey(Edit, ':');
                                            counter = 0;
                                            dots++;
                                            nos_of_digits++;
                                            memset(local_ip_buff, 0, sizeof(local_ip_buff));
                                        }
																				if(nos_of_digits>23)
																					nos_of_digits = 23;
                                    }																		
																		
                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DATE_ORG_NO) == 0)
                                    {

                                        nos_of_digits++;
                                        if(nos_of_digits == 2)
                                        {
                                            /*MM/DD/YYYY format*/
                                            if(Pcm_var.Current_Date_Format == MMDDYYYY)
                                            {
                                                if((local_ip_buff[0] >= '1') &&
                                                    (acBuffer[0] >= '2'))
                                                {
                                                    acBuffer[0] = '2';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '1');
                                                }
                                            }
                                            else
                                            {
                                                if((local_ip_buff[0] >= '3') &&
                                                        (acBuffer[0] > '1'))
                                                {
                                                    acBuffer[0] = '1';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '3');
                                                }
                                            }
                                        }
                                        else if(nos_of_digits == 5)
                                        {
                                            /*MM/DD/YYYY format*/
                                            if(Pcm_var.Current_Date_Format == MMDDYYYY)
                                            {
                                                if((local_ip_buff[0] >= '3') &&
                                                        (acBuffer[0] > '1'))
                                                {
                                                    acBuffer[0] = '1';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '3');
                                                }
                                            }
                                            else
                                            {
                                                if((local_ip_buff[0] >= '1') &&
                                                        (acBuffer[0] >= '2'))
                                                {
                                                    acBuffer[0] = '2';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '1');
                                                }
                                            }
                                        }
                                        else
                                        {
                                            local_ip_buff[0] = acBuffer[0];
                                        }
                                        if(nos_of_digits <= 10)
                                        {
                                            EDIT_AddKey(Edit, acBuffer[0]);
                                        }
                                        if(((nos_of_digits == 2) || (nos_of_digits == 5)) && (dots < 2))
                                        {
                                            EDIT_AddKey(Edit, '/');
                                            dots++;
                                            nos_of_digits++;
                                        }
																				if(nos_of_digits>10)
																					nos_of_digits = 10;
                                    }
                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TIME_ORG_NO) == 0)
                                    {
                                        nos_of_digits++;
																			  if(nos_of_digits == 1) // rtc testing
                                        {
                                            /*12 Hour Format*/
                                            if( Pcm_var.Current_Time_Format == TWELVE_HR)
                                            {
                                                if(acBuffer[0] > '1') 
                                                {
                                                    acBuffer[0] = '1';
                                                }
                                            }
                                            else
                                            {
                                                if(acBuffer[0] > '2')
                                                {
                                                    acBuffer[0] = '2';
                                                }
                                            }
                                        } // rtc testing end
																				
                                        if(nos_of_digits == 2)
                                        {
                                            /*12 Hour Format*/
                                            if( Pcm_var.Current_Time_Format == TWELVE_HR)
                                            {
                                                //if((local_ip_buff[0] >= '1' || local_ip_buff[0] == '0') &&
                                                //        ((acBuffer[0] > '2') || (acBuffer[0] == '0'))) // rtc testing
																							if((local_ip_buff[0] >= '1' && acBuffer[0] > '2') || 
																								( local_ip_buff[0] == '0' && acBuffer[0] == '0'))
                                                {
                                                    acBuffer[0] = '2';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '1');
                                                }

                                            }
                                            else
                                            {
                                                if((local_ip_buff[0] >= '2') &&
                                                   (acBuffer[0] > '3'))
                                                {
                                                    acBuffer[0] = '3';
                                                    EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                    EDIT_AddKey(Edit, '2');
                                                }
                                            }
                                        }
                                        else if(nos_of_digits == 5)
                                        {
                                            if((local_ip_buff[0] == '6') &&
                                               (acBuffer[0] >= '0'))
                                            {
                                                acBuffer[0] = '9';
                                                EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                EDIT_AddKey(Edit, '5');
                                            }
                                        }
                                        else
                                        {
                                            local_ip_buff[0] = acBuffer[0];
                                        }
                                        
																				 if(nos_of_digits <= 5)
                                        {
                                            EDIT_AddKey(Edit, acBuffer[0]);
                                        }
                                        if(((nos_of_digits == 2) || (nos_of_digits == 5)) && (dots < 1))
                                        {
                                            EDIT_AddKey(Edit, ':');
                                            dots++;
                                            nos_of_digits++;
                                        }
																				if(nos_of_digits>5)
																					nos_of_digits = 5;
                                    }
                                    else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NETWORK_ADDR_ORG_NO) == 0))// ||(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, NUM_OF_SLAVES_ORG_NO) == 0))                                            
                                    {
                                        nos_of_digits++;
                                        counter++;
                                        if(counter == 1)
                                        {
                                            local_ip_buff[0] = acBuffer[0];
                                        }
                                        if(counter == 2)
                                        {
                                            if((local_ip_buff[0] >= '3') &&
                                               (acBuffer[0] > '2'))
                                            {
                                                acBuffer[0] = '2';
                                                EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                EDIT_AddKey(Edit, '3');
                                            }
                                        }
                                        if(nos_of_digits <= 2)
                                        {
                                            EDIT_AddKey(Edit, acBuffer[0]);
                                        }
																				if(nos_of_digits > 2)
																					nos_of_digits = 2;
                                    }
                                    else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, MODBUS_SLAVEID_ORG_NO) == 0))
                                    {
                                        nos_of_digits++;
                                        counter++;
                                        if(counter == 1)
                                        {
																					if(acBuffer[0] == '0')
                                          {														
                                            EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);																																										
																					}
																					local_ip_buff[0] = acBuffer[0];
                                        }
                                        if(counter == 2)
                                        {
																						local_ip_buff[1] = acBuffer[0];
                                        }
																				if(counter == 3)
																				{
																						local_ip_buff[2] = acBuffer[0];
																					if(local_ip_buff[0]>'2')
																					{
																						 acBuffer[0] = '5';																						
                                             EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                             EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);	
                                             EDIT_AddKey(Edit, '2');
                                             EDIT_AddKey(Edit, '5');
																					}
																					else if(local_ip_buff[0]>='2' && local_ip_buff[1]>'5')
																					{
																						 acBuffer[0] = '5';
      																			 EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                             EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);	
                                             EDIT_AddKey(Edit, '2');
                                             EDIT_AddKey(Edit, '5');
																					}
																					else if(local_ip_buff[0]>='2' && local_ip_buff[1] >= '5' && acBuffer[0] > '5')
																					{
																						 acBuffer[0] = '5';
                                             EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                             EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);	
                                             EDIT_AddKey(Edit, '2');
                                             EDIT_AddKey(Edit, '5');
																					}
																				}
                                        if(nos_of_digits <= 3)
                                        {
                                            EDIT_AddKey(Edit, acBuffer[0]);
                                        }
																				if(nos_of_digits > 3)
																					nos_of_digits = 3;
                                    }
                                    else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, SCBD_ALTN_SELECT_ORG_NO) == 0)
                                    {
                                        nos_of_digits++;
                                        counter++;
                                        if(counter == 1)
                                        {
                                            local_ip_buff[0] = acBuffer[0];
                                        }
                                        if(counter == 2)
                                        {
                                            if(local_ip_buff[0] > '5')
                                            {
                                                acBuffer[0] = '9';
                                                EDIT_AddKey(Edit, GUI_KEY_BACKSPACE);
                                                EDIT_AddKey(Edit, '5');
                                            }
                                        }
                                        if(nos_of_digits <= 2)
                                        {
                                            EDIT_AddKey(Edit, acBuffer[0]);
                                        }
																				if(nos_of_digits>2)
																					nos_of_digits = 2;
                                    }
									
                                    else
                                    {
                                        EDIT_AddKey(Edit, acBuffer[0]);
                                    }
                                }
                            }
                            break;
                    }//switch(NCode)
                break;
            }
            break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_LEFT:
                            ;
                            break;

                        case GUI_KEY_RIGHT:
                            ;
                            break;

                        case GUI_KEY_RIGHT_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;

                        case GUI_KEY_ENTER:
                            #ifdef TODAY
												    if (GUI_data_nav.Current_screen_info->screens != NULL)
														{   
																 //GUI_data_nav.Change = 1;
																  for (i = 0; i < 6; i++)
                                  {
                                    GUI_data_nav.New_screen_num[i] = GUI_data_nav.Current_screen_info->screens->Next->number[i];
                                  }
														}
                            if ((sel + 1) == GUI_data_nav.Current_screen_info->size_of_screen_data)
                            {
                                if ((GUI_data_nav.Current_screen_info->Screen_org_no[0] == '2') &&
                                    (GUI_data_nav.Current_screen_info->Screen_org_no[1] == '8'))
                                {
                                    if (nos_of_dist <= (Scale_setup_var.Number_of_idlers))
                                    {
                                        GUI_data_nav.Change = 1;
                                        for (i = 0; i < 6; i++)
                                        {
//                                            GUI_data_nav.New_screen_num[i] = Org_280000_children[nos_of_dist].Screen_org_no[i];
                                        }
                                        //GUI_data_nav.New_screen_num[6] = '\0';
                                        nos_of_dist++;
                                    }
                                    else
                                    {
                                        nos_of_dist = 1;
                                        GUI_data_nav.Child_present = 0;
                                        if (!GUI_data_nav.In_wizard)
                                        {
                                            go_back_one_level();
                                        }//if (!in_wizard)
                                        else if (GUI_data_nav.In_wizard)
                                        {
                                             GUI_data_nav.Key_press = 1;
                                        }
                                    }
                                }//if '2' and '8'
                                else if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,CONV_ANGLE_GREATER_30) == 0) &&
                                        (Scale_setup_var.Conveyor_angle > 30))
                                {
                                    GUI_data_nav.New_screen_num[0] = '2';
                                    GUI_data_nav.New_screen_num[1] = '7';
                                    GUI_data_nav.New_screen_num[2] = '1';
                                    GUI_data_nav.New_screen_num[3] = '0';
                                    GUI_data_nav.New_screen_num[4] = '0';
                                    GUI_data_nav.New_screen_num[5] = '0';
                                    //GUI_data_nav.New_screen_num[6] = '\0';
                                    GUI_data_nav.Change = 1;
                                }
                                else
                                {
                                    if (!GUI_data_nav.In_wizard)
                                    {
                                        go_back_one_level();
                                    }//if (!in_wizard)
                                    else if (GUI_data_nav.In_wizard)
                                    {
                                         GUI_data_nav.Key_press = 1;
                                    }
                                }
                            }
                            else
                            {
                            #endif
                                if (data[sel].screen_an_option != 0)
                                {
                                    new_screen_org = GUI_data_nav.Current_screen_info->child;
                                    new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
                                    for (i = 0; i < 6; i++)
                                    {
                                        GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
                                    }
                                    //GUI_data_nav.New_screen_num[6] = '\0';
                                    GUI_data_nav.Change = 1;
                                    GUI_data_nav.Child_present = 1;
                                }
                                else
                                {
                                    GUI_data_nav.Child_present = 0;
                                    if((!security_bit_set()) || (password_struct.password_entered))
                                    {
                                        EDIT_SetText(Edit, "");
                                        WM_SetFocus(GUI_data_nav.Numpad);
                                        WM_SelectWindow(Dlg_numpad);
										                    WM_SetFocus(first_button);
                                        WM_EnableWindow(Edit);
                                        GUI_data_nav.Child_present = 0;

                                    }
                                    else
                                    {
                                        //create the password screen, store the current screen org number to come back to it
                                        //in case the password entered is correct
                                        GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info;
                                        //strcpy(GUI_data_nav.Screen_before_pw_org_num->Screen_org_no, GUI_data_nav.Current_screen_info->Screen_org_no);
                                        GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
                                        password_struct.password_to_be_entered = 1;
                                    }
                                }
                            #ifdef TODAY
                            }
                            #endif
                            /*if((Calib_struct.Cal_status_flag == CLEAR_CAL_FLAG) && (GUI_data_nav.Percent_complete == 0))
														{														
																if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no,MATERIAL_TEST_AUTO_START_ORG_NO) == 0)
                                {
                                    Calib_struct.Cal_status_flag = START_MATERIAL_CAL;
																		//log_data_populate(periodic_mat_log);
                                }
														}*/
                            break;
                            case GUI_KEY_BACK:

																	go_back_one_level();

//                                 if(GUI_data_nav.In_wizard)
//                                 {
//                                     GUI_data_nav.Back_one_level = GO_BACK_FLAG;
//                                 }
                                break;
                    }
                    break;

                case ID_MULTI_TEXT_1:
                    ;
                    break;

                default:
                    switch (Key_entry)
                    {
                        case GUI_KEY_UP:
                            if ((Id >= NUMPAD_GUI_ID_USER) && (Id <= (NUMPAD_GUI_ID_USER + MAX_NUMPAD_BUTTON)))
                            {
                                for (i = 0; i < NUMPAD_MAX_NUMBER_OF_COL; i++)
                                {
                                    if((Id == (NUMPAD_GUI_ID_USER + NUMPAD_MAX_NUMBER_OF_COL)) && (i==0))
                                    {
                                        //skip one row
                                        for(j=0; j < NUMPAD_MAX_NUMBER_OF_COL;j++)
                                        {
                                            GUI_SendKeyMsg(GUI_KEY_BACKTAB,1);
                                        }
                                    }
                                    if((Id <= (NUMPAD_GUI_ID_USER + NUMPAD_MAX_NUMBER_OF_COL)) && (i==0))
                                    {
                                        i= i + 1;
                                    }

                                    GUI_SendKeyMsg(GUI_KEY_BACKTAB,1);
                                }
                            }
                            break;

                        case GUI_KEY_LEFT:
                            GUI_SendKeyMsg(GUI_KEY_BACKTAB,1);
                            break;

                        case GUI_KEY_LEFT_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_DOWN:
                            if ((Id >= NUMPAD_GUI_ID_USER) && (Id <= (NUMPAD_GUI_ID_USER + MAX_NUMPAD_BUTTON)))
                            {
                                for (i = 0; i < NUMPAD_MAX_NUMBER_OF_COL; i++)
                                {
                                    if((Id == (NUMPAD_GUI_ID_USER + (MAX_NUMPAD_BUTTON - (NUMPAD_MAX_NUMBER_OF_COL - 1)))) && (i==0))
                                    {
                                        //skip one row
                                        for(j=0; j < NUMPAD_MAX_NUMBER_OF_COL;j++)
                                        {
                                            GUI_SendKeyMsg(GUI_KEY_TAB,1);
                                        }
                                    }
                                    if((Id >= (NUMPAD_GUI_ID_USER + (MAX_NUMPAD_BUTTON - (NUMPAD_MAX_NUMBER_OF_COL - 1)))) && (i==0))
                                    {
                                        i= i + 1;
                                    }
                                    GUI_SendKeyMsg(GUI_KEY_TAB,1);
                                }
                            }
                            break;

                        case GUI_KEY_RIGHT:
                            GUI_SendKeyMsg(GUI_KEY_TAB,1);
                            break;

                        case GUI_KEY_ENTER:
                            break;

                        case GUI_KEY_BACK:
                            custom_sprintf(data[sel].data_type, data[sel].Variable1, rcvBuffer, sizeof(rcvBuffer));
                            EDIT_SetText(Edit, rcvBuffer);
                            WM_SelectWindow(Dlg_numpad);
                            WM_DisableWindow(Edit);
                            WM_SetFocus(GUI_data_nav.Listbox);
                            counter = pos = dots = nos_of_digits = 0;
                            memset(acBuffer, 0, sizeof(acBuffer));
                            memset(local_ip_buff, 0, sizeof(local_ip_buff));
                            break;
                    }
                    break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen5(SCREEN_TYPE5_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox + multiedit screen and sets the default settings
*                            : of the listbox and the multiedit wigdets. Also the number pad
*                            : screen is drawn and the default button settings are configured.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen5(SCREEN_TYPE5_DATA *data, int count)
{
    int i;

    GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX_SIZE_X, LISTBOX_SIZE_Y,
                                             WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
    for (i = 0; i < count; i++)
    {
        LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
    }

    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
    LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
    LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
    LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
    LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
    LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);
    LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);
    GUI_data_nav.Multi_text = MULTIEDIT_CreateEx(MULTI_EDIT_START, MULTI_WIN_OFFSET,
                                     MULTI_EDIT_SIZE_X, MULTI_EDIT_SIZE_Y,
                                     WM_HBKWIN, WM_CF_SHOW, 0, ID_MULTI_TEXT_1, 100, NULL);
		MULTIEDIT_EnableBlink(GUI_data_nav.Multi_text, 100, 1);  // cursor testing
    MULTIEDIT_SetFont(GUI_data_nav.Multi_text, &GUI_FontArial_Unicode_MS16_Bold);
    MULTIEDIT_SetBkColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_BLACK);
    MULTIEDIT_SetTextColor(GUI_data_nav.Multi_text, MULTIEDIT_CI_READONLY, GUI_WHITE);
    MULTIEDIT_SetWrapWord(GUI_data_nav.Multi_text);
    MULTIEDIT_SetReadOnly(GUI_data_nav.Multi_text, 1);
    GUI_data_nav.Numpad = GUI_CreateDialogBox(_aDialogNumPad, GUI_COUNTOF(_aDialogNumPad),
                                              callback_screen5, GUI_data_nav.Multi_text, 0, 0);
    WINDOW_SetBkColor(GUI_data_nav.Numpad, GUI_BLACK);
    WM_SetCallback(WM_HBKWIN, callback_screen5);
    WM_SetFocus(GUI_data_nav.Listbox);
    if((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DATE_ORG_NO) == 0) ||
       (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TIME_ORG_NO) == 0))
    {
        Pcm_var.Config_Time.Sec = Current_time.RTC_Sec;
        Pcm_var.Config_Time.Min = Current_time.RTC_Min;
        Pcm_var.Config_Time.Hr = Current_time.RTC_Hour;
        Pcm_var.Config_Date.Day = Current_time.RTC_Mday;
        Pcm_var.Config_Date.Mon = Current_time.RTC_Mon;
        Pcm_var.Config_Date.Year = Current_time.RTC_Year;
    }
	LISTBOX_SetSel(GUI_data_nav.Listbox, count);
    return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
