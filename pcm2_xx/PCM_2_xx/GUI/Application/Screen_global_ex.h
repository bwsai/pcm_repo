/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen_global_ex.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012,  5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef  SCREEN_GLOBAL_EX
#define  SCREEN_GLOBAL_EX

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../GUI/DIALOG.h"
#include "../GUI/TEXT.h"
#include "../GUI/ICONVIEW.h"
#include "Screen_structure.h"
#include "Screen_data_var.h"

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
#define DISP_2ND_PARAMETER_POS_X1							250
#define DISP_LINE1_POS_Y0											(WINDOW_HEADER_TEXT1_POS_Y0 + 20)	
#define DISP_LINE2_POS_Y0											(DISP_LINE1_POS_Y0 + 20)
#define DISP_LINE3_POS_Y0											(DISP_LINE2_POS_Y0 + 20)
//#define DISP_LINE4_POS_Y0											(DISP_LINE3_POS_Y0 + 20)
#define DISP_LINE5_POS_Y0											(DISP_LINE3_POS_Y0 + 20)
#define DISP_LINE6_POS_Y0											(DISP_LINE5_POS_Y0 + 20)
#define DISP_LINE7_POS_Y0											(DISP_LINE6_POS_Y0 + 20)
#define DISP_LINE8_POS_Y0											(DISP_LINE7_POS_Y0 + 20)
#define DISP_LINE9_POS_Y0											(DISP_LINE8_POS_Y0 + 20)
#define DISP_LINE10_POS_Y0											(DISP_LINE9_POS_Y0 + 20)
#define DISP_2ND_PARA_POS_X											70
#define DISP_3RD_PARA_POS_X											170
#define DISP_4TH_PARA_POS_X											210
#define DISP_UNIT_POS_X													280
#define DISP_5TH_PARA_POS_X											360
//Draw Outer rectangle
#define WINDOW_RECT_POS_X0                      0
#define WINDOW_RECT_POS_Y0                      0
#define WINDOW_RECT_POS_X1                      476
#define WINDOW_RECT_POS_Y1                      268

//Start header text offset
#define WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_X     1
#define WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_Y     1
#define WINDOW_DISPLAY_START_HEADER_TEXT2_OFFSET_X    175
#define WINDOW_DISPLAY_HEADER_TEXT_HIGHT_OFFSET_Y     (WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_Y + 16)

//Start footer text offset
#define WINDOW_DISPLAY_FOOTER_START_TEXT2_OFFSET_X    65
#define WINDOW_DISPLAY_FOOTER_START_OFFSET_Y          16
#define WINDOW_DISPLAY_FOOTER_START_ICON_OFFSET_Y     (WINDOW_DISPLAY_FOOTER_START_OFFSET_Y + 4)
#define WINDOW_DISPLAY_FOOTER_TEXT_HIGHT_OFFSET_Y     (WINDOW_DISPLAY_FOOTER_START_ICON_OFFSET_Y + 2)

#define WINDOW_DISPLAY_FOOTER_START_ICON1_OFFSET_X    410
#define WINDOW_DISPLAY_FOOTER_START_ICON2_OFFSET_X    (WINDOW_DISPLAY_FOOTER_START_ICON1_OFFSET_X + 22)

//Display Header
#define WINDOW_HEADER_TEXT1_POS_X0              (WINDOW_RECT_POS_X0 + WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_X)
#define WINDOW_HEADER_TEXT1_POS_Y0              (WINDOW_RECT_POS_Y0 + WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_Y)

#define WINDOW_HEADER_TEXT2_POS_X0              (WINDOW_RECT_POS_X0 + WINDOW_DISPLAY_START_HEADER_TEXT2_OFFSET_X)
#define WINDOW_HEADER_TEXT2_POS_Y0              (WINDOW_RECT_POS_Y0 + WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_Y)

#define WINDOW_HEADER_TEXT3_POS_X0              WINDOW_RECT_POS_X1
#define WINDOW_HEADER_TEXT3_POS_Y0              (WINDOW_RECT_POS_Y0 + WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_Y)

//Line below Header
#define WINDOW_HEADER_LINE_POS_X0               (WINDOW_RECT_POS_X0)
#define WINDOW_HEADER_LINE_POS_Y0               (WINDOW_RECT_POS_Y0 + WINDOW_DISPLAY_HEADER_TEXT_HIGHT_OFFSET_Y)
#define WINDOW_HEADER_LINE_POS_X1               WINDOW_RECT_POS_X1
#define WINDOW_HEADER_LINE_POS_Y1               (WINDOW_RECT_POS_Y0 + WINDOW_DISPLAY_HEADER_TEXT_HIGHT_OFFSET_Y)

//Display Footer
#define WINDOW_FOOTER_TEXT1_POS_X0              (WINDOW_RECT_POS_X0 + WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_X)
#define WINDOW_FOOTER_TEXT1_POS_Y0              (WINDOW_RECT_POS_Y1 - WINDOW_DISPLAY_FOOTER_START_OFFSET_Y)

#define WINDOW_FOOTER_TEXT2_POS_X0              (WINDOW_RECT_POS_X0 + WINDOW_DISPLAY_FOOTER_START_TEXT2_OFFSET_X)
#define WINDOW_FOOTER_TEXT2_POS_Y0              (WINDOW_RECT_POS_Y1 - WINDOW_DISPLAY_FOOTER_START_OFFSET_Y)

#define WINDOW_FOOTER_ICON1_POS_X0              (WINDOW_RECT_POS_X0 + WINDOW_DISPLAY_FOOTER_START_ICON1_OFFSET_X)
#define WINDOW_FOOTER_ICON1_POS_Y0              (WINDOW_RECT_POS_Y1 - WINDOW_DISPLAY_FOOTER_START_ICON_OFFSET_Y)

#define WINDOW_FOOTER_ICON2_POS_X0              (WINDOW_RECT_POS_X0 + WINDOW_DISPLAY_FOOTER_START_ICON2_OFFSET_X)
#define WINDOW_FOOTER_ICON2_POS_Y0              (WINDOW_RECT_POS_Y1 - WINDOW_DISPLAY_FOOTER_START_ICON_OFFSET_Y)

//Line above footer
#define WINDOW_FOOTER_LINE_POS_X0               (WINDOW_RECT_POS_X0)
#define WINDOW_FOOTER_LINE_POS_Y0               (WINDOW_RECT_POS_Y1 - WINDOW_DISPLAY_FOOTER_TEXT_HIGHT_OFFSET_Y)
#define WINDOW_FOOTER_LINE_POS_X1               WINDOW_RECT_POS_X1
#define WINDOW_FOOTER_LINE_POS_Y1               (WINDOW_RECT_POS_Y1 - WINDOW_DISPLAY_FOOTER_TEXT_HIGHT_OFFSET_Y)

//Offset for screen
#define SCREEN_OFFSET_X                         (WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_X + 2)
#define SCREEN_OFFSET_Y                         (WINDOW_DISPLAY_START_HEADER_TEXT_OFFSET_Y + WINDOW_DISPLAY_HEADER_TEXT_HIGHT_OFFSET_Y  + 1)

//List box ID
#define ID_LISTBOX_1                            (GUI_ID_USER + 72)
#define ID_MULTI_TEXT_1                         (GUI_ID_USER + 63)

//screen window max x and y size
#define WINDOW_SIZE_X                           (WINDOW_RECT_POS_X1 - (SCREEN_OFFSET_X + 1)) //480
#define WINDOW_SIZE_YY                          (WINDOW_RECT_POS_Y1 - (SCREEN_OFFSET_Y + 1)) //272

// Actual Y size =  Actual- (footer offset + header offset)
#define WINDOW_OFFSET                           WINDOW_DISPLAY_FOOTER_START_ICON_OFFSET_Y //(SCREEN_OFFSET_Y + 2)//20//42
#define WINDOW_SIZE_Y                           (WINDOW_SIZE_YY - WINDOW_OFFSET)

//Scroll bar scrolling item size
#define SCROLLBAR_WIDTH                         4

//Start of screen window X
#define LISTBOX_WIN_OFFSET                      SCREEN_OFFSET_Y
#define LISTBOX_START                           SCREEN_OFFSET_X
#define LISTBOX_SIZE_X                          194
#define LISTBOX_SIZE_Y                          (WINDOW_SIZE_Y - 2)
#define LISTBOX_RECT_STD_X                      (LISTBOX_SIZE_X - SCROLLBAR_WIDTH)
#define LISTBOX_RECT_STD_Y                      55

#define OFFSET                                  5
#define TEXT_OFFSET                             4
#define VARIABLE_OFFSET                         205
#define ARROW_OFFSET_X                          440
#define ARROW_OFFSET_Y                          20

#define RECTANGLE_OFFSET                        3
#define FILL_RECT_OFFSET                        1

#define RED_LOCK_IMAGE_X_POS                    150
#define RED_LOCK_IMAGE_Y_POS                    7
#define GRN_UNLOCK_IMAGE_X_POS                  145
#define GRN_UNLOCK_IMAGE_Y_POS                  60

#define LISTBOX_RECT_FILL_X                     (LISTBOX_RECT_STD_X - OFFSET - 1)
#define LISTBOX_RECT_FILL_Y                     (LISTBOX_RECT_STD_Y - OFFSET + 1)

#define MULTI_WIN_OFFSET                        SCREEN_OFFSET_Y
#define MULTI_EDIT_START                        (LISTBOX_SIZE_X + 2)
#define MULTI_EDIT_SIZE_X                       (WINDOW_SIZE_X - LISTBOX_SIZE_X - 2)
#define MULTI_EDIT_SIZE_Y                       LISTBOX_SIZE_Y

/************************************ ICON POSITIONS ************************************/
#define ICONVIEW_START_POS_X0                   LISTBOX_START
#define ICONVIEW_START_POS_Y0                   LISTBOX_WIN_OFFSET
#define ICONVIEW_START_SIZE_X                   (WINDOW_SIZE_X - (SCREEN_OFFSET_X - 2))
#define ICONVIEW_START_SIZE_Y                   (WINDOW_SIZE_Y - (SCREEN_OFFSET_Y- 2))
#define ICONVIEW_SPACE_X_SIZE                   01
#define ICONVIEW_SPACE_Y_SIZE                   10

/************************************ LISTBOX POSITIONS ************************************/
#define LISTBOX2_SIZE_X                         (WINDOW_SIZE_X - 2 /*4*/)
#define LISTBOX2_SIZE_Y                         (WINDOW_SIZE_Y - 2 /*4*/)
#define LISTBOX2_RECT_STD_X                     (LISTBOX2_SIZE_X - SCROLLBAR_WIDTH/*-2*/)
#define LISTBOX2_RECT_STD_Y                     55//62

#define LISTBOX2_RECT_FILL_X                    (LISTBOX2_RECT_STD_X - OFFSET)
#define LISTBOX2_RECT_FILL_Y                    (LISTBOX2_RECT_STD_Y - OFFSET)

/************************************ NUMPAD DETAILS ************************************/
#define NUMPAD_WINDOW_SIZE_X                    (MULTI_EDIT_SIZE_X-2)
#define NUMPAD_WINDOW_SIZE_Y                    MULTI_EDIT_SIZE_Y
#define NUMPAD_WINDOW_SIZE                      130
#define NUMPAD_WINDOW_XPOS                      280
#define NUMPAD_WINDOW_YPOS                      85
/************************************ Button sizes ************************************/
#define NUMPAD_GUI_ID_USER                      GUI_ID_USER
#define NUMPAD_MAX_NUMBER_OF_COL                4
#define NUMPAD_MAX_NUMBER_OF_ROW                4
#define MAX_NUMPAD_BUTTON                       15

#define NUMPAD_BUTTON_SIZE_X                    30
#define NUMPAD_BUTTON_SIZE_Y                    30
#define NUMPAD_BUTTON_OFFSET_ROW                2
#define NUMPAD_BUTTON_OFFSET_COL                2
#define NUMPAD_DIFF_BUTTON_SIZE_X               (NUMPAD_BUTTON_SIZE_X + NUMPAD_BUTTON_SIZE_X + NUMPAD_BUTTON_OFFSET_COL)
#define NUMPAD_DIFF_BUTTON_SIZE_Y               (NUMPAD_BUTTON_SIZE_Y + NUMPAD_BUTTON_SIZE_Y + NUMPAD_BUTTON_OFFSET_ROW)

/************************************ BUTTON POSITIONS ************************************/
/************************************ ROW POSITIONS ************************************/
#define NUMPAD_ROW_POS_1                        80
#define NUMPAD_ROW_POS_2                        (NUMPAD_ROW_POS_1 + NUMPAD_BUTTON_SIZE_Y + NUMPAD_BUTTON_OFFSET_ROW)
#define NUMPAD_ROW_POS_3                        (NUMPAD_ROW_POS_2 + NUMPAD_BUTTON_SIZE_Y + NUMPAD_BUTTON_OFFSET_ROW)
#define NUMPAD_ROW_POS_4                        (NUMPAD_ROW_POS_3 + NUMPAD_BUTTON_SIZE_Y + NUMPAD_BUTTON_OFFSET_ROW)
#define NUMPAD_ROW_POS_5                        (NUMPAD_ROW_POS_4 + NUMPAD_BUTTON_SIZE_Y + NUMPAD_BUTTON_OFFSET_ROW)

/************************************ COLUMN POSITIONS ************************************/
#define NUMPAD_COL_POS_1                        80
#define NUMPAD_COL_POS_2                        (NUMPAD_COL_POS_1 + NUMPAD_BUTTON_SIZE_X + NUMPAD_BUTTON_OFFSET_COL)
#define NUMPAD_COL_POS_3                        (NUMPAD_COL_POS_2 + NUMPAD_BUTTON_SIZE_X + NUMPAD_BUTTON_OFFSET_COL)
#define NUMPAD_COL_POS_4                        (NUMPAD_COL_POS_3 + NUMPAD_BUTTON_SIZE_X + NUMPAD_BUTTON_OFFSET_COL)
#define NUMPAD_COL_POS_5                        (NUMPAD_COL_POS_4 + NUMPAD_BUTTON_SIZE_X + NUMPAD_BUTTON_OFFSET_COL)

/************************************ KEYPAD DETAILS ************************************/
#define KEYPAD_WINDOW_XPOS                      LISTBOX_SIZE_X
#define KEYPAD_WINDOW_YPOS                      MULTI_WIN_OFFSET
#define KEYPAD_WINDOW_SIZE_X                    (MULTI_EDIT_SIZE_X-2)
#define KEYPAD_WINDOW_SIZE_Y                    MULTI_EDIT_SIZE_Y

/************************************ BUTTON SIZES ************************************/
#define KEYPAD_GUI_ID_USER                      (GUI_ID_USER + MAX_NUMPAD_BUTTON)
#define KEYPAD_MAX_NUMBER_OF_COL                11
#define KEYPAD_MAX_NUMBER_OF_ROW                4
#define MAX_KEYPAD_BUTTON                       45

#define KEYPAD_BUTTON_SIZE_X                    22
#define KEYPAD_BUTTON_SIZE_Y                    20
#define KEYPAD_DIFF_BUTTON_SIZE_X               44
#define KEYPAD_BUTTON_OFFSET_ROW                1
#define KEYPAD_BUTTON_OFFSET_COL                1

/************************************ ROW POSITIONS ************************************/
#define KEYPAD_ROW_POS_1                        95
#define KEYPAD_ROW_POS_2                        (KEYPAD_ROW_POS_1 + KEYPAD_BUTTON_SIZE_Y + KEYPAD_BUTTON_OFFSET_ROW)
#define KEYPAD_ROW_POS_3                        (KEYPAD_ROW_POS_2 + KEYPAD_BUTTON_SIZE_Y + KEYPAD_BUTTON_OFFSET_ROW)
#define KEYPAD_ROW_POS_4                        (KEYPAD_ROW_POS_3 + KEYPAD_BUTTON_SIZE_Y + KEYPAD_BUTTON_OFFSET_ROW)
#define KEYPAD_ROW_POS_5                        (KEYPAD_ROW_POS_4 + KEYPAD_BUTTON_SIZE_Y + KEYPAD_BUTTON_OFFSET_ROW)

/************************************ COLUMN POSITIONS ************************************/
#define KEYPAD_COL_POS_1                        2
#define KEYPAD_COL_POS_2                        (KEYPAD_COL_POS_1 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_3                        (KEYPAD_COL_POS_2 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_4                        (KEYPAD_COL_POS_3 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_5                        (KEYPAD_COL_POS_4 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_6                        (KEYPAD_COL_POS_5 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_7                        (KEYPAD_COL_POS_6 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_8                        (KEYPAD_COL_POS_7 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_9                        (KEYPAD_COL_POS_8 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_10                       (KEYPAD_COL_POS_9 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_11                       (KEYPAD_COL_POS_10 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)
#define KEYPAD_COL_POS_12                       (KEYPAD_COL_POS_11 + KEYPAD_BUTTON_SIZE_X + KEYPAD_BUTTON_OFFSET_COL)

/************************************ EDIT WINDOW POSITIONS ************************************/
#define EDIT_WINDOW_XPOS                        40
#define EDIT_WINDOW_YPOS                        20
#define EDIT_WINDOW_SIZE_X                      210
#define EDIT_WINDOW_SIZE_Y                      40

#define EDIT_WINDOW_XPOS_KEYPAD                 (EDIT_WINDOW_XPOS - 38)
#define EDIT_WINDOW_SIZE_X_KEYPAD               (EDIT_WINDOW_SIZE_X - 35/*25*/)

#define EDIT_UNIT_WINDOW_XPOS                   (EDIT_WINDOW_SIZE_X_KEYPAD + 4)
#define EDIT_UNIT_WINDOW_YPOS                   EDIT_WINDOW_YPOS
#define EDIT_UNIT_WINDOW_SIZE_X                 (KEYPAD_WINDOW_SIZE_X - EDIT_WINDOW_SIZE_X_KEYPAD-6)
#define EDIT_UNIT_WINDOW_SIZE_Y                  EDIT_WINDOW_SIZE_Y

/********************************LISTBOX_MULTIEDIT_CUSTOM_SIZE*********************************/
#define LOAD_CTRL_LISTBOX_SIZE_X                (LISTBOX_SIZE_X - 30)
#define LOAD_CTRL_MULTIEDIT_START_X             (MULTI_EDIT_START - 30)
#define LOAD_CTRL_MULTIEDIT_SIZE_X              (MULTI_EDIT_SIZE_X + 35)
/************************************KEY DEFINITIONS*******************************************/
#define GUI_KEY_BACK                            62

/************************************SCREEN 4 DEFINITIONS*******************************************/
#define IMAGE_X_POS                             3
#define IMAGE_Y_POS                             150
#define IMAGE_X_SIZE                            270
#define IMAGE_Y_SIZE                            50

/*********************************** DATA VALIDATION ***************************************/
#define CONVEYOR_ANGLE_DEFAULT                  12.0

/************************************ DATE TIME FORMATS ************************************/
#define MMDDYYYY                                Screen31_str3
#define DDMMYYYY                                Screen31_str6
#define AM_TIME_FORMAT                          Screen6152_str3
#define PM_TIME_FORMAT                          Screen6152_str6
#define TWELVE_HR                               Screen32_str3
#define TWENTYFOUR_HR                           Screen32_str4
#define AM																			0
#define PM																			1
/*********************************** DAYS OF THE WEEK ************************************/
#define SUNDAY                                  Screen6143_str3
#define MONDAY                                  Screen6143_str4
#define TUESDAY                                 Screen6143_str5
#define WEDNESDAY                               Screen6143_str6
#define THURSDAY                                Screen6143_str7
#define FRIDAY                                  Screen6143_str8
#define SATURDAY                                Screen6143_str9

/*********************************** DISTANCE UNITS *************************************/
#define INCHES                                  Screen24_str8
#define FEET                                    Screen24_str9
#define METERS                                  Screen24_str10
#define CENTIMETERS                             Screen24_str11

/*********************************** WEIGHT UNITS ***************************************/
#define TONS			                              Screen25_str2
#define LONG_TON                                Screen25_str5
#define LBS                                     Screen25_str8
#define TONNE                                   Screen25_str9
#define KG                                      Screen25_str12

#define LOCK_ENABLE_STR                         Screen631_str1

/*********************************** ASSIGN INPUTS ***************************************/
#define CLEAR_WEIGHT_STR                        Screen4411_str2
#define PRINT_TICKET_STR                        Screen4411_str5
#define PRINT_AND_CLEAR_STR                     Screen4411_str8
#define ENTER_LOAD_STR                          Screen4411_str11
#define PID_RATE_ZERO_STR                       Screen4411_str14
#define ZERO_CAL_STR                            Screen4411_str17
#define ERROR_ACK_STR                           Screen4411_str20

/*********************************** ASSIGN OUTPUTS ***************************************/
#define PULSED_OUTPUT_STR                       Screen4421_str2
#define QUADRATURE_WAVE_STR                     Screen4421_str5
#define EEROR_ALARM_STR                         Screen4421_str8
#define MIN_MAX_SPEED_STR                       Screen4421_str11
#define MIN_MAX_RATE_STR                        Screen4421_str14
#define BATCHING_LOADOUT_STR                    Screen4421_str17

/*********************************** ERROR ALARMS ****************************************/
#define LOAD_CELL_ERR_STR                       Screen44213_str2
#define ANGLE_SENSOR_ERR_STR                    Screen44213_str5
#define COMMUNICATION_ERR_STR                   Screen44213_str8
#define NEGATIVE_RATE_ERR_STR                   Screen44213_str11
#define ANY_ERROR_STR                           Screen44213_str14

/*********************************** MIN/MAX SPEED ***************************************/
#define MIN_SPEED_OUTPUT_STR                    Screen442142_str1
#define MAX_SPEED_OUTPUT_STR                    Screen442142_str4

/*********************************** MIN/MAX RATE ****************************************/
#define MIN_RATE_OUTPUT_STR                     Screen442152_str1
#define MAX_RATE_OUTPUT_STR                     Screen442152_str4

/********************************** WEIGHT PER PULSE *************************************/
#define FIRST_WT_UNIT_PER_PULSE_STR             Screen442111_str1
#define SECOND_WT_UNIT_PER_PULSE_STR            Screen442111_str4
#define THIRD_WT_UNIT_PER_PULSE_STR             Screen442111_str6
#define FOURTH_WT_UNIT_PER_PULSE_STR            Screen442111_str8
#define FIFTH_WT_UNIT_PER_PULSE_STR							Screen442111_str10

/********************************** DECIMAL DIGITS FOR WEIGHT DISPLAY *************************************/
#define NO_DECIMAL_DIGIT												Screen2_str32
#define ONE_DECIMAL_DIGIT												Screen2_str21
#define TWO_DECIMAL_DIGIT												Screen2_str22
#define THREE_DECIMAL_DIGIT											Screen2_str23

/********************************** DECIMAL DIGITS FOR WEIGHT DISPLAY *************************************/
#define INSTALLED       												Screen2_str27
#define NOT_INSTALLED														Screen2_str29


/****************************************************************/
/************************************ SCREEN ORG NOS ************************************/

#define MAIN_MENU_ORG_NO                        "000000"

#define SETUP_WIZARD_ORG_NO                     "100000"
#define SETUP_WIZARD_SPAN_CAL_ORG_NO            "130000"
#define SETUP_WIZARD_SPAN_CAL_CHILD_ORG_NO      "131000"
#define SETUP_WIZARD_COMPLETE_ORG_NO            "140000"

#define PCM_SETUP_ORG_NO                      "200000"
#define PCM_WEB_SELECT_ORG_NO                 "212000"
#define PCM_CONNECTION_SELECT_ORG_NO          "213000"

#define DHCP_ENABLE_ORG_NO										"221000"
#define SNTP_ORG_NO														"250000"
#define RUN_MODE_SELECTION_SCRN									"210000"
#define LOAD_CELL_SIZE_SCREEN										"230000"
#define LOAD_CELL_SIZE_COMPLETE_SCREEN					"231300"
//#define DISTANCE_UNIT_SCREEN                    "240000"
#define WEIGHT_UNIT_SCREEN                      "950000"

#define RATE_UNIT_SELECT_ORG_NO                 "260000"
#define CONV_ANGLE_INSTALL_OPTION_ORG_NO        "271000"
#define CONV_ANGLE_GREATER_30                   "272000"
#define CONV_ANGLE_WARNING                      "272100"
#define IDLER_DISTANCE_A_MODIFY_ORG							"281000"
#define IDLER_DISTANCE_B_MODIFY_ORG							"282000"
#define IDLER_DISTANCE_C_MODIFY_ORG							"283000"
#define IDLER_DISTANCE_D_MODIFY_ORG							"284000"
#define IDLER_DISTANCE_E_MODIFY_ORG							"285000"
#define WHEEL_DIA_ENTER_ORG_NO                  "291000"
#define PULSES_PER_REV_ENTER_ORG_NO             "292000"
#define USER_BELT_SPEED_ORG_NO		              "293100"
#define DECIMAL_PLACES_ORG_NO										"2A0000"
#define OPTIONAL_IO_BOARD_INTALL_ORG_NO					"2B0000"								

#define CAL_SCREEN_ORG_NO                       "A00000"
#define ADMIN_FRMW_UPDATE_ORG_NO								"600000"
//-------------Test Weight Calibration ----------------

#define TEST_WEIGHT_ENTRY_ORG_NO                "3A0000"
#define TEST_WEIGHT_VER_ENTRY_ORG_NO            "31A000"
//#define TEST_WEIGHT_ACK_ORG_NO                  "312000"
// #define TEST_WEIGHT_VERIFY_ORG_NO               "313000"
#define TEST_WEIGHT_START_CON_ORG_NO            "31B000"
#define TEST_WEIGHT_START_CAL                   "31C000"
#define TEST_WT_CAL_ORG_NO                      "31D000"

//-------------Material Test Calibration---------------
#define MAT_TEST_WIZARD_ORG_NO                  "3A0000"
#define TEST_WEIGHT_CERTIFIED_UNIT_ORG_NO       "3A1000"
#define MATERIAL_TEST_CERTIFIED_UNIT            "3A3000"
#define MATERIAL_TEST_CERTIFIED_UNIT_SELECT     "3A3100"
#define MATERIAL_TEST_AUTO_START_ORG_NO					"3A2000"
#define MAT_TST_START_CAL_ORG_NO                "3A4000"
#define MAT_TST_CAL_ACPT_ORG_NO                 "3A5000"

//-------------Digital Calibration---------------------
#define DIGI_CAL_WIZARD_ORG_NO                  "3A0000"
#define DIGITAL_START_CAL                       "3A1000"
#define DIG_CAL_ACPT_ORG_NO                     "3A2000"

//-------------Length and zero Calibration-------------
// #define LEN_CAL_WIZARD_ORG_NO                   "340000"
// #define LEN_ZERO_START_BELT_ORG_NO              "341000"
// #define LEN_ZERO_CAL_START                      "341100"
// #define LEN_ZERO_CAL_STOP                       "341200"
// #define LEN_ZER_CAL_ACPT_LEN_ORG_NO             "341300"
// #define LEN_ZER_CAL_ACPT_ZER_ORG_NO             "341400"
// #define LEN_ZER_CAL_COMPLETE_ORG_NO             "341500"
#define RESTORE_FACTORY_SETTINGS_SCREEN						"340000"

//-------------Auto length Calibration-----------------
// #define AUTO_BELT_START_BELT_ORG_NO             "342000"
// #define AUTO_BELT_CAL_START                     "342100"
// #define AUTO_BELT_CAL_STOP                      "342200"
// #define AUTO_BELT_LEN_CAL_ACPT_ORG_NO           "342300"

// //------------Manual Belt length Calibration------------
// #define MANUAL_BELT_LENGTH_ORG_NO               "343100"

//------------Zero calibration selection---------------
#define ZERO_CAL_WIZARD_ORG_NO                  "350000"

//-------------Static zero Calibration-----------------
#define STATIC_ZEROCAL_START                    "352000"
#define STATIC_ZEROCAL_COMPLETE                 "352100"

//--------------Dynamic zero calibration---------------
//#define DYNAMIC_ZERO_CAL_WIZARD_ORG_NO          "351000"
#define DYNAMIC_ZEROCAL_ACK_ORG_NO              "351000"
#define DYNAMIC_ZEROCAL_START                   "351100"
#define DYN_ZER_CAL_ACPT_ORG_NO                 "351200"
#define DYN_ZER_CAL_COMPLETE_ORG_NO             "351300"

#define SETUP_DEVICES_ORG_NO                    "400000"
#define PRINTER_TICKET_NUMBER_RESET							"414000"
#define PRINTER_TYPE_SELECT_ORG_NO              "412000"
#define SCBD_TYPE_SELECT_ORG_NO                 "422000"
#define SCBD_ALTN_SELECT_ORG_NO                 "421100" 
#define RUN_LOG_TIME_MODIFY_ORG_NO							"432100"
#define USB_BACKUP_RESTORE_SCREEN								"436000"
#define ASSIGN_OUTPUT_ORG_NO                    "442000"
#define SETUP_PULSE_OUTPUT1_ORG_NO              "442110"
#define SETUP_PULSE_OUTPUT2_ORG_NO              "442210"
#define SETUP_PULSE_OUTPUT3_ORG_NO              "442310"
#define PULSE_ON_TIME1_ORG_NO                   "442112"
#define PULSE_ON_TIME2_ORG_NO                   "442212"
#define PULSE_ON_TIME3_ORG_NO                   "442312"
#define ASSIGN_RELAY_ORG_NO                     "443000"
#define AO1_SETPOINT_ORG_NO											"444120"
#define AO1_MAX_RATE_ORG_NO											"444130"
#define AO2_SETPOINT_ORG_NO											"444220"
#define AO2_MAX_RATE_ORG_NO											"444230"
#define PID_CONTROL_ORG_NO											"451000"
#define PRESET_LOADS_CONTROL_ORG_NO							"452000"
#define BLENDING_CNTRL_SETUP_ORG_NO						  "453000"
#define BLENDING_INGRDIENT_PER_ORG_NO						"453200"
#define FEEDER_MAX_RATE_ORG_NO									"453500"
#define RATE_CNTRL_SETUP_ORG_NO								  "454000" 
#define RATE_CNRTL_TRGET_RATE_ORG_NO						"454100"
#define LOAD_CNTRL_SETUP_ORG_NO								  "455000"
#define LOAD_CNTRL_TRGET_LOAD_ORG_NO						"455100"

#define SET_PID_SETPOINT_TYPE                   "451300"
#define NETWORK_ADDR_ORG_NO                     "453100"
#define NUM_OF_SLAVES_ORG_NO                    "453200"
#define FEED_DELAY_ORG_NO                       "453300"
#define PRELOAD_DELAY_ORG_NO										"453400"
#define MIN_BELT_SPEED_ORG_NO                   "455200"

#define TOTALS_SCREEN_ORG_NO										"510000"
#define DIAG_SCREEN_ORG_NO                      "520000"
#define DIAG_SCREEN_LIVE_SENSOR_DATA_ORG_NO     "522000" //live sensor data
#define DIAG_SCREEN_LIVE_VOLTAGE_DATA_ORG_NO    "524000" //live voltage data
#define DIAG_SCREEN_LIVE_IO_DATA_ORG_NO         "523000" //live voltage data
#define DIAG_SCREEN_COMM_PORT_STATUS_ORG_NO     "525000" //Communication Port Status
#define DIAG_SCREEN_LIVE_ANALOG_OPT_ORG_NO      "523400" //live Analog output data

#define ADMIN_SCREEN_ORG_NO                     "600000"

#define DATE_ORG_NO                             "312000"
#define DAY_OF_WEEK_ORG_NO                      "614300"
#define TIME_ORG_NO                             "321000"
#define AM_PM_SET_ORG_NO                        "615200"
#define AUTO_ZERO_TOL_ORG_NO                    "616000"
#define BACKUP_RESTORE_USB_ORG_NO               "619000"
#define BACKUP_USB_WIZARD_ORG_NO                "619100"
#define BACKUP_USB_ACK_ORG_NO                   "619110"
#define BACKUP_USB_FILE_STORE_ORG_NO            "619120"
#define RESTORE_USB_WIZARD_ORG_NO               "619200"
#define RESTORE_USB_FILE_VIEW_ORG_NO            "619211"
#define RESTORE_USB_ACK_ORG_NO                  "619220"
#define RESTORE_FILE_FROM_USB_ORG_NO            "619230"
#define LANGUAGE_SELECTION_SCREEN_ORG_NO        "61B000"


#define SETUP_WIZARD_SEC_ORG_NO                 "631000"
#define CALIBRATION_SEC_ORG_NO                  "632000"
#define ZERO_CALIBRATION_SEC_ORG_NO             "633000"
#define SETUP_DEVICES_SEC_ORG_NO                "634000"
#define ADMIN_SEC_ORG_NO                        "635000"

#define PASSWORD_SCREEN_ORG_NO                  "637000"
#define PWD_ERROR_ORG_NO                        "637100"
#define NEW_PWD_SCREEN_ORG_NO                   "637200"
#define VERIFY_PWD_SCREEN_ORG_NO                "637300"
#define VERIFY_PWD_ERROR_ORG_NO                 "637310"

#define INT_UPDT_WIZARD_ORG_NO                  "330000"
#define INT_USB_CHK_WIZARD_ORG_NO               "331000"
#define INT_UPDT_FILE_VIEW_ORG_NO               "332100"
#define INT_OLD_VER_WRN_ORG_NO                  "332110"
#define FIRMWARE_UPGRADE_ORG_NO                 "332120"
#define INT_UPDT_ACK_ORG_NO                     "333000"
#define BEGIN_FIRMWARE_UPDT_ORG_NO              "334000"

#define TRIM_FACTOR_MODIFY_ORG_NO               "651000"
#define ZERO_VALUE_MODFIY_ORG_NO                "652000"
#define BELT_LENGTH_MODIFY_ORG_NO               "653000"

#define DHCP_CONFIG_ORG_NO                      "661000"
#define IP_ADDR_ORG_NO                          "222000"
#define SUBNET_MASK_ORG_NO                      "223000"
#define GATEWAY_ORG_NO                          "224000"
#define PRIDNS_ORG_NO                          	"226000"
#define SECDNS_ORG_NO                          	"227000"
#define MAC_ID_ORG_NO                          	"225000"
#define PPP_STATUS_ORG_NO												"241000"
#define FOUR_G_IP_ADDR_ORG_NO                  	"243000"


#define MODBUS_SLAVEID_ORG_NO										"665000"

//Run mode screens
#define  RUN_MODE_ORG_NO                        "700000"

#define  WEIGHT_RUN_MODE_SCREEN_ORG_NO_STR      "700000"
#define  RATE_RUN_MODE_SCREEN_ORG_NO_STR        "710000"
#define  LOADOUT_RUN_MODE_SCREEN_ORG_NO_STR     "720000"
#define  BLENDING_RUN_MODE_SCREEN_ORG_NO_STR    "730000"
#define  RATE_CTRL_RUN_MODE_SCREEN_ORG_NO_STR   "740000"
#define  LOAD_CTRL_RUN_MODE_SCREEN_ORG_NO_STR   "750000"
#define  WEIGHT_PREV_RUN_MODE_SCREEN_ORG_NO_STR "760000"
#define WEIGHT_PREV1_RUN_MODE_ORG_NO_STR				"761000"

#define WEIGHT_RUN_MODE_SCREEN_ORG_NO           700000
#define RATE_RUN_MODE_SCREEN_ORG_NO             710000
#define LOADOUT_RUN_MODE_SCREEN_ORG_NO          720000
#define BLENDING_RUN_MODE_SCREEN_ORG_NO         730000
#define LOAD_CONTROL_RUN_MODE_SCREEN_ORG_NO     740000
#define RATE_CONTROL_RUN_MODE_SCREEN_ORG_NO     750000
#define WEIGHT_PREVIOUS_RUN_MODE_SCREEN_ORG_NO	760000
#define WEIGHT_PREV1_RUN_MODE_ORG_NO						761000
#define WEIGHT_PREV2_RUN_MODE_ORG_NO						762000
#define WEIGHT_PREV3_RUN_MODE_ORG_NO						763000
#define WEIGHT_PREV4_RUN_MODE_ORG_NO						764000
#define WEIGHT_PREV5_RUN_MODE_ORG_NO						765000
#define WEIGHT_PREV6_RUN_MODE_ORG_NO						766000
#define WEIGHT_PREV7_RUN_MODE_ORG_NO						767000
#define WEIGHT_PREV8_RUN_MODE_ORG_NO						768000

#define PCM_SCREEN_ORG_NO												"800000"
#define PCM_PARAMETER_SCREEN_ORG_NO							"900000"
#define SELECT_VERSION_NO_ORG_NO								"332000"

#define GO_BACK_FLAG                            2
#define GO_BACK_LOOP_FLAG												3
#define GO_BACK_END_FLAG												4
#define CREATE_SCREEN                           1
#define UPDATE_SCREEN                           0

#define DAC_FULL_SCALE_COUNT                    4095.0			// only 12 bits are assigned so max value is 4095(FFF)				
#define SCALING_FACTOR_0_20MA										((float)20/4096)
#define SCALING_FACTOR_4_20MA										((float)16/4096)
//-----------------Calibration flags--------------------
#define CLEAR_CAL_FLAG                          0x00

#define START_TEST_WEIGHT_CAL                   0x01
#define START_DIGITAL_CAL                       0x02
#define START_MATERIAL_CAL                      0x04
#define START_STATICZERO_CAL                    0x08
#define START_DYNAMICZERO_CAL                   0x10
#define START_LENZERO_CAL                       0x20
#define START_BELT_CAL                          0x40

//----------------Default values------------------------
#define WHEEL_DIAMETER_BELTWAY_DEF              7.970
#define PULSES_PER_REV_BELTWAY_DEF              100.0

//----------Clear weight password flags------------------
#define CLR_WT_FLAG_RESET                       1
#define CLR_WT_PW_SCREEN                        2
#define CLR_WT_CHK_PW_SCREEN                    3
#define CLEAR_THE_WEIGHT                        4


#ifdef ETHERNET
	#define MY_IP                                 localm[NETIF_ETH].IpAdr
  #define DHCP_TOUT                             500   /* DHCP timeout 5 seconds      */
#endif


extern GUI_CONST_STORAGE GUI_BITMAP _bmArrowEnter;
extern GUI_CONST_STORAGE GUI_BITMAP _bmArrowEnter1;
extern GUI_CONST_STORAGE GUI_BITMAP _bmArrowEnter2;
extern GUI_CONST_STORAGE GUI_BITMAP _bmArrowRight1;
extern GUI_CONST_STORAGE GUI_BITMAP _bmArrowRight2;

extern Screen_org_data Org_700000_parent[];
extern Screen_org_data Org_000000_parent[];
extern Screen_org_data Org_800000_parent[];
extern Screen_org_data  Org_322000_parent[];

extern Screen_org_data Org_760000_children[];
extern Screen_org_data Org_280000_children[];
extern SETUP_WIZARD_SCREEN Org_230000_screen; 
extern SETUP_WIZARD_SCREEN Org_351000_screen;
extern SCREEN_TYPE1_DATA Org_000000_data[];
//extern SCREEN_TYPE1_DATA Org_300000_data[];
extern SCREEN_TYPE1_DATA Org_400000_data[];
extern SCREEN_TYPE1_DATA Org_440000_data[];
extern SCREEN_TYPE1_DATA Org_450000_data[];
extern SCREEN_TYPE1_DATA Org_500000_data[];
//extern SCREEN_TYPE1_DATA Org_510000_data[];
extern SCREEN_TYPE1_DATA Org_520000_data[];
extern SCREEN_TYPE1_DATA Org_523000_data[];
extern SCREEN_TYPE1_DATA Org_600000_data[];

extern SCREEN_TYPE2_DATA Org_210000_data[];
extern SCREEN_TYPE2_DATA Org_220000_data[];
extern SCREEN_TYPE2_DATA Org_230000_data[];
extern SCREEN_TYPE2_DATA Org_240000_data[];
extern SCREEN_TYPE2_DATA Org_250000_data[];
extern SCREEN_TYPE2_DATA Org_300000_data[];
extern SCREEN_TYPE2_DATA Org_510000_data[];
extern SCREEN_TYPE2_DATA Org_522000_data[];
extern SCREEN_TYPE2_DATA Org_524000_data[];
extern SCREEN_TYPE2_DATA Org_525000_data[];
extern SCREEN_TYPE2_DATA Org_660000_data[];

extern SCREEN_TYPE3_DATA Org_212000_data[];
extern SCREEN_TYPE3_DATA Org_221000_data[];
extern SCREEN_TYPE3_DATA Org_241000_data[];
extern SCREEN_TYPE3_DATA Org_311000_data[];
extern SCREEN_TYPE3_DATA Org_332000_data[];
extern SCREEN_TYPE3_DATA Org_331000_data[];

extern SCREEN_TYPE3_DATA Org_231210_data[];
extern SCREEN_TYPE3_DATA Org_260000_data[];
extern SCREEN_TYPE3_DATA Org_271000_data[];
extern SCREEN_TYPE3_DATA Org_2A0000_data[];
extern SCREEN_TYPE3_DATA Org_2B0000_data[];
extern SCREEN_TYPE3_DATA Org_315000_data[];
extern SCREEN_TYPE3_DATA Org_322100_data[];
extern SCREEN_TYPE3_DATA Org_324100_data[];
extern SCREEN_TYPE3_DATA Org_326000_data[];
extern SCREEN_TYPE3_DATA Org_340000_data[];

extern SCREEN_TYPE3_DATA Org_411000_data[];
extern SCREEN_TYPE3_DATA Org_412000_data[];
extern SCREEN_TYPE3_DATA Org_412000_data[];
extern SCREEN_TYPE3_DATA Org_412210_data[];
extern SCREEN_TYPE3_DATA Org_412220_data[];
extern SCREEN_TYPE3_DATA Org_412230_data[];
extern SCREEN_TYPE3_DATA Org_412240_data[];
extern SCREEN_TYPE3_DATA Org_421000_data[];
extern SCREEN_TYPE3_DATA Org_422000_data[];
extern SCREEN_TYPE3_DATA Org_423000_data[];
extern SCREEN_TYPE3_DATA Org_422211_data[];
extern SCREEN_TYPE3_DATA Org_422212_data[];
extern SCREEN_TYPE3_DATA Org_422213_data[];
extern SCREEN_TYPE3_DATA Org_431000_data[];
extern SCREEN_TYPE3_DATA Org_432000_data[];
extern SCREEN_TYPE3_DATA Org_433000_data[];
extern SCREEN_TYPE3_DATA Org_434000_data[];
extern SCREEN_TYPE3_DATA Org_435000_data[];
extern SCREEN_TYPE3_DATA Org_441100_data[];
extern SCREEN_TYPE3_DATA Org_441200_data[];
extern SCREEN_TYPE3_DATA Org_441300_data[];
extern SCREEN_TYPE3_DATA Org_441400_data[];
extern SCREEN_TYPE3_DATA Org_442100_data[];
extern SCREEN_TYPE3_DATA Org_442111_data[];
extern SCREEN_TYPE3_DATA Org_442130_data[];
extern SCREEN_TYPE3_DATA Org_442142_data[];
extern SCREEN_TYPE3_DATA Org_442152_data[];
extern SCREEN_TYPE3_DATA Org_442200_data[];
extern SCREEN_TYPE3_DATA Org_442211_data[];
extern SCREEN_TYPE3_DATA Org_442230_data[];
extern SCREEN_TYPE3_DATA Org_442242_data[];
extern SCREEN_TYPE3_DATA Org_442252_data[];
extern SCREEN_TYPE3_DATA Org_442300_data[];
extern SCREEN_TYPE3_DATA Org_442311_data[];
extern SCREEN_TYPE3_DATA Org_442330_data[];
extern SCREEN_TYPE3_DATA Org_442342_data[];
extern SCREEN_TYPE3_DATA Org_442352_data[];
extern SCREEN_TYPE3_DATA Org_443100_data[];
extern SCREEN_TYPE3_DATA Org_443110_data[];
extern SCREEN_TYPE3_DATA Org_443122_data[];
extern SCREEN_TYPE3_DATA Org_443132_data[];
extern SCREEN_TYPE3_DATA Org_443200_data[];
extern SCREEN_TYPE3_DATA Org_443210_data[];
extern SCREEN_TYPE3_DATA Org_443222_data[];
extern SCREEN_TYPE3_DATA Org_443232_data[];
extern SCREEN_TYPE3_DATA Org_443300_data[];
extern SCREEN_TYPE3_DATA Org_443310_data[];
extern SCREEN_TYPE3_DATA Org_443322_data[];
extern SCREEN_TYPE3_DATA Org_443332_data[];
extern SCREEN_TYPE3_DATA Org_444110_data[];
extern SCREEN_TYPE3_DATA Org_444210_data[];
extern SCREEN_TYPE3_DATA Org_451200_data[];
extern SCREEN_TYPE3_DATA Org_451300_data[];
extern SCREEN_TYPE3_DATA Org_511000_data[];
extern SCREEN_TYPE3_DATA Org_614210_data[];
extern SCREEN_TYPE3_DATA Org_614300_data[];
extern SCREEN_TYPE3_DATA Org_615200_data[];
extern SCREEN_TYPE3_DATA Org_615300_data[];
extern SCREEN_TYPE3_DATA Org_617100_data[];
extern SCREEN_TYPE3_DATA Org_631000_data[];
extern SCREEN_TYPE3_DATA Org_632000_data[];
extern SCREEN_TYPE3_DATA Org_633000_data[];
extern SCREEN_TYPE3_DATA Org_634000_data[];
extern SCREEN_TYPE3_DATA Org_635000_data[];
extern SCREEN_TYPE3_DATA Org_636000_data[];
extern SCREEN_TYPE3_DATA Org_661000_data[];

extern SCREEN_TYPE5_DATA Org_211000_data[];

extern SCREEN_TYPE5_DATA Org_222000_data[];
extern SCREEN_TYPE5_DATA Org_223000_data[];
extern SCREEN_TYPE5_DATA Org_224000_data[];
extern SCREEN_TYPE5_DATA Org_226000_data[];
extern SCREEN_TYPE5_DATA Org_231000_data[];
//extern SCREEN_TYPE5_DATA Org_242000_data[];
extern SCREEN_TYPE5_DATA Org_252000_data[];
extern SCREEN_TYPE5_DATA Org_312000_data[];
extern SCREEN_TYPE5_DATA Org_321000_data[];
extern SCREEN_TYPE5_DATA Org_231100_data[];
extern SCREEN_TYPE5_DATA Org_231300_data[];
extern SCREEN_TYPE5_DATA Org_272000_data[];
extern SCREEN_TYPE5_DATA Org_281000_data[];
extern SCREEN_TYPE5_DATA Org_282000_data[];
extern SCREEN_TYPE5_DATA Org_283000_data[];
extern SCREEN_TYPE5_DATA Org_284000_data[];
extern SCREEN_TYPE5_DATA Org_285000_data[];
extern SCREEN_TYPE5_DATA Org_291000_data[];
extern SCREEN_TYPE5_DATA Org_292000_data[];
extern SCREEN_TYPE5_DATA Org_293100_data[];
extern SCREEN_TYPE5_DATA Org_421100_data[];
extern SCREEN_TYPE5_DATA Org_432100_data[];
extern SCREEN_TYPE5_DATA Org_442112_data[];
extern SCREEN_TYPE5_DATA Org_442141_data[];
extern SCREEN_TYPE5_DATA Org_442151_data[];
extern SCREEN_TYPE5_DATA Org_442212_data[];
extern SCREEN_TYPE5_DATA Org_442241_data[];
extern SCREEN_TYPE5_DATA Org_442251_data[];
extern SCREEN_TYPE5_DATA Org_442312_data[];
extern SCREEN_TYPE5_DATA Org_442341_data[];
extern SCREEN_TYPE5_DATA Org_442351_data[];
extern SCREEN_TYPE5_DATA Org_443121_data[];
extern SCREEN_TYPE5_DATA Org_443131_data[];
extern SCREEN_TYPE5_DATA Org_443221_data[];
extern SCREEN_TYPE5_DATA Org_443231_data[];
extern SCREEN_TYPE5_DATA Org_443321_data[];
extern SCREEN_TYPE5_DATA Org_443331_data[];
extern SCREEN_TYPE5_DATA Org_444120_data[];
extern SCREEN_TYPE5_DATA Org_444130_data[];
extern SCREEN_TYPE5_DATA Org_444140_data[];
extern SCREEN_TYPE5_DATA Org_444220_data[];
extern SCREEN_TYPE5_DATA Org_444230_data[];
extern SCREEN_TYPE5_DATA Org_444240_data[];
extern SCREEN_TYPE5_DATA Org_451100_data[];
extern SCREEN_TYPE5_DATA Org_451310_data[];
extern SCREEN_TYPE5_DATA Org_451320_data[];
extern SCREEN_TYPE5_DATA Org_451400_data[];
extern SCREEN_TYPE5_DATA Org_452100_data[];
extern SCREEN_TYPE5_DATA Org_452200_data[];
extern SCREEN_TYPE5_DATA Org_452300_data[];
extern SCREEN_TYPE5_DATA Org_452400_data[];
extern SCREEN_TYPE5_DATA Org_452500_data[];
extern SCREEN_TYPE5_DATA Org_452600_data[];
extern SCREEN_TYPE5_DATA Org_452700_data[];
extern SCREEN_TYPE5_DATA Org_452800_data[];
extern SCREEN_TYPE5_DATA Org_453100_data[];
extern SCREEN_TYPE5_DATA Org_453200_data[];
extern SCREEN_TYPE5_DATA Org_453300_data[];
extern SCREEN_TYPE5_DATA Org_453400_data[];
extern SCREEN_TYPE5_DATA Org_453500_data[];
extern SCREEN_TYPE5_DATA Org_454100_data[];
extern SCREEN_TYPE5_DATA Org_454200_data[];
extern SCREEN_TYPE5_DATA Org_455100_data[];
extern SCREEN_TYPE5_DATA Org_455200_data[];
extern SCREEN_TYPE5_DATA Org_455300_data[];
extern SCREEN_TYPE5_DATA Org_455400_data[];
extern SCREEN_TYPE5_DATA Org_541000_data[];
extern SCREEN_TYPE5_DATA Org_614100_data[];
extern SCREEN_TYPE5_DATA Org_615100_data[];
extern SCREEN_TYPE5_DATA Org_616000_data[];
extern SCREEN_TYPE5_DATA Org_617200_data[];
extern SCREEN_TYPE5_DATA Org_618100_data[];
extern SCREEN_TYPE5_DATA Org_618200_data[];
extern SCREEN_TYPE5_DATA Org_662000_data[];
extern SCREEN_TYPE5_DATA Org_663000_data[];
extern SCREEN_TYPE5_DATA Org_664000_data[];
extern SCREEN_TYPE5_DATA Org_243000_data[];

extern SCREEN_TYPE6_DATA Org_225000_data[];
extern SCREEN_TYPE6_DATA Org_232000_data[];
extern SCREEN_TYPE6_DATA Org_242000_data[];
extern SCREEN_TYPE6_DATA Org_244000_data[];
extern SCREEN_TYPE6_DATA Org_253000_data[];

//extern SCREEN_TYPE9_DATA Org_311000_data[];
//extern SCREEN_TYPE9_DATA Org_321000_data[];
extern SCREEN_TYPE9_DATA Org_323000_data[];
extern SCREEN_TYPE9_DATA Org_330000_data[];
extern SCREEN_TYPE9_DATA Org_333000_data[];
extern SCREEN_TYPE9_DATA Org_335000_data[];
extern SCREEN_TYPE10_DATA Org_314000_data[];
extern SCREEN_TYPE10_DATA Org_325000_data[];
extern SCREEN_TYPE9_DATA Org_341200_data[];
extern SCREEN_TYPE10_DATA Org_341400_data[];
extern SCREEN_TYPE9_DATA Org_342200_data[];
extern SCREEN_TYPE10_DATA Org_342400_data[];
extern SCREEN_TYPE10_DATA Org_351000_data[];
extern SCREEN_TYPE11_DATA Org_351100_data[];
extern SCREEN_TYPE10_DATA Org_351200_data[];

extern RUNMODE_SCREEN_DATA Org_761000_data[];
extern GUI_CONST_STORAGE GUI_BITMAP bmadmin;
extern GUI_CONST_STORAGE GUI_BITMAP bmanalogoutput;
extern GUI_CONST_STORAGE GUI_BITMAP bmBeltloadcontrol;
extern GUI_CONST_STORAGE GUI_BITMAP bmblending2;
extern GUI_CONST_STORAGE GUI_BITMAP bmcalibration;
extern GUI_CONST_STORAGE GUI_BITMAP bmcommunications;
extern GUI_CONST_STORAGE GUI_BITMAP bmcontrol;
extern GUI_CONST_STORAGE GUI_BITMAP bmdevicesetup;
extern GUI_CONST_STORAGE GUI_BITMAP bmdigitalcalibration;
extern GUI_CONST_STORAGE GUI_BITMAP bmdual;
extern GUI_CONST_STORAGE GUI_BITMAP bmdualdims;
extern GUI_CONST_STORAGE GUI_BITMAP bmfirmware;
extern GUI_CONST_STORAGE GUI_BITMAP bmgreenethernetactivityicon;
extern GUI_CONST_STORAGE GUI_BITMAP bmhttp_image;


extern GUI_CONST_STORAGE GUI_BITMAP bmreports_diagnostics;
extern GUI_CONST_STORAGE GUI_BITMAP bmscalesetup;
extern GUI_CONST_STORAGE GUI_BITMAP bmwizard;
extern GUI_CONST_STORAGE GUI_BITMAP bmgreenunlockicon;
extern GUI_CONST_STORAGE GUI_BITMAP bmI_O;
extern GUI_CONST_STORAGE GUI_BITMAP bmlenthcal;
extern GUI_CONST_STORAGE GUI_BITMAP bmmaterialtest;
extern GUI_CONST_STORAGE GUI_BITMAP bmpassword;
extern GUI_CONST_STORAGE GUI_BITMAP bmplantconnect_blackbackgroundnotext;
extern GUI_CONST_STORAGE GUI_BITMAP bmprinter;
extern GUI_CONST_STORAGE GUI_BITMAP bmquad;
extern GUI_CONST_STORAGE GUI_BITMAP bmquaddims;
extern GUI_CONST_STORAGE GUI_BITMAP bmratecontrol;
extern GUI_CONST_STORAGE GUI_BITMAP bmredlock;
extern GUI_CONST_STORAGE GUI_BITMAP bmrelay;
extern GUI_CONST_STORAGE GUI_BITMAP bmrunmode;
extern GUI_CONST_STORAGE GUI_BITMAP bmscoreboard;
extern GUI_CONST_STORAGE GUI_BITMAP bmsecurity;
extern GUI_CONST_STORAGE GUI_BITMAP bmsensors;
extern GUI_CONST_STORAGE GUI_BITMAP bmsettings;
extern GUI_CONST_STORAGE GUI_BITMAP bmSingle;
extern GUI_CONST_STORAGE GUI_BITMAP bmsingledims;
extern GUI_CONST_STORAGE GUI_BITMAP bmsmallgreenethernetactivityicon;
extern GUI_CONST_STORAGE GUI_BITMAP bmUSB_small;
extern GUI_CONST_STORAGE GUI_BITMAP bmUSB_red_small;								//sks
extern GUI_CONST_STORAGE GUI_BITMAP bmtestweight;
extern GUI_CONST_STORAGE GUI_BITMAP bmtriple;
extern GUI_CONST_STORAGE GUI_BITMAP bmtripledims;
extern GUI_CONST_STORAGE GUI_BITMAP bmusb;
extern GUI_CONST_STORAGE GUI_BITMAP bmvoltages;
extern GUI_CONST_STORAGE GUI_BITMAP bmzeroCal;
extern GUI_CONST_STORAGE GUI_BITMAP bmAnalogInput;
extern GUI_CONST_STORAGE GUI_BITMAP bmdigitalinputs;
extern GUI_CONST_STORAGE GUI_BITMAP bmdigitaloutput;
extern GUI_CONST_STORAGE GUI_BITMAP bmblack_image;
//extern GUI_CONST_STORAGE GUI_BITMAP bmHTTP_IMAGE;

extern GUI_CONST_STORAGE GUI_FONT GUI_FontArial_Unicode_MS16_Bold;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontArial_Unicode_MS24_Bold;
extern GUI_CONST_STORAGE GUI_FONT GUI_FontArialUnicodeMS72_Bold;

extern PASSWORD_STRUCT_DEF password_struct;
extern const STRING_DATA Strings_english[],Strings_polish[];
extern const STRING_DATA * Strings;
extern ADMIN_STRUCT         Admin_var;
extern SCALE_SETUP_STRUCT   Scale_setup_var;
extern CALIBRATION_STRUCT   Calibration_var;
extern SETUP_DEVICES_STRUCT Setup_device_var;
extern RPRTS_DIAG_STRUCT    Rprts_diag_var;
extern PCM_STRUCT         Pcm_var;

extern UNITS_STRUCT         Units_var;
extern TOTALS_STRUCT    		Totals_var;
extern MISC_STRUCT         Misc_var;
extern char *Units[];
extern int PID_Old_Local_Setpoint;
//extern CALC_STRUCT Calculation_struct;
extern FILE_UPDATE_STRUCT Filename;
extern char MAC_String[18];
extern GUI_DATA_NAV_STRUCT GUI_data_nav;
// subwizard testing
extern PARENT_WIZARD parent_wizard;
//----------------------------------------------------------------------------------------------------
extern int _OwnerDraw_Screen23456(const WIDGET_ITEM_DRAW_INFO * pDrawItemInfo);
extern void CreateWindow_screen1(SCREEN_TYPE1_DATA *icon_bitmaps, int count);
extern void CreateWindow_screen2(SCREEN_TYPE2_DATA *data, int count);
extern void CreateWindow_screen3(SCREEN_TYPE3_DATA *data, int count);
extern void CreateWindow_screen4(SCREEN_TYPE4_DATA *data, int count);
extern void CreateWindow_screen5(SCREEN_TYPE5_DATA *data, int count);
extern void CreateWindow_screen6(SCREEN_TYPE6_DATA *data, int count);
extern void CreateWindow_screen9(SCREEN_TYPE9_DATA *data, int count);
extern void CreateWindow_screen10(SCREEN_TYPE10_DATA *data, int count);
extern void CreateWindow_screen11(SCREEN_TYPE11_DATA *data, int count);
extern void CreateWindow_runmode_screen(int create_update);
extern void Create_Weight_Run_mode_screen(int create_update_weight_screen_flag);
extern void Create_loadout_screen (int create_update);
extern void Create_blending_load_rate_ctrl_screen (int create_update);
extern void Create_pcm_main_screen(int create_update_weight_screen_flag);

extern void multiedit_insert_txt(int type, void * data);
extern void custom_sprintf(int data_type, void * data, char * char_data1, int size);
extern void custom_sscanf(int data_type, void * data, char * char_data1);
extern void format_date(char * char_data);
extern void format_time(char * char_data);
extern U8 security_bit_set(void);
extern void go_back_one_level(void);
extern void read_files_on_usb (int type);
extern void set_beltway_printer_param (void);
extern void set_beltway_scoreboard_param (void);
extern void set_default_eng_metric_units (unsigned int prev_dist_unit);
extern void set_weight_unit(int sel);
extern void switch_integrator_master_slave_tasks(void);
extern void change_ip_address (void);
extern void dhcp_check (void);
extern void dhcp_chk_enable_disable (void) ;
extern float Unit_per_pulse_value (char * Unit_per_pulse_string);

/*============================================================================
* Public Function Declarations
*===========================================================================*/

#endif
/*****************************************************************************
* End of file
*****************************************************************************/
