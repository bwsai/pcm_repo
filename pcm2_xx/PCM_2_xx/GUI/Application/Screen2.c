/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Screen2.c
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_global_ex.h"
#include "Screen_data_enum.h"
#include "File_update.h"
//#include "Log_report_data_calculate.h"
#include "Ext_flash_high_level.h"
#include "RTOS_main.h"
//#include "Calibration.h"
#include "USB_main.h"
#include "Global_ex.h"
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define TODAY1
/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */
static unsigned char firmware_file_status = 0;
/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/
static void callback_screen2(WM_MESSAGE * pMsg);
/*============================================================================
* Function Implementation Section
*===========================================================================*/

/*****************************************************************************
* @note       Function name  : static void callback_screen2(WM_MESSAGE * pMsg)
* @returns    returns        : None
* @param      arg1           : message sent by the window manager
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Used to process the messages sent to the window manager
*                            : based on the user input i.e key press.
* @note       Notes          :
*****************************************************************************/
static void callback_screen2(WM_MESSAGE * pMsg)
{
    int Id, NCode, sel, Key_entry, i, length,system_unlocked = 0;
	  //PVK - 13 Jan 2016
	  int Clear_Total_Flag = 0;
    //int i;
    Screen_org_data * new_screen_org;
    static SCREEN_TYPE2_DATA *data;
    float current_ver, new_ver;

    sel = LISTBOX_GetSel(GUI_data_nav.Listbox);
    data = GUI_data_nav.Current_screen_info->screen_data;

    switch (pMsg->MsgId)
    {
        case WM_NOTIFY_PARENT:
            Id    = WM_GetId(pMsg->hWinSrc);
            NCode = pMsg->Data.v;
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch(NCode)
                    {
                    case WM_NOTIFICATION_CLICKED:
                        break;

                    case WM_NOTIFICATION_RELEASED:
                        break;

                    case WM_NOTIFICATION_SEL_CHANGED:
                        break;
                    }
                    break;
            }
            break;

        case WM_KEY:
            Id = WM_GetId(pMsg->hWinSrc);
            Key_entry = (((WM_KEY_INFO*)(pMsg->Data.p))->Key);
            switch(Id)
            {
                case ID_LISTBOX_1: // Notifications sent by 'Listbox'
                    switch (Key_entry)
                    {
                        case GUI_KEY_LEFT_CUSTOM:
                            ;
                           break;
                        case GUI_KEY_CLEAR:
// 													if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, TOTALS_SCREEN_ORG_NO)== 0)
// 							            {
// 													  switch(sel)
// 											      {
// 															case 0: Rprts_diag_var.Job_Total = 0.0;
// 																			Totals_var.Job_Total = 0.0;
// 															        Clear_Total_Flag = 1;
// 															        break;
// 															case 1: 
// 																			//Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix
// 																			/*populate the periodic log for Daily weight and update on the USB drive, if enabled*/
// 																			if (Setup_device_var.Log_clear_wt_data == Screen431_str2)
// 																			{
// //																				log_data_populate(daily_wt_clr_log);
// 																			}																	
// 																			Rprts_diag_var.daily_Total_weight = 0.0;
// 																			Totals_var.daily_Total_weight = 0.0;
// 																			//Added on 20-Nov-2015 to log clear daily weight entry as per BS-120 fix
// 																			/*populate the periodic log for cleared Daily weight and update on the USB drive, if enabled*/
// 																			if (Setup_device_var.Log_run_time_data == Screen431_str2)
// 																			{ 
// //																				log_data_populate(clear_log);
// 																			}
//                                       Clear_Total_Flag = 1;																			
// 															        break;
// 														  case 2: Rprts_diag_var.weekly_Total_weight = 0.0;
// 																			Totals_var.weekly_Total_weight = 0.0;
// 																			Clear_Total_Flag = 1;
// 															        break;
// 															case 3: Rprts_diag_var.monthly_Total_weight = 0.0;
// 																			Totals_var.monthly_Total_weight = 0.0;
// 																			Clear_Total_Flag = 1;
// 															        break;
// 															case 4: Rprts_diag_var.yearly_Total_weight = 0.0;
// 																			Totals_var.yearly_Total_weight = 0.0;
// 																			Clear_Total_Flag = 1;
// 															        break;
// 														  case 5: Rprts_diag_var.Master_total = 0.0;
// 																			Totals_var.Master_total = 0.0;
// 																			Clear_Total_Flag = 1;
// 															        break;
// // 															case 6:	
// // 																			Backup_var_into_usb();
// // 															
// // 																			break;
// 														 default: break;
// 														}
// 														//PVK - 13 Jan 2016
// 														if(Clear_Total_Flag == 1)
// 														{	

// 														}	
// 													 }
													 break;
                        case GUI_KEY_LEFT:
                            ;
                            break;

                        case GUI_KEY_UP:
                            ;
                            break;

                        case GUI_KEY_UP_CUSTOM:
                            ;
                            break;

                        case GUI_KEY_DOWN:
                            ;
                            break;
												case GUI_KEY_RIGHT:
                        case GUI_KEY_RIGHT_CUSTOM:
                        case GUI_KEY_ENTER:
//													if(sel ==5){
//														    GUI_DispStringAt("                                                                                  ", WINDOW_FOOTER_TEXT2_POS_X0, WINDOW_FOOTER_TEXT2_POS_Y0);
//
//														GUI_DispStringAt("Message: Writing to USB.......", WINDOW_FOOTER_TEXT1_POS_X0, WINDOW_FOOTER_TEXT1_POS_Y0);
//														Backup_var_into_usb();//go_back_one_level();
//														GUI_data_nav.Back_one_level = GO_BACK_FLAG;														
//													 GUI_data_nav.Wizard_screen_index = 0;
//													 GUI_data_nav.Key_press = 1;
//													 GUI_data_nav.Cancel_operation = 0;														
//														}
                            data[0].selection = LISTBOX_GetSel(GUI_data_nav.Listbox);
														if((data[sel].screen_an_option != 0) &&
															((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, PCM_SETUP_ORG_NO) == 0)))
// 														||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, LOAD_CELL_SIZE_SCREEN) == 0)   ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, "220000") == 0)   						 ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DISTANCE_UNIT_SCREEN) == 0)    ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, WEIGHT_UNIT_SCREEN) == 0)      ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RATE_UNIT_SELECT_ORG_NO) == 0) ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, "270000") == 0) 							 ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, "280000") == 0) 							 ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, "290000") == 0) 							 ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, DECIMAL_PLACES_ORG_NO) == 0) 	 ||
// 															 (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, OPTIONAL_IO_BOARD_INTALL_ORG_NO) == 0)
// 															))
														{
															if(!security_bit_set())
															{
																	system_unlocked = 1;
															}
															else
															{
																	if(password_struct.password_entered)
																	{
																			system_unlocked = 1;
																	}
															}
															if(system_unlocked)
															{
																	new_screen_org = GUI_data_nav.Current_screen_info->child;
																	new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
																	for (i = 0; i < 6; i++)
																	{
																			GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
																	}
																	GUI_data_nav.Change = 1;
																	GUI_data_nav.Child_present = 1;
															}
															else
															{
																	//create the password screen, store the current screen org number to come back to it
																	//in case the password entered is correct
																	GUI_data_nav.Screen_before_pw_org_num = GUI_data_nav.Current_screen_info->child;
																	GUI_data_nav.Screen_before_pw_org_num = &GUI_data_nav.Screen_before_pw_org_num[(data[sel].screen_an_option - 1)];
																	GUI_data_nav.Change = GUI_data_nav.Key_press = 0;
																	password_struct.password_to_be_entered = 1;
															}													
														}
                            else if ((data[sel].screen_an_option != 0) &&
															  (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) != 0) &&
								                (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) != 0))
                            {
																if(data[0].selection != 8)//Added by PT on January 2, 2016 for temporarily disabling the Backup/Restore key operation as suggested by Jonathan through mail dated, December 31, 2015.
																{
																	new_screen_org = GUI_data_nav.Current_screen_info->child;
																	new_screen_org = &new_screen_org[(data[sel].screen_an_option - 1)];
																	for (i = 0; i < 6; i++)
																	{
																			GUI_data_nav.New_screen_num[i] = new_screen_org->Screen_org_no[i];
																	}
																	if(strncmp(GUI_data_nav.New_screen_num, PASSWORD_SCREEN_ORG_NO,6) == 0)
																	{
																		 GUI_data_nav.In_wizard = __TRUE;
																	}
																	GUI_data_nav.Change = 1;
																	GUI_data_nav.Child_present = 1;
																}
                            }														
                            else
                            {
                                GUI_data_nav.Child_present = 0;
															  if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) == 0)
																{
																	if(firmware_file_status != 1)
																	{
																		//modify the file name w.r.t the selected file name and also modify
																		//the update integrator ver num variable. Compare the version number
																		//with the current version and warn if the version is older(641311)
																		GUI_data_nav.Back_one_level = 0;
																		custom_sscanf(data[0].data_type, data[0].Variable3, Filename.name[sel]);
																		length = strlen(data[0].Variable3);
																		for (i = 0; i < length; i++)
																		{
																				if(Pcm_var.Int_update_file_name[i] == '_' && Pcm_var.Int_update_file_name[i-1] == 'r')
																				{
																						Pcm_var.Int_update_version[0] = Pcm_var.Int_update_file_name[i+1];
																						Pcm_var.Int_update_version[1] = Pcm_var.Int_update_file_name[i+2];
																						Pcm_var.Int_update_version[2] = '.';
																						Pcm_var.Int_update_version[3] = Pcm_var.Int_update_file_name[i+3];
																						Pcm_var.Int_update_version[4] = Pcm_var.Int_update_file_name[i+4];
																						break;
																				}
																		}
																		current_ver = atof(Pcm_var.Int_firmware_version);
																		new_ver = atof(Pcm_var.Int_update_version);
																		if(current_ver > new_ver)
																		{
																				strcpy(GUI_data_nav.New_screen_num, INT_OLD_VER_WRN_ORG_NO);
																				GUI_data_nav.Change = 1;
																		}
																		else
																		{
																				strcpy(GUI_data_nav.New_screen_num, FIRMWARE_UPGRADE_ORG_NO);
																				GUI_data_nav.Change = 1;
																		}
																	}
																	else
																	{ 
																		 firmware_file_status = 0;
																		 GUI_data_nav.Wizard_screen_index = 0;
																		 GUI_data_nav.Back_one_level = GO_BACK_LOOP_FLAG;
																		 GUI_data_nav.Key_press = 1;
                                     GUI_data_nav.Cancel_operation = 0;
																	}
																}
																else if(strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) == 0)
																{
																	 //modify the file name w.r.t the selected file name
																		custom_sscanf(data[0].data_type, data[0].Variable3, Filename.name[sel]);
																}
                            }
                            GUI_data_nav.Key_press = 1;
                            break;

                        case GUI_KEY_BACK:
                            data[0].selection = 0; 

															go_back_one_level();

                            break;
                    }
                break;
            }
            break;

        default:
            WM_DefaultProc(pMsg);
            break;
    }
    return;
}

/*****************************************************************************
* @note       Function name  : void CreateWindow_screen2(SCREEN_TYPE2_DATA *data, int count)
* @returns    returns        : None
* @param      arg1           : pointer to the screen data structure
*             arg2           : count of the listbox items to be drawn
* @author                    : Anagha Basole
* @date       date created   : 6th November 2012
* @brief      Description    : Creates the listbox screen and sets the default settings
*                            : of the listbox.
* @note       Notes          :
*****************************************************************************/
void CreateWindow_screen2(SCREEN_TYPE2_DATA *data, int count)
{
    int i;
	  #ifdef TODAY1
	  float New_Wt_Per_Pulse = 0.1;
	  #endif

    GUI_data_nav.Listbox = LISTBOX_CreateEx (LISTBOX_START, MULTI_WIN_OFFSET, LISTBOX2_SIZE_X, LISTBOX2_SIZE_Y,
                                             WM_HBKWIN, WM_CF_SHOW, 0, ID_LISTBOX_1, 0);
	  if ((strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) == 0) ||
        (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, RESTORE_USB_FILE_VIEW_ORG_NO) == 0))
    {
        if (strcmp(GUI_data_nav.Current_screen_info->Screen_org_no, INT_UPDT_FILE_VIEW_ORG_NO) == 0)
        {
           read_files_on_usb (1);
        }
        else
        {
           read_files_on_usb (2);
        }
				if(Filename.count)
				{
						for (i = 0; i < Filename.count; i++)
						{
								LISTBOX_AddString(GUI_data_nav.Listbox, Filename.name[i]);
						}
			  }
				else
				{
					  firmware_file_status = 1;
					  LISTBOX_AddString(GUI_data_nav.Listbox, Strings[No_firmware_file_Str].Text);
				}
    }
		else
		{
			  for (i = 0; i < count; i++)
				{
						if (data[i].Main_text_index != 0)
						{
								LISTBOX_AddString(GUI_data_nav.Listbox, Strings[data[i].Main_text_index].Text);
						}
				}
		}
    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_WHITE);
    LISTBOX_SetTextColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_BLACK);
    LISTBOX_SetFont (GUI_data_nav.Listbox, &GUI_FontArial_Unicode_MS24_Bold);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_UNSEL, GUI_BLACK);
    LISTBOX_SetBkColor (GUI_data_nav.Listbox, LISTBOX_CI_SELFOCUS, GUI_LIGHTYELLOW);
    LISTBOX_SetItemSpacing(GUI_data_nav.Listbox, 30);
    LISTBOX_SetAutoScrollV (GUI_data_nav.Listbox, 1);
    LISTBOX_SetAutoScrollH (GUI_data_nav.Listbox, 0);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_THUMB, GUI_BLACK);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_SHAFT, GUI_LIGHTYELLOW);
    LISTBOX_SetScrollbarColor(GUI_data_nav.Listbox, SCROLLBAR_CI_ARROW, GUI_BLACK);
    LISTBOX_SetScrollbarWidth(GUI_data_nav.Listbox, 4);
    WM_SetCallback(WM_HBKWIN, callback_screen2);
    LISTBOX_SetOwnerDraw(GUI_data_nav.Listbox, _OwnerDraw_Screen23456);
    WM_SetFocus(GUI_data_nav.Listbox);
    LISTBOX_SetSel(GUI_data_nav.Listbox, data[0].selection);
    return;
}
/*****************************************************************************
* End of file
*****************************************************************************/
