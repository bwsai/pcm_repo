/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Weight_rate_run_mode_screen.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Friday, November 23, 2012, 10:00:00 AM
* @date Last Modified  : Friday, November 23, 2012, 5:10:52 PM
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef  WEIGHT_RATE_RUN_MODE_SCREEN
#define  WEIGHT_RATE_RUN_MODE_SCREEN

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "Screen_structure.h"
#include "Screen_data_enum.h"
#include "Sensor_board_data_process.h"

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/

__align(4) RUNMODE_SCREEN_DATA Org_700000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen7_str1, Screen7_str2, Screen7_str3,NULL,
			 &Calculation_struct.Total_weight,  &Calculation_struct.Rate_for_display,  &Calculation_struct.Belt_speed,0,
         &Scale_setup_var.Weight_unit,  &Scale_setup_var.Rate_time_unit,  &Scale_setup_var.Speed_unit,0},
};
// There is no screen for 710000 from this version if customer wants it back please remove type casting
__align(4) RUNMODE_SCREEN_DATA Org_710000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen71_str1, Screen71_str2, Screen7_str3,NULL,
			(double *) &Calculation_struct.Rate_for_display, (float *) &Calculation_struct.Total_weight,  &Calculation_struct.Belt_speed,0,
         &Scale_setup_var.Rate_time_unit,  &Scale_setup_var.Weight_unit,  &Scale_setup_var.Speed_unit,0},
			 };
			 
__align(4) RUNMODE_SCREEN_DATA Org_760000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen7_str1, Screen7_str2, Screen76_str1,Screen76_str2,
			&Calculation_struct.Total_weight,  &Calculation_struct.Rate_for_display,  &Calculation_struct.Rate,&Calculation_struct.run_time,
         &Scale_setup_var.Weight_unit,  &Scale_setup_var.Rate_time_unit,  &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_761000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str3, Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[0],  &Rprts_diag_var.Avg_rate_for_mode[0],  &Rprts_diag_var.RunTime[0],0,
         &Rprts_diag_var.LastCleared_Weight_unit[0],  &Rprts_diag_var.LastCleared_Rate_time_unit[0], &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_762000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str4, Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[1],  &Rprts_diag_var.Avg_rate_for_mode[1],  &Rprts_diag_var.RunTime[1],0,
         &Rprts_diag_var.LastCleared_Weight_unit[1],  &Rprts_diag_var.LastCleared_Rate_time_unit[1],  &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_763000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str5, Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[2],  &Rprts_diag_var.Avg_rate_for_mode[2],  &Rprts_diag_var.RunTime[2],0,
         &Rprts_diag_var.LastCleared_Weight_unit[2],  &Rprts_diag_var.LastCleared_Rate_time_unit[2],  &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_764000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str6, Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[3],  &Rprts_diag_var.Avg_rate_for_mode[3],  &Rprts_diag_var.RunTime[3],0,
         &Rprts_diag_var.LastCleared_Weight_unit[3],  &Rprts_diag_var.LastCleared_Rate_time_unit[3],  &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_765000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str7,  Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[4],  &Rprts_diag_var.Avg_rate_for_mode[4],  &Rprts_diag_var.RunTime[4],0,
         &Rprts_diag_var.LastCleared_Weight_unit[4],  &Rprts_diag_var.LastCleared_Rate_time_unit[4],  &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_766000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str8,  Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[5],  &Rprts_diag_var.Avg_rate_for_mode[5],  &Rprts_diag_var.RunTime[5],0,
         &Rprts_diag_var.LastCleared_Weight_unit[5],  &Rprts_diag_var.LastCleared_Rate_time_unit[5],  &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_767000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str9,  Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[6],  &Rprts_diag_var.Avg_rate_for_mode[6],  &Rprts_diag_var.RunTime[6],0,
         &Rprts_diag_var.LastCleared_Weight_unit[6],  &Rprts_diag_var.LastCleared_Rate_time_unit[6],  &Scale_setup_var.Rate_time_unit,0},
};

__align(4) RUNMODE_SCREEN_DATA Org_768000_data[] __attribute__ ((section ("GUI_DATA_RAM"))) = {
    {Screen76_str10,  Screen76_str1,Screen76_str2,Screen7_str2,
			&Rprts_diag_var.LastClearedTotal[7],  &Rprts_diag_var.Avg_rate_for_mode[7],  &Rprts_diag_var.RunTime[7],0,
         &Rprts_diag_var.LastCleared_Weight_unit[7],  &Rprts_diag_var.LastCleared_Rate_time_unit[7],  &Scale_setup_var.Rate_time_unit,0},
};


#endif
/*****************************************************************************
* End of file
*****************************************************************************/
