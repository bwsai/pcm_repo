/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename	 	   : Screen1.c
* @brief			 	  		 : Controller Board
*
* @author		       	   : Anagha Basole
*
* @date Created		     : November, 2012  
* @date Last Modified  : November, 2012  
*
* @internal Change Log : <YYYY-MM-DD>
* @internal 		  		 : 
* @internal 		   		 :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
//#include "Screen_global_ex.h"

/*============================================================================
* Private Macro Definitions
*===========================================================================*/

/*============================================================================
* Private Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/
/*----------------------------------------------------------------------
File        : GUIConf.c
Purpose     : Display controller initialization
---------------------------END-OF-HEADER------------------------------
*/

#include "GUI.h"

/*********************************************************************
*
*       Defines
*
**********************************************************************
*/
//
// Define the available number of bytes available for the GUI
//
#define GUI_NUMBYTES  (0x3200)  // x MByte

//
// Define the average block size
//
#define GUI_BLOCKSIZE 0x80

/*********************************************************************
*
*       Static data
*
**********************************************************************
*/

//
// 32 bit aligned memory area
//

#ifdef __CC_ARM
	U16 static _aMemory[GUI_NUMBYTES / 2] __attribute__ ((section ("GUI_RAM"), zero_init));
#endif

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
/*********************************************************************
*
*       GUI_X_Config
*
* Purpose:
*   Called during the initialization process in order to set up the
*   available memory for the GUI.
*/
void GUI_X_Config(void) 
{
  // Assign memory to emWin
  GUI_ALLOC_AssignMemory(_aMemory, GUI_NUMBYTES);
  GUI_ALLOC_SetAvBlockSize(GUI_BLOCKSIZE);
}

/*************************** End of file ****************************/
