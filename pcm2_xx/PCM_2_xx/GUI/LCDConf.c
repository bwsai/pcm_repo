/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename        : Screen1.c
* @brief                : Controller Board
*
* @author                : Anagha Basole
*
* @date Created         : November, 2012
* @date Last Modified  : November, 2012
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal             :
*
*****************************************************************************/

/*============================================================================
* Include Header Files
*===========================================================================*/
#include "GUI.h"
#include <stddef.h>
#include "GUIDRV_Lin.h"
#include "LPC177x_8x.h"
#include "system_LPC177x_8x.h"
#include "LCDConf.h"
#include <string.h>
/*============================================================================
* Private Macro Definitions
*===========================================================================*/
#define LCD_BACKLIGHT_BIT 	1
/*============================================================================
* Private Data Types
*===========================================================================*/
uint32_t sys_tick;
/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Private Function Prototypes Declarations
*===========================================================================*/

/*============================================================================
* Function Implementation Section
*===========================================================================*/


/*********************************************************************
*
*       Layer configuration
*
**********************************************************************
*/
#define LAYER_INDEX 0

//
// Physical display size
//
#define XSIZE_PHYS     480
#define YSIZE_PHYS     272

//
// Color conversion
//
#define COLOR_CONVERSION   GUICC_565//GUICC_444_12  //GUICC_565

//
// Pixel width in bytes
//
#define PIXEL_WIDTH  2 //in bytes
#define BITSPERPIXEL  (PIXEL_WIDTH * 8) //bits per pixel

//
// Display driver
//
#define DISPLAY_DRIVER   &GUIDRV_Lin_16_API//GUIDRV_CompactColor_16//&GUIDRV_Lin_16_API //GUIDRV_FLEXCOLOR


//
// Video RAM address
//
#define VRAM_ADDR_PHYS  (U32)&_aVRAM[0]

//#define MATRIX_ARB  (*(volatile U32*)(0x400FC188))

#define BCD_BIT     26    // Bypass pixel clock divider

//
// Buffers / VScreens
//


#define NUM_BUFFERS   2
#define NUM_VSCREENS  1

//
// Display orientation
//
#define DISPLAY_ORIENTATION  0

#define HBP_LCD_4_3       43       // Horizontal back porch in clocks
#define HFP_LCD_4_3       8       // Horizontal front porch in clocks
#define HSW_LCD_4_3         3        // HSYNC pulse width in clocks
#define PPL_LCD_4_3       XSIZE_PHYS      // Pixels per line
#define VBP_LCD_4_3       4        // Vertical back porch in clocks
#define VFP_LCD_4_3       4        // Vertical front porch in clocks
#define VSW_LCD_4_3       11        // VSYNC pulse width in clocks
#define LPP_LCD_4_3       YSIZE_PHYS      // Lines per panel
#define IOE_LCD_4_3       0        // Invert output enable, 1 = invert
#define IPC_LCD_4_3       0        // Invert panel clock, 1 = invert
#define IHS_LCD_4_3       1        // Invert HSYNC, 1 = invert
#define IVS_LCD_4_3       1        // Invert VSYNC, 1 = invert
//#define ACB_LCD_4_3       1        // AC bias frequency in clocks (not used)
#define BPP_LCD_4_3       6       // Bits per pixel b110 = 16 bpp 5:6:5 mode
#define CLK_LCD_4_3       9000000  // Optimal clock rate (Hz) between 5-12 MHz according to HDA430-3GH-1 datasheet
#define LCD_LCD_4_3       1        // Panel type; 1: LCD TFT panel
#define DUAL_LCD_4_3      0        // No dual panel
#define BGR_LCD_4_3       0          //R & B not swapped - 0, BGR - 1(R & B swapped)

/*Enable the function type 7 for the LCD and also enable hysteresis*/
#define LCD_VD_HYST_EN       0x07
#define LCD_PWR_HYST_EN     0x07
#define LCD_LE_HYST_EN       0x07-

#define LCD_DCLK_HYST_EN     0x07
#define LCD_FP_HYST_EN       0x07
#define LCD_ENAB_HYST_EN     0x07
#define LCD_LP_HYST_EN       0x07

#define LCD_DISP_ON_OFF_BIT 0

/*********************************************************************
*
*       Configuration checking
*
**********************************************************************
*/
#ifndef   XSIZE_PHYS
  #error Physical X size of display is not defined!
#endif

#ifndef   YSIZE_PHYS
  #error Physical Y size of display is not defined!
#endif

#ifndef   COLOR_CONVERSION
  #error Color conversion not defined!
#endif

#ifndef   DISPLAY_DRIVER
  #error No display driver defined!
#endif

#ifndef   NUM_VSCREENS
  #define NUM_VSCREENS 1
#else
  #if (NUM_VSCREENS <= 0)
    #error At least one screeen needs to be defined!
  #endif
#endif

#if (NUM_VSCREENS > 1) && (NUM_BUFFERS > 1)
  #error Virtual screens and multiple buffers are not allowed!
#endif

#ifndef   DISPLAY_ORIENTATION
  #define DISPLAY_ORIENTATION  0
#endif

#if ((DISPLAY_ORIENTATION & GUI_SWAP_XY) != 0)
  #define LANDSCAPE   1
#else
  #define LANDSCAPE   0
#endif

#if (LANDSCAPE == 1)
  #define WIDTH       YSIZE_PHYS  /* Screen Width (in pixels)         */
  #define HEIGHT      XSIZE_PHYS  /* Screen Hight (in pixels)         */
#else
  #define WIDTH       XSIZE_PHYS  /* Screen Width (in pixels)         */
  #define HEIGHT      YSIZE_PHYS  /* Screen Hight (in pixels)         */
#endif

//
// Video RAM
//__at_0x20000,
#ifdef __CC_ARM

__align(8) static U16 _aVRAM[((XSIZE_PHYS * YSIZE_PHYS * NUM_BUFFERS * BITSPERPIXEL)/8)/2] __attribute__ ((section ("VRAM"), zero_init));

#endif

/*********************************************************************
*
*       Driver Port functions
*
**********************************************************************
*/


/*********************************************************************
*
*       Private code
*
**********************************************************************
*/
/*static void delay (int cnt)
{
  cnt <<= 15;
  while (cnt--);
}*/
/*********************************************************************
*
*       _FindClockDivisor
*
* Function description
*   Find closest clock divider to get the desired clock rate.
*/
static U32 _FindClockDivisor(U32 TargetClock)
{
  U32 Divider;
  U32 r;

  Divider = 1;
  while (((SystemCoreClock / Divider) > TargetClock) && (Divider <= 0x3F)) {
    Divider++;
  }
  if (Divider <= 1) {
    r = (1 << BCD_BIT);  // Skip divider logic if clock divider is 1
  } else {
    //
    // Use found divider
    //
    Divider -= 2;
    r = 0
        | (((Divider >> 0) & 0x1F)
        | (((Divider >> 5) & 0x1F) << 27))
        ;
  }
  return r;
}

/*********************************************************************
*
*       _InitLCDPorts
*
* Function description
*   Initializes the port pins as needed for the LCD.
*/
static void _InitLCDPorts(void)
{
  /* Assign pins*/
  /*******************Port 0**********************************/
  LPC_IOCON->P0_4 = LCD_VD_HYST_EN;   /*LCD Panel Data-LCD_VD[0]*/
  LPC_IOCON->P0_5 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[1]*/
  LPC_IOCON->P0_6 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[8]*/
  LPC_IOCON->P0_7 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[9]*/
  LPC_IOCON->P0_8 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[16]*/
  LPC_IOCON->P0_9 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[17]*/

  /*******************Port 1**********************************/
  /*P1.12*/
  /*P1.13*/
  LPC_IOCON->P1_20 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[10]*/
  LPC_IOCON->P1_21 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[11]*/
  LPC_IOCON->P1_22 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[12]*/
  LPC_IOCON->P1_23 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[13]*/
  LPC_IOCON->P1_24 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[14]*/
  LPC_IOCON->P1_25 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[15]*/
  LPC_IOCON->P1_26 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[20]*/
  LPC_IOCON->P1_27 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[21]*/
  LPC_IOCON->P1_28 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[22]*/
  LPC_IOCON->P1_29 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[23]*/

  /*******************Port 2**********************************/
  LPC_IOCON->P2_0 = LCD_PWR_HYST_EN;  /*LCD panel power enable*/
  LPC_IOCON->P2_1 = 0x00;             /*Backlight*/
  LPC_GPIO2->DIR |= ((1UL<<1));
  LPC_IOCON->P2_2 = LCD_DCLK_HYST_EN; /*LCD panel clock*/
  LPC_IOCON->P2_3 = LCD_FP_HYST_EN;    /*Vertical synchronization pulse (TFT)*/
  LPC_IOCON->P2_4 = LCD_ENAB_HYST_EN; /*TFT data enable output*/
  LPC_IOCON->P2_5 = LCD_LP_HYST_EN;   /*Horizontal synchronization pulse (TFT)*/
  LPC_IOCON->P2_6 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[4]*/
  LPC_IOCON->P2_7 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[5]*/
  LPC_IOCON->P2_8 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[6]*/
  LPC_IOCON->P2_9 = LCD_VD_HYST_EN;    /*LCD Panel Data-LCD_VD[7]*/
  LPC_IOCON->P2_12 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[18]*/
  LPC_IOCON->P2_13 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[19]*/

  /*******************Port 4**********************************/
  LPC_IOCON->P4_28 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[2]*/
  LPC_IOCON->P4_29 = LCD_VD_HYST_EN;  /*LCD Panel Data-LCD_VD[3]*/
}

/*********************************************************************
*
*       _InitLCDC
*
* Function description
*   Inits the LCD controller, backlight and sets the frame buffer.
*/

static void _InitLCDC(void) {
  U32 i;

  //
  // Init LCDC
  //
  LPC_SC->PCONP |=  (1UL << 0);  // Power the LCDC
  LPC_LCD->CTRL &= ~(1UL << 0);  // Disable the LCDC
  LPC_LCD->TIMH  = 0             // Configure horizontal axis
                   | ((((U32)PPL_LCD_4_3 / 16) - 1) <<  2)
                   |  (((U32)HSW_LCD_4_3 - 1)       <<  8)
                   |  (((U32)HFP_LCD_4_3 - 1)       << 16)
                   |  (((U32)HBP_LCD_4_3 - 1)       << 24)
                   ;
  LPC_LCD->TIMV  = 0             // Configure vertical axis
                   | (((U32)LPP_LCD_4_3 - 1) <<  0)
                   | (((U32)VSW_LCD_4_3 - 1) << 10)
                   | (((U32)VFP_LCD_4_3)     << 16)
                   | (((U32)VBP_LCD_4_3)     << 24)
                   ;
  LPC_LCD->POL   = 0             // Configure clock and signal polarity
                   | (_FindClockDivisor(CLK_LCD_4_3) <<  0)
                   /*| (((U32)ACB_LCD_4_3 - 1)         <<  6)*/
                   | (((U32)IVS_LCD_4_3)             << 11)
                   | (((U32)IHS_LCD_4_3)             << 12)
                   | (((U32)IPC_LCD_4_3)             << 13)
                   | (((U32)PPL_LCD_4_3 - 1)         << 16)
                   ;
  LPC_LCD->CTRL  = 0             // Configure operating mode and panel parameters
                   | ((U32)BPP_LCD_4_3 << 1)
                   | ((U32)BGR_LCD_4_3 << 8)
                   | ((U32)LCD_LCD_4_3 << 5)
                   ;

  i          =  GUI_COUNTOF(LPC_LCD->PAL);
  for (i = 0; i < GUI_COUNTOF(LPC_LCD->PAL); i++)
  {
    LPC_LCD->PAL[i] = 1;  // Clear the color palette with black
  }
  LPC_SC->LCD_CFG = 0x0;  // No panel clock prescaler
  //
  // Enable LCDC
  //
  LPC_LCD->UPBASE  = VRAM_ADDR_PHYS;
  //delay(10);

  LPC_LCD->CTRL   |= (1 <<  0);                 // Enable LCD signals
  //delay(10);
  LPC_LCD->CTRL   |= (1 << 11);                 // Enable LCD power

  //Turn on the backlight
  LPC_GPIO2->SET |= (1 << LCD_BACKLIGHT_BIT); // Set backlight to on
}


/*********************************************************************
*
*       _InitLCD
*
* Function description
*   Initializes the port pins as needed for the LCD and selects
*   an available LCD.
*/

static void _InitLCD(void)
{
  _InitLCDPorts();
  //
  // Setup BUS priorities
  //
  /*
  #if 0
  MATRIX_ARB = 0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
               | (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
               | (2 <<  2)  // PRI_DCODE : D-Code bus priority.
               | (0 <<  4)  // PRI_SYS   : System bus priority.
               | (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
               | (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
               | (3 << 10)  // PRI_LCD   : LCD DMA priority.
               | (0 << 12)  // PRI_USB   : USB DMA priority.
               ;
  #else
  MATRIX_ARB = 0            // Set AHB Matrix priorities [0..3] with 3 being highest priority
               | (1 <<  0)  // PRI_ICODE : I-Code bus priority. Should be lower than PRI_DCODE for proper operation.
               | (2 <<  2)  // PRI_DCODE : D-Code bus priority.
               | (0 <<  4)  // PRI_SYS   : System bus priority.
               | (0 <<  6)  // PRI_GPDMA : General Purpose DMA controller priority.
               | (0 <<  8)  // PRI_ETH   : Ethernet: DMA priority.
               | (3 << 10)  // PRI_LCD   : LCD DMA priority.
               | (0 << 12)  // PRI_USB   : USB DMA priority.
               | (1 << 16)  // ROM_LAT   : ROM latency select.
               ;
  #endif*/
  //
  // Initialize Display
  //
  _InitLCDC();

}

/*********************************************************************
*
*       _InitController
*
* Purpose:
*   Initializes the display controller
*/
static void _InitController(void)
{

  _InitLCD();
  //
  // Set display size and video-RAM address
  //
  LCD_SetSizeEx  (LAYER_INDEX, XSIZE_PHYS , YSIZE_PHYS);
  LCD_SetVSizeEx (LAYER_INDEX, XSIZE_PHYS, YSIZE_PHYS);
  LCD_SetVRAMAddrEx(LAYER_INDEX, (void*)VRAM_ADDR_PHYS);
}

/*********************************************************************
*
*       Public code
*
**********************************************************************
*/
static void _CopyBuffer(int LayerIndex, unsigned long IndexSrc, unsigned long IndexDst)
{
unsigned long BufferSize, AddrSrc, AddrDst;

// Calculate the size of one frame buffer
//
BufferSize = (XSIZE_PHYS * YSIZE_PHYS * BITSPERPIXEL ) / 8;

  //
// Calculate source- and destination address
//
AddrSrc = VRAM_ADDR_PHYS + BufferSize * IndexSrc;
AddrDst = VRAM_ADDR_PHYS + BufferSize * IndexDst;
memcpy((void *)AddrDst, (void *)AddrSrc, BufferSize);
}
/*********************************************************************
*
*       LCD_X_Config
*
* Purpose:
*   Called during the initialization process in order to set up the
*   display driver configuration.
*
*/
void LCD_X_Config(void)
{
  //Initialize use of multiple buffers
  GUI_MULTIBUF_Config(NUM_BUFFERS);

  //Creates a display driver device and associates the color conversion
  //routines to be used
  GUI_DEVICE_CreateAndLink(DISPLAY_DRIVER, COLOR_CONVERSION, 0, 0);

  //Display driver configuration, required for Lin-driver
  LCD_SetSizeEx  (LAYER_INDEX, XSIZE_PHYS , YSIZE_PHYS);
  LCD_SetVSizeEx (LAYER_INDEX, XSIZE_PHYS, YSIZE_PHYS);
  LCD_SetVRAMAddrEx(LAYER_INDEX, (void*)VRAM_ADDR_PHYS);

  // Set custom callback function for copy operation
  LCD_SetDevFunc(0, LCD_DEVFUNC_COPYBUFFER, (void (*)())_CopyBuffer);

}
/*********************************************************************
*
*       LCD_X_DisplayDriver
*
* Purpose:
*   This function is called by the display driver for several purposes.
*   To support the according task the routine needs to be adapted to
*   the display controller. Please note that the commands marked with
*   'optional' are not cogently required and should only be adapted if
*   the display controller supports these features.
*
* Parameter:
*   LayerIndex - Index of layer to be configured
*   Cmd        - Please refer to the details in the switch statement below
*   pData      - Pointer to a LCD_X_DATA structure
*
* Return Value:
*   < -1 - Error
*     -1 - Command not handled
*      0 - OK
*/
int LCD_X_DisplayDriver(unsigned LayerIndex, unsigned Cmd, void * pData)
{
  int r;
  unsigned long Addr, BufferSize;

  switch (Cmd)
  {
    case LCD_X_SHOWBUFFER:
         {
            LCD_X_SHOWBUFFER_INFO * pData_info;
            pData_info = (LCD_X_SHOWBUFFER_INFO *)pData;

            // Calculate address of the given buffer
            BufferSize = (XSIZE_PHYS * YSIZE_PHYS * BITSPERPIXEL ) / 8;
            Addr = VRAM_ADDR_PHYS + (BufferSize * pData_info->Index);

            // Make the given buffer visible
            LPC_LCD->UPBASE = Addr;

            // Send a confirmation that the buffer is visible now
            GUI_MULTIBUF_Confirm(pData_info->Index);
            return 0;
        }


    case LCD_X_INITCONTROLLER:
         {
            // Called during the initialization process in order to set up the
            // display controller and put it into operation. If the display
            // controller is not initialized by any external routine this needs
            // to be adapted by the customer...
            _InitController();
            return 0;
          }

    case LCD_X_SETVRAMADDR:
         {
            // Required for setting the address of the video RAM for drivers
            // with memory mapped video RAM which is passed in the 'pVRAM' element of p
            //
            LCD_X_SETVRAMADDR_INFO * p;
            p = (LCD_X_SETVRAMADDR_INFO *)pData;
            LPC_LCD->UPBASE = (U32)(p->pVRAM);
            return 0;
          }
    case LCD_X_SETORG:
         {
            // Required for setting the display origin which is passed in the 'xPos' and 'yPos' element of p
            //
            LCD_X_SETORG_INFO * p;
            p = (LCD_X_SETORG_INFO *)pData;
            LPC_LCD->UPBASE = (U32)(VRAM_ADDR_PHYS + ((p->yPos) * YSIZE_PHYS * PIXEL_WIDTH));
            //_SetDisplayOrigin(p->xPos, p->yPos);
            return 0;
          }
    case LCD_X_SETLUTENTRY:
         {
            //
            // Required for setting a lookup table entry which is passed in the 'Pos' and 'Color' element of p
            //
            return 0;
         }
    case LCD_X_ON:
        {
          //
          // Required if the display controller should support switching on and off
          //
          return 0;
        }
    case LCD_X_OFF:
         {
            //
            // Required if the display controller should support switching on and off
            //
            // ...
            return 0;
          }
    default:
        r = -1;
        return r;
    }
}

/*************************** End of file ****************************/
