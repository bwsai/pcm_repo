/*****************************************************************************
* @copyright Copyright (c) 2012-2013 Beltway, Inc.
* @copyright This software is a copyrighted work and/or information
* @copyright protected as a trade secret. Legal rights of Beltway. In this
* @copyright software is distinct from ownership of any medium in which
* @copyright the software is embodied. Copyright or trade secret notices
* @copyright included must be reproduced in any copies authorized by
* @copyright Beltway, Inc.
*
* @detail Project      : Beltscale Weighing Product - Integrator Board
* @detail Customer     : Beltway
*
* @file Filename       : Display_task.h
* @brief               : Controller Board
*
* @author              : Anagha Basole
*
* @date Created        : Feb 13th Wednesday, 2013  <Feb 14, 2013>
* @date Last Modified  : Feb 13th Wednesday, 2013  <Feb 14, 2013>
*
* @internal Change Log : <YYYY-MM-DD>
* @internal            :
* @internal            :
*
*****************************************************************************/
#ifndef __DISPLAY_TASK_H
#define __DISPLAY_TASK_H

/*============================================================================
* Include Header Files
*===========================================================================*/
#include <RTL.h>

/*============================================================================
* Public Macro Definitions
*===========================================================================*/
/* Defines Section */

/*============================================================================
* Public Data Types
*===========================================================================*/

/*============================================================================
*Public Variables
*===========================================================================*/
/* Constants section */

/* Boolean variables section */

/* Character variables section */

/* unsigned integer variables section */

/* Signed integer variables section */

/* unsigned long integer variables section */

/* signed long integer variables section */

/* unsigned long long integer variables section */

/* signed long long integer variables section */

/* Float variables section */

/* Double variables section */

/* Structure or Union variables section */

/*============================================================================
* Public Function Declarations
*===========================================================================*/
extern __task void update_lcd (void);
extern void screen_variables_init(void);
void init_EEP_conf_vars (void);
#endif /*__DISPLAY_TASK_H*/
/*****************************************************************************
* End of file
*****************************************************************************/
